mod automation;
mod display;
mod macros;
mod sections;

use std::path::PathBuf;

use clap::Parser;

#[derive(Parser, Debug)]
struct Control {
    #[arg(long, name = "test-config", default_value = "data/tests.json")]
    test_config: Option<PathBuf>,
    // #[arg(short, long, default_value = None)]
    section: Option<String>,
    // #[arg(short, long, default_value = None)]
    tests: Option<Vec<String>>,
}

fn main() {
    let control = Control::parse();
    // println!("{control:?}");

    match control.section {
        None => println!("Please specify a section"),
        Some(section) => match section.to_ascii_lowercase().as_str() {
            "warmup" => sections::warmup::runners::run(control.tests),
            "strings" => sections::strings::runners::run(control.tests),
            "recursion" => sections::recursion::runners::run(control.tests),
            "logic" => sections::logic::runners::run(control.tests),
            "arrays" => sections::arrays::runners::run(control.tests),
            "ap" => sections::ap::runners::run(control.tests),
            _ => println!("Unknown section: {section}"),
        },
    }
}
