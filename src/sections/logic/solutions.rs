use super::implementations::{AlarmSetting, ChocolateResult, DayOfTheWeek, TableResult, TeaPartyResult, TicketResult};
use itertools::Itertools;
use std::cmp::{max, min};

pub fn cigar_party_solution(cigars: i32, is_weekend: bool) -> bool {
    cigars >= 40 && (is_weekend || cigars <= 60)
}

pub fn date_fashion_solution(you: i32, date: i32) -> TableResult {
    if you <= 2 || date <= 2 {
        TableResult::No
    } else if you >= 8 || date >= 8 {
        TableResult::Yes
    } else {
        TableResult::Maybe
    }
}

pub fn squirrel_play_solution(temp: i32, is_summer: bool) -> bool {
    temp >= 60 && temp <= (if is_summer { 100 } else { 90 })
}

pub fn caught_speeding_solution(speed: u32, is_birthday: bool) -> TicketResult {
    let speed = if is_birthday { max(speed, 5) - 5 } else { speed };
    match speed {
        0..=60 => TicketResult::NoTicket,
        61..=80 => TicketResult::SmallTicket,
        _ => TicketResult::BigTicket,
    }
}

pub fn sorta_sum_solution(a: i32, b: i32) -> i32 {
    match a + b {
        10..=19 => 20,
        e => e,
    }
}

pub fn alarm_clock_solution(day: DayOfTheWeek, vacation: bool) -> AlarmSetting {
    let weekday_time = if vacation { AlarmSetting::Late } else { AlarmSetting::Normal };
    let weekend_time = if vacation { AlarmSetting::Off } else { AlarmSetting::Late };

    match day {
        DayOfTheWeek::Saturday | DayOfTheWeek::Sunday => weekend_time,
        _ => weekday_time,
    }
}

pub fn love6_solution(a: i32, b: i32) -> bool {
    a == 6 || b == 6 || a + b == 6 || i32::abs_diff(a, b) == 6
}

pub fn in1_to10_solution(n: i32, outside_mode: bool) -> bool {
    match outside_mode {
        true => n <= 1 || n >= 10,
        false => n >= 1 && n <= 10,
    }
}

pub fn special_eleven_solution(n: i32) -> bool {
    match n % 11 {
        0..=1 => true,
        _ => false,
    }
}

pub fn more20_solution(n: i32) -> bool {
    match n % 20 {
        1..=2 => true,
        _ => false,
    }
}

pub fn old35_solution(n: i32) -> bool {
    (n % 3 == 0) ^ (n % 5 == 0)
}

pub fn less20_solution(n: i32) -> bool {
    match n % 20 {
        18..=19 => true,
        _ => false,
    }
}

pub fn near_ten_solution(num: i32) -> bool {
    match num % 10 {
        0..=2 | 8..=9 => true,
        _ => false,
    }
}

pub fn teen_sum_solution(a: i32, b: i32) -> i32 {
    let a_teen = a >= 13 && a <= 19;
    let b_teen = b >= 13 && b <= 19;
    if a_teen || b_teen {
        19
    } else {
        a + b
    }
}

pub fn answer_cell_solution(is_morning: bool, is_mom: bool, is_asleep: bool) -> bool {
    if is_asleep {
        false
    } else {
        if is_morning && is_mom {
            true
        } else {
            !is_morning
        }
    }
}

pub fn tea_party_solution(tea: i32, candy: i32) -> TeaPartyResult {
    let resource_deficient = tea < 5 || candy < 5;
    let resource_surplus = tea >= (candy * 2) || candy >= (tea * 2);

    if resource_deficient {
        TeaPartyResult::BadParty
    } else if resource_surplus {
        TeaPartyResult::GreatParty
    } else {
        TeaPartyResult::GoodParty
    }
}

pub fn fizz_string_solution(str: impl Into<String>) -> String {
    let input: String = str.into();
    let fizz = input.starts_with('f');
    let buzz = input.ends_with('b');

    if fizz || buzz {
        let f_str = if fizz { "Fizz" } else { "" };
        let b_str = if buzz { "Buzz" } else { "" };
        format!("{f_str}{b_str}")
    } else {
        input
    }
}

pub fn fizz_string2_solution(n: i32) -> String {
    let fizz = 0 == n % 3;
    let buzz = 0 == n % 5;
    let f_str = if fizz { "Fizz" } else { "" };
    let b_str = if buzz { "Buzz" } else { "" };

    if fizz || buzz {
        format!("{f_str}{b_str}!")
    } else {
        format!("{}!", n)
    }
}

pub fn two_as_one_solution(a: i32, b: i32, c: i32) -> bool {
    a + b == c || a + c == b || b + c == a
}

pub fn in_order_solution(a: i32, b: i32, c: i32, b_ok: bool) -> bool {
    c > b && ((b > a) || b_ok)
}

pub fn in_order_equal_solution(a: i32, b: i32, c: i32, equal_ok: bool) -> bool {
    match equal_ok {
        true => a <= b && b <= c,
        false => a < b && b < c,
    }
}

pub fn last_digit_solution(a: i32, b: i32, c: i32) -> bool {
    let a = a % 10;
    let b = b % 10;
    let c = c % 10;

    a == b || a == c || b == c
}

pub fn less_by10_solution(a: i32, b: i32, c: i32) -> bool {
    let nums = vec![a, b, c];
    let large = nums.iter().reduce(|a, e| max(a, e)).unwrap_or(&0);
    let small = nums.iter().reduce(|a, e| min(a, e)).unwrap_or(&0);

    (large - small) >= 10
}

pub fn without_doubles_solution(die1: i32, die2: i32, no_doubles: bool) -> i32 {
    if no_doubles && die1 == die2 {
        (die1 % 6) + die2 + 1
    } else {
        die1 + die2
    }
}

pub fn max_mod5_solution(a: i32, b: i32) -> i32 {
    if a == b {
        0
    } else {
        let a_mod = a % 5;
        let b_mod = b % 5;

        if a_mod == b_mod {
            min(a, b)
        } else {
            max(a, b)
        }
    }
}

pub fn red_ticket_solution(a: i32, b: i32, c: i32) -> i32 {
    match (a, b, c) {
        (2, 2, 2) => 10,
        (1, 1, 1) | (0, 0, 0) => 5,
        (i, j, k) => {
            if i != j && i != k {
                1
            } else {
                0
            }
        }
    }
}

pub fn green_ticket_solution(a: i32, b: i32, c: i32) -> i32 {
    let nums = vec![a, b, c];
    let qty = nums
        .iter() //
        .combinations(2)
        .map(|v| v[0] == v[1])
        .filter(|&c| c)
        .count();
    match qty {
        3 => 20,
        1 => 10,
        _ => 0,
    }
}

pub fn blue_ticket_solution(a: i32, b: i32, c: i32) -> i32 {
    let nums = vec![a, b, c];

    let be_ten = nums
        .iter() //
        .combinations(2)
        .map(|e| 10 == e[0] + e[1])
        .filter(|&e| e)
        .count()
        > 0;

    let ab = a + b;
    let ac10 = a + c + 10;
    let bc10 = b + c + 10;
    let be_five = ab == ac10 || ab == bc10;

    if be_ten {
        10
    } else if be_five {
        5
    } else {
        0
    }
}

pub fn share_digit_solution(a: i32, b: i32) -> bool {
    let a_vec = vec![a % 10, (a / 10) % 10];
    let b_vec = vec![b % 10, (b / 10) % 10];

    a_vec.iter().map(|e| b_vec.contains(e)).filter(|&e| e).count() > 0
}

pub fn sum_limit_solution(a: i32, b: i32) -> i32 {
    let test = |a: i32, s: i32| a.to_string().len() == s.to_string().len();
    let s = a + b;

    if test(a, s) {
        s
    } else {
        a
    }
}

pub fn make_bricks_solution(small: i32, big: i32, goal: i32) -> bool {
    (goal - (min(goal / 5, big) * 5)) <= small
}

pub fn lone_sum_solution(a: i32, b: i32, c: i32) -> i32 {
    let mut nums = Vec::<i32>::new();

    if a != b && a != c {
        nums.push(a);
    }

    if b != a && b != c {
        nums.push(b);
    }

    if c != a && c != b {
        nums.push(c);
    }

    nums.iter().sum()
}

pub fn lucky_sum_solution(a: i32, b: i32, c: i32) -> i32 {
    let nums = vec![a, b, c];
    let mut sum = 0;

    for i in 0..nums.len() {
        if nums[i] == 13 {
            break;
        } else {
            sum += nums[i];
        }
    }

    sum
}

pub fn no_teen_sum_solution(a: i32, b: i32, c: i32) -> i32 {
    let fix_teen = |v| match v {
        15 | 16 => v,
        13..=19 => 0,
        _ => v,
    };

    vec![a, b, c].iter().map(|&e| fix_teen(e)).reduce(|a, e| a + e).unwrap_or(0)
}

pub fn round_sum_solution(a: i32, b: i32, c: i32) -> i32 {
    let round_10 = |v| (((v as f64) / 10f64).round() * 10f64) as i32;

    vec![a, b, c].iter().map(|&e| round_10(e)).reduce(|a, e| a + e).unwrap_or(0)
}

pub fn close_far_solution(a: i32, b: i32, c: i32) -> bool {
    let test_close = |v: i32, t: i32| (v - t).abs() <= 1;
    let test_far = |v: i32, t: i32| (v - t).abs() >= 2;

    test_far(b, c) && // Wrap
        ((test_close(a, b) && test_far(a, c)) || // Wrap
         (test_close(a, c) && test_far(a, b)))
}

pub fn blackjack_solution(a: i32, b: i32) -> i32 {
    let a = if a > 21 { 0 } else { a };
    let b = if b > 21 { 0 } else { b };

    max(a, b)
}

pub fn evenly_spaced_solution(a: i32, b: i32, c: i32) -> bool {
    let mut nums = vec![a, b, c];
    nums.sort();

    (nums[2] - nums[1]) == (nums[1] - nums[0])
}

pub fn make_chocolate_solution(small: i32, big: i32, goal: i32) -> ChocolateResult {
    let small_needed = goal - (min(goal / 5, big) * 5);

    if small >= small_needed {
        ChocolateResult::Enough(small_needed)
    } else {
        ChocolateResult::NotEnough
    }
}
