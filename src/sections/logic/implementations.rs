use std::fmt::Display;

/// ## Logic-1 -- cigarParty
/// When squirrels get together for a party, they like to have cigars. A
/// squirrel party is successful when the number of cigars is between 40 and 60,
/// inclusive. Unless it is the weekend, in which case there is no upper bound
/// on the number of cigars. Return true if the party with the given values is
/// successful, or false otherwise.
/// ### Examples
///    cigar_party(30, false) -> false
///    cigar_party(50, false) -> true
///    cigar_party(70, true) -> true
#[allow(unused)]
pub fn cigar_party(cigars: i32, is_weekend: bool) -> bool {
    todo!()
}

/// ## Logic-1 -- dateFashion
/// You and your date are trying to get a table at a restaurant. The parameter
/// "you" is the stylishness of your clothes, in the range 0..10, and "date" is
/// the stylishness of your date's clothes. The result getting the table is
/// encoded as a TableResult enum of No, Maybe, or Yes. If either of you is very
/// stylish, 8 or more, then the result is Yes. With the exception that if
/// either of you has style of 2 or less, then the result is No. Otherwise
/// the result is Maybe.
/// ### Examples
///    date_fashion(5, 10) -> Yes
///    date_fashion(5, 2) -> No
///    date_fashion(5, 5) -> Maybe
#[allow(unused)]
pub fn date_fashion(you: i32, date: i32) -> TableResult {
    todo!()
}

#[derive(Debug, PartialEq)]
pub enum TableResult {
    No,
    Maybe,
    Yes,
}

impl Display for TableResult {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                TableResult::Maybe => String::from("Maybe"),
                TableResult::No => String::from("No"),
                TableResult::Yes => String::from("Yes"),
            }
        )
    }
}

/// ## Logic-1 -- squirrelPlay
/// The squirrels in Palo Alto spend most of the day playing. In particular,
/// they play if the temperature is between 60 and 90 (inclusive). Unless it is
/// summer, then the upper limit is 100 instead of 90. Given an int temperature
/// and a boolean isSummer, return true if the squirrels play and false
/// otherwise.
/// ### Examples
///    squirrel_play(70, false) -> true
///    squirrel_play(95, false) -> false
///    squirrel_play(95, true) -> true
#[allow(unused)]
pub fn squirrel_play(temp: i32, is_summer: bool) -> bool {
    todo!()
}

/// ## Logic-1 -- caughtSpeeding
/// You are driving a little too fast, and a police officer stops you. Write
/// code to compute the result, encoded as a TicketResult enum value of: NoTicket,
/// SmallTicket, BigTicket. If speed is 60 or less, the result is NoTicket. If speed is
/// between 61 and 80 inclusive, the result is SmallTicket. If speed is 81 or more, the
/// result is BigTicket. Unless it is your birthday -- on that day, your speed can be 5
/// higher in all cases.
/// ### Examples
///    caught_speeding(60, false) -> NoTicket
///    caught_speeding(65, false) -> SmallTicket
///    caught_speeding(65, true) -> NoTicket
#[allow(unused)]
pub fn caught_speeding(speed: u32, is_birthday: bool) -> TicketResult {
    todo!()
}

#[derive(Debug, PartialEq)]
pub enum TicketResult {
    NoTicket,
    SmallTicket,
    BigTicket,
}

impl Display for TicketResult {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                TicketResult::SmallTicket => String::from("Small Ticket"),
                TicketResult::NoTicket => String::from("No Ticket"),
                TicketResult::BigTicket => String::from("Big Ticket"),
            }
        )
    }
}

/// ## Logic-1 -- sortaSum
/// Given 2 ints, a and b, return their sum. However, sums in the range 10..19
/// inclusive, are forbidden, so in that case just return 20.
/// ### Examples
///    sorta_sum(3, 4) -> 7
///    sorta_sum(9, 4) -> 20
///    sorta_sum(10, 11) -> 21
#[allow(unused)]
pub fn sorta_sum(a: i32, b: i32) -> i32 {
    todo!()
}

/// ## Logic-1 -- alarmClock
/// Given a day of the week encoded as a DayOfTheWeek enum, and a
/// boolean indicating if we are on vacation, return a value from AlarmSetting
/// indicating when the alarm clock should ring. Weekdays, the alarm should be
/// Normal and on the weekend it should be Late. Unless we are on vacation --
/// then on weekdays it should be Late and weekends it should be Off.
/// ### Examples
///    alarm_clock(Monday, false) -> Normal
///    alarm_clock(Friday, false) -> Normal
///    alarm_clock(Sunday, false) -> Late
#[allow(unused)]
pub fn alarm_clock(day: DayOfTheWeek, vacation: bool) -> AlarmSetting {
    todo!()
}

#[allow(unused)]
#[derive(Debug)]
pub enum DayOfTheWeek {
    Sunday,
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday,
}

#[derive(Debug, PartialEq)]
pub enum AlarmSetting {
    Normal,
    Late,
    Off,
}

#[allow(unused)]
impl DayOfTheWeek {
    pub fn is_weekday(&self) -> bool {
        match self {
            DayOfTheWeek::Sunday | DayOfTheWeek::Saturday => false,
            _ => true,
        }
    }

    pub fn is_weekend(&self) -> bool {
        !self.is_weekday()
    }
}

impl Display for DayOfTheWeek {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                DayOfTheWeek::Sunday => String::from("Sunday"),
                DayOfTheWeek::Monday => String::from("Monday"),
                DayOfTheWeek::Tuesday => String::from("Tuesday"),
                DayOfTheWeek::Wednesday => String::from("Wednesday"),
                DayOfTheWeek::Thursday => String::from("Thursday"),
                DayOfTheWeek::Friday => String::from("Friday"),
                DayOfTheWeek::Saturday => String::from("Saturday"),
            }
        )
    }
}

impl Display for AlarmSetting {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                AlarmSetting::Normal => String::from("Normal"),
                AlarmSetting::Late => String::from("Late"),
                AlarmSetting::Off => String::from("Off"),
            }
        )
    }
}

/// ## Logic-1 -- love6
/// The number 6 is a truly great number. Given two int values, a and b, return
/// true if either one is 6. Or if their sum or difference is 6. Note: the
/// function Math.abs(num) computes the absolute value of a number.
/// ### Examples
///    love6(6, 4) -> true
///    love6(4, 5) -> false
///    love6(1, 5) -> true
#[allow(unused)]
pub fn love6(a: i32, b: i32) -> bool {
    todo!()
}

/// ## Logic-1 -- in1To10
/// Given a number n, return true if n is in the range 1..10, inclusive. Unless
/// "outsideMode" is true, in which case return true if the number is less or
/// equal to 1, or greater or equal to 10.
/// ### Examples
///    in1_to10(5, false) -> true
///    in1_to10(11, false) -> false
///    in1_to10(11, true) -> true
#[allow(unused)]
pub fn in1_to10(n: i32, outside_mode: bool) -> bool {
    todo!()
}

/// ## Logic-1 -- specialEleven
/// We'll say a number is special if it is a multiple of 11 or if it is one more
/// than a multiple of 11. Return true if the given non-negative number is
/// special. Use the % "mod" operator.
/// ### Examples
///    special_eleven(22) -> true
///    special_eleven(23) -> true
///    special_eleven(24) -> false
#[allow(unused)]
pub fn special_eleven(n: i32) -> bool {
    todo!()
}

/// ## Logic-1 -- more20
/// Return true if the given non-negative number is 1 or 2 more than a multiple
/// of 20.
/// ### Examples
///    more20(20) -> false
///    more20(21) -> true
///    more20(22) -> true
#[allow(unused)]
pub fn more20(n: i32) -> bool {
    todo!()
}

/// ## Logic-1 -- old35
/// Return true if the given non-negative number is a multiple of 3 or 5, but
/// not both. Use the % "mod" operator.
/// ### Examples
///    old35(3) -> true
///    old35(10) -> true
///    old35(15) -> false
#[allow(unused)]
pub fn old35(n: i32) -> bool {
    todo!()
}

/// ## Logic-1 -- less20
/// Return true if the given non-negative number is 1 or 2 less than a multiple
/// of 20. So for example 38 and 39 return true, but 40 returns false.
/// ### Examples
///    less20(18) -> true
///    less20(19) -> true
///    less20(20) -> false
#[allow(unused)]
pub fn less20(n: i32) -> bool {
    todo!()
}

/// ## Logic-1 -- nearTen
/// Given a non-negative number "num", return true if num is within 2 of a
/// multiple of 10. Note: (a % b) is the remainder of dividing a by b, so (7 %
/// 5) is 2.
/// ### Examples
///    near_ten(12) -> true
///    near_ten(17) -> false
///    near_ten(19) -> true
#[allow(unused)]
pub fn near_ten(num: i32) -> bool {
    todo!()
}

/// ## Logic-1 -- teenSum
/// Given 2 ints, a and b, return their sum. However, "teen" values in the range
/// 13..19 inclusive, are extra lucky. So if either value is a teen, just return
/// 19.
/// ### Examples
///    teen_sum(3, 4) -> 7
///    teen_sum(10, 13) -> 19
///    teen_sum(13, 2) -> 19
#[allow(unused)]
pub fn teen_sum(a: i32, b: i32) -> i32 {
    todo!()
}

/// ## Logic-1 -- answerCell
/// Your cell phone rings. Return true if you should answer it. Normally you
/// answer, except in the morning you only answer if it is your mom calling. In
/// all cases, if you are asleep, you do not answer.
/// ### Examples
///    answer_cell(false, false, false) -> true
///    answer_cell(false, false, true) -> false
///    answer_cell(true, false, false) -> false
#[allow(unused)]
pub fn answer_cell(is_morning: bool, is_mom: bool, is_asleep: bool) -> bool {
    todo!()
}

/// ## Logic-1 -- teaParty
/// We are having a party with amounts of tea and candy. Return the int outcome
/// of the party encoded as the enum TeaPartyResult. A party is GoodParty if
/// both tea and candy are at least 5. However, if either tea or candy is at
/// least double the amount of the other one, the party is GreatParty. However,
/// in all cases, if either tea or candy is less than 5, the party is always
/// BadParty.
/// ### Examples
///    tea_party(6, 8) -> GoodParty
///    tea_party(3, 8) -> BadParty
///    tea_party(20, 6) -> GreatParty
#[allow(unused)]
pub fn tea_party(tea: i32, candy: i32) -> TeaPartyResult {
    todo!()
}

#[derive(Debug, PartialEq)]
pub enum TeaPartyResult {
    BadParty,
    GoodParty,
    GreatParty,
}

impl Display for TeaPartyResult {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                TeaPartyResult::BadParty => String::from("Bad"),
                TeaPartyResult::GoodParty => String::from("Good"),
                TeaPartyResult::GreatParty => String::from("Great"),
            }
        )
    }
}

/// ## Logic-1 -- fizzString
/// Given a string str, if the string starts with "f" return "Fizz". If the
/// string ends with "b" return "Buzz". If both the "f" and "b" conditions are
/// true, return "FizzBuzz". In all other cases, return the string unchanged.
/// ### Examples
///    fizz_string("fig") -> "Fizz"
///    fizz_string("dib") -> "Buzz"
///    fizz_string("fib") -> "FizzBuzz"
#[allow(unused)]
pub fn fizz_string(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## Logic-1 -- fizzString2
/// Given an int n, return the string form of the number followed by "!". So the
/// int 6 yields "6!". Except if the number is divisible by 3 use "Fizz" instead
/// of the number, and if the number is divisible by 5 use "Buzz", and if
/// divisible by both 3 and 5, use "FizzBuzz". Note: the % "mod" operator
/// computes the remainder after division, so 23 % 10 yields 3. What will the
/// remainder be when one number divides evenly into another?
/// ### Examples
///    fizz_string2(1) -> "1!"
///    fizz_string2(2) -> "2!"
///    fizz_string2(3) -> "Fizz!"
#[allow(unused)]
pub fn fizz_string2(n: i32) -> String {
    todo!()
}

/// ## Logic-1 -- twoAsOne
/// Given three ints, a b c, return true if it is possible to add two of the
/// ints to get the third.
/// ### Examples
///    two_as_one(1, 2, 3) -> true
///    two_as_one(3, 1, 2) -> true
///    two_as_one(3, 2, 2) -> false
#[allow(unused)]
pub fn two_as_one(a: i32, b: i32, c: i32) -> bool {
    todo!()
}

/// ## Logic-1 -- inOrder
/// Given three ints, a b c, return true if b is greater than a, and c is
/// greater than b. However, with the exception that if "bOk" is true, b does
/// not need to be greater than a.
/// ### Examples
///    in_order(1, 2, 4, false) -> true
///    in_order(1, 2, 1, false) -> false
///    in_order(1, 1, 2, true) -> true
#[allow(unused)]
pub fn in_order(a: i32, b: i32, c: i32, b_ok: bool) -> bool {
    todo!()
}

/// ## Logic-1 -- inOrderEqual
/// Given three ints, a b c, return true if they are in strict increasing order,
/// such as 2 5 11, or 5 6 7, but not 6 5 7 or 5 5 7. However, with the
/// exception that if "equalOk" is true, equality is allowed, such as 5 5 7 or 5
/// 5 5.
/// ### Examples
///    in_order_equal(2, 5, 11, false) -> true
///    in_order_equal(5, 7, 6, false) -> false
///    in_order_equal(5, 5, 7, true) -> true
#[allow(unused)]
pub fn in_order_equal(a: i32, b: i32, c: i32, equal_ok: bool) -> bool {
    todo!()
}

/// ## Logic-1 -- lastDigit
/// Given three ints, a b c, return true if two or more of them have the same
/// rightmost digit. The ints are non-negative. Note: the % "mod" operator
/// computes the remainder, e.g. 17 % 10 is 7.
/// ### Examples
///    last_digit(23, 19, 13) -> false
///    last_digit(23, 19, 12) -> false
///    last_digit(23, 19, 3) -> true
#[allow(unused)]
pub fn last_digit(a: i32, b: i32, c: i32) -> bool {
    todo!()
}

/// ## Logic-1 -- lessBy10
/// Given three ints, a b c, return true if one of them is 10 or more less than
/// one of the others.
/// ### Examples
///    less_by10(1, 7, 11) -> true
///    less_by10(1, 7, 10) -> false
///    less_by10(11, 1, 7) -> true
#[allow(unused)]
pub fn less_by10(a: i32, b: i32, c: i32) -> bool {
    todo!()
}

/// ## Logic-1 -- withoutDoubles
/// Return the sum of two 6-sided dice rolls, each in the range 1..6. However,
/// if noDoubles is true, if the two dice show the same value, increment one die
/// to the next value, wrapping around to 1 if its value was 6.
/// ### Examples
///    without_doubles(2, 3, true) -> 5
///    without_doubles(3, 3, true) -> 7
///    without_doubles(3, 3, false) -> 6
#[allow(unused)]
pub fn without_doubles(die1: i32, die2: i32, no_doubles: bool) -> i32 {
    todo!()
}

/// ## Logic-1 -- maxMod5
/// Given two int values, return whichever value is larger. However if the two
/// values have the same remainder when divided by 5, then the return the
/// smaller value. However, in all cases, if the two values are the same, return
/// 0. Note: the % "mod" operator computes the remainder, e.g. 7 % 5 is 2.
/// ### Examples
///    max_mod5(2, 3) -> 3
///    max_mod5(6, 2) -> 6
///    max_mod5(3, 2) -> 3
#[allow(unused)]
pub fn max_mod5(a: i32, b: i32) -> i32 {
    todo!()
}

/// ## Logic-1 -- redTicket
/// You have a red lottery ticket showing ints a, b, and c, each of which is 0,
/// 1, or 2. If they are all the value 2, the result is 10. Otherwise if they
/// are all the same, the result is 5. Otherwise so long as both b and c are
/// different from a, the result is 1. Otherwise the result is 0.
/// ### Examples
///    red_ticket(2, 2, 2) -> 10
///    red_ticket(2, 2, 1) -> 0
///    red_ticket(0, 0, 0) -> 5
#[allow(unused)]
pub fn red_ticket(a: i32, b: i32, c: i32) -> i32 {
    todo!()
}

/// ## Logic-1 -- greenTicket
/// You have a green lottery ticket, with ints a, b, and c on it. If the numbers
/// are all different from each other, the result is 0. If all of the numbers
/// are the same, the result is 20. If two of the numbers are the same, the
/// result is 10.
/// ### Examples
///    green_ticket(1, 2, 3) -> 0
///    green_ticket(2, 2, 2) -> 20
///    green_ticket(1, 1, 2) -> 10
#[allow(unused)]
pub fn green_ticket(a: i32, b: i32, c: i32) -> i32 {
    todo!()
}

/// ## Logic-1 -- blueTicket
/// You have a blue lottery ticket, with ints a, b, and c on it. This makes
/// three pairs, which we'll call ab, bc, and ac. Consider the sum of the
/// numbers in each pair. If any pair sums to exactly 10, the result is 10.
/// Otherwise if the ab sum is exactly 10 more than either bc or ac sums, the
/// result is 5. Otherwise the result is 0.
/// ### Examples
///    blue_ticket(9, 1, 0) -> 10
///    blue_ticket(9, 2, 0) -> 0
///    blue_ticket(6, 1, 4) -> 10
#[allow(unused)]
pub fn blue_ticket(a: i32, b: i32, c: i32) -> i32 {
    todo!()
}

/// ## Logic-1 -- shareDigit
/// Given two ints, each in the range 10..99, return true if there is a digit
/// that appears in both numbers, such as the 2 in 12 and 23. (Note: division,
/// e.g. n/10, gives the left digit while the % "mod" n%10 gives the right
/// digit.)
/// ### Examples
///    share_digit(12, 23) -> true
///    share_digit(12, 43) -> false
///    share_digit(12, 44) -> false
#[allow(unused)]
pub fn share_digit(a: i32, b: i32) -> bool {
    todo!()
}

/// ## Logic-1 -- sumLimit
/// Given 2 non-negative ints, a and b, return their sum, so long as the sum has
/// the same number of digits as a. If the sum has more digits than a, just
/// return a without b. (Note: one way to compute the number of digits of a
/// non-negative int n is to convert it to a string with String.valueOf(n) and
/// then check the length of the string.)
/// ### Examples
///    sum_limit(2, 3) -> 5
///    sum_limit(8, 3) -> 8
///    sum_limit(8, 1) -> 9
#[allow(unused)]
pub fn sum_limit(a: i32, b: i32) -> i32 {
    todo!()
}

/// ## Logic-2 -- makeBricks
/// We want to make a row of bricks that is goal inches long. We have a number
/// of small bricks (1 inch each) and big bricks (5 inches each). Return true if
/// it is possible to make the goal by choosing from the given bricks. This is a
/// little harder than it looks and can be done without any loops. See also:
/// Introduction to MakeBricks
/// ### Examples
///    make_bricks(3, 1, 8) -> true
///    make_bricks(3, 1, 9) -> false
///    make_bricks(3, 2, 10) -> true
#[allow(unused)]
pub fn make_bricks(small: i32, big: i32, goal: i32) -> bool {
    todo!()
}

/// ## Logic-2 -- loneSum
/// Given 3 int values, a b c, return their sum. However, if one of the values
/// is the same as another of the values, it does not count towards the sum.
/// ### Examples
///    lone_sum(1, 2, 3) -> 6
///    lone_sum(3, 2, 3) -> 2
///    lone_sum(3, 3, 3) -> 0
#[allow(unused)]
pub fn lone_sum(a: i32, b: i32, c: i32) -> i32 {
    todo!()
}

/// ## Logic-2 -- luckySum
/// Given 3 int values, a b c, return their sum. However, if one of the values
/// is 13 then it does not count towards the sum and values to its right do not
/// count. So for example, if b is 13, then both b and c do not count.
/// ### Examples
///    lucky_sum(1, 2, 3) -> 6
///    lucky_sum(1, 2, 13) -> 3
///    lucky_sum(1, 13, 3) -> 1
#[allow(unused)]
pub fn lucky_sum(a: i32, b: i32, c: i32) -> i32 {
    todo!()
}

/// ## Logic-2 -- noTeenSum
/// Given 3 int values, a b c, return their sum. However, if any of the values
/// is a teen -- in the range 13..19 inclusive -- then that value counts as 0,
/// except 15 and 16 do not count as a teens. Write a separate helper "public
/// int fixTeen(int n) {"that takes in an int value and returns that value fixed
/// for the teen rule. In this way, you avoid repeating the teen code 3 times
/// (i.e. "decomposition"). Define the helper below and at the same indent level
/// as the main noTeenSum().
/// ### Examples
///    no_teen_sum(1, 2, 3) -> 6
///    no_teen_sum(2, 13, 1) -> 3
///    no_teen_sum(2, 1, 14) -> 3
#[allow(unused)]
pub fn no_teen_sum(a: i32, b: i32, c: i32) -> i32 {
    todo!()
}

/// ## Logic-2 -- roundSum
/// For this problem, we'll round an int value up to the next multiple of 10 if
/// its rightmost digit is 5 or more, so 15 rounds up to 20. Alternately, round
/// down to the previous multiple of 10 if its rightmost digit is less than 5,
/// so 12 rounds down to 10. Given 3 ints, a b c, return the sum of their
/// rounded values. To avoid code repetition, write a separate helper "public
/// int round10(int num) {" and call it 3 times. Write the helper entirely below
/// and at the same indent level as roundSum().
/// ### Examples
///    round_sum(16, 17, 18) -> 60
///    round_sum(12, 13, 14) -> 30
///    round_sum(6, 4, 4) -> 10
#[allow(unused)]
pub fn round_sum(a: i32, b: i32, c: i32) -> i32 {
    todo!()
}

/// ## Logic-2 -- closeFar
/// Given three ints, a b c, return true if one of b or c is "close" (differing
/// from a by at most 1), while the other is "far", differing from both other
/// values by 2 or more. Note: Math.abs(num) computes the absolute value of a
/// number.
/// ### Examples
///    close_far(1, 2, 10) -> true
///    close_far(1, 2, 3) -> false
///    close_far(4, 1, 3) -> true
#[allow(unused)]
pub fn close_far(a: i32, b: i32, c: i32) -> bool {
    todo!()
}

/// ## Logic-2 -- blackjack
/// Given 2 int values greater than 0, return whichever value is nearest to 21
/// without going over. Return 0 if they both go over.
/// ### Examples
///    blackjack(19, 21) -> 21
///    blackjack(21, 19) -> 21
///    blackjack(19, 22) -> 19
#[allow(unused)]
pub fn blackjack(a: i32, b: i32) -> i32 {
    todo!()
}

/// ## Logic-2 -- evenlySpaced
/// Given three ints, a b c, one of them is small, one is medium and one is
/// large. Return true if the three values are evenly spaced, so the difference
/// between small and medium is the same as the difference between medium and
/// large.
/// ### Examples
///    evenly_spaced(2, 4, 6) -> true
///    evenly_spaced(4, 6, 2) -> true
///    evenly_spaced(4, 6, 3) -> false
#[allow(unused)]
pub fn evenly_spaced(a: i32, b: i32, c: i32) -> bool {
    todo!()
}

/// ## Logic-2 -- makeChocolate
/// We want make a package of goal kilos of chocolate. We have small bars (1
/// kilo each) and big bars (5 kilos each). Return the number of small bars to
/// use, assuming we always use big bars before small bars. Return NotEnough
/// if it can't be done.
/// ### Examples
///    make_chocolate(4, 1, 9) -> Enough(4)
///    make_chocolate(4, 1, 10) -> NotEnough
///    make_chocolate(4, 1, 7) -> Enough(2)
#[allow(unused)]
pub fn make_chocolate(small: i32, big: i32, goal: i32) -> ChocolateResult {
    todo!()
}

#[derive(Debug, PartialEq)]
pub enum ChocolateResult {
    NotEnough,
    Enough(i32),
}

impl Display for ChocolateResult {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                ChocolateResult::NotEnough => String::from("Not enough bars"),
                ChocolateResult::Enough(n) => format!("Used {n} bars"),
            }
        )
    }
}
