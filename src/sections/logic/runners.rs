use super::implementations::*;
use super::solutions::*;
use crate::display::TestDisplay;

pub fn run(test: Option<Vec<String>>) {
    match test {
        // None => println!("Please specify a test name"),
        None => {
            cigar_party_run();
            date_fashion_run();
            squirrel_play_run();
            caught_speeding_run();
            sorta_sum_run();
            alarm_clock_run();
            love6_run();
            in1_to10_run();
            special_eleven_run();
            more20_run();
            old35_run();
            less20_run();
            near_ten_run();
            teen_sum_run();
            answer_cell_run();
            tea_party_run();
            fizz_string_run();
            fizz_string2_run();
            two_as_one_run();
            in_order_run();
            in_order_equal_run();
            last_digit_run();
            less_by10_run();
            without_doubles_run();
            max_mod5_run();
            red_ticket_run();
            green_ticket_run();
            blue_ticket_run();
            share_digit_run();
            sum_limit_run();
            make_bricks_run();
            lone_sum_run();
            lucky_sum_run();
            no_teen_sum_run();
            round_sum_run();
            close_far_run();
            blackjack_run();
            evenly_spaced_run();
            make_chocolate_run();
        }
        Some(tests) => {
            for test_name in tests {
                match test_name.to_ascii_lowercase().as_str() {
                    "cigarParty" | "cigar_party" => cigar_party_run(),
                    "dateFashion" | "date_fashion" => date_fashion_run(),
                    "squirrelPlay" | "squirrel_play" => squirrel_play_run(),
                    "caughtSpeeding" | "caught_speeding" => caught_speeding_run(),
                    "sortaSum" | "sorta_sum" => sorta_sum_run(),
                    "alarmClock" | "alarm_clock" => alarm_clock_run(),
                    "love6" => love6_run(),
                    "in1To10" | "in1_to10" => in1_to10_run(),
                    "specialEleven" | "special_eleven" => special_eleven_run(),
                    "more20" => more20_run(),
                    "old35" => old35_run(),
                    "less20" => less20_run(),
                    "nearTen" | "near_ten" => near_ten_run(),
                    "teenSum" | "teen_sum" => teen_sum_run(),
                    "answerCell" | "answer_cell" => answer_cell_run(),
                    "teaParty" | "tea_party" => tea_party_run(),
                    "fizzString" | "fizz_string" => fizz_string_run(),
                    "fizzString2" | "fizz_string2" => fizz_string2_run(),
                    "twoAsOne" | "two_as_one" => two_as_one_run(),
                    "inOrder" | "in_order" => in_order_run(),
                    "inOrderEqual" | "in_order_equal" => in_order_equal_run(),
                    "lastDigit" | "last_digit" => last_digit_run(),
                    "lessBy10" | "less_by10" => less_by10_run(),
                    "withoutDoubles" | "without_doubles" => without_doubles_run(),
                    "maxMod5" | "max_mod5" => max_mod5_run(),
                    "redTicket" | "red_ticket" => red_ticket_run(),
                    "greenTicket" | "green_ticket" => green_ticket_run(),
                    "blueTicket" | "blue_ticket" => blue_ticket_run(),
                    "shareDigit" | "share_digit" => share_digit_run(),
                    "sumLimit" | "sum_limit" => sum_limit_run(),
                    "makeBricks" | "make_bricks" => make_bricks_run(),
                    "loneSum" | "lone_sum" => lone_sum_run(),
                    "luckySum" | "lucky_sum" => lucky_sum_run(),
                    "noTeenSum" | "no_teen_sum" => no_teen_sum_run(),
                    "roundSum" | "round_sum" => round_sum_run(),
                    "closeFar" | "close_far" => close_far_run(),
                    "blackjack" => blackjack_run(),
                    "evenlySpaced" | "evenly_spaced" => evenly_spaced_run(),
                    "makeChocolate" | "make_chocolate" => make_chocolate_run(),
                    _ => println!("No test named {test_name}"),
                };
            }
        }
    };
}

pub fn cigar_party_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("cigar_party");
    layout.print_header();
    layout.print_test(format!("{:?}", (30, false)), cigar_party_solution(30, false), cigar_party(30, false));
    layout.print_test(format!("{:?}", (50, false)), cigar_party_solution(50, false), cigar_party(50, false));
    layout.print_test(format!("{:?}", (70, true)), cigar_party_solution(70, true), cigar_party(70, true));
    layout.print_test(format!("{:?}", (30, true)), cigar_party_solution(30, true), cigar_party(30, true));
    layout.print_test(format!("{:?}", (50, true)), cigar_party_solution(50, true), cigar_party(50, true));
    layout.print_test(format!("{:?}", (60, false)), cigar_party_solution(60, false), cigar_party(60, false));
    layout.print_test(format!("{:?}", (61, false)), cigar_party_solution(61, false), cigar_party(61, false));
    layout.print_test(format!("{:?}", (40, false)), cigar_party_solution(40, false), cigar_party(40, false));
    layout.print_test(format!("{:?}", (39, false)), cigar_party_solution(39, false), cigar_party(39, false));
    layout.print_test(format!("{:?}", (40, true)), cigar_party_solution(40, true), cigar_party(40, true));
    layout.print_test(format!("{:?}", (39, true)), cigar_party_solution(39, true), cigar_party(39, true));
    layout.print_footer();
}

pub fn date_fashion_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("date_fashion");
    layout.print_header();
    layout.print_test(format!("{:?}", (5, 10)), date_fashion_solution(5, 10), date_fashion(5, 10));
    layout.print_test(format!("{:?}", (5, 2)), date_fashion_solution(5, 2), date_fashion(5, 2));
    layout.print_test(format!("{:?}", (5, 5)), date_fashion_solution(5, 5), date_fashion(5, 5));
    layout.print_test(format!("{:?}", (3, 3)), date_fashion_solution(3, 3), date_fashion(3, 3));
    layout.print_test(format!("{:?}", (10, 2)), date_fashion_solution(10, 2), date_fashion(10, 2));
    layout.print_test(format!("{:?}", (2, 9)), date_fashion_solution(2, 9), date_fashion(2, 9));
    layout.print_test(format!("{:?}", (9, 9)), date_fashion_solution(9, 9), date_fashion(9, 9));
    layout.print_test(format!("{:?}", (10, 5)), date_fashion_solution(10, 5), date_fashion(10, 5));
    layout.print_test(format!("{:?}", (2, 2)), date_fashion_solution(2, 2), date_fashion(2, 2));
    layout.print_test(format!("{:?}", (3, 7)), date_fashion_solution(3, 7), date_fashion(3, 7));
    layout.print_test(format!("{:?}", (2, 7)), date_fashion_solution(2, 7), date_fashion(2, 7));
    layout.print_test(format!("{:?}", (6, 2)), date_fashion_solution(6, 2), date_fashion(6, 2));
    layout.print_footer();
}

pub fn squirrel_play_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("squirrel_play");
    layout.print_header();
    layout.print_test(format!("{:?}", (70, false)), squirrel_play_solution(70, false), squirrel_play(70, false));
    layout.print_test(format!("{:?}", (95, false)), squirrel_play_solution(95, false), squirrel_play(95, false));
    layout.print_test(format!("{:?}", (95, true)), squirrel_play_solution(95, true), squirrel_play(95, true));
    layout.print_test(format!("{:?}", (90, false)), squirrel_play_solution(90, false), squirrel_play(90, false));
    layout.print_test(format!("{:?}", (90, true)), squirrel_play_solution(90, true), squirrel_play(90, true));
    layout.print_test(format!("{:?}", (50, false)), squirrel_play_solution(50, false), squirrel_play(50, false));
    layout.print_test(format!("{:?}", (50, true)), squirrel_play_solution(50, true), squirrel_play(50, true));
    layout.print_test(format!("{:?}", (100, false)), squirrel_play_solution(100, false), squirrel_play(100, false));
    layout.print_test(format!("{:?}", (100, true)), squirrel_play_solution(100, true), squirrel_play(100, true));
    layout.print_test(format!("{:?}", (105, true)), squirrel_play_solution(105, true), squirrel_play(105, true));
    layout.print_test(format!("{:?}", (59, false)), squirrel_play_solution(59, false), squirrel_play(59, false));
    layout.print_test(format!("{:?}", (59, true)), squirrel_play_solution(59, true), squirrel_play(59, true));
    layout.print_test(format!("{:?}", (60, false)), squirrel_play_solution(60, false), squirrel_play(60, false));
    layout.print_footer();
}

pub fn caught_speeding_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("caught_speeding");
    layout.print_header();
    layout.print_test(format!("{:?}", (60, false)), caught_speeding_solution(60, false), caught_speeding(60, false));
    layout.print_test(format!("{:?}", (65, false)), caught_speeding_solution(65, false), caught_speeding(65, false));
    layout.print_test(format!("{:?}", (65, true)), caught_speeding_solution(65, true), caught_speeding(65, true));
    layout.print_test(format!("{:?}", (80, false)), caught_speeding_solution(80, false), caught_speeding(80, false));
    layout.print_test(format!("{:?}", (85, false)), caught_speeding_solution(85, false), caught_speeding(85, false));
    layout.print_test(format!("{:?}", (85, true)), caught_speeding_solution(85, true), caught_speeding(85, true));
    layout.print_test(format!("{:?}", (70, false)), caught_speeding_solution(70, false), caught_speeding(70, false));
    layout.print_test(format!("{:?}", (75, false)), caught_speeding_solution(75, false), caught_speeding(75, false));
    layout.print_test(format!("{:?}", (75, true)), caught_speeding_solution(75, true), caught_speeding(75, true));
    layout.print_test(format!("{:?}", (40, false)), caught_speeding_solution(40, false), caught_speeding(40, false));
    layout.print_test(format!("{:?}", (40, true)), caught_speeding_solution(40, true), caught_speeding(40, true));
    layout.print_test(format!("{:?}", (90, false)), caught_speeding_solution(90, false), caught_speeding(90, false));
    layout.print_footer();
}

pub fn sorta_sum_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("sorta_sum");
    layout.print_header();
    layout.print_test(format!("{:?}", (3, 4)), sorta_sum_solution(3, 4), sorta_sum(3, 4));
    layout.print_test(format!("{:?}", (9, 4)), sorta_sum_solution(9, 4), sorta_sum(9, 4));
    layout.print_test(format!("{:?}", (10, 11)), sorta_sum_solution(10, 11), sorta_sum(10, 11));
    layout.print_test(format!("{:?}", (12, -3)), sorta_sum_solution(12, -3), sorta_sum(12, -3));
    layout.print_test(format!("{:?}", (-3, 12)), sorta_sum_solution(-3, 12), sorta_sum(-3, 12));
    layout.print_test(format!("{:?}", (4, 5)), sorta_sum_solution(4, 5), sorta_sum(4, 5));
    layout.print_test(format!("{:?}", (4, 6)), sorta_sum_solution(4, 6), sorta_sum(4, 6));
    layout.print_test(format!("{:?}", (14, 7)), sorta_sum_solution(14, 7), sorta_sum(14, 7));
    layout.print_test(format!("{:?}", (14, 6)), sorta_sum_solution(14, 6), sorta_sum(14, 6));
    layout.print_footer();
}

pub fn alarm_clock_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("alarm_clock");
    layout.print_header();
    layout.print_test(format!("{:?}", (DayOfTheWeek::Monday, false)), alarm_clock_solution(DayOfTheWeek::Monday, false), alarm_clock(DayOfTheWeek::Monday, false));
    layout.print_test(format!("{:?}", (DayOfTheWeek::Friday, false)), alarm_clock_solution(DayOfTheWeek::Friday, false), alarm_clock(DayOfTheWeek::Friday, false));
    layout.print_test(format!("{:?}", (DayOfTheWeek::Sunday, false)), alarm_clock_solution(DayOfTheWeek::Sunday, false), alarm_clock(DayOfTheWeek::Sunday, false));
    layout.print_test(format!("{:?}", (DayOfTheWeek::Saturday, false)), alarm_clock_solution(DayOfTheWeek::Saturday, false), alarm_clock(DayOfTheWeek::Saturday, false));
    layout.print_test(format!("{:?}", (DayOfTheWeek::Sunday, true)), alarm_clock_solution(DayOfTheWeek::Sunday, true), alarm_clock(DayOfTheWeek::Sunday, true));
    layout.print_test(format!("{:?}", (DayOfTheWeek::Saturday, true)), alarm_clock_solution(DayOfTheWeek::Saturday, true), alarm_clock(DayOfTheWeek::Saturday, true));
    layout.print_test(format!("{:?}", (DayOfTheWeek::Monday, true)), alarm_clock_solution(DayOfTheWeek::Monday, true), alarm_clock(DayOfTheWeek::Monday, true));
    layout.print_test(format!("{:?}", (DayOfTheWeek::Wednesday, true)), alarm_clock_solution(DayOfTheWeek::Wednesday, true), alarm_clock(DayOfTheWeek::Wednesday, true));
    layout.print_test(format!("{:?}", (DayOfTheWeek::Friday, true)), alarm_clock_solution(DayOfTheWeek::Friday, true), alarm_clock(DayOfTheWeek::Friday, true));
    layout.print_footer();
}

pub fn love6_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("love6");
    layout.print_header();
    layout.print_test(format!("{:?}", (6, 4)), love6_solution(6, 4), love6(6, 4));
    layout.print_test(format!("{:?}", (4, 5)), love6_solution(4, 5), love6(4, 5));
    layout.print_test(format!("{:?}", (1, 5)), love6_solution(1, 5), love6(1, 5));
    layout.print_test(format!("{:?}", (1, 6)), love6_solution(1, 6), love6(1, 6));
    layout.print_test(format!("{:?}", (1, 8)), love6_solution(1, 8), love6(1, 8));
    layout.print_test(format!("{:?}", (1, 7)), love6_solution(1, 7), love6(1, 7));
    layout.print_test(format!("{:?}", (7, 5)), love6_solution(7, 5), love6(7, 5));
    layout.print_test(format!("{:?}", (8, 2)), love6_solution(8, 2), love6(8, 2));
    layout.print_test(format!("{:?}", (6, 6)), love6_solution(6, 6), love6(6, 6));
    layout.print_test(format!("{:?}", (-6, 2)), love6_solution(-6, 2), love6(-6, 2));
    layout.print_test(format!("{:?}", (-4, -10)), love6_solution(-4, -10), love6(-4, -10));
    layout.print_test(format!("{:?}", (-7, 1)), love6_solution(-7, 1), love6(-7, 1));
    layout.print_test(format!("{:?}", (7, -1)), love6_solution(7, -1), love6(7, -1));
    layout.print_test(format!("{:?}", (-6, 12)), love6_solution(-6, 12), love6(-6, 12));
    layout.print_test(format!("{:?}", (-2, -4)), love6_solution(-2, -4), love6(-2, -4));
    layout.print_test(format!("{:?}", (7, 1)), love6_solution(7, 1), love6(7, 1));
    layout.print_test(format!("{:?}", (0, 9)), love6_solution(0, 9), love6(0, 9));
    layout.print_test(format!("{:?}", (8, 3)), love6_solution(8, 3), love6(8, 3));
    layout.print_test(format!("{:?}", (3, 3)), love6_solution(3, 3), love6(3, 3));
    layout.print_test(format!("{:?}", (3, 4)), love6_solution(3, 4), love6(3, 4));
    layout.print_footer();
}

pub fn in1_to10_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("in1_to10");
    layout.print_header();
    layout.print_test(format!("{:?}", (5, false)), in1_to10_solution(5, false), in1_to10(5, false));
    layout.print_test(format!("{:?}", (11, false)), in1_to10_solution(11, false), in1_to10(11, false));
    layout.print_test(format!("{:?}", (11, true)), in1_to10_solution(11, true), in1_to10(11, true));
    layout.print_test(format!("{:?}", (10, false)), in1_to10_solution(10, false), in1_to10(10, false));
    layout.print_test(format!("{:?}", (10, true)), in1_to10_solution(10, true), in1_to10(10, true));
    layout.print_test(format!("{:?}", (9, false)), in1_to10_solution(9, false), in1_to10(9, false));
    layout.print_test(format!("{:?}", (9, true)), in1_to10_solution(9, true), in1_to10(9, true));
    layout.print_test(format!("{:?}", (1, false)), in1_to10_solution(1, false), in1_to10(1, false));
    layout.print_test(format!("{:?}", (1, true)), in1_to10_solution(1, true), in1_to10(1, true));
    layout.print_test(format!("{:?}", (0, false)), in1_to10_solution(0, false), in1_to10(0, false));
    layout.print_test(format!("{:?}", (0, true)), in1_to10_solution(0, true), in1_to10(0, true));
    layout.print_test(format!("{:?}", (-1, false)), in1_to10_solution(-1, false), in1_to10(-1, false));
    layout.print_footer();
}

pub fn special_eleven_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("special_eleven");
    layout.print_header();
    layout.print_test(format!("{:?}", (22)), special_eleven_solution(22), special_eleven(22));
    layout.print_test(format!("{:?}", (23)), special_eleven_solution(23), special_eleven(23));
    layout.print_test(format!("{:?}", (24)), special_eleven_solution(24), special_eleven(24));
    layout.print_test(format!("{:?}", (21)), special_eleven_solution(21), special_eleven(21));
    layout.print_test(format!("{:?}", (11)), special_eleven_solution(11), special_eleven(11));
    layout.print_test(format!("{:?}", (12)), special_eleven_solution(12), special_eleven(12));
    layout.print_test(format!("{:?}", (10)), special_eleven_solution(10), special_eleven(10));
    layout.print_test(format!("{:?}", (9)), special_eleven_solution(9), special_eleven(9));
    layout.print_test(format!("{:?}", (8)), special_eleven_solution(8), special_eleven(8));
    layout.print_test(format!("{:?}", (0)), special_eleven_solution(0), special_eleven(0));
    layout.print_test(format!("{:?}", (1)), special_eleven_solution(1), special_eleven(1));
    layout.print_test(format!("{:?}", (2)), special_eleven_solution(2), special_eleven(2));
    layout.print_test(format!("{:?}", (121)), special_eleven_solution(121), special_eleven(121));
    layout.print_test(format!("{:?}", (122)), special_eleven_solution(122), special_eleven(122));
    layout.print_test(format!("{:?}", (123)), special_eleven_solution(123), special_eleven(123));
    layout.print_test(format!("{:?}", (46)), special_eleven_solution(46), special_eleven(46));
    layout.print_test(format!("{:?}", (49)), special_eleven_solution(49), special_eleven(49));
    layout.print_test(format!("{:?}", (52)), special_eleven_solution(52), special_eleven(52));
    layout.print_test(format!("{:?}", (54)), special_eleven_solution(54), special_eleven(54));
    layout.print_test(format!("{:?}", (55)), special_eleven_solution(55), special_eleven(55));
    layout.print_footer();
}

pub fn more20_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("more20");
    layout.print_header();
    layout.print_test(format!("{:?}", (20)), more20_solution(20), more20(20));
    layout.print_test(format!("{:?}", (21)), more20_solution(21), more20(21));
    layout.print_test(format!("{:?}", (22)), more20_solution(22), more20(22));
    layout.print_test(format!("{:?}", (23)), more20_solution(23), more20(23));
    layout.print_test(format!("{:?}", (25)), more20_solution(25), more20(25));
    layout.print_test(format!("{:?}", (30)), more20_solution(30), more20(30));
    layout.print_test(format!("{:?}", (31)), more20_solution(31), more20(31));
    layout.print_test(format!("{:?}", (59)), more20_solution(59), more20(59));
    layout.print_test(format!("{:?}", (60)), more20_solution(60), more20(60));
    layout.print_test(format!("{:?}", (61)), more20_solution(61), more20(61));
    layout.print_test(format!("{:?}", (62)), more20_solution(62), more20(62));
    layout.print_test(format!("{:?}", (1020)), more20_solution(1020), more20(1020));
    layout.print_test(format!("{:?}", (1021)), more20_solution(1021), more20(1021));
    layout.print_test(format!("{:?}", (1000)), more20_solution(1000), more20(1000));
    layout.print_test(format!("{:?}", (1001)), more20_solution(1001), more20(1001));
    layout.print_test(format!("{:?}", (50)), more20_solution(50), more20(50));
    layout.print_test(format!("{:?}", (55)), more20_solution(55), more20(55));
    layout.print_test(format!("{:?}", (40)), more20_solution(40), more20(40));
    layout.print_test(format!("{:?}", (41)), more20_solution(41), more20(41));
    layout.print_test(format!("{:?}", (39)), more20_solution(39), more20(39));
    layout.print_test(format!("{:?}", (42)), more20_solution(42), more20(42));
    layout.print_footer();
}

pub fn old35_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("old35");
    layout.print_header();
    layout.print_test(format!("{:?}", (3)), old35_solution(3), old35(3));
    layout.print_test(format!("{:?}", (10)), old35_solution(10), old35(10));
    layout.print_test(format!("{:?}", (15)), old35_solution(15), old35(15));
    layout.print_test(format!("{:?}", (5)), old35_solution(5), old35(5));
    layout.print_test(format!("{:?}", (9)), old35_solution(9), old35(9));
    layout.print_test(format!("{:?}", (8)), old35_solution(8), old35(8));
    layout.print_test(format!("{:?}", (7)), old35_solution(7), old35(7));
    layout.print_test(format!("{:?}", (6)), old35_solution(6), old35(6));
    layout.print_test(format!("{:?}", (17)), old35_solution(17), old35(17));
    layout.print_test(format!("{:?}", (18)), old35_solution(18), old35(18));
    layout.print_test(format!("{:?}", (29)), old35_solution(29), old35(29));
    layout.print_test(format!("{:?}", (20)), old35_solution(20), old35(20));
    layout.print_test(format!("{:?}", (21)), old35_solution(21), old35(21));
    layout.print_test(format!("{:?}", (22)), old35_solution(22), old35(22));
    layout.print_test(format!("{:?}", (45)), old35_solution(45), old35(45));
    layout.print_test(format!("{:?}", (99)), old35_solution(99), old35(99));
    layout.print_footer();
}

pub fn less20_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("less20");
    layout.print_header();
    layout.print_test(format!("{:?}", (18)), less20_solution(18), less20(18));
    layout.print_test(format!("{:?}", (19)), less20_solution(19), less20(19));
    layout.print_test(format!("{:?}", (20)), less20_solution(20), less20(20));
    layout.print_test(format!("{:?}", (8)), less20_solution(8), less20(8));
    layout.print_test(format!("{:?}", (17)), less20_solution(17), less20(17));
    layout.print_test(format!("{:?}", (23)), less20_solution(23), less20(23));
    layout.print_test(format!("{:?}", (25)), less20_solution(25), less20(25));
    layout.print_test(format!("{:?}", (30)), less20_solution(30), less20(30));
    layout.print_test(format!("{:?}", (31)), less20_solution(31), less20(31));
    layout.print_test(format!("{:?}", (58)), less20_solution(58), less20(58));
    layout.print_test(format!("{:?}", (59)), less20_solution(59), less20(59));
    layout.print_test(format!("{:?}", (60)), less20_solution(60), less20(60));
    layout.print_test(format!("{:?}", (61)), less20_solution(61), less20(61));
    layout.print_test(format!("{:?}", (62)), less20_solution(62), less20(62));
    layout.print_test(format!("{:?}", (1017)), less20_solution(1017), less20(1017));
    layout.print_test(format!("{:?}", (1018)), less20_solution(1018), less20(1018));
    layout.print_test(format!("{:?}", (1019)), less20_solution(1019), less20(1019));
    layout.print_test(format!("{:?}", (1020)), less20_solution(1020), less20(1020));
    layout.print_test(format!("{:?}", (1021)), less20_solution(1021), less20(1021));
    layout.print_test(format!("{:?}", (1022)), less20_solution(1022), less20(1022));
    layout.print_test(format!("{:?}", (1023)), less20_solution(1023), less20(1023));
    layout.print_test(format!("{:?}", (37)), less20_solution(37), less20(37));
    layout.print_footer();
}

pub fn near_ten_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("near_ten");
    layout.print_header();
    layout.print_test(format!("{:?}", (12)), near_ten_solution(12), near_ten(12));
    layout.print_test(format!("{:?}", (17)), near_ten_solution(17), near_ten(17));
    layout.print_test(format!("{:?}", (19)), near_ten_solution(19), near_ten(19));
    layout.print_test(format!("{:?}", (31)), near_ten_solution(31), near_ten(31));
    layout.print_test(format!("{:?}", (6)), near_ten_solution(6), near_ten(6));
    layout.print_test(format!("{:?}", (10)), near_ten_solution(10), near_ten(10));
    layout.print_test(format!("{:?}", (11)), near_ten_solution(11), near_ten(11));
    layout.print_test(format!("{:?}", (21)), near_ten_solution(21), near_ten(21));
    layout.print_test(format!("{:?}", (22)), near_ten_solution(22), near_ten(22));
    layout.print_test(format!("{:?}", (23)), near_ten_solution(23), near_ten(23));
    layout.print_test(format!("{:?}", (54)), near_ten_solution(54), near_ten(54));
    layout.print_test(format!("{:?}", (155)), near_ten_solution(155), near_ten(155));
    layout.print_test(format!("{:?}", (158)), near_ten_solution(158), near_ten(158));
    layout.print_test(format!("{:?}", (3)), near_ten_solution(3), near_ten(3));
    layout.print_test(format!("{:?}", (1)), near_ten_solution(1), near_ten(1));
    layout.print_footer();
}

pub fn teen_sum_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("teen_sum");
    layout.print_header();
    layout.print_test(format!("{:?}", (3, 4)), teen_sum_solution(3, 4), teen_sum(3, 4));
    layout.print_test(format!("{:?}", (10, 13)), teen_sum_solution(10, 13), teen_sum(10, 13));
    layout.print_test(format!("{:?}", (13, 2)), teen_sum_solution(13, 2), teen_sum(13, 2));
    layout.print_test(format!("{:?}", (3, 19)), teen_sum_solution(3, 19), teen_sum(3, 19));
    layout.print_test(format!("{:?}", (13, 13)), teen_sum_solution(13, 13), teen_sum(13, 13));
    layout.print_test(format!("{:?}", (10, 10)), teen_sum_solution(10, 10), teen_sum(10, 10));
    layout.print_test(format!("{:?}", (6, 14)), teen_sum_solution(6, 14), teen_sum(6, 14));
    layout.print_test(format!("{:?}", (15, 2)), teen_sum_solution(15, 2), teen_sum(15, 2));
    layout.print_test(format!("{:?}", (19, 19)), teen_sum_solution(19, 19), teen_sum(19, 19));
    layout.print_test(format!("{:?}", (19, 20)), teen_sum_solution(19, 20), teen_sum(19, 20));
    layout.print_test(format!("{:?}", (2, 18)), teen_sum_solution(2, 18), teen_sum(2, 18));
    layout.print_test(format!("{:?}", (12, 4)), teen_sum_solution(12, 4), teen_sum(12, 4));
    layout.print_test(format!("{:?}", (2, 20)), teen_sum_solution(2, 20), teen_sum(2, 20));
    layout.print_test(format!("{:?}", (2, 17)), teen_sum_solution(2, 17), teen_sum(2, 17));
    layout.print_test(format!("{:?}", (2, 16)), teen_sum_solution(2, 16), teen_sum(2, 16));
    layout.print_test(format!("{:?}", (6, 7)), teen_sum_solution(6, 7), teen_sum(6, 7));
    layout.print_footer();
}

pub fn answer_cell_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("answer_cell");
    layout.print_header();
    layout.print_test(format!("{:?}", (false, false, false)), answer_cell_solution(false, false, false), answer_cell(false, false, false));
    layout.print_test(format!("{:?}", (false, false, true)), answer_cell_solution(false, false, true), answer_cell(false, false, true));
    layout.print_test(format!("{:?}", (true, false, false)), answer_cell_solution(true, false, false), answer_cell(true, false, false));
    layout.print_test(format!("{:?}", (true, true, false)), answer_cell_solution(true, true, false), answer_cell(true, true, false));
    layout.print_test(format!("{:?}", (false, true, false)), answer_cell_solution(false, true, false), answer_cell(false, true, false));
    layout.print_test(format!("{:?}", (true, true, true)), answer_cell_solution(true, true, true), answer_cell(true, true, true));
    layout.print_footer();
}

pub fn tea_party_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("tea_party");
    layout.print_header();
    layout.print_test(format!("{:?}", (6, 8)), tea_party_solution(6, 8), tea_party(6, 8));
    layout.print_test(format!("{:?}", (3, 8)), tea_party_solution(3, 8), tea_party(3, 8));
    layout.print_test(format!("{:?}", (20, 6)), tea_party_solution(20, 6), tea_party(20, 6));
    layout.print_test(format!("{:?}", (12, 6)), tea_party_solution(12, 6), tea_party(12, 6));
    layout.print_test(format!("{:?}", (11, 6)), tea_party_solution(11, 6), tea_party(11, 6));
    layout.print_test(format!("{:?}", (11, 4)), tea_party_solution(11, 4), tea_party(11, 4));
    layout.print_test(format!("{:?}", (4, 5)), tea_party_solution(4, 5), tea_party(4, 5));
    layout.print_test(format!("{:?}", (5, 5)), tea_party_solution(5, 5), tea_party(5, 5));
    layout.print_test(format!("{:?}", (6, 6)), tea_party_solution(6, 6), tea_party(6, 6));
    layout.print_test(format!("{:?}", (5, 10)), tea_party_solution(5, 10), tea_party(5, 10));
    layout.print_test(format!("{:?}", (5, 9)), tea_party_solution(5, 9), tea_party(5, 9));
    layout.print_test(format!("{:?}", (10, 4)), tea_party_solution(10, 4), tea_party(10, 4));
    layout.print_test(format!("{:?}", (10, 20)), tea_party_solution(10, 20), tea_party(10, 20));
    layout.print_footer();
}

pub fn fizz_string_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("fizz_string");
    layout.print_header();
    layout.print_test(format!("{:?}", ("fig")), fizz_string_solution("fig"), fizz_string("fig"));
    layout.print_test(format!("{:?}", ("dib")), fizz_string_solution("dib"), fizz_string("dib"));
    layout.print_test(format!("{:?}", ("fib")), fizz_string_solution("fib"), fizz_string("fib"));
    layout.print_test(format!("{:?}", ("abc")), fizz_string_solution("abc"), fizz_string("abc"));
    layout.print_test(format!("{:?}", ("fooo")), fizz_string_solution("fooo"), fizz_string("fooo"));
    layout.print_test(format!("{:?}", ("booo")), fizz_string_solution("booo"), fizz_string("booo"));
    layout.print_test(format!("{:?}", ("ooob")), fizz_string_solution("ooob"), fizz_string("ooob"));
    layout.print_test(format!("{:?}", ("fooob")), fizz_string_solution("fooob"), fizz_string("fooob"));
    layout.print_test(format!("{:?}", ("f")), fizz_string_solution("f"), fizz_string("f"));
    layout.print_test(format!("{:?}", ("b")), fizz_string_solution("b"), fizz_string("b"));
    layout.print_test(format!("{:?}", ("abcb")), fizz_string_solution("abcb"), fizz_string("abcb"));
    layout.print_test(format!("{:?}", ("Hello")), fizz_string_solution("Hello"), fizz_string("Hello"));
    layout.print_test(format!("{:?}", ("Hellob")), fizz_string_solution("Hellob"), fizz_string("Hellob"));
    layout.print_test(format!("{:?}", ("af")), fizz_string_solution("af"), fizz_string("af"));
    layout.print_test(format!("{:?}", ("bf")), fizz_string_solution("bf"), fizz_string("bf"));
    layout.print_test(format!("{:?}", ("fb")), fizz_string_solution("fb"), fizz_string("fb"));
    layout.print_footer();
}

pub fn fizz_string2_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("fizz_string2");
    layout.print_header();
    layout.print_test(format!("{:?}", (1)), fizz_string2_solution(1), fizz_string2(1));
    layout.print_test(format!("{:?}", (2)), fizz_string2_solution(2), fizz_string2(2));
    layout.print_test(format!("{:?}", (3)), fizz_string2_solution(3), fizz_string2(3));
    layout.print_test(format!("{:?}", (4)), fizz_string2_solution(4), fizz_string2(4));
    layout.print_test(format!("{:?}", (5)), fizz_string2_solution(5), fizz_string2(5));
    layout.print_test(format!("{:?}", (6)), fizz_string2_solution(6), fizz_string2(6));
    layout.print_test(format!("{:?}", (7)), fizz_string2_solution(7), fizz_string2(7));
    layout.print_test(format!("{:?}", (8)), fizz_string2_solution(8), fizz_string2(8));
    layout.print_test(format!("{:?}", (9)), fizz_string2_solution(9), fizz_string2(9));
    layout.print_test(format!("{:?}", (15)), fizz_string2_solution(15), fizz_string2(15));
    layout.print_test(format!("{:?}", (16)), fizz_string2_solution(16), fizz_string2(16));
    layout.print_test(format!("{:?}", (18)), fizz_string2_solution(18), fizz_string2(18));
    layout.print_test(format!("{:?}", (19)), fizz_string2_solution(19), fizz_string2(19));
    layout.print_test(format!("{:?}", (21)), fizz_string2_solution(21), fizz_string2(21));
    layout.print_test(format!("{:?}", (44)), fizz_string2_solution(44), fizz_string2(44));
    layout.print_test(format!("{:?}", (45)), fizz_string2_solution(45), fizz_string2(45));
    layout.print_test(format!("{:?}", (100)), fizz_string2_solution(100), fizz_string2(100));
    layout.print_footer();
}

pub fn two_as_one_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("two_as_one");
    layout.print_header();
    layout.print_test(format!("{:?}", (1, 2, 3)), two_as_one_solution(1, 2, 3), two_as_one(1, 2, 3));
    layout.print_test(format!("{:?}", (3, 1, 2)), two_as_one_solution(3, 1, 2), two_as_one(3, 1, 2));
    layout.print_test(format!("{:?}", (3, 2, 2)), two_as_one_solution(3, 2, 2), two_as_one(3, 2, 2));
    layout.print_test(format!("{:?}", (2, 3, 1)), two_as_one_solution(2, 3, 1), two_as_one(2, 3, 1));
    layout.print_test(format!("{:?}", (5, 3, -2)), two_as_one_solution(5, 3, -2), two_as_one(5, 3, -2));
    layout.print_test(format!("{:?}", (5, 3, -3)), two_as_one_solution(5, 3, -3), two_as_one(5, 3, -3));
    layout.print_test(format!("{:?}", (2, 5, 3)), two_as_one_solution(2, 5, 3), two_as_one(2, 5, 3));
    layout.print_test(format!("{:?}", (9, 5, 5)), two_as_one_solution(9, 5, 5), two_as_one(9, 5, 5));
    layout.print_test(format!("{:?}", (9, 4, 5)), two_as_one_solution(9, 4, 5), two_as_one(9, 4, 5));
    layout.print_test(format!("{:?}", (5, 4, 9)), two_as_one_solution(5, 4, 9), two_as_one(5, 4, 9));
    layout.print_test(format!("{:?}", (3, 3, 0)), two_as_one_solution(3, 3, 0), two_as_one(3, 3, 0));
    layout.print_test(format!("{:?}", (3, 3, 2)), two_as_one_solution(3, 3, 2), two_as_one(3, 3, 2));
    layout.print_footer();
}

pub fn in_order_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("in_order");
    layout.print_header();
    layout.print_test(format!("{:?}", (1, 2, 4, false)), in_order_solution(1, 2, 4, false), in_order(1, 2, 4, false));
    layout.print_test(format!("{:?}", (1, 2, 1, false)), in_order_solution(1, 2, 1, false), in_order(1, 2, 1, false));
    layout.print_test(format!("{:?}", (1, 1, 2, true)), in_order_solution(1, 1, 2, true), in_order(1, 1, 2, true));
    layout.print_test(format!("{:?}", (3, 2, 4, false)), in_order_solution(3, 2, 4, false), in_order(3, 2, 4, false));
    layout.print_test(format!("{:?}", (2, 3, 4, false)), in_order_solution(2, 3, 4, false), in_order(2, 3, 4, false));
    layout.print_test(format!("{:?}", (3, 2, 4, true)), in_order_solution(3, 2, 4, true), in_order(3, 2, 4, true));
    layout.print_test(format!("{:?}", (4, 2, 2, true)), in_order_solution(4, 2, 2, true), in_order(4, 2, 2, true));
    layout.print_test(format!("{:?}", (4, 5, 2, true)), in_order_solution(4, 5, 2, true), in_order(4, 5, 2, true));
    layout.print_test(format!("{:?}", (2, 4, 6, true)), in_order_solution(2, 4, 6, true), in_order(2, 4, 6, true));
    layout.print_test(format!("{:?}", (7, 9, 10, false)), in_order_solution(7, 9, 10, false), in_order(7, 9, 10, false));
    layout.print_test(format!("{:?}", (7, 5, 6, true)), in_order_solution(7, 5, 6, true), in_order(7, 5, 6, true));
    layout.print_test(format!("{:?}", (7, 5, 4, true)), in_order_solution(7, 5, 4, true), in_order(7, 5, 4, true));
    layout.print_footer();
}

pub fn in_order_equal_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("in_order_equal");
    layout.print_header();
    layout.print_test(format!("{:?}", (2, 5, 11, false)), in_order_equal_solution(2, 5, 11, false), in_order_equal(2, 5, 11, false));
    layout.print_test(format!("{:?}", (5, 7, 6, false)), in_order_equal_solution(5, 7, 6, false), in_order_equal(5, 7, 6, false));
    layout.print_test(format!("{:?}", (5, 5, 7, true)), in_order_equal_solution(5, 5, 7, true), in_order_equal(5, 5, 7, true));
    layout.print_test(format!("{:?}", (5, 5, 7, false)), in_order_equal_solution(5, 5, 7, false), in_order_equal(5, 5, 7, false));
    layout.print_test(format!("{:?}", (2, 5, 4, false)), in_order_equal_solution(2, 5, 4, false), in_order_equal(2, 5, 4, false));
    layout.print_test(format!("{:?}", (3, 4, 3, false)), in_order_equal_solution(3, 4, 3, false), in_order_equal(3, 4, 3, false));
    layout.print_test(format!("{:?}", (3, 4, 4, false)), in_order_equal_solution(3, 4, 4, false), in_order_equal(3, 4, 4, false));
    layout.print_test(format!("{:?}", (3, 4, 3, true)), in_order_equal_solution(3, 4, 3, true), in_order_equal(3, 4, 3, true));
    layout.print_test(format!("{:?}", (3, 4, 4, true)), in_order_equal_solution(3, 4, 4, true), in_order_equal(3, 4, 4, true));
    layout.print_test(format!("{:?}", (1, 5, 5, true)), in_order_equal_solution(1, 5, 5, true), in_order_equal(1, 5, 5, true));
    layout.print_test(format!("{:?}", (5, 5, 5, true)), in_order_equal_solution(5, 5, 5, true), in_order_equal(5, 5, 5, true));
    layout.print_test(format!("{:?}", (2, 2, 1, true)), in_order_equal_solution(2, 2, 1, true), in_order_equal(2, 2, 1, true));
    layout.print_test(format!("{:?}", (9, 2, 2, true)), in_order_equal_solution(9, 2, 2, true), in_order_equal(9, 2, 2, true));
    layout.print_test(format!("{:?}", (0, 1, 0, true)), in_order_equal_solution(0, 1, 0, true), in_order_equal(0, 1, 0, true));
    layout.print_footer();
}

pub fn last_digit_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("last_digit");
    layout.print_header();
    layout.print_test(format!("{:?}", (23, 19, 13)), last_digit_solution(23, 19, 13), last_digit(23, 19, 13));
    layout.print_test(format!("{:?}", (23, 19, 12)), last_digit_solution(23, 19, 12), last_digit(23, 19, 12));
    layout.print_test(format!("{:?}", (23, 19, 3)), last_digit_solution(23, 19, 3), last_digit(23, 19, 3));
    layout.print_test(format!("{:?}", (23, 19, 39)), last_digit_solution(23, 19, 39), last_digit(23, 19, 39));
    layout.print_test(format!("{:?}", (1, 2, 3)), last_digit_solution(1, 2, 3), last_digit(1, 2, 3));
    layout.print_test(format!("{:?}", (1, 1, 2)), last_digit_solution(1, 1, 2), last_digit(1, 1, 2));
    layout.print_test(format!("{:?}", (1, 2, 2)), last_digit_solution(1, 2, 2), last_digit(1, 2, 2));
    layout.print_test(format!("{:?}", (14, 25, 43)), last_digit_solution(14, 25, 43), last_digit(14, 25, 43));
    layout.print_test(format!("{:?}", (14, 25, 45)), last_digit_solution(14, 25, 45), last_digit(14, 25, 45));
    layout.print_test(format!("{:?}", (248, 106, 1002)), last_digit_solution(248, 106, 1002), last_digit(248, 106, 1002));
    layout.print_test(format!("{:?}", (248, 106, 1008)), last_digit_solution(248, 106, 1008), last_digit(248, 106, 1008));
    layout.print_test(format!("{:?}", (10, 11, 20)), last_digit_solution(10, 11, 20), last_digit(10, 11, 20));
    layout.print_test(format!("{:?}", (0, 11, 0)), last_digit_solution(0, 11, 0), last_digit(0, 11, 0));
    layout.print_footer();
}

pub fn less_by10_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("less_by10");
    layout.print_header();
    layout.print_test(format!("{:?}", (1, 7, 11)), less_by10_solution(1, 7, 11), less_by10(1, 7, 11));
    layout.print_test(format!("{:?}", (1, 7, 10)), less_by10_solution(1, 7, 10), less_by10(1, 7, 10));
    layout.print_test(format!("{:?}", (11, 1, 7)), less_by10_solution(11, 1, 7), less_by10(11, 1, 7));
    layout.print_test(format!("{:?}", (10, 7, 1)), less_by10_solution(10, 7, 1), less_by10(10, 7, 1));
    layout.print_test(format!("{:?}", (-10, 2, 2)), less_by10_solution(-10, 2, 2), less_by10(-10, 2, 2));
    layout.print_test(format!("{:?}", (2, 11, 11)), less_by10_solution(2, 11, 11), less_by10(2, 11, 11));
    layout.print_test(format!("{:?}", (3, 3, 30)), less_by10_solution(3, 3, 30), less_by10(3, 3, 30));
    layout.print_test(format!("{:?}", (3, 3, 3)), less_by10_solution(3, 3, 3), less_by10(3, 3, 3));
    layout.print_test(format!("{:?}", (10, 1, 11)), less_by10_solution(10, 1, 11), less_by10(10, 1, 11));
    layout.print_test(format!("{:?}", (10, 11, 1)), less_by10_solution(10, 11, 1), less_by10(10, 11, 1));
    layout.print_test(format!("{:?}", (10, 11, 2)), less_by10_solution(10, 11, 2), less_by10(10, 11, 2));
    layout.print_test(format!("{:?}", (3, 30, 3)), less_by10_solution(3, 30, 3), less_by10(3, 30, 3));
    layout.print_test(format!("{:?}", (2, 2, -8)), less_by10_solution(2, 2, -8), less_by10(2, 2, -8));
    layout.print_test(format!("{:?}", (2, 8, 12)), less_by10_solution(2, 8, 12), less_by10(2, 8, 12));
    layout.print_footer();
}

pub fn without_doubles_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("without_doubles");
    layout.print_header();
    layout.print_test(format!("{:?}", (2, 3, true)), without_doubles_solution(2, 3, true), without_doubles(2, 3, true));
    layout.print_test(format!("{:?}", (3, 3, true)), without_doubles_solution(3, 3, true), without_doubles(3, 3, true));
    layout.print_test(format!("{:?}", (3, 3, false)), without_doubles_solution(3, 3, false), without_doubles(3, 3, false));
    layout.print_test(format!("{:?}", (2, 3, false)), without_doubles_solution(2, 3, false), without_doubles(2, 3, false));
    layout.print_test(format!("{:?}", (5, 4, true)), without_doubles_solution(5, 4, true), without_doubles(5, 4, true));
    layout.print_test(format!("{:?}", (5, 4, false)), without_doubles_solution(5, 4, false), without_doubles(5, 4, false));
    layout.print_test(format!("{:?}", (5, 5, true)), without_doubles_solution(5, 5, true), without_doubles(5, 5, true));
    layout.print_test(format!("{:?}", (5, 5, false)), without_doubles_solution(5, 5, false), without_doubles(5, 5, false));
    layout.print_test(format!("{:?}", (6, 6, true)), without_doubles_solution(6, 6, true), without_doubles(6, 6, true));
    layout.print_test(format!("{:?}", (6, 6, false)), without_doubles_solution(6, 6, false), without_doubles(6, 6, false));
    layout.print_test(format!("{:?}", (1, 6, true)), without_doubles_solution(1, 6, true), without_doubles(1, 6, true));
    layout.print_test(format!("{:?}", (6, 1, false)), without_doubles_solution(6, 1, false), without_doubles(6, 1, false));
    layout.print_footer();
}

pub fn max_mod5_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("max_mod5");
    layout.print_header();
    layout.print_test(format!("{:?}", (2, 3)), max_mod5_solution(2, 3), max_mod5(2, 3));
    layout.print_test(format!("{:?}", (6, 2)), max_mod5_solution(6, 2), max_mod5(6, 2));
    layout.print_test(format!("{:?}", (3, 2)), max_mod5_solution(3, 2), max_mod5(3, 2));
    layout.print_test(format!("{:?}", (8, 12)), max_mod5_solution(8, 12), max_mod5(8, 12));
    layout.print_test(format!("{:?}", (7, 12)), max_mod5_solution(7, 12), max_mod5(7, 12));
    layout.print_test(format!("{:?}", (11, 6)), max_mod5_solution(11, 6), max_mod5(11, 6));
    layout.print_test(format!("{:?}", (2, 7)), max_mod5_solution(2, 7), max_mod5(2, 7));
    layout.print_test(format!("{:?}", (7, 7)), max_mod5_solution(7, 7), max_mod5(7, 7));
    layout.print_test(format!("{:?}", (9, 1)), max_mod5_solution(9, 1), max_mod5(9, 1));
    layout.print_test(format!("{:?}", (9, 14)), max_mod5_solution(9, 14), max_mod5(9, 14));
    layout.print_test(format!("{:?}", (1, 2)), max_mod5_solution(1, 2), max_mod5(1, 2));
    layout.print_footer();
}

pub fn red_ticket_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("red_ticket");
    layout.print_header();
    layout.print_test(format!("{:?}", (2, 2, 2)), red_ticket_solution(2, 2, 2), red_ticket(2, 2, 2));
    layout.print_test(format!("{:?}", (2, 2, 1)), red_ticket_solution(2, 2, 1), red_ticket(2, 2, 1));
    layout.print_test(format!("{:?}", (0, 0, 0)), red_ticket_solution(0, 0, 0), red_ticket(0, 0, 0));
    layout.print_test(format!("{:?}", (2, 0, 0)), red_ticket_solution(2, 0, 0), red_ticket(2, 0, 0));
    layout.print_test(format!("{:?}", (1, 1, 1)), red_ticket_solution(1, 1, 1), red_ticket(1, 1, 1));
    layout.print_test(format!("{:?}", (1, 2, 1)), red_ticket_solution(1, 2, 1), red_ticket(1, 2, 1));
    layout.print_test(format!("{:?}", (1, 2, 0)), red_ticket_solution(1, 2, 0), red_ticket(1, 2, 0));
    layout.print_test(format!("{:?}", (0, 2, 2)), red_ticket_solution(0, 2, 2), red_ticket(0, 2, 2));
    layout.print_test(format!("{:?}", (1, 2, 2)), red_ticket_solution(1, 2, 2), red_ticket(1, 2, 2));
    layout.print_test(format!("{:?}", (0, 2, 0)), red_ticket_solution(0, 2, 0), red_ticket(0, 2, 0));
    layout.print_test(format!("{:?}", (1, 1, 2)), red_ticket_solution(1, 1, 2), red_ticket(1, 1, 2));
    layout.print_footer();
}

pub fn green_ticket_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("green_ticket");
    layout.print_header();
    layout.print_test(format!("{:?}", (1, 2, 3)), green_ticket_solution(1, 2, 3), green_ticket(1, 2, 3));
    layout.print_test(format!("{:?}", (2, 2, 2)), green_ticket_solution(2, 2, 2), green_ticket(2, 2, 2));
    layout.print_test(format!("{:?}", (1, 1, 2)), green_ticket_solution(1, 1, 2), green_ticket(1, 1, 2));
    layout.print_test(format!("{:?}", (2, 1, 1)), green_ticket_solution(2, 1, 1), green_ticket(2, 1, 1));
    layout.print_test(format!("{:?}", (1, 2, 1)), green_ticket_solution(1, 2, 1), green_ticket(1, 2, 1));
    layout.print_test(format!("{:?}", (3, 2, 1)), green_ticket_solution(3, 2, 1), green_ticket(3, 2, 1));
    layout.print_test(format!("{:?}", (0, 0, 0)), green_ticket_solution(0, 0, 0), green_ticket(0, 0, 0));
    layout.print_test(format!("{:?}", (2, 0, 0)), green_ticket_solution(2, 0, 0), green_ticket(2, 0, 0));
    layout.print_test(format!("{:?}", (0, 9, 10)), green_ticket_solution(0, 9, 10), green_ticket(0, 9, 10));
    layout.print_test(format!("{:?}", (0, 10, 0)), green_ticket_solution(0, 10, 0), green_ticket(0, 10, 0));
    layout.print_test(format!("{:?}", (9, 9, 9)), green_ticket_solution(9, 9, 9), green_ticket(9, 9, 9));
    layout.print_test(format!("{:?}", (9, 0, 9)), green_ticket_solution(9, 0, 9), green_ticket(9, 0, 9));
    layout.print_footer();
}

pub fn blue_ticket_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("blue_ticket");
    layout.print_header();
    layout.print_test(format!("{:?}", (9, 1, 0)), blue_ticket_solution(9, 1, 0), blue_ticket(9, 1, 0));
    layout.print_test(format!("{:?}", (9, 2, 0)), blue_ticket_solution(9, 2, 0), blue_ticket(9, 2, 0));
    layout.print_test(format!("{:?}", (6, 1, 4)), blue_ticket_solution(6, 1, 4), blue_ticket(6, 1, 4));
    layout.print_test(format!("{:?}", (6, 1, 5)), blue_ticket_solution(6, 1, 5), blue_ticket(6, 1, 5));
    layout.print_test(format!("{:?}", (10, 0, 0)), blue_ticket_solution(10, 0, 0), blue_ticket(10, 0, 0));
    layout.print_test(format!("{:?}", (15, 0, 5)), blue_ticket_solution(15, 0, 5), blue_ticket(15, 0, 5));
    layout.print_test(format!("{:?}", (5, 15, 5)), blue_ticket_solution(5, 15, 5), blue_ticket(5, 15, 5));
    layout.print_test(format!("{:?}", (4, 11, 1)), blue_ticket_solution(4, 11, 1), blue_ticket(4, 11, 1));
    layout.print_test(format!("{:?}", (13, 2, 3)), blue_ticket_solution(13, 2, 3), blue_ticket(13, 2, 3));
    layout.print_test(format!("{:?}", (8, 4, 3)), blue_ticket_solution(8, 4, 3), blue_ticket(8, 4, 3));
    layout.print_test(format!("{:?}", (8, 4, 2)), blue_ticket_solution(8, 4, 2), blue_ticket(8, 4, 2));
    layout.print_test(format!("{:?}", (8, 4, 1)), blue_ticket_solution(8, 4, 1), blue_ticket(8, 4, 1));
    layout.print_footer();
}

pub fn share_digit_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("share_digit");
    layout.print_header();
    layout.print_test(format!("{:?}", (12, 23)), share_digit_solution(12, 23), share_digit(12, 23));
    layout.print_test(format!("{:?}", (12, 43)), share_digit_solution(12, 43), share_digit(12, 43));
    layout.print_test(format!("{:?}", (12, 44)), share_digit_solution(12, 44), share_digit(12, 44));
    layout.print_test(format!("{:?}", (23, 12)), share_digit_solution(23, 12), share_digit(23, 12));
    layout.print_test(format!("{:?}", (23, 39)), share_digit_solution(23, 39), share_digit(23, 39));
    layout.print_test(format!("{:?}", (23, 19)), share_digit_solution(23, 19), share_digit(23, 19));
    layout.print_test(format!("{:?}", (30, 90)), share_digit_solution(30, 90), share_digit(30, 90));
    layout.print_test(format!("{:?}", (30, 91)), share_digit_solution(30, 91), share_digit(30, 91));
    layout.print_test(format!("{:?}", (55, 55)), share_digit_solution(55, 55), share_digit(55, 55));
    layout.print_test(format!("{:?}", (55, 44)), share_digit_solution(55, 44), share_digit(55, 44));
    layout.print_footer();
}

pub fn sum_limit_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("sum_limit");
    layout.print_header();
    layout.print_test(format!("{:?}", (2, 3)), sum_limit_solution(2, 3), sum_limit(2, 3));
    layout.print_test(format!("{:?}", (8, 3)), sum_limit_solution(8, 3), sum_limit(8, 3));
    layout.print_test(format!("{:?}", (8, 1)), sum_limit_solution(8, 1), sum_limit(8, 1));
    layout.print_test(format!("{:?}", (11, 39)), sum_limit_solution(11, 39), sum_limit(11, 39));
    layout.print_test(format!("{:?}", (11, 99)), sum_limit_solution(11, 99), sum_limit(11, 99));
    layout.print_test(format!("{:?}", (0, 0)), sum_limit_solution(0, 0), sum_limit(0, 0));
    layout.print_test(format!("{:?}", (99, 0)), sum_limit_solution(99, 0), sum_limit(99, 0));
    layout.print_test(format!("{:?}", (99, 1)), sum_limit_solution(99, 1), sum_limit(99, 1));
    layout.print_test(format!("{:?}", (123, 1)), sum_limit_solution(123, 1), sum_limit(123, 1));
    layout.print_test(format!("{:?}", (1, 123)), sum_limit_solution(1, 123), sum_limit(1, 123));
    layout.print_test(format!("{:?}", (23, 60)), sum_limit_solution(23, 60), sum_limit(23, 60));
    layout.print_test(format!("{:?}", (23, 80)), sum_limit_solution(23, 80), sum_limit(23, 80));
    layout.print_test(format!("{:?}", (9000, 1)), sum_limit_solution(9000, 1), sum_limit(9000, 1));
    layout.print_test(format!("{:?}", (90000000, 1)), sum_limit_solution(90000000, 1), sum_limit(90000000, 1));
    layout.print_test(format!("{:?}", (9000, 1000)), sum_limit_solution(9000, 1000), sum_limit(9000, 1000));
    layout.print_footer();
}

pub fn make_bricks_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("make_bricks");
    layout.print_header();
    layout.print_test(format!("{:?}", (3, 1, 8)), make_bricks_solution(3, 1, 8), make_bricks(3, 1, 8));
    layout.print_test(format!("{:?}", (3, 1, 9)), make_bricks_solution(3, 1, 9), make_bricks(3, 1, 9));
    layout.print_test(format!("{:?}", (3, 2, 10)), make_bricks_solution(3, 2, 10), make_bricks(3, 2, 10));
    layout.print_test(format!("{:?}", (3, 2, 8)), make_bricks_solution(3, 2, 8), make_bricks(3, 2, 8));
    layout.print_test(format!("{:?}", (3, 2, 9)), make_bricks_solution(3, 2, 9), make_bricks(3, 2, 9));
    layout.print_test(format!("{:?}", (6, 1, 11)), make_bricks_solution(6, 1, 11), make_bricks(6, 1, 11));
    layout.print_test(format!("{:?}", (6, 0, 11)), make_bricks_solution(6, 0, 11), make_bricks(6, 0, 11));
    layout.print_test(format!("{:?}", (1, 4, 11)), make_bricks_solution(1, 4, 11), make_bricks(1, 4, 11));
    layout.print_test(format!("{:?}", (0, 3, 10)), make_bricks_solution(0, 3, 10), make_bricks(0, 3, 10));
    layout.print_test(format!("{:?}", (1, 4, 12)), make_bricks_solution(1, 4, 12), make_bricks(1, 4, 12));
    layout.print_test(format!("{:?}", (3, 1, 7)), make_bricks_solution(3, 1, 7), make_bricks(3, 1, 7));
    layout.print_test(format!("{:?}", (1, 1, 7)), make_bricks_solution(1, 1, 7), make_bricks(1, 1, 7));
    layout.print_test(format!("{:?}", (2, 1, 7)), make_bricks_solution(2, 1, 7), make_bricks(2, 1, 7));
    layout.print_test(format!("{:?}", (7, 1, 11)), make_bricks_solution(7, 1, 11), make_bricks(7, 1, 11));
    layout.print_test(format!("{:?}", (7, 1, 8)), make_bricks_solution(7, 1, 8), make_bricks(7, 1, 8));
    layout.print_test(format!("{:?}", (7, 1, 13)), make_bricks_solution(7, 1, 13), make_bricks(7, 1, 13));
    layout.print_test(format!("{:?}", (43, 1, 46)), make_bricks_solution(43, 1, 46), make_bricks(43, 1, 46));
    layout.print_test(format!("{:?}", (40, 1, 46)), make_bricks_solution(40, 1, 46), make_bricks(40, 1, 46));
    layout.print_test(format!("{:?}", (40, 2, 47)), make_bricks_solution(40, 2, 47), make_bricks(40, 2, 47));
    layout.print_test(format!("{:?}", (40, 2, 50)), make_bricks_solution(40, 2, 50), make_bricks(40, 2, 50));
    layout.print_test(format!("{:?}", (40, 2, 52)), make_bricks_solution(40, 2, 52), make_bricks(40, 2, 52));
    layout.print_test(format!("{:?}", (22, 2, 33)), make_bricks_solution(22, 2, 33), make_bricks(22, 2, 33));
    layout.print_test(format!("{:?}", (0, 2, 10)), make_bricks_solution(0, 2, 10), make_bricks(0, 2, 10));
    layout.print_test(format!("{:?}", (1000000, 1000, 1000100)), make_bricks_solution(1000000, 1000, 1000100), make_bricks(1000000, 1000, 1000100));
    layout.print_test(format!("{:?}", (2, 1000000, 100003)), make_bricks_solution(2, 1000000, 100003), make_bricks(2, 1000000, 100003));
    layout.print_test(format!("{:?}", (20, 0, 19)), make_bricks_solution(20, 0, 19), make_bricks(20, 0, 19));
    layout.print_test(format!("{:?}", (20, 0, 21)), make_bricks_solution(20, 0, 21), make_bricks(20, 0, 21));
    layout.print_test(format!("{:?}", (20, 4, 51)), make_bricks_solution(20, 4, 51), make_bricks(20, 4, 51));
    layout.print_test(format!("{:?}", (20, 4, 39)), make_bricks_solution(20, 4, 39), make_bricks(20, 4, 39));
    layout.print_footer();
}

pub fn lone_sum_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("lone_sum");
    layout.print_header();
    layout.print_test(format!("{:?}", (1, 2, 3)), lone_sum_solution(1, 2, 3), lone_sum(1, 2, 3));
    layout.print_test(format!("{:?}", (3, 2, 3)), lone_sum_solution(3, 2, 3), lone_sum(3, 2, 3));
    layout.print_test(format!("{:?}", (3, 3, 3)), lone_sum_solution(3, 3, 3), lone_sum(3, 3, 3));
    layout.print_test(format!("{:?}", (9, 2, 2)), lone_sum_solution(9, 2, 2), lone_sum(9, 2, 2));
    layout.print_test(format!("{:?}", (2, 2, 9)), lone_sum_solution(2, 2, 9), lone_sum(2, 2, 9));
    layout.print_test(format!("{:?}", (2, 9, 2)), lone_sum_solution(2, 9, 2), lone_sum(2, 9, 2));
    layout.print_test(format!("{:?}", (2, 9, 3)), lone_sum_solution(2, 9, 3), lone_sum(2, 9, 3));
    layout.print_test(format!("{:?}", (4, 2, 3)), lone_sum_solution(4, 2, 3), lone_sum(4, 2, 3));
    layout.print_test(format!("{:?}", (1, 3, 1)), lone_sum_solution(1, 3, 1), lone_sum(1, 3, 1));
    layout.print_footer();
}

pub fn lucky_sum_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("lucky_sum");
    layout.print_header();
    layout.print_test(format!("{:?}", (1, 2, 3)), lucky_sum_solution(1, 2, 3), lucky_sum(1, 2, 3));
    layout.print_test(format!("{:?}", (1, 2, 13)), lucky_sum_solution(1, 2, 13), lucky_sum(1, 2, 13));
    layout.print_test(format!("{:?}", (1, 13, 3)), lucky_sum_solution(1, 13, 3), lucky_sum(1, 13, 3));
    layout.print_test(format!("{:?}", (1, 13, 13)), lucky_sum_solution(1, 13, 13), lucky_sum(1, 13, 13));
    layout.print_test(format!("{:?}", (6, 5, 2)), lucky_sum_solution(6, 5, 2), lucky_sum(6, 5, 2));
    layout.print_test(format!("{:?}", (13, 2, 3)), lucky_sum_solution(13, 2, 3), lucky_sum(13, 2, 3));
    layout.print_test(format!("{:?}", (13, 2, 13)), lucky_sum_solution(13, 2, 13), lucky_sum(13, 2, 13));
    layout.print_test(format!("{:?}", (13, 13, 2)), lucky_sum_solution(13, 13, 2), lucky_sum(13, 13, 2));
    layout.print_test(format!("{:?}", (9, 4, 13)), lucky_sum_solution(9, 4, 13), lucky_sum(9, 4, 13));
    layout.print_test(format!("{:?}", (8, 13, 2)), lucky_sum_solution(8, 13, 2), lucky_sum(8, 13, 2));
    layout.print_test(format!("{:?}", (7, 2, 1)), lucky_sum_solution(7, 2, 1), lucky_sum(7, 2, 1));
    layout.print_test(format!("{:?}", (3, 3, 13)), lucky_sum_solution(3, 3, 13), lucky_sum(3, 3, 13));
    layout.print_footer();
}

pub fn no_teen_sum_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("no_teen_sum");
    layout.print_header();
    layout.print_test(format!("{:?}", (1, 2, 3)), no_teen_sum_solution(1, 2, 3), no_teen_sum(1, 2, 3));
    layout.print_test(format!("{:?}", (2, 13, 1)), no_teen_sum_solution(2, 13, 1), no_teen_sum(2, 13, 1));
    layout.print_test(format!("{:?}", (2, 1, 14)), no_teen_sum_solution(2, 1, 14), no_teen_sum(2, 1, 14));
    layout.print_test(format!("{:?}", (2, 1, 15)), no_teen_sum_solution(2, 1, 15), no_teen_sum(2, 1, 15));
    layout.print_test(format!("{:?}", (2, 1, 16)), no_teen_sum_solution(2, 1, 16), no_teen_sum(2, 1, 16));
    layout.print_test(format!("{:?}", (2, 1, 17)), no_teen_sum_solution(2, 1, 17), no_teen_sum(2, 1, 17));
    layout.print_test(format!("{:?}", (17, 1, 2)), no_teen_sum_solution(17, 1, 2), no_teen_sum(17, 1, 2));
    layout.print_test(format!("{:?}", (2, 15, 2)), no_teen_sum_solution(2, 15, 2), no_teen_sum(2, 15, 2));
    layout.print_test(format!("{:?}", (16, 17, 18)), no_teen_sum_solution(16, 17, 18), no_teen_sum(16, 17, 18));
    layout.print_test(format!("{:?}", (17, 18, 19)), no_teen_sum_solution(17, 18, 19), no_teen_sum(17, 18, 19));
    layout.print_test(format!("{:?}", (15, 16, 1)), no_teen_sum_solution(15, 16, 1), no_teen_sum(15, 16, 1));
    layout.print_test(format!("{:?}", (15, 15, 19)), no_teen_sum_solution(15, 15, 19), no_teen_sum(15, 15, 19));
    layout.print_test(format!("{:?}", (15, 19, 16)), no_teen_sum_solution(15, 19, 16), no_teen_sum(15, 19, 16));
    layout.print_test(format!("{:?}", (5, 17, 18)), no_teen_sum_solution(5, 17, 18), no_teen_sum(5, 17, 18));
    layout.print_test(format!("{:?}", (17, 18, 16)), no_teen_sum_solution(17, 18, 16), no_teen_sum(17, 18, 16));
    layout.print_test(format!("{:?}", (17, 19, 18)), no_teen_sum_solution(17, 19, 18), no_teen_sum(17, 19, 18));
    layout.print_footer();
}

pub fn round_sum_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("round_sum");
    layout.print_header();
    layout.print_test(format!("{:?}", (16, 17, 18)), round_sum_solution(16, 17, 18), round_sum(16, 17, 18));
    layout.print_test(format!("{:?}", (12, 13, 14)), round_sum_solution(12, 13, 14), round_sum(12, 13, 14));
    layout.print_test(format!("{:?}", (6, 4, 4)), round_sum_solution(6, 4, 4), round_sum(6, 4, 4));
    layout.print_test(format!("{:?}", (4, 6, 5)), round_sum_solution(4, 6, 5), round_sum(4, 6, 5));
    layout.print_test(format!("{:?}", (4, 4, 6)), round_sum_solution(4, 4, 6), round_sum(4, 4, 6));
    layout.print_test(format!("{:?}", (9, 4, 4)), round_sum_solution(9, 4, 4), round_sum(9, 4, 4));
    layout.print_test(format!("{:?}", (0, 0, 1)), round_sum_solution(0, 0, 1), round_sum(0, 0, 1));
    layout.print_test(format!("{:?}", (0, 9, 0)), round_sum_solution(0, 9, 0), round_sum(0, 9, 0));
    layout.print_test(format!("{:?}", (10, 10, 19)), round_sum_solution(10, 10, 19), round_sum(10, 10, 19));
    layout.print_test(format!("{:?}", (20, 30, 40)), round_sum_solution(20, 30, 40), round_sum(20, 30, 40));
    layout.print_test(format!("{:?}", (45, 21, 30)), round_sum_solution(45, 21, 30), round_sum(45, 21, 30));
    layout.print_test(format!("{:?}", (23, 11, 26)), round_sum_solution(23, 11, 26), round_sum(23, 11, 26));
    layout.print_test(format!("{:?}", (23, 24, 25)), round_sum_solution(23, 24, 25), round_sum(23, 24, 25));
    layout.print_test(format!("{:?}", (25, 24, 25)), round_sum_solution(25, 24, 25), round_sum(25, 24, 25));
    layout.print_test(format!("{:?}", (23, 24, 29)), round_sum_solution(23, 24, 29), round_sum(23, 24, 29));
    layout.print_test(format!("{:?}", (11, 24, 36)), round_sum_solution(11, 24, 36), round_sum(11, 24, 36));
    layout.print_test(format!("{:?}", (24, 36, 32)), round_sum_solution(24, 36, 32), round_sum(24, 36, 32));
    layout.print_test(format!("{:?}", (14, 12, 26)), round_sum_solution(14, 12, 26), round_sum(14, 12, 26));
    layout.print_test(format!("{:?}", (12, 10, 24)), round_sum_solution(12, 10, 24), round_sum(12, 10, 24));
    layout.print_footer();
}

pub fn close_far_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("close_far");
    layout.print_header();
    layout.print_test(format!("{:?}", (1, 2, 10)), close_far_solution(1, 2, 10), close_far(1, 2, 10));
    layout.print_test(format!("{:?}", (1, 2, 3)), close_far_solution(1, 2, 3), close_far(1, 2, 3));
    layout.print_test(format!("{:?}", (4, 1, 3)), close_far_solution(4, 1, 3), close_far(4, 1, 3));
    layout.print_test(format!("{:?}", (4, 5, 3)), close_far_solution(4, 5, 3), close_far(4, 5, 3));
    layout.print_test(format!("{:?}", (4, 3, 5)), close_far_solution(4, 3, 5), close_far(4, 3, 5));
    layout.print_test(format!("{:?}", (-1, 10, 0)), close_far_solution(-1, 10, 0), close_far(-1, 10, 0));
    layout.print_test(format!("{:?}", (0, -1, 10)), close_far_solution(0, -1, 10), close_far(0, -1, 10));
    layout.print_test(format!("{:?}", (10, 10, 8)), close_far_solution(10, 10, 8), close_far(10, 10, 8));
    layout.print_test(format!("{:?}", (10, 8, 9)), close_far_solution(10, 8, 9), close_far(10, 8, 9));
    layout.print_test(format!("{:?}", (8, 9, 10)), close_far_solution(8, 9, 10), close_far(8, 9, 10));
    layout.print_test(format!("{:?}", (8, 9, 7)), close_far_solution(8, 9, 7), close_far(8, 9, 7));
    layout.print_test(format!("{:?}", (8, 6, 9)), close_far_solution(8, 6, 9), close_far(8, 6, 9));
    layout.print_footer();
}

pub fn blackjack_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("blackjack");
    layout.print_header();
    layout.print_test(format!("{:?}", (19, 21)), blackjack_solution(19, 21), blackjack(19, 21));
    layout.print_test(format!("{:?}", (21, 19)), blackjack_solution(21, 19), blackjack(21, 19));
    layout.print_test(format!("{:?}", (19, 22)), blackjack_solution(19, 22), blackjack(19, 22));
    layout.print_test(format!("{:?}", (22, 19)), blackjack_solution(22, 19), blackjack(22, 19));
    layout.print_test(format!("{:?}", (22, 50)), blackjack_solution(22, 50), blackjack(22, 50));
    layout.print_test(format!("{:?}", (22, 22)), blackjack_solution(22, 22), blackjack(22, 22));
    layout.print_test(format!("{:?}", (33, 1)), blackjack_solution(33, 1), blackjack(33, 1));
    layout.print_test(format!("{:?}", (1, 2)), blackjack_solution(1, 2), blackjack(1, 2));
    layout.print_test(format!("{:?}", (34, 33)), blackjack_solution(34, 33), blackjack(34, 33));
    layout.print_test(format!("{:?}", (17, 19)), blackjack_solution(17, 19), blackjack(17, 19));
    layout.print_test(format!("{:?}", (18, 17)), blackjack_solution(18, 17), blackjack(18, 17));
    layout.print_test(format!("{:?}", (16, 23)), blackjack_solution(16, 23), blackjack(16, 23));
    layout.print_test(format!("{:?}", (3, 4)), blackjack_solution(3, 4), blackjack(3, 4));
    layout.print_test(format!("{:?}", (3, 2)), blackjack_solution(3, 2), blackjack(3, 2));
    layout.print_test(format!("{:?}", (21, 20)), blackjack_solution(21, 20), blackjack(21, 20));
    layout.print_footer();
}

pub fn evenly_spaced_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("evenly_spaced");
    layout.print_header();
    layout.print_test(format!("{:?}", (2, 4, 6)), evenly_spaced_solution(2, 4, 6), evenly_spaced(2, 4, 6));
    layout.print_test(format!("{:?}", (4, 6, 2)), evenly_spaced_solution(4, 6, 2), evenly_spaced(4, 6, 2));
    layout.print_test(format!("{:?}", (4, 6, 3)), evenly_spaced_solution(4, 6, 3), evenly_spaced(4, 6, 3));
    layout.print_test(format!("{:?}", (6, 2, 4)), evenly_spaced_solution(6, 2, 4), evenly_spaced(6, 2, 4));
    layout.print_test(format!("{:?}", (6, 2, 8)), evenly_spaced_solution(6, 2, 8), evenly_spaced(6, 2, 8));
    layout.print_test(format!("{:?}", (2, 2, 2)), evenly_spaced_solution(2, 2, 2), evenly_spaced(2, 2, 2));
    layout.print_test(format!("{:?}", (2, 2, 3)), evenly_spaced_solution(2, 2, 3), evenly_spaced(2, 2, 3));
    layout.print_test(format!("{:?}", (9, 10, 11)), evenly_spaced_solution(9, 10, 11), evenly_spaced(9, 10, 11));
    layout.print_test(format!("{:?}", (10, 9, 11)), evenly_spaced_solution(10, 9, 11), evenly_spaced(10, 9, 11));
    layout.print_test(format!("{:?}", (10, 9, 9)), evenly_spaced_solution(10, 9, 9), evenly_spaced(10, 9, 9));
    layout.print_test(format!("{:?}", (2, 4, 4)), evenly_spaced_solution(2, 4, 4), evenly_spaced(2, 4, 4));
    layout.print_test(format!("{:?}", (2, 2, 4)), evenly_spaced_solution(2, 2, 4), evenly_spaced(2, 2, 4));
    layout.print_test(format!("{:?}", (3, 6, 12)), evenly_spaced_solution(3, 6, 12), evenly_spaced(3, 6, 12));
    layout.print_test(format!("{:?}", (12, 3, 6)), evenly_spaced_solution(12, 3, 6), evenly_spaced(12, 3, 6));
    layout.print_footer();
}

pub fn make_chocolate_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("make_chocolate");
    layout.print_header();
    layout.print_test(format!("{:?}", (4, 1, 9)), make_chocolate_solution(4, 1, 9), make_chocolate(4, 1, 9));
    layout.print_test(format!("{:?}", (4, 1, 10)), make_chocolate_solution(4, 1, 10), make_chocolate(4, 1, 10));
    layout.print_test(format!("{:?}", (4, 1, 7)), make_chocolate_solution(4, 1, 7), make_chocolate(4, 1, 7));
    layout.print_test(format!("{:?}", (6, 2, 7)), make_chocolate_solution(6, 2, 7), make_chocolate(6, 2, 7));
    layout.print_test(format!("{:?}", (4, 1, 5)), make_chocolate_solution(4, 1, 5), make_chocolate(4, 1, 5));
    layout.print_test(format!("{:?}", (4, 1, 4)), make_chocolate_solution(4, 1, 4), make_chocolate(4, 1, 4));
    layout.print_test(format!("{:?}", (5, 4, 9)), make_chocolate_solution(5, 4, 9), make_chocolate(5, 4, 9));
    layout.print_test(format!("{:?}", (9, 3, 18)), make_chocolate_solution(9, 3, 18), make_chocolate(9, 3, 18));
    layout.print_test(format!("{:?}", (3, 1, 9)), make_chocolate_solution(3, 1, 9), make_chocolate(3, 1, 9));
    layout.print_test(format!("{:?}", (1, 2, 7)), make_chocolate_solution(1, 2, 7), make_chocolate(1, 2, 7));
    layout.print_test(format!("{:?}", (1, 2, 6)), make_chocolate_solution(1, 2, 6), make_chocolate(1, 2, 6));
    layout.print_test(format!("{:?}", (1, 2, 5)), make_chocolate_solution(1, 2, 5), make_chocolate(1, 2, 5));
    layout.print_test(format!("{:?}", (6, 1, 10)), make_chocolate_solution(6, 1, 10), make_chocolate(6, 1, 10));
    layout.print_test(format!("{:?}", (6, 1, 11)), make_chocolate_solution(6, 1, 11), make_chocolate(6, 1, 11));
    layout.print_test(format!("{:?}", (6, 1, 12)), make_chocolate_solution(6, 1, 12), make_chocolate(6, 1, 12));
    layout.print_test(format!("{:?}", (6, 1, 13)), make_chocolate_solution(6, 1, 13), make_chocolate(6, 1, 13));
    layout.print_test(format!("{:?}", (6, 2, 10)), make_chocolate_solution(6, 2, 10), make_chocolate(6, 2, 10));
    layout.print_test(format!("{:?}", (6, 2, 11)), make_chocolate_solution(6, 2, 11), make_chocolate(6, 2, 11));
    layout.print_test(format!("{:?}", (6, 2, 12)), make_chocolate_solution(6, 2, 12), make_chocolate(6, 2, 12));
    layout.print_test(format!("{:?}", (60, 100, 550)), make_chocolate_solution(60, 100, 550), make_chocolate(60, 100, 550));
    layout.print_test(format!("{:?}", (1000, 1000000, 5000006)), make_chocolate_solution(1000, 1000000, 5000006), make_chocolate(1000, 1000000, 5000006));
    layout.print_test(format!("{:?}", (7, 1, 12)), make_chocolate_solution(7, 1, 12), make_chocolate(7, 1, 12));
    layout.print_test(format!("{:?}", (7, 1, 13)), make_chocolate_solution(7, 1, 13), make_chocolate(7, 1, 13));
    layout.print_test(format!("{:?}", (7, 2, 13)), make_chocolate_solution(7, 2, 13), make_chocolate(7, 2, 13));
    layout.print_footer();
}
