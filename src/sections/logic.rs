pub mod implementations;
pub mod runners;
pub mod solutions;

#[cfg(test)]
mod test_cigar_party {
    use super::implementations::cigar_party;
    use super::solutions::cigar_party_solution;

    #[test]
    fn test_cigar_party_1() {
        assert_eq!(cigar_party(30, false), cigar_party_solution(30, false))
    }
    #[test]
    fn test_cigar_party_2() {
        assert_eq!(cigar_party(50, false), cigar_party_solution(50, false))
    }
    #[test]
    fn test_cigar_party_3() {
        assert_eq!(cigar_party(70, true), cigar_party_solution(70, true))
    }
    #[test]
    fn test_cigar_party_4() {
        assert_eq!(cigar_party(30, true), cigar_party_solution(30, true))
    }
    #[test]
    fn test_cigar_party_5() {
        assert_eq!(cigar_party(50, true), cigar_party_solution(50, true))
    }
    #[test]
    fn test_cigar_party_6() {
        assert_eq!(cigar_party(60, false), cigar_party_solution(60, false))
    }
    #[test]
    fn test_cigar_party_7() {
        assert_eq!(cigar_party(61, false), cigar_party_solution(61, false))
    }
    #[test]
    fn test_cigar_party_8() {
        assert_eq!(cigar_party(40, false), cigar_party_solution(40, false))
    }
    #[test]
    fn test_cigar_party_9() {
        assert_eq!(cigar_party(39, false), cigar_party_solution(39, false))
    }
    #[test]
    fn test_cigar_party_10() {
        assert_eq!(cigar_party(40, true), cigar_party_solution(40, true))
    }
    #[test]
    fn test_cigar_party_11() {
        assert_eq!(cigar_party(39, true), cigar_party_solution(39, true))
    }
}

#[cfg(test)]
mod test_date_fashion {
    use super::implementations::date_fashion;
    use super::solutions::date_fashion_solution;

    #[test]
    fn test_date_fashion_1() {
        assert_eq!(date_fashion(5, 10), date_fashion_solution(5, 10))
    }
    #[test]
    fn test_date_fashion_2() {
        assert_eq!(date_fashion(5, 2), date_fashion_solution(5, 2))
    }
    #[test]
    fn test_date_fashion_3() {
        assert_eq!(date_fashion(5, 5), date_fashion_solution(5, 5))
    }
    #[test]
    fn test_date_fashion_4() {
        assert_eq!(date_fashion(3, 3), date_fashion_solution(3, 3))
    }
    #[test]
    fn test_date_fashion_5() {
        assert_eq!(date_fashion(10, 2), date_fashion_solution(10, 2))
    }
    #[test]
    fn test_date_fashion_6() {
        assert_eq!(date_fashion(2, 9), date_fashion_solution(2, 9))
    }
    #[test]
    fn test_date_fashion_7() {
        assert_eq!(date_fashion(9, 9), date_fashion_solution(9, 9))
    }
    #[test]
    fn test_date_fashion_8() {
        assert_eq!(date_fashion(10, 5), date_fashion_solution(10, 5))
    }
    #[test]
    fn test_date_fashion_9() {
        assert_eq!(date_fashion(2, 2), date_fashion_solution(2, 2))
    }
    #[test]
    fn test_date_fashion_10() {
        assert_eq!(date_fashion(3, 7), date_fashion_solution(3, 7))
    }
    #[test]
    fn test_date_fashion_11() {
        assert_eq!(date_fashion(2, 7), date_fashion_solution(2, 7))
    }
    #[test]
    fn test_date_fashion_12() {
        assert_eq!(date_fashion(6, 2), date_fashion_solution(6, 2))
    }
}

#[cfg(test)]
mod test_squirrel_play {
    use super::implementations::squirrel_play;
    use super::solutions::squirrel_play_solution;

    #[test]
    fn test_squirrel_play_1() {
        assert_eq!(squirrel_play(70, false), squirrel_play_solution(70, false))
    }
    #[test]
    fn test_squirrel_play_2() {
        assert_eq!(squirrel_play(95, false), squirrel_play_solution(95, false))
    }
    #[test]
    fn test_squirrel_play_3() {
        assert_eq!(squirrel_play(95, true), squirrel_play_solution(95, true))
    }
    #[test]
    fn test_squirrel_play_4() {
        assert_eq!(squirrel_play(90, false), squirrel_play_solution(90, false))
    }
    #[test]
    fn test_squirrel_play_5() {
        assert_eq!(squirrel_play(90, true), squirrel_play_solution(90, true))
    }
    #[test]
    fn test_squirrel_play_6() {
        assert_eq!(squirrel_play(50, false), squirrel_play_solution(50, false))
    }
    #[test]
    fn test_squirrel_play_7() {
        assert_eq!(squirrel_play(50, true), squirrel_play_solution(50, true))
    }
    #[test]
    fn test_squirrel_play_8() {
        assert_eq!(squirrel_play(100, false), squirrel_play_solution(100, false))
    }
    #[test]
    fn test_squirrel_play_9() {
        assert_eq!(squirrel_play(100, true), squirrel_play_solution(100, true))
    }
    #[test]
    fn test_squirrel_play_10() {
        assert_eq!(squirrel_play(105, true), squirrel_play_solution(105, true))
    }
    #[test]
    fn test_squirrel_play_11() {
        assert_eq!(squirrel_play(59, false), squirrel_play_solution(59, false))
    }
    #[test]
    fn test_squirrel_play_12() {
        assert_eq!(squirrel_play(59, true), squirrel_play_solution(59, true))
    }
    #[test]
    fn test_squirrel_play_13() {
        assert_eq!(squirrel_play(60, false), squirrel_play_solution(60, false))
    }
}

#[cfg(test)]
mod test_caught_speeding {
    use super::implementations::caught_speeding;
    use super::solutions::caught_speeding_solution;

    #[test]
    fn test_caught_speeding_1() {
        assert_eq!(caught_speeding(60, false), caught_speeding_solution(60, false))
    }
    #[test]
    fn test_caught_speeding_2() {
        assert_eq!(caught_speeding(65, false), caught_speeding_solution(65, false))
    }
    #[test]
    fn test_caught_speeding_3() {
        assert_eq!(caught_speeding(65, true), caught_speeding_solution(65, true))
    }
    #[test]
    fn test_caught_speeding_4() {
        assert_eq!(caught_speeding(80, false), caught_speeding_solution(80, false))
    }
    #[test]
    fn test_caught_speeding_5() {
        assert_eq!(caught_speeding(85, false), caught_speeding_solution(85, false))
    }
    #[test]
    fn test_caught_speeding_6() {
        assert_eq!(caught_speeding(85, true), caught_speeding_solution(85, true))
    }
    #[test]
    fn test_caught_speeding_7() {
        assert_eq!(caught_speeding(70, false), caught_speeding_solution(70, false))
    }
    #[test]
    fn test_caught_speeding_8() {
        assert_eq!(caught_speeding(75, false), caught_speeding_solution(75, false))
    }
    #[test]
    fn test_caught_speeding_9() {
        assert_eq!(caught_speeding(75, true), caught_speeding_solution(75, true))
    }
    #[test]
    fn test_caught_speeding_10() {
        assert_eq!(caught_speeding(40, false), caught_speeding_solution(40, false))
    }
    #[test]
    fn test_caught_speeding_11() {
        assert_eq!(caught_speeding(40, true), caught_speeding_solution(40, true))
    }
    #[test]
    fn test_caught_speeding_12() {
        assert_eq!(caught_speeding(90, false), caught_speeding_solution(90, false))
    }
}

#[cfg(test)]
mod test_sorta_sum {
    use super::implementations::sorta_sum;
    use super::solutions::sorta_sum_solution;

    #[test]
    fn test_sorta_sum_1() {
        assert_eq!(sorta_sum(3, 4), sorta_sum_solution(3, 4))
    }
    #[test]
    fn test_sorta_sum_2() {
        assert_eq!(sorta_sum(9, 4), sorta_sum_solution(9, 4))
    }
    #[test]
    fn test_sorta_sum_3() {
        assert_eq!(sorta_sum(10, 11), sorta_sum_solution(10, 11))
    }
    #[test]
    fn test_sorta_sum_4() {
        assert_eq!(sorta_sum(12, -3), sorta_sum_solution(12, -3))
    }
    #[test]
    fn test_sorta_sum_5() {
        assert_eq!(sorta_sum(-3, 12), sorta_sum_solution(-3, 12))
    }
    #[test]
    fn test_sorta_sum_6() {
        assert_eq!(sorta_sum(4, 5), sorta_sum_solution(4, 5))
    }
    #[test]
    fn test_sorta_sum_7() {
        assert_eq!(sorta_sum(4, 6), sorta_sum_solution(4, 6))
    }
    #[test]
    fn test_sorta_sum_8() {
        assert_eq!(sorta_sum(14, 7), sorta_sum_solution(14, 7))
    }
    #[test]
    fn test_sorta_sum_9() {
        assert_eq!(sorta_sum(14, 6), sorta_sum_solution(14, 6))
    }
}

#[cfg(test)]
mod test_alarm_clock {
    use crate::sections::logic::implementations::DayOfTheWeek;

    use super::implementations::alarm_clock;
    use super::solutions::alarm_clock_solution;

    #[test]
    fn test_alarm_clock_1() {
        assert_eq!(alarm_clock(DayOfTheWeek::Monday, false), alarm_clock_solution(DayOfTheWeek::Monday, false))
    }
    #[test]
    fn test_alarm_clock_2() {
        assert_eq!(alarm_clock(DayOfTheWeek::Friday, false), alarm_clock_solution(DayOfTheWeek::Friday, false))
    }
    #[test]
    fn test_alarm_clock_3() {
        assert_eq!(alarm_clock(DayOfTheWeek::Sunday, false), alarm_clock_solution(DayOfTheWeek::Sunday, false))
    }
    #[test]
    fn test_alarm_clock_4() {
        assert_eq!(alarm_clock(DayOfTheWeek::Saturday, false), alarm_clock_solution(DayOfTheWeek::Saturday, false))
    }
    #[test]
    fn test_alarm_clock_5() {
        assert_eq!(alarm_clock(DayOfTheWeek::Sunday, true), alarm_clock_solution(DayOfTheWeek::Sunday, true))
    }
    #[test]
    fn test_alarm_clock_6() {
        assert_eq!(alarm_clock(DayOfTheWeek::Saturday, true), alarm_clock_solution(DayOfTheWeek::Saturday, true))
    }
    #[test]
    fn test_alarm_clock_7() {
        assert_eq!(alarm_clock(DayOfTheWeek::Monday, true), alarm_clock_solution(DayOfTheWeek::Monday, true))
    }
    #[test]
    fn test_alarm_clock_8() {
        assert_eq!(alarm_clock(DayOfTheWeek::Wednesday, true), alarm_clock_solution(DayOfTheWeek::Wednesday, true))
    }
    #[test]
    fn test_alarm_clock_9() {
        assert_eq!(alarm_clock(DayOfTheWeek::Friday, true), alarm_clock_solution(DayOfTheWeek::Friday, true))
    }
}

#[cfg(test)]
mod test_love6 {
    use super::implementations::love6;
    use super::solutions::love6_solution;

    #[test]
    fn test_love6_1() {
        assert_eq!(love6(6, 4), love6_solution(6, 4))
    }
    #[test]
    fn test_love6_2() {
        assert_eq!(love6(4, 5), love6_solution(4, 5))
    }
    #[test]
    fn test_love6_3() {
        assert_eq!(love6(1, 5), love6_solution(1, 5))
    }
    #[test]
    fn test_love6_4() {
        assert_eq!(love6(1, 6), love6_solution(1, 6))
    }
    #[test]
    fn test_love6_5() {
        assert_eq!(love6(1, 8), love6_solution(1, 8))
    }
    #[test]
    fn test_love6_6() {
        assert_eq!(love6(1, 7), love6_solution(1, 7))
    }
    #[test]
    fn test_love6_7() {
        assert_eq!(love6(7, 5), love6_solution(7, 5))
    }
    #[test]
    fn test_love6_8() {
        assert_eq!(love6(8, 2), love6_solution(8, 2))
    }
    #[test]
    fn test_love6_9() {
        assert_eq!(love6(6, 6), love6_solution(6, 6))
    }
    #[test]
    fn test_love6_10() {
        assert_eq!(love6(-6, 2), love6_solution(-6, 2))
    }
    #[test]
    fn test_love6_11() {
        assert_eq!(love6(-4, -10), love6_solution(-4, -10))
    }
    #[test]
    fn test_love6_12() {
        assert_eq!(love6(-7, 1), love6_solution(-7, 1))
    }
    #[test]
    fn test_love6_13() {
        assert_eq!(love6(7, -1), love6_solution(7, -1))
    }
    #[test]
    fn test_love6_14() {
        assert_eq!(love6(-6, 12), love6_solution(-6, 12))
    }
    #[test]
    fn test_love6_15() {
        assert_eq!(love6(-2, -4), love6_solution(-2, -4))
    }
    #[test]
    fn test_love6_16() {
        assert_eq!(love6(7, 1), love6_solution(7, 1))
    }
    #[test]
    fn test_love6_17() {
        assert_eq!(love6(0, 9), love6_solution(0, 9))
    }
    #[test]
    fn test_love6_18() {
        assert_eq!(love6(8, 3), love6_solution(8, 3))
    }
    #[test]
    fn test_love6_19() {
        assert_eq!(love6(3, 3), love6_solution(3, 3))
    }
    #[test]
    fn test_love6_20() {
        assert_eq!(love6(3, 4), love6_solution(3, 4))
    }
}

#[cfg(test)]
mod test_in1_to10 {
    use super::implementations::in1_to10;
    use super::solutions::in1_to10_solution;

    #[test]
    fn test_in1_to10_1() {
        assert_eq!(in1_to10(5, false), in1_to10_solution(5, false))
    }
    #[test]
    fn test_in1_to10_2() {
        assert_eq!(in1_to10(11, false), in1_to10_solution(11, false))
    }
    #[test]
    fn test_in1_to10_3() {
        assert_eq!(in1_to10(11, true), in1_to10_solution(11, true))
    }
    #[test]
    fn test_in1_to10_4() {
        assert_eq!(in1_to10(10, false), in1_to10_solution(10, false))
    }
    #[test]
    fn test_in1_to10_5() {
        assert_eq!(in1_to10(10, true), in1_to10_solution(10, true))
    }
    #[test]
    fn test_in1_to10_6() {
        assert_eq!(in1_to10(9, false), in1_to10_solution(9, false))
    }
    #[test]
    fn test_in1_to10_7() {
        assert_eq!(in1_to10(9, true), in1_to10_solution(9, true))
    }
    #[test]
    fn test_in1_to10_8() {
        assert_eq!(in1_to10(1, false), in1_to10_solution(1, false))
    }
    #[test]
    fn test_in1_to10_9() {
        assert_eq!(in1_to10(1, true), in1_to10_solution(1, true))
    }
    #[test]
    fn test_in1_to10_10() {
        assert_eq!(in1_to10(0, false), in1_to10_solution(0, false))
    }
    #[test]
    fn test_in1_to10_11() {
        assert_eq!(in1_to10(0, true), in1_to10_solution(0, true))
    }
    #[test]
    fn test_in1_to10_12() {
        assert_eq!(in1_to10(-1, false), in1_to10_solution(-1, false))
    }
}

#[cfg(test)]
mod test_special_eleven {
    use super::implementations::special_eleven;
    use super::solutions::special_eleven_solution;

    #[test]
    fn test_special_eleven_1() {
        assert_eq!(special_eleven(22), special_eleven_solution(22))
    }
    #[test]
    fn test_special_eleven_2() {
        assert_eq!(special_eleven(23), special_eleven_solution(23))
    }
    #[test]
    fn test_special_eleven_3() {
        assert_eq!(special_eleven(24), special_eleven_solution(24))
    }
    #[test]
    fn test_special_eleven_4() {
        assert_eq!(special_eleven(21), special_eleven_solution(21))
    }
    #[test]
    fn test_special_eleven_5() {
        assert_eq!(special_eleven(11), special_eleven_solution(11))
    }
    #[test]
    fn test_special_eleven_6() {
        assert_eq!(special_eleven(12), special_eleven_solution(12))
    }
    #[test]
    fn test_special_eleven_7() {
        assert_eq!(special_eleven(10), special_eleven_solution(10))
    }
    #[test]
    fn test_special_eleven_8() {
        assert_eq!(special_eleven(9), special_eleven_solution(9))
    }
    #[test]
    fn test_special_eleven_9() {
        assert_eq!(special_eleven(8), special_eleven_solution(8))
    }
    #[test]
    fn test_special_eleven_10() {
        assert_eq!(special_eleven(0), special_eleven_solution(0))
    }
    #[test]
    fn test_special_eleven_11() {
        assert_eq!(special_eleven(1), special_eleven_solution(1))
    }
    #[test]
    fn test_special_eleven_12() {
        assert_eq!(special_eleven(2), special_eleven_solution(2))
    }
    #[test]
    fn test_special_eleven_13() {
        assert_eq!(special_eleven(121), special_eleven_solution(121))
    }
    #[test]
    fn test_special_eleven_14() {
        assert_eq!(special_eleven(122), special_eleven_solution(122))
    }
    #[test]
    fn test_special_eleven_15() {
        assert_eq!(special_eleven(123), special_eleven_solution(123))
    }
    #[test]
    fn test_special_eleven_16() {
        assert_eq!(special_eleven(46), special_eleven_solution(46))
    }
    #[test]
    fn test_special_eleven_17() {
        assert_eq!(special_eleven(49), special_eleven_solution(49))
    }
    #[test]
    fn test_special_eleven_18() {
        assert_eq!(special_eleven(52), special_eleven_solution(52))
    }
    #[test]
    fn test_special_eleven_19() {
        assert_eq!(special_eleven(54), special_eleven_solution(54))
    }
    #[test]
    fn test_special_eleven_20() {
        assert_eq!(special_eleven(55), special_eleven_solution(55))
    }
}

#[cfg(test)]
mod test_more20 {
    use super::implementations::more20;
    use super::solutions::more20_solution;

    #[test]
    fn test_more20_1() {
        assert_eq!(more20(20), more20_solution(20))
    }
    #[test]
    fn test_more20_2() {
        assert_eq!(more20(21), more20_solution(21))
    }
    #[test]
    fn test_more20_3() {
        assert_eq!(more20(22), more20_solution(22))
    }
    #[test]
    fn test_more20_4() {
        assert_eq!(more20(23), more20_solution(23))
    }
    #[test]
    fn test_more20_5() {
        assert_eq!(more20(25), more20_solution(25))
    }
    #[test]
    fn test_more20_6() {
        assert_eq!(more20(30), more20_solution(30))
    }
    #[test]
    fn test_more20_7() {
        assert_eq!(more20(31), more20_solution(31))
    }
    #[test]
    fn test_more20_8() {
        assert_eq!(more20(59), more20_solution(59))
    }
    #[test]
    fn test_more20_9() {
        assert_eq!(more20(60), more20_solution(60))
    }
    #[test]
    fn test_more20_10() {
        assert_eq!(more20(61), more20_solution(61))
    }
    #[test]
    fn test_more20_11() {
        assert_eq!(more20(62), more20_solution(62))
    }
    #[test]
    fn test_more20_12() {
        assert_eq!(more20(1020), more20_solution(1020))
    }
    #[test]
    fn test_more20_13() {
        assert_eq!(more20(1021), more20_solution(1021))
    }
    #[test]
    fn test_more20_14() {
        assert_eq!(more20(1000), more20_solution(1000))
    }
    #[test]
    fn test_more20_15() {
        assert_eq!(more20(1001), more20_solution(1001))
    }
    #[test]
    fn test_more20_16() {
        assert_eq!(more20(50), more20_solution(50))
    }
    #[test]
    fn test_more20_17() {
        assert_eq!(more20(55), more20_solution(55))
    }
    #[test]
    fn test_more20_18() {
        assert_eq!(more20(40), more20_solution(40))
    }
    #[test]
    fn test_more20_19() {
        assert_eq!(more20(41), more20_solution(41))
    }
    #[test]
    fn test_more20_20() {
        assert_eq!(more20(39), more20_solution(39))
    }
    #[test]
    fn test_more20_21() {
        assert_eq!(more20(42), more20_solution(42))
    }
}

#[cfg(test)]
mod test_old35 {
    use super::implementations::old35;
    use super::solutions::old35_solution;

    #[test]
    fn test_old35_1() {
        assert_eq!(old35(3), old35_solution(3))
    }
    #[test]
    fn test_old35_2() {
        assert_eq!(old35(10), old35_solution(10))
    }
    #[test]
    fn test_old35_3() {
        assert_eq!(old35(15), old35_solution(15))
    }
    #[test]
    fn test_old35_4() {
        assert_eq!(old35(5), old35_solution(5))
    }
    #[test]
    fn test_old35_5() {
        assert_eq!(old35(9), old35_solution(9))
    }
    #[test]
    fn test_old35_6() {
        assert_eq!(old35(8), old35_solution(8))
    }
    #[test]
    fn test_old35_7() {
        assert_eq!(old35(7), old35_solution(7))
    }
    #[test]
    fn test_old35_8() {
        assert_eq!(old35(6), old35_solution(6))
    }
    #[test]
    fn test_old35_9() {
        assert_eq!(old35(17), old35_solution(17))
    }
    #[test]
    fn test_old35_10() {
        assert_eq!(old35(18), old35_solution(18))
    }
    #[test]
    fn test_old35_11() {
        assert_eq!(old35(29), old35_solution(29))
    }
    #[test]
    fn test_old35_12() {
        assert_eq!(old35(20), old35_solution(20))
    }
    #[test]
    fn test_old35_13() {
        assert_eq!(old35(21), old35_solution(21))
    }
    #[test]
    fn test_old35_14() {
        assert_eq!(old35(22), old35_solution(22))
    }
    #[test]
    fn test_old35_15() {
        assert_eq!(old35(45), old35_solution(45))
    }
    #[test]
    fn test_old35_16() {
        assert_eq!(old35(99), old35_solution(99))
    }
}

#[cfg(test)]
mod test_less20 {
    use super::implementations::less20;
    use super::solutions::less20_solution;

    #[test]
    fn test_less20_1() {
        assert_eq!(less20(18), less20_solution(18))
    }
    #[test]
    fn test_less20_2() {
        assert_eq!(less20(19), less20_solution(19))
    }
    #[test]
    fn test_less20_3() {
        assert_eq!(less20(20), less20_solution(20))
    }
    #[test]
    fn test_less20_4() {
        assert_eq!(less20(8), less20_solution(8))
    }
    #[test]
    fn test_less20_5() {
        assert_eq!(less20(17), less20_solution(17))
    }
    #[test]
    fn test_less20_6() {
        assert_eq!(less20(23), less20_solution(23))
    }
    #[test]
    fn test_less20_7() {
        assert_eq!(less20(25), less20_solution(25))
    }
    #[test]
    fn test_less20_8() {
        assert_eq!(less20(30), less20_solution(30))
    }
    #[test]
    fn test_less20_9() {
        assert_eq!(less20(31), less20_solution(31))
    }
    #[test]
    fn test_less20_10() {
        assert_eq!(less20(58), less20_solution(58))
    }
    #[test]
    fn test_less20_11() {
        assert_eq!(less20(59), less20_solution(59))
    }
    #[test]
    fn test_less20_12() {
        assert_eq!(less20(60), less20_solution(60))
    }
    #[test]
    fn test_less20_13() {
        assert_eq!(less20(61), less20_solution(61))
    }
    #[test]
    fn test_less20_14() {
        assert_eq!(less20(62), less20_solution(62))
    }
    #[test]
    fn test_less20_15() {
        assert_eq!(less20(1017), less20_solution(1017))
    }
    #[test]
    fn test_less20_16() {
        assert_eq!(less20(1018), less20_solution(1018))
    }
    #[test]
    fn test_less20_17() {
        assert_eq!(less20(1019), less20_solution(1019))
    }
    #[test]
    fn test_less20_18() {
        assert_eq!(less20(1020), less20_solution(1020))
    }
    #[test]
    fn test_less20_19() {
        assert_eq!(less20(1021), less20_solution(1021))
    }
    #[test]
    fn test_less20_20() {
        assert_eq!(less20(1022), less20_solution(1022))
    }
    #[test]
    fn test_less20_21() {
        assert_eq!(less20(1023), less20_solution(1023))
    }
    #[test]
    fn test_less20_22() {
        assert_eq!(less20(37), less20_solution(37))
    }
}

#[cfg(test)]
mod test_near_ten {
    use super::implementations::near_ten;
    use super::solutions::near_ten_solution;

    #[test]
    fn test_near_ten_1() {
        assert_eq!(near_ten(12), near_ten_solution(12))
    }
    #[test]
    fn test_near_ten_2() {
        assert_eq!(near_ten(17), near_ten_solution(17))
    }
    #[test]
    fn test_near_ten_3() {
        assert_eq!(near_ten(19), near_ten_solution(19))
    }
    #[test]
    fn test_near_ten_4() {
        assert_eq!(near_ten(31), near_ten_solution(31))
    }
    #[test]
    fn test_near_ten_5() {
        assert_eq!(near_ten(6), near_ten_solution(6))
    }
    #[test]
    fn test_near_ten_6() {
        assert_eq!(near_ten(10), near_ten_solution(10))
    }
    #[test]
    fn test_near_ten_7() {
        assert_eq!(near_ten(11), near_ten_solution(11))
    }
    #[test]
    fn test_near_ten_8() {
        assert_eq!(near_ten(21), near_ten_solution(21))
    }
    #[test]
    fn test_near_ten_9() {
        assert_eq!(near_ten(22), near_ten_solution(22))
    }
    #[test]
    fn test_near_ten_10() {
        assert_eq!(near_ten(23), near_ten_solution(23))
    }
    #[test]
    fn test_near_ten_11() {
        assert_eq!(near_ten(54), near_ten_solution(54))
    }
    #[test]
    fn test_near_ten_12() {
        assert_eq!(near_ten(155), near_ten_solution(155))
    }
    #[test]
    fn test_near_ten_13() {
        assert_eq!(near_ten(158), near_ten_solution(158))
    }
    #[test]
    fn test_near_ten_14() {
        assert_eq!(near_ten(3), near_ten_solution(3))
    }
    #[test]
    fn test_near_ten_15() {
        assert_eq!(near_ten(1), near_ten_solution(1))
    }
}

#[cfg(test)]
mod test_teen_sum {
    use super::implementations::teen_sum;
    use super::solutions::teen_sum_solution;

    #[test]
    fn test_teen_sum_1() {
        assert_eq!(teen_sum(3, 4), teen_sum_solution(3, 4))
    }
    #[test]
    fn test_teen_sum_2() {
        assert_eq!(teen_sum(10, 13), teen_sum_solution(10, 13))
    }
    #[test]
    fn test_teen_sum_3() {
        assert_eq!(teen_sum(13, 2), teen_sum_solution(13, 2))
    }
    #[test]
    fn test_teen_sum_4() {
        assert_eq!(teen_sum(3, 19), teen_sum_solution(3, 19))
    }
    #[test]
    fn test_teen_sum_5() {
        assert_eq!(teen_sum(13, 13), teen_sum_solution(13, 13))
    }
    #[test]
    fn test_teen_sum_6() {
        assert_eq!(teen_sum(10, 10), teen_sum_solution(10, 10))
    }
    #[test]
    fn test_teen_sum_7() {
        assert_eq!(teen_sum(6, 14), teen_sum_solution(6, 14))
    }
    #[test]
    fn test_teen_sum_8() {
        assert_eq!(teen_sum(15, 2), teen_sum_solution(15, 2))
    }
    #[test]
    fn test_teen_sum_9() {
        assert_eq!(teen_sum(19, 19), teen_sum_solution(19, 19))
    }
    #[test]
    fn test_teen_sum_10() {
        assert_eq!(teen_sum(19, 20), teen_sum_solution(19, 20))
    }
    #[test]
    fn test_teen_sum_11() {
        assert_eq!(teen_sum(2, 18), teen_sum_solution(2, 18))
    }
    #[test]
    fn test_teen_sum_12() {
        assert_eq!(teen_sum(12, 4), teen_sum_solution(12, 4))
    }
    #[test]
    fn test_teen_sum_13() {
        assert_eq!(teen_sum(2, 20), teen_sum_solution(2, 20))
    }
    #[test]
    fn test_teen_sum_14() {
        assert_eq!(teen_sum(2, 17), teen_sum_solution(2, 17))
    }
    #[test]
    fn test_teen_sum_15() {
        assert_eq!(teen_sum(2, 16), teen_sum_solution(2, 16))
    }
    #[test]
    fn test_teen_sum_16() {
        assert_eq!(teen_sum(6, 7), teen_sum_solution(6, 7))
    }
}

#[cfg(test)]
mod test_answer_cell {
    use super::implementations::answer_cell;
    use super::solutions::answer_cell_solution;

    #[test]
    fn test_answer_cell_1() {
        assert_eq!(answer_cell(false, false, false), answer_cell_solution(false, false, false))
    }
    #[test]
    fn test_answer_cell_2() {
        assert_eq!(answer_cell(false, false, true), answer_cell_solution(false, false, true))
    }
    #[test]
    fn test_answer_cell_3() {
        assert_eq!(answer_cell(true, false, false), answer_cell_solution(true, false, false))
    }
    #[test]
    fn test_answer_cell_4() {
        assert_eq!(answer_cell(true, true, false), answer_cell_solution(true, true, false))
    }
    #[test]
    fn test_answer_cell_5() {
        assert_eq!(answer_cell(false, true, false), answer_cell_solution(false, true, false))
    }
    #[test]
    fn test_answer_cell_6() {
        assert_eq!(answer_cell(true, true, true), answer_cell_solution(true, true, true))
    }
}

#[cfg(test)]
mod test_tea_party {
    use super::implementations::tea_party;
    use super::solutions::tea_party_solution;

    #[test]
    fn test_tea_party_1() {
        assert_eq!(tea_party(6, 8), tea_party_solution(6, 8))
    }
    #[test]
    fn test_tea_party_2() {
        assert_eq!(tea_party(3, 8), tea_party_solution(3, 8))
    }
    #[test]
    fn test_tea_party_3() {
        assert_eq!(tea_party(20, 6), tea_party_solution(20, 6))
    }
    #[test]
    fn test_tea_party_4() {
        assert_eq!(tea_party(12, 6), tea_party_solution(12, 6))
    }
    #[test]
    fn test_tea_party_5() {
        assert_eq!(tea_party(11, 6), tea_party_solution(11, 6))
    }
    #[test]
    fn test_tea_party_6() {
        assert_eq!(tea_party(11, 4), tea_party_solution(11, 4))
    }
    #[test]
    fn test_tea_party_7() {
        assert_eq!(tea_party(4, 5), tea_party_solution(4, 5))
    }
    #[test]
    fn test_tea_party_8() {
        assert_eq!(tea_party(5, 5), tea_party_solution(5, 5))
    }
    #[test]
    fn test_tea_party_9() {
        assert_eq!(tea_party(6, 6), tea_party_solution(6, 6))
    }
    #[test]
    fn test_tea_party_10() {
        assert_eq!(tea_party(5, 10), tea_party_solution(5, 10))
    }
    #[test]
    fn test_tea_party_11() {
        assert_eq!(tea_party(5, 9), tea_party_solution(5, 9))
    }
    #[test]
    fn test_tea_party_12() {
        assert_eq!(tea_party(10, 4), tea_party_solution(10, 4))
    }
    #[test]
    fn test_tea_party_13() {
        assert_eq!(tea_party(10, 20), tea_party_solution(10, 20))
    }
}

#[cfg(test)]
mod test_fizz_string {
    use super::implementations::fizz_string;
    use super::solutions::fizz_string_solution;

    #[test]
    fn test_fizz_string_1() {
        assert_eq!(fizz_string("fig"), fizz_string_solution("fig"))
    }
    #[test]
    fn test_fizz_string_2() {
        assert_eq!(fizz_string("dib"), fizz_string_solution("dib"))
    }
    #[test]
    fn test_fizz_string_3() {
        assert_eq!(fizz_string("fib"), fizz_string_solution("fib"))
    }
    #[test]
    fn test_fizz_string_4() {
        assert_eq!(fizz_string("abc"), fizz_string_solution("abc"))
    }
    #[test]
    fn test_fizz_string_5() {
        assert_eq!(fizz_string("fooo"), fizz_string_solution("fooo"))
    }
    #[test]
    fn test_fizz_string_6() {
        assert_eq!(fizz_string("booo"), fizz_string_solution("booo"))
    }
    #[test]
    fn test_fizz_string_7() {
        assert_eq!(fizz_string("ooob"), fizz_string_solution("ooob"))
    }
    #[test]
    fn test_fizz_string_8() {
        assert_eq!(fizz_string("fooob"), fizz_string_solution("fooob"))
    }
    #[test]
    fn test_fizz_string_9() {
        assert_eq!(fizz_string("f"), fizz_string_solution("f"))
    }
    #[test]
    fn test_fizz_string_10() {
        assert_eq!(fizz_string("b"), fizz_string_solution("b"))
    }
    #[test]
    fn test_fizz_string_11() {
        assert_eq!(fizz_string("abcb"), fizz_string_solution("abcb"))
    }
    #[test]
    fn test_fizz_string_12() {
        assert_eq!(fizz_string("Hello"), fizz_string_solution("Hello"))
    }
    #[test]
    fn test_fizz_string_13() {
        assert_eq!(fizz_string("Hellob"), fizz_string_solution("Hellob"))
    }
    #[test]
    fn test_fizz_string_14() {
        assert_eq!(fizz_string("af"), fizz_string_solution("af"))
    }
    #[test]
    fn test_fizz_string_15() {
        assert_eq!(fizz_string("bf"), fizz_string_solution("bf"))
    }
    #[test]
    fn test_fizz_string_16() {
        assert_eq!(fizz_string("fb"), fizz_string_solution("fb"))
    }
}

#[cfg(test)]
mod test_fizz_string2 {
    use super::implementations::fizz_string2;
    use super::solutions::fizz_string2_solution;

    #[test]
    fn test_fizz_string2_1() {
        assert_eq!(fizz_string2(1), fizz_string2_solution(1))
    }
    #[test]
    fn test_fizz_string2_2() {
        assert_eq!(fizz_string2(2), fizz_string2_solution(2))
    }
    #[test]
    fn test_fizz_string2_3() {
        assert_eq!(fizz_string2(3), fizz_string2_solution(3))
    }
    #[test]
    fn test_fizz_string2_4() {
        assert_eq!(fizz_string2(4), fizz_string2_solution(4))
    }
    #[test]
    fn test_fizz_string2_5() {
        assert_eq!(fizz_string2(5), fizz_string2_solution(5))
    }
    #[test]
    fn test_fizz_string2_6() {
        assert_eq!(fizz_string2(6), fizz_string2_solution(6))
    }
    #[test]
    fn test_fizz_string2_7() {
        assert_eq!(fizz_string2(7), fizz_string2_solution(7))
    }
    #[test]
    fn test_fizz_string2_8() {
        assert_eq!(fizz_string2(8), fizz_string2_solution(8))
    }
    #[test]
    fn test_fizz_string2_9() {
        assert_eq!(fizz_string2(9), fizz_string2_solution(9))
    }
    #[test]
    fn test_fizz_string2_10() {
        assert_eq!(fizz_string2(15), fizz_string2_solution(15))
    }
    #[test]
    fn test_fizz_string2_11() {
        assert_eq!(fizz_string2(16), fizz_string2_solution(16))
    }
    #[test]
    fn test_fizz_string2_12() {
        assert_eq!(fizz_string2(18), fizz_string2_solution(18))
    }
    #[test]
    fn test_fizz_string2_13() {
        assert_eq!(fizz_string2(19), fizz_string2_solution(19))
    }
    #[test]
    fn test_fizz_string2_14() {
        assert_eq!(fizz_string2(21), fizz_string2_solution(21))
    }
    #[test]
    fn test_fizz_string2_15() {
        assert_eq!(fizz_string2(44), fizz_string2_solution(44))
    }
    #[test]
    fn test_fizz_string2_16() {
        assert_eq!(fizz_string2(45), fizz_string2_solution(45))
    }
    #[test]
    fn test_fizz_string2_17() {
        assert_eq!(fizz_string2(100), fizz_string2_solution(100))
    }
}

#[cfg(test)]
mod test_two_as_one {
    use super::implementations::two_as_one;
    use super::solutions::two_as_one_solution;

    #[test]
    fn test_two_as_one_1() {
        assert_eq!(two_as_one(1, 2, 3), two_as_one_solution(1, 2, 3))
    }
    #[test]
    fn test_two_as_one_2() {
        assert_eq!(two_as_one(3, 1, 2), two_as_one_solution(3, 1, 2))
    }
    #[test]
    fn test_two_as_one_3() {
        assert_eq!(two_as_one(3, 2, 2), two_as_one_solution(3, 2, 2))
    }
    #[test]
    fn test_two_as_one_4() {
        assert_eq!(two_as_one(2, 3, 1), two_as_one_solution(2, 3, 1))
    }
    #[test]
    fn test_two_as_one_5() {
        assert_eq!(two_as_one(5, 3, -2), two_as_one_solution(5, 3, -2))
    }
    #[test]
    fn test_two_as_one_6() {
        assert_eq!(two_as_one(5, 3, -3), two_as_one_solution(5, 3, -3))
    }
    #[test]
    fn test_two_as_one_7() {
        assert_eq!(two_as_one(2, 5, 3), two_as_one_solution(2, 5, 3))
    }
    #[test]
    fn test_two_as_one_8() {
        assert_eq!(two_as_one(9, 5, 5), two_as_one_solution(9, 5, 5))
    }
    #[test]
    fn test_two_as_one_9() {
        assert_eq!(two_as_one(9, 4, 5), two_as_one_solution(9, 4, 5))
    }
    #[test]
    fn test_two_as_one_10() {
        assert_eq!(two_as_one(5, 4, 9), two_as_one_solution(5, 4, 9))
    }
    #[test]
    fn test_two_as_one_11() {
        assert_eq!(two_as_one(3, 3, 0), two_as_one_solution(3, 3, 0))
    }
    #[test]
    fn test_two_as_one_12() {
        assert_eq!(two_as_one(3, 3, 2), two_as_one_solution(3, 3, 2))
    }
}

#[cfg(test)]
mod test_in_order {
    use super::implementations::in_order;
    use super::solutions::in_order_solution;

    #[test]
    fn test_in_order_1() {
        assert_eq!(in_order(1, 2, 4, false), in_order_solution(1, 2, 4, false))
    }
    #[test]
    fn test_in_order_2() {
        assert_eq!(in_order(1, 2, 1, false), in_order_solution(1, 2, 1, false))
    }
    #[test]
    fn test_in_order_3() {
        assert_eq!(in_order(1, 1, 2, true), in_order_solution(1, 1, 2, true))
    }
    #[test]
    fn test_in_order_4() {
        assert_eq!(in_order(3, 2, 4, false), in_order_solution(3, 2, 4, false))
    }
    #[test]
    fn test_in_order_5() {
        assert_eq!(in_order(2, 3, 4, false), in_order_solution(2, 3, 4, false))
    }
    #[test]
    fn test_in_order_6() {
        assert_eq!(in_order(3, 2, 4, true), in_order_solution(3, 2, 4, true))
    }
    #[test]
    fn test_in_order_7() {
        assert_eq!(in_order(4, 2, 2, true), in_order_solution(4, 2, 2, true))
    }
    #[test]
    fn test_in_order_8() {
        assert_eq!(in_order(4, 5, 2, true), in_order_solution(4, 5, 2, true))
    }
    #[test]
    fn test_in_order_9() {
        assert_eq!(in_order(2, 4, 6, true), in_order_solution(2, 4, 6, true))
    }
    #[test]
    fn test_in_order_10() {
        assert_eq!(in_order(7, 9, 10, false), in_order_solution(7, 9, 10, false))
    }
    #[test]
    fn test_in_order_11() {
        assert_eq!(in_order(7, 5, 6, true), in_order_solution(7, 5, 6, true))
    }
    #[test]
    fn test_in_order_12() {
        assert_eq!(in_order(7, 5, 4, true), in_order_solution(7, 5, 4, true))
    }
}

#[cfg(test)]
mod test_in_order_equal {
    use super::implementations::in_order_equal;
    use super::solutions::in_order_equal_solution;

    #[test]
    fn test_in_order_equal_1() {
        assert_eq!(in_order_equal(2, 5, 11, false), in_order_equal_solution(2, 5, 11, false))
    }
    #[test]
    fn test_in_order_equal_2() {
        assert_eq!(in_order_equal(5, 7, 6, false), in_order_equal_solution(5, 7, 6, false))
    }
    #[test]
    fn test_in_order_equal_3() {
        assert_eq!(in_order_equal(5, 5, 7, true), in_order_equal_solution(5, 5, 7, true))
    }
    #[test]
    fn test_in_order_equal_4() {
        assert_eq!(in_order_equal(5, 5, 7, false), in_order_equal_solution(5, 5, 7, false))
    }
    #[test]
    fn test_in_order_equal_5() {
        assert_eq!(in_order_equal(2, 5, 4, false), in_order_equal_solution(2, 5, 4, false))
    }
    #[test]
    fn test_in_order_equal_6() {
        assert_eq!(in_order_equal(3, 4, 3, false), in_order_equal_solution(3, 4, 3, false))
    }
    #[test]
    fn test_in_order_equal_7() {
        assert_eq!(in_order_equal(3, 4, 4, false), in_order_equal_solution(3, 4, 4, false))
    }
    #[test]
    fn test_in_order_equal_8() {
        assert_eq!(in_order_equal(3, 4, 3, true), in_order_equal_solution(3, 4, 3, true))
    }
    #[test]
    fn test_in_order_equal_9() {
        assert_eq!(in_order_equal(3, 4, 4, true), in_order_equal_solution(3, 4, 4, true))
    }
    #[test]
    fn test_in_order_equal_10() {
        assert_eq!(in_order_equal(1, 5, 5, true), in_order_equal_solution(1, 5, 5, true))
    }
    #[test]
    fn test_in_order_equal_11() {
        assert_eq!(in_order_equal(5, 5, 5, true), in_order_equal_solution(5, 5, 5, true))
    }
    #[test]
    fn test_in_order_equal_12() {
        assert_eq!(in_order_equal(2, 2, 1, true), in_order_equal_solution(2, 2, 1, true))
    }
    #[test]
    fn test_in_order_equal_13() {
        assert_eq!(in_order_equal(9, 2, 2, true), in_order_equal_solution(9, 2, 2, true))
    }
    #[test]
    fn test_in_order_equal_14() {
        assert_eq!(in_order_equal(0, 1, 0, true), in_order_equal_solution(0, 1, 0, true))
    }
}

#[cfg(test)]
mod test_last_digit {
    use super::implementations::last_digit;
    use super::solutions::last_digit_solution;

    #[test]
    fn test_last_digit_1() {
        assert_eq!(last_digit(23, 19, 13), last_digit_solution(23, 19, 13))
    }
    #[test]
    fn test_last_digit_2() {
        assert_eq!(last_digit(23, 19, 12), last_digit_solution(23, 19, 12))
    }
    #[test]
    fn test_last_digit_3() {
        assert_eq!(last_digit(23, 19, 3), last_digit_solution(23, 19, 3))
    }
    #[test]
    fn test_last_digit_4() {
        assert_eq!(last_digit(23, 19, 39), last_digit_solution(23, 19, 39))
    }
    #[test]
    fn test_last_digit_5() {
        assert_eq!(last_digit(1, 2, 3), last_digit_solution(1, 2, 3))
    }
    #[test]
    fn test_last_digit_6() {
        assert_eq!(last_digit(1, 1, 2), last_digit_solution(1, 1, 2))
    }
    #[test]
    fn test_last_digit_7() {
        assert_eq!(last_digit(1, 2, 2), last_digit_solution(1, 2, 2))
    }
    #[test]
    fn test_last_digit_8() {
        assert_eq!(last_digit(14, 25, 43), last_digit_solution(14, 25, 43))
    }
    #[test]
    fn test_last_digit_9() {
        assert_eq!(last_digit(14, 25, 45), last_digit_solution(14, 25, 45))
    }
    #[test]
    fn test_last_digit_10() {
        assert_eq!(last_digit(248, 106, 1002), last_digit_solution(248, 106, 1002))
    }
    #[test]
    fn test_last_digit_11() {
        assert_eq!(last_digit(248, 106, 1008), last_digit_solution(248, 106, 1008))
    }
    #[test]
    fn test_last_digit_12() {
        assert_eq!(last_digit(10, 11, 20), last_digit_solution(10, 11, 20))
    }
    #[test]
    fn test_last_digit_13() {
        assert_eq!(last_digit(0, 11, 0), last_digit_solution(0, 11, 0))
    }
}

#[cfg(test)]
mod test_less_by10 {
    use super::implementations::less_by10;
    use super::solutions::less_by10_solution;

    #[test]
    fn test_less_by10_1() {
        assert_eq!(less_by10(1, 7, 11), less_by10_solution(1, 7, 11))
    }
    #[test]
    fn test_less_by10_2() {
        assert_eq!(less_by10(1, 7, 10), less_by10_solution(1, 7, 10))
    }
    #[test]
    fn test_less_by10_3() {
        assert_eq!(less_by10(11, 1, 7), less_by10_solution(11, 1, 7))
    }
    #[test]
    fn test_less_by10_4() {
        assert_eq!(less_by10(10, 7, 1), less_by10_solution(10, 7, 1))
    }
    #[test]
    fn test_less_by10_5() {
        assert_eq!(less_by10(-10, 2, 2), less_by10_solution(-10, 2, 2))
    }
    #[test]
    fn test_less_by10_6() {
        assert_eq!(less_by10(2, 11, 11), less_by10_solution(2, 11, 11))
    }
    #[test]
    fn test_less_by10_7() {
        assert_eq!(less_by10(3, 3, 30), less_by10_solution(3, 3, 30))
    }
    #[test]
    fn test_less_by10_8() {
        assert_eq!(less_by10(3, 3, 3), less_by10_solution(3, 3, 3))
    }
    #[test]
    fn test_less_by10_9() {
        assert_eq!(less_by10(10, 1, 11), less_by10_solution(10, 1, 11))
    }
    #[test]
    fn test_less_by10_10() {
        assert_eq!(less_by10(10, 11, 1), less_by10_solution(10, 11, 1))
    }
    #[test]
    fn test_less_by10_11() {
        assert_eq!(less_by10(10, 11, 2), less_by10_solution(10, 11, 2))
    }
    #[test]
    fn test_less_by10_12() {
        assert_eq!(less_by10(3, 30, 3), less_by10_solution(3, 30, 3))
    }
    #[test]
    fn test_less_by10_13() {
        assert_eq!(less_by10(2, 2, -8), less_by10_solution(2, 2, -8))
    }
    #[test]
    fn test_less_by10_14() {
        assert_eq!(less_by10(2, 8, 12), less_by10_solution(2, 8, 12))
    }
}

#[cfg(test)]
mod test_without_doubles {
    use super::implementations::without_doubles;
    use super::solutions::without_doubles_solution;

    #[test]
    fn test_without_doubles_1() {
        assert_eq!(without_doubles(2, 3, true), without_doubles_solution(2, 3, true))
    }
    #[test]
    fn test_without_doubles_2() {
        assert_eq!(without_doubles(3, 3, true), without_doubles_solution(3, 3, true))
    }
    #[test]
    fn test_without_doubles_3() {
        assert_eq!(without_doubles(3, 3, false), without_doubles_solution(3, 3, false))
    }
    #[test]
    fn test_without_doubles_4() {
        assert_eq!(without_doubles(2, 3, false), without_doubles_solution(2, 3, false))
    }
    #[test]
    fn test_without_doubles_5() {
        assert_eq!(without_doubles(5, 4, true), without_doubles_solution(5, 4, true))
    }
    #[test]
    fn test_without_doubles_6() {
        assert_eq!(without_doubles(5, 4, false), without_doubles_solution(5, 4, false))
    }
    #[test]
    fn test_without_doubles_7() {
        assert_eq!(without_doubles(5, 5, true), without_doubles_solution(5, 5, true))
    }
    #[test]
    fn test_without_doubles_8() {
        assert_eq!(without_doubles(5, 5, false), without_doubles_solution(5, 5, false))
    }
    #[test]
    fn test_without_doubles_9() {
        assert_eq!(without_doubles(6, 6, true), without_doubles_solution(6, 6, true))
    }
    #[test]
    fn test_without_doubles_10() {
        assert_eq!(without_doubles(6, 6, false), without_doubles_solution(6, 6, false))
    }
    #[test]
    fn test_without_doubles_11() {
        assert_eq!(without_doubles(1, 6, true), without_doubles_solution(1, 6, true))
    }
    #[test]
    fn test_without_doubles_12() {
        assert_eq!(without_doubles(6, 1, false), without_doubles_solution(6, 1, false))
    }
}

#[cfg(test)]
mod test_max_mod5 {
    use super::implementations::max_mod5;
    use super::solutions::max_mod5_solution;

    #[test]
    fn test_max_mod5_1() {
        assert_eq!(max_mod5(2, 3), max_mod5_solution(2, 3))
    }
    #[test]
    fn test_max_mod5_2() {
        assert_eq!(max_mod5(6, 2), max_mod5_solution(6, 2))
    }
    #[test]
    fn test_max_mod5_3() {
        assert_eq!(max_mod5(3, 2), max_mod5_solution(3, 2))
    }
    #[test]
    fn test_max_mod5_4() {
        assert_eq!(max_mod5(8, 12), max_mod5_solution(8, 12))
    }
    #[test]
    fn test_max_mod5_5() {
        assert_eq!(max_mod5(7, 12), max_mod5_solution(7, 12))
    }
    #[test]
    fn test_max_mod5_6() {
        assert_eq!(max_mod5(11, 6), max_mod5_solution(11, 6))
    }
    #[test]
    fn test_max_mod5_7() {
        assert_eq!(max_mod5(2, 7), max_mod5_solution(2, 7))
    }
    #[test]
    fn test_max_mod5_8() {
        assert_eq!(max_mod5(7, 7), max_mod5_solution(7, 7))
    }
    #[test]
    fn test_max_mod5_9() {
        assert_eq!(max_mod5(9, 1), max_mod5_solution(9, 1))
    }
    #[test]
    fn test_max_mod5_10() {
        assert_eq!(max_mod5(9, 14), max_mod5_solution(9, 14))
    }
    #[test]
    fn test_max_mod5_11() {
        assert_eq!(max_mod5(1, 2), max_mod5_solution(1, 2))
    }
}

#[cfg(test)]
mod test_red_ticket {
    use super::implementations::red_ticket;
    use super::solutions::red_ticket_solution;

    #[test]
    fn test_red_ticket_1() {
        assert_eq!(red_ticket(2, 2, 2), red_ticket_solution(2, 2, 2))
    }
    #[test]
    fn test_red_ticket_2() {
        assert_eq!(red_ticket(2, 2, 1), red_ticket_solution(2, 2, 1))
    }
    #[test]
    fn test_red_ticket_3() {
        assert_eq!(red_ticket(0, 0, 0), red_ticket_solution(0, 0, 0))
    }
    #[test]
    fn test_red_ticket_4() {
        assert_eq!(red_ticket(2, 0, 0), red_ticket_solution(2, 0, 0))
    }
    #[test]
    fn test_red_ticket_5() {
        assert_eq!(red_ticket(1, 1, 1), red_ticket_solution(1, 1, 1))
    }
    #[test]
    fn test_red_ticket_6() {
        assert_eq!(red_ticket(1, 2, 1), red_ticket_solution(1, 2, 1))
    }
    #[test]
    fn test_red_ticket_7() {
        assert_eq!(red_ticket(1, 2, 0), red_ticket_solution(1, 2, 0))
    }
    #[test]
    fn test_red_ticket_8() {
        assert_eq!(red_ticket(0, 2, 2), red_ticket_solution(0, 2, 2))
    }
    #[test]
    fn test_red_ticket_9() {
        assert_eq!(red_ticket(1, 2, 2), red_ticket_solution(1, 2, 2))
    }
    #[test]
    fn test_red_ticket_10() {
        assert_eq!(red_ticket(0, 2, 0), red_ticket_solution(0, 2, 0))
    }
    #[test]
    fn test_red_ticket_11() {
        assert_eq!(red_ticket(1, 1, 2), red_ticket_solution(1, 1, 2))
    }
}

#[cfg(test)]
mod test_green_ticket {
    use super::implementations::green_ticket;
    use super::solutions::green_ticket_solution;

    #[test]
    fn test_green_ticket_1() {
        assert_eq!(green_ticket(1, 2, 3), green_ticket_solution(1, 2, 3))
    }
    #[test]
    fn test_green_ticket_2() {
        assert_eq!(green_ticket(2, 2, 2), green_ticket_solution(2, 2, 2))
    }
    #[test]
    fn test_green_ticket_3() {
        assert_eq!(green_ticket(1, 1, 2), green_ticket_solution(1, 1, 2))
    }
    #[test]
    fn test_green_ticket_4() {
        assert_eq!(green_ticket(2, 1, 1), green_ticket_solution(2, 1, 1))
    }
    #[test]
    fn test_green_ticket_5() {
        assert_eq!(green_ticket(1, 2, 1), green_ticket_solution(1, 2, 1))
    }
    #[test]
    fn test_green_ticket_6() {
        assert_eq!(green_ticket(3, 2, 1), green_ticket_solution(3, 2, 1))
    }
    #[test]
    fn test_green_ticket_7() {
        assert_eq!(green_ticket(0, 0, 0), green_ticket_solution(0, 0, 0))
    }
    #[test]
    fn test_green_ticket_8() {
        assert_eq!(green_ticket(2, 0, 0), green_ticket_solution(2, 0, 0))
    }
    #[test]
    fn test_green_ticket_9() {
        assert_eq!(green_ticket(0, 9, 10), green_ticket_solution(0, 9, 10))
    }
    #[test]
    fn test_green_ticket_10() {
        assert_eq!(green_ticket(0, 10, 0), green_ticket_solution(0, 10, 0))
    }
    #[test]
    fn test_green_ticket_11() {
        assert_eq!(green_ticket(9, 9, 9), green_ticket_solution(9, 9, 9))
    }
    #[test]
    fn test_green_ticket_12() {
        assert_eq!(green_ticket(9, 0, 9), green_ticket_solution(9, 0, 9))
    }
}

#[cfg(test)]
mod test_blue_ticket {
    use super::implementations::blue_ticket;
    use super::solutions::blue_ticket_solution;

    #[test]
    fn test_blue_ticket_1() {
        assert_eq!(blue_ticket(9, 1, 0), blue_ticket_solution(9, 1, 0))
    }
    #[test]
    fn test_blue_ticket_2() {
        assert_eq!(blue_ticket(9, 2, 0), blue_ticket_solution(9, 2, 0))
    }
    #[test]
    fn test_blue_ticket_3() {
        assert_eq!(blue_ticket(6, 1, 4), blue_ticket_solution(6, 1, 4))
    }
    #[test]
    fn test_blue_ticket_4() {
        assert_eq!(blue_ticket(6, 1, 5), blue_ticket_solution(6, 1, 5))
    }
    #[test]
    fn test_blue_ticket_5() {
        assert_eq!(blue_ticket(10, 0, 0), blue_ticket_solution(10, 0, 0))
    }
    #[test]
    fn test_blue_ticket_6() {
        assert_eq!(blue_ticket(15, 0, 5), blue_ticket_solution(15, 0, 5))
    }
    #[test]
    fn test_blue_ticket_7() {
        assert_eq!(blue_ticket(5, 15, 5), blue_ticket_solution(5, 15, 5))
    }
    #[test]
    fn test_blue_ticket_8() {
        assert_eq!(blue_ticket(4, 11, 1), blue_ticket_solution(4, 11, 1))
    }
    #[test]
    fn test_blue_ticket_9() {
        assert_eq!(blue_ticket(13, 2, 3), blue_ticket_solution(13, 2, 3))
    }
    #[test]
    fn test_blue_ticket_10() {
        assert_eq!(blue_ticket(8, 4, 3), blue_ticket_solution(8, 4, 3))
    }
    #[test]
    fn test_blue_ticket_11() {
        assert_eq!(blue_ticket(8, 4, 2), blue_ticket_solution(8, 4, 2))
    }
    #[test]
    fn test_blue_ticket_12() {
        assert_eq!(blue_ticket(8, 4, 1), blue_ticket_solution(8, 4, 1))
    }
}

#[cfg(test)]
mod test_share_digit {
    use super::implementations::share_digit;
    use super::solutions::share_digit_solution;

    #[test]
    fn test_share_digit_1() {
        assert_eq!(share_digit(12, 23), share_digit_solution(12, 23))
    }
    #[test]
    fn test_share_digit_2() {
        assert_eq!(share_digit(12, 43), share_digit_solution(12, 43))
    }
    #[test]
    fn test_share_digit_3() {
        assert_eq!(share_digit(12, 44), share_digit_solution(12, 44))
    }
    #[test]
    fn test_share_digit_4() {
        assert_eq!(share_digit(23, 12), share_digit_solution(23, 12))
    }
    #[test]
    fn test_share_digit_5() {
        assert_eq!(share_digit(23, 39), share_digit_solution(23, 39))
    }
    #[test]
    fn test_share_digit_6() {
        assert_eq!(share_digit(23, 19), share_digit_solution(23, 19))
    }
    #[test]
    fn test_share_digit_7() {
        assert_eq!(share_digit(30, 90), share_digit_solution(30, 90))
    }
    #[test]
    fn test_share_digit_8() {
        assert_eq!(share_digit(30, 91), share_digit_solution(30, 91))
    }
    #[test]
    fn test_share_digit_9() {
        assert_eq!(share_digit(55, 55), share_digit_solution(55, 55))
    }
    #[test]
    fn test_share_digit_10() {
        assert_eq!(share_digit(55, 44), share_digit_solution(55, 44))
    }
}

#[cfg(test)]
mod test_sum_limit {
    use super::implementations::sum_limit;
    use super::solutions::sum_limit_solution;

    #[test]
    fn test_sum_limit_1() {
        assert_eq!(sum_limit(2, 3), sum_limit_solution(2, 3))
    }
    #[test]
    fn test_sum_limit_2() {
        assert_eq!(sum_limit(8, 3), sum_limit_solution(8, 3))
    }
    #[test]
    fn test_sum_limit_3() {
        assert_eq!(sum_limit(8, 1), sum_limit_solution(8, 1))
    }
    #[test]
    fn test_sum_limit_4() {
        assert_eq!(sum_limit(11, 39), sum_limit_solution(11, 39))
    }
    #[test]
    fn test_sum_limit_5() {
        assert_eq!(sum_limit(11, 99), sum_limit_solution(11, 99))
    }
    #[test]
    fn test_sum_limit_6() {
        assert_eq!(sum_limit(0, 0), sum_limit_solution(0, 0))
    }
    #[test]
    fn test_sum_limit_7() {
        assert_eq!(sum_limit(99, 0), sum_limit_solution(99, 0))
    }
    #[test]
    fn test_sum_limit_8() {
        assert_eq!(sum_limit(99, 1), sum_limit_solution(99, 1))
    }
    #[test]
    fn test_sum_limit_9() {
        assert_eq!(sum_limit(123, 1), sum_limit_solution(123, 1))
    }
    #[test]
    fn test_sum_limit_10() {
        assert_eq!(sum_limit(1, 123), sum_limit_solution(1, 123))
    }
    #[test]
    fn test_sum_limit_11() {
        assert_eq!(sum_limit(23, 60), sum_limit_solution(23, 60))
    }
    #[test]
    fn test_sum_limit_12() {
        assert_eq!(sum_limit(23, 80), sum_limit_solution(23, 80))
    }
    #[test]
    fn test_sum_limit_13() {
        assert_eq!(sum_limit(9000, 1), sum_limit_solution(9000, 1))
    }
    #[test]
    fn test_sum_limit_14() {
        assert_eq!(sum_limit(90000000, 1), sum_limit_solution(90000000, 1))
    }
    #[test]
    fn test_sum_limit_15() {
        assert_eq!(sum_limit(9000, 1000), sum_limit_solution(9000, 1000))
    }
}

#[cfg(test)]
mod test_make_bricks {
    use super::implementations::make_bricks;
    use super::solutions::make_bricks_solution;

    #[test]
    fn test_make_bricks_1() {
        assert_eq!(make_bricks(3, 1, 8), make_bricks_solution(3, 1, 8))
    }
    #[test]
    fn test_make_bricks_2() {
        assert_eq!(make_bricks(3, 1, 9), make_bricks_solution(3, 1, 9))
    }
    #[test]
    fn test_make_bricks_3() {
        assert_eq!(make_bricks(3, 2, 10), make_bricks_solution(3, 2, 10))
    }
    #[test]
    fn test_make_bricks_4() {
        assert_eq!(make_bricks(3, 2, 8), make_bricks_solution(3, 2, 8))
    }
    #[test]
    fn test_make_bricks_5() {
        assert_eq!(make_bricks(3, 2, 9), make_bricks_solution(3, 2, 9))
    }
    #[test]
    fn test_make_bricks_6() {
        assert_eq!(make_bricks(6, 1, 11), make_bricks_solution(6, 1, 11))
    }
    #[test]
    fn test_make_bricks_7() {
        assert_eq!(make_bricks(6, 0, 11), make_bricks_solution(6, 0, 11))
    }
    #[test]
    fn test_make_bricks_8() {
        assert_eq!(make_bricks(1, 4, 11), make_bricks_solution(1, 4, 11))
    }
    #[test]
    fn test_make_bricks_9() {
        assert_eq!(make_bricks(0, 3, 10), make_bricks_solution(0, 3, 10))
    }
    #[test]
    fn test_make_bricks_10() {
        assert_eq!(make_bricks(1, 4, 12), make_bricks_solution(1, 4, 12))
    }
    #[test]
    fn test_make_bricks_11() {
        assert_eq!(make_bricks(3, 1, 7), make_bricks_solution(3, 1, 7))
    }
    #[test]
    fn test_make_bricks_12() {
        assert_eq!(make_bricks(1, 1, 7), make_bricks_solution(1, 1, 7))
    }
    #[test]
    fn test_make_bricks_13() {
        assert_eq!(make_bricks(2, 1, 7), make_bricks_solution(2, 1, 7))
    }
    #[test]
    fn test_make_bricks_14() {
        assert_eq!(make_bricks(7, 1, 11), make_bricks_solution(7, 1, 11))
    }
    #[test]
    fn test_make_bricks_15() {
        assert_eq!(make_bricks(7, 1, 8), make_bricks_solution(7, 1, 8))
    }
    #[test]
    fn test_make_bricks_16() {
        assert_eq!(make_bricks(7, 1, 13), make_bricks_solution(7, 1, 13))
    }
    #[test]
    fn test_make_bricks_17() {
        assert_eq!(make_bricks(43, 1, 46), make_bricks_solution(43, 1, 46))
    }
    #[test]
    fn test_make_bricks_18() {
        assert_eq!(make_bricks(40, 1, 46), make_bricks_solution(40, 1, 46))
    }
    #[test]
    fn test_make_bricks_19() {
        assert_eq!(make_bricks(40, 2, 47), make_bricks_solution(40, 2, 47))
    }
    #[test]
    fn test_make_bricks_20() {
        assert_eq!(make_bricks(40, 2, 50), make_bricks_solution(40, 2, 50))
    }
    #[test]
    fn test_make_bricks_21() {
        assert_eq!(make_bricks(40, 2, 52), make_bricks_solution(40, 2, 52))
    }
    #[test]
    fn test_make_bricks_22() {
        assert_eq!(make_bricks(22, 2, 33), make_bricks_solution(22, 2, 33))
    }
    #[test]
    fn test_make_bricks_23() {
        assert_eq!(make_bricks(0, 2, 10), make_bricks_solution(0, 2, 10))
    }
    #[test]
    fn test_make_bricks_24() {
        assert_eq!(make_bricks(1000000, 1000, 1000100), make_bricks_solution(1000000, 1000, 1000100))
    }
    #[test]
    fn test_make_bricks_25() {
        assert_eq!(make_bricks(2, 1000000, 100003), make_bricks_solution(2, 1000000, 100003))
    }
    #[test]
    fn test_make_bricks_26() {
        assert_eq!(make_bricks(20, 0, 19), make_bricks_solution(20, 0, 19))
    }
    #[test]
    fn test_make_bricks_27() {
        assert_eq!(make_bricks(20, 0, 21), make_bricks_solution(20, 0, 21))
    }
    #[test]
    fn test_make_bricks_28() {
        assert_eq!(make_bricks(20, 4, 51), make_bricks_solution(20, 4, 51))
    }
    #[test]
    fn test_make_bricks_29() {
        assert_eq!(make_bricks(20, 4, 39), make_bricks_solution(20, 4, 39))
    }
}

#[cfg(test)]
mod test_lone_sum {
    use super::implementations::lone_sum;
    use super::solutions::lone_sum_solution;

    #[test]
    fn test_lone_sum_1() {
        assert_eq!(lone_sum(1, 2, 3), lone_sum_solution(1, 2, 3))
    }
    #[test]
    fn test_lone_sum_2() {
        assert_eq!(lone_sum(3, 2, 3), lone_sum_solution(3, 2, 3))
    }
    #[test]
    fn test_lone_sum_3() {
        assert_eq!(lone_sum(3, 3, 3), lone_sum_solution(3, 3, 3))
    }
    #[test]
    fn test_lone_sum_4() {
        assert_eq!(lone_sum(9, 2, 2), lone_sum_solution(9, 2, 2))
    }
    #[test]
    fn test_lone_sum_5() {
        assert_eq!(lone_sum(2, 2, 9), lone_sum_solution(2, 2, 9))
    }
    #[test]
    fn test_lone_sum_6() {
        assert_eq!(lone_sum(2, 9, 2), lone_sum_solution(2, 9, 2))
    }
    #[test]
    fn test_lone_sum_7() {
        assert_eq!(lone_sum(2, 9, 3), lone_sum_solution(2, 9, 3))
    }
    #[test]
    fn test_lone_sum_8() {
        assert_eq!(lone_sum(4, 2, 3), lone_sum_solution(4, 2, 3))
    }
    #[test]
    fn test_lone_sum_9() {
        assert_eq!(lone_sum(1, 3, 1), lone_sum_solution(1, 3, 1))
    }
}

#[cfg(test)]
mod test_lucky_sum {
    use super::implementations::lucky_sum;
    use super::solutions::lucky_sum_solution;

    #[test]
    fn test_lucky_sum_1() {
        assert_eq!(lucky_sum(1, 2, 3), lucky_sum_solution(1, 2, 3))
    }
    #[test]
    fn test_lucky_sum_2() {
        assert_eq!(lucky_sum(1, 2, 13), lucky_sum_solution(1, 2, 13))
    }
    #[test]
    fn test_lucky_sum_3() {
        assert_eq!(lucky_sum(1, 13, 3), lucky_sum_solution(1, 13, 3))
    }
    #[test]
    fn test_lucky_sum_4() {
        assert_eq!(lucky_sum(1, 13, 13), lucky_sum_solution(1, 13, 13))
    }
    #[test]
    fn test_lucky_sum_5() {
        assert_eq!(lucky_sum(6, 5, 2), lucky_sum_solution(6, 5, 2))
    }
    #[test]
    fn test_lucky_sum_6() {
        assert_eq!(lucky_sum(13, 2, 3), lucky_sum_solution(13, 2, 3))
    }
    #[test]
    fn test_lucky_sum_7() {
        assert_eq!(lucky_sum(13, 2, 13), lucky_sum_solution(13, 2, 13))
    }
    #[test]
    fn test_lucky_sum_8() {
        assert_eq!(lucky_sum(13, 13, 2), lucky_sum_solution(13, 13, 2))
    }
    #[test]
    fn test_lucky_sum_9() {
        assert_eq!(lucky_sum(9, 4, 13), lucky_sum_solution(9, 4, 13))
    }
    #[test]
    fn test_lucky_sum_10() {
        assert_eq!(lucky_sum(8, 13, 2), lucky_sum_solution(8, 13, 2))
    }
    #[test]
    fn test_lucky_sum_11() {
        assert_eq!(lucky_sum(7, 2, 1), lucky_sum_solution(7, 2, 1))
    }
    #[test]
    fn test_lucky_sum_12() {
        assert_eq!(lucky_sum(3, 3, 13), lucky_sum_solution(3, 3, 13))
    }
}

#[cfg(test)]
mod test_no_teen_sum {
    use super::implementations::no_teen_sum;
    use super::solutions::no_teen_sum_solution;

    #[test]
    fn test_no_teen_sum_1() {
        assert_eq!(no_teen_sum(1, 2, 3), no_teen_sum_solution(1, 2, 3))
    }
    #[test]
    fn test_no_teen_sum_2() {
        assert_eq!(no_teen_sum(2, 13, 1), no_teen_sum_solution(2, 13, 1))
    }
    #[test]
    fn test_no_teen_sum_3() {
        assert_eq!(no_teen_sum(2, 1, 14), no_teen_sum_solution(2, 1, 14))
    }
    #[test]
    fn test_no_teen_sum_4() {
        assert_eq!(no_teen_sum(2, 1, 15), no_teen_sum_solution(2, 1, 15))
    }
    #[test]
    fn test_no_teen_sum_5() {
        assert_eq!(no_teen_sum(2, 1, 16), no_teen_sum_solution(2, 1, 16))
    }
    #[test]
    fn test_no_teen_sum_6() {
        assert_eq!(no_teen_sum(2, 1, 17), no_teen_sum_solution(2, 1, 17))
    }
    #[test]
    fn test_no_teen_sum_7() {
        assert_eq!(no_teen_sum(17, 1, 2), no_teen_sum_solution(17, 1, 2))
    }
    #[test]
    fn test_no_teen_sum_8() {
        assert_eq!(no_teen_sum(2, 15, 2), no_teen_sum_solution(2, 15, 2))
    }
    #[test]
    fn test_no_teen_sum_9() {
        assert_eq!(no_teen_sum(16, 17, 18), no_teen_sum_solution(16, 17, 18))
    }
    #[test]
    fn test_no_teen_sum_10() {
        assert_eq!(no_teen_sum(17, 18, 19), no_teen_sum_solution(17, 18, 19))
    }
    #[test]
    fn test_no_teen_sum_11() {
        assert_eq!(no_teen_sum(15, 16, 1), no_teen_sum_solution(15, 16, 1))
    }
    #[test]
    fn test_no_teen_sum_12() {
        assert_eq!(no_teen_sum(15, 15, 19), no_teen_sum_solution(15, 15, 19))
    }
    #[test]
    fn test_no_teen_sum_13() {
        assert_eq!(no_teen_sum(15, 19, 16), no_teen_sum_solution(15, 19, 16))
    }
    #[test]
    fn test_no_teen_sum_14() {
        assert_eq!(no_teen_sum(5, 17, 18), no_teen_sum_solution(5, 17, 18))
    }
    #[test]
    fn test_no_teen_sum_15() {
        assert_eq!(no_teen_sum(17, 18, 16), no_teen_sum_solution(17, 18, 16))
    }
    #[test]
    fn test_no_teen_sum_16() {
        assert_eq!(no_teen_sum(17, 19, 18), no_teen_sum_solution(17, 19, 18))
    }
}

#[cfg(test)]
mod test_round_sum {
    use super::implementations::round_sum;
    use super::solutions::round_sum_solution;

    #[test]
    fn test_round_sum_1() {
        assert_eq!(round_sum(16, 17, 18), round_sum_solution(16, 17, 18))
    }
    #[test]
    fn test_round_sum_2() {
        assert_eq!(round_sum(12, 13, 14), round_sum_solution(12, 13, 14))
    }
    #[test]
    fn test_round_sum_3() {
        assert_eq!(round_sum(6, 4, 4), round_sum_solution(6, 4, 4))
    }
    #[test]
    fn test_round_sum_4() {
        assert_eq!(round_sum(4, 6, 5), round_sum_solution(4, 6, 5))
    }
    #[test]
    fn test_round_sum_5() {
        assert_eq!(round_sum(4, 4, 6), round_sum_solution(4, 4, 6))
    }
    #[test]
    fn test_round_sum_6() {
        assert_eq!(round_sum(9, 4, 4), round_sum_solution(9, 4, 4))
    }
    #[test]
    fn test_round_sum_7() {
        assert_eq!(round_sum(0, 0, 1), round_sum_solution(0, 0, 1))
    }
    #[test]
    fn test_round_sum_8() {
        assert_eq!(round_sum(0, 9, 0), round_sum_solution(0, 9, 0))
    }
    #[test]
    fn test_round_sum_9() {
        assert_eq!(round_sum(10, 10, 19), round_sum_solution(10, 10, 19))
    }
    #[test]
    fn test_round_sum_10() {
        assert_eq!(round_sum(20, 30, 40), round_sum_solution(20, 30, 40))
    }
    #[test]
    fn test_round_sum_11() {
        assert_eq!(round_sum(45, 21, 30), round_sum_solution(45, 21, 30))
    }
    #[test]
    fn test_round_sum_12() {
        assert_eq!(round_sum(23, 11, 26), round_sum_solution(23, 11, 26))
    }
    #[test]
    fn test_round_sum_13() {
        assert_eq!(round_sum(23, 24, 25), round_sum_solution(23, 24, 25))
    }
    #[test]
    fn test_round_sum_14() {
        assert_eq!(round_sum(25, 24, 25), round_sum_solution(25, 24, 25))
    }
    #[test]
    fn test_round_sum_15() {
        assert_eq!(round_sum(23, 24, 29), round_sum_solution(23, 24, 29))
    }
    #[test]
    fn test_round_sum_16() {
        assert_eq!(round_sum(11, 24, 36), round_sum_solution(11, 24, 36))
    }
    #[test]
    fn test_round_sum_17() {
        assert_eq!(round_sum(24, 36, 32), round_sum_solution(24, 36, 32))
    }
    #[test]
    fn test_round_sum_18() {
        assert_eq!(round_sum(14, 12, 26), round_sum_solution(14, 12, 26))
    }
    #[test]
    fn test_round_sum_19() {
        assert_eq!(round_sum(12, 10, 24), round_sum_solution(12, 10, 24))
    }
}

#[cfg(test)]
mod test_close_far {
    use super::implementations::close_far;
    use super::solutions::close_far_solution;

    #[test]
    fn test_close_far_1() {
        assert_eq!(close_far(1, 2, 10), close_far_solution(1, 2, 10))
    }
    #[test]
    fn test_close_far_2() {
        assert_eq!(close_far(1, 2, 3), close_far_solution(1, 2, 3))
    }
    #[test]
    fn test_close_far_3() {
        assert_eq!(close_far(4, 1, 3), close_far_solution(4, 1, 3))
    }
    #[test]
    fn test_close_far_4() {
        assert_eq!(close_far(4, 5, 3), close_far_solution(4, 5, 3))
    }
    #[test]
    fn test_close_far_5() {
        assert_eq!(close_far(4, 3, 5), close_far_solution(4, 3, 5))
    }
    #[test]
    fn test_close_far_6() {
        assert_eq!(close_far(-1, 10, 0), close_far_solution(-1, 10, 0))
    }
    #[test]
    fn test_close_far_7() {
        assert_eq!(close_far(0, -1, 10), close_far_solution(0, -1, 10))
    }
    #[test]
    fn test_close_far_8() {
        assert_eq!(close_far(10, 10, 8), close_far_solution(10, 10, 8))
    }
    #[test]
    fn test_close_far_9() {
        assert_eq!(close_far(10, 8, 9), close_far_solution(10, 8, 9))
    }
    #[test]
    fn test_close_far_10() {
        assert_eq!(close_far(8, 9, 10), close_far_solution(8, 9, 10))
    }
    #[test]
    fn test_close_far_11() {
        assert_eq!(close_far(8, 9, 7), close_far_solution(8, 9, 7))
    }
    #[test]
    fn test_close_far_12() {
        assert_eq!(close_far(8, 6, 9), close_far_solution(8, 6, 9))
    }
}

#[cfg(test)]
mod test_blackjack {
    use super::implementations::blackjack;
    use super::solutions::blackjack_solution;

    #[test]
    fn test_blackjack_1() {
        assert_eq!(blackjack(19, 21), blackjack_solution(19, 21))
    }
    #[test]
    fn test_blackjack_2() {
        assert_eq!(blackjack(21, 19), blackjack_solution(21, 19))
    }
    #[test]
    fn test_blackjack_3() {
        assert_eq!(blackjack(19, 22), blackjack_solution(19, 22))
    }
    #[test]
    fn test_blackjack_4() {
        assert_eq!(blackjack(22, 19), blackjack_solution(22, 19))
    }
    #[test]
    fn test_blackjack_5() {
        assert_eq!(blackjack(22, 50), blackjack_solution(22, 50))
    }
    #[test]
    fn test_blackjack_6() {
        assert_eq!(blackjack(22, 22), blackjack_solution(22, 22))
    }
    #[test]
    fn test_blackjack_7() {
        assert_eq!(blackjack(33, 1), blackjack_solution(33, 1))
    }
    #[test]
    fn test_blackjack_8() {
        assert_eq!(blackjack(1, 2), blackjack_solution(1, 2))
    }
    #[test]
    fn test_blackjack_9() {
        assert_eq!(blackjack(34, 33), blackjack_solution(34, 33))
    }
    #[test]
    fn test_blackjack_10() {
        assert_eq!(blackjack(17, 19), blackjack_solution(17, 19))
    }
    #[test]
    fn test_blackjack_11() {
        assert_eq!(blackjack(18, 17), blackjack_solution(18, 17))
    }
    #[test]
    fn test_blackjack_12() {
        assert_eq!(blackjack(16, 23), blackjack_solution(16, 23))
    }
    #[test]
    fn test_blackjack_13() {
        assert_eq!(blackjack(3, 4), blackjack_solution(3, 4))
    }
    #[test]
    fn test_blackjack_14() {
        assert_eq!(blackjack(3, 2), blackjack_solution(3, 2))
    }
    #[test]
    fn test_blackjack_15() {
        assert_eq!(blackjack(21, 20), blackjack_solution(21, 20))
    }
}

#[cfg(test)]
mod test_evenly_spaced {
    use super::implementations::evenly_spaced;
    use super::solutions::evenly_spaced_solution;

    #[test]
    fn test_evenly_spaced_1() {
        assert_eq!(evenly_spaced(2, 4, 6), evenly_spaced_solution(2, 4, 6))
    }
    #[test]
    fn test_evenly_spaced_2() {
        assert_eq!(evenly_spaced(4, 6, 2), evenly_spaced_solution(4, 6, 2))
    }
    #[test]
    fn test_evenly_spaced_3() {
        assert_eq!(evenly_spaced(4, 6, 3), evenly_spaced_solution(4, 6, 3))
    }
    #[test]
    fn test_evenly_spaced_4() {
        assert_eq!(evenly_spaced(6, 2, 4), evenly_spaced_solution(6, 2, 4))
    }
    #[test]
    fn test_evenly_spaced_5() {
        assert_eq!(evenly_spaced(6, 2, 8), evenly_spaced_solution(6, 2, 8))
    }
    #[test]
    fn test_evenly_spaced_6() {
        assert_eq!(evenly_spaced(2, 2, 2), evenly_spaced_solution(2, 2, 2))
    }
    #[test]
    fn test_evenly_spaced_7() {
        assert_eq!(evenly_spaced(2, 2, 3), evenly_spaced_solution(2, 2, 3))
    }
    #[test]
    fn test_evenly_spaced_8() {
        assert_eq!(evenly_spaced(9, 10, 11), evenly_spaced_solution(9, 10, 11))
    }
    #[test]
    fn test_evenly_spaced_9() {
        assert_eq!(evenly_spaced(10, 9, 11), evenly_spaced_solution(10, 9, 11))
    }
    #[test]
    fn test_evenly_spaced_10() {
        assert_eq!(evenly_spaced(10, 9, 9), evenly_spaced_solution(10, 9, 9))
    }
    #[test]
    fn test_evenly_spaced_11() {
        assert_eq!(evenly_spaced(2, 4, 4), evenly_spaced_solution(2, 4, 4))
    }
    #[test]
    fn test_evenly_spaced_12() {
        assert_eq!(evenly_spaced(2, 2, 4), evenly_spaced_solution(2, 2, 4))
    }
    #[test]
    fn test_evenly_spaced_13() {
        assert_eq!(evenly_spaced(3, 6, 12), evenly_spaced_solution(3, 6, 12))
    }
    #[test]
    fn test_evenly_spaced_14() {
        assert_eq!(evenly_spaced(12, 3, 6), evenly_spaced_solution(12, 3, 6))
    }
}

#[cfg(test)]
mod test_make_chocolate {
    use super::implementations::make_chocolate;
    use super::solutions::make_chocolate_solution;

    #[test]
    fn test_make_chocolate_1() {
        assert_eq!(make_chocolate(4, 1, 9), make_chocolate_solution(4, 1, 9))
    }
    #[test]
    fn test_make_chocolate_2() {
        assert_eq!(make_chocolate(4, 1, 10), make_chocolate_solution(4, 1, 10))
    }
    #[test]
    fn test_make_chocolate_3() {
        assert_eq!(make_chocolate(4, 1, 7), make_chocolate_solution(4, 1, 7))
    }
    #[test]
    fn test_make_chocolate_4() {
        assert_eq!(make_chocolate(6, 2, 7), make_chocolate_solution(6, 2, 7))
    }
    #[test]
    fn test_make_chocolate_5() {
        assert_eq!(make_chocolate(4, 1, 5), make_chocolate_solution(4, 1, 5))
    }
    #[test]
    fn test_make_chocolate_6() {
        assert_eq!(make_chocolate(4, 1, 4), make_chocolate_solution(4, 1, 4))
    }
    #[test]
    fn test_make_chocolate_7() {
        assert_eq!(make_chocolate(5, 4, 9), make_chocolate_solution(5, 4, 9))
    }
    #[test]
    fn test_make_chocolate_8() {
        assert_eq!(make_chocolate(9, 3, 18), make_chocolate_solution(9, 3, 18))
    }
    #[test]
    fn test_make_chocolate_9() {
        assert_eq!(make_chocolate(3, 1, 9), make_chocolate_solution(3, 1, 9))
    }
    #[test]
    fn test_make_chocolate_10() {
        assert_eq!(make_chocolate(1, 2, 7), make_chocolate_solution(1, 2, 7))
    }
    #[test]
    fn test_make_chocolate_11() {
        assert_eq!(make_chocolate(1, 2, 6), make_chocolate_solution(1, 2, 6))
    }
    #[test]
    fn test_make_chocolate_12() {
        assert_eq!(make_chocolate(1, 2, 5), make_chocolate_solution(1, 2, 5))
    }
    #[test]
    fn test_make_chocolate_13() {
        assert_eq!(make_chocolate(6, 1, 10), make_chocolate_solution(6, 1, 10))
    }
    #[test]
    fn test_make_chocolate_14() {
        assert_eq!(make_chocolate(6, 1, 11), make_chocolate_solution(6, 1, 11))
    }
    #[test]
    fn test_make_chocolate_15() {
        assert_eq!(make_chocolate(6, 1, 12), make_chocolate_solution(6, 1, 12))
    }
    #[test]
    fn test_make_chocolate_16() {
        assert_eq!(make_chocolate(6, 1, 13), make_chocolate_solution(6, 1, 13))
    }
    #[test]
    fn test_make_chocolate_17() {
        assert_eq!(make_chocolate(6, 2, 10), make_chocolate_solution(6, 2, 10))
    }
    #[test]
    fn test_make_chocolate_18() {
        assert_eq!(make_chocolate(6, 2, 11), make_chocolate_solution(6, 2, 11))
    }
    #[test]
    fn test_make_chocolate_19() {
        assert_eq!(make_chocolate(6, 2, 12), make_chocolate_solution(6, 2, 12))
    }
    #[test]
    fn test_make_chocolate_20() {
        assert_eq!(make_chocolate(60, 100, 550), make_chocolate_solution(60, 100, 550))
    }
    #[test]
    fn test_make_chocolate_21() {
        assert_eq!(make_chocolate(1000, 1000000, 5000006), make_chocolate_solution(1000, 1000000, 5000006))
    }
    #[test]
    fn test_make_chocolate_22() {
        assert_eq!(make_chocolate(7, 1, 12), make_chocolate_solution(7, 1, 12))
    }
    #[test]
    fn test_make_chocolate_23() {
        assert_eq!(make_chocolate(7, 1, 13), make_chocolate_solution(7, 1, 13))
    }
    #[test]
    fn test_make_chocolate_24() {
        assert_eq!(make_chocolate(7, 2, 13), make_chocolate_solution(7, 2, 13))
    }
}
