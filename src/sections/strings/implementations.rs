/// ## String-1 -- helloName
/// Given a string name, e.g. "Bob", return a greeting of the form "Hello Bob!".
/// ### Examples
///    hello_name("Bob") -> "Hello Bob!"
///    hello_name("Alice") -> "Hello Alice!"
///    hello_name("X") -> "Hello X!"
#[allow(unused)]
pub fn hello_name(name: impl Into<String>) -> String {
    let name: String = name.into();
    todo!()
}

/// ## String-1 -- makeAbba
/// Given two strings, a and b, return the result of putting them together in
/// the order abba, e.g. "Hi" and "Bye" returns "HiByeByeHi".
/// ### Examples
///    make_abba("Hi", "Bye") -> "HiByeByeHi"
///    make_abba("Yo", "Alice") -> "YoAliceAliceYo"
///    make_abba("What", "Up") -> "WhatUpUpWhat"
#[allow(unused)]
pub fn make_abba(a: impl Into<String>, b: impl Into<String>) -> String {
    let a: String = a.into();
    let b: String = b.into();
    todo!()
}

/// ## String-1 -- makeTags
/// The web is built with HTML strings like "<i>Yay</i>" which draws Yay as
/// italic text. In this example, the "i" tag makes <i> and </i> which surround
/// the word "Yay". Given tag and word strings, create the HTML string with tags
/// around the word, e.g. "<i>Yay</i>".
/// ### Examples
///    make_tags("i", "Yay") -> "<i>Yay</i>"
///    make_tags("i", "Hello") -> "<i>Hello</i>"
///    make_tags("cite", "Yay") -> "<cite>Yay</cite>"
#[allow(unused)]
pub fn make_tags(tag: impl Into<String>, word: impl Into<String>) -> String {
    let tag: String = tag.into();
    let word: String = word.into();
    todo!()
}

/// ## String-1 -- makeOutWord
/// Given an "out" string length 4, such as "<<>>", and a word, return a new
/// string where the word is in the middle of the out string, e.g. "<<word>>".
/// Note: use str.substring(i, j) to extract the String starting at index i and
/// going up to but not including index j.
/// ### Examples
///    make_out_word("<<>>", "Yay") -> "<<Yay>>"
///    make_out_word("<<>>", "WooHoo") -> "<<WooHoo>>"
///    make_out_word("[[]]", "word") -> "[[word]]"
#[allow(unused)]
pub fn make_out_word(out: impl Into<String>, word: impl Into<String>) -> String {
    let out: String = out.into();
    let word: String = word.into();
    todo!()
}

/// ## String-1 -- extraEnd
/// Given a string, return a new string made of 3 copies of the last 2 chars of
/// the original string. The string length will be at least 2.
/// ### Examples
///    extra_end("Hello") -> "lololo"
///    extra_end("ab") -> "ababab"
///    extra_end("Hi") -> "HiHiHi"
#[allow(unused)]
pub fn extra_end(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## String-1 -- firstTwo
/// Given a string, return the string made of its first two chars, so the String
/// "Hello" yields "He". If the string is shorter than length 2, return whatever
/// there is, so "X" yields "X", and the empty string "" yields the empty string
/// "". Note that str.length() returns the length of a string.
/// ### Examples
///    first_two("Hello") -> "He"
///    first_two("abcdefg") -> "ab"
///    first_two("ab") -> "ab"
#[allow(unused)]
pub fn first_two(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## String-1 -- firstHalf
/// Given a string of even length, return the first half. So the string "WooHoo"
/// yields "Woo".
/// ### Examples
///    first_half("WooHoo") -> "WooHoo"
///    first_half("HelloThere") -> "HelloThere"
///    first_half("abcdefg") -> "abcdefg"
#[allow(unused)]
pub fn first_half(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## String-1 -- withoutEnd
/// Given a string, return a version without the first and last char, so "Hello"
/// yields "ell". The string length will be at least 2.
/// ### Examples
///    without_end("Hello") -> "ell"
///    without_end("java") -> "av"
///    without_end("coding") -> "odin"
#[allow(unused)]
pub fn without_end(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## String-1 -- comboString
/// Given 2 strings, a and b, return a string of the form short+long+short, with
/// the shorter string on the outside and the longer string on the inside. The
/// strings will not be the same length, but they may be empty (length 0).
/// ### Examples
///    combo_string("Hello", "hi") -> "hiHellohi"
///    combo_string("Hi", "Hello") -> "HiHelloHi"
///    combo_string("aaa", "b") -> "baaab"
#[allow(unused)]
pub fn combo_string(a: impl Into<String>, b: impl Into<String>) -> String {
    let a: String = a.into();
    let b: String = b.into();
    todo!()
}

/// ## String-1 -- nonStart
/// Given 2 strings, return their concatenation, except omit the first char of
/// each. The strings will be at least length 1.
/// ### Examples
///    non_start("Hello", "There") -> "ellohere"
///    non_start("java", "code") -> "avaode"
///    non_start("shotl", "java") -> "hotlava"
#[allow(unused)]
pub fn non_start(a: impl Into<String>, b: impl Into<String>) -> String {
    let a: String = a.into();
    let b: String = b.into();
    todo!()
}

/// ## String-1 -- left2
/// Given a string, return a "rotated left 2" version where the first 2 chars
/// are moved to the end. The string length will be at least 2.
/// ### Examples
///    left2("Hello") -> "lloHe"
///    left2("java") -> "vaja"
///    left2("Hi") -> "Hi"
#[allow(unused)]
pub fn left2(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## String-1 -- right2
/// Given a string, return a "rotated right 2" version where the last 2 chars
/// are moved to the start. The string length will be at least 2.
/// ### Examples
///    right2("Hello") -> "loHel"
///    right2("java") -> "vaja"
///    right2("Hi") -> "Hi"
#[allow(unused)]
pub fn right2(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## String-1 -- theEnd
/// Given a string, return a string length 1 from its front, unless front is
/// false, in which case return a string length 1 from its back. The string will
/// be non-empty.
/// ### Examples
///    the_end("Hello", true) -> "H"
///    the_end("Hello", false) -> "o"
///    the_end("oh", true) -> "o"
#[allow(unused)]
pub fn the_end(str: impl Into<String>, front: bool) -> String {
    let str: String = str.into();
    todo!()
}

/// ## String-1 -- withoutEnd2
/// Given a string, return a version without both the first and last char of the
/// string. The string may be any length, including 0.
/// ### Examples
///    without_end2("Hello") -> "ell"
///    without_end2("abc") -> "b"
///    without_end2("ab") -> ""
#[allow(unused)]
pub fn without_end2(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## String-1 -- middleTwo
/// Given a string of even length, return a string made of the middle two chars,
/// so the string "string" yields "ri". The string length will be at least 2.
/// ### Examples
///    middle_two("string") -> ""
///    middle_two("code") -> ""
///    middle_two("Practice") -> ""
#[allow(unused)]
pub fn middle_two(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## String-1 -- endsLy
/// Given a string, return true if it ends in "ly".
/// ### Examples
///    ends_ly("oddly") -> true
///    ends_ly("y") -> false
///    ends_ly("oddl") -> false
#[allow(unused)]
pub fn ends_ly(str: impl Into<String>) -> bool {
    let str: String = str.into();
    todo!()
}

/// ## String-1 -- nTwice
/// Given a string and an int n, return a string made of the first and last n
/// chars from the string. The string length will be at least n.
/// ### Examples
///    n_twice("hello", 2) -> "helo"
///    n_twice("Chocolate", 3) -> "Choate"
///    n_twice("Chocolate", 1) -> "Ce"
#[allow(unused)]
pub fn n_twice(str: impl Into<String>, n: i32) -> String {
    let str: String = str.into();
    todo!()
}

/// ## String-1 -- twoChar
/// Given a string and an index, return a string length 2 starting at the given
/// index. If the index is too big or too small to define a string length 2, use
/// the first 2 chars. The string length will be at least 2.
/// ### Examples
///    two_char("java", 0) -> "ja"
///    two_char("java", 2) -> "va"
///    two_char("java", 3) -> "ja"
#[allow(unused)]
pub fn two_char(str: impl Into<String>, index: i32) -> String {
    let str: String = str.into();
    todo!()
}

/// ## String-1 -- middleThree
/// Given a string of odd length, return the string length 3 from its middle, so
/// "Candy" yields "and". The string length will be at least 3.
/// ### Examples
///    middle_three("Candy") -> "and"
///    middle_three("and") -> "and"
///    middle_three("solving") -> "lvi"
#[allow(unused)]
pub fn middle_three(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## String-1 -- hasBad
/// Given a string, return true if "bad" appears starting at index 0 or 1 in the
/// string, such as with "badxxx" or "xbadxx" but not "xxbadxx". The string may
/// be any length, including 0.
/// ### Examples
///    has_bad("badxx") -> true
///    has_bad("xbadxx") -> true
///    has_bad("xxbadxx") -> false
#[allow(unused)]
pub fn has_bad(str: impl Into<String>) -> bool {
    let str: String = str.into();
    todo!()
}

/// ## String-1 -- atFirst
/// Given a string, return a string length 2 made of its first 2 chars. If the
/// string length is less than 2, use '@' for the missing chars.
/// ### Examples
///    at_first("hello") -> "he"
///    at_first("hi") -> "hi"
///    at_first("h") -> "h@"
#[allow(unused)]
pub fn at_first(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## String-1 -- lastChars
/// Given 2 strings, a and b, return a new string made of the first char of a
/// and the last char of b, so "yo" and "java" yields "ya". If either string is
/// length 0, use '@' for its missing char.
/// ### Examples
///    last_chars("last", "chars") -> "ls"
///    last_chars("yo", "java") -> "ya"
///    last_chars("hi", "") -> "h@"
#[allow(unused)]
pub fn last_chars(a: impl Into<String>, b: impl Into<String>) -> String {
    todo!()
}

/// ## String-1 -- conCat
/// Given two strings, append them together (known as "concatenation") and
/// return the result. However, if the concatenation creates a double-char, then
/// omit one of the chars, so "abc" and "cat" yields "abcat".
/// ### Examples
///    con_cat("abc", "cat") -> "abcat"
///    con_cat("dog", "cat") -> "dogcat"
///    con_cat("abc", "") -> "abc"
#[allow(unused)]
pub fn con_cat(a: impl Into<String>, b: impl Into<String>) -> String {
    let a: String = a.into();
    let b: String = b.into();
    todo!()
}

/// ## String-1 -- lastTwo
/// Given a string of any length, return a new string where the last 2 chars, if
/// present, are swapped, so "coding" yields "codign".
/// ### Examples
///    last_two("coding") -> "codign"
///    last_two("cat") -> "cta"
///    last_two("ab") -> "ba"
#[allow(unused)]
pub fn last_two(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## String-1 -- seeColor
/// Given a string, if the string begins with "red" or "blue" return that color
/// string, otherwise return the empty string.
/// ### Examples
///    see_color("redxx") -> "red"
///    see_color("xxred") -> ""
///    see_color("blueTimes") -> "blue"
#[allow(unused)]
pub fn see_color(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## String-1 -- frontAgain
/// Given a string, return true if the first 2 chars in the string also appear
/// at the end of the string, such as with "edited".
/// ### Examples
///    front_again("edited") -> true
///    front_again("edit") -> false
///    front_again("ed") -> true
#[allow(unused)]
pub fn front_again(str: impl Into<String>) -> bool {
    let str: String = str.into();
    todo!()
}

/// ## String-1 -- minCat
/// Given two strings, append them together (known as "concatenation") and
/// return the result. However, if the strings are different lengths, omit chars
/// from the longer string so it is the same length as the shorter string. So
/// "Hello" and "Hi" yield "loHi". The strings may be any length.
/// ### Examples
///    min_cat("Hello", "Hi") -> "loHi"
///    min_cat("Hello", "java") -> "ellojava"
///    min_cat("java", "Hello") -> "javaello"
#[allow(unused)]
pub fn min_cat(a: impl Into<String>, b: impl Into<String>) -> String {
    let a: String = a.into();
    let b: String = b.into();
    todo!()
}

/// ## String-1 -- extraFront
/// Given a string, return a new string made of 3 copies of the first 2 chars of
/// the original string. The string may be any length. If there are fewer than 2
/// chars, use whatever is there.
/// ### Examples
///    extra_front("Hello") -> "HeHeHe"
///    extra_front("ab") -> "ababab"
///    extra_front("H") -> "HHH"
#[allow(unused)]
pub fn extra_front(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## String-1 -- without2
/// Given a string, if a length 2 substring appears at both its beginning and
/// end, return a string without the substring at the beginning, so "HelloHe"
/// yields "lloHe". The substring may overlap with itself, so "Hi" yields "".
/// Otherwise, return the original string unchanged.
/// ### Examples
///    without2("HelloHe") -> "lloHe"
///    without2("HelloHi") -> "HelloHi"
///    without2("Hi") -> ""
#[allow(unused)]
pub fn without2(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## String-1 -- deFront
/// Given a string, return a version without the first 2 chars. Except keep the
/// first char if it is 'a' and keep the second char if it is 'b'. The string
/// may be any length. Harder than it looks.
/// ### Examples
///    de_front("Hello") -> "llo"
///    de_front("java") -> "va"
///    de_front("away") -> "aay"
#[allow(unused)]
pub fn de_front(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## String-1 -- startWord
/// Given a string and a second "word" string, we'll say that the word matches
/// the string if it appears at the front of the string, except its first char
/// does not need to match exactly. On a match, return the front of the string,
/// or otherwise return the empty string. So, so with the string "hippo" the
/// word "hi" returns "hi" and "xip" returns "hip". The word will be at least
/// length 1.
/// ### Examples
///    start_word("hippo", "hi") -> "hi"
///    start_word("hippo", "xip") -> "hip"
///    start_word("hippo", "i") -> "h"
#[allow(unused)]
pub fn start_word(str: impl Into<String>, word: impl Into<String>) -> String {
    let str: String = str.into();
    let word: String = word.into();
    todo!()
}

/// ## String-1 -- withoutX
/// Given a string, if the first or last chars are 'x', return the string
/// without those 'x' chars, and otherwise return the string unchanged.
/// ### Examples
///    without_x("xHix") -> "Hi"
///    without_x("xHi") -> "Hi"
///    without_x("Hxix") -> "Hxi"
#[allow(unused)]
pub fn without_x(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## String-1 -- withoutX2
/// Given a string, if one or both of the first 2 chars is 'x', return the
/// string without those 'x' chars, and otherwise return the string unchanged.
/// This is a little harder than it looks.
/// ### Examples
///    without_x2("xHi") -> "Hi"
///    without_x2("Hxi") -> "Hi"
///    without_x2("Hi") -> "Hi"
#[allow(unused)]
pub fn without_x2(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## String-2 -- doubleChar
/// Given a string, return a string where for every char in the original, there
/// are two chars.
/// ### Examples
///    double_char("The") -> "TThhee"
///    double_char("AAbb") -> "AAAAbbbb"
///    double_char("Hi-There") -> "HHii--TThheerree"
#[allow(unused)]
pub fn double_char(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## String-2 -- countHi
/// Return the number of times that the string "hi" appears anywhere in the
/// given string.
/// ### Examples
///    count_hi("abc hi ho") -> 1
///    count_hi("ABChi hi") -> 2
///    count_hi("hihi") -> 2
#[allow(unused)]
pub fn count_hi(str: impl Into<String>) -> i32 {
    let str: String = str.into();
    todo!()
}

/// ## String-2 -- catDog
/// Return true if the string "cat" and "dog" appear the same number of times in
/// the given string.
/// ### Examples
///    cat_dog("catdog") -> true
///    cat_dog("catcat") -> false
///    cat_dog("1cat1cadodog") -> true
#[allow(unused)]
pub fn cat_dog(str: impl Into<String>) -> bool {
    let str: String = str.into();
    todo!()
}

/// ## String-2 -- countCode
/// Return the number of times that the string "code" appears anywhere in the
/// given string, except we'll accept any letter for the 'd', so "cope" and
/// "cooe" count.
/// ### Examples
///    count_code("aaacodebbb") -> 1
///    count_code("codexxcode") -> 2
///    count_code("cozexxcope") -> 2
#[allow(unused)]
pub fn count_code(str: impl Into<String>) -> i32 {
    let str: String = str.into();
    todo!()
}

/// ## String-2 -- endOther
/// Given two strings, return true if either of the strings appears at the very
/// end of the other string, ignoring upper/lower case differences (in other
/// words, the computation should not be "case sensitive"). Note:
/// str.toLowerCase() returns the lowercase version of a string.
/// ### Examples
///    end_other("Hiabc", "abc") -> true
///    end_other("AbC", "HiaBc") -> true
///    end_other("abc", "abXabc") -> true
#[allow(unused)]
pub fn end_other(a: impl Into<String>, b: impl Into<String>) -> bool {
    let a: String = a.into();
    let b: String = b.into();
    todo!()
}

/// ## String-2 -- xyzThere
/// Return true if the given string contains an appearance of "xyz" where the
/// xyz is not directly preceeded by a period (.). So "xxyz" counts but "x.xyz"
/// does not.
/// ### Examples
///    xyz_there("abcxyz") -> true
///    xyz_there("abc.xyz") -> false
///    xyz_there("xyz.abc") -> true
#[allow(unused)]
pub fn xyz_there(str: impl Into<String>) -> bool {
    let str: String = str.into();
    todo!()
}

/// ## String-2 -- bobThere
/// Return true if the given string contains a "bob" string, but where the
/// middle 'o' char can be any char.
/// ### Examples
///    bob_there("abcbob") -> true
///    bob_there("b9b") -> true
///    bob_there("bac") -> false
#[allow(unused)]
pub fn bob_there(str: impl Into<String>) -> bool {
    let str: String = str.into();
    todo!()
}

/// ## String-2 -- xyBalance
/// We'll say that a String is xy-balanced if for all the 'x' chars in the
/// string, there exists a 'y' char somewhere later in the string. So "xxy" is
/// balanced, but "xyx" is not. One 'y' can balance multiple 'x's. Return true
/// if the given string is xy-balanced.
/// ### Examples
///    xy_balance("aaxbby") -> true
///    xy_balance("aaxbb") -> false
///    xy_balance("yaaxbb") -> false
#[allow(unused)]
pub fn xy_balance(str: impl Into<String>) -> bool {
    let str: String = str.into();
    todo!()
}

/// ## String-2 -- mixString
/// Given two strings, a and b, create a bigger string made of the first char of
/// a, the first char of b, the second char of a, the second char of b, and so
/// on. Any leftover chars go at the end of the result.
/// ### Examples
///    mix_string("abc", "xyz") -> "axbycz"
///    mix_string("Hi", "There") -> "HTihere"
///    mix_string("xxxx", "There") -> "xTxhxexre"
#[allow(unused)]
pub fn mix_string(a: impl Into<String>, b: impl Into<String>) -> String {
    let a: String = a.into();
    let b: String = b.into();
    todo!()
}

/// ## String-2 -- repeatEnd
/// Given a string and an int n, return a string made of n repetitions of the
/// last n characters of the string. You may assume that n is between 0 and the
/// length of the string, inclusive.
/// ### Examples
///    repeat_end("Hello", 3) -> "llollollo"
///    repeat_end("Hello", 2) -> "lolo"
///    repeat_end("Hello", 1) -> "o"
#[allow(unused)]
pub fn repeat_end(str: impl Into<String>, n: i32) -> String {
    let str: String = str.into();
    todo!()
}

/// ## String-2 -- repeatFront
/// Given a string and an int n, return a string made of the first n characters
/// of the string, followed by the first n-1 characters of the string, and so
/// on. You may assume that n is between 0 and the length of the string,
/// inclusive (i.e. n >= 0 and n <= str.length()).
/// ### Examples
///    repeat_front("Chocolate", 4) -> "ChocChoChC"
///    repeat_front("Chocolate", 3) -> "ChoChC"
///    repeat_front("Ice Cream", 2) -> "IcI"
#[allow(unused)]
pub fn repeat_front(str: impl Into<String>, n: i32) -> String {
    let str: String = str.into();
    todo!()
}

/// ## String-2 -- repeatSeparator
/// Given two strings, word and a separator sep, return a big string made of
/// count occurrences of the word, separated by the separator string.
/// ### Examples
///    repeat_separator("Word", "X", 3) -> "WordXWordXWord"
///    repeat_separator("This", "And", 2) -> "ThisAndThis"
///    repeat_separator("This", "And", 1) -> "This"
#[allow(unused)]
pub fn repeat_separator(word: impl Into<String>, sep: impl Into<String>, count: i32) -> String {
    let word: String = word.into();
    let sep: String = sep.into();
    todo!()
}

/// ## String-2 -- prefixAgain
/// Given a string, consider the prefix string made of the first N chars of the
/// string. Does that prefix string appear somewhere else in the string? Assume
/// that the string is not empty and that N is in the range 1..str.length().
/// ### Examples
///    prefix_again("abXYabc", 1) -> true
///    prefix_again("abXYabc", 2) -> true
///    prefix_again("abXYabc", 3) -> false
#[allow(unused)]
pub fn prefix_again(str: impl Into<String>, n: i32) -> bool {
    let str: String = str.into();
    todo!()
}

/// ## String-2 -- xyzMiddle
/// Given a string, does "xyz" appear in the middle of the string? To define
/// middle, we'll say that the number of chars to the left and right of the
/// "xyz" must differ by at most one. This problem is harder than it looks.
/// ### Examples
///    xyz_middle("AAxyzBB") -> true
///    xyz_middle("AxyzBB") -> true
///    xyz_middle("AxyzBBB") -> false
#[allow(unused)]
pub fn xyz_middle(str: impl Into<String>) -> bool {
    let str: String = str.into();
    todo!()
}

/// ## String-2 -- getSandwich
/// A sandwich is two pieces of bread with something in between. Return the
/// string that is between the first and last appearance of "bread" in the given
/// string, or return the empty string "" if there are not two pieces of bread.
/// ### Examples
///    get_sandwich("breadjambread") -> "jam"
///    get_sandwich("xxbreadjambreadyy") -> "jam"
///    get_sandwich("xxbreadyy") -> ""
#[allow(unused)]
pub fn get_sandwich(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## String-2 -- sameStarChar
/// Returns true if for every '*' (star) in the string, if there are chars both
/// immediately before and after the star, they are the same.
/// ### Examples
///    same_star_char("xy*yzz") -> true
///    same_star_char("xy*zzz") -> false
///    same_star_char("*xa*az") -> true
#[allow(unused)]
pub fn same_star_char(str: impl Into<String>) -> bool {
    let str: String = str.into();
    todo!()
}

/// ## String-2 -- oneTwo
/// Given a string, compute a new string by moving the first char to come after
/// the next two chars, so "abc" yields "bca". Repeat this process for each
/// subsequent group of 3 chars, so "abcdef" yields "bcaefd". Ignore any group
/// of fewer than 3 chars at the end.
/// ### Examples
///    one_two("abc") -> "bca"
///    one_two("tca") -> "cat"
///    one_two("tcagdo") -> "catdog"
#[allow(unused)]
pub fn one_two(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## String-2 -- zipZap
/// Look for patterns like "zip" and "zap" in the string -- length-3, starting
/// with 'z' and ending with 'p'. Return a string where for all such words, the
/// middle letter is gone, so "zipXzap" yields "zpXzp".
/// ### Examples
///    zip_zap("zipXzap") -> "zpXzp"
///    zip_zap("zopzop") -> "zpzp"
///    zip_zap("zzzopzop") -> "zzzpzp"
#[allow(unused)]
pub fn zip_zap(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## String-2 -- starOut
/// Return a version of the given string, where for every star (*) in the string
/// the star and the chars immediately to its left and right are gone. So
/// "ab*cd" yields "ad" and "ab**cd" also yields "ad".
/// ### Examples
///    star_out("ab*cd") -> "ad"
///    star_out("ab**cd") -> "ad"
///    star_out("sm*eilly") -> "silly"
#[allow(unused)]
pub fn star_out(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## String-2 -- plusOut
/// Given a string and a non-empty word string, return a version of the original
/// String where all chars have been replaced by pluses ("+"), except for
/// appearances of the word string which are preserved unchanged.
/// ### Examples
///    plus_out("12xy34", "xy") -> "++xy++"
///    plus_out("12xy34", "1") -> "1+++++"
///    plus_out("12xy34xyabcxy", "xy") -> "++xy++xy+++xy"
#[allow(unused)]
pub fn plus_out(str: impl Into<String>, word: impl Into<String>) -> String {
    let str: String = str.into();
    let word: String = word.into();
    todo!()
}

/// ## String-2 -- wordEnds
/// Given a string and a non-empty word string, return a string made of each
/// char just before and just after every appearance of the word in the string.
/// Ignore cases where there is no char before or after the word, and a char may
/// be included twice if it is between two words.
/// ### Examples
///    word_ends("abcXY123XYijk", "XY") -> "c13i"
///    word_ends("XY123XY", "XY") -> "13"
///    word_ends("XY1XY", "XY") -> "11"
#[allow(unused)]
pub fn word_ends(str: impl Into<String>, word: impl Into<String>) -> String {
    let str: String = str.into();
    let word: String = word.into();
    todo!()
}

/// ## String-3 -- countYZ
/// Given a string, count the number of words ending in 'y' or 'z' -- so the 'y'
/// in "heavy" and the 'z' in "fez" count, but not the 'y' in "yellow" (not case
/// sensitive). We'll say that a y or z is at the end of a word if there is not
/// an alphabetic letter immediately following it. (Note:
/// Character.isLetter(char) tests if a char is an alphabetic letter.)
/// ### Examples
///    count_y_z("fez day") -> 2
///    count_y_z("day fez") -> 2
///    count_y_z("day fyyyz") -> 2
#[allow(unused)]
pub fn count_y_z(str: impl Into<String>) -> i32 {
    let str: String = str.into();
    todo!()
}

/// ## String-3 -- withoutString
/// Given two strings, base and remove, return a version of the base string
/// where all instances of the remove string have been removed (not case
/// sensitive). You may assume that the remove string is length 1 or more.
/// Remove only non-overlapping instances, so with "xxx" removing "xx" leaves
/// "x".
/// ### Examples
///    without_string("Hello there", "llo") -> "He there"
///    without_string("Hello there", "e") -> "Hllo thr"
///    without_string("Hello there", "x") -> "Hello there"
#[allow(unused)]
pub fn without_string(base: impl Into<String>, remove: impl Into<String>) -> String {
    let base: String = base.into();
    let remove: String = remove.into();
    todo!()
}

/// ## String-3 -- equalIsNot
/// Given a string, return true if the number of appearances of "is" anywhere in
/// the string is equal to the number of appearances of "not" anywhere in the
/// string (case sensitive).
/// ### Examples
///    equal_is_not("This is not") -> false
///    equal_is_not("This is notnot") -> true
///    equal_is_not("noisxxnotyynotxisi") -> true
#[allow(unused)]
pub fn equal_is_not(str: impl Into<String>) -> bool {
    let str: String = str.into();
    todo!()
}

/// ## String-3 -- gHappy
/// We'll say that a lowercase 'g' in a string is "happy" if there is another
/// 'g' immediately to its left or right. Return true if all the g's in the
/// given string are happy.
/// ### Examples
///    g_happy("xxggxx") -> true
///    g_happy("xxgxx") -> false
///    g_happy("xxggyygxx") -> false
#[allow(unused)]
pub fn g_happy(str: impl Into<String>) -> bool {
    let str: String = str.into();
    todo!()
}

/// ## String-3 -- countTriple
/// We'll say that a "triple" in a string is a char appearing three times in a
/// row. Return the number of triples in the given string. The triples may
/// overlap.
/// ### Examples
///    count_triple("abcXXXabc") -> 1
///    count_triple("xxxabyyyycd") -> 3
///    count_triple("a") -> 0
#[allow(unused)]
pub fn count_triple(str: impl Into<String>) -> i32 {
    let str: String = str.into();
    todo!()
}

/// ## String-3 -- sumDigits
/// Given a string, return the sum of the digits 0-9 that appear in the string,
/// ignoring all other characters. Return 0 if there are no digits in the
/// string. (Note: Character.isDigit(char) tests if a char is one of the chars
/// '0', '1', .. '9'. Integer.parseInt(string) converts a string to an int.)
/// ### Examples
///    sum_digits("aa1bc2d3") -> 6
///    sum_digits("aa11b33") -> 8
///    sum_digits("Chocolate") -> 0
#[allow(unused)]
pub fn sum_digits(str: impl Into<String>) -> i32 {
    let str: String = str.into();
    todo!()
}

/// ## String-3 -- sameEnds
/// Given a string, return the longest substring that appears at both the
/// beginning and end of the string without overlapping. For example,
/// sameEnds("abXab") is "ab".
/// ### Examples
///    same_ends("abXYab") -> "ab"
///    same_ends("xx") -> "x"
///    same_ends("xxx") -> "x"
#[allow(unused)]
pub fn same_ends(nums: impl Into<String>) -> String {
    let nums: String = nums.into();
    todo!()
}

/// ## String-3 -- mirrorEnds
/// Given a string, look for a mirror image (backwards) string at both the
/// beginning and end of the given string. In other words, zero or more
/// characters at the very begining of the given string, and at the very end of
/// the string in reverse order (possibly overlapping). For example, the string
/// "abXYZba" has the mirror end "ab".
/// ### Examples
///    mirror_ends("abXYZba") -> "ab"
///    mirror_ends("abca") -> "a"
///    mirror_ends("aba") -> "aba"
#[allow(unused)]
pub fn mirror_ends(string: impl Into<String>) -> String {
    let string: String = string.into();
    todo!()
}

/// ## String-3 -- maxBlock
/// Given a string, return the length of the largest "block" in the string. A
/// block is a run of adjacent chars that are the same.
/// ### Examples
///    max_block("hoopla") -> 2
///    max_block("abbCCCddBBBxx") -> 3
///    max_block("") -> 0
#[allow(unused)]
pub fn max_block(str: impl Into<String>) -> i32 {
    let str: String = str.into();
    todo!()
}

/// ## String-3 -- sumNumbers
/// Given a string, return the sum of the numbers appearing in the string,
/// ignoring all other characters. A number is a series of 1 or more digit chars
/// in a row. (Note: Character.isDigit(char) tests if a char is one of the chars
/// '0', '1', .. '9'. Integer.parseInt(string) converts a string to an int.)
/// ### Examples
///    sum_numbers("abc123xyz") -> 123
///    sum_numbers("aa11b33") -> 44
///    sum_numbers("7 11") -> 18
#[allow(unused)]
pub fn sum_numbers(str: impl Into<String>) -> i32 {
    let str: String = str.into();
    todo!()
}

/// ## String-3 -- notReplace
/// Given a string, return a string where every appearance of the lowercase word
/// "is" has been replaced with "is not". The word "is" should not be
/// immediately preceeded or followed by a letter -- so for example the "is" in
/// "this" does not count. (Note: Character.isLetter(char) tests if a char is a
/// letter.)
/// ### Examples
///    not_replace("is test") -> "is not test"
///    not_replace("is-is") -> "is not-is not"
///    not_replace("This is right") -> "This is not right"
#[allow(unused)]
pub fn not_replace(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}
