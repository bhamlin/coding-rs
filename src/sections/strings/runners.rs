use super::implementations::*;
use super::solutions::*;
use crate::display::TestDisplay;

pub fn run(test: Option<Vec<String>>) {
    match test {
        // None => println!("Please specify a test name"),
        None => {
            hello_name_run();
            make_abba_run();
            make_tags_run();
            make_out_word_run();
            extra_end_run();
            first_two_run();
            first_half_run();
            without_end_run();
            combo_string_run();
            non_start_run();
            left2_run();
            right2_run();
            the_end_run();
            without_end2_run();
            middle_two_run();
            ends_ly_run();
            n_twice_run();
            two_char_run();
            middle_three_run();
            has_bad_run();
            at_first_run();
            last_chars_run();
            con_cat_run();
            last_two_run();
            see_color_run();
            front_again_run();
            min_cat_run();
            extra_front_run();
            without2_run();
            de_front_run();
            start_word_run();
            without_x_run();
            without_x2_run();
            double_char_run();
            count_hi_run();
            cat_dog_run();
            count_code_run();
            end_other_run();
            xyz_there_run();
            bob_there_run();
            xy_balance_run();
            mix_string_run();
            repeat_end_run();
            repeat_front_run();
            repeat_separator_run();
            prefix_again_run();
            xyz_middle_run();
            get_sandwich_run();
            same_star_char_run();
            one_two_run();
            zip_zap_run();
            star_out_run();
            plus_out_run();
            word_ends_run();
            count_y_z_run();
            without_string_run();
            equal_is_not_run();
            g_happy_run();
            count_triple_run();
            sum_digits_run();
            same_ends_run();
            mirror_ends_run();
            max_block_run();
            sum_numbers_run();
            not_replace_run();
        }
        Some(tests) => {
            for test_name in tests {
                match test_name.to_ascii_lowercase().as_str() {
                    "helloName" | "hello_name" => hello_name_run(),
                    "makeAbba" | "make_abba" => make_abba_run(),
                    "makeTags" | "make_tags" => make_tags_run(),
                    "makeOutWord" | "make_out_word" => make_out_word_run(),
                    "extraEnd" | "extra_end" => extra_end_run(),
                    "firstTwo" | "first_two" => first_two_run(),
                    "firstHalf" | "first_half" => first_half_run(),
                    "withoutEnd" | "without_end" => without_end_run(),
                    "comboString" | "combo_string" => combo_string_run(),
                    "nonStart" | "non_start" => non_start_run(),
                    "left2" => left2_run(),
                    "right2" => right2_run(),
                    "theEnd" | "the_end" => the_end_run(),
                    "withoutEnd2" | "without_end2" => without_end2_run(),
                    "middleTwo" | "middle_two" => middle_two_run(),
                    "endsLy" | "ends_ly" => ends_ly_run(),
                    "nTwice" | "n_twice" => n_twice_run(),
                    "twoChar" | "two_char" => two_char_run(),
                    "middleThree" | "middle_three" => middle_three_run(),
                    "hasBad" | "has_bad" => has_bad_run(),
                    "atFirst" | "at_first" => at_first_run(),
                    "lastChars" | "last_chars" => last_chars_run(),
                    "conCat" | "con_cat" => con_cat_run(),
                    "lastTwo" | "last_two" => last_two_run(),
                    "seeColor" | "see_color" => see_color_run(),
                    "frontAgain" | "front_again" => front_again_run(),
                    "minCat" | "min_cat" => min_cat_run(),
                    "extraFront" | "extra_front" => extra_front_run(),
                    "without2" => without2_run(),
                    "deFront" | "de_front" => de_front_run(),
                    "startWord" | "start_word" => start_word_run(),
                    "withoutX" | "without_x" => without_x_run(),
                    "withoutX2" | "without_x2" => without_x2_run(),
                    "doubleChar" | "double_char" => double_char_run(),
                    "countHi" | "count_hi" => count_hi_run(),
                    "catDog" | "cat_dog" => cat_dog_run(),
                    "countCode" | "count_code" => count_code_run(),
                    "endOther" | "end_other" => end_other_run(),
                    "xyzThere" | "xyz_there" => xyz_there_run(),
                    "bobThere" | "bob_there" => bob_there_run(),
                    "xyBalance" | "xy_balance" => xy_balance_run(),
                    "mixString" | "mix_string" => mix_string_run(),
                    "repeatEnd" | "repeat_end" => repeat_end_run(),
                    "repeatFront" | "repeat_front" => repeat_front_run(),
                    "repeatSeparator" | "repeat_separator" => repeat_separator_run(),
                    "prefixAgain" | "prefix_again" => prefix_again_run(),
                    "xyzMiddle" | "xyz_middle" => xyz_middle_run(),
                    "getSandwich" | "get_sandwich" => get_sandwich_run(),
                    "sameStarChar" | "same_star_char" => same_star_char_run(),
                    "oneTwo" | "one_two" => one_two_run(),
                    "zipZap" | "zip_zap" => zip_zap_run(),
                    "starOut" | "star_out" => star_out_run(),
                    "plusOut" | "plus_out" => plus_out_run(),
                    "wordEnds" | "word_ends" => word_ends_run(),
                    "countYZ" | "count_y_z" => count_y_z_run(),
                    "withoutString" | "without_string" => without_string_run(),
                    "equalIsNot" | "equal_is_not" => equal_is_not_run(),
                    "gHappy" | "g_happy" => g_happy_run(),
                    "countTriple" | "count_triple" => count_triple_run(),
                    "sumDigits" | "sum_digits" => sum_digits_run(),
                    "sameEnds" | "same_ends" => same_ends_run(),
                    "mirrorEnds" | "mirror_ends" => mirror_ends_run(),
                    "maxBlock" | "max_block" => max_block_run(),
                    "sumNumbers" | "sum_numbers" => sum_numbers_run(),
                    "notReplace" | "not_replace" => not_replace_run(),
                    _ => println!("No test named {test_name}"),
                };
            }
        }
    };
}

pub fn hello_name_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("hello_name");
    layout.print_header();
    layout.print_test(format!("{:?}", ("Bob")), hello_name_solution("Bob"), hello_name("Bob"));
    layout.print_test(format!("{:?}", ("Alice")), hello_name_solution("Alice"), hello_name("Alice"));
    layout.print_test(format!("{:?}", ("X")), hello_name_solution("X"), hello_name("X"));
    layout.print_test(format!("{:?}", ("Dolly")), hello_name_solution("Dolly"), hello_name("Dolly"));
    layout.print_test(format!("{:?}", ("Alpha")), hello_name_solution("Alpha"), hello_name("Alpha"));
    layout.print_test(format!("{:?}", ("Omega")), hello_name_solution("Omega"), hello_name("Omega"));
    layout.print_test(format!("{:?}", ("Goodbye")), hello_name_solution("Goodbye"), hello_name("Goodbye"));
    layout.print_test(format!("{:?}", ("ho ho ho")), hello_name_solution("ho ho ho"), hello_name("ho ho ho"));
    layout.print_test(format!("{:?}", ("xyz!")), hello_name_solution("xyz!"), hello_name("xyz!"));
    layout.print_test(format!("{:?}", ("Hello!")), hello_name_solution("Hello!"), hello_name("Hello!"));
    layout.print_footer();
}

pub fn make_abba_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("make_abba");
    layout.print_header();
    layout.print_test(format!("{:?}", ("Hi", "Bye")), make_abba_solution("Hi", "Bye"), make_abba("Hi", "Bye"));
    layout.print_test(format!("{:?}", ("Yo", "Alice")), make_abba_solution("Yo", "Alice"), make_abba("Yo", "Alice"));
    layout.print_test(format!("{:?}", ("What", "Up")), make_abba_solution("What", "Up"), make_abba("What", "Up"));
    layout.print_test(format!("{:?}", ("aaa", "bbb")), make_abba_solution("aaa", "bbb"), make_abba("aaa", "bbb"));
    layout.print_test(format!("{:?}", ("x", "y")), make_abba_solution("x", "y"), make_abba("x", "y"));
    layout.print_test(format!("{:?}", ("x", "")), make_abba_solution("x", ""), make_abba("x", ""));
    layout.print_test(format!("{:?}", ("ba", "Ya")), make_abba_solution("ba", "Ya"), make_abba("ba", "Ya"));
    layout.print_test(format!("{:?}", ("Ya", "Ya")), make_abba_solution("Ya", "Ya"), make_abba("Ya", "Ya"));
    layout.print_footer();
}

pub fn make_tags_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("make_tags");
    layout.print_header();
    layout.print_test(format!("{:?}", ("i", "Yay")), make_tags_solution("i", "Yay"), make_tags("i", "Yay"));
    layout.print_test(format!("{:?}", ("i", "Hello")), make_tags_solution("i", "Hello"), make_tags("i", "Hello"));
    layout.print_test(format!("{:?}", ("cite", "Yay")), make_tags_solution("cite", "Yay"), make_tags("cite", "Yay"));
    layout.print_test(format!("{:?}", ("address", "here")), make_tags_solution("address", "here"), make_tags("address", "here"));
    layout.print_test(format!("{:?}", ("body", "Heart")), make_tags_solution("body", "Heart"), make_tags("body", "Heart"));
    layout.print_test(format!("{:?}", ("i", "i")), make_tags_solution("i", "i"), make_tags("i", "i"));
    layout.print_test(format!("{:?}", ("i", "i")), make_tags_solution("i", "i"), make_tags("i", "i"));
    layout.print_test(format!("{:?}", ("i", "")), make_tags_solution("i", ""), make_tags("i", ""));
    layout.print_footer();
}

pub fn make_out_word_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("make_out_word");
    layout.print_header();
    layout.print_test(format!("{:?}", ("<<>>", "Yay")), make_out_word_solution("<<>>", "Yay"), make_out_word("<<>>", "Yay"));
    layout.print_test(format!("{:?}", ("<<>>", "WooHoo")), make_out_word_solution("<<>>", "WooHoo"), make_out_word("<<>>", "WooHoo"));
    layout.print_test(format!("{:?}", ("[[]]", "word")), make_out_word_solution("[[]]", "word"), make_out_word("[[]]", "word"));
    layout.print_test(format!("{:?}", ("HHoo", "Hello")), make_out_word_solution("HHoo", "Hello"), make_out_word("HHoo", "Hello"));
    layout.print_test(format!("{:?}", ("abyz", "YAY")), make_out_word_solution("abyz", "YAY"), make_out_word("abyz", "YAY"));
    layout.print_footer();
}

pub fn extra_end_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("extra_end");
    layout.print_header();
    layout.print_test(format!("{:?}", ("Hello")), extra_end_solution("Hello"), extra_end("Hello"));
    layout.print_test(format!("{:?}", ("ab")), extra_end_solution("ab"), extra_end("ab"));
    layout.print_test(format!("{:?}", ("Hi")), extra_end_solution("Hi"), extra_end("Hi"));
    layout.print_test(format!("{:?}", ("Candy")), extra_end_solution("Candy"), extra_end("Candy"));
    layout.print_test(format!("{:?}", ("Code")), extra_end_solution("Code"), extra_end("Code"));
    layout.print_footer();
}

pub fn first_two_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("first_two");
    layout.print_header();
    layout.print_test(format!("{:?}", ("Hello")), first_two_solution("Hello"), first_two("Hello"));
    layout.print_test(format!("{:?}", ("abcdefg")), first_two_solution("abcdefg"), first_two("abcdefg"));
    layout.print_test(format!("{:?}", ("ab")), first_two_solution("ab"), first_two("ab"));
    layout.print_test(format!("{:?}", ("a")), first_two_solution("a"), first_two("a"));
    layout.print_test(format!("{:?}", ("")), first_two_solution(""), first_two(""));
    layout.print_test(format!("{:?}", ("kitten")), first_two_solution("kitten"), first_two("kitten"));
    layout.print_test(format!("{:?}", ("hi")), first_two_solution("hi"), first_two("hi"));
    layout.print_test(format!("{:?}", ("hiya")), first_two_solution("hiya"), first_two("hiya"));
    layout.print_footer();
}

pub fn first_half_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("first_half");
    layout.print_header();
    layout.print_test(format!("{:?}", ("WooHoo")), first_half_solution("WooHoo"), first_half("WooHoo"));
    layout.print_test(format!("{:?}", ("HelloThere")), first_half_solution("HelloThere"), first_half("HelloThere"));
    layout.print_test(format!("{:?}", ("abcdefg")), first_half_solution("abcdefg"), first_half("abcdefg"));
    layout.print_test(format!("{:?}", ("ab")), first_half_solution("ab"), first_half("ab"));
    layout.print_test(format!("{:?}", ("")), first_half_solution(""), first_half(""));
    layout.print_test(format!("{:?}", ("0123456789")), first_half_solution("0123456789"), first_half("0123456789"));
    layout.print_test(format!("{:?}", ("kitten")), first_half_solution("kitten"), first_half("kitten"));
    layout.print_footer();
}

pub fn without_end_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("without_end");
    layout.print_header();
    layout.print_test(format!("{:?}", ("Hello")), without_end_solution("Hello"), without_end("Hello"));
    layout.print_test(format!("{:?}", ("java")), without_end_solution("java"), without_end("java"));
    layout.print_test(format!("{:?}", ("coding")), without_end_solution("coding"), without_end("coding"));
    layout.print_test(format!("{:?}", ("code")), without_end_solution("code"), without_end("code"));
    layout.print_test(format!("{:?}", ("ab")), without_end_solution("ab"), without_end("ab"));
    layout.print_test(format!("{:?}", ("Chocolate!")), without_end_solution("Chocolate!"), without_end("Chocolate!"));
    layout.print_test(format!("{:?}", ("kitten")), without_end_solution("kitten"), without_end("kitten"));
    layout.print_test(format!("{:?}", ("woohoo")), without_end_solution("woohoo"), without_end("woohoo"));
    layout.print_footer();
}

pub fn combo_string_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("combo_string");
    layout.print_header();
    layout.print_test(format!("{:?}", ("Hello", "hi")), combo_string_solution("Hello", "hi"), combo_string("Hello", "hi"));
    layout.print_test(format!("{:?}", ("Hi", "Hello")), combo_string_solution("Hi", "Hello"), combo_string("Hi", "Hello"));
    layout.print_test(format!("{:?}", ("aaa", "b")), combo_string_solution("aaa", "b"), combo_string("aaa", "b"));
    layout.print_test(format!("{:?}", ("b", "aaa")), combo_string_solution("b", "aaa"), combo_string("b", "aaa"));
    layout.print_test(format!("{:?}", ("aaa", "")), combo_string_solution("aaa", ""), combo_string("aaa", ""));
    layout.print_test(format!("{:?}", ("", "bb")), combo_string_solution("", "bb"), combo_string("", "bb"));
    layout.print_test(format!("{:?}", ("aaa", "1234")), combo_string_solution("aaa", "1234"), combo_string("aaa", "1234"));
    layout.print_test(format!("{:?}", ("aaa", "bb")), combo_string_solution("aaa", "bb"), combo_string("aaa", "bb"));
    layout.print_test(format!("{:?}", ("a", "bb")), combo_string_solution("a", "bb"), combo_string("a", "bb"));
    layout.print_test(format!("{:?}", ("bb", "a")), combo_string_solution("bb", "a"), combo_string("bb", "a"));
    layout.print_test(format!("{:?}", ("a", "bb")), combo_string_solution("a", "bb"), combo_string("a", "bb"));
    layout.print_test(format!("{:?}", ("xyz", "ab")), combo_string_solution("xyz", "ab"), combo_string("xyz", "ab"));
    layout.print_footer();
}

pub fn non_start_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("non_start");
    layout.print_header();
    layout.print_test(format!("{:?}", ("Hello", "There")), non_start_solution("Hello", "There"), non_start("Hello", "There"));
    layout.print_test(format!("{:?}", ("java", "code")), non_start_solution("java", "code"), non_start("java", "code"));
    layout.print_test(format!("{:?}", ("shotl", "java")), non_start_solution("shotl", "java"), non_start("shotl", "java"));
    layout.print_test(format!("{:?}", ("ab", "xy")), non_start_solution("ab", "xy"), non_start("ab", "xy"));
    layout.print_test(format!("{:?}", ("ab", "x")), non_start_solution("ab", "x"), non_start("ab", "x"));
    layout.print_test(format!("{:?}", ("x", "ac")), non_start_solution("x", "ac"), non_start("x", "ac"));
    layout.print_test(format!("{:?}", ("a", "x")), non_start_solution("a", "x"), non_start("a", "x"));
    layout.print_test(format!("{:?}", ("kit", "kat")), non_start_solution("kit", "kat"), non_start("kit", "kat"));
    layout.print_test(format!("{:?}", ("mart", "dart")), non_start_solution("mart", "dart"), non_start("mart", "dart"));
    layout.print_footer();
}

pub fn left2_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("left2");
    layout.print_header();
    layout.print_test(format!("{:?}", ("Hello")), left2_solution("Hello"), left2("Hello"));
    layout.print_test(format!("{:?}", ("java")), left2_solution("java"), left2("java"));
    layout.print_test(format!("{:?}", ("Hi")), left2_solution("Hi"), left2("Hi"));
    layout.print_test(format!("{:?}", ("code")), left2_solution("code"), left2("code"));
    layout.print_test(format!("{:?}", ("cat")), left2_solution("cat"), left2("cat"));
    layout.print_test(format!("{:?}", ("12345")), left2_solution("12345"), left2("12345"));
    layout.print_test(format!("{:?}", ("Chocolate")), left2_solution("Chocolate"), left2("Chocolate"));
    layout.print_test(format!("{:?}", ("bricks")), left2_solution("bricks"), left2("bricks"));
    layout.print_footer();
}

pub fn right2_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("right2");
    layout.print_header();
    layout.print_test(format!("{:?}", ("Hello")), right2_solution("Hello"), right2("Hello"));
    layout.print_test(format!("{:?}", ("java")), right2_solution("java"), right2("java"));
    layout.print_test(format!("{:?}", ("Hi")), right2_solution("Hi"), right2("Hi"));
    layout.print_test(format!("{:?}", ("code")), right2_solution("code"), right2("code"));
    layout.print_test(format!("{:?}", ("cat")), right2_solution("cat"), right2("cat"));
    layout.print_test(format!("{:?}", ("12345")), right2_solution("12345"), right2("12345"));
    layout.print_footer();
}

pub fn the_end_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("the_end");
    layout.print_header();
    layout.print_test(format!("{:?}", ("Hello", true)), the_end_solution("Hello", true), the_end("Hello", true));
    layout.print_test(format!("{:?}", ("Hello", false)), the_end_solution("Hello", false), the_end("Hello", false));
    layout.print_test(format!("{:?}", ("oh", true)), the_end_solution("oh", true), the_end("oh", true));
    layout.print_test(format!("{:?}", ("oh", false)), the_end_solution("oh", false), the_end("oh", false));
    layout.print_test(format!("{:?}", ("x", true)), the_end_solution("x", true), the_end("x", true));
    layout.print_test(format!("{:?}", ("x", false)), the_end_solution("x", false), the_end("x", false));
    layout.print_test(format!("{:?}", ("java", true)), the_end_solution("java", true), the_end("java", true));
    layout.print_test(format!("{:?}", ("chocolate", false)), the_end_solution("chocolate", false), the_end("chocolate", false));
    layout.print_test(format!("{:?}", ("1234", true)), the_end_solution("1234", true), the_end("1234", true));
    layout.print_test(format!("{:?}", ("code", false)), the_end_solution("code", false), the_end("code", false));
    layout.print_footer();
}

pub fn without_end2_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("without_end2");
    layout.print_header();
    layout.print_test(format!("{:?}", ("Hello")), without_end2_solution("Hello"), without_end2("Hello"));
    layout.print_test(format!("{:?}", ("abc")), without_end2_solution("abc"), without_end2("abc"));
    layout.print_test(format!("{:?}", ("ab")), without_end2_solution("ab"), without_end2("ab"));
    layout.print_test(format!("{:?}", ("a")), without_end2_solution("a"), without_end2("a"));
    layout.print_test(format!("{:?}", ("")), without_end2_solution(""), without_end2(""));
    layout.print_test(format!("{:?}", ("coldy")), without_end2_solution("coldy"), without_end2("coldy"));
    layout.print_test(format!("{:?}", ("java code")), without_end2_solution("java code"), without_end2("java code"));
    layout.print_footer();
}

pub fn middle_two_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("middle_two");
    layout.print_header();
    layout.print_test(format!("{:?}", ("string")), middle_two_solution("string"), middle_two("string"));
    layout.print_test(format!("{:?}", ("code")), middle_two_solution("code"), middle_two("code"));
    layout.print_test(format!("{:?}", ("Practice")), middle_two_solution("Practice"), middle_two("Practice"));
    layout.print_test(format!("{:?}", ("ab")), middle_two_solution("ab"), middle_two("ab"));
    layout.print_test(format!("{:?}", ("123456789")), middle_two_solution("123456789"), middle_two("123456789"));
    layout.print_footer();
}

pub fn ends_ly_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("ends_ly");
    layout.print_header();
    layout.print_test(format!("{:?}", ("oddly")), ends_ly_solution("oddly"), ends_ly("oddly"));
    layout.print_test(format!("{:?}", ("y")), ends_ly_solution("y"), ends_ly("y"));
    layout.print_test(format!("{:?}", ("oddl")), ends_ly_solution("oddl"), ends_ly("oddl"));
    layout.print_test(format!("{:?}", ("olydd")), ends_ly_solution("olydd"), ends_ly("olydd"));
    layout.print_test(format!("{:?}", ("ly")), ends_ly_solution("ly"), ends_ly("ly"));
    layout.print_test(format!("{:?}", ("")), ends_ly_solution(""), ends_ly(""));
    layout.print_test(format!("{:?}", ("falsely")), ends_ly_solution("falsely"), ends_ly("falsely"));
    layout.print_test(format!("{:?}", ("evenly")), ends_ly_solution("evenly"), ends_ly("evenly"));
    layout.print_footer();
}

pub fn n_twice_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("n_twice");
    layout.print_header();
    layout.print_test(format!("{:?}", ("hello", 2)), n_twice_solution("hello", 2), n_twice("hello", 2));
    layout.print_test(format!("{:?}", ("Chocolate", 3)), n_twice_solution("Chocolate", 3), n_twice("Chocolate", 3));
    layout.print_test(format!("{:?}", ("Chocolate", 1)), n_twice_solution("Chocolate", 1), n_twice("Chocolate", 1));
    layout.print_test(format!("{:?}", ("Chocolate", 0)), n_twice_solution("Chocolate", 0), n_twice("Chocolate", 0));
    layout.print_test(format!("{:?}", ("Hello", 4)), n_twice_solution("Hello", 4), n_twice("Hello", 4));
    layout.print_test(format!("{:?}", ("Code", 4)), n_twice_solution("Code", 4), n_twice("Code", 4));
    layout.print_test(format!("{:?}", ("Code", 2)), n_twice_solution("Code", 2), n_twice("Code", 2));
    layout.print_footer();
}

pub fn two_char_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("two_char");
    layout.print_header();
    layout.print_test(format!("{:?}", ("java", 0)), two_char_solution("java", 0), two_char("java", 0));
    layout.print_test(format!("{:?}", ("java", 2)), two_char_solution("java", 2), two_char("java", 2));
    layout.print_test(format!("{:?}", ("java", 3)), two_char_solution("java", 3), two_char("java", 3));
    layout.print_test(format!("{:?}", ("java", 4)), two_char_solution("java", 4), two_char("java", 4));
    layout.print_test(format!("{:?}", ("java", -1)), two_char_solution("java", -1), two_char("java", -1));
    layout.print_test(format!("{:?}", ("Hello", 0)), two_char_solution("Hello", 0), two_char("Hello", 0));
    layout.print_test(format!("{:?}", ("Hello", 1)), two_char_solution("Hello", 1), two_char("Hello", 1));
    layout.print_test(format!("{:?}", ("Hello", 99)), two_char_solution("Hello", 99), two_char("Hello", 99));
    layout.print_test(format!("{:?}", ("Hello", 3)), two_char_solution("Hello", 3), two_char("Hello", 3));
    layout.print_test(format!("{:?}", ("Hello", 4)), two_char_solution("Hello", 4), two_char("Hello", 4));
    layout.print_test(format!("{:?}", ("Hello", 5)), two_char_solution("Hello", 5), two_char("Hello", 5));
    layout.print_test(format!("{:?}", ("Hello", -7)), two_char_solution("Hello", -7), two_char("Hello", -7));
    layout.print_test(format!("{:?}", ("Hello", 6)), two_char_solution("Hello", 6), two_char("Hello", 6));
    layout.print_test(format!("{:?}", ("Hello", -1)), two_char_solution("Hello", -1), two_char("Hello", -1));
    layout.print_test(format!("{:?}", ("yay", 0)), two_char_solution("yay", 0), two_char("yay", 0));
    layout.print_footer();
}

pub fn middle_three_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("middle_three");
    layout.print_header();
    layout.print_test(format!("{:?}", ("Candy")), middle_three_solution("Candy"), middle_three("Candy"));
    layout.print_test(format!("{:?}", ("and")), middle_three_solution("and"), middle_three("and"));
    layout.print_test(format!("{:?}", ("solving")), middle_three_solution("solving"), middle_three("solving"));
    layout.print_test(format!("{:?}", ("Hi yet Hi")), middle_three_solution("Hi yet Hi"), middle_three("Hi yet Hi"));
    layout.print_test(format!("{:?}", ("java yet java")), middle_three_solution("java yet java"), middle_three("java yet java"));
    layout.print_test(format!("{:?}", ("Chocolate")), middle_three_solution("Chocolate"), middle_three("Chocolate"));
    layout.print_test(format!("{:?}", ("XabcxyzabcX")), middle_three_solution("XabcxyzabcX"), middle_three("XabcxyzabcX"));
    layout.print_footer();
}

pub fn has_bad_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("has_bad");
    layout.print_header();
    layout.print_test(format!("{:?}", ("badxx")), has_bad_solution("badxx"), has_bad("badxx"));
    layout.print_test(format!("{:?}", ("xbadxx")), has_bad_solution("xbadxx"), has_bad("xbadxx"));
    layout.print_test(format!("{:?}", ("xxbadxx")), has_bad_solution("xxbadxx"), has_bad("xxbadxx"));
    layout.print_test(format!("{:?}", ("code")), has_bad_solution("code"), has_bad("code"));
    layout.print_test(format!("{:?}", ("bad")), has_bad_solution("bad"), has_bad("bad"));
    layout.print_test(format!("{:?}", ("ba")), has_bad_solution("ba"), has_bad("ba"));
    layout.print_test(format!("{:?}", ("xba")), has_bad_solution("xba"), has_bad("xba"));
    layout.print_test(format!("{:?}", ("xbad")), has_bad_solution("xbad"), has_bad("xbad"));
    layout.print_test(format!("{:?}", ("")), has_bad_solution(""), has_bad(""));
    layout.print_test(format!("{:?}", ("badyy")), has_bad_solution("badyy"), has_bad("badyy"));
    layout.print_footer();
}

pub fn at_first_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("at_first");
    layout.print_header();
    layout.print_test(format!("{:?}", ("hello")), at_first_solution("hello"), at_first("hello"));
    layout.print_test(format!("{:?}", ("hi")), at_first_solution("hi"), at_first("hi"));
    layout.print_test(format!("{:?}", ("h")), at_first_solution("h"), at_first("h"));
    layout.print_test(format!("{:?}", ("")), at_first_solution(""), at_first(""));
    layout.print_test(format!("{:?}", ("kitten")), at_first_solution("kitten"), at_first("kitten"));
    layout.print_test(format!("{:?}", ("java")), at_first_solution("java"), at_first("java"));
    layout.print_test(format!("{:?}", ("j")), at_first_solution("j"), at_first("j"));
    layout.print_footer();
}

pub fn last_chars_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("last_chars");
    layout.print_header();
    layout.print_test(format!("{:?}", ("last", "chars")), last_chars_solution("last", "chars"), last_chars("last", "chars"));
    layout.print_test(format!("{:?}", ("yo", "java")), last_chars_solution("yo", "java"), last_chars("yo", "java"));
    layout.print_test(format!("{:?}", ("hi", "")), last_chars_solution("hi", ""), last_chars("hi", ""));
    layout.print_test(format!("{:?}", ("", "hello")), last_chars_solution("", "hello"), last_chars("", "hello"));
    layout.print_test(format!("{:?}", ("", "")), last_chars_solution("", ""), last_chars("", ""));
    layout.print_test(format!("{:?}", ("kitten", "hi")), last_chars_solution("kitten", "hi"), last_chars("kitten", "hi"));
    layout.print_test(format!("{:?}", ("k", "zip")), last_chars_solution("k", "zip"), last_chars("k", "zip"));
    layout.print_test(format!("{:?}", ("kitten", "")), last_chars_solution("kitten", ""), last_chars("kitten", ""));
    layout.print_test(format!("{:?}", ("kitten", "zip")), last_chars_solution("kitten", "zip"), last_chars("kitten", "zip"));
    layout.print_footer();
}

pub fn con_cat_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("con_cat");
    layout.print_header();
    layout.print_test(format!("{:?}", ("abc", "cat")), con_cat_solution("abc", "cat"), con_cat("abc", "cat"));
    layout.print_test(format!("{:?}", ("dog", "cat")), con_cat_solution("dog", "cat"), con_cat("dog", "cat"));
    layout.print_test(format!("{:?}", ("abc", "")), con_cat_solution("abc", ""), con_cat("abc", ""));
    layout.print_test(format!("{:?}", ("", "cat")), con_cat_solution("", "cat"), con_cat("", "cat"));
    layout.print_test(format!("{:?}", ("pig", "g")), con_cat_solution("pig", "g"), con_cat("pig", "g"));
    layout.print_test(format!("{:?}", ("pig", "doggy")), con_cat_solution("pig", "doggy"), con_cat("pig", "doggy"));
    layout.print_footer();
}

pub fn last_two_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("last_two");
    layout.print_header();
    layout.print_test(format!("{:?}", ("coding")), last_two_solution("coding"), last_two("coding"));
    layout.print_test(format!("{:?}", ("cat")), last_two_solution("cat"), last_two("cat"));
    layout.print_test(format!("{:?}", ("ab")), last_two_solution("ab"), last_two("ab"));
    layout.print_test(format!("{:?}", ("a")), last_two_solution("a"), last_two("a"));
    layout.print_test(format!("{:?}", ("")), last_two_solution(""), last_two(""));
    layout.print_footer();
}

pub fn see_color_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("see_color");
    layout.print_header();
    layout.print_test(format!("{:?}", ("redxx")), see_color_solution("redxx"), see_color("redxx"));
    layout.print_test(format!("{:?}", ("xxred")), see_color_solution("xxred"), see_color("xxred"));
    layout.print_test(format!("{:?}", ("blueTimes")), see_color_solution("blueTimes"), see_color("blueTimes"));
    layout.print_test(format!("{:?}", ("NoColor")), see_color_solution("NoColor"), see_color("NoColor"));
    layout.print_test(format!("{:?}", ("red")), see_color_solution("red"), see_color("red"));
    layout.print_test(format!("{:?}", ("re")), see_color_solution("re"), see_color("re"));
    layout.print_test(format!("{:?}", ("blu")), see_color_solution("blu"), see_color("blu"));
    layout.print_test(format!("{:?}", ("blue")), see_color_solution("blue"), see_color("blue"));
    layout.print_test(format!("{:?}", ("a")), see_color_solution("a"), see_color("a"));
    layout.print_test(format!("{:?}", ("")), see_color_solution(""), see_color(""));
    layout.print_test(format!("{:?}", ("xyzred")), see_color_solution("xyzred"), see_color("xyzred"));
    layout.print_footer();
}

pub fn front_again_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("front_again");
    layout.print_header();
    layout.print_test(format!("{:?}", ("edited")), front_again_solution("edited"), front_again("edited"));
    layout.print_test(format!("{:?}", ("edit")), front_again_solution("edit"), front_again("edit"));
    layout.print_test(format!("{:?}", ("ed")), front_again_solution("ed"), front_again("ed"));
    layout.print_test(format!("{:?}", ("jj")), front_again_solution("jj"), front_again("jj"));
    layout.print_test(format!("{:?}", ("jjj")), front_again_solution("jjj"), front_again("jjj"));
    layout.print_test(format!("{:?}", ("jjjj")), front_again_solution("jjjj"), front_again("jjjj"));
    layout.print_test(format!("{:?}", ("jjjk")), front_again_solution("jjjk"), front_again("jjjk"));
    layout.print_test(format!("{:?}", ("x")), front_again_solution("x"), front_again("x"));
    layout.print_test(format!("{:?}", ("")), front_again_solution(""), front_again(""));
    layout.print_test(format!("{:?}", ("java")), front_again_solution("java"), front_again("java"));
    layout.print_test(format!("{:?}", ("javaja")), front_again_solution("javaja"), front_again("javaja"));
    layout.print_footer();
}

pub fn min_cat_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("min_cat");
    layout.print_header();
    layout.print_test(format!("{:?}", ("Hello", "Hi")), min_cat_solution("Hello", "Hi"), min_cat("Hello", "Hi"));
    layout.print_test(format!("{:?}", ("Hello", "java")), min_cat_solution("Hello", "java"), min_cat("Hello", "java"));
    layout.print_test(format!("{:?}", ("java", "Hello")), min_cat_solution("java", "Hello"), min_cat("java", "Hello"));
    layout.print_test(format!("{:?}", ("abc", "x")), min_cat_solution("abc", "x"), min_cat("abc", "x"));
    layout.print_test(format!("{:?}", ("x", "abc")), min_cat_solution("x", "abc"), min_cat("x", "abc"));
    layout.print_test(format!("{:?}", ("abc", "")), min_cat_solution("abc", ""), min_cat("abc", ""));
    layout.print_footer();
}

pub fn extra_front_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("extra_front");
    layout.print_header();
    layout.print_test(format!("{:?}", ("Hello")), extra_front_solution("Hello"), extra_front("Hello"));
    layout.print_test(format!("{:?}", ("ab")), extra_front_solution("ab"), extra_front("ab"));
    layout.print_test(format!("{:?}", ("H")), extra_front_solution("H"), extra_front("H"));
    layout.print_test(format!("{:?}", ("")), extra_front_solution(""), extra_front(""));
    layout.print_test(format!("{:?}", ("Candy")), extra_front_solution("Candy"), extra_front("Candy"));
    layout.print_test(format!("{:?}", ("Code")), extra_front_solution("Code"), extra_front("Code"));
    layout.print_footer();
}

pub fn without2_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("without2");
    layout.print_header();
    layout.print_test(format!("{:?}", ("HelloHe")), without2_solution("HelloHe"), without2("HelloHe"));
    layout.print_test(format!("{:?}", ("HelloHi")), without2_solution("HelloHi"), without2("HelloHi"));
    layout.print_test(format!("{:?}", ("Hi")), without2_solution("Hi"), without2("Hi"));
    layout.print_test(format!("{:?}", ("Chocolate")), without2_solution("Chocolate"), without2("Chocolate"));
    layout.print_test(format!("{:?}", ("xxx")), without2_solution("xxx"), without2("xxx"));
    layout.print_test(format!("{:?}", ("xx")), without2_solution("xx"), without2("xx"));
    layout.print_test(format!("{:?}", ("x")), without2_solution("x"), without2("x"));
    layout.print_test(format!("{:?}", ("")), without2_solution(""), without2(""));
    layout.print_test(format!("{:?}", ("Fruits")), without2_solution("Fruits"), without2("Fruits"));
    layout.print_footer();
}

pub fn de_front_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("de_front");
    layout.print_header();
    layout.print_test(format!("{:?}", ("Hello")), de_front_solution("Hello"), de_front("Hello"));
    layout.print_test(format!("{:?}", ("java")), de_front_solution("java"), de_front("java"));
    layout.print_test(format!("{:?}", ("away")), de_front_solution("away"), de_front("away"));
    layout.print_test(format!("{:?}", ("axy")), de_front_solution("axy"), de_front("axy"));
    layout.print_test(format!("{:?}", ("abc")), de_front_solution("abc"), de_front("abc"));
    layout.print_test(format!("{:?}", ("xby")), de_front_solution("xby"), de_front("xby"));
    layout.print_test(format!("{:?}", ("ab")), de_front_solution("ab"), de_front("ab"));
    layout.print_test(format!("{:?}", ("ax")), de_front_solution("ax"), de_front("ax"));
    layout.print_test(format!("{:?}", ("axb")), de_front_solution("axb"), de_front("axb"));
    layout.print_test(format!("{:?}", ("aaa")), de_front_solution("aaa"), de_front("aaa"));
    layout.print_test(format!("{:?}", ("xbc")), de_front_solution("xbc"), de_front("xbc"));
    layout.print_test(format!("{:?}", ("bbb")), de_front_solution("bbb"), de_front("bbb"));
    layout.print_test(format!("{:?}", ("bazz")), de_front_solution("bazz"), de_front("bazz"));
    layout.print_test(format!("{:?}", ("ba")), de_front_solution("ba"), de_front("ba"));
    layout.print_test(format!("{:?}", ("abxyz")), de_front_solution("abxyz"), de_front("abxyz"));
    layout.print_test(format!("{:?}", ("hi")), de_front_solution("hi"), de_front("hi"));
    layout.print_test(format!("{:?}", ("his")), de_front_solution("his"), de_front("his"));
    layout.print_test(format!("{:?}", ("xz")), de_front_solution("xz"), de_front("xz"));
    layout.print_test(format!("{:?}", ("zzz")), de_front_solution("zzz"), de_front("zzz"));
    layout.print_footer();
}

pub fn start_word_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("start_word");
    layout.print_header();
    layout.print_test(format!("{:?}", ("hippo", "hi")), start_word_solution("hippo", "hi"), start_word("hippo", "hi"));
    layout.print_test(format!("{:?}", ("hippo", "xip")), start_word_solution("hippo", "xip"), start_word("hippo", "xip"));
    layout.print_test(format!("{:?}", ("hippo", "i")), start_word_solution("hippo", "i"), start_word("hippo", "i"));
    layout.print_test(format!("{:?}", ("hippo", "ix")), start_word_solution("hippo", "ix"), start_word("hippo", "ix"));
    layout.print_test(format!("{:?}", ("h", "ix")), start_word_solution("h", "ix"), start_word("h", "ix"));
    layout.print_test(format!("{:?}", ("", "i")), start_word_solution("", "i"), start_word("", "i"));
    layout.print_test(format!("{:?}", ("hip", "zi")), start_word_solution("hip", "zi"), start_word("hip", "zi"));
    layout.print_test(format!("{:?}", ("hip", "zip")), start_word_solution("hip", "zip"), start_word("hip", "zip"));
    layout.print_test(format!("{:?}", ("hip", "zig")), start_word_solution("hip", "zig"), start_word("hip", "zig"));
    layout.print_test(format!("{:?}", ("h", "z")), start_word_solution("h", "z"), start_word("h", "z"));
    layout.print_test(format!("{:?}", ("hippo", "xippo")), start_word_solution("hippo", "xippo"), start_word("hippo", "xippo"));
    layout.print_test(format!("{:?}", ("hippo", "xyz")), start_word_solution("hippo", "xyz"), start_word("hippo", "xyz"));
    layout.print_test(format!("{:?}", ("hippo", "hip")), start_word_solution("hippo", "hip"), start_word("hippo", "hip"));
    layout.print_test(format!("{:?}", ("kitten", "cit")), start_word_solution("kitten", "cit"), start_word("kitten", "cit"));
    layout.print_test(format!("{:?}", ("kit", "cit")), start_word_solution("kit", "cit"), start_word("kit", "cit"));
    layout.print_footer();
}

pub fn without_x_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("without_x");
    layout.print_header();
    layout.print_test(format!("{:?}", ("xHix")), without_x_solution("xHix"), without_x("xHix"));
    layout.print_test(format!("{:?}", ("xHi")), without_x_solution("xHi"), without_x("xHi"));
    layout.print_test(format!("{:?}", ("Hxix")), without_x_solution("Hxix"), without_x("Hxix"));
    layout.print_test(format!("{:?}", ("Hi")), without_x_solution("Hi"), without_x("Hi"));
    layout.print_test(format!("{:?}", ("xxHi")), without_x_solution("xxHi"), without_x("xxHi"));
    layout.print_test(format!("{:?}", ("Hix")), without_x_solution("Hix"), without_x("Hix"));
    layout.print_test(format!("{:?}", ("xaxbx")), without_x_solution("xaxbx"), without_x("xaxbx"));
    layout.print_test(format!("{:?}", ("xx")), without_x_solution("xx"), without_x("xx"));
    layout.print_test(format!("{:?}", ("x")), without_x_solution("x"), without_x("x"));
    layout.print_test(format!("{:?}", ("")), without_x_solution(""), without_x(""));
    layout.print_test(format!("{:?}", ("Hello")), without_x_solution("Hello"), without_x("Hello"));
    layout.print_test(format!("{:?}", ("Hexllo")), without_x_solution("Hexllo"), without_x("Hexllo"));
    layout.print_footer();
}

pub fn without_x2_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("without_x2");
    layout.print_header();
    layout.print_test(format!("{:?}", ("xHi")), without_x2_solution("xHi"), without_x2("xHi"));
    layout.print_test(format!("{:?}", ("Hxi")), without_x2_solution("Hxi"), without_x2("Hxi"));
    layout.print_test(format!("{:?}", ("Hi")), without_x2_solution("Hi"), without_x2("Hi"));
    layout.print_test(format!("{:?}", ("xxHi")), without_x2_solution("xxHi"), without_x2("xxHi"));
    layout.print_test(format!("{:?}", ("Hix")), without_x2_solution("Hix"), without_x2("Hix"));
    layout.print_test(format!("{:?}", ("xaxb")), without_x2_solution("xaxb"), without_x2("xaxb"));
    layout.print_test(format!("{:?}", ("xx")), without_x2_solution("xx"), without_x2("xx"));
    layout.print_test(format!("{:?}", ("x")), without_x2_solution("x"), without_x2("x"));
    layout.print_test(format!("{:?}", ("")), without_x2_solution(""), without_x2(""));
    layout.print_test(format!("{:?}", ("Hello")), without_x2_solution("Hello"), without_x2("Hello"));
    layout.print_test(format!("{:?}", ("Hexllo")), without_x2_solution("Hexllo"), without_x2("Hexllo"));
    layout.print_test(format!("{:?}", ("xHxllo")), without_x2_solution("xHxllo"), without_x2("xHxllo"));
    layout.print_footer();
}

pub fn double_char_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("double_char");
    layout.print_header();
    layout.print_test(format!("{:?}", ("The")), double_char_solution("The"), double_char("The"));
    layout.print_test(format!("{:?}", ("AAbb")), double_char_solution("AAbb"), double_char("AAbb"));
    layout.print_test(format!("{:?}", ("Hi-There")), double_char_solution("Hi-There"), double_char("Hi-There"));
    layout.print_test(format!("{:?}", ("Word!")), double_char_solution("Word!"), double_char("Word!"));
    layout.print_test(format!("{:?}", ("!!")), double_char_solution("!!"), double_char("!!"));
    layout.print_test(format!("{:?}", ("")), double_char_solution(""), double_char(""));
    layout.print_test(format!("{:?}", ("a")), double_char_solution("a"), double_char("a"));
    layout.print_test(format!("{:?}", (".")), double_char_solution("."), double_char("."));
    layout.print_test(format!("{:?}", ("aa")), double_char_solution("aa"), double_char("aa"));
    layout.print_footer();
}

pub fn count_hi_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("count_hi");
    layout.print_header();
    layout.print_test(format!("{:?}", ("abc hi ho")), count_hi_solution("abc hi ho"), count_hi("abc hi ho"));
    layout.print_test(format!("{:?}", ("ABChi hi")), count_hi_solution("ABChi hi"), count_hi("ABChi hi"));
    layout.print_test(format!("{:?}", ("hihi")), count_hi_solution("hihi"), count_hi("hihi"));
    layout.print_test(format!("{:?}", ("hiHIhi")), count_hi_solution("hiHIhi"), count_hi("hiHIhi"));
    layout.print_test(format!("{:?}", ("")), count_hi_solution(""), count_hi(""));
    layout.print_test(format!("{:?}", ("h")), count_hi_solution("h"), count_hi("h"));
    layout.print_test(format!("{:?}", ("hi")), count_hi_solution("hi"), count_hi("hi"));
    layout.print_test(format!("{:?}", ("Hi is no HI on ahI")), count_hi_solution("Hi is no HI on ahI"), count_hi("Hi is no HI on ahI"));
    layout.print_test(format!("{:?}", ("hiho not HOHIhi")), count_hi_solution("hiho not HOHIhi"), count_hi("hiho not HOHIhi"));
    layout.print_footer();
}

pub fn cat_dog_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("cat_dog");
    layout.print_header();
    layout.print_test(format!("{:?}", ("catdog")), cat_dog_solution("catdog"), cat_dog("catdog"));
    layout.print_test(format!("{:?}", ("catcat")), cat_dog_solution("catcat"), cat_dog("catcat"));
    layout.print_test(format!("{:?}", ("1cat1cadodog")), cat_dog_solution("1cat1cadodog"), cat_dog("1cat1cadodog"));
    layout.print_test(format!("{:?}", ("catxxdogxxxdog")), cat_dog_solution("catxxdogxxxdog"), cat_dog("catxxdogxxxdog"));
    layout.print_test(format!("{:?}", ("catxdogxdogxcat")), cat_dog_solution("catxdogxdogxcat"), cat_dog("catxdogxdogxcat"));
    layout.print_test(format!("{:?}", ("catxdogxdogxca")), cat_dog_solution("catxdogxdogxca"), cat_dog("catxdogxdogxca"));
    layout.print_test(format!("{:?}", ("dogdogcat")), cat_dog_solution("dogdogcat"), cat_dog("dogdogcat"));
    layout.print_test(format!("{:?}", ("dogogcat")), cat_dog_solution("dogogcat"), cat_dog("dogogcat"));
    layout.print_test(format!("{:?}", ("dog")), cat_dog_solution("dog"), cat_dog("dog"));
    layout.print_test(format!("{:?}", ("cat")), cat_dog_solution("cat"), cat_dog("cat"));
    layout.print_test(format!("{:?}", ("ca")), cat_dog_solution("ca"), cat_dog("ca"));
    layout.print_test(format!("{:?}", ("c")), cat_dog_solution("c"), cat_dog("c"));
    layout.print_test(format!("{:?}", ("")), cat_dog_solution(""), cat_dog(""));
    layout.print_footer();
}

pub fn count_code_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("count_code");
    layout.print_header();
    layout.print_test(format!("{:?}", ("aaacodebbb")), count_code_solution("aaacodebbb"), count_code("aaacodebbb"));
    layout.print_test(format!("{:?}", ("codexxcode")), count_code_solution("codexxcode"), count_code("codexxcode"));
    layout.print_test(format!("{:?}", ("cozexxcope")), count_code_solution("cozexxcope"), count_code("cozexxcope"));
    layout.print_test(format!("{:?}", ("cozfxxcope")), count_code_solution("cozfxxcope"), count_code("cozfxxcope"));
    layout.print_test(format!("{:?}", ("xxcozeyycop")), count_code_solution("xxcozeyycop"), count_code("xxcozeyycop"));
    layout.print_test(format!("{:?}", ("cozcop")), count_code_solution("cozcop"), count_code("cozcop"));
    layout.print_test(format!("{:?}", ("abcxyz")), count_code_solution("abcxyz"), count_code("abcxyz"));
    layout.print_test(format!("{:?}", ("code")), count_code_solution("code"), count_code("code"));
    layout.print_test(format!("{:?}", ("ode")), count_code_solution("ode"), count_code("ode"));
    layout.print_test(format!("{:?}", ("c")), count_code_solution("c"), count_code("c"));
    layout.print_test(format!("{:?}", ("")), count_code_solution(""), count_code(""));
    layout.print_test(format!("{:?}", ("AAcodeBBcoleCCccoreDD")), count_code_solution("AAcodeBBcoleCCccoreDD"), count_code("AAcodeBBcoleCCccoreDD"));
    layout.print_test(format!("{:?}", ("AAcodeBBcoleCCccorfDD")), count_code_solution("AAcodeBBcoleCCccorfDD"), count_code("AAcodeBBcoleCCccorfDD"));
    layout.print_test(format!("{:?}", ("coAcodeBcoleccoreDD")), count_code_solution("coAcodeBcoleccoreDD"), count_code("coAcodeBcoleccoreDD"));
    layout.print_footer();
}

pub fn end_other_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("end_other");
    layout.print_header();
    layout.print_test(format!("{:?}", ("Hiabc", "abc")), end_other_solution("Hiabc", "abc"), end_other("Hiabc", "abc"));
    layout.print_test(format!("{:?}", ("AbC", "HiaBc")), end_other_solution("AbC", "HiaBc"), end_other("AbC", "HiaBc"));
    layout.print_test(format!("{:?}", ("abc", "abXabc")), end_other_solution("abc", "abXabc"), end_other("abc", "abXabc"));
    layout.print_test(format!("{:?}", ("Hiabc", "abcd")), end_other_solution("Hiabc", "abcd"), end_other("Hiabc", "abcd"));
    layout.print_test(format!("{:?}", ("Hiabc", "bc")), end_other_solution("Hiabc", "bc"), end_other("Hiabc", "bc"));
    layout.print_test(format!("{:?}", ("Hiabcx", "bc")), end_other_solution("Hiabcx", "bc"), end_other("Hiabcx", "bc"));
    layout.print_test(format!("{:?}", ("abc", "abc")), end_other_solution("abc", "abc"), end_other("abc", "abc"));
    layout.print_test(format!("{:?}", ("xyz", "12xyz")), end_other_solution("xyz", "12xyz"), end_other("xyz", "12xyz"));
    layout.print_test(format!("{:?}", ("yz", "12xz")), end_other_solution("yz", "12xz"), end_other("yz", "12xz"));
    layout.print_test(format!("{:?}", ("Z", "12xz")), end_other_solution("Z", "12xz"), end_other("Z", "12xz"));
    layout.print_test(format!("{:?}", ("12", "12")), end_other_solution("12", "12"), end_other("12", "12"));
    layout.print_test(format!("{:?}", ("abcXYZ", "abcDEF")), end_other_solution("abcXYZ", "abcDEF"), end_other("abcXYZ", "abcDEF"));
    layout.print_test(format!("{:?}", ("ab", "ab12")), end_other_solution("ab", "ab12"), end_other("ab", "ab12"));
    layout.print_test(format!("{:?}", ("ab", "12ab")), end_other_solution("ab", "12ab"), end_other("ab", "12ab"));
    layout.print_footer();
}

pub fn xyz_there_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("xyz_there");
    layout.print_header();
    layout.print_test(format!("{:?}", ("abcxyz")), xyz_there_solution("abcxyz"), xyz_there("abcxyz"));
    layout.print_test(format!("{:?}", ("abc.xyz")), xyz_there_solution("abc.xyz"), xyz_there("abc.xyz"));
    layout.print_test(format!("{:?}", ("xyz.abc")), xyz_there_solution("xyz.abc"), xyz_there("xyz.abc"));
    layout.print_test(format!("{:?}", ("abcxy")), xyz_there_solution("abcxy"), xyz_there("abcxy"));
    layout.print_test(format!("{:?}", ("xyz")), xyz_there_solution("xyz"), xyz_there("xyz"));
    layout.print_test(format!("{:?}", ("xy")), xyz_there_solution("xy"), xyz_there("xy"));
    layout.print_test(format!("{:?}", ("x")), xyz_there_solution("x"), xyz_there("x"));
    layout.print_test(format!("{:?}", ("")), xyz_there_solution(""), xyz_there(""));
    layout.print_test(format!("{:?}", ("abc.xyzxyz")), xyz_there_solution("abc.xyzxyz"), xyz_there("abc.xyzxyz"));
    layout.print_test(format!("{:?}", ("abc.xxyz")), xyz_there_solution("abc.xxyz"), xyz_there("abc.xxyz"));
    layout.print_test(format!("{:?}", (".xyz")), xyz_there_solution(".xyz"), xyz_there(".xyz"));
    layout.print_test(format!("{:?}", ("12.xyz")), xyz_there_solution("12.xyz"), xyz_there("12.xyz"));
    layout.print_test(format!("{:?}", ("12xyz")), xyz_there_solution("12xyz"), xyz_there("12xyz"));
    layout.print_test(format!("{:?}", ("1.xyz.xyz2.xyz")), xyz_there_solution("1.xyz.xyz2.xyz"), xyz_there("1.xyz.xyz2.xyz"));
    layout.print_footer();
}

pub fn bob_there_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("bob_there");
    layout.print_header();
    layout.print_test(format!("{:?}", ("abcbob")), bob_there_solution("abcbob"), bob_there("abcbob"));
    layout.print_test(format!("{:?}", ("b9b")), bob_there_solution("b9b"), bob_there("b9b"));
    layout.print_test(format!("{:?}", ("bac")), bob_there_solution("bac"), bob_there("bac"));
    layout.print_test(format!("{:?}", ("bbb")), bob_there_solution("bbb"), bob_there("bbb"));
    layout.print_test(format!("{:?}", ("abcdefb")), bob_there_solution("abcdefb"), bob_there("abcdefb"));
    layout.print_test(format!("{:?}", ("123abcbcdbabxyz")), bob_there_solution("123abcbcdbabxyz"), bob_there("123abcbcdbabxyz"));
    layout.print_test(format!("{:?}", ("b12")), bob_there_solution("b12"), bob_there("b12"));
    layout.print_test(format!("{:?}", ("b1b")), bob_there_solution("b1b"), bob_there("b1b"));
    layout.print_test(format!("{:?}", ("b12b1b")), bob_there_solution("b12b1b"), bob_there("b12b1b"));
    layout.print_test(format!("{:?}", ("bbc")), bob_there_solution("bbc"), bob_there("bbc"));
    layout.print_test(format!("{:?}", ("bbb")), bob_there_solution("bbb"), bob_there("bbb"));
    layout.print_test(format!("{:?}", ("bb")), bob_there_solution("bb"), bob_there("bb"));
    layout.print_test(format!("{:?}", ("b")), bob_there_solution("b"), bob_there("b"));
    layout.print_footer();
}

pub fn xy_balance_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("xy_balance");
    layout.print_header();
    layout.print_test(format!("{:?}", ("aaxbby")), xy_balance_solution("aaxbby"), xy_balance("aaxbby"));
    layout.print_test(format!("{:?}", ("aaxbb")), xy_balance_solution("aaxbb"), xy_balance("aaxbb"));
    layout.print_test(format!("{:?}", ("yaaxbb")), xy_balance_solution("yaaxbb"), xy_balance("yaaxbb"));
    layout.print_test(format!("{:?}", ("yaaxbby")), xy_balance_solution("yaaxbby"), xy_balance("yaaxbby"));
    layout.print_test(format!("{:?}", ("xaxxbby")), xy_balance_solution("xaxxbby"), xy_balance("xaxxbby"));
    layout.print_test(format!("{:?}", ("xaxxbbyx")), xy_balance_solution("xaxxbbyx"), xy_balance("xaxxbbyx"));
    layout.print_test(format!("{:?}", ("xxbxy")), xy_balance_solution("xxbxy"), xy_balance("xxbxy"));
    layout.print_test(format!("{:?}", ("xxbx")), xy_balance_solution("xxbx"), xy_balance("xxbx"));
    layout.print_test(format!("{:?}", ("bbb")), xy_balance_solution("bbb"), xy_balance("bbb"));
    layout.print_test(format!("{:?}", ("bxbb")), xy_balance_solution("bxbb"), xy_balance("bxbb"));
    layout.print_test(format!("{:?}", ("bxyb")), xy_balance_solution("bxyb"), xy_balance("bxyb"));
    layout.print_test(format!("{:?}", ("xy")), xy_balance_solution("xy"), xy_balance("xy"));
    layout.print_test(format!("{:?}", ("y")), xy_balance_solution("y"), xy_balance("y"));
    layout.print_test(format!("{:?}", ("x")), xy_balance_solution("x"), xy_balance("x"));
    layout.print_test(format!("{:?}", ("")), xy_balance_solution(""), xy_balance(""));
    layout.print_test(format!("{:?}", ("yxyxyxyx")), xy_balance_solution("yxyxyxyx"), xy_balance("yxyxyxyx"));
    layout.print_test(format!("{:?}", ("yxyxyxyxy")), xy_balance_solution("yxyxyxyxy"), xy_balance("yxyxyxyxy"));
    layout.print_test(format!("{:?}", ("12xabxxydxyxyzz")), xy_balance_solution("12xabxxydxyxyzz"), xy_balance("12xabxxydxyxyzz"));
    layout.print_footer();
}

pub fn mix_string_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("mix_string");
    layout.print_header();
    layout.print_test(format!("{:?}", ("abc", "xyz")), mix_string_solution("abc", "xyz"), mix_string("abc", "xyz"));
    layout.print_test(format!("{:?}", ("Hi", "There")), mix_string_solution("Hi", "There"), mix_string("Hi", "There"));
    layout.print_test(format!("{:?}", ("xxxx", "There")), mix_string_solution("xxxx", "There"), mix_string("xxxx", "There"));
    layout.print_test(format!("{:?}", ("xxx", "X")), mix_string_solution("xxx", "X"), mix_string("xxx", "X"));
    layout.print_test(format!("{:?}", ("2/", "27 around")), mix_string_solution("2/", "27 around"), mix_string("2/", "27 around"));
    layout.print_test(format!("{:?}", ("", "Hello")), mix_string_solution("", "Hello"), mix_string("", "Hello"));
    layout.print_test(format!("{:?}", ("Abc", "")), mix_string_solution("Abc", ""), mix_string("Abc", ""));
    layout.print_test(format!("{:?}", ("", "")), mix_string_solution("", ""), mix_string("", ""));
    layout.print_test(format!("{:?}", ("a", "b")), mix_string_solution("a", "b"), mix_string("a", "b"));
    layout.print_test(format!("{:?}", ("ax", "b")), mix_string_solution("ax", "b"), mix_string("ax", "b"));
    layout.print_test(format!("{:?}", ("a", "bx")), mix_string_solution("a", "bx"), mix_string("a", "bx"));
    layout.print_test(format!("{:?}", ("So", "Long")), mix_string_solution("So", "Long"), mix_string("So", "Long"));
    layout.print_test(format!("{:?}", ("Long", "So")), mix_string_solution("Long", "So"), mix_string("Long", "So"));
    layout.print_footer();
}

pub fn repeat_end_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("repeat_end");
    layout.print_header();
    layout.print_test(format!("{:?}", ("Hello", 3)), repeat_end_solution("Hello", 3), repeat_end("Hello", 3));
    layout.print_test(format!("{:?}", ("Hello", 2)), repeat_end_solution("Hello", 2), repeat_end("Hello", 2));
    layout.print_test(format!("{:?}", ("Hello", 1)), repeat_end_solution("Hello", 1), repeat_end("Hello", 1));
    layout.print_test(format!("{:?}", ("Hello", 0)), repeat_end_solution("Hello", 0), repeat_end("Hello", 0));
    layout.print_test(format!("{:?}", ("abc", 3)), repeat_end_solution("abc", 3), repeat_end("abc", 3));
    layout.print_test(format!("{:?}", ("1234", 2)), repeat_end_solution("1234", 2), repeat_end("1234", 2));
    layout.print_test(format!("{:?}", ("1234", 3)), repeat_end_solution("1234", 3), repeat_end("1234", 3));
    layout.print_test(format!("{:?}", ("", 0)), repeat_end_solution("", 0), repeat_end("", 0));
    layout.print_footer();
}

pub fn repeat_front_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("repeat_front");
    layout.print_header();
    layout.print_test(format!("{:?}", ("Chocolate", 4)), repeat_front_solution("Chocolate", 4), repeat_front("Chocolate", 4));
    layout.print_test(format!("{:?}", ("Chocolate", 3)), repeat_front_solution("Chocolate", 3), repeat_front("Chocolate", 3));
    layout.print_test(format!("{:?}", ("Ice Cream", 2)), repeat_front_solution("Ice Cream", 2), repeat_front("Ice Cream", 2));
    layout.print_test(format!("{:?}", ("Ice Cream", 1)), repeat_front_solution("Ice Cream", 1), repeat_front("Ice Cream", 1));
    layout.print_test(format!("{:?}", ("Ice Cream", 0)), repeat_front_solution("Ice Cream", 0), repeat_front("Ice Cream", 0));
    layout.print_test(format!("{:?}", ("xyz", 3)), repeat_front_solution("xyz", 3), repeat_front("xyz", 3));
    layout.print_test(format!("{:?}", ("", 0)), repeat_front_solution("", 0), repeat_front("", 0));
    layout.print_test(format!("{:?}", ("Java", 4)), repeat_front_solution("Java", 4), repeat_front("Java", 4));
    layout.print_test(format!("{:?}", ("Java", 1)), repeat_front_solution("Java", 1), repeat_front("Java", 1));
    layout.print_footer();
}

pub fn repeat_separator_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("repeat_separator");
    layout.print_header();
    layout.print_test(format!("{:?}", ("Word", "X", 3)), repeat_separator_solution("Word", "X", 3), repeat_separator("Word", "X", 3));
    layout.print_test(format!("{:?}", ("This", "And", 2)), repeat_separator_solution("This", "And", 2), repeat_separator("This", "And", 2));
    layout.print_test(format!("{:?}", ("This", "And", 1)), repeat_separator_solution("This", "And", 1), repeat_separator("This", "And", 1));
    layout.print_test(format!("{:?}", ("Hi", "-n-", 2)), repeat_separator_solution("Hi", "-n-", 2), repeat_separator("Hi", "-n-", 2));
    layout.print_test(format!("{:?}", ("AAA", "", 1)), repeat_separator_solution("AAA", "", 1), repeat_separator("AAA", "", 1));
    layout.print_test(format!("{:?}", ("AAA", "", 0)), repeat_separator_solution("AAA", "", 0), repeat_separator("AAA", "", 0));
    layout.print_test(format!("{:?}", ("A", "B", 5)), repeat_separator_solution("A", "B", 5), repeat_separator("A", "B", 5));
    layout.print_test(format!("{:?}", ("abc", "XX", 3)), repeat_separator_solution("abc", "XX", 3), repeat_separator("abc", "XX", 3));
    layout.print_test(format!("{:?}", ("abc", "XX", 2)), repeat_separator_solution("abc", "XX", 2), repeat_separator("abc", "XX", 2));
    layout.print_test(format!("{:?}", ("abc", "XX", 1)), repeat_separator_solution("abc", "XX", 1), repeat_separator("abc", "XX", 1));
    layout.print_test(format!("{:?}", ("XYZ", "a", 2)), repeat_separator_solution("XYZ", "a", 2), repeat_separator("XYZ", "a", 2));
    layout.print_footer();
}

pub fn prefix_again_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("prefix_again");
    layout.print_header();
    layout.print_test(format!("{:?}", ("abXYabc", 1)), prefix_again_solution("abXYabc", 1), prefix_again("abXYabc", 1));
    layout.print_test(format!("{:?}", ("abXYabc", 2)), prefix_again_solution("abXYabc", 2), prefix_again("abXYabc", 2));
    layout.print_test(format!("{:?}", ("abXYabc", 3)), prefix_again_solution("abXYabc", 3), prefix_again("abXYabc", 3));
    layout.print_test(format!("{:?}", ("xyzxyxyxy", 2)), prefix_again_solution("xyzxyxyxy", 2), prefix_again("xyzxyxyxy", 2));
    layout.print_test(format!("{:?}", ("xyzxyxyxy", 3)), prefix_again_solution("xyzxyxyxy", 3), prefix_again("xyzxyxyxy", 3));
    layout.print_test(format!("{:?}", ("Hi12345Hi6789Hi10", 1)), prefix_again_solution("Hi12345Hi6789Hi10", 1), prefix_again("Hi12345Hi6789Hi10", 1));
    layout.print_test(format!("{:?}", ("Hi12345Hi6789Hi10", 2)), prefix_again_solution("Hi12345Hi6789Hi10", 2), prefix_again("Hi12345Hi6789Hi10", 2));
    layout.print_test(format!("{:?}", ("Hi12345Hi6789Hi10", 3)), prefix_again_solution("Hi12345Hi6789Hi10", 3), prefix_again("Hi12345Hi6789Hi10", 3));
    layout.print_test(format!("{:?}", ("Hi12345Hi6789Hi10", 4)), prefix_again_solution("Hi12345Hi6789Hi10", 4), prefix_again("Hi12345Hi6789Hi10", 4));
    layout.print_test(format!("{:?}", ("a", 1)), prefix_again_solution("a", 1), prefix_again("a", 1));
    layout.print_test(format!("{:?}", ("aa", 1)), prefix_again_solution("aa", 1), prefix_again("aa", 1));
    layout.print_test(format!("{:?}", ("ab", 1)), prefix_again_solution("ab", 1), prefix_again("ab", 1));
    layout.print_footer();
}

pub fn xyz_middle_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("xyz_middle");
    layout.print_header();
    layout.print_test(format!("{:?}", ("AAxyzBB")), xyz_middle_solution("AAxyzBB"), xyz_middle("AAxyzBB"));
    layout.print_test(format!("{:?}", ("AxyzBB")), xyz_middle_solution("AxyzBB"), xyz_middle("AxyzBB"));
    layout.print_test(format!("{:?}", ("AxyzBBB")), xyz_middle_solution("AxyzBBB"), xyz_middle("AxyzBBB"));
    layout.print_test(format!("{:?}", ("AxyzBBBB")), xyz_middle_solution("AxyzBBBB"), xyz_middle("AxyzBBBB"));
    layout.print_test(format!("{:?}", ("AAAxyzB")), xyz_middle_solution("AAAxyzB"), xyz_middle("AAAxyzB"));
    layout.print_test(format!("{:?}", ("AAAxyzBB")), xyz_middle_solution("AAAxyzBB"), xyz_middle("AAAxyzBB"));
    layout.print_test(format!("{:?}", ("AAAAxyzBB")), xyz_middle_solution("AAAAxyzBB"), xyz_middle("AAAAxyzBB"));
    layout.print_test(format!("{:?}", ("AAAAAxyzBBB")), xyz_middle_solution("AAAAAxyzBBB"), xyz_middle("AAAAAxyzBBB"));
    layout.print_test(format!("{:?}", ("1x345xyz12x4")), xyz_middle_solution("1x345xyz12x4"), xyz_middle("1x345xyz12x4"));
    layout.print_test(format!("{:?}", ("xyzAxyzBBB")), xyz_middle_solution("xyzAxyzBBB"), xyz_middle("xyzAxyzBBB"));
    layout.print_test(format!("{:?}", ("xyzAxyzBxyz")), xyz_middle_solution("xyzAxyzBxyz"), xyz_middle("xyzAxyzBxyz"));
    layout.print_test(format!("{:?}", ("xyzxyzAxyzBxyzxyz")), xyz_middle_solution("xyzxyzAxyzBxyzxyz"), xyz_middle("xyzxyzAxyzBxyzxyz"));
    layout.print_test(format!("{:?}", ("xyzxyzxyzBxyzxyz")), xyz_middle_solution("xyzxyzxyzBxyzxyz"), xyz_middle("xyzxyzxyzBxyzxyz"));
    layout.print_test(format!("{:?}", ("xyzxyzAxyzxyzxyz")), xyz_middle_solution("xyzxyzAxyzxyzxyz"), xyz_middle("xyzxyzAxyzxyzxyz"));
    layout.print_test(format!("{:?}", ("xyzxyzAxyzxyzxy")), xyz_middle_solution("xyzxyzAxyzxyzxy"), xyz_middle("xyzxyzAxyzxyzxy"));
    layout.print_test(format!("{:?}", ("AxyzxyzBB")), xyz_middle_solution("AxyzxyzBB"), xyz_middle("AxyzxyzBB"));
    layout.print_test(format!("{:?}", ("")), xyz_middle_solution(""), xyz_middle(""));
    layout.print_test(format!("{:?}", ("x")), xyz_middle_solution("x"), xyz_middle("x"));
    layout.print_test(format!("{:?}", ("xy")), xyz_middle_solution("xy"), xyz_middle("xy"));
    layout.print_test(format!("{:?}", ("xyz")), xyz_middle_solution("xyz"), xyz_middle("xyz"));
    layout.print_test(format!("{:?}", ("xyzz")), xyz_middle_solution("xyzz"), xyz_middle("xyzz"));
    layout.print_footer();
}

pub fn get_sandwich_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("get_sandwich");
    layout.print_header();
    layout.print_test(format!("{:?}", ("breadjambread")), get_sandwich_solution("breadjambread"), get_sandwich("breadjambread"));
    layout.print_test(format!("{:?}", ("xxbreadjambreadyy")), get_sandwich_solution("xxbreadjambreadyy"), get_sandwich("xxbreadjambreadyy"));
    layout.print_test(format!("{:?}", ("xxbreadyy")), get_sandwich_solution("xxbreadyy"), get_sandwich("xxbreadyy"));
    layout.print_test(format!("{:?}", ("xxbreadbreadjambreadyy")), get_sandwich_solution("xxbreadbreadjambreadyy"), get_sandwich("xxbreadbreadjambreadyy"));
    layout.print_test(format!("{:?}", ("breadAbread")), get_sandwich_solution("breadAbread"), get_sandwich("breadAbread"));
    layout.print_test(format!("{:?}", ("breadbread")), get_sandwich_solution("breadbread"), get_sandwich("breadbread"));
    layout.print_test(format!("{:?}", ("abcbreaz")), get_sandwich_solution("abcbreaz"), get_sandwich("abcbreaz"));
    layout.print_test(format!("{:?}", ("xyz")), get_sandwich_solution("xyz"), get_sandwich("xyz"));
    layout.print_test(format!("{:?}", ("")), get_sandwich_solution(""), get_sandwich(""));
    layout.print_test(format!("{:?}", ("breadbreaxbread")), get_sandwich_solution("breadbreaxbread"), get_sandwich("breadbreaxbread"));
    layout.print_test(format!("{:?}", ("breaxbreadybread")), get_sandwich_solution("breaxbreadybread"), get_sandwich("breaxbreadybread"));
    layout.print_test(format!("{:?}", ("breadbreadbreadbread")), get_sandwich_solution("breadbreadbreadbread"), get_sandwich("breadbreadbreadbread"));
    layout.print_footer();
}

pub fn same_star_char_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("same_star_char");
    layout.print_header();
    layout.print_test(format!("{:?}", ("xy*yzz")), same_star_char_solution("xy*yzz"), same_star_char("xy*yzz"));
    layout.print_test(format!("{:?}", ("xy*zzz")), same_star_char_solution("xy*zzz"), same_star_char("xy*zzz"));
    layout.print_test(format!("{:?}", ("*xa*az")), same_star_char_solution("*xa*az"), same_star_char("*xa*az"));
    layout.print_test(format!("{:?}", ("*xa*bz")), same_star_char_solution("*xa*bz"), same_star_char("*xa*bz"));
    layout.print_test(format!("{:?}", ("*xa*a*")), same_star_char_solution("*xa*a*"), same_star_char("*xa*a*"));
    layout.print_test(format!("{:?}", ("")), same_star_char_solution(""), same_star_char(""));
    layout.print_test(format!("{:?}", ("*xa*a*a")), same_star_char_solution("*xa*a*a"), same_star_char("*xa*a*a"));
    layout.print_test(format!("{:?}", ("*xa*a*b")), same_star_char_solution("*xa*a*b"), same_star_char("*xa*a*b"));
    layout.print_test(format!("{:?}", ("*12*2*2")), same_star_char_solution("*12*2*2"), same_star_char("*12*2*2"));
    layout.print_test(format!("{:?}", ("12*2*3*")), same_star_char_solution("12*2*3*"), same_star_char("12*2*3*"));
    layout.print_test(format!("{:?}", ("abcDEF")), same_star_char_solution("abcDEF"), same_star_char("abcDEF"));
    layout.print_test(format!("{:?}", ("XY*YYYY*Z*")), same_star_char_solution("XY*YYYY*Z*"), same_star_char("XY*YYYY*Z*"));
    layout.print_test(format!("{:?}", ("XY*YYYY*Y*")), same_star_char_solution("XY*YYYY*Y*"), same_star_char("XY*YYYY*Y*"));
    layout.print_test(format!("{:?}", ("12*2*3*")), same_star_char_solution("12*2*3*"), same_star_char("12*2*3*"));
    layout.print_test(format!("{:?}", ("*")), same_star_char_solution("*"), same_star_char("*"));
    layout.print_test(format!("{:?}", ("**")), same_star_char_solution("**"), same_star_char("**"));
    layout.print_footer();
}

pub fn one_two_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("one_two");
    layout.print_header();
    layout.print_test(format!("{:?}", ("abc")), one_two_solution("abc"), one_two("abc"));
    layout.print_test(format!("{:?}", ("tca")), one_two_solution("tca"), one_two("tca"));
    layout.print_test(format!("{:?}", ("tcagdo")), one_two_solution("tcagdo"), one_two("tcagdo"));
    layout.print_test(format!("{:?}", ("chocolate")), one_two_solution("chocolate"), one_two("chocolate"));
    layout.print_test(format!("{:?}", ("1234567890")), one_two_solution("1234567890"), one_two("1234567890"));
    layout.print_test(format!("{:?}", ("xabxabxabxabxabxabxab")), one_two_solution("xabxabxabxabxabxabxab"), one_two("xabxabxabxabxabxabxab"));
    layout.print_test(format!("{:?}", ("abcdefx")), one_two_solution("abcdefx"), one_two("abcdefx"));
    layout.print_test(format!("{:?}", ("abcdefxy")), one_two_solution("abcdefxy"), one_two("abcdefxy"));
    layout.print_test(format!("{:?}", ("abcdefxyz")), one_two_solution("abcdefxyz"), one_two("abcdefxyz"));
    layout.print_test(format!("{:?}", ("")), one_two_solution(""), one_two(""));
    layout.print_test(format!("{:?}", ("x")), one_two_solution("x"), one_two("x"));
    layout.print_test(format!("{:?}", ("xy")), one_two_solution("xy"), one_two("xy"));
    layout.print_test(format!("{:?}", ("xyz")), one_two_solution("xyz"), one_two("xyz"));
    layout.print_test(format!("{:?}", ("abcdefghijklkmnopqrstuvwxyz1234567890")), one_two_solution("abcdefghijklkmnopqrstuvwxyz1234567890"), one_two("abcdefghijklkmnopqrstuvwxyz1234567890"));
    layout.print_test(format!("{:?}", ("abcdefghijklkmnopqrstuvwxyz123456789")), one_two_solution("abcdefghijklkmnopqrstuvwxyz123456789"), one_two("abcdefghijklkmnopqrstuvwxyz123456789"));
    layout.print_test(format!("{:?}", ("abcdefghijklkmnopqrstuvwxyz12345678")), one_two_solution("abcdefghijklkmnopqrstuvwxyz12345678"), one_two("abcdefghijklkmnopqrstuvwxyz12345678"));
    layout.print_footer();
}

pub fn zip_zap_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("zip_zap");
    layout.print_header();
    layout.print_test(format!("{:?}", ("zipXzap")), zip_zap_solution("zipXzap"), zip_zap("zipXzap"));
    layout.print_test(format!("{:?}", ("zopzop")), zip_zap_solution("zopzop"), zip_zap("zopzop"));
    layout.print_test(format!("{:?}", ("zzzopzop")), zip_zap_solution("zzzopzop"), zip_zap("zzzopzop"));
    layout.print_test(format!("{:?}", ("zibzap")), zip_zap_solution("zibzap"), zip_zap("zibzap"));
    layout.print_test(format!("{:?}", ("zip")), zip_zap_solution("zip"), zip_zap("zip"));
    layout.print_test(format!("{:?}", ("zi")), zip_zap_solution("zi"), zip_zap("zi"));
    layout.print_test(format!("{:?}", ("z")), zip_zap_solution("z"), zip_zap("z"));
    layout.print_test(format!("{:?}", ("")), zip_zap_solution(""), zip_zap(""));
    layout.print_test(format!("{:?}", ("zzp")), zip_zap_solution("zzp"), zip_zap("zzp"));
    layout.print_test(format!("{:?}", ("abcppp")), zip_zap_solution("abcppp"), zip_zap("abcppp"));
    layout.print_test(format!("{:?}", ("azbcppp")), zip_zap_solution("azbcppp"), zip_zap("azbcppp"));
    layout.print_test(format!("{:?}", ("azbcpzpp")), zip_zap_solution("azbcpzpp"), zip_zap("azbcpzpp"));
    layout.print_footer();
}

pub fn star_out_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("star_out");
    layout.print_header();
    layout.print_test(format!("{:?}", ("ab*cd")), star_out_solution("ab*cd"), star_out("ab*cd"));
    layout.print_test(format!("{:?}", ("ab**cd")), star_out_solution("ab**cd"), star_out("ab**cd"));
    layout.print_test(format!("{:?}", ("sm*eilly")), star_out_solution("sm*eilly"), star_out("sm*eilly"));
    layout.print_test(format!("{:?}", ("sm*eil*ly")), star_out_solution("sm*eil*ly"), star_out("sm*eil*ly"));
    layout.print_test(format!("{:?}", ("sm**eil*ly")), star_out_solution("sm**eil*ly"), star_out("sm**eil*ly"));
    layout.print_test(format!("{:?}", ("sm***eil*ly")), star_out_solution("sm***eil*ly"), star_out("sm***eil*ly"));
    layout.print_test(format!("{:?}", ("stringy*")), star_out_solution("stringy*"), star_out("stringy*"));
    layout.print_test(format!("{:?}", ("*stringy")), star_out_solution("*stringy"), star_out("*stringy"));
    layout.print_test(format!("{:?}", ("*str*in*gy")), star_out_solution("*str*in*gy"), star_out("*str*in*gy"));
    layout.print_test(format!("{:?}", ("abc")), star_out_solution("abc"), star_out("abc"));
    layout.print_test(format!("{:?}", ("a*bc")), star_out_solution("a*bc"), star_out("a*bc"));
    layout.print_test(format!("{:?}", ("ab")), star_out_solution("ab"), star_out("ab"));
    layout.print_test(format!("{:?}", ("a*b")), star_out_solution("a*b"), star_out("a*b"));
    layout.print_test(format!("{:?}", ("a")), star_out_solution("a"), star_out("a"));
    layout.print_test(format!("{:?}", ("a*")), star_out_solution("a*"), star_out("a*"));
    layout.print_test(format!("{:?}", ("*a")), star_out_solution("*a"), star_out("*a"));
    layout.print_test(format!("{:?}", ("*")), star_out_solution("*"), star_out("*"));
    layout.print_test(format!("{:?}", ("")), star_out_solution(""), star_out(""));
    layout.print_footer();
}

pub fn plus_out_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("plus_out");
    layout.print_header();
    layout.print_test(format!("{:?}", ("12xy34", "xy")), plus_out_solution("12xy34", "xy"), plus_out("12xy34", "xy"));
    layout.print_test(format!("{:?}", ("12xy34", "1")), plus_out_solution("12xy34", "1"), plus_out("12xy34", "1"));
    layout.print_test(format!("{:?}", ("12xy34xyabcxy", "xy")), plus_out_solution("12xy34xyabcxy", "xy"), plus_out("12xy34xyabcxy", "xy"));
    layout.print_test(format!("{:?}", ("abXYabcXYZ", "ab")), plus_out_solution("abXYabcXYZ", "ab"), plus_out("abXYabcXYZ", "ab"));
    layout.print_test(format!("{:?}", ("abXYabcXYZ", "abc")), plus_out_solution("abXYabcXYZ", "abc"), plus_out("abXYabcXYZ", "abc"));
    layout.print_test(format!("{:?}", ("abXYabcXYZ", "XY")), plus_out_solution("abXYabcXYZ", "XY"), plus_out("abXYabcXYZ", "XY"));
    layout.print_test(format!("{:?}", ("abXYxyzXYZ", "XYZ")), plus_out_solution("abXYxyzXYZ", "XYZ"), plus_out("abXYxyzXYZ", "XYZ"));
    layout.print_test(format!("{:?}", ("--++ab", "++")), plus_out_solution("--++ab", "++"), plus_out("--++ab", "++"));
    layout.print_test(format!("{:?}", ("aaxxxxbb", "xx")), plus_out_solution("aaxxxxbb", "xx"), plus_out("aaxxxxbb", "xx"));
    layout.print_test(format!("{:?}", ("123123", "3")), plus_out_solution("123123", "3"), plus_out("123123", "3"));
    layout.print_footer();
}

pub fn word_ends_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("word_ends");
    layout.print_header();
    layout.print_test(format!("{:?}", ("abcXY123XYijk", "XY")), word_ends_solution("abcXY123XYijk", "XY"), word_ends("abcXY123XYijk", "XY"));
    layout.print_test(format!("{:?}", ("XY123XY", "XY")), word_ends_solution("XY123XY", "XY"), word_ends("XY123XY", "XY"));
    layout.print_test(format!("{:?}", ("XY1XY", "XY")), word_ends_solution("XY1XY", "XY"), word_ends("XY1XY", "XY"));
    layout.print_test(format!("{:?}", ("XYXY", "XY")), word_ends_solution("XYXY", "XY"), word_ends("XYXY", "XY"));
    layout.print_test(format!("{:?}", ("XY", "XY")), word_ends_solution("XY", "XY"), word_ends("XY", "XY"));
    layout.print_test(format!("{:?}", ("Hi", "XY")), word_ends_solution("Hi", "XY"), word_ends("Hi", "XY"));
    layout.print_test(format!("{:?}", ("", "XY")), word_ends_solution("", "XY"), word_ends("", "XY"));
    layout.print_test(format!("{:?}", ("abc1xyz1i1j", "1")), word_ends_solution("abc1xyz1i1j", "1"), word_ends("abc1xyz1i1j", "1"));
    layout.print_test(format!("{:?}", ("abc1xyz1", "1")), word_ends_solution("abc1xyz1", "1"), word_ends("abc1xyz1", "1"));
    layout.print_test(format!("{:?}", ("abc1xyz11", "1")), word_ends_solution("abc1xyz11", "1"), word_ends("abc1xyz11", "1"));
    layout.print_test(format!("{:?}", ("abc1xyz1abc", "abc")), word_ends_solution("abc1xyz1abc", "abc"), word_ends("abc1xyz1abc", "abc"));
    layout.print_test(format!("{:?}", ("abc1xyz1abc", "b")), word_ends_solution("abc1xyz1abc", "b"), word_ends("abc1xyz1abc", "b"));
    layout.print_test(format!("{:?}", ("abc1abc1abc", "abc")), word_ends_solution("abc1abc1abc", "abc"), word_ends("abc1abc1abc", "abc"));
    layout.print_footer();
}

pub fn count_y_z_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("count_y_z");
    layout.print_header();
    layout.print_test(format!("{:?}", ("fez day")), count_y_z_solution("fez day"), count_y_z("fez day"));
    layout.print_test(format!("{:?}", ("day fez")), count_y_z_solution("day fez"), count_y_z("day fez"));
    layout.print_test(format!("{:?}", ("day fyyyz")), count_y_z_solution("day fyyyz"), count_y_z("day fyyyz"));
    layout.print_test(format!("{:?}", ("day yak")), count_y_z_solution("day yak"), count_y_z("day yak"));
    layout.print_test(format!("{:?}", ("day:yak")), count_y_z_solution("day:yak"), count_y_z("day:yak"));
    layout.print_test(format!("{:?}", ("!!day--yaz!!")), count_y_z_solution("!!day--yaz!!"), count_y_z("!!day--yaz!!"));
    layout.print_test(format!("{:?}", ("yak zak")), count_y_z_solution("yak zak"), count_y_z("yak zak"));
    layout.print_test(format!("{:?}", ("DAY abc XYZ")), count_y_z_solution("DAY abc XYZ"), count_y_z("DAY abc XYZ"));
    layout.print_test(format!("{:?}", ("aaz yyz my")), count_y_z_solution("aaz yyz my"), count_y_z("aaz yyz my"));
    layout.print_test(format!("{:?}", ("y2bz")), count_y_z_solution("y2bz"), count_y_z("y2bz"));
    layout.print_test(format!("{:?}", ("zxyx")), count_y_z_solution("zxyx"), count_y_z("zxyx"));
    layout.print_footer();
}

pub fn without_string_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("without_string");
    layout.print_header();
    layout.print_test(format!("{:?}", ("Hello there", "llo")), without_string_solution("Hello there", "llo"), without_string("Hello there", "llo"));
    layout.print_test(format!("{:?}", ("Hello there", "e")), without_string_solution("Hello there", "e"), without_string("Hello there", "e"));
    layout.print_test(format!("{:?}", ("Hello there", "x")), without_string_solution("Hello there", "x"), without_string("Hello there", "x"));
    layout.print_test(format!("{:?}", ("This is a FISH", "IS")), without_string_solution("This is a FISH", "IS"), without_string("This is a FISH", "IS"));
    layout.print_test(format!("{:?}", ("THIS is a FISH", "is")), without_string_solution("THIS is a FISH", "is"), without_string("THIS is a FISH", "is"));
    layout.print_test(format!("{:?}", ("THIS is a FISH", "iS")), without_string_solution("THIS is a FISH", "iS"), without_string("THIS is a FISH", "iS"));
    layout.print_test(format!("{:?}", ("abxxxxab", "xx")), without_string_solution("abxxxxab", "xx"), without_string("abxxxxab", "xx"));
    layout.print_test(format!("{:?}", ("abxxxab", "xx")), without_string_solution("abxxxab", "xx"), without_string("abxxxab", "xx"));
    layout.print_test(format!("{:?}", ("abxxxab", "x")), without_string_solution("abxxxab", "x"), without_string("abxxxab", "x"));
    layout.print_test(format!("{:?}", ("xxx", "x")), without_string_solution("xxx", "x"), without_string("xxx", "x"));
    layout.print_test(format!("{:?}", ("xxx", "xx")), without_string_solution("xxx", "xx"), without_string("xxx", "xx"));
    layout.print_test(format!("{:?}", ("xyzzy", "Y")), without_string_solution("xyzzy", "Y"), without_string("xyzzy", "Y"));
    layout.print_test(format!("{:?}", ("", "x")), without_string_solution("", "x"), without_string("", "x"));
    layout.print_test(format!("{:?}", ("abcabc", "b")), without_string_solution("abcabc", "b"), without_string("abcabc", "b"));
    layout.print_test(format!("{:?}", ("AA22bb", "2")), without_string_solution("AA22bb", "2"), without_string("AA22bb", "2"));
    layout.print_test(format!("{:?}", ("1111", "1")), without_string_solution("1111", "1"), without_string("1111", "1"));
    layout.print_test(format!("{:?}", ("1111", "11")), without_string_solution("1111", "11"), without_string("1111", "11"));
    layout.print_test(format!("{:?}", ("MkjtMkx", "Mk")), without_string_solution("MkjtMkx", "Mk"), without_string("MkjtMkx", "Mk"));
    layout.print_test(format!("{:?}", ("Hi HoHo", "Ho")), without_string_solution("Hi HoHo", "Ho"), without_string("Hi HoHo", "Ho"));
    layout.print_footer();
}

pub fn equal_is_not_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("equal_is_not");
    layout.print_header();
    layout.print_test(format!("{:?}", ("This is not")), equal_is_not_solution("This is not"), equal_is_not("This is not"));
    layout.print_test(format!("{:?}", ("This is notnot")), equal_is_not_solution("This is notnot"), equal_is_not("This is notnot"));
    layout.print_test(format!("{:?}", ("noisxxnotyynotxisi")), equal_is_not_solution("noisxxnotyynotxisi"), equal_is_not("noisxxnotyynotxisi"));
    layout.print_test(format!("{:?}", ("noisxxnotyynotxsi")), equal_is_not_solution("noisxxnotyynotxsi"), equal_is_not("noisxxnotyynotxsi"));
    layout.print_test(format!("{:?}", ("xxxyyyzzzintint")), equal_is_not_solution("xxxyyyzzzintint"), equal_is_not("xxxyyyzzzintint"));
    layout.print_test(format!("{:?}", ("")), equal_is_not_solution(""), equal_is_not(""));
    layout.print_test(format!("{:?}", ("isisnotnot")), equal_is_not_solution("isisnotnot"), equal_is_not("isisnotnot"));
    layout.print_test(format!("{:?}", ("isisnotno7Not")), equal_is_not_solution("isisnotno7Not"), equal_is_not("isisnotno7Not"));
    layout.print_test(format!("{:?}", ("isnotis")), equal_is_not_solution("isnotis"), equal_is_not("isnotis"));
    layout.print_test(format!("{:?}", ("mis3notpotbotis")), equal_is_not_solution("mis3notpotbotis"), equal_is_not("mis3notpotbotis"));
    layout.print_footer();
}

pub fn g_happy_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("g_happy");
    layout.print_header();
    layout.print_test(format!("{:?}", ("xxggxx")), g_happy_solution("xxggxx"), g_happy("xxggxx"));
    layout.print_test(format!("{:?}", ("xxgxx")), g_happy_solution("xxgxx"), g_happy("xxgxx"));
    layout.print_test(format!("{:?}", ("xxggyygxx")), g_happy_solution("xxggyygxx"), g_happy("xxggyygxx"));
    layout.print_test(format!("{:?}", ("g")), g_happy_solution("g"), g_happy("g"));
    layout.print_test(format!("{:?}", ("gg")), g_happy_solution("gg"), g_happy("gg"));
    layout.print_test(format!("{:?}", ("")), g_happy_solution(""), g_happy(""));
    layout.print_test(format!("{:?}", ("xxgggxyz")), g_happy_solution("xxgggxyz"), g_happy("xxgggxyz"));
    layout.print_test(format!("{:?}", ("xxgggxyg")), g_happy_solution("xxgggxyg"), g_happy("xxgggxyg"));
    layout.print_test(format!("{:?}", ("xxgggxygg")), g_happy_solution("xxgggxygg"), g_happy("xxgggxygg"));
    layout.print_test(format!("{:?}", ("mgm")), g_happy_solution("mgm"), g_happy("mgm"));
    layout.print_test(format!("{:?}", ("mggm")), g_happy_solution("mggm"), g_happy("mggm"));
    layout.print_test(format!("{:?}", ("yyygggxyy")), g_happy_solution("yyygggxyy"), g_happy("yyygggxyy"));
    layout.print_footer();
}

pub fn count_triple_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("count_triple");
    layout.print_header();
    layout.print_test(format!("{:?}", ("abcXXXabc")), count_triple_solution("abcXXXabc"), count_triple("abcXXXabc"));
    layout.print_test(format!("{:?}", ("xxxabyyyycd")), count_triple_solution("xxxabyyyycd"), count_triple("xxxabyyyycd"));
    layout.print_test(format!("{:?}", ("a")), count_triple_solution("a"), count_triple("a"));
    layout.print_test(format!("{:?}", ("")), count_triple_solution(""), count_triple(""));
    layout.print_test(format!("{:?}", ("XXXabc")), count_triple_solution("XXXabc"), count_triple("XXXabc"));
    layout.print_test(format!("{:?}", ("XXXXabc")), count_triple_solution("XXXXabc"), count_triple("XXXXabc"));
    layout.print_test(format!("{:?}", ("XXXXXabc")), count_triple_solution("XXXXXabc"), count_triple("XXXXXabc"));
    layout.print_test(format!("{:?}", ("222abyyycdXXX")), count_triple_solution("222abyyycdXXX"), count_triple("222abyyycdXXX"));
    layout.print_test(format!("{:?}", ("abYYYabXXXXXab")), count_triple_solution("abYYYabXXXXXab"), count_triple("abYYYabXXXXXab"));
    layout.print_test(format!("{:?}", ("abYYXabXXYXXab")), count_triple_solution("abYYXabXXYXXab"), count_triple("abYYXabXXYXXab"));
    layout.print_test(format!("{:?}", ("abYYXabXXYXXab")), count_triple_solution("abYYXabXXYXXab"), count_triple("abYYXabXXYXXab"));
    layout.print_test(format!("{:?}", ("122abhhh2")), count_triple_solution("122abhhh2"), count_triple("122abhhh2"));
    layout.print_footer();
}

pub fn sum_digits_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("sum_digits");
    layout.print_header();
    layout.print_test(format!("{:?}", ("aa1bc2d3")), sum_digits_solution("aa1bc2d3"), sum_digits("aa1bc2d3"));
    layout.print_test(format!("{:?}", ("aa11b33")), sum_digits_solution("aa11b33"), sum_digits("aa11b33"));
    layout.print_test(format!("{:?}", ("Chocolate")), sum_digits_solution("Chocolate"), sum_digits("Chocolate"));
    layout.print_test(format!("{:?}", ("5hoco1a1e")), sum_digits_solution("5hoco1a1e"), sum_digits("5hoco1a1e"));
    layout.print_test(format!("{:?}", ("123abc123")), sum_digits_solution("123abc123"), sum_digits("123abc123"));
    layout.print_test(format!("{:?}", ("")), sum_digits_solution(""), sum_digits(""));
    layout.print_test(format!("{:?}", ("Hello")), sum_digits_solution("Hello"), sum_digits("Hello"));
    layout.print_test(format!("{:?}", ("X1z9b2")), sum_digits_solution("X1z9b2"), sum_digits("X1z9b2"));
    layout.print_test(format!("{:?}", ("5432a")), sum_digits_solution("5432a"), sum_digits("5432a"));
    layout.print_footer();
}

pub fn same_ends_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("same_ends");
    layout.print_header();
    layout.print_test(format!("{:?}", ("abXYab")), same_ends_solution("abXYab"), same_ends("abXYab"));
    layout.print_test(format!("{:?}", ("xx")), same_ends_solution("xx"), same_ends("xx"));
    layout.print_test(format!("{:?}", ("xxx")), same_ends_solution("xxx"), same_ends("xxx"));
    layout.print_test(format!("{:?}", ("xxxx")), same_ends_solution("xxxx"), same_ends("xxxx"));
    layout.print_test(format!("{:?}", ("javaXYZjava")), same_ends_solution("javaXYZjava"), same_ends("javaXYZjava"));
    layout.print_test(format!("{:?}", ("javajava")), same_ends_solution("javajava"), same_ends("javajava"));
    layout.print_test(format!("{:?}", ("xavaXYZjava")), same_ends_solution("xavaXYZjava"), same_ends("xavaXYZjava"));
    layout.print_test(format!("{:?}", ("Hello! and Hello!")), same_ends_solution("Hello! and Hello!"), same_ends("Hello! and Hello!"));
    layout.print_test(format!("{:?}", ("x")), same_ends_solution("x"), same_ends("x"));
    layout.print_test(format!("{:?}", ("")), same_ends_solution(""), same_ends(""));
    layout.print_test(format!("{:?}", ("abcb")), same_ends_solution("abcb"), same_ends("abcb"));
    layout.print_test(format!("{:?}", ("mymmy")), same_ends_solution("mymmy"), same_ends("mymmy"));
    layout.print_footer();
}

pub fn mirror_ends_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("mirror_ends");
    layout.print_header();
    layout.print_test(format!("{:?}", ("abXYZba")), mirror_ends_solution("abXYZba"), mirror_ends("abXYZba"));
    layout.print_test(format!("{:?}", ("abca")), mirror_ends_solution("abca"), mirror_ends("abca"));
    layout.print_test(format!("{:?}", ("aba")), mirror_ends_solution("aba"), mirror_ends("aba"));
    layout.print_test(format!("{:?}", ("abab")), mirror_ends_solution("abab"), mirror_ends("abab"));
    layout.print_test(format!("{:?}", ("xxx")), mirror_ends_solution("xxx"), mirror_ends("xxx"));
    layout.print_test(format!("{:?}", ("xxYxx")), mirror_ends_solution("xxYxx"), mirror_ends("xxYxx"));
    layout.print_test(format!("{:?}", ("Hi and iH")), mirror_ends_solution("Hi and iH"), mirror_ends("Hi and iH"));
    layout.print_test(format!("{:?}", ("x")), mirror_ends_solution("x"), mirror_ends("x"));
    layout.print_test(format!("{:?}", ("")), mirror_ends_solution(""), mirror_ends(""));
    layout.print_test(format!("{:?}", ("123and then 321")), mirror_ends_solution("123and then 321"), mirror_ends("123and then 321"));
    layout.print_test(format!("{:?}", ("band andab")), mirror_ends_solution("band andab"), mirror_ends("band andab"));
    layout.print_footer();
}

pub fn max_block_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("max_block");
    layout.print_header();
    layout.print_test(format!("{:?}", ("hoopla")), max_block_solution("hoopla"), max_block("hoopla"));
    layout.print_test(format!("{:?}", ("abbCCCddBBBxx")), max_block_solution("abbCCCddBBBxx"), max_block("abbCCCddBBBxx"));
    layout.print_test(format!("{:?}", ("")), max_block_solution(""), max_block(""));
    layout.print_test(format!("{:?}", ("xyz")), max_block_solution("xyz"), max_block("xyz"));
    layout.print_test(format!("{:?}", ("xxyz")), max_block_solution("xxyz"), max_block("xxyz"));
    layout.print_test(format!("{:?}", ("xyzz")), max_block_solution("xyzz"), max_block("xyzz"));
    layout.print_test(format!("{:?}", ("abbbcbbbxbbbx")), max_block_solution("abbbcbbbxbbbx"), max_block("abbbcbbbxbbbx"));
    layout.print_test(format!("{:?}", ("XXBBBbbxx")), max_block_solution("XXBBBbbxx"), max_block("XXBBBbbxx"));
    layout.print_test(format!("{:?}", ("XXBBBBbbxx")), max_block_solution("XXBBBBbbxx"), max_block("XXBBBBbbxx"));
    layout.print_test(format!("{:?}", ("XXBBBbbxxXXXX")), max_block_solution("XXBBBbbxxXXXX"), max_block("XXBBBbbxxXXXX"));
    layout.print_test(format!("{:?}", ("XX2222BBBbbXX2222")), max_block_solution("XX2222BBBbbXX2222"), max_block("XX2222BBBbbXX2222"));
    layout.print_footer();
}

pub fn sum_numbers_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("sum_numbers");
    layout.print_header();
    layout.print_test(format!("{:?}", ("abc123xyz")), sum_numbers_solution("abc123xyz"), sum_numbers("abc123xyz"));
    layout.print_test(format!("{:?}", ("aa11b33")), sum_numbers_solution("aa11b33"), sum_numbers("aa11b33"));
    layout.print_test(format!("{:?}", ("7 11")), sum_numbers_solution("7 11"), sum_numbers("7 11"));
    layout.print_test(format!("{:?}", ("Chocolate")), sum_numbers_solution("Chocolate"), sum_numbers("Chocolate"));
    layout.print_test(format!("{:?}", ("5hoco1a1e")), sum_numbers_solution("5hoco1a1e"), sum_numbers("5hoco1a1e"));
    layout.print_test(format!("{:?}", ("5$$1;;1!!")), sum_numbers_solution("5$$1;;1!!"), sum_numbers("5$$1;;1!!"));
    layout.print_test(format!("{:?}", ("a1234bb11")), sum_numbers_solution("a1234bb11"), sum_numbers("a1234bb11"));
    layout.print_test(format!("{:?}", ("")), sum_numbers_solution(""), sum_numbers(""));
    layout.print_test(format!("{:?}", ("a22bbb3")), sum_numbers_solution("a22bbb3"), sum_numbers("a22bbb3"));
    layout.print_footer();
}

pub fn not_replace_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("not_replace");
    layout.print_header();
    layout.print_test(format!("{:?}", ("is test")), not_replace_solution("is test"), not_replace("is test"));
    layout.print_test(format!("{:?}", ("is-is")), not_replace_solution("is-is"), not_replace("is-is"));
    layout.print_test(format!("{:?}", ("This is right")), not_replace_solution("This is right"), not_replace("This is right"));
    layout.print_test(format!("{:?}", ("This is isabell")), not_replace_solution("This is isabell"), not_replace("This is isabell"));
    layout.print_test(format!("{:?}", ("")), not_replace_solution(""), not_replace(""));
    layout.print_test(format!("{:?}", ("is")), not_replace_solution("is"), not_replace("is"));
    layout.print_test(format!("{:?}", ("isis")), not_replace_solution("isis"), not_replace("isis"));
    layout.print_test(format!("{:?}", ("Dis is bliss is")), not_replace_solution("Dis is bliss is"), not_replace("Dis is bliss is"));
    layout.print_test(format!("{:?}", ("is his")), not_replace_solution("is his"), not_replace("is his"));
    layout.print_test(format!("{:?}", ("xis yis")), not_replace_solution("xis yis"), not_replace("xis yis"));
    layout.print_test(format!("{:?}", ("AAAis is")), not_replace_solution("AAAis is"), not_replace("AAAis is"));
    layout.print_footer();
}
