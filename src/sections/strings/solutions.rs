use regex::{Regex, RegexBuilder};
use std::cmp::{max, min};

pub fn hello_name_solution(name: impl Into<String>) -> String {
    let input: String = name.into();
    format!("Hello {input}!")
}

pub fn make_abba_solution(a: impl Into<String>, b: impl Into<String>) -> String {
    let a: String = a.into();
    let b: String = b.into();
    format!("{a}{b}{b}{a}")
}

pub fn make_tags_solution(tag: impl Into<String>, word: impl Into<String>) -> String {
    let tag: String = tag.into();
    let word: String = word.into();
    format!("<{tag}>{word}</{tag}>")
}

pub fn make_out_word_solution(out: impl Into<String>, word: impl Into<String>) -> String {
    let out: String = out.into();
    let word: String = word.into();
    let out_chars = out.chars();
    let left: String = out_chars.clone().take(2).collect();
    let right: String = out_chars.skip(2).take(2).collect();
    format!("{left}{word}{right}")
}

pub fn extra_end_solution(str: impl Into<String>) -> String {
    let input: String = str.into();
    let len = input.len();
    let chunk: String = input.chars().skip(len - 2).collect();
    format!("{chunk}{chunk}{chunk}")
}

pub fn first_two_solution(str: impl Into<String>) -> String {
    let input: String = str.into();
    input.chars().take(2).collect()
}

pub fn first_half_solution(str: impl Into<String>) -> String {
    let input: String = str.into();
    let len = input.len();
    let half = len / 2;
    input.chars().take(half).collect()
}

pub fn without_end_solution(str: impl Into<String>) -> String {
    let input: String = str.into();
    let len = input.len();
    input.chars().skip(1).take(len - 2).collect()
}

pub fn combo_string_solution(a: impl Into<String>, b: impl Into<String>) -> String {
    let a: String = a.into();
    let b: String = b.into();
    let a_len = a.len();
    let b_len = b.len();

    let long: String;
    let short: String;
    if a_len > b_len {
        long = a;
        short = b;
    } else {
        long = b;
        short = a;
    }
    format!("{short}{long}{short}")
}

pub fn non_start_solution(a: impl Into<String>, b: impl Into<String>) -> String {
    let a: String = a.into();
    let b: String = b.into();
    let a: String = a.chars().skip(1).collect();
    let b: String = b.chars().skip(1).collect();
    format!("{a}{b}")
}

pub fn left2_solution(str: impl Into<String>) -> String {
    let input: String = str.into();
    let front: String = input.chars().take(2).collect();
    let chunk: String = input.chars().skip(2).collect();
    format!("{chunk}{front}")
}

pub fn right2_solution(str: impl Into<String>) -> String {
    let input: String = str.into();
    let len = input.len();
    let front: String = input.chars().take(len - 2).collect();
    let chunk: String = input.chars().skip(len - 2).collect();
    format!("{chunk}{front}")
}

pub fn the_end_solution(str: impl Into<String>, front: bool) -> String {
    let input: String = str.into();
    let len = input.len();
    let chunk = input.chars();
    let output;
    if front {
        output = chunk.take(1).collect();
    } else {
        output = chunk.skip(len - 1).take(1).collect();
    }
    output
}

pub fn without_end2_solution(str: impl Into<String>) -> String {
    let input: String = str.into();
    let len = input.len();

    if len > 2 {
        input.chars().skip(1).take(len - 2).collect()
    } else {
        String::new()
    }
}

pub fn middle_two_solution(str: impl Into<String>) -> String {
    let input: String = str.into();
    let len = input.len();
    let start = (len / 2) - 1;
    input.chars().skip(start).take(2).collect()
}

pub fn ends_ly_solution(str: impl Into<String>) -> bool {
    let input: String = str.into();
    input.ends_with("ly")
}

pub fn n_twice_solution(str: impl Into<String>, n: i32) -> String {
    let input: String = str.into();
    let len = input.len();
    let n: usize = n.try_into().expect("n must be 0 or positive");
    let front: String = input.chars().take(n).collect();
    let back: String = input.chars().skip(len - n).collect();
    format!("{front}{back}")
}

pub fn two_char_solution(str: impl Into<String>, index: i32) -> String {
    let input: String = str.into();
    let len = input.len();
    let index: usize = max(0, index).try_into().expect("n must be 0 or positive");

    if (len > index) && ((len - index) > 1) {
        input.chars().skip(index).take(2).collect()
    } else {
        input.chars().take(2).collect()
    }
}

pub fn middle_three_solution(str: impl Into<String>) -> String {
    let input: String = str.into();
    let len = input.len();
    let start = (len / 2) - 1;
    input.chars().skip(start).take(3).collect()
}

pub fn has_bad_solution(str: impl Into<String>) -> bool {
    let input: String = str.into();
    let re = Regex::new(r"^.?bad").expect("Write better regex");
    re.is_match(&input)
}

pub fn at_first_solution(str: impl Into<String>) -> String {
    let input: String = str.into();
    let mut result: String = input.chars().take(2).collect();

    while result.len() < 2 {
        result.push('@')
    }

    result
}

pub fn last_chars_solution(a: impl Into<String>, b: impl Into<String>) -> String {
    let a: String = a.into();
    let b: String = b.into();
    let b_len = b.len();

    let a: String = a.chars().take(1).collect();
    let b: String = b.chars().skip(max(b_len, 1) - 1).collect();
    let at = &String::from("@");

    format!(
        "{}{}",
        match a.len() {
            0 => at,
            _ => &a,
        },
        match b.len() {
            0 => at,
            _ => &b,
        }
    )
}

pub fn con_cat_solution(a: impl Into<String>, b: impl Into<String>) -> String {
    let a: String = a.into();
    let b: String = b.into();
    let last_a: String = a.chars().rev().take(1).collect();
    let first_b: String = b.chars().take(1).collect();
    let b_chunk: String = match last_a == first_b {
        true => b.chars().skip(1).collect(),
        false => b,
    };

    format!("{a}{b_chunk}")
}

pub fn last_two_solution(str: impl Into<String>) -> String {
    let input: String = str.into();
    let len = input.len();
    let skip_qty = max(len, 2) - 2;
    let front: String = input.chars().take(skip_qty).collect();
    let back: String = input.chars().rev().take(2).collect();
    format!("{front}{back}")
}

pub fn see_color_solution(str: impl Into<String>) -> String {
    let input: String = str.into();
    if input.starts_with("blue") {
        String::from("blue")
    } else if input.starts_with("red") {
        String::from("red")
    } else {
        String::new()
    }
}

pub fn front_again_solution(str: impl Into<String>) -> bool {
    let input: String = str.into();
    let len = input.len();
    let front: String = input.chars().take(2).collect();
    if len > 1 {
        let back: String = input.chars().skip(len - 2).collect();
        front == back
    } else {
        false
    }
}

pub fn min_cat_solution(a: impl Into<String>, b: impl Into<String>) -> String {
    let a: String = a.into();
    let b: String = b.into();
    let a_len = a.len();
    let b_len = b.len();
    let str_len = min(a_len, b_len);

    let a: String = a.chars().skip(a_len - str_len).collect();
    let b: String = b.chars().skip(b_len - str_len).collect();

    format!("{a}{b}")
}

pub fn extra_front_solution(str: impl Into<String>) -> String {
    let input: String = str.into();
    let front: String = input.chars().take(2).collect();
    format!("{front}{front}{front}")
}

pub fn without2_solution(str: impl Into<String>) -> String {
    let input: String = str.into();
    let len = input.len();

    if len > 1 {
        let front: String = input.chars().take(2).collect();
        let back: String = input.chars().skip(max(len, 2) - 2).collect();

        match front == back {
            true => input.chars().skip(2).collect(),
            false => input,
        }
    } else {
        input
    }
}

pub fn de_front_solution(str: impl Into<String>) -> String {
    let input: String = str.into();
    let chars: Vec<char> = input.chars().take(2).collect();
    let o1 = match chars[0] {
        'a' => String::from("a"),
        _ => String::new(),
    };
    let o2 = match chars[1] {
        'b' => String::from("b"),
        _ => String::new(),
    };
    let back: String = input.chars().skip(2).collect();

    format!("{}{}{back}", o1, o2)
}

pub fn start_word_solution(str: impl Into<String>, word: impl Into<String>) -> String {
    let input: String = str.into();
    let word: String = word.into();
    let mut word: Vec<char> = word.chars().collect();
    word[0] = '.';
    let word: String = word.iter().collect();
    let re = Regex::new(&format!("({word})")).expect("Write better regex");

    if let Some(caps) = re.captures(&input) {
        if caps.len() > 0 {
            return caps[0].to_string();
        }
    }
    String::new()
}

pub fn without_x_solution(str: impl Into<String>) -> String {
    let input: String = str.into();
    let len = input.len();
    let skip_front = match input.starts_with('x') {
        true => 1,
        false => 0,
    };
    let skip_back = match input.ends_with('x') {
        true => 1,
        false => 0,
    };

    input
        .chars()
        .skip(skip_front)
        .take(max(len, skip_front + skip_back) - skip_front - skip_back)
        .collect()
}

pub fn without_x2_solution(str: impl Into<String>) -> String {
    let input: String = str.into();
    let len = input.len();
    let front: String = input.chars().take(2).filter(|&c| c != 'x').collect();
    let back: String = input.chars().skip(min(len, 2)).collect();

    format!("{front}{back}")
}

pub fn double_char_solution(str: impl Into<String>) -> String {
    let input: String = str.into();
    input.chars().map(|c| format!("{c}{c}")).collect()
}

pub fn count_hi_solution(str: impl Into<String>) -> i32 {
    let input: String = str.into();
    let re = Regex::new(r"hi").expect("Write better regex");

    re.captures_iter(&input).collect::<Vec<_>>().len().try_into().unwrap_or(0)
}

pub fn cat_dog_solution(str: impl Into<String>) -> bool {
    let input: String = str.into();
    let re_cat = Regex::new(r"cat").expect("Write better regex");
    let re_dog = Regex::new(r"dog").expect("Write better regex");

    let cat_count = re_cat.captures_iter(&input).collect::<Vec<_>>().len().try_into().unwrap_or(0);
    let dog_count = re_dog.captures_iter(&input).collect::<Vec<_>>().len().try_into().unwrap_or(0);

    cat_count == dog_count
}

pub fn count_code_solution(str: impl Into<String>) -> i32 {
    let input: String = str.into();
    let re = Regex::new(r"co.e").expect("Write better regex");

    re.captures_iter(&input).collect::<Vec<_>>().len().try_into().unwrap_or(0)
}

pub fn end_other_solution(a: impl Into<String>, b: impl Into<String>) -> bool {
    let a: String = a.into().to_lowercase();
    let b: String = b.into().to_lowercase();

    a.ends_with(&b) || b.ends_with(&a)
}

pub fn xyz_there_solution(str: impl Into<String>) -> bool {
    let input: String = str.into();
    let xyz = Regex::new(r"xyz").expect("Write better regex");
    let dxyz = Regex::new(r"\.xyz").expect("Write better regex");

    xyz.is_match(&dxyz.replace_all(&input, ""))
}

pub fn bob_there_solution(str: impl Into<String>) -> bool {
    let input: String = str.into();
    let re = Regex::new(r"b.b").expect("Write better regex");

    re.is_match(&input)
}

pub fn xy_balance_solution(str: impl Into<String>) -> bool {
    let input: String = str.into();
    let re = Regex::new(r"x[^y]*y").expect("Write better regex");
    re.replace_all(&input.chars().filter(|&c| c == 'x' || c == 'y').collect::<String>(), "")
        .replace('y', "")
        .len()
        == 0
}

pub fn mix_string_solution(a: impl Into<String>, b: impl Into<String>) -> String {
    let a: Vec<char> = a.into().chars().collect();
    let b: Vec<char> = b.into().chars().collect();
    let a_len = a.len();
    let b_len = b.len();

    let mut result = String::new();

    for i in 0..max(a_len, b_len) {
        if i < a_len {
            result.push(a[i]);
        }
        if i < b_len {
            result.push(b[i]);
        }
    }

    result
}

pub fn repeat_end_solution(str: impl Into<String>, n: i32) -> String {
    let input: String = str.into();
    let n: usize = n.try_into().expect("n must be 0 or positive");
    let len = input.len() - n;
    (0..n).map(|_| input.chars().skip(len).collect::<String>()).collect()
}

pub fn repeat_front_solution(str: impl Into<String>, n: i32) -> String {
    let input: String = str.into();
    let n: usize = n.try_into().expect("n must be 0 or positive");
    (0..n).map(|n| n).rev().map(|n| &input[0..(n + 1)]).collect()
}

pub fn repeat_separator_solution(word: impl Into<String>, sep: impl Into<String>, count: i32) -> String {
    let word: String = word.into();
    let sep: String = sep.into();
    (0..count).map(|_| word.clone()).collect::<Vec<String>>().join(&sep)
}

pub fn prefix_again_solution(str: impl Into<String>, n: i32) -> bool {
    let input: String = str.into();
    let n: usize = n.try_into().expect("n must be 0 or positive");
    let chunk: String = input.chars().take(n).collect();
    input.chars().skip(n).collect::<String>().contains(&chunk)
}

pub fn xyz_middle_solution(str: impl Into<String>) -> bool {
    let input: String = str.into();
    let len = input.len();
    let start = max(len / 2, 1);
    let even = 0 == (len % 2);

    for n in 1..3 {
        if start >= n {
            if (even) || (!even && n == 1) {
                let mid_str = input.chars().skip(start - n).take(3).collect::<String>();
                if mid_str == "xyz" {
                    return true;
                }
            }
        }
    }
    false
}

pub fn get_sandwich_solution(str: impl Into<String>) -> String {
    let input: String = str.into();
    let re = Regex::new(r"^.*?bread(.*)bread.*?$").expect("Write better regex");
    if re.is_match(&input) {
        re.replace(&input, "$1").to_string()
    } else {
        String::new()
    }
}

pub fn same_star_char_solution(str: impl Into<String>) -> bool {
    let input: String = str.into();
    let chars = input.chars().collect::<Vec<char>>();
    let len = input.len();

    if input.contains('*') {
        let mut matches = Vec::<bool>::new();

        if len > 2 {
            for i in 1..len - 1 {
                let l = chars[i - 1];
                let m = chars[i];
                let r = chars[i + 1];
                if m == '*' {
                    matches.push(l == r);
                }
            }
        }

        matches.iter().fold(true, |a, &e| a && e)
    } else {
        true
    }
}

pub fn one_two_solution(str: impl Into<String>) -> String {
    let input: String = str.into();
    let mut view = input.chars().peekable();
    let mut result = Vec::<char>::new();

    while view.peek().is_some() {
        let chunk: Vec<char> = view.by_ref().take(3).collect();
        if chunk.len() == 3 {
            result.push(chunk[1]);
            result.push(chunk[2]);
            result.push(chunk[0]);
        }
    }

    result.iter().collect()
}

pub fn zip_zap_solution(str: impl Into<String>) -> String {
    let input: String = str.into();
    let re = Regex::new(r"z.p").expect("Write better regex");

    re.replace_all(&input, "zp").to_string()
}

pub fn star_out_solution(str: impl Into<String>) -> String {
    let input: String = str.into();
    let re = Regex::new(r".?\*+.?").expect("Write better regex");

    re.replace_all(&input, "").to_string()
}

pub fn plus_out_solution(str: impl Into<String>, word: impl Into<String>) -> String {
    let input: Vec<char> = str.into().chars().collect();
    let i_len = input.len();
    let word: Vec<char> = word.into().chars().collect();
    let w_len = word.len();
    let mut result = Vec::<char>::new();

    let mut i = 0;
    while i < (i_len - w_len + 1) {
        match word == &input[i..i + w_len] {
            true => {
                result.extend(&word);
                i += w_len;
            }
            _ => {
                result.push('+');
                i += 1;
            }
        }
    }
    while result.len() < i_len {
        result.push('+');
    }

    result.iter().collect()
}

pub fn word_ends_solution(str: impl Into<String>, word: impl Into<String>) -> String {
    let input: Vec<char> = str.into().chars().collect();
    let i_len = input.len();
    let word: Vec<char> = word.into().chars().collect();
    let w_len = word.len();
    let mut result = Vec::<char>::new();

    let mut i = 0;
    if i_len > w_len {
        while i < (i_len - w_len + 1) {
            match word == &input[i..i + w_len] {
                true => {
                    if i > 0 {
                        result.push(input[i - 1]);
                    }
                    if i < (i_len - w_len) {
                        result.push(input[i + w_len]);
                    }
                    i += w_len;
                }
                _ => {
                    i += 1;
                }
            }
        }
    }

    result.iter().collect()
}

pub fn count_y_z_solution(str: impl Into<String>) -> i32 {
    let input: String = str.into();

    input //
        .split(|c: char| !c.is_alphabetic() || c.is_whitespace())
        .into_iter()
        .filter(|&e| !e.is_empty())
        .map(|t| t.to_lowercase())
        .filter(|t| t.ends_with('y') || t.ends_with('z'))
        .count()
        .try_into()
        .unwrap_or(0)
}

pub fn without_string_solution(base: impl Into<String>, remove: impl Into<String>) -> String {
    let base: String = base.into();
    let remove: String = remove.into();
    let remove_string = RegexBuilder::new(&remove) //
        .case_insensitive(true)
        .build()
        .expect("Write better regex");
    let remove_spaces = Regex::new("\\s\\s+").expect("Write better regex");

    remove_spaces.replace_all(&remove_string.replace_all(&base, ""), " ").to_string()
}

pub fn equal_is_not_solution(str: impl Into<String>) -> bool {
    let input: String = str.into();
    let re_is = Regex::new(r"is").expect("Write better regex");
    let re_not = Regex::new(r"not").expect("Write better regex");

    let is_count = re_is.captures_iter(&input).count();
    let not_count = re_not.captures_iter(&input).count();

    is_count == not_count
}

pub fn g_happy_solution(str: impl Into<String>) -> bool {
    let input: String = str.into();
    let re = Regex::new(r"g+").expect("Write better regex");

    re.captures_iter(&input)
        .map(|cap| {
            cap.iter() //
                .map(|m| m.unwrap().as_str().len())
                .filter(|&e| e == 1)
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>()
        .iter()
        .filter(|&e| e.len() > 0)
        .collect::<Vec<_>>()
        .len()
        == 0
}

pub fn count_triple_solution(str: impl Into<String>) -> i32 {
    let input: Vec<char> = str.into().chars().collect();

    input
        .windows(3) //
        .filter(|&e1| {
            e1.windows(2) //
                .all(|e2| e2[0] == e2[1])
        })
        .count()
        .try_into()
        .unwrap_or(0)
}

pub fn sum_digits_solution(str: impl Into<String>) -> i32 {
    str.into()
        .chars()
        .filter(|&c| c.is_numeric())
        .map(|e| char::to_digit(e, 10).unwrap_or(0) as i32)
        .fold(0, |a, e| a + e)
}

pub fn same_ends_solution(nums: impl Into<String>) -> String {
    let input: Vec<char> = nums.into().chars().collect();
    let len = input.len() / 2;

    for i in (1..len + 1).rev() {
        let chunks = input
            .windows(i) //
            .map(|a| a.clone().to_owned())
            .collect::<Vec<_>>();
        if chunks[0] == chunks[chunks.len() - 1] {
            return chunks[0].iter().collect();
        }
    }
    String::new()
}

pub fn mirror_ends_solution(string: impl Into<String>) -> String {
    let string: String = string.into();
    let input: &Vec<char> = &string.clone().chars().collect();
    let tupni: &Vec<char> = &string.clone().chars().rev().collect();
    let len = input.len();

    for i in (1..len + 1).rev() {
        let chunk_f: String = input.iter().take(i).collect();
        let chunk_b: String = tupni.iter().take(i).collect();

        if chunk_f == chunk_b {
            return chunk_f;
        }
    }
    String::new()
}

pub fn max_block_solution(str: impl Into<String>) -> i32 {
    let input: Vec<char> = str.into().chars().collect();
    let len = input.len();

    let mut result = 0;
    if len > 0 {
        let mut last: char = input[0].into();
        let mut work = 1;

        for i in 1..len {
            if input[i] == last {
                work += 1;
            } else {
                last = input[i];
                work = 1;
            }
            result = max(work, result);
        }
    }
    result
}

pub fn sum_numbers_solution(str: impl Into<String>) -> i32 {
    str.try_into()
        .unwrap_or(String::new()) //
        .split(|c: char| !c.is_numeric())
        .filter(|e| !e.is_empty())
        .map(|e| e.parse::<i32>().unwrap_or(0))
        .fold(0, |a, e| a + e)
}

pub fn not_replace_solution(str: impl Into<String>) -> String {
    let input: String = str.into();

    input //
        .split_inclusive(|c: char| !c.is_alphabetic())
        // .into_iter()
        .map(|e| {
            if e.starts_with("is") && (e.len() <= 3) {
                e.replace("is", "is not")
            } else {
                e.to_string()
            }
        })
        .collect::<Vec<_>>()
        .join("")
}
