pub mod implementations;
pub mod runners;
pub mod solutions;

#[cfg(test)]
mod test_first_last6 {
    use super::implementations::first_last6;
    use super::solutions::first_last6_solution;

    #[test]
    fn test_first_last6_1() {
        assert_eq!(first_last6(vec![1, 2, 6]), first_last6_solution(vec![1, 2, 6]))
    }
    #[test]
    fn test_first_last6_2() {
        assert_eq!(first_last6(vec![6, 1, 2, 3]), first_last6_solution(vec![6, 1, 2, 3]))
    }
    #[test]
    fn test_first_last6_3() {
        assert_eq!(first_last6(vec![13, 6, 1, 2, 3]), first_last6_solution(vec![13, 6, 1, 2, 3]))
    }
    #[test]
    fn test_first_last6_4() {
        assert_eq!(first_last6(vec![13, 6, 1, 2, 6]), first_last6_solution(vec![13, 6, 1, 2, 6]))
    }
    #[test]
    fn test_first_last6_5() {
        assert_eq!(first_last6(vec![3, 2, 1]), first_last6_solution(vec![3, 2, 1]))
    }
    #[test]
    fn test_first_last6_6() {
        assert_eq!(first_last6(vec![3, 6, 1]), first_last6_solution(vec![3, 6, 1]))
    }
    #[test]
    fn test_first_last6_7() {
        assert_eq!(first_last6(vec![3, 6]), first_last6_solution(vec![3, 6]))
    }
    #[test]
    fn test_first_last6_8() {
        assert_eq!(first_last6(vec![6]), first_last6_solution(vec![6]))
    }
    #[test]
    fn test_first_last6_9() {
        assert_eq!(first_last6(vec![3]), first_last6_solution(vec![3]))
    }
    #[test]
    fn test_first_last6_10() {
        assert_eq!(first_last6(vec![5, 6]), first_last6_solution(vec![5, 6]))
    }
    #[test]
    fn test_first_last6_11() {
        assert_eq!(first_last6(vec![5, 5]), first_last6_solution(vec![5, 5]))
    }
    #[test]
    fn test_first_last6_12() {
        assert_eq!(first_last6(vec![1, 2, 3, 4, 6]), first_last6_solution(vec![1, 2, 3, 4, 6]))
    }
    #[test]
    fn test_first_last6_13() {
        assert_eq!(first_last6(vec![1, 2, 3, 4]), first_last6_solution(vec![1, 2, 3, 4]))
    }
}

#[cfg(test)]
mod test_same_first_last {
    use super::implementations::same_first_last;
    use super::solutions::same_first_last_solution;

    #[test]
    fn test_same_first_last_1() {
        assert_eq!(same_first_last(vec![1, 2, 3]), same_first_last_solution(vec![1, 2, 3]))
    }
    #[test]
    fn test_same_first_last_2() {
        assert_eq!(same_first_last(vec![1, 2, 3, 1]), same_first_last_solution(vec![1, 2, 3, 1]))
    }
    #[test]
    fn test_same_first_last_3() {
        assert_eq!(same_first_last(vec![1, 2, 1]), same_first_last_solution(vec![1, 2, 1]))
    }
    #[test]
    fn test_same_first_last_4() {
        assert_eq!(same_first_last(vec![7]), same_first_last_solution(vec![7]))
    }
    #[test]
    fn test_same_first_last_5() {
        assert_eq!(same_first_last(vec![]), same_first_last_solution(vec![]))
    }
    #[test]
    fn test_same_first_last_6() {
        assert_eq!(same_first_last(vec![1, 2, 3, 4, 5, 1]), same_first_last_solution(vec![1, 2, 3, 4, 5, 1]))
    }
    #[test]
    fn test_same_first_last_7() {
        assert_eq!(same_first_last(vec![1, 2, 3, 4, 5, 13]), same_first_last_solution(vec![1, 2, 3, 4, 5, 13]))
    }
    #[test]
    fn test_same_first_last_8() {
        assert_eq!(same_first_last(vec![13, 2, 3, 4, 5, 13]), same_first_last_solution(vec![13, 2, 3, 4, 5, 13]))
    }
    #[test]
    fn test_same_first_last_9() {
        assert_eq!(same_first_last(vec![7, 7]), same_first_last_solution(vec![7, 7]))
    }
}

#[cfg(test)]
mod test_make_pi {
    use super::implementations::make_pi;
    use super::solutions::make_pi_solution;

    #[test]
    fn test_make_pi_1() {
        assert_eq!(make_pi(), make_pi_solution())
    }
}

#[cfg(test)]
mod test_common_end {
    use super::implementations::common_end;
    use super::solutions::common_end_solution;

    #[test]
    fn test_common_end_1() {
        assert_eq!(common_end(vec![1, 2, 3], vec![7, 3]), common_end_solution(vec![1, 2, 3], vec![7, 3]))
    }
    #[test]
    fn test_common_end_2() {
        assert_eq!(common_end(vec![1, 2, 3], vec![7, 3, 2]), common_end_solution(vec![1, 2, 3], vec![7, 3, 2]))
    }
    #[test]
    fn test_common_end_3() {
        assert_eq!(common_end(vec![1, 2, 3], vec![1, 3]), common_end_solution(vec![1, 2, 3], vec![1, 3]))
    }
    #[test]
    fn test_common_end_4() {
        assert_eq!(common_end(vec![1, 2, 3], vec![1]), common_end_solution(vec![1, 2, 3], vec![1]))
    }
    #[test]
    fn test_common_end_5() {
        assert_eq!(common_end(vec![1, 2, 3], vec![2]), common_end_solution(vec![1, 2, 3], vec![2]))
    }
}

#[cfg(test)]
mod test_sum3 {
    use super::implementations::sum3;
    use super::solutions::sum3_solution;

    #[test]
    fn test_sum3_1() {
        assert_eq!(sum3(vec![1, 2, 3]), sum3_solution(vec![1, 2, 3]))
    }
    #[test]
    fn test_sum3_2() {
        assert_eq!(sum3(vec![5, 11, 2]), sum3_solution(vec![5, 11, 2]))
    }
    #[test]
    fn test_sum3_3() {
        assert_eq!(sum3(vec![7, 0, 0]), sum3_solution(vec![7, 0, 0]))
    }
    #[test]
    fn test_sum3_4() {
        assert_eq!(sum3(vec![1, 2, 1]), sum3_solution(vec![1, 2, 1]))
    }
    #[test]
    fn test_sum3_5() {
        assert_eq!(sum3(vec![1, 1, 1]), sum3_solution(vec![1, 1, 1]))
    }
    #[test]
    fn test_sum3_6() {
        assert_eq!(sum3(vec![2, 7, 2]), sum3_solution(vec![2, 7, 2]))
    }
}

#[cfg(test)]
mod test_rotate_left3 {
    use super::implementations::rotate_left3;
    use super::solutions::rotate_left3_solution;

    #[test]
    fn test_rotate_left3_1() {
        assert_eq!(rotate_left3(vec![1, 2, 3]), rotate_left3_solution(vec![1, 2, 3]))
    }
    #[test]
    fn test_rotate_left3_2() {
        assert_eq!(rotate_left3(vec![5, 11, 9]), rotate_left3_solution(vec![5, 11, 9]))
    }
    #[test]
    fn test_rotate_left3_3() {
        assert_eq!(rotate_left3(vec![7, 0, 0]), rotate_left3_solution(vec![7, 0, 0]))
    }
    #[test]
    fn test_rotate_left3_4() {
        assert_eq!(rotate_left3(vec![1, 2, 1]), rotate_left3_solution(vec![1, 2, 1]))
    }
    #[test]
    fn test_rotate_left3_5() {
        assert_eq!(rotate_left3(vec![0, 0, 1]), rotate_left3_solution(vec![0, 0, 1]))
    }
}

#[cfg(test)]
mod test_reverse3 {
    use super::implementations::reverse3;
    use super::solutions::reverse3_solution;

    #[test]
    fn test_reverse3_1() {
        assert_eq!(reverse3(vec![1, 2, 3]), reverse3_solution(vec![1, 2, 3]))
    }
    #[test]
    fn test_reverse3_2() {
        assert_eq!(reverse3(vec![5, 11, 9]), reverse3_solution(vec![5, 11, 9]))
    }
    #[test]
    fn test_reverse3_3() {
        assert_eq!(reverse3(vec![7, 0, 0]), reverse3_solution(vec![7, 0, 0]))
    }
    #[test]
    fn test_reverse3_4() {
        assert_eq!(reverse3(vec![2, 1, 2]), reverse3_solution(vec![2, 1, 2]))
    }
    #[test]
    fn test_reverse3_5() {
        assert_eq!(reverse3(vec![1, 2, 1]), reverse3_solution(vec![1, 2, 1]))
    }
    #[test]
    fn test_reverse3_6() {
        assert_eq!(reverse3(vec![2, 11, 3]), reverse3_solution(vec![2, 11, 3]))
    }
    #[test]
    fn test_reverse3_7() {
        assert_eq!(reverse3(vec![0, 6, 5]), reverse3_solution(vec![0, 6, 5]))
    }
    #[test]
    fn test_reverse3_8() {
        assert_eq!(reverse3(vec![7, 2, 3]), reverse3_solution(vec![7, 2, 3]))
    }
}

#[cfg(test)]
mod test_max_end3 {
    use super::implementations::max_end3;
    use super::solutions::max_end3_solution;

    #[test]
    fn test_max_end3_1() {
        assert_eq!(max_end3(vec![1, 2, 3]), max_end3_solution(vec![1, 2, 3]))
    }
    #[test]
    fn test_max_end3_2() {
        assert_eq!(max_end3(vec![11, 5, 9]), max_end3_solution(vec![11, 5, 9]))
    }
    #[test]
    fn test_max_end3_3() {
        assert_eq!(max_end3(vec![2, 11, 3]), max_end3_solution(vec![2, 11, 3]))
    }
    #[test]
    fn test_max_end3_4() {
        assert_eq!(max_end3(vec![11, 3, 3]), max_end3_solution(vec![11, 3, 3]))
    }
    #[test]
    fn test_max_end3_5() {
        assert_eq!(max_end3(vec![3, 11, 11]), max_end3_solution(vec![3, 11, 11]))
    }
    #[test]
    fn test_max_end3_6() {
        assert_eq!(max_end3(vec![2, 2, 2]), max_end3_solution(vec![2, 2, 2]))
    }
    #[test]
    fn test_max_end3_7() {
        assert_eq!(max_end3(vec![2, 11, 2]), max_end3_solution(vec![2, 11, 2]))
    }
    #[test]
    fn test_max_end3_8() {
        assert_eq!(max_end3(vec![0, 0, 1]), max_end3_solution(vec![0, 0, 1]))
    }
}

#[cfg(test)]
mod test_sum2 {
    use super::implementations::sum2;
    use super::solutions::sum2_solution;

    #[test]
    fn test_sum2_1() {
        assert_eq!(sum2(vec![1, 2, 3]), sum2_solution(vec![1, 2, 3]))
    }
    #[test]
    fn test_sum2_2() {
        assert_eq!(sum2(vec![1, 1]), sum2_solution(vec![1, 1]))
    }
    #[test]
    fn test_sum2_3() {
        assert_eq!(sum2(vec![1, 1, 1, 1]), sum2_solution(vec![1, 1, 1, 1]))
    }
    #[test]
    fn test_sum2_4() {
        assert_eq!(sum2(vec![1, 2]), sum2_solution(vec![1, 2]))
    }
    #[test]
    fn test_sum2_5() {
        assert_eq!(sum2(vec![1]), sum2_solution(vec![1]))
    }
    #[test]
    fn test_sum2_6() {
        assert_eq!(sum2(vec![]), sum2_solution(vec![]))
    }
    #[test]
    fn test_sum2_7() {
        assert_eq!(sum2(vec![4, 5, 6]), sum2_solution(vec![4, 5, 6]))
    }
    #[test]
    fn test_sum2_8() {
        assert_eq!(sum2(vec![4]), sum2_solution(vec![4]))
    }
}

#[cfg(test)]
mod test_middle_way {
    use super::implementations::middle_way;
    use super::solutions::middle_way_solution;

    #[test]
    fn test_middle_way_1() {
        assert_eq!(middle_way(vec![1, 2, 3], vec![4, 5, 6]), middle_way_solution(vec![1, 2, 3], vec![4, 5, 6]))
    }
    #[test]
    fn test_middle_way_2() {
        assert_eq!(middle_way(vec![7, 7, 7], vec![3, 8, 0]), middle_way_solution(vec![7, 7, 7], vec![3, 8, 0]))
    }
    #[test]
    fn test_middle_way_3() {
        assert_eq!(middle_way(vec![5, 2, 9], vec![1, 4, 5]), middle_way_solution(vec![5, 2, 9], vec![1, 4, 5]))
    }
    #[test]
    fn test_middle_way_4() {
        assert_eq!(middle_way(vec![1, 9, 7], vec![4, 8, 8]), middle_way_solution(vec![1, 9, 7], vec![4, 8, 8]))
    }
    #[test]
    fn test_middle_way_5() {
        assert_eq!(middle_way(vec![1, 2, 3], vec![3, 1, 4]), middle_way_solution(vec![1, 2, 3], vec![3, 1, 4]))
    }
    #[test]
    fn test_middle_way_6() {
        assert_eq!(middle_way(vec![1, 2, 3], vec![4, 1, 1]), middle_way_solution(vec![1, 2, 3], vec![4, 1, 1]))
    }
}

#[cfg(test)]
mod test_make_ends {
    use super::implementations::make_ends;
    use super::solutions::make_ends_solution;

    #[test]
    fn test_make_ends_1() {
        assert_eq!(make_ends(vec![1, 2, 3]), make_ends_solution(vec![1, 2, 3]))
    }
    #[test]
    fn test_make_ends_2() {
        assert_eq!(make_ends(vec![1, 2, 3, 4]), make_ends_solution(vec![1, 2, 3, 4]))
    }
    #[test]
    fn test_make_ends_3() {
        assert_eq!(make_ends(vec![7, 4, 6, 2]), make_ends_solution(vec![7, 4, 6, 2]))
    }
    #[test]
    fn test_make_ends_4() {
        assert_eq!(make_ends(vec![1, 2, 2, 2, 2, 2, 2, 3]), make_ends_solution(vec![1, 2, 2, 2, 2, 2, 2, 3]))
    }
    #[test]
    fn test_make_ends_5() {
        assert_eq!(make_ends(vec![7, 4]), make_ends_solution(vec![7, 4]))
    }
    #[test]
    fn test_make_ends_6() {
        assert_eq!(make_ends(vec![7]), make_ends_solution(vec![7]))
    }
    #[test]
    fn test_make_ends_7() {
        assert_eq!(make_ends(vec![5, 2, 9]), make_ends_solution(vec![5, 2, 9]))
    }
    #[test]
    fn test_make_ends_8() {
        assert_eq!(make_ends(vec![2, 3, 4, 1]), make_ends_solution(vec![2, 3, 4, 1]))
    }
}

#[cfg(test)]
mod test_has23 {
    use super::implementations::has23;
    use super::solutions::has23_solution;

    #[test]
    fn test_has23_1() {
        assert_eq!(has23(vec![2, 5]), has23_solution(vec![2, 5]))
    }
    #[test]
    fn test_has23_2() {
        assert_eq!(has23(vec![4, 3]), has23_solution(vec![4, 3]))
    }
    #[test]
    fn test_has23_3() {
        assert_eq!(has23(vec![4, 5]), has23_solution(vec![4, 5]))
    }
    #[test]
    fn test_has23_4() {
        assert_eq!(has23(vec![2, 2]), has23_solution(vec![2, 2]))
    }
    #[test]
    fn test_has23_5() {
        assert_eq!(has23(vec![3, 2]), has23_solution(vec![3, 2]))
    }
    #[test]
    fn test_has23_6() {
        assert_eq!(has23(vec![3, 3]), has23_solution(vec![3, 3]))
    }
    #[test]
    fn test_has23_7() {
        assert_eq!(has23(vec![7, 7]), has23_solution(vec![7, 7]))
    }
    #[test]
    fn test_has23_8() {
        assert_eq!(has23(vec![3, 9]), has23_solution(vec![3, 9]))
    }
    #[test]
    fn test_has23_9() {
        assert_eq!(has23(vec![9, 5]), has23_solution(vec![9, 5]))
    }
}

#[cfg(test)]
mod test_no23 {
    use super::implementations::no23;
    use super::solutions::no23_solution;

    #[test]
    fn test_no23_1() {
        assert_eq!(no23(vec![4, 5]), no23_solution(vec![4, 5]))
    }
    #[test]
    fn test_no23_2() {
        assert_eq!(no23(vec![4, 2]), no23_solution(vec![4, 2]))
    }
    #[test]
    fn test_no23_3() {
        assert_eq!(no23(vec![3, 5]), no23_solution(vec![3, 5]))
    }
    #[test]
    fn test_no23_4() {
        assert_eq!(no23(vec![1, 9]), no23_solution(vec![1, 9]))
    }
    #[test]
    fn test_no23_5() {
        assert_eq!(no23(vec![2, 9]), no23_solution(vec![2, 9]))
    }
    #[test]
    fn test_no23_6() {
        assert_eq!(no23(vec![1, 3]), no23_solution(vec![1, 3]))
    }
    #[test]
    fn test_no23_7() {
        assert_eq!(no23(vec![1, 1]), no23_solution(vec![1, 1]))
    }
    #[test]
    fn test_no23_8() {
        assert_eq!(no23(vec![2, 2]), no23_solution(vec![2, 2]))
    }
    #[test]
    fn test_no23_9() {
        assert_eq!(no23(vec![3, 3]), no23_solution(vec![3, 3]))
    }
    #[test]
    fn test_no23_10() {
        assert_eq!(no23(vec![7, 8]), no23_solution(vec![7, 8]))
    }
    #[test]
    fn test_no23_11() {
        assert_eq!(no23(vec![8, 7]), no23_solution(vec![8, 7]))
    }
}

#[cfg(test)]
mod test_make_last {
    use super::implementations::make_last;
    use super::solutions::make_last_solution;

    #[test]
    fn test_make_last_1() {
        assert_eq!(make_last(vec![4, 5, 6]), make_last_solution(vec![4, 5, 6]))
    }
    #[test]
    fn test_make_last_2() {
        assert_eq!(make_last(vec![1, 2]), make_last_solution(vec![1, 2]))
    }
    #[test]
    fn test_make_last_3() {
        assert_eq!(make_last(vec![3]), make_last_solution(vec![3]))
    }
    #[test]
    fn test_make_last_4() {
        assert_eq!(make_last(vec![0]), make_last_solution(vec![0]))
    }
    #[test]
    fn test_make_last_5() {
        assert_eq!(make_last(vec![7, 7, 7]), make_last_solution(vec![7, 7, 7]))
    }
    #[test]
    fn test_make_last_6() {
        assert_eq!(make_last(vec![3, 1, 4]), make_last_solution(vec![3, 1, 4]))
    }
    #[test]
    fn test_make_last_7() {
        assert_eq!(make_last(vec![1, 2, 3, 4]), make_last_solution(vec![1, 2, 3, 4]))
    }
    #[test]
    fn test_make_last_8() {
        assert_eq!(make_last(vec![1, 2, 3, 0]), make_last_solution(vec![1, 2, 3, 0]))
    }
    #[test]
    fn test_make_last_9() {
        assert_eq!(make_last(vec![2, 4]), make_last_solution(vec![2, 4]))
    }
}

#[cfg(test)]
mod test_double23 {
    use super::implementations::double23;
    use super::solutions::double23_solution;

    #[test]
    fn test_double23_1() {
        assert_eq!(double23(vec![2, 2]), double23_solution(vec![2, 2]))
    }
    #[test]
    fn test_double23_2() {
        assert_eq!(double23(vec![3, 3]), double23_solution(vec![3, 3]))
    }
    #[test]
    fn test_double23_3() {
        assert_eq!(double23(vec![2, 3]), double23_solution(vec![2, 3]))
    }
    #[test]
    fn test_double23_4() {
        assert_eq!(double23(vec![3, 2]), double23_solution(vec![3, 2]))
    }
    #[test]
    fn test_double23_5() {
        assert_eq!(double23(vec![4, 5]), double23_solution(vec![4, 5]))
    }
    #[test]
    fn test_double23_6() {
        assert_eq!(double23(vec![2]), double23_solution(vec![2]))
    }
    #[test]
    fn test_double23_7() {
        assert_eq!(double23(vec![3]), double23_solution(vec![3]))
    }
    #[test]
    fn test_double23_8() {
        assert_eq!(double23(vec![]), double23_solution(vec![]))
    }
    #[test]
    fn test_double23_9() {
        assert_eq!(double23(vec![3, 4]), double23_solution(vec![3, 4]))
    }
}

#[cfg(test)]
mod test_fix23 {
    use super::implementations::fix23;
    use super::solutions::fix23_solution;

    #[test]
    fn test_fix23_1() {
        assert_eq!(fix23(vec![1, 2, 3]), fix23_solution(vec![1, 2, 3]))
    }
    #[test]
    fn test_fix23_2() {
        assert_eq!(fix23(vec![2, 3, 5]), fix23_solution(vec![2, 3, 5]))
    }
    #[test]
    fn test_fix23_3() {
        assert_eq!(fix23(vec![1, 2, 1]), fix23_solution(vec![1, 2, 1]))
    }
    #[test]
    fn test_fix23_4() {
        assert_eq!(fix23(vec![3, 2, 1]), fix23_solution(vec![3, 2, 1]))
    }
    #[test]
    fn test_fix23_5() {
        assert_eq!(fix23(vec![2, 2, 3]), fix23_solution(vec![2, 2, 3]))
    }
    #[test]
    fn test_fix23_6() {
        assert_eq!(fix23(vec![2, 3, 3]), fix23_solution(vec![2, 3, 3]))
    }
}

#[cfg(test)]
mod test_start1 {
    use super::implementations::start1;
    use super::solutions::start1_solution;

    #[test]
    fn test_start1_1() {
        assert_eq!(start1(vec![1, 2, 3], vec![1, 3]), start1_solution(vec![1, 2, 3], vec![1, 3]))
    }
    #[test]
    fn test_start1_2() {
        assert_eq!(start1(vec![7, 2, 3], vec![1]), start1_solution(vec![7, 2, 3], vec![1]))
    }
    #[test]
    fn test_start1_3() {
        assert_eq!(start1(vec![1, 2], vec![]), start1_solution(vec![1, 2], vec![]))
    }
    #[test]
    fn test_start1_4() {
        assert_eq!(start1(vec![], vec![1, 2]), start1_solution(vec![], vec![1, 2]))
    }
    #[test]
    fn test_start1_5() {
        assert_eq!(start1(vec![7], vec![]), start1_solution(vec![7], vec![]))
    }
    #[test]
    fn test_start1_6() {
        assert_eq!(start1(vec![7], vec![1]), start1_solution(vec![7], vec![1]))
    }
    #[test]
    fn test_start1_7() {
        assert_eq!(start1(vec![1], vec![1]), start1_solution(vec![1], vec![1]))
    }
    #[test]
    fn test_start1_8() {
        assert_eq!(start1(vec![7], vec![8]), start1_solution(vec![7], vec![8]))
    }
    #[test]
    fn test_start1_9() {
        assert_eq!(start1(vec![], vec![]), start1_solution(vec![], vec![]))
    }
    #[test]
    fn test_start1_10() {
        assert_eq!(start1(vec![1, 3], vec![1]), start1_solution(vec![1, 3], vec![1]))
    }
}

#[cfg(test)]
mod test_bigger_two {
    use super::implementations::bigger_two;
    use super::solutions::bigger_two_solution;

    #[test]
    fn test_bigger_two_1() {
        assert_eq!(bigger_two(vec![1, 2], vec![3, 4]), bigger_two_solution(vec![1, 2], vec![3, 4]))
    }
    #[test]
    fn test_bigger_two_2() {
        assert_eq!(bigger_two(vec![3, 4], vec![1, 2]), bigger_two_solution(vec![3, 4], vec![1, 2]))
    }
    #[test]
    fn test_bigger_two_3() {
        assert_eq!(bigger_two(vec![1, 1], vec![1, 2]), bigger_two_solution(vec![1, 1], vec![1, 2]))
    }
    #[test]
    fn test_bigger_two_4() {
        assert_eq!(bigger_two(vec![2, 1], vec![1, 1]), bigger_two_solution(vec![2, 1], vec![1, 1]))
    }
    #[test]
    fn test_bigger_two_5() {
        assert_eq!(bigger_two(vec![2, 2], vec![1, 3]), bigger_two_solution(vec![2, 2], vec![1, 3]))
    }
    #[test]
    fn test_bigger_two_6() {
        assert_eq!(bigger_two(vec![1, 3], vec![2, 2]), bigger_two_solution(vec![1, 3], vec![2, 2]))
    }
    #[test]
    fn test_bigger_two_7() {
        assert_eq!(bigger_two(vec![6, 7], vec![3, 1]), bigger_two_solution(vec![6, 7], vec![3, 1]))
    }
}

#[cfg(test)]
mod test_make_middle {
    use super::implementations::make_middle;
    use super::solutions::make_middle_solution;

    #[test]
    fn test_make_middle_1() {
        assert_eq!(make_middle(vec![1, 2, 3, 4]), make_middle_solution(vec![1, 2, 3, 4]))
    }
    #[test]
    fn test_make_middle_2() {
        assert_eq!(make_middle(vec![7, 1, 2, 3, 4, 9]), make_middle_solution(vec![7, 1, 2, 3, 4, 9]))
    }
    #[test]
    fn test_make_middle_3() {
        assert_eq!(make_middle(vec![1, 2]), make_middle_solution(vec![1, 2]))
    }
    #[test]
    fn test_make_middle_4() {
        assert_eq!(make_middle(vec![5, 2, 4, 7]), make_middle_solution(vec![5, 2, 4, 7]))
    }
    #[test]
    fn test_make_middle_5() {
        assert_eq!(make_middle(vec![9, 0, 4, 3, 9, 1]), make_middle_solution(vec![9, 0, 4, 3, 9, 1]))
    }
}

#[cfg(test)]
mod test_plus_two {
    use super::implementations::plus_two;
    use super::solutions::plus_two_solution;

    #[test]
    fn test_plus_two_1() {
        assert_eq!(plus_two(vec![1, 2], vec![3, 4]), plus_two_solution(vec![1, 2], vec![3, 4]))
    }
    #[test]
    fn test_plus_two_2() {
        assert_eq!(plus_two(vec![4, 4], vec![2, 2]), plus_two_solution(vec![4, 4], vec![2, 2]))
    }
    #[test]
    fn test_plus_two_3() {
        assert_eq!(plus_two(vec![9, 2], vec![3, 4]), plus_two_solution(vec![9, 2], vec![3, 4]))
    }
}

#[cfg(test)]
mod test_swap_ends {
    use super::implementations::swap_ends;
    use super::solutions::swap_ends_solution;

    #[test]
    fn test_swap_ends_1() {
        assert_eq!(swap_ends(vec![1, 2, 3, 4]), swap_ends_solution(vec![1, 2, 3, 4]))
    }
    #[test]
    fn test_swap_ends_2() {
        assert_eq!(swap_ends(vec![1, 2, 3]), swap_ends_solution(vec![1, 2, 3]))
    }
    #[test]
    fn test_swap_ends_3() {
        assert_eq!(swap_ends(vec![8, 6, 7, 9, 5]), swap_ends_solution(vec![8, 6, 7, 9, 5]))
    }
    #[test]
    fn test_swap_ends_4() {
        assert_eq!(swap_ends(vec![3, 1, 4, 1, 5, 9]), swap_ends_solution(vec![3, 1, 4, 1, 5, 9]))
    }
    #[test]
    fn test_swap_ends_5() {
        assert_eq!(swap_ends(vec![1, 2]), swap_ends_solution(vec![1, 2]))
    }
    #[test]
    fn test_swap_ends_6() {
        assert_eq!(swap_ends(vec![1]), swap_ends_solution(vec![1]))
    }
}

#[cfg(test)]
mod test_mid_three {
    use super::implementations::mid_three;
    use super::solutions::mid_three_solution;

    #[test]
    fn test_mid_three_1() {
        assert_eq!(mid_three(vec![1, 2, 3, 4, 5]), mid_three_solution(vec![1, 2, 3, 4, 5]))
    }
    #[test]
    fn test_mid_three_2() {
        assert_eq!(mid_three(vec![8, 6, 7, 5, 3, 0, 9]), mid_three_solution(vec![8, 6, 7, 5, 3, 0, 9]))
    }
    #[test]
    fn test_mid_three_3() {
        assert_eq!(mid_three(vec![1, 2, 3]), mid_three_solution(vec![1, 2, 3]))
    }
}

#[cfg(test)]
mod test_max_triple {
    use super::implementations::max_triple;
    use super::solutions::max_triple_solution;

    #[test]
    fn test_max_triple_1() {
        assert_eq!(max_triple(vec![1, 2, 3]), max_triple_solution(vec![1, 2, 3]))
    }
    #[test]
    fn test_max_triple_2() {
        assert_eq!(max_triple(vec![1, 5, 3]), max_triple_solution(vec![1, 5, 3]))
    }
    #[test]
    fn test_max_triple_3() {
        assert_eq!(max_triple(vec![5, 2, 3]), max_triple_solution(vec![5, 2, 3]))
    }
    #[test]
    fn test_max_triple_4() {
        assert_eq!(max_triple(vec![1, 2, 3, 1, 1]), max_triple_solution(vec![1, 2, 3, 1, 1]))
    }
    #[test]
    fn test_max_triple_5() {
        assert_eq!(max_triple(vec![1, 7, 3, 1, 5]), max_triple_solution(vec![1, 7, 3, 1, 5]))
    }
    #[test]
    fn test_max_triple_6() {
        assert_eq!(max_triple(vec![5, 1, 3, 7, 1]), max_triple_solution(vec![5, 1, 3, 7, 1]))
    }
    #[test]
    fn test_max_triple_7() {
        assert_eq!(max_triple(vec![5, 1, 7, 3, 7, 8, 1]), max_triple_solution(vec![5, 1, 7, 3, 7, 8, 1]))
    }
    #[test]
    fn test_max_triple_8() {
        assert_eq!(max_triple(vec![5, 1, 7, 9, 7, 8, 1]), max_triple_solution(vec![5, 1, 7, 9, 7, 8, 1]))
    }
    #[test]
    fn test_max_triple_9() {
        assert_eq!(max_triple(vec![5, 1, 7, 3, 7, 8, 9]), max_triple_solution(vec![5, 1, 7, 3, 7, 8, 9]))
    }
    #[test]
    fn test_max_triple_10() {
        assert_eq!(max_triple(vec![2, 2, 5, 1, 1]), max_triple_solution(vec![2, 2, 5, 1, 1]))
    }
}

#[cfg(test)]
mod test_front_piece {
    use super::implementations::front_piece;
    use super::solutions::front_piece_solution;

    #[test]
    fn test_front_piece_1() {
        assert_eq!(front_piece(vec![1, 2, 3]), front_piece_solution(vec![1, 2, 3]))
    }
    #[test]
    fn test_front_piece_2() {
        assert_eq!(front_piece(vec![1, 2]), front_piece_solution(vec![1, 2]))
    }
    #[test]
    fn test_front_piece_3() {
        assert_eq!(front_piece(vec![1]), front_piece_solution(vec![1]))
    }
    #[test]
    fn test_front_piece_4() {
        assert_eq!(front_piece(vec![]), front_piece_solution(vec![]))
    }
    #[test]
    fn test_front_piece_5() {
        assert_eq!(front_piece(vec![6, 5, 0]), front_piece_solution(vec![6, 5, 0]))
    }
    #[test]
    fn test_front_piece_6() {
        assert_eq!(front_piece(vec![6, 5]), front_piece_solution(vec![6, 5]))
    }
    #[test]
    fn test_front_piece_7() {
        assert_eq!(front_piece(vec![3, 1, 4, 1, 5]), front_piece_solution(vec![3, 1, 4, 1, 5]))
    }
    #[test]
    fn test_front_piece_8() {
        assert_eq!(front_piece(vec![6]), front_piece_solution(vec![6]))
    }
}

#[cfg(test)]
mod test_unlucky1 {
    use super::implementations::unlucky1;
    use super::solutions::unlucky1_solution;

    #[test]
    fn test_unlucky1_1() {
        assert_eq!(unlucky1(vec![1, 3, 4, 5]), unlucky1_solution(vec![1, 3, 4, 5]))
    }
    #[test]
    fn test_unlucky1_2() {
        assert_eq!(unlucky1(vec![2, 1, 3, 4, 5]), unlucky1_solution(vec![2, 1, 3, 4, 5]))
    }
    #[test]
    fn test_unlucky1_3() {
        assert_eq!(unlucky1(vec![1, 1, 1]), unlucky1_solution(vec![1, 1, 1]))
    }
    #[test]
    fn test_unlucky1_4() {
        assert_eq!(unlucky1(vec![1, 3, 1]), unlucky1_solution(vec![1, 3, 1]))
    }
    #[test]
    fn test_unlucky1_5() {
        assert_eq!(unlucky1(vec![1, 1, 3]), unlucky1_solution(vec![1, 1, 3]))
    }
    #[test]
    fn test_unlucky1_6() {
        assert_eq!(unlucky1(vec![1, 2, 3]), unlucky1_solution(vec![1, 2, 3]))
    }
    #[test]
    fn test_unlucky1_7() {
        assert_eq!(unlucky1(vec![3, 3, 3]), unlucky1_solution(vec![3, 3, 3]))
    }
    #[test]
    fn test_unlucky1_8() {
        assert_eq!(unlucky1(vec![1, 3]), unlucky1_solution(vec![1, 3]))
    }
    #[test]
    fn test_unlucky1_9() {
        assert_eq!(unlucky1(vec![1, 4]), unlucky1_solution(vec![1, 4]))
    }
    #[test]
    fn test_unlucky1_10() {
        assert_eq!(unlucky1(vec![1]), unlucky1_solution(vec![1]))
    }
    #[test]
    fn test_unlucky1_11() {
        assert_eq!(unlucky1(vec![]), unlucky1_solution(vec![]))
    }
    #[test]
    fn test_unlucky1_12() {
        assert_eq!(unlucky1(vec![1, 1, 1, 3, 1]), unlucky1_solution(vec![1, 1, 1, 3, 1]))
    }
    #[test]
    fn test_unlucky1_13() {
        assert_eq!(unlucky1(vec![1, 1, 3, 1, 1]), unlucky1_solution(vec![1, 1, 3, 1, 1]))
    }
    #[test]
    fn test_unlucky1_14() {
        assert_eq!(unlucky1(vec![1, 1, 1, 1, 3]), unlucky1_solution(vec![1, 1, 1, 1, 3]))
    }
    #[test]
    fn test_unlucky1_15() {
        assert_eq!(unlucky1(vec![1, 4, 1, 5]), unlucky1_solution(vec![1, 4, 1, 5]))
    }
    #[test]
    fn test_unlucky1_16() {
        assert_eq!(unlucky1(vec![1, 1, 2, 3]), unlucky1_solution(vec![1, 1, 2, 3]))
    }
    #[test]
    fn test_unlucky1_17() {
        assert_eq!(unlucky1(vec![2, 3, 2, 1]), unlucky1_solution(vec![2, 3, 2, 1]))
    }
    #[test]
    fn test_unlucky1_18() {
        assert_eq!(unlucky1(vec![2, 3, 1, 3]), unlucky1_solution(vec![2, 3, 1, 3]))
    }
    #[test]
    fn test_unlucky1_19() {
        assert_eq!(unlucky1(vec![1, 2, 3, 4, 1, 3]), unlucky1_solution(vec![1, 2, 3, 4, 1, 3]))
    }
}

#[cfg(test)]
mod test_make2 {
    use super::implementations::make2;
    use super::solutions::make2_solution;

    #[test]
    fn test_make2_1() {
        assert_eq!(make2(vec![4, 5], vec![1, 2, 3]), make2_solution(vec![4, 5], vec![1, 2, 3]))
    }
    #[test]
    fn test_make2_2() {
        assert_eq!(make2(vec![4], vec![1, 2, 3]), make2_solution(vec![4], vec![1, 2, 3]))
    }
    #[test]
    fn test_make2_3() {
        assert_eq!(make2(vec![], vec![1, 2]), make2_solution(vec![], vec![1, 2]))
    }
    #[test]
    fn test_make2_4() {
        assert_eq!(make2(vec![1, 2], vec![]), make2_solution(vec![1, 2], vec![]))
    }
    #[test]
    fn test_make2_5() {
        assert_eq!(make2(vec![3], vec![1, 2, 3]), make2_solution(vec![3], vec![1, 2, 3]))
    }
    #[test]
    fn test_make2_6() {
        assert_eq!(make2(vec![3], vec![1]), make2_solution(vec![3], vec![1]))
    }
    #[test]
    fn test_make2_7() {
        assert_eq!(make2(vec![3, 1, 4], vec![]), make2_solution(vec![3, 1, 4], vec![]))
    }
    #[test]
    fn test_make2_8() {
        assert_eq!(make2(vec![1], vec![1]), make2_solution(vec![1], vec![1]))
    }
    #[test]
    fn test_make2_9() {
        assert_eq!(make2(vec![1, 2, 3], vec![7, 8]), make2_solution(vec![1, 2, 3], vec![7, 8]))
    }
    #[test]
    fn test_make2_10() {
        assert_eq!(make2(vec![7, 8], vec![1, 2, 3]), make2_solution(vec![7, 8], vec![1, 2, 3]))
    }
    #[test]
    fn test_make2_11() {
        assert_eq!(make2(vec![7], vec![1, 2, 3]), make2_solution(vec![7], vec![1, 2, 3]))
    }
    #[test]
    fn test_make2_12() {
        assert_eq!(make2(vec![5, 4], vec![2, 3, 7]), make2_solution(vec![5, 4], vec![2, 3, 7]))
    }
}

#[cfg(test)]
mod test_front11 {
    use super::implementations::front11;
    use super::solutions::front11_solution;

    #[test]
    fn test_front11_1() {
        assert_eq!(front11(vec![1, 2, 3], vec![7, 9, 8]), front11_solution(vec![1, 2, 3], vec![7, 9, 8]))
    }
    #[test]
    fn test_front11_2() {
        assert_eq!(front11(vec![1], vec![2]), front11_solution(vec![1], vec![2]))
    }
    #[test]
    fn test_front11_3() {
        assert_eq!(front11(vec![1, 7], vec![]), front11_solution(vec![1, 7], vec![]))
    }
    #[test]
    fn test_front11_4() {
        assert_eq!(front11(vec![], vec![2, 8]), front11_solution(vec![], vec![2, 8]))
    }
    #[test]
    fn test_front11_5() {
        assert_eq!(front11(vec![], vec![]), front11_solution(vec![], vec![]))
    }
    #[test]
    fn test_front11_6() {
        assert_eq!(front11(vec![3], vec![1, 4, 1, 9]), front11_solution(vec![3], vec![1, 4, 1, 9]))
    }
    #[test]
    fn test_front11_7() {
        assert_eq!(front11(vec![1, 4, 1, 9], vec![]), front11_solution(vec![1, 4, 1, 9], vec![]))
    }
}

#[cfg(test)]
mod test_find_lowest_index {
    use super::implementations::find_lowest_index;
    use super::solutions::find_lowest_index_solution;

    #[test]
    fn test_find_lowest_index_1() {
        assert_eq!(find_lowest_index(vec![99, 98, 97, 96, 95]), find_lowest_index_solution(vec![99, 98, 97, 96, 95]))
    }
    #[test]
    fn test_find_lowest_index_2() {
        assert_eq!(find_lowest_index(vec![2, 2, 0]), find_lowest_index_solution(vec![2, 2, 0]))
    }
    #[test]
    fn test_find_lowest_index_3() {
        assert_eq!(find_lowest_index(vec![1, 3, 5]), find_lowest_index_solution(vec![1, 3, 5]))
    }
    #[test]
    fn test_find_lowest_index_4() {
        assert_eq!(find_lowest_index(vec![5]), find_lowest_index_solution(vec![5]))
    }
    #[test]
    fn test_find_lowest_index_5() {
        assert_eq!(find_lowest_index(vec![11, 9, 0, 1]), find_lowest_index_solution(vec![11, 9, 0, 1]))
    }
    #[test]
    fn test_find_lowest_index_6() {
        assert_eq!(find_lowest_index(vec![2, 11, 9, 0]), find_lowest_index_solution(vec![2, 11, 9, 0]))
    }
    #[test]
    fn test_find_lowest_index_7() {
        assert_eq!(find_lowest_index(vec![2]), find_lowest_index_solution(vec![2]))
    }
    #[test]
    fn test_find_lowest_index_8() {
        assert_eq!(find_lowest_index(vec![2, 5, -12]), find_lowest_index_solution(vec![2, 5, -12]))
    }
}

#[cfg(test)]
mod test_count_evens {
    use super::implementations::count_evens;
    use super::solutions::count_evens_solution;

    #[test]
    fn test_count_evens_1() {
        assert_eq!(count_evens(vec![2, 1, 2, 3, 4]), count_evens_solution(vec![2, 1, 2, 3, 4]))
    }
    #[test]
    fn test_count_evens_2() {
        assert_eq!(count_evens(vec![2, 2, 0]), count_evens_solution(vec![2, 2, 0]))
    }
    #[test]
    fn test_count_evens_3() {
        assert_eq!(count_evens(vec![1, 3, 5]), count_evens_solution(vec![1, 3, 5]))
    }
    #[test]
    fn test_count_evens_4() {
        assert_eq!(count_evens(vec![]), count_evens_solution(vec![]))
    }
    #[test]
    fn test_count_evens_5() {
        assert_eq!(count_evens(vec![11, 9, 0, 1]), count_evens_solution(vec![11, 9, 0, 1]))
    }
    #[test]
    fn test_count_evens_6() {
        assert_eq!(count_evens(vec![2, 11, 9, 0]), count_evens_solution(vec![2, 11, 9, 0]))
    }
    #[test]
    fn test_count_evens_7() {
        assert_eq!(count_evens(vec![2]), count_evens_solution(vec![2]))
    }
    #[test]
    fn test_count_evens_8() {
        assert_eq!(count_evens(vec![2, 5, 12]), count_evens_solution(vec![2, 5, 12]))
    }
}

#[cfg(test)]
mod test_big_diff {
    use super::implementations::big_diff;
    use super::solutions::big_diff_solution;

    #[test]
    fn test_big_diff_1() {
        assert_eq!(big_diff(vec![10, 3, 5, 6]), big_diff_solution(vec![10, 3, 5, 6]))
    }
    #[test]
    fn test_big_diff_2() {
        assert_eq!(big_diff(vec![7, 2, 10, 9]), big_diff_solution(vec![7, 2, 10, 9]))
    }
    #[test]
    fn test_big_diff_3() {
        assert_eq!(big_diff(vec![2, 10, 7, 2]), big_diff_solution(vec![2, 10, 7, 2]))
    }
    #[test]
    fn test_big_diff_4() {
        assert_eq!(big_diff(vec![2, 10]), big_diff_solution(vec![2, 10]))
    }
    #[test]
    fn test_big_diff_5() {
        assert_eq!(big_diff(vec![10, 2]), big_diff_solution(vec![10, 2]))
    }
    #[test]
    fn test_big_diff_6() {
        assert_eq!(big_diff(vec![10, 0]), big_diff_solution(vec![10, 0]))
    }
    #[test]
    fn test_big_diff_7() {
        assert_eq!(big_diff(vec![2, 3]), big_diff_solution(vec![2, 3]))
    }
    #[test]
    fn test_big_diff_8() {
        assert_eq!(big_diff(vec![2, 2]), big_diff_solution(vec![2, 2]))
    }
    #[test]
    fn test_big_diff_9() {
        assert_eq!(big_diff(vec![2]), big_diff_solution(vec![2]))
    }
    #[test]
    fn test_big_diff_10() {
        assert_eq!(big_diff(vec![5, 1, 6, 1, 9, 9]), big_diff_solution(vec![5, 1, 6, 1, 9, 9]))
    }
    #[test]
    fn test_big_diff_11() {
        assert_eq!(big_diff(vec![7, 6, 8, 5]), big_diff_solution(vec![7, 6, 8, 5]))
    }
    #[test]
    fn test_big_diff_12() {
        assert_eq!(big_diff(vec![7, 7, 6, 8, 5, 5, 6]), big_diff_solution(vec![7, 7, 6, 8, 5, 5, 6]))
    }
}

#[cfg(test)]
mod test_centered_average {
    use super::implementations::centered_average;
    use super::solutions::centered_average_solution;

    #[test]
    fn test_centered_average_1() {
        assert_eq!(centered_average(vec![1, 2, 3, 4, 100]), centered_average_solution(vec![1, 2, 3, 4, 100]))
    }
    #[test]
    fn test_centered_average_2() {
        assert_eq!(centered_average(vec![1, 1, 5, 5, 10, 8, 7]), centered_average_solution(vec![1, 1, 5, 5, 10, 8, 7]))
    }
    #[test]
    fn test_centered_average_3() {
        assert_eq!(centered_average(vec![-10, -4, -2, -4, -2, 0]), centered_average_solution(vec![-10, -4, -2, -4, -2, 0]))
    }
    #[test]
    fn test_centered_average_4() {
        assert_eq!(centered_average(vec![5, 3, 4, 6, 2]), centered_average_solution(vec![5, 3, 4, 6, 2]))
    }
    #[test]
    fn test_centered_average_5() {
        assert_eq!(centered_average(vec![5, 3, 4, 0, 100]), centered_average_solution(vec![5, 3, 4, 0, 100]))
    }
    #[test]
    fn test_centered_average_6() {
        assert_eq!(centered_average(vec![100, 0, 5, 3, 4]), centered_average_solution(vec![100, 0, 5, 3, 4]))
    }
    #[test]
    fn test_centered_average_7() {
        assert_eq!(centered_average(vec![4, 0, 100]), centered_average_solution(vec![4, 0, 100]))
    }
    #[test]
    fn test_centered_average_8() {
        assert_eq!(centered_average(vec![0, 2, 3, 4, 100]), centered_average_solution(vec![0, 2, 3, 4, 100]))
    }
    #[test]
    fn test_centered_average_9() {
        assert_eq!(centered_average(vec![1, 1, 100]), centered_average_solution(vec![1, 1, 100]))
    }
    #[test]
    fn test_centered_average_10() {
        assert_eq!(centered_average(vec![7, 7, 7]), centered_average_solution(vec![7, 7, 7]))
    }
    #[test]
    fn test_centered_average_11() {
        assert_eq!(centered_average(vec![1, 7, 8]), centered_average_solution(vec![1, 7, 8]))
    }
    #[test]
    fn test_centered_average_12() {
        assert_eq!(centered_average(vec![1, 1, 99, 99]), centered_average_solution(vec![1, 1, 99, 99]))
    }
    #[test]
    fn test_centered_average_13() {
        assert_eq!(centered_average(vec![1000, 0, 1, 99]), centered_average_solution(vec![1000, 0, 1, 99]))
    }
    #[test]
    fn test_centered_average_14() {
        assert_eq!(centered_average(vec![4, 4, 4, 4, 5]), centered_average_solution(vec![4, 4, 4, 4, 5]))
    }
    #[test]
    fn test_centered_average_15() {
        assert_eq!(centered_average(vec![4, 4, 4, 1, 5]), centered_average_solution(vec![4, 4, 4, 1, 5]))
    }
    #[test]
    fn test_centered_average_16() {
        assert_eq!(centered_average(vec![6, 4, 8, 12, 3]), centered_average_solution(vec![6, 4, 8, 12, 3]))
    }
}

#[cfg(test)]
mod test_sum13 {
    use super::implementations::sum13;
    use super::solutions::sum13_solution;

    #[test]
    fn test_sum13_1() {
        assert_eq!(sum13(vec![1, 2, 2, 1]), sum13_solution(vec![1, 2, 2, 1]))
    }
    #[test]
    fn test_sum13_2() {
        assert_eq!(sum13(vec![1, 1]), sum13_solution(vec![1, 1]))
    }
    #[test]
    fn test_sum13_3() {
        assert_eq!(sum13(vec![1, 2, 2, 1, 13]), sum13_solution(vec![1, 2, 2, 1, 13]))
    }
    #[test]
    fn test_sum13_4() {
        assert_eq!(sum13(vec![1, 2, 13, 2, 1, 13]), sum13_solution(vec![1, 2, 13, 2, 1, 13]))
    }
    #[test]
    fn test_sum13_5() {
        assert_eq!(sum13(vec![13, 1, 2, 13, 2, 1, 13]), sum13_solution(vec![13, 1, 2, 13, 2, 1, 13]))
    }
    #[test]
    fn test_sum13_6() {
        assert_eq!(sum13(vec![]), sum13_solution(vec![]))
    }
    #[test]
    fn test_sum13_7() {
        assert_eq!(sum13(vec![13]), sum13_solution(vec![13]))
    }
    #[test]
    fn test_sum13_8() {
        assert_eq!(sum13(vec![13, 13]), sum13_solution(vec![13, 13]))
    }
    #[test]
    fn test_sum13_9() {
        assert_eq!(sum13(vec![13, 0, 13]), sum13_solution(vec![13, 0, 13]))
    }
    #[test]
    fn test_sum13_10() {
        assert_eq!(sum13(vec![13, 1, 13]), sum13_solution(vec![13, 1, 13]))
    }
    #[test]
    fn test_sum13_11() {
        assert_eq!(sum13(vec![5, 7, 2]), sum13_solution(vec![5, 7, 2]))
    }
    #[test]
    fn test_sum13_12() {
        assert_eq!(sum13(vec![5, 13, 2]), sum13_solution(vec![5, 13, 2]))
    }
    #[test]
    fn test_sum13_13() {
        assert_eq!(sum13(vec![0]), sum13_solution(vec![0]))
    }
    #[test]
    fn test_sum13_14() {
        assert_eq!(sum13(vec![13, 0]), sum13_solution(vec![13, 0]))
    }
}

#[cfg(test)]
mod test_sum67 {
    use super::implementations::sum67;
    use super::solutions::sum67_solution;

    #[test]
    fn test_sum67_1() {
        assert_eq!(sum67(vec![1, 2, 2]), sum67_solution(vec![1, 2, 2]))
    }
    #[test]
    fn test_sum67_2() {
        assert_eq!(sum67(vec![1, 2, 2, 6, 99, 99, 7]), sum67_solution(vec![1, 2, 2, 6, 99, 99, 7]))
    }
    #[test]
    fn test_sum67_3() {
        assert_eq!(sum67(vec![1, 1, 6, 7, 2]), sum67_solution(vec![1, 1, 6, 7, 2]))
    }
    #[test]
    fn test_sum67_4() {
        assert_eq!(sum67(vec![1, 6, 2, 2, 7, 1, 6, 99, 99, 7]), sum67_solution(vec![1, 6, 2, 2, 7, 1, 6, 99, 99, 7]))
    }
    #[test]
    fn test_sum67_5() {
        assert_eq!(sum67(vec![1, 6, 2, 6, 2, 7, 1, 6, 99, 99, 7]), sum67_solution(vec![1, 6, 2, 6, 2, 7, 1, 6, 99, 99, 7]))
    }
    #[test]
    fn test_sum67_6() {
        assert_eq!(sum67(vec![2, 7, 6, 2, 6, 7, 2, 7]), sum67_solution(vec![2, 7, 6, 2, 6, 7, 2, 7]))
    }
    #[test]
    fn test_sum67_7() {
        assert_eq!(sum67(vec![2, 7, 6, 2, 6, 2, 7]), sum67_solution(vec![2, 7, 6, 2, 6, 2, 7]))
    }
    #[test]
    fn test_sum67_8() {
        assert_eq!(sum67(vec![1, 6, 7, 7]), sum67_solution(vec![1, 6, 7, 7]))
    }
    #[test]
    fn test_sum67_9() {
        assert_eq!(sum67(vec![6, 7, 1, 6, 7, 7]), sum67_solution(vec![6, 7, 1, 6, 7, 7]))
    }
    #[test]
    fn test_sum67_10() {
        assert_eq!(sum67(vec![6, 8, 1, 6, 7]), sum67_solution(vec![6, 8, 1, 6, 7]))
    }
    #[test]
    fn test_sum67_11() {
        assert_eq!(sum67(vec![]), sum67_solution(vec![]))
    }
    #[test]
    fn test_sum67_12() {
        assert_eq!(sum67(vec![6, 7, 11]), sum67_solution(vec![6, 7, 11]))
    }
    #[test]
    fn test_sum67_13() {
        assert_eq!(sum67(vec![11, 6, 7, 11]), sum67_solution(vec![11, 6, 7, 11]))
    }
    #[test]
    fn test_sum67_14() {
        assert_eq!(sum67(vec![2, 2, 6, 7, 7]), sum67_solution(vec![2, 2, 6, 7, 7]))
    }
}

#[cfg(test)]
mod test_has22 {
    use super::implementations::has22;
    use super::solutions::has22_solution;

    #[test]
    fn test_has22_1() {
        assert_eq!(has22(vec![1, 2, 2]), has22_solution(vec![1, 2, 2]))
    }
    #[test]
    fn test_has22_2() {
        assert_eq!(has22(vec![1, 2, 1, 2]), has22_solution(vec![1, 2, 1, 2]))
    }
    #[test]
    fn test_has22_3() {
        assert_eq!(has22(vec![2, 1, 2]), has22_solution(vec![2, 1, 2]))
    }
    #[test]
    fn test_has22_4() {
        assert_eq!(has22(vec![2, 2, 1, 2]), has22_solution(vec![2, 2, 1, 2]))
    }
    #[test]
    fn test_has22_5() {
        assert_eq!(has22(vec![1, 3, 2]), has22_solution(vec![1, 3, 2]))
    }
    #[test]
    fn test_has22_6() {
        assert_eq!(has22(vec![1, 3, 2, 2]), has22_solution(vec![1, 3, 2, 2]))
    }
    #[test]
    fn test_has22_7() {
        assert_eq!(has22(vec![2, 3, 2, 2]), has22_solution(vec![2, 3, 2, 2]))
    }
    #[test]
    fn test_has22_8() {
        assert_eq!(has22(vec![4, 2, 4, 2, 2, 5]), has22_solution(vec![4, 2, 4, 2, 2, 5]))
    }
    #[test]
    fn test_has22_9() {
        assert_eq!(has22(vec![1, 2]), has22_solution(vec![1, 2]))
    }
    #[test]
    fn test_has22_10() {
        assert_eq!(has22(vec![2, 2]), has22_solution(vec![2, 2]))
    }
    #[test]
    fn test_has22_11() {
        assert_eq!(has22(vec![2]), has22_solution(vec![2]))
    }
    #[test]
    fn test_has22_12() {
        assert_eq!(has22(vec![]), has22_solution(vec![]))
    }
    #[test]
    fn test_has22_13() {
        assert_eq!(has22(vec![3, 3, 2, 2]), has22_solution(vec![3, 3, 2, 2]))
    }
    #[test]
    fn test_has22_14() {
        assert_eq!(has22(vec![5, 2, 5, 2]), has22_solution(vec![5, 2, 5, 2]))
    }
}

#[cfg(test)]
mod test_lucky13 {
    use super::implementations::lucky13;
    use super::solutions::lucky13_solution;

    #[test]
    fn test_lucky13_1() {
        assert_eq!(lucky13(vec![0, 2, 4]), lucky13_solution(vec![0, 2, 4]))
    }
    #[test]
    fn test_lucky13_2() {
        assert_eq!(lucky13(vec![1, 2, 3]), lucky13_solution(vec![1, 2, 3]))
    }
    #[test]
    fn test_lucky13_3() {
        assert_eq!(lucky13(vec![1, 2, 4]), lucky13_solution(vec![1, 2, 4]))
    }
    #[test]
    fn test_lucky13_4() {
        assert_eq!(lucky13(vec![2, 7, 2, 8]), lucky13_solution(vec![2, 7, 2, 8]))
    }
    #[test]
    fn test_lucky13_5() {
        assert_eq!(lucky13(vec![2, 7, 1, 8]), lucky13_solution(vec![2, 7, 1, 8]))
    }
    #[test]
    fn test_lucky13_6() {
        assert_eq!(lucky13(vec![3, 7, 2, 8]), lucky13_solution(vec![3, 7, 2, 8]))
    }
    #[test]
    fn test_lucky13_7() {
        assert_eq!(lucky13(vec![2, 7, 2, 1]), lucky13_solution(vec![2, 7, 2, 1]))
    }
    #[test]
    fn test_lucky13_8() {
        assert_eq!(lucky13(vec![1, 2]), lucky13_solution(vec![1, 2]))
    }
    #[test]
    fn test_lucky13_9() {
        assert_eq!(lucky13(vec![2, 2]), lucky13_solution(vec![2, 2]))
    }
    #[test]
    fn test_lucky13_10() {
        assert_eq!(lucky13(vec![2]), lucky13_solution(vec![2]))
    }
    #[test]
    fn test_lucky13_11() {
        assert_eq!(lucky13(vec![3]), lucky13_solution(vec![3]))
    }
    #[test]
    fn test_lucky13_12() {
        assert_eq!(lucky13(vec![]), lucky13_solution(vec![]))
    }
}

#[cfg(test)]
mod test_sum28 {
    use super::implementations::sum28;
    use super::solutions::sum28_solution;

    #[test]
    fn test_sum28_1() {
        assert_eq!(sum28(vec![2, 3, 2, 2, 4, 2]), sum28_solution(vec![2, 3, 2, 2, 4, 2]))
    }
    #[test]
    fn test_sum28_2() {
        assert_eq!(sum28(vec![2, 3, 2, 2, 4, 2, 2]), sum28_solution(vec![2, 3, 2, 2, 4, 2, 2]))
    }
    #[test]
    fn test_sum28_3() {
        assert_eq!(sum28(vec![1, 2, 3, 4]), sum28_solution(vec![1, 2, 3, 4]))
    }
    #[test]
    fn test_sum28_4() {
        assert_eq!(sum28(vec![2, 2, 2, 2]), sum28_solution(vec![2, 2, 2, 2]))
    }
    #[test]
    fn test_sum28_5() {
        assert_eq!(sum28(vec![1, 2, 2, 2, 2, 4]), sum28_solution(vec![1, 2, 2, 2, 2, 4]))
    }
    #[test]
    fn test_sum28_6() {
        assert_eq!(sum28(vec![]), sum28_solution(vec![]))
    }
    #[test]
    fn test_sum28_7() {
        assert_eq!(sum28(vec![2]), sum28_solution(vec![2]))
    }
    #[test]
    fn test_sum28_8() {
        assert_eq!(sum28(vec![8]), sum28_solution(vec![8]))
    }
    #[test]
    fn test_sum28_9() {
        assert_eq!(sum28(vec![2, 2, 2]), sum28_solution(vec![2, 2, 2]))
    }
    #[test]
    fn test_sum28_10() {
        assert_eq!(sum28(vec![2, 2, 2, 2, 2]), sum28_solution(vec![2, 2, 2, 2, 2]))
    }
    #[test]
    fn test_sum28_11() {
        assert_eq!(sum28(vec![1, 2, 2, 1, 2, 2]), sum28_solution(vec![1, 2, 2, 1, 2, 2]))
    }
    #[test]
    fn test_sum28_12() {
        assert_eq!(sum28(vec![5, 2, 2, 2, 4, 2]), sum28_solution(vec![5, 2, 2, 2, 4, 2]))
    }
}

#[cfg(test)]
mod test_more14 {
    use super::implementations::more14;
    use super::solutions::more14_solution;

    #[test]
    fn test_more14_1() {
        assert_eq!(more14(vec![1, 4, 1]), more14_solution(vec![1, 4, 1]))
    }
    #[test]
    fn test_more14_2() {
        assert_eq!(more14(vec![1, 4, 1, 4]), more14_solution(vec![1, 4, 1, 4]))
    }
    #[test]
    fn test_more14_3() {
        assert_eq!(more14(vec![1, 1]), more14_solution(vec![1, 1]))
    }
    #[test]
    fn test_more14_4() {
        assert_eq!(more14(vec![1, 6, 6]), more14_solution(vec![1, 6, 6]))
    }
    #[test]
    fn test_more14_5() {
        assert_eq!(more14(vec![1]), more14_solution(vec![1]))
    }
    #[test]
    fn test_more14_6() {
        assert_eq!(more14(vec![1, 4]), more14_solution(vec![1, 4]))
    }
    #[test]
    fn test_more14_7() {
        assert_eq!(more14(vec![6, 1, 1]), more14_solution(vec![6, 1, 1]))
    }
    #[test]
    fn test_more14_8() {
        assert_eq!(more14(vec![1, 6, 4]), more14_solution(vec![1, 6, 4]))
    }
    #[test]
    fn test_more14_9() {
        assert_eq!(more14(vec![1, 1, 4, 4, 1]), more14_solution(vec![1, 1, 4, 4, 1]))
    }
    #[test]
    fn test_more14_10() {
        assert_eq!(more14(vec![1, 1, 6, 4, 4, 1]), more14_solution(vec![1, 1, 6, 4, 4, 1]))
    }
    #[test]
    fn test_more14_11() {
        assert_eq!(more14(vec![]), more14_solution(vec![]))
    }
    #[test]
    fn test_more14_12() {
        assert_eq!(more14(vec![4, 1, 4, 6]), more14_solution(vec![4, 1, 4, 6]))
    }
    #[test]
    fn test_more14_13() {
        assert_eq!(more14(vec![4, 1, 4, 6, 1]), more14_solution(vec![4, 1, 4, 6, 1]))
    }
    #[test]
    fn test_more14_14() {
        assert_eq!(more14(vec![1, 4, 1, 4, 1, 6]), more14_solution(vec![1, 4, 1, 4, 1, 6]))
    }
}

#[cfg(test)]
mod test_prepend_sum {
    use super::implementations::prepend_sum;
    use super::solutions::prepend_sum_solution;

    #[test]
    fn test_prepend_sum_1() {
        assert_eq!(prepend_sum(vec![1, 2, 4, 4]), prepend_sum_solution(vec![1, 2, 4, 4]))
    }
    #[test]
    fn test_prepend_sum_2() {
        assert_eq!(prepend_sum(vec![3, 3, 0]), prepend_sum_solution(vec![3, 3, 0]))
    }
    #[test]
    fn test_prepend_sum_3() {
        assert_eq!(prepend_sum(vec![1, 1, 1, 1, 1]), prepend_sum_solution(vec![1, 1, 1, 1, 1]))
    }
    #[test]
    fn test_prepend_sum_4() {
        assert_eq!(prepend_sum(vec![5, 7]), prepend_sum_solution(vec![5, 7]))
    }
    #[test]
    fn test_prepend_sum_5() {
        assert_eq!(prepend_sum(vec![0, 0, 0, 0]), prepend_sum_solution(vec![0, 0, 0, 0]))
    }
    #[test]
    fn test_prepend_sum_6() {
        assert_eq!(prepend_sum(vec![12, 13, 19, 20]), prepend_sum_solution(vec![12, 13, 19, 20]))
    }
    #[test]
    fn test_prepend_sum_7() {
        assert_eq!(prepend_sum(vec![-2, 2, -2, 2]), prepend_sum_solution(vec![-2, 2, -2, 2]))
    }
    #[test]
    fn test_prepend_sum_8() {
        assert_eq!(prepend_sum(vec![5, 4, 3, 2, 1, 0]), prepend_sum_solution(vec![5, 4, 3, 2, 1, 0]))
    }
}

#[cfg(test)]
mod test_fizz_array {
    use super::implementations::fizz_array;
    use super::solutions::fizz_array_solution;

    #[test]
    fn test_fizz_array_1() {
        assert_eq!(fizz_array(4), fizz_array_solution(4))
    }
    #[test]
    fn test_fizz_array_2() {
        assert_eq!(fizz_array(1), fizz_array_solution(1))
    }
    #[test]
    fn test_fizz_array_3() {
        assert_eq!(fizz_array(10), fizz_array_solution(10))
    }
    #[test]
    fn test_fizz_array_4() {
        assert_eq!(fizz_array(0), fizz_array_solution(0))
    }
    #[test]
    fn test_fizz_array_5() {
        assert_eq!(fizz_array(2), fizz_array_solution(2))
    }
    #[test]
    fn test_fizz_array_6() {
        assert_eq!(fizz_array(7), fizz_array_solution(7))
    }
}

#[cfg(test)]
mod test_only14 {
    use super::implementations::only14;
    use super::solutions::only14_solution;

    #[test]
    fn test_only14_1() {
        assert_eq!(only14(vec![1, 4, 1, 4]), only14_solution(vec![1, 4, 1, 4]))
    }
    #[test]
    fn test_only14_2() {
        assert_eq!(only14(vec![1, 4, 2, 4]), only14_solution(vec![1, 4, 2, 4]))
    }
    #[test]
    fn test_only14_3() {
        assert_eq!(only14(vec![1, 1]), only14_solution(vec![1, 1]))
    }
    #[test]
    fn test_only14_4() {
        assert_eq!(only14(vec![4, 1]), only14_solution(vec![4, 1]))
    }
    #[test]
    fn test_only14_5() {
        assert_eq!(only14(vec![2]), only14_solution(vec![2]))
    }
    #[test]
    fn test_only14_6() {
        assert_eq!(only14(vec![]), only14_solution(vec![]))
    }
    #[test]
    fn test_only14_7() {
        assert_eq!(only14(vec![1, 4, 1, 3]), only14_solution(vec![1, 4, 1, 3]))
    }
    #[test]
    fn test_only14_8() {
        assert_eq!(only14(vec![3, 1, 3]), only14_solution(vec![3, 1, 3]))
    }
    #[test]
    fn test_only14_9() {
        assert_eq!(only14(vec![1]), only14_solution(vec![1]))
    }
    #[test]
    fn test_only14_10() {
        assert_eq!(only14(vec![4]), only14_solution(vec![4]))
    }
    #[test]
    fn test_only14_11() {
        assert_eq!(only14(vec![3, 4]), only14_solution(vec![3, 4]))
    }
    #[test]
    fn test_only14_12() {
        assert_eq!(only14(vec![1, 3, 4]), only14_solution(vec![1, 3, 4]))
    }
    #[test]
    fn test_only14_13() {
        assert_eq!(only14(vec![1, 1, 1]), only14_solution(vec![1, 1, 1]))
    }
    #[test]
    fn test_only14_14() {
        assert_eq!(only14(vec![1, 1, 1, 5]), only14_solution(vec![1, 1, 1, 5]))
    }
    #[test]
    fn test_only14_15() {
        assert_eq!(only14(vec![4, 1, 4, 1]), only14_solution(vec![4, 1, 4, 1]))
    }
}

#[cfg(test)]
mod test_fizz_array2 {
    use super::implementations::fizz_array2;
    use super::solutions::fizz_array2_solution;

    #[test]
    fn test_fizz_array2_1() {
        assert_eq!(fizz_array2(4), fizz_array2_solution(4))
    }
    #[test]
    fn test_fizz_array2_2() {
        assert_eq!(fizz_array2(10), fizz_array2_solution(10))
    }
    #[test]
    fn test_fizz_array2_3() {
        assert_eq!(fizz_array2(2), fizz_array2_solution(2))
    }
    #[test]
    fn test_fizz_array2_4() {
        assert_eq!(fizz_array2(1), fizz_array2_solution(1))
    }
    #[test]
    fn test_fizz_array2_5() {
        assert_eq!(fizz_array2(0), fizz_array2_solution(0))
    }
    #[test]
    fn test_fizz_array2_6() {
        assert_eq!(fizz_array2(7), fizz_array2_solution(7))
    }
    #[test]
    fn test_fizz_array2_7() {
        assert_eq!(fizz_array2(9), fizz_array2_solution(9))
    }
    #[test]
    fn test_fizz_array2_8() {
        assert_eq!(fizz_array2(11), fizz_array2_solution(11))
    }
}

#[cfg(test)]
mod test_no14 {
    use super::implementations::no14;
    use super::solutions::no14_solution;

    #[test]
    fn test_no14_1() {
        assert_eq!(no14(vec![1, 2, 3]), no14_solution(vec![1, 2, 3]))
    }
    #[test]
    fn test_no14_2() {
        assert_eq!(no14(vec![1, 2, 3, 4]), no14_solution(vec![1, 2, 3, 4]))
    }
    #[test]
    fn test_no14_3() {
        assert_eq!(no14(vec![2, 3, 4]), no14_solution(vec![2, 3, 4]))
    }
    #[test]
    fn test_no14_4() {
        assert_eq!(no14(vec![1, 1, 4, 4]), no14_solution(vec![1, 1, 4, 4]))
    }
    #[test]
    fn test_no14_5() {
        assert_eq!(no14(vec![2, 2, 4, 4]), no14_solution(vec![2, 2, 4, 4]))
    }
    #[test]
    fn test_no14_6() {
        assert_eq!(no14(vec![2, 3, 4, 1]), no14_solution(vec![2, 3, 4, 1]))
    }
    #[test]
    fn test_no14_7() {
        assert_eq!(no14(vec![2, 1, 1]), no14_solution(vec![2, 1, 1]))
    }
    #[test]
    fn test_no14_8() {
        assert_eq!(no14(vec![1, 4]), no14_solution(vec![1, 4]))
    }
    #[test]
    fn test_no14_9() {
        assert_eq!(no14(vec![2]), no14_solution(vec![2]))
    }
    #[test]
    fn test_no14_10() {
        assert_eq!(no14(vec![2, 1]), no14_solution(vec![2, 1]))
    }
    #[test]
    fn test_no14_11() {
        assert_eq!(no14(vec![1]), no14_solution(vec![1]))
    }
    #[test]
    fn test_no14_12() {
        assert_eq!(no14(vec![4]), no14_solution(vec![4]))
    }
    #[test]
    fn test_no14_13() {
        assert_eq!(no14(vec![]), no14_solution(vec![]))
    }
    #[test]
    fn test_no14_14() {
        assert_eq!(no14(vec![1, 1, 1, 1]), no14_solution(vec![1, 1, 1, 1]))
    }
    #[test]
    fn test_no14_15() {
        assert_eq!(no14(vec![9, 4, 4, 1]), no14_solution(vec![9, 4, 4, 1]))
    }
    #[test]
    fn test_no14_16() {
        assert_eq!(no14(vec![4, 2, 3, 1]), no14_solution(vec![4, 2, 3, 1]))
    }
    #[test]
    fn test_no14_17() {
        assert_eq!(no14(vec![4, 2, 3, 5]), no14_solution(vec![4, 2, 3, 5]))
    }
    #[test]
    fn test_no14_18() {
        assert_eq!(no14(vec![4, 4, 2]), no14_solution(vec![4, 4, 2]))
    }
    #[test]
    fn test_no14_19() {
        assert_eq!(no14(vec![1, 4, 4]), no14_solution(vec![1, 4, 4]))
    }
}

#[cfg(test)]
mod test_is_everywhere {
    use super::implementations::is_everywhere;
    use super::solutions::is_everywhere_solution;

    #[test]
    fn test_is_everywhere_1() {
        assert_eq!(is_everywhere(vec![1, 2, 1, 3], 1), is_everywhere_solution(vec![1, 2, 1, 3], 1))
    }
    #[test]
    fn test_is_everywhere_2() {
        assert_eq!(is_everywhere(vec![1, 2, 1, 3], 2), is_everywhere_solution(vec![1, 2, 1, 3], 2))
    }
    #[test]
    fn test_is_everywhere_3() {
        assert_eq!(is_everywhere(vec![1, 2, 1, 3, 4], 1), is_everywhere_solution(vec![1, 2, 1, 3, 4], 1))
    }
    #[test]
    fn test_is_everywhere_4() {
        assert_eq!(is_everywhere(vec![2, 1, 2, 1], 1), is_everywhere_solution(vec![2, 1, 2, 1], 1))
    }
    #[test]
    fn test_is_everywhere_5() {
        assert_eq!(is_everywhere(vec![2, 1, 2, 1], 2), is_everywhere_solution(vec![2, 1, 2, 1], 2))
    }
    #[test]
    fn test_is_everywhere_6() {
        assert_eq!(is_everywhere(vec![2, 1, 2, 3, 1], 2), is_everywhere_solution(vec![2, 1, 2, 3, 1], 2))
    }
    #[test]
    fn test_is_everywhere_7() {
        assert_eq!(is_everywhere(vec![3, 1], 3), is_everywhere_solution(vec![3, 1], 3))
    }
    #[test]
    fn test_is_everywhere_8() {
        assert_eq!(is_everywhere(vec![3, 1], 2), is_everywhere_solution(vec![3, 1], 2))
    }
    #[test]
    fn test_is_everywhere_9() {
        assert_eq!(is_everywhere(vec![3], 1), is_everywhere_solution(vec![3], 1))
    }
    #[test]
    fn test_is_everywhere_10() {
        assert_eq!(is_everywhere(vec![], 1), is_everywhere_solution(vec![], 1))
    }
    #[test]
    fn test_is_everywhere_11() {
        assert_eq!(is_everywhere(vec![1, 2, 1, 2, 3, 2, 5], 2), is_everywhere_solution(vec![1, 2, 1, 2, 3, 2, 5], 2))
    }
    #[test]
    fn test_is_everywhere_12() {
        assert_eq!(is_everywhere(vec![1, 2, 1, 1, 1, 2], 2), is_everywhere_solution(vec![1, 2, 1, 1, 1, 2], 2))
    }
    #[test]
    fn test_is_everywhere_13() {
        assert_eq!(is_everywhere(vec![2, 1, 2, 1, 1, 2], 2), is_everywhere_solution(vec![2, 1, 2, 1, 1, 2], 2))
    }
    #[test]
    fn test_is_everywhere_14() {
        assert_eq!(is_everywhere(vec![2, 1, 2, 2, 2, 1, 1, 2], 2), is_everywhere_solution(vec![2, 1, 2, 2, 2, 1, 1, 2], 2))
    }
    #[test]
    fn test_is_everywhere_15() {
        assert_eq!(is_everywhere(vec![2, 1, 2, 2, 2, 1, 2, 1], 2), is_everywhere_solution(vec![2, 1, 2, 2, 2, 1, 2, 1], 2))
    }
    #[test]
    fn test_is_everywhere_16() {
        assert_eq!(is_everywhere(vec![2, 1, 2, 1, 2], 2), is_everywhere_solution(vec![2, 1, 2, 1, 2], 2))
    }
}

#[cfg(test)]
mod test_either24 {
    use super::implementations::either24;
    use super::solutions::either24_solution;

    #[test]
    fn test_either24_1() {
        assert_eq!(either24(vec![1, 2, 2]), either24_solution(vec![1, 2, 2]))
    }
    #[test]
    fn test_either24_2() {
        assert_eq!(either24(vec![4, 4, 1]), either24_solution(vec![4, 4, 1]))
    }
    #[test]
    fn test_either24_3() {
        assert_eq!(either24(vec![4, 4, 1, 2, 2]), either24_solution(vec![4, 4, 1, 2, 2]))
    }
    #[test]
    fn test_either24_4() {
        assert_eq!(either24(vec![1, 2, 3, 4]), either24_solution(vec![1, 2, 3, 4]))
    }
    #[test]
    fn test_either24_5() {
        assert_eq!(either24(vec![3, 5, 9]), either24_solution(vec![3, 5, 9]))
    }
    #[test]
    fn test_either24_6() {
        assert_eq!(either24(vec![1, 2, 3, 4, 4]), either24_solution(vec![1, 2, 3, 4, 4]))
    }
    #[test]
    fn test_either24_7() {
        assert_eq!(either24(vec![2, 2, 3, 4]), either24_solution(vec![2, 2, 3, 4]))
    }
    #[test]
    fn test_either24_8() {
        assert_eq!(either24(vec![1, 2, 3, 2, 2, 4]), either24_solution(vec![1, 2, 3, 2, 2, 4]))
    }
    #[test]
    fn test_either24_9() {
        assert_eq!(either24(vec![1, 2, 3, 2, 2, 4, 4]), either24_solution(vec![1, 2, 3, 2, 2, 4, 4]))
    }
    #[test]
    fn test_either24_10() {
        assert_eq!(either24(vec![1, 2]), either24_solution(vec![1, 2]))
    }
    #[test]
    fn test_either24_11() {
        assert_eq!(either24(vec![2, 2]), either24_solution(vec![2, 2]))
    }
    #[test]
    fn test_either24_12() {
        assert_eq!(either24(vec![4, 4]), either24_solution(vec![4, 4]))
    }
    #[test]
    fn test_either24_13() {
        assert_eq!(either24(vec![2]), either24_solution(vec![2]))
    }
    #[test]
    fn test_either24_14() {
        assert_eq!(either24(vec![]), either24_solution(vec![]))
    }
}

#[cfg(test)]
mod test_match_up {
    use super::implementations::match_up;
    use super::solutions::match_up_solution;

    #[test]
    fn test_match_up_1() {
        assert_eq!(match_up(vec![1, 2, 3], vec![2, 3, 10]), match_up_solution(vec![1, 2, 3], vec![2, 3, 10]))
    }
    #[test]
    fn test_match_up_2() {
        assert_eq!(match_up(vec![1, 2, 3], vec![2, 3, 5]), match_up_solution(vec![1, 2, 3], vec![2, 3, 5]))
    }
    #[test]
    fn test_match_up_3() {
        assert_eq!(match_up(vec![1, 2, 3], vec![2, 3, 3]), match_up_solution(vec![1, 2, 3], vec![2, 3, 3]))
    }
    #[test]
    fn test_match_up_4() {
        assert_eq!(match_up(vec![5, 3], vec![5, 5]), match_up_solution(vec![5, 3], vec![5, 5]))
    }
    #[test]
    fn test_match_up_5() {
        assert_eq!(match_up(vec![5, 3], vec![4, 4]), match_up_solution(vec![5, 3], vec![4, 4]))
    }
    #[test]
    fn test_match_up_6() {
        assert_eq!(match_up(vec![5, 3], vec![3, 3]), match_up_solution(vec![5, 3], vec![3, 3]))
    }
    #[test]
    fn test_match_up_7() {
        assert_eq!(match_up(vec![5, 3], vec![2, 2]), match_up_solution(vec![5, 3], vec![2, 2]))
    }
    #[test]
    fn test_match_up_8() {
        assert_eq!(match_up(vec![5, 3], vec![1, 1]), match_up_solution(vec![5, 3], vec![1, 1]))
    }
    #[test]
    fn test_match_up_9() {
        assert_eq!(match_up(vec![5, 3], vec![0, 0]), match_up_solution(vec![5, 3], vec![0, 0]))
    }
    #[test]
    fn test_match_up_10() {
        assert_eq!(match_up(vec![4], vec![4]), match_up_solution(vec![4], vec![4]))
    }
    #[test]
    fn test_match_up_11() {
        assert_eq!(match_up(vec![4], vec![5]), match_up_solution(vec![4], vec![5]))
    }
}

#[cfg(test)]
mod test_has77 {
    use super::implementations::has77;
    use super::solutions::has77_solution;

    #[test]
    fn test_has77_1() {
        assert_eq!(has77(vec![1, 7, 7]), has77_solution(vec![1, 7, 7]))
    }
    #[test]
    fn test_has77_2() {
        assert_eq!(has77(vec![1, 7, 1, 7]), has77_solution(vec![1, 7, 1, 7]))
    }
    #[test]
    fn test_has77_3() {
        assert_eq!(has77(vec![1, 7, 1, 1, 7]), has77_solution(vec![1, 7, 1, 1, 7]))
    }
    #[test]
    fn test_has77_4() {
        assert_eq!(has77(vec![7, 7, 1, 1, 7]), has77_solution(vec![7, 7, 1, 1, 7]))
    }
    #[test]
    fn test_has77_5() {
        assert_eq!(has77(vec![2, 7, 2, 2, 7, 2]), has77_solution(vec![2, 7, 2, 2, 7, 2]))
    }
    #[test]
    fn test_has77_6() {
        assert_eq!(has77(vec![2, 7, 2, 2, 7, 7]), has77_solution(vec![2, 7, 2, 2, 7, 7]))
    }
    #[test]
    fn test_has77_7() {
        assert_eq!(has77(vec![7, 2, 7, 2, 2, 7]), has77_solution(vec![7, 2, 7, 2, 2, 7]))
    }
    #[test]
    fn test_has77_8() {
        assert_eq!(has77(vec![7, 2, 6, 2, 2, 7]), has77_solution(vec![7, 2, 6, 2, 2, 7]))
    }
    #[test]
    fn test_has77_9() {
        assert_eq!(has77(vec![7, 7, 7]), has77_solution(vec![7, 7, 7]))
    }
    #[test]
    fn test_has77_10() {
        assert_eq!(has77(vec![7, 1, 7]), has77_solution(vec![7, 1, 7]))
    }
    #[test]
    fn test_has77_11() {
        assert_eq!(has77(vec![7, 1, 1]), has77_solution(vec![7, 1, 1]))
    }
    #[test]
    fn test_has77_12() {
        assert_eq!(has77(vec![1, 2]), has77_solution(vec![1, 2]))
    }
    #[test]
    fn test_has77_13() {
        assert_eq!(has77(vec![1, 7]), has77_solution(vec![1, 7]))
    }
    #[test]
    fn test_has77_14() {
        assert_eq!(has77(vec![7]), has77_solution(vec![7]))
    }
}

#[cfg(test)]
mod test_has12 {
    use super::implementations::has12;
    use super::solutions::has12_solution;

    #[test]
    fn test_has12_1() {
        assert_eq!(has12(vec![1, 3, 2]), has12_solution(vec![1, 3, 2]))
    }
    #[test]
    fn test_has12_2() {
        assert_eq!(has12(vec![3, 1, 2]), has12_solution(vec![3, 1, 2]))
    }
    #[test]
    fn test_has12_3() {
        assert_eq!(has12(vec![3, 1, 4, 5, 2]), has12_solution(vec![3, 1, 4, 5, 2]))
    }
    #[test]
    fn test_has12_4() {
        assert_eq!(has12(vec![3, 1, 4, 5, 6]), has12_solution(vec![3, 1, 4, 5, 6]))
    }
    #[test]
    fn test_has12_5() {
        assert_eq!(has12(vec![3, 1, 4, 1, 6, 2]), has12_solution(vec![3, 1, 4, 1, 6, 2]))
    }
    #[test]
    fn test_has12_6() {
        assert_eq!(has12(vec![2, 1, 4, 1, 6, 2]), has12_solution(vec![2, 1, 4, 1, 6, 2]))
    }
    #[test]
    fn test_has12_7() {
        assert_eq!(has12(vec![2, 1, 4, 1, 6]), has12_solution(vec![2, 1, 4, 1, 6]))
    }
    #[test]
    fn test_has12_8() {
        assert_eq!(has12(vec![1]), has12_solution(vec![1]))
    }
    #[test]
    fn test_has12_9() {
        assert_eq!(has12(vec![2, 1, 3]), has12_solution(vec![2, 1, 3]))
    }
    #[test]
    fn test_has12_10() {
        assert_eq!(has12(vec![2, 1, 3, 2]), has12_solution(vec![2, 1, 3, 2]))
    }
    #[test]
    fn test_has12_11() {
        assert_eq!(has12(vec![2]), has12_solution(vec![2]))
    }
    #[test]
    fn test_has12_12() {
        assert_eq!(has12(vec![3, 2]), has12_solution(vec![3, 2]))
    }
    #[test]
    fn test_has12_13() {
        assert_eq!(has12(vec![3, 1, 3, 2]), has12_solution(vec![3, 1, 3, 2]))
    }
    #[test]
    fn test_has12_14() {
        assert_eq!(has12(vec![3, 5, 9]), has12_solution(vec![3, 5, 9]))
    }
    #[test]
    fn test_has12_15() {
        assert_eq!(has12(vec![3, 5, 1]), has12_solution(vec![3, 5, 1]))
    }
    #[test]
    fn test_has12_16() {
        assert_eq!(has12(vec![3, 2, 1]), has12_solution(vec![3, 2, 1]))
    }
    #[test]
    fn test_has12_17() {
        assert_eq!(has12(vec![1, 2]), has12_solution(vec![1, 2]))
    }
}

#[cfg(test)]
mod test_mod_three {
    use super::implementations::mod_three;
    use super::solutions::mod_three_solution;

    #[test]
    fn test_mod_three_1() {
        assert_eq!(mod_three(vec![2, 1, 3, 5]), mod_three_solution(vec![2, 1, 3, 5]))
    }
    #[test]
    fn test_mod_three_2() {
        assert_eq!(mod_three(vec![2, 1, 2, 5]), mod_three_solution(vec![2, 1, 2, 5]))
    }
    #[test]
    fn test_mod_three_3() {
        assert_eq!(mod_three(vec![2, 4, 2, 5]), mod_three_solution(vec![2, 4, 2, 5]))
    }
    #[test]
    fn test_mod_three_4() {
        assert_eq!(mod_three(vec![1, 2, 1, 2, 1]), mod_three_solution(vec![1, 2, 1, 2, 1]))
    }
    #[test]
    fn test_mod_three_5() {
        assert_eq!(mod_three(vec![9, 9, 9]), mod_three_solution(vec![9, 9, 9]))
    }
    #[test]
    fn test_mod_three_6() {
        assert_eq!(mod_three(vec![1, 2, 1]), mod_three_solution(vec![1, 2, 1]))
    }
    #[test]
    fn test_mod_three_7() {
        assert_eq!(mod_three(vec![1, 2]), mod_three_solution(vec![1, 2]))
    }
    #[test]
    fn test_mod_three_8() {
        assert_eq!(mod_three(vec![1]), mod_three_solution(vec![1]))
    }
    #[test]
    fn test_mod_three_9() {
        assert_eq!(mod_three(vec![]), mod_three_solution(vec![]))
    }
    #[test]
    fn test_mod_three_10() {
        assert_eq!(mod_three(vec![9, 7, 2, 9]), mod_three_solution(vec![9, 7, 2, 9]))
    }
    #[test]
    fn test_mod_three_11() {
        assert_eq!(mod_three(vec![9, 7, 2, 9, 2, 2]), mod_three_solution(vec![9, 7, 2, 9, 2, 2]))
    }
    #[test]
    fn test_mod_three_12() {
        assert_eq!(mod_three(vec![9, 7, 2, 9, 2, 2, 6]), mod_three_solution(vec![9, 7, 2, 9, 2, 2, 6]))
    }
}

#[cfg(test)]
mod test_find_the_median {
    use super::implementations::find_the_median;
    use super::solutions::find_the_median_solution;

    #[test]
    fn test_find_the_median_1() {
        assert_eq!(find_the_median(vec![4, 9, 9, 2, 1, 5]), find_the_median_solution(vec![4, 9, 9, 2, 1, 5]))
    }
    #[test]
    fn test_find_the_median_2() {
        assert_eq!(find_the_median(vec![1, 5, 3, 1, 5]), find_the_median_solution(vec![1, 5, 3, 1, 5]))
    }
    #[test]
    fn test_find_the_median_3() {
        assert_eq!(find_the_median(vec![10, 12, 15]), find_the_median_solution(vec![10, 12, 15]))
    }
    #[test]
    fn test_find_the_median_4() {
        assert_eq!(find_the_median(vec![5]), find_the_median_solution(vec![5]))
    }
    #[test]
    fn test_find_the_median_5() {
        assert_eq!(find_the_median(vec![11, 9, 0, 1]), find_the_median_solution(vec![11, 9, 0, 1]))
    }
    #[test]
    fn test_find_the_median_6() {
        assert_eq!(find_the_median(vec![-1, 11, -2, 10, -3, 15]), find_the_median_solution(vec![-1, 11, -2, 10, -3, 15]))
    }
    #[test]
    fn test_find_the_median_7() {
        assert_eq!(find_the_median(vec![2, 10, 15, 13]), find_the_median_solution(vec![2, 10, 15, 13]))
    }
    #[test]
    fn test_find_the_median_8() {
        assert_eq!(find_the_median(vec![2, 5, -12]), find_the_median_solution(vec![2, 5, -12]))
    }
}

#[cfg(test)]
mod test_have_three {
    use super::implementations::have_three;
    use super::solutions::have_three_solution;

    #[test]
    fn test_have_three_1() {
        assert_eq!(have_three(vec![3, 1, 3, 1, 3]), have_three_solution(vec![3, 1, 3, 1, 3]))
    }
    #[test]
    fn test_have_three_2() {
        assert_eq!(have_three(vec![3, 1, 3, 3]), have_three_solution(vec![3, 1, 3, 3]))
    }
    #[test]
    fn test_have_three_3() {
        assert_eq!(have_three(vec![3, 4, 3, 3, 4]), have_three_solution(vec![3, 4, 3, 3, 4]))
    }
    #[test]
    fn test_have_three_4() {
        assert_eq!(have_three(vec![1, 3, 1, 3, 1, 2]), have_three_solution(vec![1, 3, 1, 3, 1, 2]))
    }
    #[test]
    fn test_have_three_5() {
        assert_eq!(have_three(vec![1, 3, 1, 3, 1, 3]), have_three_solution(vec![1, 3, 1, 3, 1, 3]))
    }
    #[test]
    fn test_have_three_6() {
        assert_eq!(have_three(vec![1, 3, 3, 1, 3]), have_three_solution(vec![1, 3, 3, 1, 3]))
    }
    #[test]
    fn test_have_three_7() {
        assert_eq!(have_three(vec![1, 3, 1, 3, 1, 3, 4, 3]), have_three_solution(vec![1, 3, 1, 3, 1, 3, 4, 3]))
    }
    #[test]
    fn test_have_three_8() {
        assert_eq!(have_three(vec![3, 4, 3, 4, 3, 4, 4]), have_three_solution(vec![3, 4, 3, 4, 3, 4, 4]))
    }
    #[test]
    fn test_have_three_9() {
        assert_eq!(have_three(vec![3, 3, 3]), have_three_solution(vec![3, 3, 3]))
    }
    #[test]
    fn test_have_three_10() {
        assert_eq!(have_three(vec![1, 3]), have_three_solution(vec![1, 3]))
    }
    #[test]
    fn test_have_three_11() {
        assert_eq!(have_three(vec![3]), have_three_solution(vec![3]))
    }
    #[test]
    fn test_have_three_12() {
        assert_eq!(have_three(vec![1]), have_three_solution(vec![1]))
    }
}

#[cfg(test)]
mod test_two_two {
    use super::implementations::two_two;
    use super::solutions::two_two_solution;

    #[test]
    fn test_two_two_1() {
        assert_eq!(two_two(vec![4, 2, 2, 3]), two_two_solution(vec![4, 2, 2, 3]))
    }
    #[test]
    fn test_two_two_2() {
        assert_eq!(two_two(vec![2, 2, 4]), two_two_solution(vec![2, 2, 4]))
    }
    #[test]
    fn test_two_two_3() {
        assert_eq!(two_two(vec![2, 2, 4, 2]), two_two_solution(vec![2, 2, 4, 2]))
    }
    #[test]
    fn test_two_two_4() {
        assert_eq!(two_two(vec![1, 3, 4]), two_two_solution(vec![1, 3, 4]))
    }
    #[test]
    fn test_two_two_5() {
        assert_eq!(two_two(vec![1, 2, 2, 3, 4]), two_two_solution(vec![1, 2, 2, 3, 4]))
    }
    #[test]
    fn test_two_two_6() {
        assert_eq!(two_two(vec![1, 2, 3, 4]), two_two_solution(vec![1, 2, 3, 4]))
    }
    #[test]
    fn test_two_two_7() {
        assert_eq!(two_two(vec![2, 2]), two_two_solution(vec![2, 2]))
    }
    #[test]
    fn test_two_two_8() {
        assert_eq!(two_two(vec![2, 2, 7]), two_two_solution(vec![2, 2, 7]))
    }
    #[test]
    fn test_two_two_9() {
        assert_eq!(two_two(vec![2, 2, 7, 2, 1]), two_two_solution(vec![2, 2, 7, 2, 1]))
    }
    #[test]
    fn test_two_two_10() {
        assert_eq!(two_two(vec![4, 2, 2, 2]), two_two_solution(vec![4, 2, 2, 2]))
    }
    #[test]
    fn test_two_two_11() {
        assert_eq!(two_two(vec![2, 2, 2]), two_two_solution(vec![2, 2, 2]))
    }
    #[test]
    fn test_two_two_12() {
        assert_eq!(two_two(vec![1, 2]), two_two_solution(vec![1, 2]))
    }
    #[test]
    fn test_two_two_13() {
        assert_eq!(two_two(vec![2]), two_two_solution(vec![2]))
    }
    #[test]
    fn test_two_two_14() {
        assert_eq!(two_two(vec![1]), two_two_solution(vec![1]))
    }
    #[test]
    fn test_two_two_15() {
        assert_eq!(two_two(vec![]), two_two_solution(vec![]))
    }
    #[test]
    fn test_two_two_16() {
        assert_eq!(two_two(vec![5, 2, 2, 3]), two_two_solution(vec![5, 2, 2, 3]))
    }
    #[test]
    fn test_two_two_17() {
        assert_eq!(two_two(vec![2, 2, 5, 2]), two_two_solution(vec![2, 2, 5, 2]))
    }
}

#[cfg(test)]
mod test_same_ends {
    use super::implementations::same_ends;
    use super::solutions::same_ends_solution;

    #[test]
    fn test_same_ends_1() {
        assert_eq!(same_ends(vec![5, 6, 45, 99, 13, 5, 6], 1), same_ends_solution(vec![5, 6, 45, 99, 13, 5, 6], 1))
    }
    #[test]
    fn test_same_ends_2() {
        assert_eq!(same_ends(vec![5, 6, 45, 99, 13, 5, 6], 2), same_ends_solution(vec![5, 6, 45, 99, 13, 5, 6], 2))
    }
    #[test]
    fn test_same_ends_3() {
        assert_eq!(same_ends(vec![5, 6, 45, 99, 13, 5, 6], 3), same_ends_solution(vec![5, 6, 45, 99, 13, 5, 6], 3))
    }
    #[test]
    fn test_same_ends_4() {
        assert_eq!(same_ends(vec![1, 2, 5, 2, 1], 1), same_ends_solution(vec![1, 2, 5, 2, 1], 1))
    }
    #[test]
    fn test_same_ends_5() {
        assert_eq!(same_ends(vec![1, 2, 5, 2, 1], 2), same_ends_solution(vec![1, 2, 5, 2, 1], 2))
    }
    #[test]
    fn test_same_ends_6() {
        assert_eq!(same_ends(vec![1, 2, 5, 2, 1], 0), same_ends_solution(vec![1, 2, 5, 2, 1], 0))
    }
    #[test]
    fn test_same_ends_7() {
        assert_eq!(same_ends(vec![1, 2, 5, 2, 1], 5), same_ends_solution(vec![1, 2, 5, 2, 1], 5))
    }
    #[test]
    fn test_same_ends_8() {
        assert_eq!(same_ends(vec![1, 1, 1], 0), same_ends_solution(vec![1, 1, 1], 0))
    }
    #[test]
    fn test_same_ends_9() {
        assert_eq!(same_ends(vec![1, 1, 1], 1), same_ends_solution(vec![1, 1, 1], 1))
    }
    #[test]
    fn test_same_ends_10() {
        assert_eq!(same_ends(vec![1, 1, 1], 2), same_ends_solution(vec![1, 1, 1], 2))
    }
    #[test]
    fn test_same_ends_11() {
        assert_eq!(same_ends(vec![1, 1, 1], 3), same_ends_solution(vec![1, 1, 1], 3))
    }
    #[test]
    fn test_same_ends_12() {
        assert_eq!(same_ends(vec![1], 1), same_ends_solution(vec![1], 1))
    }
    #[test]
    fn test_same_ends_13() {
        assert_eq!(same_ends(vec![], 0), same_ends_solution(vec![], 0))
    }
    #[test]
    fn test_same_ends_14() {
        assert_eq!(same_ends(vec![4, 2, 4, 5], 1), same_ends_solution(vec![4, 2, 4, 5], 1))
    }
}

#[cfg(test)]
mod test_triple_up {
    use super::implementations::triple_up;
    use super::solutions::triple_up_solution;

    #[test]
    fn test_triple_up_1() {
        assert_eq!(triple_up(vec![1, 4, 5, 6, 2]), triple_up_solution(vec![1, 4, 5, 6, 2]))
    }
    #[test]
    fn test_triple_up_2() {
        assert_eq!(triple_up(vec![1, 2, 3]), triple_up_solution(vec![1, 2, 3]))
    }
    #[test]
    fn test_triple_up_3() {
        assert_eq!(triple_up(vec![1, 2, 4]), triple_up_solution(vec![1, 2, 4]))
    }
    #[test]
    fn test_triple_up_4() {
        assert_eq!(triple_up(vec![1, 2, 4, 5, 7, 6, 5, 6, 7, 6]), triple_up_solution(vec![1, 2, 4, 5, 7, 6, 5, 6, 7, 6]))
    }
    #[test]
    fn test_triple_up_5() {
        assert_eq!(triple_up(vec![1, 2, 4, 5, 7, 6, 5, 7, 7, 6]), triple_up_solution(vec![1, 2, 4, 5, 7, 6, 5, 7, 7, 6]))
    }
    #[test]
    fn test_triple_up_6() {
        assert_eq!(triple_up(vec![1, 2]), triple_up_solution(vec![1, 2]))
    }
    #[test]
    fn test_triple_up_7() {
        assert_eq!(triple_up(vec![1]), triple_up_solution(vec![1]))
    }
    #[test]
    fn test_triple_up_8() {
        assert_eq!(triple_up(vec![]), triple_up_solution(vec![]))
    }
    #[test]
    fn test_triple_up_9() {
        assert_eq!(triple_up(vec![10, 9, 8, -100, -99, -98, 100]), triple_up_solution(vec![10, 9, 8, -100, -99, -98, 100]))
    }
    #[test]
    fn test_triple_up_10() {
        assert_eq!(triple_up(vec![10, 9, 8, -100, -99, 99, 100]), triple_up_solution(vec![10, 9, 8, -100, -99, 99, 100]))
    }
    #[test]
    fn test_triple_up_11() {
        assert_eq!(triple_up(vec![-100, -99, -99, 100, 101, 102]), triple_up_solution(vec![-100, -99, -99, 100, 101, 102]))
    }
    #[test]
    fn test_triple_up_12() {
        assert_eq!(triple_up(vec![2, 3, 5, 6, 8, 9, 2, 3]), triple_up_solution(vec![2, 3, 5, 6, 8, 9, 2, 3]))
    }
}

#[cfg(test)]
mod test_fizz_array3 {
    use super::implementations::fizz_array3;
    use super::solutions::fizz_array3_solution;

    #[test]
    fn test_fizz_array3_1() {
        assert_eq!(fizz_array3(5, 10), fizz_array3_solution(5, 10))
    }
    #[test]
    fn test_fizz_array3_2() {
        assert_eq!(fizz_array3(11, 18), fizz_array3_solution(11, 18))
    }
    #[test]
    fn test_fizz_array3_3() {
        assert_eq!(fizz_array3(1, 3), fizz_array3_solution(1, 3))
    }
    #[test]
    fn test_fizz_array3_4() {
        assert_eq!(fizz_array3(1, 2), fizz_array3_solution(1, 2))
    }
    #[test]
    fn test_fizz_array3_5() {
        assert_eq!(fizz_array3(1, 1), fizz_array3_solution(1, 1))
    }
    #[test]
    fn test_fizz_array3_6() {
        assert_eq!(fizz_array3(1000, 1005), fizz_array3_solution(1000, 1005))
    }
}

#[cfg(test)]
mod test_shift_left {
    use super::implementations::shift_left;
    use super::solutions::shift_left_solution;

    #[test]
    fn test_shift_left_1() {
        assert_eq!(shift_left(vec![6, 2, 5, 3]), shift_left_solution(vec![6, 2, 5, 3]))
    }
    #[test]
    fn test_shift_left_2() {
        assert_eq!(shift_left(vec![1, 2]), shift_left_solution(vec![1, 2]))
    }
    #[test]
    fn test_shift_left_3() {
        assert_eq!(shift_left(vec![1]), shift_left_solution(vec![1]))
    }
    #[test]
    fn test_shift_left_4() {
        assert_eq!(shift_left(vec![]), shift_left_solution(vec![]))
    }
    #[test]
    fn test_shift_left_5() {
        assert_eq!(shift_left(vec![1, 1, 2, 2, 4]), shift_left_solution(vec![1, 1, 2, 2, 4]))
    }
    #[test]
    fn test_shift_left_6() {
        assert_eq!(shift_left(vec![1, 1, 1]), shift_left_solution(vec![1, 1, 1]))
    }
    #[test]
    fn test_shift_left_7() {
        assert_eq!(shift_left(vec![1, 2, 3]), shift_left_solution(vec![1, 2, 3]))
    }
}

#[cfg(test)]
mod test_ten_run {
    use super::implementations::ten_run;
    use super::solutions::ten_run_solution;

    #[test]
    fn test_ten_run_1() {
        assert_eq!(ten_run(vec![2, 10, 3, 4, 20, 5]), ten_run_solution(vec![2, 10, 3, 4, 20, 5]))
    }
    #[test]
    fn test_ten_run_2() {
        assert_eq!(ten_run(vec![10, 1, 20, 2]), ten_run_solution(vec![10, 1, 20, 2]))
    }
    #[test]
    fn test_ten_run_3() {
        assert_eq!(ten_run(vec![10, 1, 9, 20]), ten_run_solution(vec![10, 1, 9, 20]))
    }
    #[test]
    fn test_ten_run_4() {
        assert_eq!(ten_run(vec![1, 2, 50, 1]), ten_run_solution(vec![1, 2, 50, 1]))
    }
    #[test]
    fn test_ten_run_5() {
        assert_eq!(ten_run(vec![1, 20, 50, 1]), ten_run_solution(vec![1, 20, 50, 1]))
    }
    #[test]
    fn test_ten_run_6() {
        assert_eq!(ten_run(vec![10, 10]), ten_run_solution(vec![10, 10]))
    }
    #[test]
    fn test_ten_run_7() {
        assert_eq!(ten_run(vec![10, 2]), ten_run_solution(vec![10, 2]))
    }
    #[test]
    fn test_ten_run_8() {
        assert_eq!(ten_run(vec![0, 2]), ten_run_solution(vec![0, 2]))
    }
    #[test]
    fn test_ten_run_9() {
        assert_eq!(ten_run(vec![1, 2]), ten_run_solution(vec![1, 2]))
    }
    #[test]
    fn test_ten_run_10() {
        assert_eq!(ten_run(vec![1]), ten_run_solution(vec![1]))
    }
    #[test]
    fn test_ten_run_11() {
        assert_eq!(ten_run(vec![]), ten_run_solution(vec![]))
    }
}

#[cfg(test)]
mod test_pre4 {
    use super::implementations::pre4;
    use super::solutions::pre4_solution;

    #[test]
    fn test_pre4_1() {
        assert_eq!(pre4(vec![1, 2, 4, 1]), pre4_solution(vec![1, 2, 4, 1]))
    }
    #[test]
    fn test_pre4_2() {
        assert_eq!(pre4(vec![3, 1, 4]), pre4_solution(vec![3, 1, 4]))
    }
    #[test]
    fn test_pre4_3() {
        assert_eq!(pre4(vec![1, 4, 4]), pre4_solution(vec![1, 4, 4]))
    }
    #[test]
    fn test_pre4_4() {
        assert_eq!(pre4(vec![1, 4, 4, 2]), pre4_solution(vec![1, 4, 4, 2]))
    }
    #[test]
    fn test_pre4_5() {
        assert_eq!(pre4(vec![1, 3, 4, 2, 4]), pre4_solution(vec![1, 3, 4, 2, 4]))
    }
    #[test]
    fn test_pre4_6() {
        assert_eq!(pre4(vec![4, 4]), pre4_solution(vec![4, 4]))
    }
    #[test]
    fn test_pre4_7() {
        assert_eq!(pre4(vec![3, 3, 4]), pre4_solution(vec![3, 3, 4]))
    }
    #[test]
    fn test_pre4_8() {
        assert_eq!(pre4(vec![1, 2, 1, 4]), pre4_solution(vec![1, 2, 1, 4]))
    }
    #[test]
    fn test_pre4_9() {
        assert_eq!(pre4(vec![2, 1, 4, 2]), pre4_solution(vec![2, 1, 4, 2]))
    }
    #[test]
    fn test_pre4_10() {
        assert_eq!(pre4(vec![2, 1, 2, 1, 4, 2]), pre4_solution(vec![2, 1, 2, 1, 4, 2]))
    }
}

#[cfg(test)]
mod test_post4 {
    use super::implementations::post4;
    use super::solutions::post4_solution;

    #[test]
    fn test_post4_1() {
        assert_eq!(post4(vec![2, 4, 1, 2]), post4_solution(vec![2, 4, 1, 2]))
    }
    #[test]
    fn test_post4_2() {
        assert_eq!(post4(vec![4, 1, 4, 2]), post4_solution(vec![4, 1, 4, 2]))
    }
    #[test]
    fn test_post4_3() {
        assert_eq!(post4(vec![4, 4, 1, 2, 3]), post4_solution(vec![4, 4, 1, 2, 3]))
    }
    #[test]
    fn test_post4_4() {
        assert_eq!(post4(vec![4, 2]), post4_solution(vec![4, 2]))
    }
    #[test]
    fn test_post4_5() {
        assert_eq!(post4(vec![4, 4, 3]), post4_solution(vec![4, 4, 3]))
    }
    #[test]
    fn test_post4_6() {
        assert_eq!(post4(vec![4, 4]), post4_solution(vec![4, 4]))
    }
    #[test]
    fn test_post4_7() {
        assert_eq!(post4(vec![4]), post4_solution(vec![4]))
    }
    #[test]
    fn test_post4_8() {
        assert_eq!(post4(vec![2, 4, 1, 4, 3, 2]), post4_solution(vec![2, 4, 1, 4, 3, 2]))
    }
    #[test]
    fn test_post4_9() {
        assert_eq!(post4(vec![4, 1, 4, 2, 2, 2]), post4_solution(vec![4, 1, 4, 2, 2, 2]))
    }
    #[test]
    fn test_post4_10() {
        assert_eq!(post4(vec![3, 4, 3, 2]), post4_solution(vec![3, 4, 3, 2]))
    }
}

#[cfg(test)]
mod test_not_alone {
    use super::implementations::not_alone;
    use super::solutions::not_alone_solution;

    #[test]
    fn test_not_alone_1() {
        assert_eq!(not_alone(vec![1, 2, 3], 2), not_alone_solution(vec![1, 2, 3], 2))
    }
    #[test]
    fn test_not_alone_2() {
        assert_eq!(not_alone(vec![1, 2, 3, 2, 5, 2], 2), not_alone_solution(vec![1, 2, 3, 2, 5, 2], 2))
    }
    #[test]
    fn test_not_alone_3() {
        assert_eq!(not_alone(vec![3, 4], 3), not_alone_solution(vec![3, 4], 3))
    }
    #[test]
    fn test_not_alone_4() {
        assert_eq!(not_alone(vec![3, 3], 3), not_alone_solution(vec![3, 3], 3))
    }
    #[test]
    fn test_not_alone_5() {
        assert_eq!(not_alone(vec![1, 3, 1, 2], 1), not_alone_solution(vec![1, 3, 1, 2], 1))
    }
    #[test]
    fn test_not_alone_6() {
        assert_eq!(not_alone(vec![3], 3), not_alone_solution(vec![3], 3))
    }
    #[test]
    fn test_not_alone_7() {
        assert_eq!(not_alone(vec![], 3), not_alone_solution(vec![], 3))
    }
    #[test]
    fn test_not_alone_8() {
        assert_eq!(not_alone(vec![7, 1, 6], 1), not_alone_solution(vec![7, 1, 6], 1))
    }
    #[test]
    fn test_not_alone_9() {
        assert_eq!(not_alone(vec![1, 1, 1], 1), not_alone_solution(vec![1, 1, 1], 1))
    }
    #[test]
    fn test_not_alone_10() {
        assert_eq!(not_alone(vec![1, 1, 1, 2], 1), not_alone_solution(vec![1, 1, 1, 2], 1))
    }
}

#[cfg(test)]
mod test_zero_front {
    use super::implementations::zero_front;
    use super::solutions::zero_front_solution;

    #[test]
    fn test_zero_front_1() {
        assert_eq!(zero_front(vec![1, 0, 0, 1]), zero_front_solution(vec![1, 0, 0, 1]))
    }
    #[test]
    fn test_zero_front_2() {
        assert_eq!(zero_front(vec![0, 1, 1, 0, 1]), zero_front_solution(vec![0, 1, 1, 0, 1]))
    }
    #[test]
    fn test_zero_front_3() {
        assert_eq!(zero_front(vec![1, 0]), zero_front_solution(vec![1, 0]))
    }
    #[test]
    fn test_zero_front_4() {
        assert_eq!(zero_front(vec![0, 1]), zero_front_solution(vec![0, 1]))
    }
    #[test]
    fn test_zero_front_5() {
        assert_eq!(zero_front(vec![1, 1, 1, 0]), zero_front_solution(vec![1, 1, 1, 0]))
    }
    #[test]
    fn test_zero_front_6() {
        assert_eq!(zero_front(vec![2, 2, 2, 2]), zero_front_solution(vec![2, 2, 2, 2]))
    }
    #[test]
    fn test_zero_front_7() {
        assert_eq!(zero_front(vec![0, 0, 1, 0]), zero_front_solution(vec![0, 0, 1, 0]))
    }
    #[test]
    fn test_zero_front_8() {
        assert_eq!(zero_front(vec![-1, 0, 0, -1, 0]), zero_front_solution(vec![-1, 0, 0, -1, 0]))
    }
    #[test]
    fn test_zero_front_9() {
        assert_eq!(zero_front(vec![0, -3, 0, -3]), zero_front_solution(vec![0, -3, 0, -3]))
    }
    #[test]
    fn test_zero_front_10() {
        assert_eq!(zero_front(vec![]), zero_front_solution(vec![]))
    }
    #[test]
    fn test_zero_front_11() {
        assert_eq!(zero_front(vec![9, 9, 0, 9, 0, 9]), zero_front_solution(vec![9, 9, 0, 9, 0, 9]))
    }
}

#[cfg(test)]
mod test_without_ten {
    use super::implementations::without_ten;
    use super::solutions::without_ten_solution;

    #[test]
    fn test_without_ten_1() {
        assert_eq!(without_ten(vec![1, 10, 10, 2]), without_ten_solution(vec![1, 10, 10, 2]))
    }
    #[test]
    fn test_without_ten_2() {
        assert_eq!(without_ten(vec![10, 2, 10]), without_ten_solution(vec![10, 2, 10]))
    }
    #[test]
    fn test_without_ten_3() {
        assert_eq!(without_ten(vec![1, 99, 10]), without_ten_solution(vec![1, 99, 10]))
    }
    #[test]
    fn test_without_ten_4() {
        assert_eq!(without_ten(vec![10, 13, 10, 14]), without_ten_solution(vec![10, 13, 10, 14]))
    }
    #[test]
    fn test_without_ten_5() {
        assert_eq!(without_ten(vec![10, 13, 10, 14, 10]), without_ten_solution(vec![10, 13, 10, 14, 10]))
    }
    #[test]
    fn test_without_ten_6() {
        assert_eq!(without_ten(vec![10, 10, 3]), without_ten_solution(vec![10, 10, 3]))
    }
    #[test]
    fn test_without_ten_7() {
        assert_eq!(without_ten(vec![1]), without_ten_solution(vec![1]))
    }
    #[test]
    fn test_without_ten_8() {
        assert_eq!(without_ten(vec![13, 1]), without_ten_solution(vec![13, 1]))
    }
    #[test]
    fn test_without_ten_9() {
        assert_eq!(without_ten(vec![10]), without_ten_solution(vec![10]))
    }
    #[test]
    fn test_without_ten_10() {
        assert_eq!(without_ten(vec![]), without_ten_solution(vec![]))
    }
}

#[cfg(test)]
mod test_zero_max {
    use super::implementations::zero_max;
    use super::solutions::zero_max_solution;

    #[test]
    fn test_zero_max_1() {
        assert_eq!(zero_max(vec![0, 5, 0, 3]), zero_max_solution(vec![0, 5, 0, 3]))
    }
    #[test]
    fn test_zero_max_2() {
        assert_eq!(zero_max(vec![0, 4, 0, 3]), zero_max_solution(vec![0, 4, 0, 3]))
    }
    #[test]
    fn test_zero_max_3() {
        assert_eq!(zero_max(vec![0, 1, 0]), zero_max_solution(vec![0, 1, 0]))
    }
    #[test]
    fn test_zero_max_4() {
        assert_eq!(zero_max(vec![0, 1, 5]), zero_max_solution(vec![0, 1, 5]))
    }
    #[test]
    fn test_zero_max_5() {
        assert_eq!(zero_max(vec![0, 2, 0]), zero_max_solution(vec![0, 2, 0]))
    }
    #[test]
    fn test_zero_max_6() {
        assert_eq!(zero_max(vec![1]), zero_max_solution(vec![1]))
    }
    #[test]
    fn test_zero_max_7() {
        assert_eq!(zero_max(vec![0]), zero_max_solution(vec![0]))
    }
    #[test]
    fn test_zero_max_8() {
        assert_eq!(zero_max(vec![]), zero_max_solution(vec![]))
    }
    #[test]
    fn test_zero_max_9() {
        assert_eq!(zero_max(vec![7, 0, 4, 3, 0, 2]), zero_max_solution(vec![7, 0, 4, 3, 0, 2]))
    }
    #[test]
    fn test_zero_max_10() {
        assert_eq!(zero_max(vec![7, 0, 4, 3, 0, 1]), zero_max_solution(vec![7, 0, 4, 3, 0, 1]))
    }
    #[test]
    fn test_zero_max_11() {
        assert_eq!(zero_max(vec![7, 0, 4, 3, 0, 0]), zero_max_solution(vec![7, 0, 4, 3, 0, 0]))
    }
    #[test]
    fn test_zero_max_12() {
        assert_eq!(zero_max(vec![7, 0, 1, 0, 0, 7]), zero_max_solution(vec![7, 0, 1, 0, 0, 7]))
    }
}

#[cfg(test)]
mod test_even_odd {
    use super::implementations::even_odd;
    use super::solutions::even_odd_solution;

    #[test]
    fn test_even_odd_1() {
        assert_eq!(even_odd(vec![1, 0, 1, 0, 0, 1, 1]), even_odd_solution(vec![1, 0, 1, 0, 0, 1, 1]))
    }
    #[test]
    fn test_even_odd_2() {
        assert_eq!(even_odd(vec![3, 3, 2]), even_odd_solution(vec![3, 3, 2]))
    }
    #[test]
    fn test_even_odd_3() {
        assert_eq!(even_odd(vec![2, 2, 2]), even_odd_solution(vec![2, 2, 2]))
    }
    #[test]
    fn test_even_odd_4() {
        assert_eq!(even_odd(vec![3, 2, 2]), even_odd_solution(vec![3, 2, 2]))
    }
    #[test]
    fn test_even_odd_5() {
        assert_eq!(even_odd(vec![1, 1, 0, 1, 0]), even_odd_solution(vec![1, 1, 0, 1, 0]))
    }
    #[test]
    fn test_even_odd_6() {
        assert_eq!(even_odd(vec![1]), even_odd_solution(vec![1]))
    }
    #[test]
    fn test_even_odd_7() {
        assert_eq!(even_odd(vec![1, 2]), even_odd_solution(vec![1, 2]))
    }
    #[test]
    fn test_even_odd_8() {
        assert_eq!(even_odd(vec![2, 1]), even_odd_solution(vec![2, 1]))
    }
    #[test]
    fn test_even_odd_9() {
        assert_eq!(even_odd(vec![]), even_odd_solution(vec![]))
    }
}

#[cfg(test)]
mod test_fizz_buzz {
    use super::implementations::fizz_buzz;
    use super::solutions::fizz_buzz_solution;

    #[test]
    fn test_fizz_buzz_1() {
        assert_eq!(fizz_buzz(1, 6), fizz_buzz_solution(1, 6))
    }
    #[test]
    fn test_fizz_buzz_2() {
        assert_eq!(fizz_buzz(1, 8), fizz_buzz_solution(1, 8))
    }
    #[test]
    fn test_fizz_buzz_3() {
        assert_eq!(fizz_buzz(1, 11), fizz_buzz_solution(1, 11))
    }
    #[test]
    fn test_fizz_buzz_4() {
        assert_eq!(fizz_buzz(1, 16), fizz_buzz_solution(1, 16))
    }
    #[test]
    fn test_fizz_buzz_5() {
        assert_eq!(fizz_buzz(1, 4), fizz_buzz_solution(1, 4))
    }
    #[test]
    fn test_fizz_buzz_6() {
        assert_eq!(fizz_buzz(1, 2), fizz_buzz_solution(1, 2))
    }
    #[test]
    fn test_fizz_buzz_7() {
        assert_eq!(fizz_buzz(50, 56), fizz_buzz_solution(50, 56))
    }
    #[test]
    fn test_fizz_buzz_8() {
        assert_eq!(fizz_buzz(15, 17), fizz_buzz_solution(15, 17))
    }
    #[test]
    fn test_fizz_buzz_9() {
        assert_eq!(fizz_buzz(30, 36), fizz_buzz_solution(30, 36))
    }
    #[test]
    fn test_fizz_buzz_10() {
        assert_eq!(fizz_buzz(1000, 1006), fizz_buzz_solution(1000, 1006))
    }
    #[test]
    fn test_fizz_buzz_11() {
        assert_eq!(fizz_buzz(99, 102), fizz_buzz_solution(99, 102))
    }
    #[test]
    fn test_fizz_buzz_12() {
        assert_eq!(fizz_buzz(14, 20), fizz_buzz_solution(14, 20))
    }
}

#[cfg(test)]
mod test_max_span {
    use super::implementations::max_span;
    use super::solutions::max_span_solution;

    #[test]
    fn test_max_span_1() {
        assert_eq!(max_span(vec![1, 2, 1, 1, 3]), max_span_solution(vec![1, 2, 1, 1, 3]))
    }
    #[test]
    fn test_max_span_2() {
        assert_eq!(max_span(vec![1, 4, 2, 1, 4, 1, 4]), max_span_solution(vec![1, 4, 2, 1, 4, 1, 4]))
    }
    #[test]
    fn test_max_span_3() {
        assert_eq!(max_span(vec![1, 4, 2, 1, 4, 4, 4]), max_span_solution(vec![1, 4, 2, 1, 4, 4, 4]))
    }
    #[test]
    fn test_max_span_4() {
        assert_eq!(max_span(vec![3, 3, 3]), max_span_solution(vec![3, 3, 3]))
    }
    #[test]
    fn test_max_span_5() {
        assert_eq!(max_span(vec![3, 9, 3]), max_span_solution(vec![3, 9, 3]))
    }
    #[test]
    fn test_max_span_6() {
        assert_eq!(max_span(vec![3, 9, 9]), max_span_solution(vec![3, 9, 9]))
    }
    #[test]
    fn test_max_span_7() {
        assert_eq!(max_span(vec![3, 9]), max_span_solution(vec![3, 9]))
    }
    #[test]
    fn test_max_span_8() {
        assert_eq!(max_span(vec![3, 3]), max_span_solution(vec![3, 3]))
    }
    #[test]
    fn test_max_span_9() {
        assert_eq!(max_span(vec![]), max_span_solution(vec![]))
    }
    #[test]
    fn test_max_span_10() {
        assert_eq!(max_span(vec![1]), max_span_solution(vec![1]))
    }
}

#[cfg(test)]
mod test_fix34 {
    use super::implementations::fix34;
    use super::solutions::fix34_solution;

    #[test]
    fn test_fix34_1() {
        assert_eq!(fix34(vec![1, 3, 1, 4]), fix34_solution(vec![1, 3, 1, 4]))
    }
    #[test]
    fn test_fix34_2() {
        assert_eq!(fix34(vec![1, 3, 1, 4, 4, 3, 1]), fix34_solution(vec![1, 3, 1, 4, 4, 3, 1]))
    }
    #[test]
    fn test_fix34_3() {
        assert_eq!(fix34(vec![3, 2, 2, 4]), fix34_solution(vec![3, 2, 2, 4]))
    }
    #[test]
    fn test_fix34_4() {
        assert_eq!(fix34(vec![3, 2, 3, 2, 4, 4]), fix34_solution(vec![3, 2, 3, 2, 4, 4]))
    }
    #[test]
    fn test_fix34_5() {
        assert_eq!(fix34(vec![2, 3, 2, 3, 2, 4, 4]), fix34_solution(vec![2, 3, 2, 3, 2, 4, 4]))
    }
    #[test]
    fn test_fix34_6() {
        assert_eq!(fix34(vec![5, 3, 5, 4, 5, 4, 5, 4, 3, 5, 3, 5]), fix34_solution(vec![5, 3, 5, 4, 5, 4, 5, 4, 3, 5, 3, 5]))
    }
    #[test]
    fn test_fix34_7() {
        assert_eq!(fix34(vec![3, 1, 4]), fix34_solution(vec![3, 1, 4]))
    }
    #[test]
    fn test_fix34_8() {
        assert_eq!(fix34(vec![3, 4, 1]), fix34_solution(vec![3, 4, 1]))
    }
    #[test]
    fn test_fix34_9() {
        assert_eq!(fix34(vec![1, 1, 1]), fix34_solution(vec![1, 1, 1]))
    }
    #[test]
    fn test_fix34_10() {
        assert_eq!(fix34(vec![1]), fix34_solution(vec![1]))
    }
    #[test]
    fn test_fix34_11() {
        assert_eq!(fix34(vec![]), fix34_solution(vec![]))
    }
    #[test]
    fn test_fix34_12() {
        assert_eq!(fix34(vec![7, 3, 7, 7, 4]), fix34_solution(vec![7, 3, 7, 7, 4]))
    }
    #[test]
    fn test_fix34_13() {
        assert_eq!(fix34(vec![3, 1, 4, 3, 1, 4]), fix34_solution(vec![3, 1, 4, 3, 1, 4]))
    }
    #[test]
    fn test_fix34_14() {
        assert_eq!(fix34(vec![3, 1, 1, 3, 4, 4]), fix34_solution(vec![3, 1, 1, 3, 4, 4]))
    }
}

#[cfg(test)]
mod test_fix45 {
    use super::implementations::fix45;
    use super::solutions::fix45_solution;

    #[test]
    fn test_fix45_1() {
        assert_eq!(fix45(vec![5, 4, 9, 4, 9, 5]), fix45_solution(vec![5, 4, 9, 4, 9, 5]))
    }
    #[test]
    fn test_fix45_2() {
        assert_eq!(fix45(vec![1, 4, 1, 5]), fix45_solution(vec![1, 4, 1, 5]))
    }
    #[test]
    fn test_fix45_3() {
        assert_eq!(fix45(vec![1, 4, 1, 5, 5, 4, 1]), fix45_solution(vec![1, 4, 1, 5, 5, 4, 1]))
    }
    #[test]
    fn test_fix45_4() {
        assert_eq!(fix45(vec![4, 9, 4, 9, 5, 5, 4, 9, 5]), fix45_solution(vec![4, 9, 4, 9, 5, 5, 4, 9, 5]))
    }
    #[test]
    fn test_fix45_5() {
        assert_eq!(fix45(vec![5, 5, 4, 1, 4, 1]), fix45_solution(vec![5, 5, 4, 1, 4, 1]))
    }
    #[test]
    fn test_fix45_6() {
        assert_eq!(fix45(vec![4, 2, 2, 5]), fix45_solution(vec![4, 2, 2, 5]))
    }
    #[test]
    fn test_fix45_7() {
        assert_eq!(fix45(vec![4, 2, 4, 2, 5, 5]), fix45_solution(vec![4, 2, 4, 2, 5, 5]))
    }
    #[test]
    fn test_fix45_8() {
        assert_eq!(fix45(vec![4, 2, 4, 5, 5]), fix45_solution(vec![4, 2, 4, 5, 5]))
    }
    #[test]
    fn test_fix45_9() {
        assert_eq!(fix45(vec![1, 1, 1]), fix45_solution(vec![1, 1, 1]))
    }
    #[test]
    fn test_fix45_10() {
        assert_eq!(fix45(vec![4, 5]), fix45_solution(vec![4, 5]))
    }
    #[test]
    fn test_fix45_11() {
        assert_eq!(fix45(vec![5, 4, 1]), fix45_solution(vec![5, 4, 1]))
    }
    #[test]
    fn test_fix45_12() {
        assert_eq!(fix45(vec![]), fix45_solution(vec![]))
    }
    #[test]
    fn test_fix45_13() {
        assert_eq!(fix45(vec![5, 4, 5, 4, 1]), fix45_solution(vec![5, 4, 5, 4, 1]))
    }
    #[test]
    fn test_fix45_14() {
        assert_eq!(fix45(vec![4, 5, 4, 1, 5]), fix45_solution(vec![4, 5, 4, 1, 5]))
    }
    #[test]
    fn test_fix45_15() {
        assert_eq!(fix45(vec![3, 4, 5]), fix45_solution(vec![3, 4, 5]))
    }
    #[test]
    fn test_fix45_16() {
        assert_eq!(fix45(vec![4, 1, 5]), fix45_solution(vec![4, 1, 5]))
    }
    #[test]
    fn test_fix45_17() {
        assert_eq!(fix45(vec![5, 4, 1]), fix45_solution(vec![5, 4, 1]))
    }
    #[test]
    fn test_fix45_18() {
        assert_eq!(fix45(vec![2, 4, 2, 5]), fix45_solution(vec![2, 4, 2, 5]))
    }
}

#[cfg(test)]
mod test_can_balance {
    use super::implementations::can_balance;
    use super::solutions::can_balance_solution;

    #[test]
    fn test_can_balance_1() {
        assert_eq!(can_balance(vec![1, 1, 1, 2, 1]), can_balance_solution(vec![1, 1, 1, 2, 1]))
    }
    #[test]
    fn test_can_balance_2() {
        assert_eq!(can_balance(vec![2, 1, 1, 2, 1]), can_balance_solution(vec![2, 1, 1, 2, 1]))
    }
    #[test]
    fn test_can_balance_3() {
        assert_eq!(can_balance(vec![10, 10]), can_balance_solution(vec![10, 10]))
    }
    #[test]
    fn test_can_balance_4() {
        assert_eq!(can_balance(vec![10, 0, 1, -1, 10]), can_balance_solution(vec![10, 0, 1, -1, 10]))
    }
    #[test]
    fn test_can_balance_5() {
        assert_eq!(can_balance(vec![1, 1, 1, 1, 4]), can_balance_solution(vec![1, 1, 1, 1, 4]))
    }
    #[test]
    fn test_can_balance_6() {
        assert_eq!(can_balance(vec![2, 1, 1, 1, 4]), can_balance_solution(vec![2, 1, 1, 1, 4]))
    }
    #[test]
    fn test_can_balance_7() {
        assert_eq!(can_balance(vec![2, 3, 4, 1, 2]), can_balance_solution(vec![2, 3, 4, 1, 2]))
    }
    #[test]
    fn test_can_balance_8() {
        assert_eq!(can_balance(vec![1, 2, 3, 1, 0, 2, 3]), can_balance_solution(vec![1, 2, 3, 1, 0, 2, 3]))
    }
    #[test]
    fn test_can_balance_9() {
        assert_eq!(can_balance(vec![1, 2, 3, 1, 0, 1, 3]), can_balance_solution(vec![1, 2, 3, 1, 0, 1, 3]))
    }
    #[test]
    fn test_can_balance_10() {
        assert_eq!(can_balance(vec![1]), can_balance_solution(vec![1]))
    }
    #[test]
    fn test_can_balance_11() {
        assert_eq!(can_balance(vec![1, 1, 1, 2, 1]), can_balance_solution(vec![1, 1, 1, 2, 1]))
    }
}

#[cfg(test)]
mod test_linear_in {
    use super::implementations::linear_in;
    use super::solutions::linear_in_solution;

    #[test]
    fn test_linear_in_1() {
        assert_eq!(linear_in(vec![1, 2, 4, 6], vec![2, 4]), linear_in_solution(vec![1, 2, 4, 6], vec![2, 4]))
    }
    #[test]
    fn test_linear_in_2() {
        assert_eq!(linear_in(vec![1, 2, 4, 6], vec![2, 3, 4]), linear_in_solution(vec![1, 2, 4, 6], vec![2, 3, 4]))
    }
    #[test]
    fn test_linear_in_3() {
        assert_eq!(linear_in(vec![1, 2, 4, 4, 6], vec![2, 4]), linear_in_solution(vec![1, 2, 4, 4, 6], vec![2, 4]))
    }
    #[test]
    fn test_linear_in_4() {
        assert_eq!(linear_in(vec![2, 2, 4, 4, 6, 6], vec![2, 4]), linear_in_solution(vec![2, 2, 4, 4, 6, 6], vec![2, 4]))
    }
    #[test]
    fn test_linear_in_5() {
        assert_eq!(linear_in(vec![2, 2, 2, 2, 2], vec![2, 2]), linear_in_solution(vec![2, 2, 2, 2, 2], vec![2, 2]))
    }
    #[test]
    fn test_linear_in_6() {
        assert_eq!(linear_in(vec![2, 2, 2, 2, 2], vec![2, 4]), linear_in_solution(vec![2, 2, 2, 2, 2], vec![2, 4]))
    }
    #[test]
    fn test_linear_in_7() {
        assert_eq!(linear_in(vec![2, 2, 2, 2, 4], vec![2, 4]), linear_in_solution(vec![2, 2, 2, 2, 4], vec![2, 4]))
    }
    #[test]
    fn test_linear_in_8() {
        assert_eq!(linear_in(vec![1, 2, 3], vec![2]), linear_in_solution(vec![1, 2, 3], vec![2]))
    }
    #[test]
    fn test_linear_in_9() {
        assert_eq!(linear_in(vec![1, 2, 3], vec![-1]), linear_in_solution(vec![1, 2, 3], vec![-1]))
    }
    #[test]
    fn test_linear_in_10() {
        assert_eq!(linear_in(vec![1, 2, 3], vec![]), linear_in_solution(vec![1, 2, 3], vec![]))
    }
    #[test]
    fn test_linear_in_11() {
        assert_eq!(
            linear_in(vec![-1, 0, 3, 3, 3, 10, 12], vec![-1, 0, 3, 12]),
            linear_in_solution(vec![-1, 0, 3, 3, 3, 10, 12], vec![-1, 0, 3, 12])
        )
    }
    #[test]
    fn test_linear_in_12() {
        assert_eq!(
            linear_in(vec![-1, 0, 3, 3, 3, 10, 12], vec![0, 3, 12, 14]),
            linear_in_solution(vec![-1, 0, 3, 3, 3, 10, 12], vec![0, 3, 12, 14])
        )
    }
    #[test]
    fn test_linear_in_13() {
        assert_eq!(
            linear_in(vec![-1, 0, 3, 3, 3, 10, 12], vec![-1, 10, 11]),
            linear_in_solution(vec![-1, 0, 3, 3, 3, 10, 12], vec![-1, 10, 11])
        )
    }
}

#[cfg(test)]
mod test_square_up {
    use super::implementations::square_up;
    use super::solutions::square_up_solution;

    #[test]
    fn test_square_up_1() {
        assert_eq!(square_up(3), square_up_solution(3))
    }
    #[test]
    fn test_square_up_2() {
        assert_eq!(square_up(2), square_up_solution(2))
    }
    #[test]
    fn test_square_up_3() {
        assert_eq!(square_up(4), square_up_solution(4))
    }
    #[test]
    fn test_square_up_4() {
        assert_eq!(square_up(1), square_up_solution(1))
    }
    #[test]
    fn test_square_up_5() {
        assert_eq!(square_up(0), square_up_solution(0))
    }
}

#[cfg(test)]
mod test_series_up {
    use super::implementations::series_up;
    use super::solutions::series_up_solution;

    #[test]
    fn test_series_up_1() {
        assert_eq!(series_up(3), series_up_solution(3))
    }
    #[test]
    fn test_series_up_2() {
        assert_eq!(series_up(4), series_up_solution(4))
    }
    #[test]
    fn test_series_up_3() {
        assert_eq!(series_up(2), series_up_solution(2))
    }
    #[test]
    fn test_series_up_4() {
        assert_eq!(series_up(1), series_up_solution(1))
    }
    #[test]
    fn test_series_up_5() {
        assert_eq!(series_up(0), series_up_solution(0))
    }
}

#[cfg(test)]
mod test_max_mirror {
    use super::implementations::max_mirror;
    use super::solutions::max_mirror_solution;

    #[test]
    fn test_max_mirror_1() {
        assert_eq!(max_mirror(vec![1, 2, 3, 8, 9, 3, 2, 1]), max_mirror_solution(vec![1, 2, 3, 8, 9, 3, 2, 1]))
    }
    #[test]
    fn test_max_mirror_2() {
        assert_eq!(max_mirror(vec![1, 2, 1, 4]), max_mirror_solution(vec![1, 2, 1, 4]))
    }
    #[test]
    fn test_max_mirror_3() {
        assert_eq!(max_mirror(vec![7, 1, 2, 9, 7, 2, 1]), max_mirror_solution(vec![7, 1, 2, 9, 7, 2, 1]))
    }
    #[test]
    fn test_max_mirror_4() {
        assert_eq!(
            max_mirror(vec![21, 22, 9, 8, 7, 6, 23, 24, 6, 7, 8, 9, 25, 7, 8, 9]),
            max_mirror_solution(vec![21, 22, 9, 8, 7, 6, 23, 24, 6, 7, 8, 9, 25, 7, 8, 9])
        )
    }
    #[test]
    fn test_max_mirror_5() {
        assert_eq!(
            max_mirror(vec![1, 2, 1, 20, 21, 1, 2, 1, 2, 23, 24, 2, 1, 2, 1, 25]),
            max_mirror_solution(vec![1, 2, 1, 20, 21, 1, 2, 1, 2, 23, 24, 2, 1, 2, 1, 25])
        )
    }
    #[test]
    fn test_max_mirror_6() {
        assert_eq!(max_mirror(vec![1, 2, 3, 2, 1]), max_mirror_solution(vec![1, 2, 3, 2, 1]))
    }
    #[test]
    fn test_max_mirror_7() {
        assert_eq!(max_mirror(vec![1, 2, 3, 3, 8]), max_mirror_solution(vec![1, 2, 3, 3, 8]))
    }
    #[test]
    fn test_max_mirror_8() {
        assert_eq!(max_mirror(vec![1, 2, 7, 8, 1, 7, 2]), max_mirror_solution(vec![1, 2, 7, 8, 1, 7, 2]))
    }
    #[test]
    fn test_max_mirror_9() {
        assert_eq!(max_mirror(vec![1, 1, 1]), max_mirror_solution(vec![1, 1, 1]))
    }
    #[test]
    fn test_max_mirror_10() {
        assert_eq!(max_mirror(vec![1]), max_mirror_solution(vec![1]))
    }
    #[test]
    fn test_max_mirror_11() {
        assert_eq!(max_mirror(vec![]), max_mirror_solution(vec![]))
    }
    #[test]
    fn test_max_mirror_12() {
        assert_eq!(max_mirror(vec![9, 1, 1, 4, 2, 1, 1, 1]), max_mirror_solution(vec![9, 1, 1, 4, 2, 1, 1, 1]))
    }
    #[test]
    fn test_max_mirror_13() {
        assert_eq!(max_mirror(vec![5, 9, 9, 4, 5, 4, 9, 9, 2]), max_mirror_solution(vec![5, 9, 9, 4, 5, 4, 9, 9, 2]))
    }
    #[test]
    fn test_max_mirror_14() {
        assert_eq!(max_mirror(vec![5, 9, 9, 6, 5, 4, 9, 9, 2]), max_mirror_solution(vec![5, 9, 9, 6, 5, 4, 9, 9, 2]))
    }
    #[test]
    fn test_max_mirror_15() {
        assert_eq!(max_mirror(vec![5, 9, 1, 6, 5, 4, 1, 9, 5]), max_mirror_solution(vec![5, 9, 1, 6, 5, 4, 1, 9, 5]))
    }
}

#[cfg(test)]
mod test_count_clumps {
    use super::implementations::count_clumps;
    use super::solutions::count_clumps_solution;

    #[test]
    fn test_count_clumps_1() {
        assert_eq!(count_clumps(vec![1, 2, 2, 3, 4, 4]), count_clumps_solution(vec![1, 2, 2, 3, 4, 4]))
    }
    #[test]
    fn test_count_clumps_2() {
        assert_eq!(count_clumps(vec![1, 1, 2, 1, 1]), count_clumps_solution(vec![1, 1, 2, 1, 1]))
    }
    #[test]
    fn test_count_clumps_3() {
        assert_eq!(count_clumps(vec![1, 1, 1, 1, 1]), count_clumps_solution(vec![1, 1, 1, 1, 1]))
    }
    #[test]
    fn test_count_clumps_4() {
        assert_eq!(count_clumps(vec![1, 2, 3]), count_clumps_solution(vec![1, 2, 3]))
    }
    #[test]
    fn test_count_clumps_5() {
        assert_eq!(count_clumps(vec![2, 2, 1, 1, 1, 2, 1, 1, 2, 2]), count_clumps_solution(vec![2, 2, 1, 1, 1, 2, 1, 1, 2, 2]))
    }
    #[test]
    fn test_count_clumps_6() {
        assert_eq!(count_clumps(vec![0, 2, 2, 1, 1, 1, 2, 1, 1, 2, 2]), count_clumps_solution(vec![0, 2, 2, 1, 1, 1, 2, 1, 1, 2, 2]))
    }
    #[test]
    fn test_count_clumps_7() {
        assert_eq!(count_clumps(vec![0, 0, 2, 2, 1, 1, 1, 2, 1, 1, 2, 2]), count_clumps_solution(vec![0, 0, 2, 2, 1, 1, 1, 2, 1, 1, 2, 2]))
    }
    #[test]
    fn test_count_clumps_8() {
        assert_eq!(
            count_clumps(vec![0, 0, 0, 2, 2, 1, 1, 1, 2, 1, 1, 2, 2]),
            count_clumps_solution(vec![0, 0, 0, 2, 2, 1, 1, 1, 2, 1, 1, 2, 2])
        )
    }
    #[test]
    fn test_count_clumps_9() {
        assert_eq!(count_clumps(vec![]), count_clumps_solution(vec![]))
    }
}
