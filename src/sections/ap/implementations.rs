use std::fmt::Display;

/// ## AP-1 -- scoresIncreasing
/// Given a vector of scores, return true if each score is equal or greater than
/// the one before. The vector will be length 2 or more.
/// ### Examples
///    scores_increasing([1, 3, 4]) -> true
///    scores_increasing([1, 3, 2]) -> false
///    scores_increasing([1, 1, 4]) -> true
#[allow(unused)]
pub fn scores_increasing(scores: Vec<i32>) -> bool {
    todo!()
}

/// ## AP-1 -- scores100
/// Given a vector of scores, return true if there are scores of 100 next to
/// each other in the vector. The vector length will be at least 2.
/// ### Examples
///    scores100([1, 100, 100]) -> true
///    scores100([1, 100, 99, 100]) -> false
///    scores100([100, 1, 100, 100]) -> true
#[allow(unused)]
pub fn scores100(scores: Vec<i32>) -> bool {
    todo!()
}

/// ## AP-1 -- scoresClump
/// Given a vector of scores sorted in increasing order, return true if the
/// vector contains 3 adjacent scores that differ from each other by at most 2,
/// such as with {3, 4, 5} or {3, 5, 5}.
/// ### Examples
///    scores_clump([3, 4, 5]) -> true
///    scores_clump([3, 4, 6]) -> false
///    scores_clump([1, 3, 5, 5]) -> true
#[allow(unused)]
pub fn scores_clump(scores: Vec<i32>) -> bool {
    todo!()
}

/// ## AP-1 -- scoresAverage
/// Given a vector of scores, compute the int average of the first half and the
/// second half, and return whichever is larger. We'll say that the second half
/// begins at index length/2. The vector length will be at least 2. To practice
/// decomposition, write a separate helper method int average(int[] scores, int
/// start, int end) { which computes the average of the elements between indexes
/// start..end. Call your helper method twice to implement scoresAverage().
/// Write your helper method as a closure within scores_average. Normally you
/// would compute averages with doubles, but here we use ints so the expected
/// results are exact.
/// ### Examples
///    scores_average([2, 2, 4, 4]) -> 4
///    scores_average([4, 4, 4, 2, 2, 2]) -> 4
///    scores_average([3, 4, 5, 1, 2, 3]) -> 4
#[allow(unused)]
pub fn scores_average(scores: Vec<i32>) -> i32 {
    todo!()
}

/// ## AP-1 -- wordsCount
/// Given a vector of strings, return the count of the number of strings with
/// the given length.
/// ### Examples
///    words_count(["a", "bb", "b", "ccc"], 1) -> 2
///    words_count(["a", "bb", "b", "ccc"], 3) -> 1
///    words_count(["a", "bb", "b", "ccc"], 4) -> 0
#[allow(unused)]
pub fn words_count(words: Vec<String>, len: usize) -> i32 {
    todo!()
}

/// ## AP-1 -- wordsFront
/// Given a vector of strings, return a new vector containing the first N
/// strings. N will be in the range 1..length.
/// ### Examples
///    words_front(["a", "b", "c", "d"], 1) -> a
///    words_front(["a", "b", "c", "d"], 2) -> a,b
///    words_front(["a", "b", "c", "d"], 3) -> a,b,c
#[allow(unused)]
pub fn words_front(words: Vec<String>, n: usize) -> Vec<String> {
    todo!()
}

/// ## AP-1 -- wordsWithoutList
/// Given a vector of strings, return a new List (e.g. an VectorList) where all
/// the strings of the given length are omitted. See wordsWithout() below which
/// is more difficult because it uses vectors.
/// ### Examples
///    words_without_list(["a", "bb", "b", "ccc"], 1) -> bb,ccc
///    words_without_list(["a", "bb", "b", "ccc"], 3) -> a,bb,b
///    words_without_list(["a", "bb", "b", "ccc"], 4) -> a,bb,b,ccc
#[allow(unused)]
pub fn words_without_list(words: Vec<String>, len: usize) -> Vec<String> {
    todo!()
}

/// ## AP-1 -- hasOne
/// Given a positive int n, return true if it contains a 1 digit. Note: use % to
/// get the rightmost digit, and / to discard the rightmost digit.
/// ### Examples
///    has_one(10) -> true
///    has_one(22) -> false
///    has_one(220) -> false
#[allow(unused)]
pub fn has_one(n: i32) -> bool {
    todo!()
}

/// ## AP-1 -- dividesSelf
/// We'll say that a positive int divides itself if every digit in the number
/// divides into the number evenly. So for example 128 divides itself since 1,
/// 2, and 8 all divide into 128 evenly. We'll say that 0 does not divide into
/// anything evenly, so no number with a 0 digit divides itself. Note: use % to
/// get the rightmost digit, and / to discard the rightmost digit.
/// ### Examples
///    divides_self(128) -> true
///    divides_self(12) -> true
///    divides_self(120) -> false
#[allow(unused)]
pub fn divides_self(n: i32) -> bool {
    todo!()
}

/// ## AP-1 -- copyEvens
/// Given a vector of positive ints, return a new vector of length "count"
/// containing the first even numbers from the original vector. The original
/// vector will contain at least "count" even numbers.
/// ### Examples
///    copy_evens([3, 2, 4, 5, 8], 2) -> 2,4
///    copy_evens([3, 2, 4, 5, 8], 3) -> 2,4,8
///    copy_evens([6, 1, 2, 4, 5, 8], 3) -> 6,2,4
#[allow(unused)]
pub fn copy_evens(nums: Vec<i32>, count: usize) -> Vec<i32> {
    todo!()
}

/// ## AP-1 -- copyEndy
/// We'll say that a positive int n is "endy" if it is in the range 0..10 or
/// 90..100 (inclusive). Given a vector of positive ints, return a new vector of
/// length "count" containing the first endy numbers from the original vector.
/// Write a closure to test if a number is endy.
/// The original vector will contain at least "count" endy numbers.
/// ### Examples
///    copy_endy([9, 11, 90, 22, 6], 2) -> 9,90
///    copy_endy([9, 11, 90, 22, 6], 3) -> 9,90,6
///    copy_endy([12, 1, 1, 13, 0, 20], 2) -> 1,1
#[allow(unused)]
pub fn copy_endy(nums: Vec<i32>, count: usize) -> Vec<i32> {
    todo!()
}

/// ## AP-1 -- matchUp
/// Given 2 vectors that are the same length containing strings, compare the 1st
/// string in one vector to the 1st string in the other vector, the 2nd to the 2nd
/// and so on. Count the number of times that the 2 strings are non-empty and
/// start with the same char. The strings may be any length, including 0.
/// ### Examples
///    match_up(["aa", "bb", "cc"], ["aaa", "xx", "bb"]) -> 1
///    match_up(["aa", "bb", "cc"], ["aaa", "b", "bb"]) -> 2
///    match_up(["aa", "bb", "cc"], ["", "", "ccc"]) -> 1
#[allow(unused)]
pub fn match_up(a: Vec<String>, b: Vec<String>) -> i32 {
    todo!()
}

/// ## AP-1 -- scoreUp
/// The "key" vector is an vector containing the correct answers to an exam, like
/// {"a", "a", "b", "b"}. the "answers" vector contains a student's answers, with
/// "?" representing a question left blank. The two vectors are not empty and are
/// the same length. Return the score for this vector of answers, giving +4 for
/// each correct answer, -1 for each incorrect answer, and +0 for each blank
/// answer.
/// ### Examples
///    score_up(["a", "a", "b", "b"], ["a", "c", "b", "c"]) -> 6
///    score_up(["a", "a", "b", "b"], ["a", "a", "b", "c"]) -> 11
///    score_up(["a", "a", "b", "b"], ["a", "a", "b", "b"]) -> 16
#[allow(unused)]
pub fn score_up(key: Vec<String>, answers: Vec<String>) -> i32 {
    todo!()
}

/// ## AP-1 -- wordsWithout
/// Given a vector of strings, return a new vector without the strings that are
/// equal to the target string. One approach is to count the occurrences of the
/// target string, make a new vector of the correct length, and then copy over
/// the correct strings.
/// ### Examples
///    words_without(["a", "b", "c", "a"], "a") -> b,c
///    words_without(["a", "b", "c", "a"], "b") -> a,c,a
///    words_without(["a", "b", "c", "a"], "c") -> a,b,a
#[allow(unused)]
pub fn words_without(words: Vec<String>, target: impl Into<String>) -> Vec<String> {
    todo!()
}

/// ## AP-1 -- scoresSpecial
/// Given two vectors, A and B, of non-negative int scores. A "special" score is
/// one which is a multiple of 10, such as 40 or 90. Return the sum of largest
/// special score in A and the largest special score in B. To practice
/// decomposition, write a closure which finds the largest special score in an
/// vector.
/// ### Examples
///    scores_special([12, 10, 4], [2, 20, 30]) -> 40
///    scores_special([20, 10, 4], [2, 20, 10]) -> 40
///    scores_special([12, 11, 4], [2, 20, 31]) -> 20
#[allow(unused)]
pub fn scores_special(a: Vec<i32>, b: Vec<i32>) -> i32 {
    todo!()
}

/// ## AP-1 -- sumHeights
/// We have a vector of heights, representing the altitude along a walking
/// trail. Given start/end indexes into the vector, return the sum of the changes
/// for a walk beginning at the start index and ending at the end index. For
/// example, with the heights {5, 3, 6, 7, 2} and start=2, end=4 yields a sum of
/// 1 + 5 = 6. The start end end index will both be valid indexes into the vector
/// with start <= end.
/// ### Examples
///    sum_heights([5, 3, 6, 7, 2], 2, 4) -> 6
///    sum_heights([5, 3, 6, 7, 2], 0, 1) -> 2
///    sum_heights([5, 3, 6, 7, 2], 0, 4) -> 11
#[allow(unused)]
pub fn sum_heights(heights: Vec<i32>, start: usize, end: usize) -> i32 {
    todo!()
}

/// ## AP-1 -- sumHeights2
/// (A variation on the sumHeights problem.) We have a vector of heights,
/// representing the altitude along a walking trail. Given start/end indexes
/// into the vector, return the sum of the changes for a walk beginning at the
/// start index and ending at the end index, however increases in height count
/// double. For example, with the heights {5, 3, 6, 7, 2} and start=2, end=4
/// yields a sum of 1*2 + 5 = 7. The start end end index will both be valid
/// indexes into the vector with start <= end.
/// ### Examples
///    sum_heights2([5, 3, 6, 7, 2], 2, 4) -> 7
///    sum_heights2([5, 3, 6, 7, 2], 0, 1) -> 2
///    sum_heights2([5, 3, 6, 7, 2], 0, 4) -> 15
#[allow(unused)]
pub fn sum_heights2(heights: Vec<i32>, start: usize, end: usize) -> i32 {
    todo!()
}

/// ## AP-1 -- bigHeights
/// (A variation on the sumHeights problem.) We have a vector of heights,
/// representing the altitude along a walking trail. Given start/end indexes
/// into the vector, return the number of "big" steps for a walk starting at the
/// start index and ending at the end index. We'll say that step is big if it is
/// 5 or more up or down. The start end end index will both be valid indexes
/// into the vector with start <= end.
/// ### Examples
///    big_heights([5, 3, 6, 7, 2], 2, 4) -> 1
///    big_heights([5, 3, 6, 7, 2], 0, 1) -> 0
///    big_heights([5, 3, 6, 7, 2], 0, 4) -> 1
#[allow(unused)]
pub fn big_heights(heights: Vec<i32>, start: usize, end: usize) -> i32 {
    todo!()
}

/// ## AP-1 -- userCompare
/// We have data for two users, A and B, each with a String name and an int id.
/// The goal is to order the users such as for sorting. Return an appropriate
/// UserCompareResult to indicate which user should be sorted before the other.
/// Order first by the string names, and then by the id numbers if the names
/// are the same.
/// ### Examples
///    user_compare(User::new("bb", 1), User::new("zz", 2)) -> UserCompareResult::Afirst
///    user_compare(User::new("bb", 1), User::new("aa", 2)) -> UserCompareResult::Bfirst
///    user_compare(User::new("bb", 1), User::new("bb", 1)) -> UserCompareResult::Neither
#[allow(unused)]
pub fn user_compare(a: User, b: User) -> UserCompareResult {
    todo!()
}

#[derive(Debug)]
pub struct User {
    pub name: String,
    pub id: u32,
}

impl Display for User {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "User{{{}, {}}}", self.name, self.id)
    }
}

impl User {
    pub fn new(name: impl Into<String>, id: u32) -> Self {
        Self { name: name.into(), id }
    }
}

#[derive(Debug, PartialEq)]
pub enum UserCompareResult {
    Afirst,
    Neither,
    Bfirst,
}

impl Display for UserCompareResult {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                UserCompareResult::Afirst => "A first",
                UserCompareResult::Neither => "Neither",
                UserCompareResult::Bfirst => "B first",
            }
        )
    }
}

/// ## AP-1 -- mergeTwo
/// Start with two vectors of strings, A and B, each with its elements in
/// alphabetical order and without duplicates. Return a new vector containing the
/// first N elements from the two vectors. The result vector should be in
/// alphabetical order and without duplicates. A and B will both have a length
/// which is N or more. The best "linear" solution makes a single pass over A
/// and B, taking advantage of the fact that they are in alphabetical order,
/// copying elements directly to the new vector.
/// ### Examples
///    merge_two(["a", "c", "z"], ["b", "f", "z"], 3) -> a,b,c
///    merge_two(["a", "c", "z"], ["c", "f", "z"], 3) -> a,c,f
///    merge_two(["f", "g", "z"], ["c", "f", "g"], 3) -> c,f,g
#[allow(unused)]
pub fn merge_two(a: Vec<String>, b: Vec<String>, n: i32) -> Vec<String> {
    todo!()
}

/// ## AP-1 -- commonTwo
/// Start with two vectors of strings, a and b, each in alphabetical order,
/// possibly with duplicates. Return the count of the number of strings which
/// appear in both vectors. The best "linear" solution makes a single pass over
/// both vectors, taking advantage of the fact that they are in alphabetical
/// order.
/// ### Examples
///    common_two(["a", "c", "x"], ["b", "c", "d", "x"]) -> 2
///    common_two(["a", "c", "x"], ["a", "b", "c", "x", "z"]) -> 3
///    common_two(["a", "b", "c"], ["a", "b", "c"]) -> 3
#[allow(unused)]
pub fn common_two(a: Vec<String>, b: Vec<String>) -> usize {
    todo!()
}
