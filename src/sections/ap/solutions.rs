#![allow(unused)]

use std::{cmp::max, collections::HashSet, slice::Iter};

use itertools::Itertools;

use super::implementations::{User, UserCompareResult};

pub fn scores_increasing_solution(scores: Vec<i32>) -> bool {
    // let len = scores.len();

    // for i in 1..len {
    //     let left = scores[i - 1];
    //     let right = scores[i];

    //     if left > right {
    //         return false;
    //     }
    // }

    // true

    scores.windows(2).all(|e| e[0] <= e[1])
}

pub fn scores100_solution(scores: Vec<i32>) -> bool {
    // let len = scores.len();

    // for i in 1..len {
    //     let left = scores[i - 1];
    //     let right = scores[i];

    //     if (left == 100) && (left == right) {
    //         return true;
    //     }
    // }

    // false

    scores
        .windows(2) // Iter
        .filter(|&e| e[0] == e[1])
        .filter(|&e| e[0] == 100)
        .count()
        > 0
}

pub fn scores_clump_solution(scores: Vec<i32>) -> bool {
    scores.windows(3).map(|e| e[2] - e[0] <= 2).any(|e| e == true)
}

pub fn scores_average_solution(scores: Vec<i32>) -> i32 {
    let average = |s: &Vec<i32>| {
        s.iter() //Wrap
            .map(|e| *e as f64)
            .sum::<f64>()
            / (s.len() as f64)
    };
    let split = |s: &Vec<i32>, at: usize| {
        vec![
            s.iter().take(at).map(|&e| e).collect_vec(), // Wrap
            s.iter().skip(at).map(|&e| e).collect_vec(),
        ]
    };
    let mid = scores.len() / 2;

    split(&scores, mid).iter().map(average).map(|e| e.round() as i32).max().unwrap_or(0)
}

pub fn words_count_solution(words: Vec<String>, len: usize) -> i32 {
    words.iter().filter(|&e| e.len() == len).count() as i32
}

pub fn words_front_solution(words: Vec<String>, n: usize) -> Vec<String> {
    words.iter().take(n).map(|e| e.clone()).collect_vec()
}

pub fn words_without_list_solution(words: Vec<String>, len: usize) -> Vec<String> {
    words.iter().filter(|&e| e.len() != len).map(|e| e.clone()).collect_vec()
}

pub fn has_one_solution(n: i32) -> bool {
    let mut v = n;
    while v > 0 {
        let digit = v % 10;
        if digit == 1 {
            return true;
        }
        v = v / 10;
    }
    false
}

pub fn divides_self_solution(n: i32) -> bool {
    let mut v = n;
    while v > 0 {
        let digit = v % 10;
        if digit == 0 {
            return false;
        } else if (digit > 1) && (0 != (n % digit)) {
            return false;
        }
        v = v / 10;
    }
    true
}

pub fn copy_evens_solution(nums: Vec<i32>, count: usize) -> Vec<i32> {
    nums.iter().filter(|&e| 0 == (*e % 2)).take(count).map(|e| e.clone()).collect_vec()
}

pub fn copy_endy_solution(nums: Vec<i32>, count: usize) -> Vec<i32> {
    nums.iter()
        .filter(|&&e| ((e >= 0) && (e <= 10)) || ((e >= 90) && (e <= 100)))
        .take(count)
        .map(|e| e.clone())
        .collect_vec()
}

pub fn match_up_solution(a: Vec<String>, b: Vec<String>) -> i32 {
    let get_first_char = |vec: &&String| {
        // Wrap
        vec.chars().take(1).collect::<String>()
    };
    a.iter()
        .zip(b.iter())
        .filter(|(u, v)| {
            // Wrap
            (u.len() > 0) && (v.len() > 0)
        })
        .filter(|(u, v)| {
            // Wrap
            get_first_char(u) == get_first_char(v)
        })
        .count() as i32
}

pub fn score_up_solution(key: Vec<String>, answers: Vec<String>) -> i32 {
    key.iter()
        .zip(answers.iter())
        .map(|(k, a)| {
            if k.eq(a) {
                4
            } else if a.eq("?") {
                0
            } else {
                -1
            }
        })
        .sum()
}

pub fn words_without_solution(words: Vec<String>, target: impl Into<String>) -> Vec<String> {
    let target: String = target.into();
    words.iter().filter(|&e| !e.eq(&target)).map(|e| e.clone()).collect_vec()
}

pub fn scores_special_solution(a: Vec<i32>, b: Vec<i32>) -> i32 {
    let filter_special = |&v: &&i32| 0 == (*v % 10);
    let max_special = |iter: Iter<i32>| {
        //Wrap
        iter.filter(filter_special).fold(0, |a, &e| max(a, e))
    };
    let max_a = max_special(a.iter());
    let max_b = max_special(b.iter());

    max_a + max_b
}

pub fn sum_heights_solution(heights: Vec<i32>, start: usize, end: usize) -> i32 {
    // let deltas = heights.windows(2).map(|a| (a[1] - a[0]).abs()).collect_vec();
    // let diff = deltas.iter().skip(start).take(end - start).sum();
    heights.windows(2).map(|a| (a[1] - a[0]).abs()).skip(start).take(end - start).sum()
}

pub fn sum_heights2_solution(heights: Vec<i32>, start: usize, end: usize) -> i32 {
    heights
        .windows(2)
        .map(|a| {
            let v = (a[1] - a[0]).abs();
            if a[1] > a[0] {
                v * 2
            } else {
                v
            }
        })
        .skip(start)
        .take(end - start)
        .sum()
}

pub fn big_heights_solution(heights: Vec<i32>, start: usize, end: usize) -> i32 {
    heights
        .windows(2) // Wrap
        .map(|a| (a[1] - a[0]).abs())
        .skip(start)
        .take(end - start)
        .filter(|&e| e >= 5)
        .count() as i32
}

pub fn user_compare_solution(a: User, b: User) -> UserCompareResult {
    if a.name == b.name {
        if a.id < b.id {
            UserCompareResult::Afirst
        } else if a.id > b.id {
            UserCompareResult::Bfirst
        } else {
            UserCompareResult::Neither
        }
    } else {
        if a.name < b.name {
            UserCompareResult::Afirst
        } else {
            UserCompareResult::Bfirst
        }
    }
}

pub fn merge_two_solution(a: Vec<String>, b: Vec<String>, n: usize) -> Vec<String> {
    let mut ai = 0usize;
    let mut bi = 0usize;

    let mut result = vec![];
    let mut count = 0usize;
    while count < n {
        let a_val = a.get(ai);
        let b_val = b.get(bi);

        if a_val.is_some() && b_val.is_some() {
            let a_val = a_val.unwrap().clone();
            let b_val = b_val.unwrap().clone();

            if a_val <= b_val {
                if a_val == b_val {
                    bi += 1;
                }
                result.push(a_val);
                ai += 1;
            } else if a_val > b_val {
                result.push(b_val);
                bi += 1;
            }
        }

        count += 1;
    }

    result
}

pub fn common_two_solution(a: Vec<String>, b: Vec<String>) -> usize {
    let a_len = a.len();
    let b_len = b.len();

    let biggr: HashSet<_>;
    let small: HashSet<_>;

    if a_len <= b_len {
        biggr = HashSet::from_iter(a.iter());
        small = HashSet::from_iter(b.iter());
    } else {
        biggr = HashSet::from_iter(b.iter());
        small = HashSet::from_iter(a.iter());
    }

    let mut count = 0usize;
    for v in biggr {
        if small.contains(v) {
            count += 1;
        }
    }

    count
}
