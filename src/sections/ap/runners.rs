use super::implementations::*;
use super::solutions::*;
use crate::{
    display::{BatVec, TestDisplay},
    strvec,
};

pub fn run(test: Option<Vec<String>>) {
    match test {
        // None => println!("Please specify a test name"),
        None => {
            scores_increasing_run();
            scores100_run();
            scores_clump_run();
            scores_average_run();
            words_count_run();
            words_front_run();
            words_without_list_run();
            has_one_run();
            divides_self_run();
            copy_evens_run();
            copy_endy_run();
            match_up_run();
            score_up_run();
            words_without_run();
            scores_special_run();
            sum_heights_run();
            sum_heights2_run();
            big_heights_run();
            user_compare_run();
            merge_two_run();
            common_two_run();
        }
        Some(tests) => {
            for test_name in tests {
                match test_name.to_ascii_lowercase().as_str() {
                    "scoresIncreasing" | "scores_increasing" => scores_increasing_run(),
                    "scores100" => scores100_run(),
                    "scoresClump" | "scores_clump" => scores_clump_run(),
                    "scoresAverage" | "scores_average" => scores_average_run(),
                    "wordsCount" | "words_count" => words_count_run(),
                    "wordsFront" | "words_front" => words_front_run(),
                    "wordsWithoutList" | "words_without_list" => words_without_list_run(),
                    "hasOne" | "has_one" => has_one_run(),
                    "dividesSelf" | "divides_self" => divides_self_run(),
                    "copyEvens" | "copy_evens" => copy_evens_run(),
                    "copyEndy" | "copy_endy" => copy_endy_run(),
                    "matchUp" | "match_up" => match_up_run(),
                    "scoreUp" | "score_up" => score_up_run(),
                    "wordsWithout" | "words_without" => words_without_run(),
                    "scoresSpecial" | "scores_special" => scores_special_run(),
                    "sumHeights" | "sum_heights" => sum_heights_run(),
                    "sumHeights2" | "sum_heights2" => sum_heights2_run(),
                    "bigHeights" | "big_heights" => big_heights_run(),
                    "userCompare" | "user_compare" => user_compare_run(),
                    "mergeTwo" | "merge_two" => merge_two_run(),
                    "commonTwo" | "common_two" => common_two_run(),
                    _ => println!("No test named {test_name}"),
                };
            }
        }
    };
}

pub fn scores_increasing_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("scores_increasing");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 3, 4])), scores_increasing_solution(vec![1, 3, 4]), scores_increasing(vec![1, 3, 4]));
    layout.print_test(format!("{:?}", (vec![1, 3, 2])), scores_increasing_solution(vec![1, 3, 2]), scores_increasing(vec![1, 3, 2]));
    layout.print_test(format!("{:?}", (vec![1, 1, 4])), scores_increasing_solution(vec![1, 1, 4]), scores_increasing(vec![1, 1, 4]));
    layout.print_test(
        format!("{:?}", (vec![1, 1, 2, 4, 4, 7])),
        scores_increasing_solution(vec![1, 1, 2, 4, 4, 7]),
        scores_increasing(vec![1, 1, 2, 4, 4, 7]),
    );
    layout.print_test(
        format!("{:?}", (vec![1, 1, 2, 4, 3, 7])),
        scores_increasing_solution(vec![1, 1, 2, 4, 3, 7]),
        scores_increasing(vec![1, 1, 2, 4, 3, 7]),
    );
    layout.print_footer();
}

pub fn scores100_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("scores100");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 100, 100])), scores100_solution(vec![1, 100, 100]), scores100(vec![1, 100, 100]));
    layout.print_test(format!("{:?}", (vec![1, 100, 99, 100])), scores100_solution(vec![1, 100, 99, 100]), scores100(vec![1, 100, 99, 100]));
    layout.print_test(format!("{:?}", (vec![100, 1, 100, 100])), scores100_solution(vec![100, 1, 100, 100]), scores100(vec![100, 1, 100, 100]));
    layout.print_test(format!("{:?}", (vec![100, 1, 100, 1])), scores100_solution(vec![100, 1, 100, 1]), scores100(vec![100, 1, 100, 1]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3, 4, 5])), scores100_solution(vec![1, 2, 3, 4, 5]), scores100(vec![1, 2, 3, 4, 5]));
    layout.print_test(format!("{:?}", (vec![1, 2, 100, 4, 5])), scores100_solution(vec![1, 2, 100, 4, 5]), scores100(vec![1, 2, 100, 4, 5]));
    layout.print_footer();
}

pub fn scores_clump_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("scores_clump");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![3, 4, 5])), scores_clump_solution(vec![3, 4, 5]), scores_clump(vec![3, 4, 5]));
    layout.print_test(format!("{:?}", (vec![3, 4, 6])), scores_clump_solution(vec![3, 4, 6]), scores_clump(vec![3, 4, 6]));
    layout.print_test(format!("{:?}", (vec![1, 3, 5, 5])), scores_clump_solution(vec![1, 3, 5, 5]), scores_clump(vec![1, 3, 5, 5]));
    layout.print_test(format!("{:?}", (vec![2, 4, 5, 6])), scores_clump_solution(vec![2, 4, 5, 6]), scores_clump(vec![2, 4, 5, 6]));
    layout.print_test(format!("{:?}", (vec![2, 4, 5, 7])), scores_clump_solution(vec![2, 4, 5, 7]), scores_clump(vec![2, 4, 5, 7]));
    layout.print_test(format!("{:?}", (vec![2, 4, 4, 7])), scores_clump_solution(vec![2, 4, 4, 7]), scores_clump(vec![2, 4, 4, 7]));
    layout.print_test(format!("{:?}", (vec![3, 3, 6, 7, 9])), scores_clump_solution(vec![3, 3, 6, 7, 9]), scores_clump(vec![3, 3, 6, 7, 9]));
    layout.print_test(format!("{:?}", (vec![3, 3, 7, 7, 9])), scores_clump_solution(vec![3, 3, 7, 7, 9]), scores_clump(vec![3, 3, 7, 7, 9]));
    layout.print_test(format!("{:?}", (vec![4, 5, 8])), scores_clump_solution(vec![4, 5, 8]), scores_clump(vec![4, 5, 8]));
    layout.print_footer();
}

pub fn scores_average_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("scores_average");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![2, 2, 4, 4])), scores_average_solution(vec![2, 2, 4, 4]), scores_average(vec![2, 2, 4, 4]));
    layout.print_test(
        format!("{:?}", (vec![4, 4, 4, 2, 2, 2])),
        scores_average_solution(vec![4, 4, 4, 2, 2, 2]),
        scores_average(vec![4, 4, 4, 2, 2, 2]),
    );
    layout.print_test(
        format!("{:?}", (vec![3, 4, 5, 1, 2, 3])),
        scores_average_solution(vec![3, 4, 5, 1, 2, 3]),
        scores_average(vec![3, 4, 5, 1, 2, 3]),
    );
    layout.print_test(format!("{:?}", (vec![5, 6])), scores_average_solution(vec![5, 6]), scores_average(vec![5, 6]));
    layout.print_test(format!("{:?}", (vec![5, 4])), scores_average_solution(vec![5, 4]), scores_average(vec![5, 4]));
    layout.print_test(
        format!("{:?}", (vec![5, 4, 5, 6, 2, 1, 2, 3])),
        scores_average_solution(vec![5, 4, 5, 6, 2, 1, 2, 3]),
        scores_average(vec![5, 4, 5, 6, 2, 1, 2, 3]),
    );
    layout.print_footer();
}

pub fn words_count_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("words_count");
    layout.print_header();
    layout.print_test(
        format!("{:?}", (strvec!["a", "bb", "b", "ccc"], 1)),
        words_count_solution(strvec!["a", "bb", "b", "ccc"], 1),
        words_count(strvec!["a", "bb", "b", "ccc"], 1),
    );
    layout.print_test(
        format!("{:?}", (strvec!["a", "bb", "b", "ccc"], 3)),
        words_count_solution(strvec!["a", "bb", "b", "ccc"], 3),
        words_count(strvec!["a", "bb", "b", "ccc"], 3),
    );
    layout.print_test(
        format!("{:?}", (strvec!["a", "bb", "b", "ccc"], 4)),
        words_count_solution(strvec!["a", "bb", "b", "ccc"], 4),
        words_count(strvec!["a", "bb", "b", "ccc"], 4),
    );
    layout.print_test(
        format!("{:?}", (strvec!["xx", "yyy", "x", "yy", "z"], 1)),
        words_count_solution(strvec!["xx", "yyy", "x", "yy", "z"], 1),
        words_count(strvec!["xx", "yyy", "x", "yy", "z"], 1),
    );
    layout.print_test(
        format!("{:?}", (strvec!["xx", "yyy", "x", "yy", "z"], 2)),
        words_count_solution(strvec!["xx", "yyy", "x", "yy", "z"], 2),
        words_count(strvec!["xx", "yyy", "x", "yy", "z"], 2),
    );
    layout.print_footer();
}

pub fn words_front_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("words_front");
    layout.print_header();
    layout.print_test::<BatVec<String>>(
        format!("{:?}", (strvec!["a", "b", "c", "d"], 1)),
        words_front_solution(strvec!["a", "b", "c", "d"], 1).into(),
        words_front(strvec!["a", "b", "c", "d"], 1).into(),
    );
    layout.print_test::<BatVec<String>>(
        format!("{:?}", (strvec!["a", "b", "c", "d"], 2)),
        words_front_solution(strvec!["a", "b", "c", "d"], 2).into(),
        words_front(strvec!["a", "b", "c", "d"], 2).into(),
    );
    layout.print_test::<BatVec<String>>(
        format!("{:?}", (strvec!["a", "b", "c", "d"], 3)),
        words_front_solution(strvec!["a", "b", "c", "d"], 3).into(),
        words_front(strvec!["a", "b", "c", "d"], 3).into(),
    );
    layout.print_test::<BatVec<String>>(
        format!("{:?}", (strvec!["a", "b", "c", "d"], 4)),
        words_front_solution(strvec!["a", "b", "c", "d"], 4).into(),
        words_front(strvec!["a", "b", "c", "d"], 4).into(),
    );
    layout.print_test::<BatVec<String>>(
        format!("{:?}", (strvec!["Hi", "There"], 1)),
        words_front_solution(strvec!["Hi", "There"], 1).into(),
        words_front(strvec!["Hi", "There"], 1).into(),
    );
    layout.print_footer();
}

pub fn words_without_list_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("words_without_list");
    layout.print_header();
    layout.print_test::<BatVec<String>>(
        format!("{:?}", (strvec!["a", "bb", "b", "ccc"], 1)),
        words_without_list_solution(strvec!["a", "bb", "b", "ccc"], 1).into(),
        words_without_list(strvec!["a", "bb", "b", "ccc"], 1).into(),
    );
    layout.print_test::<BatVec<String>>(
        format!("{:?}", (strvec!["a", "bb", "b", "ccc"], 3)),
        words_without_list_solution(strvec!["a", "bb", "b", "ccc"], 3).into(),
        words_without_list(strvec!["a", "bb", "b", "ccc"], 3).into(),
    );
    layout.print_test::<BatVec<String>>(
        format!("{:?}", (strvec!["a", "bb", "b", "ccc"], 4)),
        words_without_list_solution(strvec!["a", "bb", "b", "ccc"], 4).into(),
        words_without_list(strvec!["a", "bb", "b", "ccc"], 4).into(),
    );
    layout.print_test::<BatVec<String>>(
        format!("{:?}", (strvec!["xx", "yyy", "x", "yy", "z"], 1)),
        words_without_list_solution(strvec!["xx", "yyy", "x", "yy", "z"], 1).into(),
        words_without_list(strvec!["xx", "yyy", "x", "yy", "z"], 1).into(),
    );
    layout.print_footer();
}

pub fn has_one_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("has_one");
    layout.print_header();
    layout.print_test(format!("{:?}", (10)), has_one_solution(10), has_one(10));
    layout.print_test(format!("{:?}", (22)), has_one_solution(22), has_one(22));
    layout.print_test(format!("{:?}", (220)), has_one_solution(220), has_one(220));
    layout.print_test(format!("{:?}", (212)), has_one_solution(212), has_one(212));
    layout.print_test(format!("{:?}", (1)), has_one_solution(1), has_one(1));
    layout.print_test(format!("{:?}", (9)), has_one_solution(9), has_one(9));
    layout.print_test(format!("{:?}", (211112)), has_one_solution(211112), has_one(211112));
    layout.print_test(format!("{:?}", (121121)), has_one_solution(121121), has_one(121121));
    layout.print_test(format!("{:?}", (222222)), has_one_solution(222222), has_one(222222));
    layout.print_test(format!("{:?}", (56156)), has_one_solution(56156), has_one(56156));
    layout.print_test(format!("{:?}", (56556)), has_one_solution(56556), has_one(56556));
    layout.print_footer();
}

pub fn divides_self_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("divides_self");
    layout.print_header();
    layout.print_test(format!("{:?}", (128)), divides_self_solution(128), divides_self(128));
    layout.print_test(format!("{:?}", (12)), divides_self_solution(12), divides_self(12));
    layout.print_test(format!("{:?}", (120)), divides_self_solution(120), divides_self(120));
    layout.print_test(format!("{:?}", (122)), divides_self_solution(122), divides_self(122));
    layout.print_test(format!("{:?}", (13)), divides_self_solution(13), divides_self(13));
    layout.print_test(format!("{:?}", (32)), divides_self_solution(32), divides_self(32));
    layout.print_test(format!("{:?}", (22)), divides_self_solution(22), divides_self(22));
    layout.print_test(format!("{:?}", (42)), divides_self_solution(42), divides_self(42));
    layout.print_test(format!("{:?}", (212)), divides_self_solution(212), divides_self(212));
    layout.print_test(format!("{:?}", (213)), divides_self_solution(213), divides_self(213));
    layout.print_test(format!("{:?}", (162)), divides_self_solution(162), divides_self(162));
    layout.print_footer();
}

pub fn copy_evens_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("copy_evens");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(
        format!("{:?}", (vec![3, 2, 4, 5, 8], 2)),
        copy_evens_solution(vec![3, 2, 4, 5, 8], 2).into(),
        copy_evens(vec![3, 2, 4, 5, 8], 2).into(),
    );
    layout.print_test::<BatVec<i32>>(
        format!("{:?}", (vec![3, 2, 4, 5, 8], 3)),
        copy_evens_solution(vec![3, 2, 4, 5, 8], 3).into(),
        copy_evens(vec![3, 2, 4, 5, 8], 3).into(),
    );
    layout.print_test::<BatVec<i32>>(
        format!("{:?}", (vec![6, 1, 2, 4, 5, 8], 3)),
        copy_evens_solution(vec![6, 1, 2, 4, 5, 8], 3).into(),
        copy_evens(vec![6, 1, 2, 4, 5, 8], 3).into(),
    );
    layout.print_test::<BatVec<i32>>(
        format!("{:?}", (vec![6, 1, 2, 4, 5, 8], 4)),
        copy_evens_solution(vec![6, 1, 2, 4, 5, 8], 4).into(),
        copy_evens(vec![6, 1, 2, 4, 5, 8], 4).into(),
    );
    layout.print_test::<BatVec<i32>>(
        format!("{:?}", (vec![3, 1, 4, 1, 5], 1)),
        copy_evens_solution(vec![3, 1, 4, 1, 5], 1).into(),
        copy_evens(vec![3, 1, 4, 1, 5], 1).into(),
    );
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![2], 1)), copy_evens_solution(vec![2], 1).into(), copy_evens(vec![2], 1).into());
    layout.print_test::<BatVec<i32>>(
        format!("{:?}", (vec![6, 2, 4, 8], 2)),
        copy_evens_solution(vec![6, 2, 4, 8], 2).into(),
        copy_evens(vec![6, 2, 4, 8], 2).into(),
    );
    layout.print_test::<BatVec<i32>>(
        format!("{:?}", (vec![6, 2, 4, 8], 3)),
        copy_evens_solution(vec![6, 2, 4, 8], 3).into(),
        copy_evens(vec![6, 2, 4, 8], 3).into(),
    );
    layout.print_test::<BatVec<i32>>(
        format!("{:?}", (vec![6, 2, 4, 8], 4)),
        copy_evens_solution(vec![6, 2, 4, 8], 4).into(),
        copy_evens(vec![6, 2, 4, 8], 4).into(),
    );
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 8, 4], 1)), copy_evens_solution(vec![1, 8, 4], 1).into(), copy_evens(vec![1, 8, 4], 1).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 8, 4], 2)), copy_evens_solution(vec![1, 8, 4], 2).into(), copy_evens(vec![1, 8, 4], 2).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![2, 8, 4], 2)), copy_evens_solution(vec![2, 8, 4], 2).into(), copy_evens(vec![2, 8, 4], 2).into());
    layout.print_footer();
}

pub fn copy_endy_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("copy_endy");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(
        format!("{:?}", (vec![9, 11, 90, 22, 6], 2)),
        copy_endy_solution(vec![9, 11, 90, 22, 6], 2).into(),
        copy_endy(vec![9, 11, 90, 22, 6], 2).into(),
    );
    layout.print_test::<BatVec<i32>>(
        format!("{:?}", (vec![9, 11, 90, 22, 6], 3)),
        copy_endy_solution(vec![9, 11, 90, 22, 6], 3).into(),
        copy_endy(vec![9, 11, 90, 22, 6], 3).into(),
    );
    layout.print_test::<BatVec<i32>>(
        format!("{:?}", (vec![12, 1, 1, 13, 0, 20], 2)),
        copy_endy_solution(vec![12, 1, 1, 13, 0, 20], 2).into(),
        copy_endy(vec![12, 1, 1, 13, 0, 20], 2).into(),
    );
    layout.print_test::<BatVec<i32>>(
        format!("{:?}", (vec![12, 1, 1, 13, 0, 20], 3)),
        copy_endy_solution(vec![12, 1, 1, 13, 0, 20], 3).into(),
        copy_endy(vec![12, 1, 1, 13, 0, 20], 3).into(),
    );
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![0], 1)), copy_endy_solution(vec![0], 1).into(), copy_endy(vec![0], 1).into());
    layout.print_test::<BatVec<i32>>(
        format!("{:?}", (vec![10, 11, 90], 2)),
        copy_endy_solution(vec![10, 11, 90], 2).into(),
        copy_endy(vec![10, 11, 90], 2).into(),
    );
    layout.print_test::<BatVec<i32>>(
        format!("{:?}", (vec![90, 22, 100], 2)),
        copy_endy_solution(vec![90, 22, 100], 2).into(),
        copy_endy(vec![90, 22, 100], 2).into(),
    );
    layout.print_test::<BatVec<i32>>(
        format!("{:?}", (vec![12, 11, 10, 89, 101, 4], 1)),
        copy_endy_solution(vec![12, 11, 10, 89, 101, 4], 1).into(),
        copy_endy(vec![12, 11, 10, 89, 101, 4], 1).into(),
    );
    layout.print_test::<BatVec<i32>>(
        format!("{:?}", (vec![13, 2, 2, 0], 2)),
        copy_endy_solution(vec![13, 2, 2, 0], 2).into(),
        copy_endy(vec![13, 2, 2, 0], 2).into(),
    );
    layout.print_test::<BatVec<i32>>(
        format!("{:?}", (vec![13, 2, 2, 0], 3)),
        copy_endy_solution(vec![13, 2, 2, 0], 3).into(),
        copy_endy(vec![13, 2, 2, 0], 3).into(),
    );
    layout.print_test::<BatVec<i32>>(
        format!("{:?}", (vec![13, 2, 13, 2, 0, 30], 2)),
        copy_endy_solution(vec![13, 2, 13, 2, 0, 30], 2).into(),
        copy_endy(vec![13, 2, 13, 2, 0, 30], 2).into(),
    );
    layout.print_test::<BatVec<i32>>(
        format!("{:?}", (vec![13, 2, 13, 2, 0, 30], 3)),
        copy_endy_solution(vec![13, 2, 13, 2, 0, 30], 3).into(),
        copy_endy(vec![13, 2, 13, 2, 0, 30], 3).into(),
    );
    layout.print_footer();
}

pub fn match_up_run() {
    let layout = TestDisplay {
        argument_width: 59,
        expected_width: 8,
        actual_width: 8,
    };
    layout.print_test_name_banner("match_up");
    layout.print_header();
    layout.print_test(
        format!("{:?}", (strvec!["aa", "bb", "cc"], strvec!["aaa", "xx", "bb"])),
        match_up_solution(strvec!["aa", "bb", "cc"], strvec!["aaa", "xx", "bb"]),
        match_up(strvec!["aa", "bb", "cc"], strvec!["aaa", "xx", "bb"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["aa", "bb", "cc"], strvec!["aaa", "b", "bb"])),
        match_up_solution(strvec!["aa", "bb", "cc"], strvec!["aaa", "b", "bb"]),
        match_up(strvec!["aa", "bb", "cc"], strvec!["aaa", "b", "bb"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["aa", "bb", "cc"], strvec!["", "", "ccc"])),
        match_up_solution(strvec!["aa", "bb", "cc"], strvec!["", "", "ccc"]),
        match_up(strvec!["aa", "bb", "cc"], strvec!["", "", "ccc"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["", "", "ccc"], strvec!["aa", "bb", "cc"])),
        match_up_solution(strvec!["", "", "ccc"], strvec!["aa", "bb", "cc"]),
        match_up(strvec!["", "", "ccc"], strvec!["aa", "bb", "cc"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["", "", ""], strvec!["", "bb", "cc"])),
        match_up_solution(strvec!["", "", ""], strvec!["", "bb", "cc"]),
        match_up(strvec!["", "", ""], strvec!["", "bb", "cc"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["aa", "bb", "cc"], strvec!["", "", ""])),
        match_up_solution(strvec!["aa", "bb", "cc"], strvec!["", "", ""]),
        match_up(strvec!["aa", "bb", "cc"], strvec!["", "", ""]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["aa", "", "ccc"], strvec!["", "bb", "cc"])),
        match_up_solution(strvec!["aa", "", "ccc"], strvec!["", "bb", "cc"]),
        match_up(strvec!["aa", "", "ccc"], strvec!["", "bb", "cc"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["x", "y", "z"], strvec!["y", "z", "x"])),
        match_up_solution(strvec!["x", "y", "z"], strvec!["y", "z", "x"]),
        match_up(strvec!["x", "y", "z"], strvec!["y", "z", "x"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["", "y", "z"], strvec!["", "y", "x"])),
        match_up_solution(strvec!["", "y", "z"], strvec!["", "y", "x"]),
        match_up(strvec!["", "y", "z"], strvec!["", "y", "x"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["x", "y", "z"], strvec!["xx", "yyy", "zzz"])),
        match_up_solution(strvec!["x", "y", "z"], strvec!["xx", "yyy", "zzz"]),
        match_up(strvec!["x", "y", "z"], strvec!["xx", "yyy", "zzz"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["x", "y", "z"], strvec!["xx", "yyy", ""])),
        match_up_solution(strvec!["x", "y", "z"], strvec!["xx", "yyy", ""]),
        match_up(strvec!["x", "y", "z"], strvec!["xx", "yyy", ""]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["b", "x", "y", "z"], strvec!["a", "xx", "yyy", "zzz"])),
        match_up_solution(strvec!["b", "x", "y", "z"], strvec!["a", "xx", "yyy", "zzz"]),
        match_up(strvec!["b", "x", "y", "z"], strvec!["a", "xx", "yyy", "zzz"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["aaa", "bb", "c"], strvec!["aaa", "xx", "bb"])),
        match_up_solution(strvec!["aaa", "bb", "c"], strvec!["aaa", "xx", "bb"]),
        match_up(strvec!["aaa", "bb", "c"], strvec!["aaa", "xx", "bb"]),
    );
    layout.print_footer();
}

pub fn score_up_run() {
    let layout = TestDisplay {
        argument_width: 64,
        expected_width: 8,
        actual_width: 8,
    };
    layout.print_test_name_banner("score_up");
    layout.print_header();
    layout.print_test(
        format!("{:?}", (strvec!["a", "a", "b", "b"], strvec!["a", "c", "b", "c"])),
        score_up_solution(strvec!["a", "a", "b", "b"], strvec!["a", "c", "b", "c"]),
        score_up(strvec!["a", "a", "b", "b"], strvec!["a", "c", "b", "c"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["a", "a", "b", "b"], strvec!["a", "a", "b", "c"])),
        score_up_solution(strvec!["a", "a", "b", "b"], strvec!["a", "a", "b", "c"]),
        score_up(strvec!["a", "a", "b", "b"], strvec!["a", "a", "b", "c"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["a", "a", "b", "b"], strvec!["a", "a", "b", "b"])),
        score_up_solution(strvec!["a", "a", "b", "b"], strvec!["a", "a", "b", "b"]),
        score_up(strvec!["a", "a", "b", "b"], strvec!["a", "a", "b", "b"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["a", "a", "b", "b"], strvec!["?", "c", "b", "?"])),
        score_up_solution(strvec!["a", "a", "b", "b"], strvec!["?", "c", "b", "?"]),
        score_up(strvec!["a", "a", "b", "b"], strvec!["?", "c", "b", "?"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["a", "a", "b", "b"], strvec!["?", "c", "?", "?"])),
        score_up_solution(strvec!["a", "a", "b", "b"], strvec!["?", "c", "?", "?"]),
        score_up(strvec!["a", "a", "b", "b"], strvec!["?", "c", "?", "?"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["a", "a", "b", "b"], strvec!["c", "?", "b", "b"])),
        score_up_solution(strvec!["a", "a", "b", "b"], strvec!["c", "?", "b", "b"]),
        score_up(strvec!["a", "a", "b", "b"], strvec!["c", "?", "b", "b"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["a", "a", "b", "b"], strvec!["c", "?", "b", "?"])),
        score_up_solution(strvec!["a", "a", "b", "b"], strvec!["c", "?", "b", "?"]),
        score_up(strvec!["a", "a", "b", "b"], strvec!["c", "?", "b", "?"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["a", "b", "c"], strvec!["a", "c", "b"])),
        score_up_solution(strvec!["a", "b", "c"], strvec!["a", "c", "b"]),
        score_up(strvec!["a", "b", "c"], strvec!["a", "c", "b"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["a", "a", "b", "b", "c", "c"], strvec!["a", "c", "a", "c", "a", "c"])),
        score_up_solution(strvec!["a", "a", "b", "b", "c", "c"], strvec!["a", "c", "a", "c", "a", "c"]),
        score_up(strvec!["a", "a", "b", "b", "c", "c"], strvec!["a", "c", "a", "c", "a", "c"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["a", "a", "b", "b", "c", "c"], strvec!["a", "c", "?", "?", "a", "c"])),
        score_up_solution(strvec!["a", "a", "b", "b", "c", "c"], strvec!["a", "c", "?", "?", "a", "c"]),
        score_up(strvec!["a", "a", "b", "b", "c", "c"], strvec!["a", "c", "?", "?", "a", "c"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["a", "a", "b", "b", "c", "c"], strvec!["a", "c", "?", "?", "c", "c"])),
        score_up_solution(strvec!["a", "a", "b", "b", "c", "c"], strvec!["a", "c", "?", "?", "c", "c"]),
        score_up(strvec!["a", "a", "b", "b", "c", "c"], strvec!["a", "c", "?", "?", "c", "c"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["a", "b", "c"], strvec!["a", "b", "c"])),
        score_up_solution(strvec!["a", "b", "c"], strvec!["a", "b", "c"]),
        score_up(strvec!["a", "b", "c"], strvec!["a", "b", "c"]),
    );
    layout.print_footer();
}

pub fn words_without_run() {
    let layout = TestDisplay {
        argument_width: 51,
        expected_width: 12,
        actual_width: 12,
    };
    layout.print_test_name_banner("words_without");
    layout.print_header();
    layout.print_test::<BatVec<String>>(
        format!("{:?}", (strvec!["a", "b", "c", "a"], "a")),
        words_without_solution(strvec!["a", "b", "c", "a"], "a").into(),
        words_without(strvec!["a", "b", "c", "a"], "a").into(),
    );
    layout.print_test::<BatVec<String>>(
        format!("{:?}", (strvec!["a", "b", "c", "a"], "b")),
        words_without_solution(strvec!["a", "b", "c", "a"], "b").into(),
        words_without(strvec!["a", "b", "c", "a"], "b").into(),
    );
    layout.print_test::<BatVec<String>>(
        format!("{:?}", (strvec!["a", "b", "c", "a"], "c")),
        words_without_solution(strvec!["a", "b", "c", "a"], "c").into(),
        words_without(strvec!["a", "b", "c", "a"], "c").into(),
    );
    layout.print_test::<BatVec<String>>(
        format!("{:?}", (strvec!["b", "c", "a", "a"], "b")),
        words_without_solution(strvec!["b", "c", "a", "a"], "b").into(),
        words_without(strvec!["b", "c", "a", "a"], "b").into(),
    );
    layout.print_test::<BatVec<String>>(
        format!("{:?}", (strvec!["xx", "yyy", "x", "yy", "x"], "x")),
        words_without_solution(strvec!["xx", "yyy", "x", "yy", "x"], "x").into(),
        words_without(strvec!["xx", "yyy", "x", "yy", "x"], "x").into(),
    );
    layout.print_test::<BatVec<String>>(
        format!("{:?}", (strvec!["xx", "yyy", "x", "yy", "x"], "yy")),
        words_without_solution(strvec!["xx", "yyy", "x", "yy", "x"], "yy").into(),
        words_without(strvec!["xx", "yyy", "x", "yy", "x"], "yy").into(),
    );
    layout.print_test::<BatVec<String>>(
        format!("{:?}", (strvec!["aa", "ab", "ac", "aa"], "aa")),
        words_without_solution(strvec!["aa", "ab", "ac", "aa"], "aa").into(),
        words_without(strvec!["aa", "ab", "ac", "aa"], "aa").into(),
    );
    layout.print_footer();
}

pub fn scores_special_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("scores_special");
    layout.print_header();
    layout.print_test(
        format!("{:?}", (vec![12, 10, 4], vec![2, 20, 30])),
        scores_special_solution(vec![12, 10, 4], vec![2, 20, 30]),
        scores_special(vec![12, 10, 4], vec![2, 20, 30]),
    );
    layout.print_test(
        format!("{:?}", (vec![20, 10, 4], vec![2, 20, 10])),
        scores_special_solution(vec![20, 10, 4], vec![2, 20, 10]),
        scores_special(vec![20, 10, 4], vec![2, 20, 10]),
    );
    layout.print_test(
        format!("{:?}", (vec![12, 11, 4], vec![2, 20, 31])),
        scores_special_solution(vec![12, 11, 4], vec![2, 20, 31]),
        scores_special(vec![12, 11, 4], vec![2, 20, 31]),
    );
    layout.print_test(
        format!("{:?}", (vec![1, 20, 2, 50], vec![3, 4, 5])),
        scores_special_solution(vec![1, 20, 2, 50], vec![3, 4, 5]),
        scores_special(vec![1, 20, 2, 50], vec![3, 4, 5]),
    );
    layout.print_test(
        format!("{:?}", (vec![3, 4, 5], vec![1, 50, 2, 20])),
        scores_special_solution(vec![3, 4, 5], vec![1, 50, 2, 20]),
        scores_special(vec![3, 4, 5], vec![1, 50, 2, 20]),
    );
    layout.print_test(
        format!("{:?}", (vec![10, 4, 20, 30], vec![20])),
        scores_special_solution(vec![10, 4, 20, 30], vec![20]),
        scores_special(vec![10, 4, 20, 30], vec![20]),
    );
    layout.print_test(
        format!("{:?}", (vec![10, 4, 20, 30], vec![20])),
        scores_special_solution(vec![10, 4, 20, 30], vec![20]),
        scores_special(vec![10, 4, 20, 30], vec![20]),
    );
    layout.print_test(
        format!("{:?}", (vec![10, 4, 20, 30], vec![3, 20, 99])),
        scores_special_solution(vec![10, 4, 20, 30], vec![3, 20, 99]),
        scores_special(vec![10, 4, 20, 30], vec![3, 20, 99]),
    );
    layout.print_test(
        format!("{:?}", (vec![10, 4, 20, 30], vec![30, 20, 99])),
        scores_special_solution(vec![10, 4, 20, 30], vec![30, 20, 99]),
        scores_special(vec![10, 4, 20, 30], vec![30, 20, 99]),
    );
    layout.print_test(
        format!("{:?}", (vec![0; 0], vec![2])),
        scores_special_solution(vec![0; 0], vec![2]),
        scores_special(vec![0; 0], vec![2]),
    );
    layout.print_test(
        format!("{:?}", (vec![0; 0], vec![20])),
        scores_special_solution(vec![0; 0], vec![20]),
        scores_special(vec![0; 0], vec![20]),
    );
    layout.print_test(
        format!("{:?}", (vec![14, 10, 4], vec![4, 20, 30])),
        scores_special_solution(vec![14, 10, 4], vec![4, 20, 30]),
        scores_special(vec![14, 10, 4], vec![4, 20, 30]),
    );
    layout.print_footer();
}

pub fn sum_heights_run() {
    let layout = TestDisplay {
        argument_width: 59,
        expected_width: 8,
        actual_width: 8,
    };
    layout.print_test_name_banner("sum_heights");
    layout.print_header();
    layout.print_test(
        format!("{:?}", (vec![5, 3, 6, 7, 2], 2, 4)),
        sum_heights_solution(vec![5, 3, 6, 7, 2], 2, 4),
        sum_heights(vec![5, 3, 6, 7, 2], 2, 4),
    );
    layout.print_test(
        format!("{:?}", (vec![5, 3, 6, 7, 2], 0, 1)),
        sum_heights_solution(vec![5, 3, 6, 7, 2], 0, 1),
        sum_heights(vec![5, 3, 6, 7, 2], 0, 1),
    );
    layout.print_test(
        format!("{:?}", (vec![5, 3, 6, 7, 2], 0, 4)),
        sum_heights_solution(vec![5, 3, 6, 7, 2], 0, 4),
        sum_heights(vec![5, 3, 6, 7, 2], 0, 4),
    );
    layout.print_test(
        format!("{:?}", (vec![5, 3, 6, 7, 2], 1, 1)),
        sum_heights_solution(vec![5, 3, 6, 7, 2], 1, 1),
        sum_heights(vec![5, 3, 6, 7, 2], 1, 1),
    );
    layout.print_test(
        format!("{:?}", (vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 0, 3)),
        sum_heights_solution(vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 0, 3),
        sum_heights(vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 0, 3),
    );
    layout.print_test(
        format!("{:?}", (vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 4, 8)),
        sum_heights_solution(vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 4, 8),
        sum_heights(vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 4, 8),
    );
    layout.print_test(
        format!("{:?}", (vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 7, 8)),
        sum_heights_solution(vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 7, 8),
        sum_heights(vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 7, 8),
    );
    layout.print_test(
        format!("{:?}", (vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 8, 8)),
        sum_heights_solution(vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 8, 8),
        sum_heights(vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 8, 8),
    );
    layout.print_test(
        format!("{:?}", (vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 2, 2)),
        sum_heights_solution(vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 2, 2),
        sum_heights(vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 2, 2),
    );
    layout.print_test(
        format!("{:?}", (vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 3, 6)),
        sum_heights_solution(vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 3, 6),
        sum_heights(vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 3, 6),
    );
    layout.print_test(
        format!("{:?}", (vec![10, 8, 7, 7, 7, 6, 7], 1, 4)),
        sum_heights_solution(vec![10, 8, 7, 7, 7, 6, 7], 1, 4),
        sum_heights(vec![10, 8, 7, 7, 7, 6, 7], 1, 4),
    );
    layout.print_test(
        format!("{:?}", (vec![10, 8, 7, 7, 7, 6, 7], 1, 5)),
        sum_heights_solution(vec![10, 8, 7, 7, 7, 6, 7], 1, 5),
        sum_heights(vec![10, 8, 7, 7, 7, 6, 7], 1, 5),
    );
    layout.print_footer();
}

pub fn sum_heights2_run() {
    let layout = TestDisplay {
        argument_width: 59,
        expected_width: 8,
        actual_width: 8,
    };
    layout.print_test_name_banner("sum_heights2");
    layout.print_header();
    layout.print_test(
        format!("{:?}", (vec![5, 3, 6, 7, 2], 2, 4)),
        sum_heights2_solution(vec![5, 3, 6, 7, 2], 2, 4),
        sum_heights2(vec![5, 3, 6, 7, 2], 2, 4),
    );
    layout.print_test(
        format!("{:?}", (vec![5, 3, 6, 7, 2], 0, 1)),
        sum_heights2_solution(vec![5, 3, 6, 7, 2], 0, 1),
        sum_heights2(vec![5, 3, 6, 7, 2], 0, 1),
    );
    layout.print_test(
        format!("{:?}", (vec![5, 3, 6, 7, 2], 0, 4)),
        sum_heights2_solution(vec![5, 3, 6, 7, 2], 0, 4),
        sum_heights2(vec![5, 3, 6, 7, 2], 0, 4),
    );
    layout.print_test(
        format!("{:?}", (vec![5, 3, 6, 7, 2], 1, 1)),
        sum_heights2_solution(vec![5, 3, 6, 7, 2], 1, 1),
        sum_heights2(vec![5, 3, 6, 7, 2], 1, 1),
    );
    layout.print_test(
        format!("{:?}", (vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 0, 3)),
        sum_heights2_solution(vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 0, 3),
        sum_heights2(vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 0, 3),
    );
    layout.print_test(
        format!("{:?}", (vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 4, 8)),
        sum_heights2_solution(vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 4, 8),
        sum_heights2(vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 4, 8),
    );
    layout.print_test(
        format!("{:?}", (vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 7, 8)),
        sum_heights2_solution(vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 7, 8),
        sum_heights2(vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 7, 8),
    );
    layout.print_test(
        format!("{:?}", (vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 8, 8)),
        sum_heights2_solution(vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 8, 8),
        sum_heights2(vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 8, 8),
    );
    layout.print_test(
        format!("{:?}", (vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 2, 2)),
        sum_heights2_solution(vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 2, 2),
        sum_heights2(vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 2, 2),
    );
    layout.print_test(
        format!("{:?}", (vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 3, 6)),
        sum_heights2_solution(vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 3, 6),
        sum_heights2(vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 3, 6),
    );
    layout.print_test(
        format!("{:?}", (vec![10, 8, 7, 7, 7, 6, 7], 1, 4)),
        sum_heights2_solution(vec![10, 8, 7, 7, 7, 6, 7], 1, 4),
        sum_heights2(vec![10, 8, 7, 7, 7, 6, 7], 1, 4),
    );
    layout.print_test(
        format!("{:?}", (vec![10, 8, 7, 7, 7, 6, 7], 1, 5)),
        sum_heights2_solution(vec![10, 8, 7, 7, 7, 6, 7], 1, 5),
        sum_heights2(vec![10, 8, 7, 7, 7, 6, 7], 1, 5),
    );
    layout.print_footer();
}

pub fn big_heights_run() {
    let layout = TestDisplay {
        argument_width: 59,
        expected_width: 8,
        actual_width: 8,
    };
    layout.print_test_name_banner("big_heights");
    layout.print_header();
    layout.print_test(
        format!("{:?}", (vec![5, 3, 6, 7, 2], 2, 4)),
        big_heights_solution(vec![5, 3, 6, 7, 2], 2, 4),
        big_heights(vec![5, 3, 6, 7, 2], 2, 4),
    );
    layout.print_test(
        format!("{:?}", (vec![5, 3, 6, 7, 2], 0, 1)),
        big_heights_solution(vec![5, 3, 6, 7, 2], 0, 1),
        big_heights(vec![5, 3, 6, 7, 2], 0, 1),
    );
    layout.print_test(
        format!("{:?}", (vec![5, 3, 6, 7, 2], 0, 4)),
        big_heights_solution(vec![5, 3, 6, 7, 2], 0, 4),
        big_heights(vec![5, 3, 6, 7, 2], 0, 4),
    );
    layout.print_test(
        format!("{:?}", (vec![5, 3, 6, 7, 3], 0, 4)),
        big_heights_solution(vec![5, 3, 6, 7, 3], 0, 4),
        big_heights(vec![5, 3, 6, 7, 3], 0, 4),
    );
    layout.print_test(
        format!("{:?}", (vec![5, 3, 6, 7, 2], 1, 1)),
        big_heights_solution(vec![5, 3, 6, 7, 2], 1, 1),
        big_heights(vec![5, 3, 6, 7, 2], 1, 1),
    );
    layout.print_test(
        format!("{:?}", (vec![5, 13, 6, 7, 2], 1, 2)),
        big_heights_solution(vec![5, 13, 6, 7, 2], 1, 2),
        big_heights(vec![5, 13, 6, 7, 2], 1, 2),
    );
    layout.print_test(
        format!("{:?}", (vec![5, 13, 6, 7, 2], 0, 2)),
        big_heights_solution(vec![5, 13, 6, 7, 2], 0, 2),
        big_heights(vec![5, 13, 6, 7, 2], 0, 2),
    );
    layout.print_test(
        format!("{:?}", (vec![5, 13, 6, 7, 2], 1, 4)),
        big_heights_solution(vec![5, 13, 6, 7, 2], 1, 4),
        big_heights(vec![5, 13, 6, 7, 2], 1, 4),
    );
    layout.print_test(
        format!("{:?}", (vec![5, 13, 6, 7, 2], 0, 4)),
        big_heights_solution(vec![5, 13, 6, 7, 2], 0, 4),
        big_heights(vec![5, 13, 6, 7, 2], 0, 4),
    );
    layout.print_test(
        format!("{:?}", (vec![5, 13, 6, 7, 2], 0, 3)),
        big_heights_solution(vec![5, 13, 6, 7, 2], 0, 3),
        big_heights(vec![5, 13, 6, 7, 2], 0, 3),
    );
    layout.print_test(
        format!("{:?}", (vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 0, 3)),
        big_heights_solution(vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 0, 3),
        big_heights(vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 0, 3),
    );
    layout.print_test(
        format!("{:?}", (vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 4, 8)),
        big_heights_solution(vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 4, 8),
        big_heights(vec![1, 2, 3, 4, 5, 4, 3, 2, 10], 4, 8),
    );
    layout.print_test(
        format!("{:?}", (vec![1, 2, 3, 14, 5, 4, 3, 2, 10], 0, 3)),
        big_heights_solution(vec![1, 2, 3, 14, 5, 4, 3, 2, 10], 0, 3),
        big_heights(vec![1, 2, 3, 14, 5, 4, 3, 2, 10], 0, 3),
    );
    layout.print_test(
        format!("{:?}", (vec![1, 2, 3, 14, 5, 4, 3, 2, 10], 7, 8)),
        big_heights_solution(vec![1, 2, 3, 14, 5, 4, 3, 2, 10], 7, 8),
        big_heights(vec![1, 2, 3, 14, 5, 4, 3, 2, 10], 7, 8),
    );
    layout.print_test(
        format!("{:?}", (vec![1, 2, 3, 14, 5, 4, 3, 2, 10], 3, 8)),
        big_heights_solution(vec![1, 2, 3, 14, 5, 4, 3, 2, 10], 3, 8),
        big_heights(vec![1, 2, 3, 14, 5, 4, 3, 2, 10], 3, 8),
    );
    layout.print_test(
        format!("{:?}", (vec![1, 2, 3, 14, 5, 4, 3, 2, 10], 2, 8)),
        big_heights_solution(vec![1, 2, 3, 14, 5, 4, 3, 2, 10], 2, 8),
        big_heights(vec![1, 2, 3, 14, 5, 4, 3, 2, 10], 2, 8),
    );
    layout.print_footer();
}

pub fn user_compare_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("user_compare");
    layout.print_header();
    layout.print_test(
        format!("{:?}", ("bb", 1, "zz", 2)),
        user_compare_solution(User::new("bb", 1), User::new("zz", 2)),
        user_compare(User::new("bb", 1), User::new("zz", 2)),
    );
    layout.print_test(
        format!("{:?}", ("bb", 1, "aa", 2)),
        user_compare_solution(User::new("bb", 1), User::new("aa", 2)),
        user_compare(User::new("bb", 1), User::new("aa", 2)),
    );
    layout.print_test(
        format!("{:?}", ("bb", 1, "bb", 1)),
        user_compare_solution(User::new("bb", 1), User::new("bb", 1)),
        user_compare(User::new("bb", 1), User::new("bb", 1)),
    );
    layout.print_test(
        format!("{:?}", ("bb", 5, "bb", 1)),
        user_compare_solution(User::new("bb", 5), User::new("bb", 1)),
        user_compare(User::new("bb", 5), User::new("bb", 1)),
    );
    layout.print_test(
        format!("{:?}", ("bb", 5, "bb", 10)),
        user_compare_solution(User::new("bb", 5), User::new("bb", 10)),
        user_compare(User::new("bb", 5), User::new("bb", 10)),
    );
    layout.print_test(
        format!("{:?}", ("adam", 1, "bob", 2)),
        user_compare_solution(User::new("adam", 1), User::new("bob", 2)),
        user_compare(User::new("adam", 1), User::new("bob", 2)),
    );
    layout.print_test(
        format!("{:?}", ("bob", 1, "bob", 2)),
        user_compare_solution(User::new("bob", 1), User::new("bob", 2)),
        user_compare(User::new("bob", 1), User::new("bob", 2)),
    );
    layout.print_test(
        format!("{:?}", ("bzb", 1, "bob", 2)),
        user_compare_solution(User::new("bzb", 1), User::new("bob", 2)),
        user_compare(User::new("bzb", 1), User::new("bob", 2)),
    );
    layout.print_footer();
}

pub fn merge_two_run() {
    let layout = TestDisplay {
        argument_width: 59,
        expected_width: 8,
        actual_width: 8,
    };
    layout.print_test_name_banner("merge_two");
    layout.print_header();
    layout.print_test::<BatVec<String>>(
        format!("{:?}", (strvec!["a", "c", "z"], strvec!["b", "f", "z"], 3)),
        merge_two_solution(strvec!["a", "c", "z"], strvec!["b", "f", "z"], 3).into(),
        merge_two(strvec!["a", "c", "z"], strvec!["b", "f", "z"], 3).into(),
    );
    layout.print_test::<BatVec<String>>(
        format!("{:?}", (strvec!["a", "c", "z"], strvec!["c", "f", "z"], 3)),
        merge_two_solution(strvec!["a", "c", "z"], strvec!["c", "f", "z"], 3).into(),
        merge_two(strvec!["a", "c", "z"], strvec!["c", "f", "z"], 3).into(),
    );
    layout.print_test::<BatVec<String>>(
        format!("{:?}", (strvec!["f", "g", "z"], strvec!["c", "f", "g"], 3)),
        merge_two_solution(strvec!["f", "g", "z"], strvec!["c", "f", "g"], 3).into(),
        merge_two(strvec!["f", "g", "z"], strvec!["c", "f", "g"], 3).into(),
    );
    layout.print_test::<BatVec<String>>(
        format!("{:?}", (strvec!["a", "c", "z"], strvec!["a", "c", "z"], 3)),
        merge_two_solution(strvec!["a", "c", "z"], strvec!["a", "c", "z"], 3).into(),
        merge_two(strvec!["a", "c", "z"], strvec!["a", "c", "z"], 3).into(),
    );
    layout.print_test::<BatVec<String>>(
        format!("{:?}", (strvec!["a", "b", "c", "z"], strvec!["a", "c", "z"], 3)),
        merge_two_solution(strvec!["a", "b", "c", "z"], strvec!["a", "c", "z"], 3).into(),
        merge_two(strvec!["a", "b", "c", "z"], strvec!["a", "c", "z"], 3).into(),
    );
    layout.print_test::<BatVec<String>>(
        format!("{:?}", (strvec!["a", "c", "z"], strvec!["a", "b", "c", "z"], 3)),
        merge_two_solution(strvec!["a", "c", "z"], strvec!["a", "b", "c", "z"], 3).into(),
        merge_two(strvec!["a", "c", "z"], strvec!["a", "b", "c", "z"], 3).into(),
    );
    layout.print_test::<BatVec<String>>(
        format!("{:?}", (strvec!["a", "c", "z"], strvec!["a", "c", "z"], 2)),
        merge_two_solution(strvec!["a", "c", "z"], strvec!["a", "c", "z"], 2).into(),
        merge_two(strvec!["a", "c", "z"], strvec!["a", "c", "z"], 2).into(),
    );
    layout.print_test::<BatVec<String>>(
        format!("{:?}", (strvec!["a", "c", "z"], strvec!["a", "c", "y", "z"], 3)),
        merge_two_solution(strvec!["a", "c", "z"], strvec!["a", "c", "y", "z"], 3).into(),
        merge_two(strvec!["a", "c", "z"], strvec!["a", "c", "y", "z"], 3).into(),
    );
    layout.print_test::<BatVec<String>>(
        format!("{:?}", (strvec!["x", "y", "z"], strvec!["a", "b", "z"], 3)),
        merge_two_solution(strvec!["x", "y", "z"], strvec!["a", "b", "z"], 3).into(),
        merge_two(strvec!["x", "y", "z"], strvec!["a", "b", "z"], 3).into(),
    );
    layout.print_footer();
}

pub fn common_two_run() {
    let layout = TestDisplay {
        argument_width: 59,
        expected_width: 8,
        actual_width: 8,
    };
    layout.print_test_name_banner("common_two");
    layout.print_header();
    layout.print_test(
        format!("{:?}", (strvec!["a", "c", "x"], strvec!["b", "c", "d", "x"])),
        common_two_solution(strvec!["a", "c", "x"], strvec!["b", "c", "d", "x"]),
        common_two(strvec!["a", "c", "x"], strvec!["b", "c", "d", "x"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["a", "c", "x"], strvec!["a", "b", "c", "x", "z"])),
        common_two_solution(strvec!["a", "c", "x"], strvec!["a", "b", "c", "x", "z"]),
        common_two(strvec!["a", "c", "x"], strvec!["a", "b", "c", "x", "z"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["a", "b", "c"], strvec!["a", "b", "c"])),
        common_two_solution(strvec!["a", "b", "c"], strvec!["a", "b", "c"]),
        common_two(strvec!["a", "b", "c"], strvec!["a", "b", "c"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["a", "a", "b", "b", "c"], strvec!["a", "b", "c"])),
        common_two_solution(strvec!["a", "a", "b", "b", "c"], strvec!["a", "b", "c"]),
        common_two(strvec!["a", "a", "b", "b", "c"], strvec!["a", "b", "c"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["a", "a", "b", "b", "c"], strvec!["a", "b", "b", "b", "c"])),
        common_two_solution(strvec!["a", "a", "b", "b", "c"], strvec!["a", "b", "b", "b", "c"]),
        common_two(strvec!["a", "a", "b", "b", "c"], strvec!["a", "b", "b", "b", "c"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["a", "a", "b", "b", "c"], strvec!["a", "b", "b", "c", "c"])),
        common_two_solution(strvec!["a", "a", "b", "b", "c"], strvec!["a", "b", "b", "c", "c"]),
        common_two(strvec!["a", "a", "b", "b", "c"], strvec!["a", "b", "b", "c", "c"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["b", "b", "b", "b", "c"], strvec!["a", "b", "b", "b", "c"])),
        common_two_solution(strvec!["b", "b", "b", "b", "c"], strvec!["a", "b", "b", "b", "c"]),
        common_two(strvec!["b", "b", "b", "b", "c"], strvec!["a", "b", "b", "b", "c"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["a", "b", "c", "c", "d"], strvec!["a", "b", "b", "c", "d", "d"])),
        common_two_solution(strvec!["a", "b", "c", "c", "d"], strvec!["a", "b", "b", "c", "d", "d"]),
        common_two(strvec!["a", "b", "c", "c", "d"], strvec!["a", "b", "b", "c", "d", "d"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["a", "a", "b", "b", "c"], strvec!["b", "b", "b"])),
        common_two_solution(strvec!["a", "a", "b", "b", "c"], strvec!["b", "b", "b"]),
        common_two(strvec!["a", "a", "b", "b", "c"], strvec!["b", "b", "b"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["a", "a", "b", "b", "c"], strvec!["c", "c"])),
        common_two_solution(strvec!["a", "a", "b", "b", "c"], strvec!["c", "c"]),
        common_two(strvec!["a", "a", "b", "b", "c"], strvec!["c", "c"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["a", "a", "b", "b", "c"], strvec!["b", "b", "b", "x"])),
        common_two_solution(strvec!["a", "a", "b", "b", "c"], strvec!["b", "b", "b", "x"]),
        common_two(strvec!["a", "a", "b", "b", "c"], strvec!["b", "b", "b", "x"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["a", "a", "b", "b", "c"], strvec!["b", "b"])),
        common_two_solution(strvec!["a", "a", "b", "b", "c"], strvec!["b", "b"]),
        common_two(strvec!["a", "a", "b", "b", "c"], strvec!["b", "b"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["a"], strvec!["a", "b"])),
        common_two_solution(strvec!["a"], strvec!["a", "b"]),
        common_two(strvec!["a"], strvec!["a", "b"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["a"], strvec!["b"])),
        common_two_solution(strvec!["a"], strvec!["b"]),
        common_two(strvec!["a"], strvec!["b"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["a", "a"], strvec!["b", "b"])),
        common_two_solution(strvec!["a", "a"], strvec!["b", "b"]),
        common_two(strvec!["a", "a"], strvec!["b", "b"]),
    );
    layout.print_test(
        format!("{:?}", (strvec!["a", "b"], strvec!["a", "b"])),
        common_two_solution(strvec!["a", "b"], strvec!["a", "b"]),
        common_two(strvec!["a", "b"], strvec!["a", "b"]),
    );
    layout.print_footer();
}
