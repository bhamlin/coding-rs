use super::implementations::*;
use super::solutions::*;
use crate::display::TestDisplay;

pub fn run(test: Option<Vec<String>>) {
    match test {
        // None => println!("Please specify a test name"),
        None => {
            sleep_in_run();
            monkey_trouble_run();
            sum_double_run();
            diff21_run();
            parrot_trouble_run();
            makes10_run();
            near_hundred_run();
            pos_neg_run();
            not_string_run();
            missing_char_run();
            front_back_run();
            front3_run();
            back_around_run();
            or35_run();
            front22_run();
            start_hi_run();
            icy_hot_run();
            in1020_run();
            has_teen_run();
            lone_teen_run();
            del_del_run();
            mix_start_run();
            start_oz_run();
            int_max_run();
            close10_run();
            in3050_run();
            max1020_run();
            string_e_run();
            last_digit_run();
            end_up_run();
            every_nth_run();
            string_times_run();
            front_times_run();
            count_x_x_run();
            double_x_run();
            string_bits_run();
            last2_run();
            array_count9_run();
            array_front9_run();
            string_match_run();
            string_x_run();
            alt_pairs_run();
            string_yak_run();
            array667_run();
            no_triples_run();
            has271_run();
        }
        Some(tests) => {
            for test_name in tests {
                match test_name.to_ascii_lowercase().as_str() {
                    "sleepIn" | "sleep_in" => sleep_in_run(),
                    "monkeyTrouble" | "monkey_trouble" => monkey_trouble_run(),
                    "sumDouble" | "sum_double" => sum_double_run(),
                    "diff21" => diff21_run(),
                    "parrotTrouble" | "parrot_trouble" => parrot_trouble_run(),
                    "makes10" => makes10_run(),
                    "nearHundred" | "near_hundred" => near_hundred_run(),
                    "posNeg" | "pos_neg" => pos_neg_run(),
                    "notString" | "not_string" => not_string_run(),
                    "missingChar" | "missing_char" => missing_char_run(),
                    "frontBack" | "front_back" => front_back_run(),
                    "front3" => front3_run(),
                    "backAround" | "back_around" => back_around_run(),
                    "or35" => or35_run(),
                    "front22" => front22_run(),
                    "startHi" | "start_hi" => start_hi_run(),
                    "icyHot" | "icy_hot" => icy_hot_run(),
                    "in1020" => in1020_run(),
                    "hasTeen" | "has_teen" => has_teen_run(),
                    "loneTeen" | "lone_teen" => lone_teen_run(),
                    "delDel" | "del_del" => del_del_run(),
                    "mixStart" | "mix_start" => mix_start_run(),
                    "startOz" | "start_oz" => start_oz_run(),
                    "intMax" | "int_max" => int_max_run(),
                    "close10" => close10_run(),
                    "in3050" => in3050_run(),
                    "max1020" => max1020_run(),
                    "stringE" | "string_e" => string_e_run(),
                    "lastDigit" | "last_digit" => last_digit_run(),
                    "endUp" | "end_up" => end_up_run(),
                    "everyNth" | "every_nth" => every_nth_run(),
                    "stringTimes" | "string_times" => string_times_run(),
                    "frontTimes" | "front_times" => front_times_run(),
                    "countXX" | "count_x_x" => count_x_x_run(),
                    "doubleX" | "double_x" => double_x_run(),
                    "stringBits" | "string_bits" => string_bits_run(),
                    "last2" => last2_run(),
                    "arrayCount9" | "array_count9" => array_count9_run(),
                    "arrayFront9" | "array_front9" => array_front9_run(),
                    "stringMatch" | "string_match" => string_match_run(),
                    "stringX" | "string_x" => string_x_run(),
                    "altPairs" | "alt_pairs" => alt_pairs_run(),
                    "stringYak" | "string_yak" => string_yak_run(),
                    "array667" => array667_run(),
                    "noTriples" | "no_triples" => no_triples_run(),
                    "has271" => has271_run(),
                    _ => println!("No test named {test_name}"),
                };
            }
        }
    };
}

pub fn sleep_in_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("sleep_in");
    layout.print_header();
    layout.print_test(format!("{:?}", (true, true)), sleep_in_solution(true, true), sleep_in(true, true));
    layout.print_test(format!("{:?}", (true, false)), sleep_in_solution(true, false), sleep_in(true, false));
    layout.print_test(format!("{:?}", (false, true)), sleep_in_solution(false, true), sleep_in(false, true));
    layout.print_test(format!("{:?}", (false, false)), sleep_in_solution(false, false), sleep_in(false, false));
    layout.print_footer();
}

pub fn monkey_trouble_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("monkey_trouble");
    layout.print_header();
    layout.print_test(format!("{:?}", (true, true)), monkey_trouble_solution(true, true), monkey_trouble(true, true));
    layout.print_test(format!("{:?}", (false, false)), monkey_trouble_solution(false, false), monkey_trouble(false, false));
    layout.print_test(format!("{:?}", (true, false)), monkey_trouble_solution(true, false), monkey_trouble(true, false));
    layout.print_test(format!("{:?}", (false, true)), monkey_trouble_solution(false, true), monkey_trouble(false, true));
    layout.print_footer();
}

pub fn sum_double_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("sum_double");
    layout.print_header();
    layout.print_test(format!("{:?}", (1, 2)), sum_double_solution(1, 2), sum_double(1, 2));
    layout.print_test(format!("{:?}", (3, 2)), sum_double_solution(3, 2), sum_double(3, 2));
    layout.print_test(format!("{:?}", (2, 2)), sum_double_solution(2, 2), sum_double(2, 2));
    layout.print_test(format!("{:?}", (-1, 0)), sum_double_solution(-1, 0), sum_double(-1, 0));
    layout.print_test(format!("{:?}", (3, 3)), sum_double_solution(3, 3), sum_double(3, 3));
    layout.print_test(format!("{:?}", (0, 0)), sum_double_solution(0, 0), sum_double(0, 0));
    layout.print_test(format!("{:?}", (0, 1)), sum_double_solution(0, 1), sum_double(0, 1));
    layout.print_test(format!("{:?}", (3, 4)), sum_double_solution(3, 4), sum_double(3, 4));
    layout.print_footer();
}

pub fn diff21_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("diff21");
    layout.print_header();
    layout.print_test(format!("{:?}", (19)), diff21_solution(19), diff21(19));
    layout.print_test(format!("{:?}", (10)), diff21_solution(10), diff21(10));
    layout.print_test(format!("{:?}", (21)), diff21_solution(21), diff21(21));
    layout.print_test(format!("{:?}", (22)), diff21_solution(22), diff21(22));
    layout.print_test(format!("{:?}", (25)), diff21_solution(25), diff21(25));
    layout.print_test(format!("{:?}", (30)), diff21_solution(30), diff21(30));
    layout.print_test(format!("{:?}", (0)), diff21_solution(0), diff21(0));
    layout.print_test(format!("{:?}", (1)), diff21_solution(1), diff21(1));
    layout.print_test(format!("{:?}", (2)), diff21_solution(2), diff21(2));
    layout.print_test(format!("{:?}", (-1)), diff21_solution(-1), diff21(-1));
    layout.print_test(format!("{:?}", (-2)), diff21_solution(-2), diff21(-2));
    layout.print_test(format!("{:?}", (50)), diff21_solution(50), diff21(50));
    layout.print_footer();
}

pub fn parrot_trouble_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("parrot_trouble");
    layout.print_header();
    layout.print_test(format!("{:?}", (true, 6)), parrot_trouble_solution(true, 6), parrot_trouble(true, 6));
    layout.print_test(format!("{:?}", (true, 7)), parrot_trouble_solution(true, 7), parrot_trouble(true, 7));
    layout.print_test(format!("{:?}", (false, 6)), parrot_trouble_solution(false, 6), parrot_trouble(false, 6));
    layout.print_test(format!("{:?}", (true, 21)), parrot_trouble_solution(true, 21), parrot_trouble(true, 21));
    layout.print_test(format!("{:?}", (false, 21)), parrot_trouble_solution(false, 21), parrot_trouble(false, 21));
    layout.print_test(format!("{:?}", (true, 23)), parrot_trouble_solution(true, 23), parrot_trouble(true, 23));
    layout.print_test(format!("{:?}", (false, 23)), parrot_trouble_solution(false, 23), parrot_trouble(false, 23));
    layout.print_test(format!("{:?}", (true, 20)), parrot_trouble_solution(true, 20), parrot_trouble(true, 20));
    layout.print_test(format!("{:?}", (false, 12)), parrot_trouble_solution(false, 12), parrot_trouble(false, 12));
    layout.print_footer();
}

pub fn makes10_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("makes10");
    layout.print_header();
    layout.print_test(format!("{:?}", (9, 10)), makes10_solution(9, 10), makes10(9, 10));
    layout.print_test(format!("{:?}", (9, 9)), makes10_solution(9, 9), makes10(9, 9));
    layout.print_test(format!("{:?}", (1, 9)), makes10_solution(1, 9), makes10(1, 9));
    layout.print_test(format!("{:?}", (10, 1)), makes10_solution(10, 1), makes10(10, 1));
    layout.print_test(format!("{:?}", (10, 10)), makes10_solution(10, 10), makes10(10, 10));
    layout.print_test(format!("{:?}", (8, 2)), makes10_solution(8, 2), makes10(8, 2));
    layout.print_test(format!("{:?}", (8, 3)), makes10_solution(8, 3), makes10(8, 3));
    layout.print_test(format!("{:?}", (10, 42)), makes10_solution(10, 42), makes10(10, 42));
    layout.print_test(format!("{:?}", (12, -2)), makes10_solution(12, -2), makes10(12, -2));
    layout.print_footer();
}

pub fn near_hundred_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("near_hundred");
    layout.print_header();
    layout.print_test(format!("{:?}", (93)), near_hundred_solution(93), near_hundred(93));
    layout.print_test(format!("{:?}", (90)), near_hundred_solution(90), near_hundred(90));
    layout.print_test(format!("{:?}", (89)), near_hundred_solution(89), near_hundred(89));
    layout.print_test(format!("{:?}", (110)), near_hundred_solution(110), near_hundred(110));
    layout.print_test(format!("{:?}", (111)), near_hundred_solution(111), near_hundred(111));
    layout.print_test(format!("{:?}", (121)), near_hundred_solution(121), near_hundred(121));
    layout.print_test(format!("{:?}", (0)), near_hundred_solution(0), near_hundred(0));
    layout.print_test(format!("{:?}", (5)), near_hundred_solution(5), near_hundred(5));
    layout.print_test(format!("{:?}", (191)), near_hundred_solution(191), near_hundred(191));
    layout.print_test(format!("{:?}", (189)), near_hundred_solution(189), near_hundred(189));
    layout.print_footer();
}

pub fn pos_neg_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("pos_neg");
    layout.print_header();
    layout.print_test(format!("{:?}", (1, -1, false)), pos_neg_solution(1, -1, false), pos_neg(1, -1, false));
    layout.print_test(format!("{:?}", (-1, 1, false)), pos_neg_solution(-1, 1, false), pos_neg(-1, 1, false));
    layout.print_test(format!("{:?}", (-4, -5, true)), pos_neg_solution(-4, -5, true), pos_neg(-4, -5, true));
    layout.print_test(format!("{:?}", (-4, -5, false)), pos_neg_solution(-4, -5, false), pos_neg(-4, -5, false));
    layout.print_test(format!("{:?}", (-4, 5, false)), pos_neg_solution(-4, 5, false), pos_neg(-4, 5, false));
    layout.print_test(format!("{:?}", (-4, 5, true)), pos_neg_solution(-4, 5, true), pos_neg(-4, 5, true));
    layout.print_test(format!("{:?}", (1, 1, false)), pos_neg_solution(1, 1, false), pos_neg(1, 1, false));
    layout.print_test(format!("{:?}", (-1, -1, false)), pos_neg_solution(-1, -1, false), pos_neg(-1, -1, false));
    layout.print_test(format!("{:?}", (1, -1, true)), pos_neg_solution(1, -1, true), pos_neg(1, -1, true));
    layout.print_test(format!("{:?}", (-1, 1, true)), pos_neg_solution(-1, 1, true), pos_neg(-1, 1, true));
    layout.print_test(format!("{:?}", (1, 1, true)), pos_neg_solution(1, 1, true), pos_neg(1, 1, true));
    layout.print_test(format!("{:?}", (-1, -1, true)), pos_neg_solution(-1, -1, true), pos_neg(-1, -1, true));
    layout.print_test(format!("{:?}", (5, -5, false)), pos_neg_solution(5, -5, false), pos_neg(5, -5, false));
    layout.print_test(format!("{:?}", (-6, 6, false)), pos_neg_solution(-6, 6, false), pos_neg(-6, 6, false));
    layout.print_test(format!("{:?}", (-5, -6, false)), pos_neg_solution(-5, -6, false), pos_neg(-5, -6, false));
    layout.print_test(format!("{:?}", (-2, -1, false)), pos_neg_solution(-2, -1, false), pos_neg(-2, -1, false));
    layout.print_test(format!("{:?}", (1, 2, false)), pos_neg_solution(1, 2, false), pos_neg(1, 2, false));
    layout.print_test(format!("{:?}", (-5, 6, true)), pos_neg_solution(-5, 6, true), pos_neg(-5, 6, true));
    layout.print_test(format!("{:?}", (-5, -5, true)), pos_neg_solution(-5, -5, true), pos_neg(-5, -5, true));
    layout.print_footer();
}

pub fn not_string_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("not_string");
    layout.print_header();
    layout.print_test(format!("{:?}", ("candy")), not_string_solution("candy"), not_string("candy"));
    layout.print_test(format!("{:?}", ("x")), not_string_solution("x"), not_string("x"));
    layout.print_test(format!("{:?}", ("not bad")), not_string_solution("not bad"), not_string("not bad"));
    layout.print_test(format!("{:?}", ("bad")), not_string_solution("bad"), not_string("bad"));
    layout.print_test(format!("{:?}", ("not")), not_string_solution("not"), not_string("not"));
    layout.print_test(format!("{:?}", ("is not")), not_string_solution("is not"), not_string("is not"));
    layout.print_test(format!("{:?}", ("no")), not_string_solution("no"), not_string("no"));
    layout.print_footer();
}

pub fn missing_char_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("missing_char");
    layout.print_header();
    layout.print_test(format!("{:?}", ("kitten", 1)), missing_char_solution("kitten", 1), missing_char("kitten", 1));
    layout.print_test(format!("{:?}", ("kitten", 0)), missing_char_solution("kitten", 0), missing_char("kitten", 0));
    layout.print_test(format!("{:?}", ("kitten", 4)), missing_char_solution("kitten", 4), missing_char("kitten", 4));
    layout.print_test(format!("{:?}", ("Hi", 0)), missing_char_solution("Hi", 0), missing_char("Hi", 0));
    layout.print_test(format!("{:?}", ("Hi", 1)), missing_char_solution("Hi", 1), missing_char("Hi", 1));
    layout.print_test(format!("{:?}", ("code", 0)), missing_char_solution("code", 0), missing_char("code", 0));
    layout.print_test(format!("{:?}", ("code", 1)), missing_char_solution("code", 1), missing_char("code", 1));
    layout.print_test(format!("{:?}", ("code", 2)), missing_char_solution("code", 2), missing_char("code", 2));
    layout.print_test(format!("{:?}", ("code", 3)), missing_char_solution("code", 3), missing_char("code", 3));
    layout.print_test(format!("{:?}", ("chocolate", 8)), missing_char_solution("chocolate", 8), missing_char("chocolate", 8));
    layout.print_footer();
}

pub fn front_back_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("front_back");
    layout.print_header();
    layout.print_test(format!("{:?}", ("code")), front_back_solution("code"), front_back("code"));
    layout.print_test(format!("{:?}", ("a")), front_back_solution("a"), front_back("a"));
    layout.print_test(format!("{:?}", ("ab")), front_back_solution("ab"), front_back("ab"));
    layout.print_test(format!("{:?}", ("abc")), front_back_solution("abc"), front_back("abc"));
    layout.print_test(format!("{:?}", ("")), front_back_solution(""), front_back(""));
    layout.print_test(format!("{:?}", ("Chocolate")), front_back_solution("Chocolate"), front_back("Chocolate"));
    layout.print_test(format!("{:?}", ("aavj")), front_back_solution("aavj"), front_back("aavj"));
    layout.print_test(format!("{:?}", ("hello")), front_back_solution("hello"), front_back("hello"));
    layout.print_footer();
}

pub fn front3_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("front3");
    layout.print_header();
    layout.print_test(format!("{:?}", ("Java")), front3_solution("Java"), front3("Java"));
    layout.print_test(format!("{:?}", ("Chocolate")), front3_solution("Chocolate"), front3("Chocolate"));
    layout.print_test(format!("{:?}", ("abc")), front3_solution("abc"), front3("abc"));
    layout.print_test(format!("{:?}", ("abcXYZ")), front3_solution("abcXYZ"), front3("abcXYZ"));
    layout.print_test(format!("{:?}", ("ab")), front3_solution("ab"), front3("ab"));
    layout.print_test(format!("{:?}", ("a")), front3_solution("a"), front3("a"));
    layout.print_test(format!("{:?}", ("")), front3_solution(""), front3(""));
    layout.print_footer();
}

pub fn back_around_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("back_around");
    layout.print_header();
    layout.print_test(format!("{:?}", ("cat")), back_around_solution("cat"), back_around("cat"));
    layout.print_test(format!("{:?}", ("Hello")), back_around_solution("Hello"), back_around("Hello"));
    layout.print_test(format!("{:?}", ("a")), back_around_solution("a"), back_around("a"));
    layout.print_test(format!("{:?}", ("abc")), back_around_solution("abc"), back_around("abc"));
    layout.print_test(format!("{:?}", ("read")), back_around_solution("read"), back_around("read"));
    layout.print_test(format!("{:?}", ("boo")), back_around_solution("boo"), back_around("boo"));
    layout.print_footer();
}

pub fn or35_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("or35");
    layout.print_header();
    layout.print_test(format!("{:?}", (3)), or35_solution(3), or35(3));
    layout.print_test(format!("{:?}", (10)), or35_solution(10), or35(10));
    layout.print_test(format!("{:?}", (8)), or35_solution(8), or35(8));
    layout.print_test(format!("{:?}", (15)), or35_solution(15), or35(15));
    layout.print_test(format!("{:?}", (5)), or35_solution(5), or35(5));
    layout.print_test(format!("{:?}", (4)), or35_solution(4), or35(4));
    layout.print_test(format!("{:?}", (9)), or35_solution(9), or35(9));
    layout.print_test(format!("{:?}", (4)), or35_solution(4), or35(4));
    layout.print_test(format!("{:?}", (7)), or35_solution(7), or35(7));
    layout.print_test(format!("{:?}", (6)), or35_solution(6), or35(6));
    layout.print_test(format!("{:?}", (17)), or35_solution(17), or35(17));
    layout.print_test(format!("{:?}", (18)), or35_solution(18), or35(18));
    layout.print_test(format!("{:?}", (29)), or35_solution(29), or35(29));
    layout.print_test(format!("{:?}", (20)), or35_solution(20), or35(20));
    layout.print_test(format!("{:?}", (21)), or35_solution(21), or35(21));
    layout.print_test(format!("{:?}", (22)), or35_solution(22), or35(22));
    layout.print_test(format!("{:?}", (45)), or35_solution(45), or35(45));
    layout.print_test(format!("{:?}", (99)), or35_solution(99), or35(99));
    layout.print_test(format!("{:?}", (100)), or35_solution(100), or35(100));
    layout.print_test(format!("{:?}", (101)), or35_solution(101), or35(101));
    layout.print_test(format!("{:?}", (121)), or35_solution(121), or35(121));
    layout.print_test(format!("{:?}", (122)), or35_solution(122), or35(122));
    layout.print_test(format!("{:?}", (123)), or35_solution(123), or35(123));
    layout.print_footer();
}

pub fn front22_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("front22");
    layout.print_header();
    layout.print_test(format!("{:?}", ("kitten")), front22_solution("kitten"), front22("kitten"));
    layout.print_test(format!("{:?}", ("Ha")), front22_solution("Ha"), front22("Ha"));
    layout.print_test(format!("{:?}", ("abc")), front22_solution("abc"), front22("abc"));
    layout.print_test(format!("{:?}", ("ab")), front22_solution("ab"), front22("ab"));
    layout.print_test(format!("{:?}", ("a")), front22_solution("a"), front22("a"));
    layout.print_test(format!("{:?}", ("")), front22_solution(""), front22(""));
    layout.print_test(format!("{:?}", ("Logic")), front22_solution("Logic"), front22("Logic"));
    layout.print_footer();
}

pub fn start_hi_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("start_hi");
    layout.print_header();
    layout.print_test(format!("{:?}", ("hi there")), start_hi_solution("hi there"), start_hi("hi there"));
    layout.print_test(format!("{:?}", ("hi")), start_hi_solution("hi"), start_hi("hi"));
    layout.print_test(format!("{:?}", ("hello hi")), start_hi_solution("hello hi"), start_hi("hello hi"));
    layout.print_test(format!("{:?}", ("he")), start_hi_solution("he"), start_hi("he"));
    layout.print_test(format!("{:?}", ("h")), start_hi_solution("h"), start_hi("h"));
    layout.print_test(format!("{:?}", ("")), start_hi_solution(""), start_hi(""));
    layout.print_test(format!("{:?}", ("ho hi")), start_hi_solution("ho hi"), start_hi("ho hi"));
    layout.print_test(format!("{:?}", ("hi ho")), start_hi_solution("hi ho"), start_hi("hi ho"));
    layout.print_footer();
}

pub fn icy_hot_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("icy_hot");
    layout.print_header();
    layout.print_test(format!("{:?}", (120, -1)), icy_hot_solution(120, -1), icy_hot(120, -1));
    layout.print_test(format!("{:?}", (-1, 120)), icy_hot_solution(-1, 120), icy_hot(-1, 120));
    layout.print_test(format!("{:?}", (2, 120)), icy_hot_solution(2, 120), icy_hot(2, 120));
    layout.print_test(format!("{:?}", (-1, 100)), icy_hot_solution(-1, 100), icy_hot(-1, 100));
    layout.print_test(format!("{:?}", (-2, 120)), icy_hot_solution(-2, 120), icy_hot(-2, 120));
    layout.print_test(format!("{:?}", (120, 120)), icy_hot_solution(120, 120), icy_hot(120, 120));
    layout.print_footer();
}

pub fn in1020_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("in1020");
    layout.print_header();
    layout.print_test(format!("{:?}", (12, 99)), in1020_solution(12, 99), in1020(12, 99));
    layout.print_test(format!("{:?}", (21, 12)), in1020_solution(21, 12), in1020(21, 12));
    layout.print_test(format!("{:?}", (8, 99)), in1020_solution(8, 99), in1020(8, 99));
    layout.print_test(format!("{:?}", (99, 10)), in1020_solution(99, 10), in1020(99, 10));
    layout.print_test(format!("{:?}", (20, 20)), in1020_solution(20, 20), in1020(20, 20));
    layout.print_test(format!("{:?}", (21, 21)), in1020_solution(21, 21), in1020(21, 21));
    layout.print_test(format!("{:?}", (9, 9)), in1020_solution(9, 9), in1020(9, 9));
    layout.print_footer();
}

pub fn has_teen_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("has_teen");
    layout.print_header();
    layout.print_test(format!("{:?}", (13, 20, 10)), has_teen_solution(13, 20, 10), has_teen(13, 20, 10));
    layout.print_test(format!("{:?}", (20, 19, 10)), has_teen_solution(20, 19, 10), has_teen(20, 19, 10));
    layout.print_test(format!("{:?}", (20, 10, 13)), has_teen_solution(20, 10, 13), has_teen(20, 10, 13));
    layout.print_test(format!("{:?}", (1, 20, 12)), has_teen_solution(1, 20, 12), has_teen(1, 20, 12));
    layout.print_test(format!("{:?}", (19, 20, 12)), has_teen_solution(19, 20, 12), has_teen(19, 20, 12));
    layout.print_test(format!("{:?}", (12, 20, 19)), has_teen_solution(12, 20, 19), has_teen(12, 20, 19));
    layout.print_test(format!("{:?}", (12, 9, 20)), has_teen_solution(12, 9, 20), has_teen(12, 9, 20));
    layout.print_test(format!("{:?}", (12, 18, 20)), has_teen_solution(12, 18, 20), has_teen(12, 18, 20));
    layout.print_test(format!("{:?}", (14, 2, 20)), has_teen_solution(14, 2, 20), has_teen(14, 2, 20));
    layout.print_test(format!("{:?}", (4, 2, 20)), has_teen_solution(4, 2, 20), has_teen(4, 2, 20));
    layout.print_test(format!("{:?}", (11, 22, 22)), has_teen_solution(11, 22, 22), has_teen(11, 22, 22));
    layout.print_footer();
}

pub fn lone_teen_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("lone_teen");
    layout.print_header();
    layout.print_test(format!("{:?}", (13, 99)), lone_teen_solution(13, 99), lone_teen(13, 99));
    layout.print_test(format!("{:?}", (21, 19)), lone_teen_solution(21, 19), lone_teen(21, 19));
    layout.print_test(format!("{:?}", (13, 13)), lone_teen_solution(13, 13), lone_teen(13, 13));
    layout.print_test(format!("{:?}", (14, 20)), lone_teen_solution(14, 20), lone_teen(14, 20));
    layout.print_test(format!("{:?}", (20, 15)), lone_teen_solution(20, 15), lone_teen(20, 15));
    layout.print_test(format!("{:?}", (16, 17)), lone_teen_solution(16, 17), lone_teen(16, 17));
    layout.print_test(format!("{:?}", (16, 9)), lone_teen_solution(16, 9), lone_teen(16, 9));
    layout.print_test(format!("{:?}", (16, 18)), lone_teen_solution(16, 18), lone_teen(16, 18));
    layout.print_test(format!("{:?}", (13, 19)), lone_teen_solution(13, 19), lone_teen(13, 19));
    layout.print_test(format!("{:?}", (13, 20)), lone_teen_solution(13, 20), lone_teen(13, 20));
    layout.print_test(format!("{:?}", (6, 18)), lone_teen_solution(6, 18), lone_teen(6, 18));
    layout.print_test(format!("{:?}", (99, 13)), lone_teen_solution(99, 13), lone_teen(99, 13));
    layout.print_test(format!("{:?}", (99, 99)), lone_teen_solution(99, 99), lone_teen(99, 99));
    layout.print_footer();
}

pub fn del_del_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("del_del");
    layout.print_header();
    layout.print_test(format!("{:?}", ("adelbc")), del_del_solution("adelbc"), del_del("adelbc"));
    layout.print_test(format!("{:?}", ("adelHello")), del_del_solution("adelHello"), del_del("adelHello"));
    layout.print_test(format!("{:?}", ("abcdel")), del_del_solution("abcdel"), del_del("abcdel"));
    layout.print_test(format!("{:?}", ("add")), del_del_solution("add"), del_del("add"));
    layout.print_test(format!("{:?}", ("ad")), del_del_solution("ad"), del_del("ad"));
    layout.print_test(format!("{:?}", ("a")), del_del_solution("a"), del_del("a"));
    layout.print_test(format!("{:?}", ("")), del_del_solution(""), del_del(""));
    layout.print_test(format!("{:?}", ("del")), del_del_solution("del"), del_del("del"));
    layout.print_test(format!("{:?}", ("adel")), del_del_solution("adel"), del_del("adel"));
    layout.print_test(format!("{:?}", ("aadelbb")), del_del_solution("aadelbb"), del_del("aadelbb"));
    layout.print_footer();
}

pub fn mix_start_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("mix_start");
    layout.print_header();
    layout.print_test(format!("{:?}", ("mix snacks")), mix_start_solution("mix snacks"), mix_start("mix snacks"));
    layout.print_test(format!("{:?}", ("pix snacks")), mix_start_solution("pix snacks"), mix_start("pix snacks"));
    layout.print_test(format!("{:?}", ("piz snacks")), mix_start_solution("piz snacks"), mix_start("piz snacks"));
    layout.print_test(format!("{:?}", ("nix")), mix_start_solution("nix"), mix_start("nix"));
    layout.print_test(format!("{:?}", ("ni")), mix_start_solution("ni"), mix_start("ni"));
    layout.print_test(format!("{:?}", ("n")), mix_start_solution("n"), mix_start("n"));
    layout.print_footer();
}

pub fn start_oz_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("start_oz");
    layout.print_header();
    layout.print_test(format!("{:?}", ("ozymandias")), start_oz_solution("ozymandias"), start_oz("ozymandias"));
    layout.print_test(format!("{:?}", ("bzoo")), start_oz_solution("bzoo"), start_oz("bzoo"));
    layout.print_test(format!("{:?}", ("oxx")), start_oz_solution("oxx"), start_oz("oxx"));
    layout.print_test(format!("{:?}", ("ounce")), start_oz_solution("ounce"), start_oz("ounce"));
    layout.print_test(format!("{:?}", ("o")), start_oz_solution("o"), start_oz("o"));
    layout.print_test(format!("{:?}", ("abc")), start_oz_solution("abc"), start_oz("abc"));
    layout.print_test(format!("{:?}", ("")), start_oz_solution(""), start_oz(""));
    layout.print_test(format!("{:?}", ("zoo")), start_oz_solution("zoo"), start_oz("zoo"));
    layout.print_test(format!("{:?}", ("aztec")), start_oz_solution("aztec"), start_oz("aztec"));
    layout.print_test(format!("{:?}", ("zzzz")), start_oz_solution("zzzz"), start_oz("zzzz"));
    layout.print_test(format!("{:?}", ("oznic")), start_oz_solution("oznic"), start_oz("oznic"));
    layout.print_footer();
}

pub fn int_max_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("int_max");
    layout.print_header();
    layout.print_test(format!("{:?}", (1, 2, 3)), int_max_solution(1, 2, 3), int_max(1, 2, 3));
    layout.print_test(format!("{:?}", (1, 3, 2)), int_max_solution(1, 3, 2), int_max(1, 3, 2));
    layout.print_test(format!("{:?}", (3, 2, 1)), int_max_solution(3, 2, 1), int_max(3, 2, 1));
    layout.print_test(format!("{:?}", (9, 3, 3)), int_max_solution(9, 3, 3), int_max(9, 3, 3));
    layout.print_test(format!("{:?}", (3, 9, 3)), int_max_solution(3, 9, 3), int_max(3, 9, 3));
    layout.print_test(format!("{:?}", (3, 3, 9)), int_max_solution(3, 3, 9), int_max(3, 3, 9));
    layout.print_test(format!("{:?}", (8, 2, 3)), int_max_solution(8, 2, 3), int_max(8, 2, 3));
    layout.print_test(format!("{:?}", (-3, -1, -2)), int_max_solution(-3, -1, -2), int_max(-3, -1, -2));
    layout.print_test(format!("{:?}", (6, 2, 5)), int_max_solution(6, 2, 5), int_max(6, 2, 5));
    layout.print_test(format!("{:?}", (5, 6, 2)), int_max_solution(5, 6, 2), int_max(5, 6, 2));
    layout.print_test(format!("{:?}", (5, 2, 6)), int_max_solution(5, 2, 6), int_max(5, 2, 6));
    layout.print_footer();
}

pub fn close10_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("close10");
    layout.print_header();
    layout.print_test(format!("{:?}", (8, 13)), close10_solution(8, 13), close10(8, 13));
    layout.print_test(format!("{:?}", (13, 8)), close10_solution(13, 8), close10(13, 8));
    layout.print_test(format!("{:?}", (13, 7)), close10_solution(13, 7), close10(13, 7));
    layout.print_test(format!("{:?}", (7, 13)), close10_solution(7, 13), close10(7, 13));
    layout.print_test(format!("{:?}", (9, 13)), close10_solution(9, 13), close10(9, 13));
    layout.print_test(format!("{:?}", (13, 8)), close10_solution(13, 8), close10(13, 8));
    layout.print_test(format!("{:?}", (10, 12)), close10_solution(10, 12), close10(10, 12));
    layout.print_test(format!("{:?}", (11, 10)), close10_solution(11, 10), close10(11, 10));
    layout.print_test(format!("{:?}", (5, 21)), close10_solution(5, 21), close10(5, 21));
    layout.print_test(format!("{:?}", (0, 20)), close10_solution(0, 20), close10(0, 20));
    layout.print_test(format!("{:?}", (0, 20)), close10_solution(0, 20), close10(0, 20));
    layout.print_test(format!("{:?}", (10, 10)), close10_solution(10, 10), close10(10, 10));
    layout.print_footer();
}

pub fn in3050_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("in3050");
    layout.print_header();
    layout.print_test(format!("{:?}", (30, 31)), in3050_solution(30, 31), in3050(30, 31));
    layout.print_test(format!("{:?}", (30, 41)), in3050_solution(30, 41), in3050(30, 41));
    layout.print_test(format!("{:?}", (40, 50)), in3050_solution(40, 50), in3050(40, 50));
    layout.print_test(format!("{:?}", (40, 51)), in3050_solution(40, 51), in3050(40, 51));
    layout.print_test(format!("{:?}", (39, 50)), in3050_solution(39, 50), in3050(39, 50));
    layout.print_test(format!("{:?}", (50, 39)), in3050_solution(50, 39), in3050(50, 39));
    layout.print_test(format!("{:?}", (40, 39)), in3050_solution(40, 39), in3050(40, 39));
    layout.print_test(format!("{:?}", (49, 48)), in3050_solution(49, 48), in3050(49, 48));
    layout.print_test(format!("{:?}", (50, 40)), in3050_solution(50, 40), in3050(50, 40));
    layout.print_test(format!("{:?}", (50, 51)), in3050_solution(50, 51), in3050(50, 51));
    layout.print_test(format!("{:?}", (35, 36)), in3050_solution(35, 36), in3050(35, 36));
    layout.print_test(format!("{:?}", (35, 45)), in3050_solution(35, 45), in3050(35, 45));
    layout.print_footer();
}

pub fn max1020_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("max1020");
    layout.print_header();
    layout.print_test(format!("{:?}", (11, 19)), max1020_solution(11, 19), max1020(11, 19));
    layout.print_test(format!("{:?}", (19, 11)), max1020_solution(19, 11), max1020(19, 11));
    layout.print_test(format!("{:?}", (11, 9)), max1020_solution(11, 9), max1020(11, 9));
    layout.print_test(format!("{:?}", (9, 21)), max1020_solution(9, 21), max1020(9, 21));
    layout.print_test(format!("{:?}", (10, 21)), max1020_solution(10, 21), max1020(10, 21));
    layout.print_test(format!("{:?}", (21, 10)), max1020_solution(21, 10), max1020(21, 10));
    layout.print_test(format!("{:?}", (9, 11)), max1020_solution(9, 11), max1020(9, 11));
    layout.print_test(format!("{:?}", (23, 10)), max1020_solution(23, 10), max1020(23, 10));
    layout.print_test(format!("{:?}", (20, 10)), max1020_solution(20, 10), max1020(20, 10));
    layout.print_test(format!("{:?}", (7, 20)), max1020_solution(7, 20), max1020(7, 20));
    layout.print_test(format!("{:?}", (17, 16)), max1020_solution(17, 16), max1020(17, 16));
    layout.print_footer();
}

pub fn string_e_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("string_e");
    layout.print_header();
    layout.print_test(format!("{:?}", ("Hello")), string_e_solution("Hello"), string_e("Hello"));
    layout.print_test(format!("{:?}", ("Heelle")), string_e_solution("Heelle"), string_e("Heelle"));
    layout.print_test(format!("{:?}", ("Heelele")), string_e_solution("Heelele"), string_e("Heelele"));
    layout.print_test(format!("{:?}", ("HII")), string_e_solution("HII"), string_e("HII"));
    layout.print_test(format!("{:?}", ("e")), string_e_solution("e"), string_e("e"));
    layout.print_test(format!("{:?}", ("")), string_e_solution(""), string_e(""));
    layout.print_footer();
}

pub fn last_digit_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("last_digit");
    layout.print_header();
    layout.print_test(format!("{:?}", (7, 17)), last_digit_solution(7, 17), last_digit(7, 17));
    layout.print_test(format!("{:?}", (6, 17)), last_digit_solution(6, 17), last_digit(6, 17));
    layout.print_test(format!("{:?}", (3, 113)), last_digit_solution(3, 113), last_digit(3, 113));
    layout.print_test(format!("{:?}", (114, 113)), last_digit_solution(114, 113), last_digit(114, 113));
    layout.print_test(format!("{:?}", (114, 4)), last_digit_solution(114, 4), last_digit(114, 4));
    layout.print_test(format!("{:?}", (10, 0)), last_digit_solution(10, 0), last_digit(10, 0));
    layout.print_test(format!("{:?}", (11, 0)), last_digit_solution(11, 0), last_digit(11, 0));
    layout.print_footer();
}

pub fn end_up_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("end_up");
    layout.print_header();
    layout.print_test(format!("{:?}", ("Hello")), end_up_solution("Hello"), end_up("Hello"));
    layout.print_test(format!("{:?}", ("hi there")), end_up_solution("hi there"), end_up("hi there"));
    layout.print_test(format!("{:?}", ("hi")), end_up_solution("hi"), end_up("hi"));
    layout.print_test(format!("{:?}", ("woo hoo")), end_up_solution("woo hoo"), end_up("woo hoo"));
    layout.print_test(format!("{:?}", ("xyz12")), end_up_solution("xyz12"), end_up("xyz12"));
    layout.print_test(format!("{:?}", ("x")), end_up_solution("x"), end_up("x"));
    layout.print_test(format!("{:?}", ("")), end_up_solution(""), end_up(""));
    layout.print_footer();
}

pub fn every_nth_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("every_nth");
    layout.print_header();
    layout.print_test(format!("{:?}", ("Miracle", 2)), every_nth_solution("Miracle", 2), every_nth("Miracle", 2));
    layout.print_test(format!("{:?}", ("abcdefg", 2)), every_nth_solution("abcdefg", 2), every_nth("abcdefg", 2));
    layout.print_test(format!("{:?}", ("abcdefg", 3)), every_nth_solution("abcdefg", 3), every_nth("abcdefg", 3));
    layout.print_test(format!("{:?}", ("Chocolate", 3)), every_nth_solution("Chocolate", 3), every_nth("Chocolate", 3));
    layout.print_test(format!("{:?}", ("Chocolates", 3)), every_nth_solution("Chocolates", 3), every_nth("Chocolates", 3));
    layout.print_test(format!("{:?}", ("Chocolates", 4)), every_nth_solution("Chocolates", 4), every_nth("Chocolates", 4));
    layout.print_test(format!("{:?}", ("Chocolates", 100)), every_nth_solution("Chocolates", 100), every_nth("Chocolates", 100));
    layout.print_footer();
}

pub fn string_times_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("string_times");
    layout.print_header();
    layout.print_test(format!("{:?}", ("Hi", 2)), string_times_solution("Hi", 2), string_times("Hi", 2));
    layout.print_test(format!("{:?}", ("Hi", 3)), string_times_solution("Hi", 3), string_times("Hi", 3));
    layout.print_test(format!("{:?}", ("Hi", 1)), string_times_solution("Hi", 1), string_times("Hi", 1));
    layout.print_test(format!("{:?}", ("Hi", 0)), string_times_solution("Hi", 0), string_times("Hi", 0));
    layout.print_test(format!("{:?}", ("Hi", 5)), string_times_solution("Hi", 5), string_times("Hi", 5));
    layout.print_test(format!("{:?}", ("Oh Boy!", 2)), string_times_solution("Oh Boy!", 2), string_times("Oh Boy!", 2));
    layout.print_test(format!("{:?}", ("x", 4)), string_times_solution("x", 4), string_times("x", 4));
    layout.print_test(format!("{:?}", ("", 4)), string_times_solution("", 4), string_times("", 4));
    layout.print_test(format!("{:?}", ("code", 2)), string_times_solution("code", 2), string_times("code", 2));
    layout.print_test(format!("{:?}", ("code", 3)), string_times_solution("code", 3), string_times("code", 3));
    layout.print_footer();
}

pub fn front_times_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("front_times");
    layout.print_header();
    layout.print_test(format!("{:?}", ("Chocolate", 2)), front_times_solution("Chocolate", 2), front_times("Chocolate", 2));
    layout.print_test(format!("{:?}", ("Chocolate", 3)), front_times_solution("Chocolate", 3), front_times("Chocolate", 3));
    layout.print_test(format!("{:?}", ("Abc", 3)), front_times_solution("Abc", 3), front_times("Abc", 3));
    layout.print_test(format!("{:?}", ("Ab", 4)), front_times_solution("Ab", 4), front_times("Ab", 4));
    layout.print_test(format!("{:?}", ("A", 4)), front_times_solution("A", 4), front_times("A", 4));
    layout.print_test(format!("{:?}", ("", 4)), front_times_solution("", 4), front_times("", 4));
    layout.print_test(format!("{:?}", ("Abc", 0)), front_times_solution("Abc", 0), front_times("Abc", 0));
    layout.print_footer();
}

pub fn count_x_x_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("count_x_x");
    layout.print_header();
    layout.print_test(format!("{:?}", ("abcxx")), count_x_x_solution("abcxx"), count_x_x("abcxx"));
    layout.print_test(format!("{:?}", ("xxx")), count_x_x_solution("xxx"), count_x_x("xxx"));
    layout.print_test(format!("{:?}", ("xxxx")), count_x_x_solution("xxxx"), count_x_x("xxxx"));
    layout.print_test(format!("{:?}", ("abc")), count_x_x_solution("abc"), count_x_x("abc"));
    layout.print_test(format!("{:?}", ("Hello There")), count_x_x_solution("Hello There"), count_x_x("Hello There"));
    layout.print_test(format!("{:?}", ("Hexxo Thxxe")), count_x_x_solution("Hexxo Thxxe"), count_x_x("Hexxo Thxxe"));
    layout.print_test(format!("{:?}", ("")), count_x_x_solution(""), count_x_x(""));
    layout.print_test(format!("{:?}", ("Kittens")), count_x_x_solution("Kittens"), count_x_x("Kittens"));
    layout.print_test(format!("{:?}", ("Kittensxxx")), count_x_x_solution("Kittensxxx"), count_x_x("Kittensxxx"));
    layout.print_footer();
}

pub fn double_x_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("double_x");
    layout.print_header();
    layout.print_test(format!("{:?}", ("axxbb")), double_x_solution("axxbb"), double_x("axxbb"));
    layout.print_test(format!("{:?}", ("axaxax")), double_x_solution("axaxax"), double_x("axaxax"));
    layout.print_test(format!("{:?}", ("xxxxx")), double_x_solution("xxxxx"), double_x("xxxxx"));
    layout.print_test(format!("{:?}", ("xaxxx")), double_x_solution("xaxxx"), double_x("xaxxx"));
    layout.print_test(format!("{:?}", ("aaaax")), double_x_solution("aaaax"), double_x("aaaax"));
    layout.print_test(format!("{:?}", ("")), double_x_solution(""), double_x(""));
    layout.print_test(format!("{:?}", ("abc")), double_x_solution("abc"), double_x("abc"));
    layout.print_test(format!("{:?}", ("x")), double_x_solution("x"), double_x("x"));
    layout.print_test(format!("{:?}", ("xx")), double_x_solution("xx"), double_x("xx"));
    layout.print_test(format!("{:?}", ("xaxx")), double_x_solution("xaxx"), double_x("xaxx"));
    layout.print_footer();
}

pub fn string_bits_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("string_bits");
    layout.print_header();
    layout.print_test(format!("{:?}", ("Hello")), string_bits_solution("Hello"), string_bits("Hello"));
    layout.print_test(format!("{:?}", ("Hi")), string_bits_solution("Hi"), string_bits("Hi"));
    layout.print_test(format!("{:?}", ("Heeololeo")), string_bits_solution("Heeololeo"), string_bits("Heeololeo"));
    layout.print_test(format!("{:?}", ("HiHiHi")), string_bits_solution("HiHiHi"), string_bits("HiHiHi"));
    layout.print_test(format!("{:?}", ("")), string_bits_solution(""), string_bits(""));
    layout.print_test(format!("{:?}", ("Greetings")), string_bits_solution("Greetings"), string_bits("Greetings"));
    layout.print_test(format!("{:?}", ("Chocolate")), string_bits_solution("Chocolate"), string_bits("Chocolate"));
    layout.print_test(format!("{:?}", ("pi")), string_bits_solution("pi"), string_bits("pi"));
    layout.print_test(format!("{:?}", ("Hello Kitten")), string_bits_solution("Hello Kitten"), string_bits("Hello Kitten"));
    layout.print_test(format!("{:?}", ("hxaxpxpxy")), string_bits_solution("hxaxpxpxy"), string_bits("hxaxpxpxy"));
    layout.print_footer();
}

pub fn last2_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("last2");
    layout.print_header();
    layout.print_test(format!("{:?}", ("hixxhi")), last2_solution("hixxhi"), last2("hixxhi"));
    layout.print_test(format!("{:?}", ("xaxxaxaxx")), last2_solution("xaxxaxaxx"), last2("xaxxaxaxx"));
    layout.print_test(format!("{:?}", ("axxaaxx")), last2_solution("axxaaxx"), last2("axxaaxx"));
    layout.print_test(format!("{:?}", ("xxaxxaxxaxx")), last2_solution("xxaxxaxxaxx"), last2("xxaxxaxxaxx"));
    layout.print_test(format!("{:?}", ("xaxaxaa")), last2_solution("xaxaxaa"), last2("xaxaxaa"));
    layout.print_test(format!("{:?}", ("xxxx")), last2_solution("xxxx"), last2("xxxx"));
    layout.print_test(format!("{:?}", ("13121312")), last2_solution("13121312"), last2("13121312"));
    layout.print_test(format!("{:?}", ("11212")), last2_solution("11212"), last2("11212"));
    layout.print_test(format!("{:?}", ("13121311")), last2_solution("13121311"), last2("13121311"));
    layout.print_test(format!("{:?}", ("hi")), last2_solution("hi"), last2("hi"));
    layout.print_test(format!("{:?}", ("h")), last2_solution("h"), last2("h"));
    layout.print_test(format!("{:?}", ("")), last2_solution(""), last2(""));
    layout.print_footer();
}

pub fn array_count9_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("array_count9");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 2, 9])), array_count9_solution(vec![1, 2, 9]), array_count9(vec![1, 2, 9]));
    layout.print_test(format!("{:?}", (vec![1, 9, 9])), array_count9_solution(vec![1, 9, 9]), array_count9(vec![1, 9, 9]));
    layout.print_test(format!("{:?}", (vec![1, 9, 9, 3, 9])), array_count9_solution(vec![1, 9, 9, 3, 9]), array_count9(vec![1, 9, 9, 3, 9]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3])), array_count9_solution(vec![1, 2, 3]), array_count9(vec![1, 2, 3]));
    layout.print_test(format!("{:?}", (vec![0; 0])), array_count9_solution(vec![0; 0]), array_count9(vec![0; 0]));
    layout.print_test(format!("{:?}", (vec![4, 2, 4, 3, 1])), array_count9_solution(vec![4, 2, 4, 3, 1]), array_count9(vec![4, 2, 4, 3, 1]));
    layout.print_test(format!("{:?}", (vec![9, 2, 4, 3, 1])), array_count9_solution(vec![9, 2, 4, 3, 1]), array_count9(vec![9, 2, 4, 3, 1]));
    layout.print_footer();
}

pub fn array_front9_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("array_front9");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 2, 9, 3, 4])), array_front9_solution(vec![1, 2, 9, 3, 4]), array_front9(vec![1, 2, 9, 3, 4]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3, 4, 9])), array_front9_solution(vec![1, 2, 3, 4, 9]), array_front9(vec![1, 2, 3, 4, 9]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3, 4, 5])), array_front9_solution(vec![1, 2, 3, 4, 5]), array_front9(vec![1, 2, 3, 4, 5]));
    layout.print_test(format!("{:?}", (vec![9, 2, 3])), array_front9_solution(vec![9, 2, 3]), array_front9(vec![9, 2, 3]));
    layout.print_test(format!("{:?}", (vec![1, 9, 9])), array_front9_solution(vec![1, 9, 9]), array_front9(vec![1, 9, 9]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3])), array_front9_solution(vec![1, 2, 3]), array_front9(vec![1, 2, 3]));
    layout.print_test(format!("{:?}", (vec![1, 9])), array_front9_solution(vec![1, 9]), array_front9(vec![1, 9]));
    layout.print_test(format!("{:?}", (vec![5, 5])), array_front9_solution(vec![5, 5]), array_front9(vec![5, 5]));
    layout.print_test(format!("{:?}", (vec![2])), array_front9_solution(vec![2]), array_front9(vec![2]));
    layout.print_test(format!("{:?}", (vec![9])), array_front9_solution(vec![9]), array_front9(vec![9]));
    layout.print_test(format!("{:?}", (vec![0; 0])), array_front9_solution(vec![0; 0]), array_front9(vec![0; 0]));
    layout.print_test(format!("{:?}", (vec![3, 9, 2, 3, 3])), array_front9_solution(vec![3, 9, 2, 3, 3]), array_front9(vec![3, 9, 2, 3, 3]));
    layout.print_footer();
}

pub fn string_match_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("string_match");
    layout.print_header();
    layout.print_test(format!("{:?}", ("xxcaazz", "xxbaaz")), string_match_solution("xxcaazz", "xxbaaz"), string_match("xxcaazz", "xxbaaz"));
    layout.print_test(format!("{:?}", ("abc", "abc")), string_match_solution("abc", "abc"), string_match("abc", "abc"));
    layout.print_test(format!("{:?}", ("abc", "axc")), string_match_solution("abc", "axc"), string_match("abc", "axc"));
    layout.print_test(format!("{:?}", ("hello", "he")), string_match_solution("hello", "he"), string_match("hello", "he"));
    layout.print_test(format!("{:?}", ("he", "hello")), string_match_solution("he", "hello"), string_match("he", "hello"));
    layout.print_test(format!("{:?}", ("", "hello")), string_match_solution("", "hello"), string_match("", "hello"));
    layout.print_test(format!("{:?}", ("aabbccdd", "abbbxxd")), string_match_solution("aabbccdd", "abbbxxd"), string_match("aabbccdd", "abbbxxd"));
    layout.print_test(format!("{:?}", ("aaxxaaxx", "iaxxai")), string_match_solution("aaxxaaxx", "iaxxai"), string_match("aaxxaaxx", "iaxxai"));
    layout.print_test(format!("{:?}", ("iaxxai", "aaxxaaxx")), string_match_solution("iaxxai", "aaxxaaxx"), string_match("iaxxai", "aaxxaaxx"));
    layout.print_footer();
}

pub fn string_x_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("string_x");
    layout.print_header();
    layout.print_test(format!("{:?}", ("xxHxix")), string_x_solution("xxHxix"), string_x("xxHxix"));
    layout.print_test(format!("{:?}", ("abxxxcd")), string_x_solution("abxxxcd"), string_x("abxxxcd"));
    layout.print_test(format!("{:?}", ("xabxxxcdx")), string_x_solution("xabxxxcdx"), string_x("xabxxxcdx"));
    layout.print_test(format!("{:?}", ("xKittenx")), string_x_solution("xKittenx"), string_x("xKittenx"));
    layout.print_test(format!("{:?}", ("Hello")), string_x_solution("Hello"), string_x("Hello"));
    layout.print_test(format!("{:?}", ("xx")), string_x_solution("xx"), string_x("xx"));
    layout.print_test(format!("{:?}", ("x")), string_x_solution("x"), string_x("x"));
    layout.print_test(format!("{:?}", ("")), string_x_solution(""), string_x(""));
    layout.print_footer();
}

pub fn alt_pairs_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("alt_pairs");
    layout.print_header();
    layout.print_test(format!("{:?}", ("kitten")), alt_pairs_solution("kitten"), alt_pairs("kitten"));
    layout.print_test(format!("{:?}", ("Chocolate")), alt_pairs_solution("Chocolate"), alt_pairs("Chocolate"));
    layout.print_test(format!("{:?}", ("CodingHorror")), alt_pairs_solution("CodingHorror"), alt_pairs("CodingHorror"));
    layout.print_test(format!("{:?}", ("yak")), alt_pairs_solution("yak"), alt_pairs("yak"));
    layout.print_test(format!("{:?}", ("ya")), alt_pairs_solution("ya"), alt_pairs("ya"));
    layout.print_test(format!("{:?}", ("y")), alt_pairs_solution("y"), alt_pairs("y"));
    layout.print_test(format!("{:?}", ("")), alt_pairs_solution(""), alt_pairs(""));
    layout.print_test(format!("{:?}", ("ThisThatTheOther")), alt_pairs_solution("ThisThatTheOther"), alt_pairs("ThisThatTheOther"));
    layout.print_footer();
}

pub fn string_yak_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("string_yak");
    layout.print_header();
    layout.print_test(format!("{:?}", ("yakpak")), string_yak_solution("yakpak"), string_yak("yakpak"));
    layout.print_test(format!("{:?}", ("pakyak")), string_yak_solution("pakyak"), string_yak("pakyak"));
    layout.print_test(format!("{:?}", ("yak123ya")), string_yak_solution("yak123ya"), string_yak("yak123ya"));
    layout.print_test(format!("{:?}", ("yak")), string_yak_solution("yak"), string_yak("yak"));
    layout.print_test(format!("{:?}", ("yakxxxyak")), string_yak_solution("yakxxxyak"), string_yak("yakxxxyak"));
    layout.print_test(format!("{:?}", ("xxcaazz")), string_yak_solution("xxcaazz"), string_yak("xxcaazz"));
    layout.print_test(format!("{:?}", ("hiyakHi")), string_yak_solution("hiyakHi"), string_yak("hiyakHi"));
    layout.print_test(format!("{:?}", ("xxxyakyyyakzzz")), string_yak_solution("xxxyakyyyakzzz"), string_yak("xxxyakyyyakzzz"));
    layout.print_footer();
}

pub fn array667_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("array667");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![6, 6, 2])), array667_solution(vec![6, 6, 2]), array667(vec![6, 6, 2]));
    layout.print_test(format!("{:?}", (vec![6, 6, 2, 6])), array667_solution(vec![6, 6, 2, 6]), array667(vec![6, 6, 2, 6]));
    layout.print_test(format!("{:?}", (vec![6, 7, 2, 6])), array667_solution(vec![6, 7, 2, 6]), array667(vec![6, 7, 2, 6]));
    layout.print_test(format!("{:?}", (vec![6, 6, 2, 7, 6, 7])), array667_solution(vec![6, 6, 2, 7, 6, 7]), array667(vec![6, 6, 2, 7, 6, 7]));
    layout.print_test(format!("{:?}", (vec![1, 6, 3])), array667_solution(vec![1, 6, 3]), array667(vec![1, 6, 3]));
    layout.print_test(format!("{:?}", (vec![6, 1])), array667_solution(vec![6, 1]), array667(vec![6, 1]));
    layout.print_test(format!("{:?}", (vec![0; 0])), array667_solution(vec![0; 0]), array667(vec![0; 0]));
    layout.print_test(format!("{:?}", (vec![3, 6, 7, 6])), array667_solution(vec![3, 6, 7, 6]), array667(vec![3, 6, 7, 6]));
    layout.print_test(format!("{:?}", (vec![3, 6, 6, 7])), array667_solution(vec![3, 6, 6, 7]), array667(vec![3, 6, 6, 7]));
    layout.print_test(format!("{:?}", (vec![6, 3, 6, 6])), array667_solution(vec![6, 3, 6, 6]), array667(vec![6, 3, 6, 6]));
    layout.print_test(format!("{:?}", (vec![6, 7, 6, 6])), array667_solution(vec![6, 7, 6, 6]), array667(vec![6, 7, 6, 6]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3, 5, 6])), array667_solution(vec![1, 2, 3, 5, 6]), array667(vec![1, 2, 3, 5, 6]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3, 6, 6])), array667_solution(vec![1, 2, 3, 6, 6]), array667(vec![1, 2, 3, 6, 6]));
    layout.print_footer();
}

pub fn no_triples_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("no_triples");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 1, 2, 2, 1])), no_triples_solution(vec![1, 1, 2, 2, 1]), no_triples(vec![1, 1, 2, 2, 1]));
    layout.print_test(format!("{:?}", (vec![1, 1, 2, 2, 2, 1])), no_triples_solution(vec![1, 1, 2, 2, 2, 1]), no_triples(vec![1, 1, 2, 2, 2, 1]));
    layout.print_test(format!("{:?}", (vec![1, 1, 2, 2, 2, 1])), no_triples_solution(vec![1, 1, 2, 2, 2, 1]), no_triples(vec![1, 1, 2, 2, 2, 1]));
    layout.print_test(format!("{:?}", (vec![1, 2, 1])), no_triples_solution(vec![1, 2, 1]), no_triples(vec![1, 2, 1]));
    layout.print_test(format!("{:?}", (vec![1, 1, 1])), no_triples_solution(vec![1, 1, 1]), no_triples(vec![1, 1, 1]));
    layout.print_test(format!("{:?}", (vec![1, 1])), no_triples_solution(vec![1, 1]), no_triples(vec![1, 1]));
    layout.print_test(format!("{:?}", (vec![1])), no_triples_solution(vec![1]), no_triples(vec![1]));
    layout.print_test(format!("{:?}", (vec![1])), no_triples_solution(vec![1]), no_triples(vec![1]));
    layout.print_test(format!("{:?}", (vec![0; 0])), no_triples_solution(vec![0; 0]), no_triples(vec![0; 0]));
    layout.print_footer();
}

pub fn has271_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("has271");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 2, 7, 1])), has271_solution(vec![1, 2, 7, 1]), has271(vec![1, 2, 7, 1]));
    layout.print_test(format!("{:?}", (vec![1, 2, 8, 1])), has271_solution(vec![1, 2, 8, 1]), has271(vec![1, 2, 8, 1]));
    layout.print_test(format!("{:?}", (vec![2, 7, 1])), has271_solution(vec![2, 7, 1]), has271(vec![2, 7, 1]));
    layout.print_test(format!("{:?}", (vec![3, 8, 2])), has271_solution(vec![3, 8, 2]), has271(vec![3, 8, 2]));
    layout.print_test(format!("{:?}", (vec![2, 7, 3])), has271_solution(vec![2, 7, 3]), has271(vec![2, 7, 3]));
    layout.print_test(format!("{:?}", (vec![2, 7, 4])), has271_solution(vec![2, 7, 4]), has271(vec![2, 7, 4]));
    layout.print_test(format!("{:?}", (vec![2, 7, -1])), has271_solution(vec![2, 7, -1]), has271(vec![2, 7, -1]));
    layout.print_test(format!("{:?}", (vec![2, 7, -2])), has271_solution(vec![2, 7, -2]), has271(vec![2, 7, -2]));
    layout.print_test(format!("{:?}", (vec![4, 5, 3, 8, 0])), has271_solution(vec![4, 5, 3, 8, 0]), has271(vec![4, 5, 3, 8, 0]));
    layout.print_test(format!("{:?}", (vec![2, 7, 5, 10, 4])), has271_solution(vec![2, 7, 5, 10, 4]), has271(vec![2, 7, 5, 10, 4]));
    layout.print_test(format!("{:?}", (vec![2, 7, -2, 4, 9, 3])), has271_solution(vec![2, 7, -2, 4, 9, 3]), has271(vec![2, 7, -2, 4, 9, 3]));
    layout.print_test(format!("{:?}", (vec![2, 7, 5, 10, 1])), has271_solution(vec![2, 7, 5, 10, 1]), has271(vec![2, 7, 5, 10, 1]));
    layout.print_test(format!("{:?}", (vec![2, 7, -2, 10, 2])), has271_solution(vec![2, 7, -2, 10, 2]), has271(vec![2, 7, -2, 10, 2]));
    layout.print_footer();
}
