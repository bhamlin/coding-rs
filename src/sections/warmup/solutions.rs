use regex::Regex;
use std::cmp::{max, min};

pub fn sleep_in_solution(weekday: bool, vacation: bool) -> bool {
    !weekday || vacation
}

pub fn monkey_trouble_solution(a_smile: bool, b_smile: bool) -> bool {
    !(a_smile ^ b_smile)
}

pub fn sum_double_solution(a: i32, b: i32) -> i32 {
    let sum = a + b;
    if a == b {
        sum * 2
    } else {
        sum
    }
}

pub fn diff21_solution(n: i32) -> i32 {
    let diff = (n - 21).abs();
    if n > 21 {
        diff * 2
    } else {
        diff
    }
}

pub fn parrot_trouble_solution(talking: bool, hour: i32) -> bool {
    match talking {
        true => (hour < 7) || (hour > 20),
        false => false,
    }
}

pub fn makes10_solution(a: i32, b: i32) -> bool {
    (10 == (a + b)) || (a == 10) || (b == 10)
}

pub fn near_hundred_solution(n: i32) -> bool {
    (10 >= (n - 100).abs()) || (10 >= (n - 200).abs())
}

pub fn pos_neg_solution(a: i32, b: i32, negative: bool) -> bool {
    match negative {
        false => (a.is_negative() && b.is_positive()) || (a.is_positive() && b.is_negative()),
        true => a.is_negative() && b.is_negative(),
    }
}

pub fn not_string_solution(str: impl Into<String>) -> String {
    let input: String = str.into();
    if !input.starts_with("not") {
        format!("not {input}")
    } else {
        input
    }
}

pub fn missing_char_solution(str: impl Into<String>, n: usize) -> String {
    let input: String = str.into();

    format!("{}{}", &input[..n], &input[n + 1..])
}

pub fn front_back_solution(str: impl Into<String>) -> String {
    let input: String = str.into();
    let n = input.len();

    if n > 2 {
        format!("{}{}{}", &input[n - 1..], &input[1..n - 1], &input[..1])
    } else if n == 2 {
        format!("{}{}", &input[1 as usize..], &input[..1 as usize])
    } else {
        input
    }
}

pub fn front3_solution(str: impl Into<String>) -> String {
    let input: String = str.into();
    let n: usize = min(3, input.len());
    let chunk = &input[..n];

    format!("{chunk}{chunk}{chunk}")
}

pub fn back_around_solution(str: impl Into<String>) -> String {
    let input: String = str.into();
    let n = input.len();
    let c = &input[n - 1..];

    format!("{c}{input}{c}")
}

pub fn or35_solution(n: i32) -> bool {
    let three = 0 == (n % 3);
    let five = 0 == (n % 5);

    three || five
}

pub fn front22_solution(str: impl Into<String>) -> String {
    let input: String = str.into();
    let n = min(2, input.len());
    let chunk = &input[..n];

    format!("{chunk}{input}{chunk}")
}

pub fn start_hi_solution(str: impl Into<String>) -> bool {
    let input: String = str.into();

    input.to_lowercase().starts_with("hi")
}

pub fn icy_hot_solution(temp1: i32, temp2: i32) -> bool {
    (min(temp1, temp2) < 0) && (max(temp1, temp2) > 100)
}

pub fn in1020_solution(a: i32, b: i32) -> bool {
    // ((a >= 10) && (a <= 20)) || ((b >= 10) && (b <= 20))
    vec![a, b]
        .iter() // Iterate over list
        .map(|&v| (v >= 10) && (v <= 20)) // Map each item
        .fold(false, |a, e| a || e) // Reduce by or
}

pub fn has_teen_solution(a: i32, b: i32, c: i32) -> bool {
    vec![a, b, c]
        .iter() // Iterate over list
        .map(|&v| (v >= 13) && (v <= 19)) // Map each item
        .fold(false, |a, e| a || e) // Reduce by or
}

pub fn lone_teen_solution(a: i32, b: i32) -> bool {
    vec![a, b]
        .iter() // Iterate over list
        .map(|&v| (v >= 13) && (v <= 19)) // Map each item
        .fold(false, |a, e| a ^ e) // Reduce by xor
}

pub fn del_del_solution(str: impl Into<String>) -> String {
    let input: String = str.into();

    if input.len() >= 4 {
        let chunk = &input[1..4];

        if chunk == "del" {
            return format!("{}{}", &input[..1], &input[4..]);
        }
    }

    input
}

pub fn mix_start_solution(str: impl Into<String>) -> bool {
    let input: String = str.into();
    let re = Regex::new(r"^.ix").expect("You should write better regex");

    re.is_match(&input)
}

pub fn start_oz_solution(str: impl Into<String>) -> String {
    let input: Vec<char> = str.into().chars().collect();
    let len = input.len();

    let o;
    if len > 0 {
        o = if input[0] == 'o' { "o" } else { "" };
    } else {
        o = "";
    }

    let z;
    if len > 1 {
        z = if input[1] == 'z' { "z" } else { "" };
    } else {
        z = "";
    }

    format!("{o}{z}")
}

pub fn int_max_solution(a: i32, b: i32, c: i32) -> i32 {
    vec![a, b, c]
        .iter() // Iterate over list
        .reduce(|a, e| max(e, a)) // Reduce to max item
        .expect("max should always return successfully") // Unwrap Option
        .to_owned() // Dereference
}

pub fn close10_solution(a: i32, b: i32) -> i32 {
    let ad = (a - 10).abs();
    let bd = (b - 10).abs();

    if ad < bd {
        a
    } else if ad > bd {
        b
    } else {
        0
    }
}

pub fn in3050_solution(a: i32, b: i32) -> bool {
    let first = ((a >= 30) && (a <= 40)) && ((b >= 30) && (b <= 40));
    let second = ((a >= 40) && (a <= 50)) && ((b >= 40) && (b <= 50));

    first || second
}

pub fn max1020_solution(a: i32, b: i32) -> i32 {
    vec![a, b]
        .iter() // Iter
        .filter(|&&e| ((e >= 10) && (e <= 20)))
        .fold(0, |a, &e| max(a, e))
}

pub fn string_e_solution(str: impl Into<String>) -> bool {
    let input: String = str.into();
    let count = input.chars().filter(|&c| c == 'e').count();

    (count >= 1) && (count <= 3)
}

pub fn last_digit_solution(a: i32, b: i32) -> bool {
    (a % 10) == (b % 10)
}

pub fn end_up_solution(str: impl Into<String>) -> String {
    let input: String = str.into();
    let n = input.len();
    let stop = max(0, n - min(n, 3));

    let front = &input[0..stop];
    let back = &input[stop..n];

    format!("{}{}", front, back.to_uppercase())
}

pub fn every_nth_solution(str: impl Into<String>, n: usize) -> String {
    let input: String = str.into();

    // Filter+Map instead of for...skip_by
    input
        .chars()
        .enumerate() // Get list of chars
        .filter(|(i, _)| 0 == (i % n)) // Pick indicies that are multiples
        .map(|(_, c)| c) // Convert from tuples to chars
        .collect() // Join into string
}

pub fn string_times_solution(str: impl Into<String>, n: usize) -> String {
    let input: String = str.into();
    let mut result = Vec::<String>::new();

    for _ in 0..n {
        result.push(input.clone());
    }

    result.join("")
}

pub fn front_times_solution(str: impl Into<String>, n: usize) -> String {
    let input: String = str.into();
    let len = input.len();
    let chunk = input[0..min(len, 3)].to_string();
    let mut result = Vec::<String>::new();

    for _ in 0..n {
        result.push(chunk.clone());
    }

    result.join("")
}

pub fn count_x_x_solution(str: impl Into<String>) -> i32 {
    let input: String = str.into();
    let len = input.len();

    // Used string scan instead of regex and replacen
    // since rust regex doesn't support lookahead
    let mut count = 0;
    for i in 1..len {
        let chunk = &input[i - 1..min(i + 1, len)];
        if chunk == "xx" {
            count += 1;
        }
    }

    count
}

pub fn double_x_solution(str: impl Into<String>) -> bool {
    let input: String = str.into();
    let len = input.len();

    let fx = input.find('x');
    if fx.is_some() {
        let fx = fx.unwrap();
        let chunk = &input[fx..min(len, fx + 2)];
        if chunk == "xx" {
            return true;
        }
    }
    false
}

pub fn string_bits_solution(str: impl Into<String>) -> String {
    let input: String = str.into();

    // Filter+Map instead of for...skip_by
    input
        .chars()
        .enumerate() // Get list of chars
        .filter(|(i, _)| 0 == (i % 2)) // Pick indicies that are multiples
        .map(|(_, c)| c) // Convert from tuples to chars
        .collect() // Join into string
}

pub fn last2_solution(str: impl Into<String>) -> i32 {
    let input: String = str.into();
    let len = input.len();

    // Used string scan instead of regex and replacen
    // since rust regex doesn't support lookahead
    let mut count = 0;
    if len > 3 {
        let pattern: String = input.chars().rev().take(2).collect();
        let pattern: String = pattern.chars().rev().collect();

        for i in 1..len - 1 {
            let chunk = &input[i - 1..min(i + 1, len)];
            if chunk == pattern {
                count += 1;
            }
        }
    }

    count
}

pub fn array_count9_solution(nums: Vec<i32>) -> i32 {
    nums.iter() // Iter
        .filter(|&&e| e == 9)
        .count()
        .try_into()
        .expect("Failed usize to i32")
}

pub fn array_front9_solution(nums: Vec<i32>) -> bool {
    nums.iter().take(4).filter(|&&e| e == 9).count() > 0
}

pub fn string_match_solution(a: impl Into<String>, b: impl Into<String>) -> i32 {
    let a: String = a.into();
    let b: String = b.into();
    let len = min(a.len(), b.len());

    let mut count = 0;
    for i in 1..len {
        let a_chonk = &a[i - 1..i + 1];
        let b_chonk = &b[i - 1..i + 1];

        if a_chonk == b_chonk {
            count += 1;
        }
    }

    count
}

pub fn string_x_solution(str: impl Into<String>) -> String {
    let input: String = str.into();
    let len = input.len();

    if len > 2 {
        let front = &input[0..1];
        let mid = &input[1..len - 1];
        let back = &input[len - 1..len];

        format!("{}{}{}", front, mid.replace('x', ""), back)
    } else {
        input
    }
}

pub fn alt_pairs_solution(str: impl Into<String>) -> String {
    let input: String = str.into();

    input
        .chars() //Iter
        .enumerate()
        .filter(|(i, _)| 2 > (i % 4))
        .map(|(_, c)| c)
        .collect()
}

pub fn string_yak_solution(str: impl Into<String>) -> String {
    let input: String = str.into();
    let re = Regex::new(r"y.k").expect("You should write better regex");

    re.replace_all(&input, "").to_string()
}

pub fn array667_solution(nums: Vec<i32>) -> i32 {
    let len = nums.len();

    let mut count = 0;
    for i in 1..len {
        let chunk = &nums[i - 1..i + 1];
        if (chunk == [6, 6]) || (chunk == [6, 7]) {
            count += 1;
        }
    }

    count
}

pub fn no_triples_solution(nums: Vec<i32>) -> bool {
    let len = nums.len();

    for i in 2..len {
        let chunk = &nums[i - 2..i + 1];
        if (chunk[0] == chunk[1]) && (chunk[1] == chunk[2]) {
            return false;
        }
    }

    true
}

pub fn has271_solution(nums: Vec<i32>) -> bool {
    let len = nums.len();

    for i in 2..len {
        let chunk = &nums[i - 2..i + 1];
        let i = chunk[0];
        let j = chunk[1];
        let k = chunk[2];

        if 5 == (j - i) {
            if 2 >= (k - i + 1).abs() {
                return true;
            }
        }
    }

    false
}
