/// ## Warmup-1 -- sleepIn
/// The parameter weekday is true if it is a weekday, and the parameter vacation
/// is true if we are on vacation. We sleep in if it is not a weekday or we're
/// on vacation. Return true if we sleep in.
/// ### Examples
///    sleep_in(true, true) -> true
///    sleep_in(true, false) -> false
///    sleep_in(false, true) -> true
#[allow(unused)]
pub fn sleep_in(weekday: bool, vacation: bool) -> bool {
    todo!()
}

/// ## Warmup-1 -- monkeyTrouble
/// We have two monkeys, a and b, and the parameters aSmile and bSmile indicate
/// if each is smiling. We are in trouble if they are both smiling or if neither
/// of them is smiling. Return true if we are in trouble.
/// ### Examples
///    monkey_trouble(true, true) -> true
///    monkey_trouble(false, false) -> true
///    monkey_trouble(true, false) -> false
#[allow(unused)]
pub fn monkey_trouble(a_smile: bool, b_smile: bool) -> bool {
    todo!()
}

/// ## Warmup-1 -- sumDouble
/// Given two int values, return their sum. Unless the two values are the same,
/// then return double their sum.
/// ### Examples
///    sum_double(1, 2) -> 3
///    sum_double(3, 2) -> 5
///    sum_double(2, 2) -> 8
#[allow(unused)]
pub fn sum_double(a: i32, b: i32) -> i32 {
    todo!()
}

/// ## Warmup-1 -- diff21
/// Given an int n, return the absolute difference between n and 21, except
/// return double the absolute difference if n is over 21.
/// ### Examples
///    diff21(19) -> 2
///    diff21(10) -> 11
///    diff21(21) -> 0
#[allow(unused)]
pub fn diff21(n: i32) -> i32 {
    todo!()
}

/// ## Warmup-1 -- parrotTrouble
/// We have a loud talking parrot. The "hour" parameter is the current hour time
/// in the range 0..23. We are in trouble if the parrot is talking and the hour
/// is before 7 or after 20. Return true if we are in trouble.
/// ### Examples
///    parrot_trouble(true, 6) -> true
///    parrot_trouble(true, 7) -> false
///    parrot_trouble(false, 6) -> false
#[allow(unused)]
pub fn parrot_trouble(talking: bool, hour: i32) -> bool {
    todo!()
}

/// ## Warmup-1 -- makes10
/// Given 2 ints, a and b, return true if one if them is 10 or if their sum is
/// 10.
/// ### Examples
///    makes10(9, 10) -> true
///    makes10(9, 9) -> false
///    makes10(1, 9) -> true
#[allow(unused)]
pub fn makes10(a: i32, b: i32) -> bool {
    todo!()
}

/// ## Warmup-1 -- nearHundred
/// Given an int n, return true if it is within 10 of 100 or 200. Note:
/// num.abs() returns the absolute value of a number.
/// ### Examples
///    near_hundred(93) -> true
///    near_hundred(90) -> true
///    near_hundred(89) -> false
#[allow(unused)]
pub fn near_hundred(n: i32) -> bool {
    todo!()
}

/// ## Warmup-1 -- posNeg
/// Given 2 int values, return true if one is negative and one is positive.
/// Except if the parameter "negative" is true, then return true only if both
/// are negative.
/// ### Examples
///    pos_neg(1, -1, false) -> true
///    pos_neg(-1, 1, false) -> true
///    pos_neg(-4, -5, true) -> true
#[allow(unused)]
pub fn pos_neg(a: i32, b: i32, negative: bool) -> bool {
    todo!()
}

/// ## Warmup-1 -- notString
/// Given a string, return a new string where "not " has been added to the
/// front. However, if the string already begins with "not", return the string
/// unchanged.
/// ### Examples
///    not_string("candy") -> "not candy"
///    not_string("x") -> "not x"
///    not_string("not bad") -> "not bad"
#[allow(unused)]
pub fn not_string(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## Warmup-1 -- missingChar
/// Given a non-empty string and an int n, return a new string where the char at
/// index n has been removed. The value of n will be a valid index of a char in
/// the original string (i.e. n will be in the range 0..str.length()-1
/// inclusive).
/// ### Examples
///    missing_char("kitten", 1) -> "ktten"
///    missing_char("kitten", 0) -> "itten"
///    missing_char("kitten", 4) -> "kittn"
#[allow(unused)]
pub fn missing_char(str: impl Into<String>, n: i32) -> String {
    let str: String = str.into();
    todo!()
}

/// ## Warmup-1 -- frontBack
/// Given a string, return a new string where the first and last chars have been
/// exchanged.
/// ### Examples
///    front_back("code") -> "eodc"
///    front_back("a") -> "a"
///    front_back("ab") -> "ba"
#[allow(unused)]
pub fn front_back(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## Warmup-1 -- front3
/// Given a string, we'll say that the front is the first 3 chars of the string.
/// If the string length is less than 3, the front is whatever is there. Return
/// a new string which is 3 copies of the front.
/// ### Examples
///    front3("Java") -> "JavJavJav"
///    front3("Chocolate") -> "ChoChoCho"
///    front3("abc") -> "abcabcabc"
#[allow(unused)]
pub fn front3(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## Warmup-1 -- backAround
/// Given a string, take the last char and return a new string with the last
/// char added at the front and back, so 'cat' yields 'tcatt' The original
/// string will be length 1 or more.
/// ### Examples
///    back_around("cat") -> "tcatt"
///    back_around("Hello") -> "oHelloo"
///    back_around("a") -> "aaa"
#[allow(unused)]
pub fn back_around(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## Warmup-1 -- or35
/// Return true if the given non-negative number is a multiple of 3 or a
/// multiple of 5. (Hint: Use the % 'mod' operator)
/// ### Examples
///    or35(3) -> true
///    or35(10) -> true
///    or35(8) -> false
#[allow(unused)]
pub fn or35(n: i32) -> bool {
    todo!()
}

/// ## Warmup-1 -- front22
/// Given a string, take the first 2 chars and return the string with the 2
/// chars added at both the front and back, so 'kitten' yields 'kikittenki'
/// chars are there.
/// ### Examples
///    front22("kitten") -> "kikittenki"
///    front22("Ha") -> "HaHaHa"
///    front22("abc") -> "ababcab"
#[allow(unused)]
pub fn front22(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## Warmup-1 -- startHi
/// Given a string, return true if the string starts with 'hi'and false
/// otherwise.
/// ### Examples
///    start_hi("hi there") -> true
///    start_hi("hi") -> true
///    start_hi("hello hi") -> false
#[allow(unused)]
pub fn start_hi(str: impl Into<String>) -> bool {
    let str: String = str.into();
    todo!()
}

/// ## Warmup-1 -- icyHot
/// Given two temperatures, return true if one is less than 0 and the other is
/// greater than 100.
/// ### Examples
///    icy_hot(120, -1) -> true
///    icy_hot(-1, 120) -> true
///    icy_hot(2, 120) -> false
#[allow(unused)]
pub fn icy_hot(temp1: i32, temp2: i32) -> bool {
    todo!()
}

/// ## Warmup-1 -- in1020
/// Given 2 int values, return true if either of them is in the range 10..20
/// inclusive.
/// ### Examples
///    in1020(12, 99) -> true
///    in1020(21, 12) -> true
///    in1020(8, 99) -> false
#[allow(unused)]
pub fn in1020(a: i32, b: i32) -> bool {
    todo!()
}

/// ## Warmup-1 -- hasTeen
/// We'll say that a number is 'teen' if it is in the range 13..19 inclusive.
/// Given 3 int values, return true if 1 or more of them are teen.
/// ### Examples
///    has_teen(13, 20, 10) -> true
///    has_teen(20, 19, 10) -> true
///    has_teen(20, 10, 13) -> true
#[allow(unused)]
pub fn has_teen(a: i32, b: i32, c: i32) -> bool {
    todo!()
}

/// ## Warmup-1 -- loneTeen
/// We'll say that a number is 'teen' if it is in the range 13..19 inclusive.
/// Given 2 int values, return true if one or the other is teen, but not both.
/// ### Examples
///    lone_teen(13, 99) -> true
///    lone_teen(21, 19) -> true
///    lone_teen(13, 13) -> false
#[allow(unused)]
pub fn lone_teen(a: i32, b: i32) -> bool {
    todo!()
}

/// ## Warmup-1 -- delDel
/// Given a string, if the string "del" appears starting at index 1, return a
/// string where that "del" has been deleted. Otherwise, return the string
/// unchanged.
/// ### Examples
///    del_del("adelbc") -> "abc"
///    del_del("adelHello") -> "aHello"
///    del_del("abcdel") -> "abcdel"
#[allow(unused)]
pub fn del_del(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## Warmup-1 -- mixStart
/// Return true if the given string begins with 'mix', except the 'm' can be
/// anything, so 'pix', '9ix' .. all count.
/// ### Examples
///    mix_start("mix snacks") -> true
///    mix_start("pix snacks") -> true
///    mix_start("piz snacks") -> false
#[allow(unused)]
pub fn mix_start(str: impl Into<String>) -> bool {
    let str: String = str.into();
    todo!()
}

/// ## Warmup-1 -- startOz
/// Given a string, return a string made of the first 2 chars (if present),
/// however include first char only if it is 'o' and include the second only if
/// it is 'z', so 'ozymandias' yields 'oz'.
/// ### Examples
///    start_oz("ozymandias") -> "oz"
///    start_oz("bzoo") -> "z"
///    start_oz("oxx") -> "o"
#[allow(unused)]
pub fn start_oz(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## Warmup-1 -- intMax
/// Given three int values, a b c, return the largest.
/// ### Examples
///    int_max(1, 2, 3) -> 3
///    int_max(1, 3, 2) -> 3
///    int_max(3, 2, 1) -> 3
#[allow(unused)]
pub fn int_max(a: i32, b: i32, c: i32) -> i32 {
    todo!()
}

/// ## Warmup-1 -- close10
/// Given 2 int values, return whichever value is nearest to the value 10, or
/// return 0 in the event of a tie. Note that Math.abs(n) returns the absolute
/// value of a number.
/// ### Examples
///    close10(8, 13) -> 8
///    close10(13, 8) -> 8
///    close10(13, 7) -> 0
#[allow(unused)]
pub fn close10(a: i32, b: i32) -> i32 {
    todo!()
}

/// ## Warmup-1 -- in3050
/// Given 2 int values, return true if they are both in the range 30..40
/// inclusive, or they are both in the range 40..50 inclusive.
/// ### Examples
///    in3050(30, 31) -> true
///    in3050(30, 41) -> false
///    in3050(40, 50) -> true
#[allow(unused)]
pub fn in3050(a: i32, b: i32) -> bool {
    todo!()
}

/// ## Warmup-1 -- max1020
/// Given 2 positive int values, return the larger value that is in the range
/// 10..20 inclusive, or return 0 if neither is in that range.
/// ### Examples
///    max1020(11, 19) -> 19
///    max1020(19, 11) -> 19
///    max1020(11, 9) -> 11
#[allow(unused)]
pub fn max1020(a: i32, b: i32) -> i32 {
    todo!()
}

/// ## Warmup-1 -- stringE
/// Return true if the given string contains between 1 and 3 'e' chars.
/// ### Examples
///    string_e("Hello") -> true
///    string_e("Heelle") -> true
///    string_e("Heelele") -> false
#[allow(unused)]
pub fn string_e(str: impl Into<String>) -> bool {
    let str: String = str.into();
    todo!()
}

/// ## Warmup-1 -- lastDigit
/// Given two non-negative int values, return true if they have the same last
/// digit, such as with 27 and 57. Note that the % 'mod' operator computes
/// remainders, so 17 % 10 is 7.
/// ### Examples
///    last_digit(7, 17) -> true
///    last_digit(6, 17) -> false
///    last_digit(3, 113) -> true
#[allow(unused)]
pub fn last_digit(a: i32, b: i32) -> bool {
    todo!()
}

/// ## Warmup-1 -- endUp
/// Given a string, return a new string where the last 3 chars are now in upper
/// case. If the string has less than 3 chars, uppercase whatever is there. Note
/// that str.to_uppercase() returns the uppercase version of a string.
/// ### Examples
///    end_up("Hello") -> "HeLLO"
///    end_up("hi there") -> "hi thERE"
///    end_up("hi") -> "HI"
#[allow(unused)]
pub fn end_up(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## Warmup-1 -- everyNth
/// Given a non-empty string and an int N, return the string made starting with
/// char 0, and then every Nth char of the string. So if N is 3, use char 0, 3,
/// 6, ... and so on. N is 1 or more.
/// ### Examples
///    every_nth("Miracle", 2) -> "Mrce"
///    every_nth("abcdefg", 2) -> "aceg"
///    every_nth("abcdefg", 3) -> "adg"
#[allow(unused)]
pub fn every_nth(str: impl Into<String>, n: i32) -> String {
    let str: String = str.into();
    todo!()
}

/// ## Warmup-2 -- stringTimes
/// Given a string and a non-negative int n, return a larger string that is n
/// copies of the original string.
/// ### Examples
///    string_times("Hi", 2) -> "HiHi"
///    string_times("Hi", 3) -> "HiHiHi"
///    string_times("Hi", 1) -> "Hi"
#[allow(unused)]
pub fn string_times(str: impl Into<String>, n: i32) -> String {
    let str: String = str.into();
    todo!()
}

/// ## Warmup-2 -- frontTimes
/// Given a string and a non-negative int n, we'll say that the front of the
/// string is the first 3 chars, or whatever is there if the string is less than
/// length 3. Return n copies of the front.
/// ### Examples
///    front_times("Chocolate", 2) -> "ChoCho"
///    front_times("Chocolate", 3) -> "ChoChoCho"
///    front_times("Abc", 3) -> "AbcAbcAbc"
#[allow(unused)]
pub fn front_times(str: impl Into<String>, n: i32) -> String {
    let str: String = str.into();
    todo!()
}

/// ## Warmup-2 -- countXX
/// Count the number of 'xx' in the given string. We'll say that overlapping is
/// allowed, so 'xxx' contains 2 'xx'.
/// ### Examples
///    count_x_x("abcxx") -> 1
///    count_x_x("xxx") -> 2
///    count_x_x("xxxx") -> 3
#[allow(unused)]
pub fn count_x_x(str: impl Into<String>) -> i32 {
    let str: String = str.into();
    todo!()
}

/// ## Warmup-2 -- doubleX
/// Given a string, return true if the first instance of "x" in the string is
/// immediately followed by another "x".
/// ### Examples
///    double_x("axxbb") -> true
///    double_x("axaxax") -> false
///    double_x("xxxxx") -> true
#[allow(unused)]
pub fn double_x(str: impl Into<String>) -> bool {
    let str: String = str.into();
    todo!()
}

/// ## Warmup-2 -- stringBits
/// Given a string, return a new string made of every other char starting with
/// the first, so "Hello" yields "Hlo".
/// ### Examples
///    string_bits("Hello") -> "Hlo"
///    string_bits("Hi") -> "H"
///    string_bits("Heeololeo") -> "Hello"
#[allow(unused)]
pub fn string_bits(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## Warmup-2 -- last2
/// Given a string, return the count of the number of times that a substring
/// length 2 appears in the string and also as the last 2 chars of the string,
/// so "hixxxhi" yields 1 (we won't count the end substring).
/// ### Examples
///    last2("hixxhi") -> 1
///    last2("xaxxaxaxx") -> 1
///    last2("axxaaxx") -> 1
#[allow(unused)]
pub fn last2(str: impl Into<String>) -> i32 {
    let str: String = str.into();
    todo!()
}

/// ## Warmup-2 -- arrayCount9
/// Given an array of ints, return the number of 9's in the array.
/// ### Examples
///    array_count9([1,2,9]) -> 1
///    array_count9([1,9,9]) -> 2
///    array_count9([1,9,9,3,9]) -> 3
#[allow(unused)]
pub fn array_count9(nums: Vec<i32>) -> i32 {
    todo!()
}

/// ## Warmup-2 -- arrayFront9
/// Given an array of ints, return true if one of the first 4 elements in the
/// array is a 9. The array length may be less than 4.
/// ### Examples
///    array_front9([1,2,9,3,4]) -> true
///    array_front9([1,2,3,4,9]) -> false
///    array_front9([1,2,3,4,5]) -> false
#[allow(unused)]
pub fn array_front9(nums: Vec<i32>) -> bool {
    todo!()
}

/// ## Warmup-2 -- stringMatch
/// Given 2 strings, a and b, return the number of the positions where they
/// contain the same length 2 substring. So "xxcaazz" and "xxbaaz" yields 3,
/// since the "xx" "xx", "aa", and "az" substrings appear in the same place in
/// both strings.
/// ### Examples
///    string_match("xxcaazz", "xxbaaz") -> 3
///    string_match("abc", "abc") -> 2
///    string_match("abc", "axc") -> 0
#[allow(unused)]
pub fn string_match(a: impl Into<String>, b: impl Into<String>) -> i32 {
    let a: String = a.into();
    let b: String = b.into();
    todo!()
}

/// ## Warmup-2 -- stringX
/// Given a string, return a version where all the "x" have been removed. Except
/// an "x" at the very start or end should not be removed.
/// ### Examples
///    string_x("xxHxix") -> "xHix"
///    string_x("abxxxcd") -> "abcd"
///    string_x("xabxxxcdx") -> "xabcdx"
#[allow(unused)]
pub fn string_x(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## Warmup-2 -- altPairs
/// Given a string, return a string made of the chars at indexes 0,1, 4,5, 8,9
/// ... so "kittens" yields "kien".
/// ### Examples
///    alt_pairs("kitten") -> "kien"
///    alt_pairs("Chocolate") -> "Chole"
///    alt_pairs("CodingHorror") -> "Congrr"
#[allow(unused)]
pub fn alt_pairs(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## Warmup-2 -- stringYak
/// Suppose the string "yak" is unlucky. Given a string, return a version where
/// all the "yak" are removed, but the "a" can be any char. The "yak" strings
/// will not overlap.
/// ### Examples
///    string_yak("yakpak") -> "pak"
///    string_yak("pakyak") -> "pak"
///    string_yak("yak123ya") -> "123ya"
#[allow(unused)]
pub fn string_yak(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## Warmup-2 -- array667
/// Given an array of ints, return the number of times that two 6's are next to
/// each other in the array. Also count instances where the second "6" is
/// actually a 7.
/// ### Examples
///    array667([6,6,2]) -> 1
///    array667([6,6,2,6]) -> 1
///    array667([6,7,2,6]) -> 1
#[allow(unused)]
pub fn array667(nums: Vec<i32>) -> i32 {
    todo!()
}

/// ## Warmup-2 -- noTriples
/// Given an array of ints, we'll say that a triple is a value appearing 3 times
/// in a row in the array. Return true if the array does not contain any
/// triples.
/// ### Examples
///    no_triples([1,1,2,2,1]) -> true
///    no_triples([1,1,2,2,2,1]) -> false
///    no_triples([1,1,2,2,2,1]) -> false
#[allow(unused)]
pub fn no_triples(nums: Vec<i32>) -> bool {
    todo!()
}

/// ## Warmup-2 -- has271
/// Given an array of ints, return true if it contains a 2, 7, 1 pattern -- a
/// value, followed by the value plus 5, followed by the value minus 1.
/// Additionally the 271 counts even if the "1" differs by 2 or less from the
/// correct value.
/// ### Examples
///    has271([1,2,7,1]) -> true
///    has271([1,2,8,1]) -> false
///    has271([2,7,1]) -> true
#[allow(unused)]
pub fn has271(nums: Vec<i32>) -> bool {
    todo!()
}
