pub mod implementations;
pub mod runners;
pub mod solutions;

#[cfg(test)]
mod test_sleep_in {
    use super::implementations::sleep_in;
    use super::solutions::sleep_in_solution;

    #[test]
    fn test_sleep_in_1() {
        assert_eq!(sleep_in(true, true), sleep_in_solution(true, true))
    }
    #[test]
    fn test_sleep_in_2() {
        assert_eq!(sleep_in(true, false), sleep_in_solution(true, false))
    }
    #[test]
    fn test_sleep_in_3() {
        assert_eq!(sleep_in(false, true), sleep_in_solution(false, true))
    }
    #[test]
    fn test_sleep_in_4() {
        assert_eq!(sleep_in(false, false), sleep_in_solution(false, false))
    }
}

#[cfg(test)]
mod test_monkey_trouble {
    use super::implementations::monkey_trouble;
    use super::solutions::monkey_trouble_solution;

    #[test]
    fn test_monkey_trouble_1() {
        assert_eq!(monkey_trouble(true, true), monkey_trouble_solution(true, true))
    }
    #[test]
    fn test_monkey_trouble_2() {
        assert_eq!(monkey_trouble(false, false), monkey_trouble_solution(false, false))
    }
    #[test]
    fn test_monkey_trouble_3() {
        assert_eq!(monkey_trouble(true, false), monkey_trouble_solution(true, false))
    }
    #[test]
    fn test_monkey_trouble_4() {
        assert_eq!(monkey_trouble(false, true), monkey_trouble_solution(false, true))
    }
}

#[cfg(test)]
mod test_sum_double {
    use super::implementations::sum_double;
    use super::solutions::sum_double_solution;

    #[test]
    fn test_sum_double_1() {
        assert_eq!(sum_double(1, 2), sum_double_solution(1, 2))
    }
    #[test]
    fn test_sum_double_2() {
        assert_eq!(sum_double(3, 2), sum_double_solution(3, 2))
    }
    #[test]
    fn test_sum_double_3() {
        assert_eq!(sum_double(2, 2), sum_double_solution(2, 2))
    }
    #[test]
    fn test_sum_double_4() {
        assert_eq!(sum_double(-1, 0), sum_double_solution(-1, 0))
    }
    #[test]
    fn test_sum_double_5() {
        assert_eq!(sum_double(3, 3), sum_double_solution(3, 3))
    }
    #[test]
    fn test_sum_double_6() {
        assert_eq!(sum_double(0, 0), sum_double_solution(0, 0))
    }
    #[test]
    fn test_sum_double_7() {
        assert_eq!(sum_double(0, 1), sum_double_solution(0, 1))
    }
    #[test]
    fn test_sum_double_8() {
        assert_eq!(sum_double(3, 4), sum_double_solution(3, 4))
    }
}

#[cfg(test)]
mod test_diff21 {
    use super::implementations::diff21;
    use super::solutions::diff21_solution;

    #[test]
    fn test_diff21_1() {
        assert_eq!(diff21(19), diff21_solution(19))
    }
    #[test]
    fn test_diff21_2() {
        assert_eq!(diff21(10), diff21_solution(10))
    }
    #[test]
    fn test_diff21_3() {
        assert_eq!(diff21(21), diff21_solution(21))
    }
    #[test]
    fn test_diff21_4() {
        assert_eq!(diff21(22), diff21_solution(22))
    }
    #[test]
    fn test_diff21_5() {
        assert_eq!(diff21(25), diff21_solution(25))
    }
    #[test]
    fn test_diff21_6() {
        assert_eq!(diff21(30), diff21_solution(30))
    }
    #[test]
    fn test_diff21_7() {
        assert_eq!(diff21(0), diff21_solution(0))
    }
    #[test]
    fn test_diff21_8() {
        assert_eq!(diff21(1), diff21_solution(1))
    }
    #[test]
    fn test_diff21_9() {
        assert_eq!(diff21(2), diff21_solution(2))
    }
    #[test]
    fn test_diff21_10() {
        assert_eq!(diff21(-1), diff21_solution(-1))
    }
    #[test]
    fn test_diff21_11() {
        assert_eq!(diff21(-2), diff21_solution(-2))
    }
    #[test]
    fn test_diff21_12() {
        assert_eq!(diff21(50), diff21_solution(50))
    }
}

#[cfg(test)]
mod test_parrot_trouble {
    use super::implementations::parrot_trouble;
    use super::solutions::parrot_trouble_solution;

    #[test]
    fn test_parrot_trouble_1() {
        assert_eq!(parrot_trouble(true, 6), parrot_trouble_solution(true, 6))
    }
    #[test]
    fn test_parrot_trouble_2() {
        assert_eq!(parrot_trouble(true, 7), parrot_trouble_solution(true, 7))
    }
    #[test]
    fn test_parrot_trouble_3() {
        assert_eq!(parrot_trouble(false, 6), parrot_trouble_solution(false, 6))
    }
    #[test]
    fn test_parrot_trouble_4() {
        assert_eq!(parrot_trouble(true, 21), parrot_trouble_solution(true, 21))
    }
    #[test]
    fn test_parrot_trouble_5() {
        assert_eq!(parrot_trouble(false, 21), parrot_trouble_solution(false, 21))
    }
    #[test]
    fn test_parrot_trouble_6() {
        assert_eq!(parrot_trouble(true, 23), parrot_trouble_solution(true, 23))
    }
    #[test]
    fn test_parrot_trouble_7() {
        assert_eq!(parrot_trouble(false, 23), parrot_trouble_solution(false, 23))
    }
    #[test]
    fn test_parrot_trouble_8() {
        assert_eq!(parrot_trouble(true, 20), parrot_trouble_solution(true, 20))
    }
    #[test]
    fn test_parrot_trouble_9() {
        assert_eq!(parrot_trouble(false, 12), parrot_trouble_solution(false, 12))
    }
}

#[cfg(test)]
mod test_makes10 {
    use super::implementations::makes10;
    use super::solutions::makes10_solution;

    #[test]
    fn test_makes10_1() {
        assert_eq!(makes10(9, 10), makes10_solution(9, 10))
    }
    #[test]
    fn test_makes10_2() {
        assert_eq!(makes10(9, 9), makes10_solution(9, 9))
    }
    #[test]
    fn test_makes10_3() {
        assert_eq!(makes10(1, 9), makes10_solution(1, 9))
    }
    #[test]
    fn test_makes10_4() {
        assert_eq!(makes10(10, 1), makes10_solution(10, 1))
    }
    #[test]
    fn test_makes10_5() {
        assert_eq!(makes10(10, 10), makes10_solution(10, 10))
    }
    #[test]
    fn test_makes10_6() {
        assert_eq!(makes10(8, 2), makes10_solution(8, 2))
    }
    #[test]
    fn test_makes10_7() {
        assert_eq!(makes10(8, 3), makes10_solution(8, 3))
    }
    #[test]
    fn test_makes10_8() {
        assert_eq!(makes10(10, 42), makes10_solution(10, 42))
    }
    #[test]
    fn test_makes10_9() {
        assert_eq!(makes10(12, -2), makes10_solution(12, -2))
    }
}

#[cfg(test)]
mod test_near_hundred {
    use super::implementations::near_hundred;
    use super::solutions::near_hundred_solution;

    #[test]
    fn test_near_hundred_1() {
        assert_eq!(near_hundred(93), near_hundred_solution(93))
    }
    #[test]
    fn test_near_hundred_2() {
        assert_eq!(near_hundred(90), near_hundred_solution(90))
    }
    #[test]
    fn test_near_hundred_3() {
        assert_eq!(near_hundred(89), near_hundred_solution(89))
    }
    #[test]
    fn test_near_hundred_4() {
        assert_eq!(near_hundred(110), near_hundred_solution(110))
    }
    #[test]
    fn test_near_hundred_5() {
        assert_eq!(near_hundred(111), near_hundred_solution(111))
    }
    #[test]
    fn test_near_hundred_6() {
        assert_eq!(near_hundred(121), near_hundred_solution(121))
    }
    #[test]
    fn test_near_hundred_7() {
        assert_eq!(near_hundred(0), near_hundred_solution(0))
    }
    #[test]
    fn test_near_hundred_8() {
        assert_eq!(near_hundred(5), near_hundred_solution(5))
    }
    #[test]
    fn test_near_hundred_9() {
        assert_eq!(near_hundred(191), near_hundred_solution(191))
    }
    #[test]
    fn test_near_hundred_10() {
        assert_eq!(near_hundred(189), near_hundred_solution(189))
    }
}

#[cfg(test)]
mod test_pos_neg {
    use super::implementations::pos_neg;
    use super::solutions::pos_neg_solution;

    #[test]
    fn test_pos_neg_1() {
        assert_eq!(pos_neg(1, -1, false), pos_neg_solution(1, -1, false))
    }
    #[test]
    fn test_pos_neg_2() {
        assert_eq!(pos_neg(-1, 1, false), pos_neg_solution(-1, 1, false))
    }
    #[test]
    fn test_pos_neg_3() {
        assert_eq!(pos_neg(-4, -5, true), pos_neg_solution(-4, -5, true))
    }
    #[test]
    fn test_pos_neg_4() {
        assert_eq!(pos_neg(-4, -5, false), pos_neg_solution(-4, -5, false))
    }
    #[test]
    fn test_pos_neg_5() {
        assert_eq!(pos_neg(-4, 5, false), pos_neg_solution(-4, 5, false))
    }
    #[test]
    fn test_pos_neg_6() {
        assert_eq!(pos_neg(-4, 5, true), pos_neg_solution(-4, 5, true))
    }
    #[test]
    fn test_pos_neg_7() {
        assert_eq!(pos_neg(1, 1, false), pos_neg_solution(1, 1, false))
    }
    #[test]
    fn test_pos_neg_8() {
        assert_eq!(pos_neg(-1, -1, false), pos_neg_solution(-1, -1, false))
    }
    #[test]
    fn test_pos_neg_9() {
        assert_eq!(pos_neg(1, -1, true), pos_neg_solution(1, -1, true))
    }
    #[test]
    fn test_pos_neg_10() {
        assert_eq!(pos_neg(-1, 1, true), pos_neg_solution(-1, 1, true))
    }
    #[test]
    fn test_pos_neg_11() {
        assert_eq!(pos_neg(1, 1, true), pos_neg_solution(1, 1, true))
    }
    #[test]
    fn test_pos_neg_12() {
        assert_eq!(pos_neg(-1, -1, true), pos_neg_solution(-1, -1, true))
    }
    #[test]
    fn test_pos_neg_13() {
        assert_eq!(pos_neg(5, -5, false), pos_neg_solution(5, -5, false))
    }
    #[test]
    fn test_pos_neg_14() {
        assert_eq!(pos_neg(-6, 6, false), pos_neg_solution(-6, 6, false))
    }
    #[test]
    fn test_pos_neg_15() {
        assert_eq!(pos_neg(-5, -6, false), pos_neg_solution(-5, -6, false))
    }
    #[test]
    fn test_pos_neg_16() {
        assert_eq!(pos_neg(-2, -1, false), pos_neg_solution(-2, -1, false))
    }
    #[test]
    fn test_pos_neg_17() {
        assert_eq!(pos_neg(1, 2, false), pos_neg_solution(1, 2, false))
    }
    #[test]
    fn test_pos_neg_18() {
        assert_eq!(pos_neg(-5, 6, true), pos_neg_solution(-5, 6, true))
    }
    #[test]
    fn test_pos_neg_19() {
        assert_eq!(pos_neg(-5, -5, true), pos_neg_solution(-5, -5, true))
    }
}

#[cfg(test)]
mod test_not_string {
    use super::implementations::not_string;
    use super::solutions::not_string_solution;

    #[test]
    fn test_not_string_1() {
        assert_eq!(not_string("candy"), not_string_solution("candy"))
    }
    #[test]
    fn test_not_string_2() {
        assert_eq!(not_string("x"), not_string_solution("x"))
    }
    #[test]
    fn test_not_string_3() {
        assert_eq!(not_string("not bad"), not_string_solution("not bad"))
    }
    #[test]
    fn test_not_string_4() {
        assert_eq!(not_string("bad"), not_string_solution("bad"))
    }
    #[test]
    fn test_not_string_5() {
        assert_eq!(not_string("not"), not_string_solution("not"))
    }
    #[test]
    fn test_not_string_6() {
        assert_eq!(not_string("is not"), not_string_solution("is not"))
    }
    #[test]
    fn test_not_string_7() {
        assert_eq!(not_string("no"), not_string_solution("no"))
    }
}

#[cfg(test)]
mod test_missing_char {
    use super::implementations::missing_char;
    use super::solutions::missing_char_solution;

    #[test]
    fn test_missing_char_1() {
        assert_eq!(missing_char("kitten", 1), missing_char_solution("kitten", 1))
    }
    #[test]
    fn test_missing_char_2() {
        assert_eq!(missing_char("kitten", 0), missing_char_solution("kitten", 0))
    }
    #[test]
    fn test_missing_char_3() {
        assert_eq!(missing_char("kitten", 4), missing_char_solution("kitten", 4))
    }
    #[test]
    fn test_missing_char_4() {
        assert_eq!(missing_char("Hi", 0), missing_char_solution("Hi", 0))
    }
    #[test]
    fn test_missing_char_5() {
        assert_eq!(missing_char("Hi", 1), missing_char_solution("Hi", 1))
    }
    #[test]
    fn test_missing_char_6() {
        assert_eq!(missing_char("code", 0), missing_char_solution("code", 0))
    }
    #[test]
    fn test_missing_char_7() {
        assert_eq!(missing_char("code", 1), missing_char_solution("code", 1))
    }
    #[test]
    fn test_missing_char_8() {
        assert_eq!(missing_char("code", 2), missing_char_solution("code", 2))
    }
    #[test]
    fn test_missing_char_9() {
        assert_eq!(missing_char("code", 3), missing_char_solution("code", 3))
    }
    #[test]
    fn test_missing_char_10() {
        assert_eq!(missing_char("chocolate", 8), missing_char_solution("chocolate", 8))
    }
}

#[cfg(test)]
mod test_front_back {
    use super::implementations::front_back;
    use super::solutions::front_back_solution;

    #[test]
    fn test_front_back_1() {
        assert_eq!(front_back("code"), front_back_solution("code"))
    }
    #[test]
    fn test_front_back_2() {
        assert_eq!(front_back("a"), front_back_solution("a"))
    }
    #[test]
    fn test_front_back_3() {
        assert_eq!(front_back("ab"), front_back_solution("ab"))
    }
    #[test]
    fn test_front_back_4() {
        assert_eq!(front_back("abc"), front_back_solution("abc"))
    }
    #[test]
    fn test_front_back_5() {
        assert_eq!(front_back(""), front_back_solution(""))
    }
    #[test]
    fn test_front_back_6() {
        assert_eq!(front_back("Chocolate"), front_back_solution("Chocolate"))
    }
    #[test]
    fn test_front_back_7() {
        assert_eq!(front_back("aavj"), front_back_solution("aavj"))
    }
    #[test]
    fn test_front_back_8() {
        assert_eq!(front_back("hello"), front_back_solution("hello"))
    }
}

#[cfg(test)]
mod test_front3 {
    use super::implementations::front3;
    use super::solutions::front3_solution;

    #[test]
    fn test_front3_1() {
        assert_eq!(front3("Java"), front3_solution("Java"))
    }
    #[test]
    fn test_front3_2() {
        assert_eq!(front3("Chocolate"), front3_solution("Chocolate"))
    }
    #[test]
    fn test_front3_3() {
        assert_eq!(front3("abc"), front3_solution("abc"))
    }
    #[test]
    fn test_front3_4() {
        assert_eq!(front3("abcXYZ"), front3_solution("abcXYZ"))
    }
    #[test]
    fn test_front3_5() {
        assert_eq!(front3("ab"), front3_solution("ab"))
    }
    #[test]
    fn test_front3_6() {
        assert_eq!(front3("a"), front3_solution("a"))
    }
    #[test]
    fn test_front3_7() {
        assert_eq!(front3(""), front3_solution(""))
    }
}

#[cfg(test)]
mod test_back_around {
    use super::implementations::back_around;
    use super::solutions::back_around_solution;

    #[test]
    fn test_back_around_1() {
        assert_eq!(back_around("cat"), back_around_solution("cat"))
    }
    #[test]
    fn test_back_around_2() {
        assert_eq!(back_around("Hello"), back_around_solution("Hello"))
    }
    #[test]
    fn test_back_around_3() {
        assert_eq!(back_around("a"), back_around_solution("a"))
    }
    #[test]
    fn test_back_around_4() {
        assert_eq!(back_around("abc"), back_around_solution("abc"))
    }
    #[test]
    fn test_back_around_5() {
        assert_eq!(back_around("read"), back_around_solution("read"))
    }
    #[test]
    fn test_back_around_6() {
        assert_eq!(back_around("boo"), back_around_solution("boo"))
    }
}

#[cfg(test)]
mod test_or35 {
    use super::implementations::or35;
    use super::solutions::or35_solution;

    #[test]
    fn test_or35_1() {
        assert_eq!(or35(3), or35_solution(3))
    }
    #[test]
    fn test_or35_2() {
        assert_eq!(or35(10), or35_solution(10))
    }
    #[test]
    fn test_or35_3() {
        assert_eq!(or35(8), or35_solution(8))
    }
    #[test]
    fn test_or35_4() {
        assert_eq!(or35(15), or35_solution(15))
    }
    #[test]
    fn test_or35_5() {
        assert_eq!(or35(5), or35_solution(5))
    }
    #[test]
    fn test_or35_6() {
        assert_eq!(or35(4), or35_solution(4))
    }
    #[test]
    fn test_or35_7() {
        assert_eq!(or35(9), or35_solution(9))
    }
    #[test]
    fn test_or35_8() {
        assert_eq!(or35(4), or35_solution(4))
    }
    #[test]
    fn test_or35_9() {
        assert_eq!(or35(7), or35_solution(7))
    }
    #[test]
    fn test_or35_10() {
        assert_eq!(or35(6), or35_solution(6))
    }
    #[test]
    fn test_or35_11() {
        assert_eq!(or35(17), or35_solution(17))
    }
    #[test]
    fn test_or35_12() {
        assert_eq!(or35(18), or35_solution(18))
    }
    #[test]
    fn test_or35_13() {
        assert_eq!(or35(29), or35_solution(29))
    }
    #[test]
    fn test_or35_14() {
        assert_eq!(or35(20), or35_solution(20))
    }
    #[test]
    fn test_or35_15() {
        assert_eq!(or35(21), or35_solution(21))
    }
    #[test]
    fn test_or35_16() {
        assert_eq!(or35(22), or35_solution(22))
    }
    #[test]
    fn test_or35_17() {
        assert_eq!(or35(45), or35_solution(45))
    }
    #[test]
    fn test_or35_18() {
        assert_eq!(or35(99), or35_solution(99))
    }
    #[test]
    fn test_or35_19() {
        assert_eq!(or35(100), or35_solution(100))
    }
    #[test]
    fn test_or35_20() {
        assert_eq!(or35(101), or35_solution(101))
    }
    #[test]
    fn test_or35_21() {
        assert_eq!(or35(121), or35_solution(121))
    }
    #[test]
    fn test_or35_22() {
        assert_eq!(or35(122), or35_solution(122))
    }
    #[test]
    fn test_or35_23() {
        assert_eq!(or35(123), or35_solution(123))
    }
}

#[cfg(test)]
mod test_front22 {
    use super::implementations::front22;
    use super::solutions::front22_solution;

    #[test]
    fn test_front22_1() {
        assert_eq!(front22("kitten"), front22_solution("kitten"))
    }
    #[test]
    fn test_front22_2() {
        assert_eq!(front22("Ha"), front22_solution("Ha"))
    }
    #[test]
    fn test_front22_3() {
        assert_eq!(front22("abc"), front22_solution("abc"))
    }
    #[test]
    fn test_front22_4() {
        assert_eq!(front22("ab"), front22_solution("ab"))
    }
    #[test]
    fn test_front22_5() {
        assert_eq!(front22("a"), front22_solution("a"))
    }
    #[test]
    fn test_front22_6() {
        assert_eq!(front22(""), front22_solution(""))
    }
    #[test]
    fn test_front22_7() {
        assert_eq!(front22("Logic"), front22_solution("Logic"))
    }
}

#[cfg(test)]
mod test_start_hi {
    use super::implementations::start_hi;
    use super::solutions::start_hi_solution;

    #[test]
    fn test_start_hi_1() {
        assert_eq!(start_hi("hi there"), start_hi_solution("hi there"))
    }
    #[test]
    fn test_start_hi_2() {
        assert_eq!(start_hi("hi"), start_hi_solution("hi"))
    }
    #[test]
    fn test_start_hi_3() {
        assert_eq!(start_hi("hello hi"), start_hi_solution("hello hi"))
    }
    #[test]
    fn test_start_hi_4() {
        assert_eq!(start_hi("he"), start_hi_solution("he"))
    }
    #[test]
    fn test_start_hi_5() {
        assert_eq!(start_hi("h"), start_hi_solution("h"))
    }
    #[test]
    fn test_start_hi_6() {
        assert_eq!(start_hi(""), start_hi_solution(""))
    }
    #[test]
    fn test_start_hi_7() {
        assert_eq!(start_hi("ho hi"), start_hi_solution("ho hi"))
    }
    #[test]
    fn test_start_hi_8() {
        assert_eq!(start_hi("hi ho"), start_hi_solution("hi ho"))
    }
}

#[cfg(test)]
mod test_icy_hot {
    use super::implementations::icy_hot;
    use super::solutions::icy_hot_solution;

    #[test]
    fn test_icy_hot_1() {
        assert_eq!(icy_hot(120, -1), icy_hot_solution(120, -1))
    }
    #[test]
    fn test_icy_hot_2() {
        assert_eq!(icy_hot(-1, 120), icy_hot_solution(-1, 120))
    }
    #[test]
    fn test_icy_hot_3() {
        assert_eq!(icy_hot(2, 120), icy_hot_solution(2, 120))
    }
    #[test]
    fn test_icy_hot_4() {
        assert_eq!(icy_hot(-1, 100), icy_hot_solution(-1, 100))
    }
    #[test]
    fn test_icy_hot_5() {
        assert_eq!(icy_hot(-2, 120), icy_hot_solution(-2, 120))
    }
    #[test]
    fn test_icy_hot_6() {
        assert_eq!(icy_hot(120, 120), icy_hot_solution(120, 120))
    }
}

#[cfg(test)]
mod test_in1020 {
    use super::implementations::in1020;
    use super::solutions::in1020_solution;

    #[test]
    fn test_in1020_1() {
        assert_eq!(in1020(12, 99), in1020_solution(12, 99))
    }
    #[test]
    fn test_in1020_2() {
        assert_eq!(in1020(21, 12), in1020_solution(21, 12))
    }
    #[test]
    fn test_in1020_3() {
        assert_eq!(in1020(8, 99), in1020_solution(8, 99))
    }
    #[test]
    fn test_in1020_4() {
        assert_eq!(in1020(99, 10), in1020_solution(99, 10))
    }
    #[test]
    fn test_in1020_5() {
        assert_eq!(in1020(20, 20), in1020_solution(20, 20))
    }
    #[test]
    fn test_in1020_6() {
        assert_eq!(in1020(21, 21), in1020_solution(21, 21))
    }
    #[test]
    fn test_in1020_7() {
        assert_eq!(in1020(9, 9), in1020_solution(9, 9))
    }
}

#[cfg(test)]
mod test_has_teen {
    use super::implementations::has_teen;
    use super::solutions::has_teen_solution;

    #[test]
    fn test_has_teen_1() {
        assert_eq!(has_teen(13, 20, 10), has_teen_solution(13, 20, 10))
    }
    #[test]
    fn test_has_teen_2() {
        assert_eq!(has_teen(20, 19, 10), has_teen_solution(20, 19, 10))
    }
    #[test]
    fn test_has_teen_3() {
        assert_eq!(has_teen(20, 10, 13), has_teen_solution(20, 10, 13))
    }
    #[test]
    fn test_has_teen_4() {
        assert_eq!(has_teen(1, 20, 12), has_teen_solution(1, 20, 12))
    }
    #[test]
    fn test_has_teen_5() {
        assert_eq!(has_teen(19, 20, 12), has_teen_solution(19, 20, 12))
    }
    #[test]
    fn test_has_teen_6() {
        assert_eq!(has_teen(12, 20, 19), has_teen_solution(12, 20, 19))
    }
    #[test]
    fn test_has_teen_7() {
        assert_eq!(has_teen(12, 9, 20), has_teen_solution(12, 9, 20))
    }
    #[test]
    fn test_has_teen_8() {
        assert_eq!(has_teen(12, 18, 20), has_teen_solution(12, 18, 20))
    }
    #[test]
    fn test_has_teen_9() {
        assert_eq!(has_teen(14, 2, 20), has_teen_solution(14, 2, 20))
    }
    #[test]
    fn test_has_teen_10() {
        assert_eq!(has_teen(4, 2, 20), has_teen_solution(4, 2, 20))
    }
    #[test]
    fn test_has_teen_11() {
        assert_eq!(has_teen(11, 22, 22), has_teen_solution(11, 22, 22))
    }
}

#[cfg(test)]
mod test_lone_teen {
    use super::implementations::lone_teen;
    use super::solutions::lone_teen_solution;

    #[test]
    fn test_lone_teen_1() {
        assert_eq!(lone_teen(13, 99), lone_teen_solution(13, 99))
    }
    #[test]
    fn test_lone_teen_2() {
        assert_eq!(lone_teen(21, 19), lone_teen_solution(21, 19))
    }
    #[test]
    fn test_lone_teen_3() {
        assert_eq!(lone_teen(13, 13), lone_teen_solution(13, 13))
    }
    #[test]
    fn test_lone_teen_4() {
        assert_eq!(lone_teen(14, 20), lone_teen_solution(14, 20))
    }
    #[test]
    fn test_lone_teen_5() {
        assert_eq!(lone_teen(20, 15), lone_teen_solution(20, 15))
    }
    #[test]
    fn test_lone_teen_6() {
        assert_eq!(lone_teen(16, 17), lone_teen_solution(16, 17))
    }
    #[test]
    fn test_lone_teen_7() {
        assert_eq!(lone_teen(16, 9), lone_teen_solution(16, 9))
    }
    #[test]
    fn test_lone_teen_8() {
        assert_eq!(lone_teen(16, 18), lone_teen_solution(16, 18))
    }
    #[test]
    fn test_lone_teen_9() {
        assert_eq!(lone_teen(13, 19), lone_teen_solution(13, 19))
    }
    #[test]
    fn test_lone_teen_10() {
        assert_eq!(lone_teen(13, 20), lone_teen_solution(13, 20))
    }
    #[test]
    fn test_lone_teen_11() {
        assert_eq!(lone_teen(6, 18), lone_teen_solution(6, 18))
    }
    #[test]
    fn test_lone_teen_12() {
        assert_eq!(lone_teen(99, 13), lone_teen_solution(99, 13))
    }
    #[test]
    fn test_lone_teen_13() {
        assert_eq!(lone_teen(99, 99), lone_teen_solution(99, 99))
    }
}

#[cfg(test)]
mod test_del_del {
    use super::implementations::del_del;
    use super::solutions::del_del_solution;

    #[test]
    fn test_del_del_1() {
        assert_eq!(del_del("adelbc"), del_del_solution("adelbc"))
    }
    #[test]
    fn test_del_del_2() {
        assert_eq!(del_del("adelHello"), del_del_solution("adelHello"))
    }
    #[test]
    fn test_del_del_3() {
        assert_eq!(del_del("abcdel"), del_del_solution("abcdel"))
    }
    #[test]
    fn test_del_del_4() {
        assert_eq!(del_del("add"), del_del_solution("add"))
    }
    #[test]
    fn test_del_del_5() {
        assert_eq!(del_del("ad"), del_del_solution("ad"))
    }
    #[test]
    fn test_del_del_6() {
        assert_eq!(del_del("a"), del_del_solution("a"))
    }
    #[test]
    fn test_del_del_7() {
        assert_eq!(del_del(""), del_del_solution(""))
    }
    #[test]
    fn test_del_del_8() {
        assert_eq!(del_del("del"), del_del_solution("del"))
    }
    #[test]
    fn test_del_del_9() {
        assert_eq!(del_del("adel"), del_del_solution("adel"))
    }
    #[test]
    fn test_del_del_10() {
        assert_eq!(del_del("aadelbb"), del_del_solution("aadelbb"))
    }
}

#[cfg(test)]
mod test_mix_start {
    use super::implementations::mix_start;
    use super::solutions::mix_start_solution;

    #[test]
    fn test_mix_start_1() {
        assert_eq!(mix_start("mix snacks"), mix_start_solution("mix snacks"))
    }
    #[test]
    fn test_mix_start_2() {
        assert_eq!(mix_start("pix snacks"), mix_start_solution("pix snacks"))
    }
    #[test]
    fn test_mix_start_3() {
        assert_eq!(mix_start("piz snacks"), mix_start_solution("piz snacks"))
    }
    #[test]
    fn test_mix_start_4() {
        assert_eq!(mix_start("nix"), mix_start_solution("nix"))
    }
    #[test]
    fn test_mix_start_5() {
        assert_eq!(mix_start("ni"), mix_start_solution("ni"))
    }
    #[test]
    fn test_mix_start_6() {
        assert_eq!(mix_start("n"), mix_start_solution("n"))
    }
}

#[cfg(test)]
mod test_start_oz {
    use super::implementations::start_oz;
    use super::solutions::start_oz_solution;

    #[test]
    fn test_start_oz_1() {
        assert_eq!(start_oz("ozymandias"), start_oz_solution("ozymandias"))
    }
    #[test]
    fn test_start_oz_2() {
        assert_eq!(start_oz("bzoo"), start_oz_solution("bzoo"))
    }
    #[test]
    fn test_start_oz_3() {
        assert_eq!(start_oz("oxx"), start_oz_solution("oxx"))
    }
    #[test]
    fn test_start_oz_4() {
        assert_eq!(start_oz("ounce"), start_oz_solution("ounce"))
    }
    #[test]
    fn test_start_oz_5() {
        assert_eq!(start_oz("o"), start_oz_solution("o"))
    }
    #[test]
    fn test_start_oz_6() {
        assert_eq!(start_oz("abc"), start_oz_solution("abc"))
    }
    #[test]
    fn test_start_oz_7() {
        assert_eq!(start_oz(""), start_oz_solution(""))
    }
    #[test]
    fn test_start_oz_8() {
        assert_eq!(start_oz("zoo"), start_oz_solution("zoo"))
    }
    #[test]
    fn test_start_oz_9() {
        assert_eq!(start_oz("aztec"), start_oz_solution("aztec"))
    }
    #[test]
    fn test_start_oz_10() {
        assert_eq!(start_oz("zzzz"), start_oz_solution("zzzz"))
    }
    #[test]
    fn test_start_oz_11() {
        assert_eq!(start_oz("oznic"), start_oz_solution("oznic"))
    }
}

#[cfg(test)]
mod test_int_max {
    use super::implementations::int_max;
    use super::solutions::int_max_solution;

    #[test]
    fn test_int_max_1() {
        assert_eq!(int_max(1, 2, 3), int_max_solution(1, 2, 3))
    }
    #[test]
    fn test_int_max_2() {
        assert_eq!(int_max(1, 3, 2), int_max_solution(1, 3, 2))
    }
    #[test]
    fn test_int_max_3() {
        assert_eq!(int_max(3, 2, 1), int_max_solution(3, 2, 1))
    }
    #[test]
    fn test_int_max_4() {
        assert_eq!(int_max(9, 3, 3), int_max_solution(9, 3, 3))
    }
    #[test]
    fn test_int_max_5() {
        assert_eq!(int_max(3, 9, 3), int_max_solution(3, 9, 3))
    }
    #[test]
    fn test_int_max_6() {
        assert_eq!(int_max(3, 3, 9), int_max_solution(3, 3, 9))
    }
    #[test]
    fn test_int_max_7() {
        assert_eq!(int_max(8, 2, 3), int_max_solution(8, 2, 3))
    }
    #[test]
    fn test_int_max_8() {
        assert_eq!(int_max(-3, -1, -2), int_max_solution(-3, -1, -2))
    }
    #[test]
    fn test_int_max_9() {
        assert_eq!(int_max(6, 2, 5), int_max_solution(6, 2, 5))
    }
    #[test]
    fn test_int_max_10() {
        assert_eq!(int_max(5, 6, 2), int_max_solution(5, 6, 2))
    }
    #[test]
    fn test_int_max_11() {
        assert_eq!(int_max(5, 2, 6), int_max_solution(5, 2, 6))
    }
}

#[cfg(test)]
mod test_close10 {
    use super::implementations::close10;
    use super::solutions::close10_solution;

    #[test]
    fn test_close10_1() {
        assert_eq!(close10(8, 13), close10_solution(8, 13))
    }
    #[test]
    fn test_close10_2() {
        assert_eq!(close10(13, 8), close10_solution(13, 8))
    }
    #[test]
    fn test_close10_3() {
        assert_eq!(close10(13, 7), close10_solution(13, 7))
    }
    #[test]
    fn test_close10_4() {
        assert_eq!(close10(7, 13), close10_solution(7, 13))
    }
    #[test]
    fn test_close10_5() {
        assert_eq!(close10(9, 13), close10_solution(9, 13))
    }
    #[test]
    fn test_close10_6() {
        assert_eq!(close10(13, 8), close10_solution(13, 8))
    }
    #[test]
    fn test_close10_7() {
        assert_eq!(close10(10, 12), close10_solution(10, 12))
    }
    #[test]
    fn test_close10_8() {
        assert_eq!(close10(11, 10), close10_solution(11, 10))
    }
    #[test]
    fn test_close10_9() {
        assert_eq!(close10(5, 21), close10_solution(5, 21))
    }
    #[test]
    fn test_close10_10() {
        assert_eq!(close10(0, 20), close10_solution(0, 20))
    }
    #[test]
    fn test_close10_11() {
        assert_eq!(close10(0, 20), close10_solution(0, 20))
    }
    #[test]
    fn test_close10_12() {
        assert_eq!(close10(10, 10), close10_solution(10, 10))
    }
}

#[cfg(test)]
mod test_in3050 {
    use super::implementations::in3050;
    use super::solutions::in3050_solution;

    #[test]
    fn test_in3050_1() {
        assert_eq!(in3050(30, 31), in3050_solution(30, 31))
    }
    #[test]
    fn test_in3050_2() {
        assert_eq!(in3050(30, 41), in3050_solution(30, 41))
    }
    #[test]
    fn test_in3050_3() {
        assert_eq!(in3050(40, 50), in3050_solution(40, 50))
    }
    #[test]
    fn test_in3050_4() {
        assert_eq!(in3050(40, 51), in3050_solution(40, 51))
    }
    #[test]
    fn test_in3050_5() {
        assert_eq!(in3050(39, 50), in3050_solution(39, 50))
    }
    #[test]
    fn test_in3050_6() {
        assert_eq!(in3050(50, 39), in3050_solution(50, 39))
    }
    #[test]
    fn test_in3050_7() {
        assert_eq!(in3050(40, 39), in3050_solution(40, 39))
    }
    #[test]
    fn test_in3050_8() {
        assert_eq!(in3050(49, 48), in3050_solution(49, 48))
    }
    #[test]
    fn test_in3050_9() {
        assert_eq!(in3050(50, 40), in3050_solution(50, 40))
    }
    #[test]
    fn test_in3050_10() {
        assert_eq!(in3050(50, 51), in3050_solution(50, 51))
    }
    #[test]
    fn test_in3050_11() {
        assert_eq!(in3050(35, 36), in3050_solution(35, 36))
    }
    #[test]
    fn test_in3050_12() {
        assert_eq!(in3050(35, 45), in3050_solution(35, 45))
    }
}

#[cfg(test)]
mod test_max1020 {
    use super::implementations::max1020;
    use super::solutions::max1020_solution;

    #[test]
    fn test_max1020_1() {
        assert_eq!(max1020(11, 19), max1020_solution(11, 19))
    }
    #[test]
    fn test_max1020_2() {
        assert_eq!(max1020(19, 11), max1020_solution(19, 11))
    }
    #[test]
    fn test_max1020_3() {
        assert_eq!(max1020(11, 9), max1020_solution(11, 9))
    }
    #[test]
    fn test_max1020_4() {
        assert_eq!(max1020(9, 21), max1020_solution(9, 21))
    }
    #[test]
    fn test_max1020_5() {
        assert_eq!(max1020(10, 21), max1020_solution(10, 21))
    }
    #[test]
    fn test_max1020_6() {
        assert_eq!(max1020(21, 10), max1020_solution(21, 10))
    }
    #[test]
    fn test_max1020_7() {
        assert_eq!(max1020(9, 11), max1020_solution(9, 11))
    }
    #[test]
    fn test_max1020_8() {
        assert_eq!(max1020(23, 10), max1020_solution(23, 10))
    }
    #[test]
    fn test_max1020_9() {
        assert_eq!(max1020(20, 10), max1020_solution(20, 10))
    }
    #[test]
    fn test_max1020_10() {
        assert_eq!(max1020(7, 20), max1020_solution(7, 20))
    }
    #[test]
    fn test_max1020_11() {
        assert_eq!(max1020(17, 16), max1020_solution(17, 16))
    }
}

#[cfg(test)]
mod test_string_e {
    use super::implementations::string_e;
    use super::solutions::string_e_solution;

    #[test]
    fn test_string_e_1() {
        assert_eq!(string_e("Hello"), string_e_solution("Hello"))
    }
    #[test]
    fn test_string_e_2() {
        assert_eq!(string_e("Heelle"), string_e_solution("Heelle"))
    }
    #[test]
    fn test_string_e_3() {
        assert_eq!(string_e("Heelele"), string_e_solution("Heelele"))
    }
    #[test]
    fn test_string_e_4() {
        assert_eq!(string_e("HII"), string_e_solution("HII"))
    }
    #[test]
    fn test_string_e_5() {
        assert_eq!(string_e("e"), string_e_solution("e"))
    }
    #[test]
    fn test_string_e_6() {
        assert_eq!(string_e(""), string_e_solution(""))
    }
}

#[cfg(test)]
mod test_last_digit {
    use super::implementations::last_digit;
    use super::solutions::last_digit_solution;

    #[test]
    fn test_last_digit_1() {
        assert_eq!(last_digit(7, 17), last_digit_solution(7, 17))
    }
    #[test]
    fn test_last_digit_2() {
        assert_eq!(last_digit(6, 17), last_digit_solution(6, 17))
    }
    #[test]
    fn test_last_digit_3() {
        assert_eq!(last_digit(3, 113), last_digit_solution(3, 113))
    }
    #[test]
    fn test_last_digit_4() {
        assert_eq!(last_digit(114, 113), last_digit_solution(114, 113))
    }
    #[test]
    fn test_last_digit_5() {
        assert_eq!(last_digit(114, 4), last_digit_solution(114, 4))
    }
    #[test]
    fn test_last_digit_6() {
        assert_eq!(last_digit(10, 0), last_digit_solution(10, 0))
    }
    #[test]
    fn test_last_digit_7() {
        assert_eq!(last_digit(11, 0), last_digit_solution(11, 0))
    }
}

#[cfg(test)]
mod test_end_up {
    use super::implementations::end_up;
    use super::solutions::end_up_solution;

    #[test]
    fn test_end_up_1() {
        assert_eq!(end_up("Hello"), end_up_solution("Hello"))
    }
    #[test]
    fn test_end_up_2() {
        assert_eq!(end_up("hi there"), end_up_solution("hi there"))
    }
    #[test]
    fn test_end_up_3() {
        assert_eq!(end_up("hi"), end_up_solution("hi"))
    }
    #[test]
    fn test_end_up_4() {
        assert_eq!(end_up("woo hoo"), end_up_solution("woo hoo"))
    }
    #[test]
    fn test_end_up_5() {
        assert_eq!(end_up("xyz12"), end_up_solution("xyz12"))
    }
    #[test]
    fn test_end_up_6() {
        assert_eq!(end_up("x"), end_up_solution("x"))
    }
    #[test]
    fn test_end_up_7() {
        assert_eq!(end_up(""), end_up_solution(""))
    }
}

#[cfg(test)]
mod test_every_nth {
    use super::implementations::every_nth;
    use super::solutions::every_nth_solution;

    #[test]
    fn test_every_nth_1() {
        assert_eq!(every_nth("Miracle", 2), every_nth_solution("Miracle", 2))
    }
    #[test]
    fn test_every_nth_2() {
        assert_eq!(every_nth("abcdefg", 2), every_nth_solution("abcdefg", 2))
    }
    #[test]
    fn test_every_nth_3() {
        assert_eq!(every_nth("abcdefg", 3), every_nth_solution("abcdefg", 3))
    }
    #[test]
    fn test_every_nth_4() {
        assert_eq!(every_nth("Chocolate", 3), every_nth_solution("Chocolate", 3))
    }
    #[test]
    fn test_every_nth_5() {
        assert_eq!(every_nth("Chocolates", 3), every_nth_solution("Chocolates", 3))
    }
    #[test]
    fn test_every_nth_6() {
        assert_eq!(every_nth("Chocolates", 4), every_nth_solution("Chocolates", 4))
    }
    #[test]
    fn test_every_nth_7() {
        assert_eq!(every_nth("Chocolates", 100), every_nth_solution("Chocolates", 100))
    }
}

#[cfg(test)]
mod test_string_times {
    use super::implementations::string_times;
    use super::solutions::string_times_solution;

    #[test]
    fn test_string_times_1() {
        assert_eq!(string_times("Hi", 2), string_times_solution("Hi", 2))
    }
    #[test]
    fn test_string_times_2() {
        assert_eq!(string_times("Hi", 3), string_times_solution("Hi", 3))
    }
    #[test]
    fn test_string_times_3() {
        assert_eq!(string_times("Hi", 1), string_times_solution("Hi", 1))
    }
    #[test]
    fn test_string_times_4() {
        assert_eq!(string_times("Hi", 0), string_times_solution("Hi", 0))
    }
    #[test]
    fn test_string_times_5() {
        assert_eq!(string_times("Hi", 5), string_times_solution("Hi", 5))
    }
    #[test]
    fn test_string_times_6() {
        assert_eq!(string_times("Oh Boy!", 2), string_times_solution("Oh Boy!", 2))
    }
    #[test]
    fn test_string_times_7() {
        assert_eq!(string_times("x", 4), string_times_solution("x", 4))
    }
    #[test]
    fn test_string_times_8() {
        assert_eq!(string_times("", 4), string_times_solution("", 4))
    }
    #[test]
    fn test_string_times_9() {
        assert_eq!(string_times("code", 2), string_times_solution("code", 2))
    }
    #[test]
    fn test_string_times_10() {
        assert_eq!(string_times("code", 3), string_times_solution("code", 3))
    }
}

#[cfg(test)]
mod test_front_times {
    use super::implementations::front_times;
    use super::solutions::front_times_solution;

    #[test]
    fn test_front_times_1() {
        assert_eq!(front_times("Chocolate", 2), front_times_solution("Chocolate", 2))
    }
    #[test]
    fn test_front_times_2() {
        assert_eq!(front_times("Chocolate", 3), front_times_solution("Chocolate", 3))
    }
    #[test]
    fn test_front_times_3() {
        assert_eq!(front_times("Abc", 3), front_times_solution("Abc", 3))
    }
    #[test]
    fn test_front_times_4() {
        assert_eq!(front_times("Ab", 4), front_times_solution("Ab", 4))
    }
    #[test]
    fn test_front_times_5() {
        assert_eq!(front_times("A", 4), front_times_solution("A", 4))
    }
    #[test]
    fn test_front_times_6() {
        assert_eq!(front_times("", 4), front_times_solution("", 4))
    }
    #[test]
    fn test_front_times_7() {
        assert_eq!(front_times("Abc", 0), front_times_solution("Abc", 0))
    }
}

#[cfg(test)]
mod test_count_x_x {
    use super::implementations::count_x_x;
    use super::solutions::count_x_x_solution;

    #[test]
    fn test_count_x_x_1() {
        assert_eq!(count_x_x("abcxx"), count_x_x_solution("abcxx"))
    }
    #[test]
    fn test_count_x_x_2() {
        assert_eq!(count_x_x("xxx"), count_x_x_solution("xxx"))
    }
    #[test]
    fn test_count_x_x_3() {
        assert_eq!(count_x_x("xxxx"), count_x_x_solution("xxxx"))
    }
    #[test]
    fn test_count_x_x_4() {
        assert_eq!(count_x_x("abc"), count_x_x_solution("abc"))
    }
    #[test]
    fn test_count_x_x_5() {
        assert_eq!(count_x_x("Hello There"), count_x_x_solution("Hello There"))
    }
    #[test]
    fn test_count_x_x_6() {
        assert_eq!(count_x_x("Hexxo Thxxe"), count_x_x_solution("Hexxo Thxxe"))
    }
    #[test]
    fn test_count_x_x_7() {
        assert_eq!(count_x_x(""), count_x_x_solution(""))
    }
    #[test]
    fn test_count_x_x_8() {
        assert_eq!(count_x_x("Kittens"), count_x_x_solution("Kittens"))
    }
    #[test]
    fn test_count_x_x_9() {
        assert_eq!(count_x_x("Kittensxxx"), count_x_x_solution("Kittensxxx"))
    }
}

#[cfg(test)]
mod test_double_x {
    use super::implementations::double_x;
    use super::solutions::double_x_solution;

    #[test]
    fn test_double_x_1() {
        assert_eq!(double_x("axxbb"), double_x_solution("axxbb"))
    }
    #[test]
    fn test_double_x_2() {
        assert_eq!(double_x("axaxax"), double_x_solution("axaxax"))
    }
    #[test]
    fn test_double_x_3() {
        assert_eq!(double_x("xxxxx"), double_x_solution("xxxxx"))
    }
    #[test]
    fn test_double_x_4() {
        assert_eq!(double_x("xaxxx"), double_x_solution("xaxxx"))
    }
    #[test]
    fn test_double_x_5() {
        assert_eq!(double_x("aaaax"), double_x_solution("aaaax"))
    }
    #[test]
    fn test_double_x_6() {
        assert_eq!(double_x(""), double_x_solution(""))
    }
    #[test]
    fn test_double_x_7() {
        assert_eq!(double_x("abc"), double_x_solution("abc"))
    }
    #[test]
    fn test_double_x_8() {
        assert_eq!(double_x("x"), double_x_solution("x"))
    }
    #[test]
    fn test_double_x_9() {
        assert_eq!(double_x("xx"), double_x_solution("xx"))
    }
    #[test]
    fn test_double_x_10() {
        assert_eq!(double_x("xaxx"), double_x_solution("xaxx"))
    }
}

#[cfg(test)]
mod test_string_bits {
    use super::implementations::string_bits;
    use super::solutions::string_bits_solution;

    #[test]
    fn test_string_bits_1() {
        assert_eq!(string_bits("Hello"), string_bits_solution("Hello"))
    }
    #[test]
    fn test_string_bits_2() {
        assert_eq!(string_bits("Hi"), string_bits_solution("Hi"))
    }
    #[test]
    fn test_string_bits_3() {
        assert_eq!(string_bits("Heeololeo"), string_bits_solution("Heeololeo"))
    }
    #[test]
    fn test_string_bits_4() {
        assert_eq!(string_bits("HiHiHi"), string_bits_solution("HiHiHi"))
    }
    #[test]
    fn test_string_bits_5() {
        assert_eq!(string_bits(""), string_bits_solution(""))
    }
    #[test]
    fn test_string_bits_6() {
        assert_eq!(string_bits("Greetings"), string_bits_solution("Greetings"))
    }
    #[test]
    fn test_string_bits_7() {
        assert_eq!(string_bits("Chocolate"), string_bits_solution("Chocolate"))
    }
    #[test]
    fn test_string_bits_8() {
        assert_eq!(string_bits("pi"), string_bits_solution("pi"))
    }
    #[test]
    fn test_string_bits_9() {
        assert_eq!(string_bits("Hello Kitten"), string_bits_solution("Hello Kitten"))
    }
    #[test]
    fn test_string_bits_10() {
        assert_eq!(string_bits("hxaxpxpxy"), string_bits_solution("hxaxpxpxy"))
    }
}

#[cfg(test)]
mod test_last2 {
    use super::implementations::last2;
    use super::solutions::last2_solution;

    #[test]
    fn test_last2_1() {
        assert_eq!(last2("hixxhi"), last2_solution("hixxhi"))
    }
    #[test]
    fn test_last2_2() {
        assert_eq!(last2("xaxxaxaxx"), last2_solution("xaxxaxaxx"))
    }
    #[test]
    fn test_last2_3() {
        assert_eq!(last2("axxaaxx"), last2_solution("axxaaxx"))
    }
    #[test]
    fn test_last2_4() {
        assert_eq!(last2("xxaxxaxxaxx"), last2_solution("xxaxxaxxaxx"))
    }
    #[test]
    fn test_last2_5() {
        assert_eq!(last2("xaxaxaa"), last2_solution("xaxaxaa"))
    }
    #[test]
    fn test_last2_6() {
        assert_eq!(last2("xxxx"), last2_solution("xxxx"))
    }
    #[test]
    fn test_last2_7() {
        assert_eq!(last2("13121312"), last2_solution("13121312"))
    }
    #[test]
    fn test_last2_8() {
        assert_eq!(last2("11212"), last2_solution("11212"))
    }
    #[test]
    fn test_last2_9() {
        assert_eq!(last2("13121311"), last2_solution("13121311"))
    }
    #[test]
    fn test_last2_10() {
        assert_eq!(last2("hi"), last2_solution("hi"))
    }
    #[test]
    fn test_last2_11() {
        assert_eq!(last2("h"), last2_solution("h"))
    }
    #[test]
    fn test_last2_12() {
        assert_eq!(last2(""), last2_solution(""))
    }
}

#[cfg(test)]
mod test_array_count9 {
    use super::implementations::array_count9;
    use super::solutions::array_count9_solution;

    #[test]
    fn test_array_count9_1() {
        assert_eq!(array_count9(vec![1, 2, 9]), array_count9_solution(vec![1, 2, 9]))
    }
    #[test]
    fn test_array_count9_2() {
        assert_eq!(array_count9(vec![1, 9, 9]), array_count9_solution(vec![1, 9, 9]))
    }
    #[test]
    fn test_array_count9_3() {
        assert_eq!(array_count9(vec![1, 9, 9, 3, 9]), array_count9_solution(vec![1, 9, 9, 3, 9]))
    }
    #[test]
    fn test_array_count9_4() {
        assert_eq!(array_count9(vec![1, 2, 3]), array_count9_solution(vec![1, 2, 3]))
    }
    #[test]
    fn test_array_count9_5() {
        assert_eq!(array_count9(vec![]), array_count9_solution(vec![]))
    }
    #[test]
    fn test_array_count9_6() {
        assert_eq!(array_count9(vec![4, 2, 4, 3, 1]), array_count9_solution(vec![4, 2, 4, 3, 1]))
    }
    #[test]
    fn test_array_count9_7() {
        assert_eq!(array_count9(vec![9, 2, 4, 3, 1]), array_count9_solution(vec![9, 2, 4, 3, 1]))
    }
}

#[cfg(test)]
mod test_array_front9 {
    use super::implementations::array_front9;
    use super::solutions::array_front9_solution;

    #[test]
    fn test_array_front9_1() {
        assert_eq!(array_front9(vec![1, 2, 9, 3, 4]), array_front9_solution(vec![1, 2, 9, 3, 4]))
    }
    #[test]
    fn test_array_front9_2() {
        assert_eq!(array_front9(vec![1, 2, 3, 4, 9]), array_front9_solution(vec![1, 2, 3, 4, 9]))
    }
    #[test]
    fn test_array_front9_3() {
        assert_eq!(array_front9(vec![1, 2, 3, 4, 5]), array_front9_solution(vec![1, 2, 3, 4, 5]))
    }
    #[test]
    fn test_array_front9_4() {
        assert_eq!(array_front9(vec![9, 2, 3]), array_front9_solution(vec![9, 2, 3]))
    }
    #[test]
    fn test_array_front9_5() {
        assert_eq!(array_front9(vec![1, 9, 9]), array_front9_solution(vec![1, 9, 9]))
    }
    #[test]
    fn test_array_front9_6() {
        assert_eq!(array_front9(vec![1, 2, 3]), array_front9_solution(vec![1, 2, 3]))
    }
    #[test]
    fn test_array_front9_7() {
        assert_eq!(array_front9(vec![1, 9]), array_front9_solution(vec![1, 9]))
    }
    #[test]
    fn test_array_front9_8() {
        assert_eq!(array_front9(vec![5, 5]), array_front9_solution(vec![5, 5]))
    }
    #[test]
    fn test_array_front9_9() {
        assert_eq!(array_front9(vec![2]), array_front9_solution(vec![2]))
    }
    #[test]
    fn test_array_front9_10() {
        assert_eq!(array_front9(vec![9]), array_front9_solution(vec![9]))
    }
    #[test]
    fn test_array_front9_11() {
        assert_eq!(array_front9(vec![]), array_front9_solution(vec![]))
    }
    #[test]
    fn test_array_front9_12() {
        assert_eq!(array_front9(vec![3, 9, 2, 3, 3]), array_front9_solution(vec![3, 9, 2, 3, 3]))
    }
}

#[cfg(test)]
mod test_string_match {
    use super::implementations::string_match;
    use super::solutions::string_match_solution;

    #[test]
    fn test_string_match_1() {
        assert_eq!(string_match("xxcaazz", "xxbaaz"), string_match_solution("xxcaazz", "xxbaaz"))
    }
    #[test]
    fn test_string_match_2() {
        assert_eq!(string_match("abc", "abc"), string_match_solution("abc", "abc"))
    }
    #[test]
    fn test_string_match_3() {
        assert_eq!(string_match("abc", "axc"), string_match_solution("abc", "axc"))
    }
    #[test]
    fn test_string_match_4() {
        assert_eq!(string_match("hello", "he"), string_match_solution("hello", "he"))
    }
    #[test]
    fn test_string_match_5() {
        assert_eq!(string_match("he", "hello"), string_match_solution("he", "hello"))
    }
    #[test]
    fn test_string_match_6() {
        assert_eq!(string_match("", "hello"), string_match_solution("", "hello"))
    }
    #[test]
    fn test_string_match_7() {
        assert_eq!(string_match("aabbccdd", "abbbxxd"), string_match_solution("aabbccdd", "abbbxxd"))
    }
    #[test]
    fn test_string_match_8() {
        assert_eq!(string_match("aaxxaaxx", "iaxxai"), string_match_solution("aaxxaaxx", "iaxxai"))
    }
    #[test]
    fn test_string_match_9() {
        assert_eq!(string_match("iaxxai", "aaxxaaxx"), string_match_solution("iaxxai", "aaxxaaxx"))
    }
}

#[cfg(test)]
mod test_string_x {
    use super::implementations::string_x;
    use super::solutions::string_x_solution;

    #[test]
    fn test_string_x_1() {
        assert_eq!(string_x("xxHxix"), string_x_solution("xxHxix"))
    }
    #[test]
    fn test_string_x_2() {
        assert_eq!(string_x("abxxxcd"), string_x_solution("abxxxcd"))
    }
    #[test]
    fn test_string_x_3() {
        assert_eq!(string_x("xabxxxcdx"), string_x_solution("xabxxxcdx"))
    }
    #[test]
    fn test_string_x_4() {
        assert_eq!(string_x("xKittenx"), string_x_solution("xKittenx"))
    }
    #[test]
    fn test_string_x_5() {
        assert_eq!(string_x("Hello"), string_x_solution("Hello"))
    }
    #[test]
    fn test_string_x_6() {
        assert_eq!(string_x("xx"), string_x_solution("xx"))
    }
    #[test]
    fn test_string_x_7() {
        assert_eq!(string_x("x"), string_x_solution("x"))
    }
    #[test]
    fn test_string_x_8() {
        assert_eq!(string_x(""), string_x_solution(""))
    }
}

#[cfg(test)]
mod test_alt_pairs {
    use super::implementations::alt_pairs;
    use super::solutions::alt_pairs_solution;

    #[test]
    fn test_alt_pairs_1() {
        assert_eq!(alt_pairs("kitten"), alt_pairs_solution("kitten"))
    }
    #[test]
    fn test_alt_pairs_2() {
        assert_eq!(alt_pairs("Chocolate"), alt_pairs_solution("Chocolate"))
    }
    #[test]
    fn test_alt_pairs_3() {
        assert_eq!(alt_pairs("CodingHorror"), alt_pairs_solution("CodingHorror"))
    }
    #[test]
    fn test_alt_pairs_4() {
        assert_eq!(alt_pairs("yak"), alt_pairs_solution("yak"))
    }
    #[test]
    fn test_alt_pairs_5() {
        assert_eq!(alt_pairs("ya"), alt_pairs_solution("ya"))
    }
    #[test]
    fn test_alt_pairs_6() {
        assert_eq!(alt_pairs("y"), alt_pairs_solution("y"))
    }
    #[test]
    fn test_alt_pairs_7() {
        assert_eq!(alt_pairs(""), alt_pairs_solution(""))
    }
    #[test]
    fn test_alt_pairs_8() {
        assert_eq!(alt_pairs("ThisThatTheOther"), alt_pairs_solution("ThisThatTheOther"))
    }
}

#[cfg(test)]
mod test_string_yak {
    use super::implementations::string_yak;
    use super::solutions::string_yak_solution;

    #[test]
    fn test_string_yak_1() {
        assert_eq!(string_yak("yakpak"), string_yak_solution("yakpak"))
    }
    #[test]
    fn test_string_yak_2() {
        assert_eq!(string_yak("pakyak"), string_yak_solution("pakyak"))
    }
    #[test]
    fn test_string_yak_3() {
        assert_eq!(string_yak("yak123ya"), string_yak_solution("yak123ya"))
    }
    #[test]
    fn test_string_yak_4() {
        assert_eq!(string_yak("yak"), string_yak_solution("yak"))
    }
    #[test]
    fn test_string_yak_5() {
        assert_eq!(string_yak("yakxxxyak"), string_yak_solution("yakxxxyak"))
    }
    #[test]
    fn test_string_yak_6() {
        assert_eq!(string_yak("xxcaazz"), string_yak_solution("xxcaazz"))
    }
    #[test]
    fn test_string_yak_7() {
        assert_eq!(string_yak("hiyakHi"), string_yak_solution("hiyakHi"))
    }
    #[test]
    fn test_string_yak_8() {
        assert_eq!(string_yak("xxxyakyyyakzzz"), string_yak_solution("xxxyakyyyakzzz"))
    }
}

#[cfg(test)]
mod test_array667 {
    use super::implementations::array667;
    use super::solutions::array667_solution;

    #[test]
    fn test_array667_1() {
        assert_eq!(array667(vec![6, 6, 2]), array667_solution(vec![6, 6, 2]))
    }
    #[test]
    fn test_array667_2() {
        assert_eq!(array667(vec![6, 6, 2, 6]), array667_solution(vec![6, 6, 2, 6]))
    }
    #[test]
    fn test_array667_3() {
        assert_eq!(array667(vec![6, 7, 2, 6]), array667_solution(vec![6, 7, 2, 6]))
    }
    #[test]
    fn test_array667_4() {
        assert_eq!(array667(vec![6, 6, 2, 7, 6, 7]), array667_solution(vec![6, 6, 2, 7, 6, 7]))
    }
    #[test]
    fn test_array667_5() {
        assert_eq!(array667(vec![1, 6, 3]), array667_solution(vec![1, 6, 3]))
    }
    #[test]
    fn test_array667_6() {
        assert_eq!(array667(vec![6, 1]), array667_solution(vec![6, 1]))
    }
    #[test]
    fn test_array667_7() {
        assert_eq!(array667(vec![]), array667_solution(vec![]))
    }
    #[test]
    fn test_array667_8() {
        assert_eq!(array667(vec![3, 6, 7, 6]), array667_solution(vec![3, 6, 7, 6]))
    }
    #[test]
    fn test_array667_9() {
        assert_eq!(array667(vec![3, 6, 6, 7]), array667_solution(vec![3, 6, 6, 7]))
    }
    #[test]
    fn test_array667_10() {
        assert_eq!(array667(vec![6, 3, 6, 6]), array667_solution(vec![6, 3, 6, 6]))
    }
    #[test]
    fn test_array667_11() {
        assert_eq!(array667(vec![6, 7, 6, 6]), array667_solution(vec![6, 7, 6, 6]))
    }
    #[test]
    fn test_array667_12() {
        assert_eq!(array667(vec![1, 2, 3, 5, 6]), array667_solution(vec![1, 2, 3, 5, 6]))
    }
    #[test]
    fn test_array667_13() {
        assert_eq!(array667(vec![1, 2, 3, 6, 6]), array667_solution(vec![1, 2, 3, 6, 6]))
    }
}

#[cfg(test)]
mod test_no_triples {
    use super::implementations::no_triples;
    use super::solutions::no_triples_solution;

    #[test]
    fn test_no_triples_1() {
        assert_eq!(no_triples(vec![1, 1, 2, 2, 1]), no_triples_solution(vec![1, 1, 2, 2, 1]))
    }
    #[test]
    fn test_no_triples_2() {
        assert_eq!(no_triples(vec![1, 1, 2, 2, 2, 1]), no_triples_solution(vec![1, 1, 2, 2, 2, 1]))
    }
    #[test]
    fn test_no_triples_3() {
        assert_eq!(no_triples(vec![1, 1, 2, 2, 2, 1]), no_triples_solution(vec![1, 1, 2, 2, 2, 1]))
    }
    #[test]
    fn test_no_triples_4() {
        assert_eq!(no_triples(vec![1, 2, 1]), no_triples_solution(vec![1, 2, 1]))
    }
    #[test]
    fn test_no_triples_5() {
        assert_eq!(no_triples(vec![1, 1, 1]), no_triples_solution(vec![1, 1, 1]))
    }
    #[test]
    fn test_no_triples_6() {
        assert_eq!(no_triples(vec![1, 1]), no_triples_solution(vec![1, 1]))
    }
    #[test]
    fn test_no_triples_7() {
        assert_eq!(no_triples(vec![1]), no_triples_solution(vec![1]))
    }
    #[test]
    fn test_no_triples_8() {
        assert_eq!(no_triples(vec![1]), no_triples_solution(vec![1]))
    }
    #[test]
    fn test_no_triples_9() {
        assert_eq!(no_triples(vec![]), no_triples_solution(vec![]))
    }
}

#[cfg(test)]
mod test_has271 {
    use super::implementations::has271;
    use super::solutions::has271_solution;

    #[test]
    fn test_has271_1() {
        assert_eq!(has271(vec![1, 2, 7, 1]), has271_solution(vec![1, 2, 7, 1]))
    }
    #[test]
    fn test_has271_2() {
        assert_eq!(has271(vec![1, 2, 8, 1]), has271_solution(vec![1, 2, 8, 1]))
    }
    #[test]
    fn test_has271_3() {
        assert_eq!(has271(vec![2, 7, 1]), has271_solution(vec![2, 7, 1]))
    }
    #[test]
    fn test_has271_4() {
        assert_eq!(has271(vec![3, 8, 2]), has271_solution(vec![3, 8, 2]))
    }
    #[test]
    fn test_has271_5() {
        assert_eq!(has271(vec![2, 7, 3]), has271_solution(vec![2, 7, 3]))
    }
    #[test]
    fn test_has271_6() {
        assert_eq!(has271(vec![2, 7, 4]), has271_solution(vec![2, 7, 4]))
    }
    #[test]
    fn test_has271_7() {
        assert_eq!(has271(vec![2, 7, -1]), has271_solution(vec![2, 7, -1]))
    }
    #[test]
    fn test_has271_8() {
        assert_eq!(has271(vec![2, 7, -2]), has271_solution(vec![2, 7, -2]))
    }
    #[test]
    fn test_has271_9() {
        assert_eq!(has271(vec![4, 5, 3, 8, 0]), has271_solution(vec![4, 5, 3, 8, 0]))
    }
    #[test]
    fn test_has271_10() {
        assert_eq!(has271(vec![2, 7, 5, 10, 4]), has271_solution(vec![2, 7, 5, 10, 4]))
    }
    #[test]
    fn test_has271_11() {
        assert_eq!(has271(vec![2, 7, -2, 4, 9, 3]), has271_solution(vec![2, 7, -2, 4, 9, 3]))
    }
    #[test]
    fn test_has271_12() {
        assert_eq!(has271(vec![2, 7, 5, 10, 1]), has271_solution(vec![2, 7, 5, 10, 1]))
    }
    #[test]
    fn test_has271_13() {
        assert_eq!(has271(vec![2, 7, -2, 10, 2]), has271_solution(vec![2, 7, -2, 10, 2]))
    }
}
