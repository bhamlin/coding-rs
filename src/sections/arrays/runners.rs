use super::implementations::*;
use super::solutions::*;
use crate::display::{BatVec, TestDisplay};

pub fn run(test: Option<Vec<String>>) {
    match test {
        // None => println!("Please specify a test name"),
        None => {
            first_last6_run();
            same_first_last_run();
            make_pi_run();
            common_end_run();
            sum3_run();
            rotate_left3_run();
            reverse3_run();
            max_end3_run();
            sum2_run();
            middle_way_run();
            make_ends_run();
            has23_run();
            no23_run();
            make_last_run();
            double23_run();
            fix23_run();
            start1_run();
            bigger_two_run();
            make_middle_run();
            plus_two_run();
            swap_ends_run();
            mid_three_run();
            max_triple_run();
            front_piece_run();
            unlucky1_run();
            make2_run();
            front11_run();
            find_lowest_index_run();
            count_evens_run();
            big_diff_run();
            centered_average_run();
            sum13_run();
            sum67_run();
            has22_run();
            lucky13_run();
            sum28_run();
            more14_run();
            prepend_sum_run();
            fizz_array_run();
            only14_run();
            fizz_array2_run();
            no14_run();
            is_everywhere_run();
            either24_run();
            match_up_run();
            has77_run();
            has12_run();
            mod_three_run();
            find_the_median_run();
            have_three_run();
            two_two_run();
            same_ends_run();
            triple_up_run();
            fizz_array3_run();
            shift_left_run();
            ten_run_run();
            pre4_run();
            post4_run();
            not_alone_run();
            zero_front_run();
            without_ten_run();
            zero_max_run();
            even_odd_run();
            fizz_buzz_run();
            max_span_run();
            fix34_run();
            fix45_run();
            can_balance_run();
            linear_in_run();
            square_up_run();
            series_up_run();
            max_mirror_run();
            count_clumps_run();
        }
        Some(tests) => {
            for test_name in tests {
                match test_name.to_ascii_lowercase().as_str() {
                    "firstLast6" | "first_last6" => first_last6_run(),
                    "sameFirstLast" | "same_first_last" => same_first_last_run(),
                    "makePi" | "make_pi" => make_pi_run(),
                    "commonEnd" | "common_end" => common_end_run(),
                    "sum3" => sum3_run(),
                    "rotateLeft3" | "rotate_left3" => rotate_left3_run(),
                    "reverse3" => reverse3_run(),
                    "maxEnd3" | "max_end3" => max_end3_run(),
                    "sum2" => sum2_run(),
                    "middleWay" | "middle_way" => middle_way_run(),
                    "makeEnds" | "make_ends" => make_ends_run(),
                    "has23" => has23_run(),
                    "no23" => no23_run(),
                    "makeLast" | "make_last" => make_last_run(),
                    "double23" => double23_run(),
                    "fix23" => fix23_run(),
                    "start1" => start1_run(),
                    "biggerTwo" | "bigger_two" => bigger_two_run(),
                    "makeMiddle" | "make_middle" => make_middle_run(),
                    "plusTwo" | "plus_two" => plus_two_run(),
                    "swapEnds" | "swap_ends" => swap_ends_run(),
                    "midThree" | "mid_three" => mid_three_run(),
                    "maxTriple" | "max_triple" => max_triple_run(),
                    "frontPiece" | "front_piece" => front_piece_run(),
                    "unlucky1" => unlucky1_run(),
                    "make2" => make2_run(),
                    "front11" => front11_run(),
                    "findLowestIndex" | "find_lowest_index" => find_lowest_index_run(),
                    "countEvens" | "count_evens" => count_evens_run(),
                    "bigDiff" | "big_diff" => big_diff_run(),
                    "centeredAverage" | "centered_average" => centered_average_run(),
                    "sum13" => sum13_run(),
                    "sum67" => sum67_run(),
                    "has22" => has22_run(),
                    "lucky13" => lucky13_run(),
                    "sum28" => sum28_run(),
                    "more14" => more14_run(),
                    "prependSum" | "prepend_sum" => prepend_sum_run(),
                    "fizzArray" | "fizz_array" => fizz_array_run(),
                    "only14" => only14_run(),
                    "fizzArray2" | "fizz_array2" => fizz_array2_run(),
                    "no14" => no14_run(),
                    "isEverywhere" | "is_everywhere" => is_everywhere_run(),
                    "either24" => either24_run(),
                    "matchUp" | "match_up" => match_up_run(),
                    "has77" => has77_run(),
                    "has12" => has12_run(),
                    "modThree" | "mod_three" => mod_three_run(),
                    "findTheMedian" | "find_the_median" => find_the_median_run(),
                    "haveThree" | "have_three" => have_three_run(),
                    "twoTwo" | "two_two" => two_two_run(),
                    "sameEnds" | "same_ends" => same_ends_run(),
                    "tripleUp" | "triple_up" => triple_up_run(),
                    "fizzArray3" | "fizz_array3" => fizz_array3_run(),
                    "shiftLeft" | "shift_left" => shift_left_run(),
                    "tenRun" | "ten_run" => ten_run_run(),
                    "pre4" => pre4_run(),
                    "post4" => post4_run(),
                    "notAlone" | "not_alone" => not_alone_run(),
                    "zeroFront" | "zero_front" => zero_front_run(),
                    "withoutTen" | "without_ten" => without_ten_run(),
                    "zeroMax" | "zero_max" => zero_max_run(),
                    "evenOdd" | "even_odd" => even_odd_run(),
                    "fizzBuzz" | "fizz_buzz" => fizz_buzz_run(),
                    "maxSpan" | "max_span" => max_span_run(),
                    "fix34" => fix34_run(),
                    "fix45" => fix45_run(),
                    "canBalance" | "can_balance" => can_balance_run(),
                    "linearIn" | "linear_in" => linear_in_run(),
                    "squareUp" | "square_up" => square_up_run(),
                    "seriesUp" | "series_up" => series_up_run(),
                    "maxMirror" | "max_mirror" => max_mirror_run(),
                    "countClumps" | "count_clumps" => count_clumps_run(),
                    _ => println!("No test named {test_name}"),
                };
            }
        }
    };
}

pub fn first_last6_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("first_last6");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 2, 6])), first_last6_solution(vec![1, 2, 6]), first_last6(vec![1, 2, 6]));
    layout.print_test(format!("{:?}", (vec![6, 1, 2, 3])), first_last6_solution(vec![6, 1, 2, 3]), first_last6(vec![6, 1, 2, 3]));
    layout.print_test(format!("{:?}", (vec![13, 6, 1, 2, 3])), first_last6_solution(vec![13, 6, 1, 2, 3]), first_last6(vec![13, 6, 1, 2, 3]));
    layout.print_test(format!("{:?}", (vec![13, 6, 1, 2, 6])), first_last6_solution(vec![13, 6, 1, 2, 6]), first_last6(vec![13, 6, 1, 2, 6]));
    layout.print_test(format!("{:?}", (vec![3, 2, 1])), first_last6_solution(vec![3, 2, 1]), first_last6(vec![3, 2, 1]));
    layout.print_test(format!("{:?}", (vec![3, 6, 1])), first_last6_solution(vec![3, 6, 1]), first_last6(vec![3, 6, 1]));
    layout.print_test(format!("{:?}", (vec![3, 6])), first_last6_solution(vec![3, 6]), first_last6(vec![3, 6]));
    layout.print_test(format!("{:?}", (vec![6])), first_last6_solution(vec![6]), first_last6(vec![6]));
    layout.print_test(format!("{:?}", (vec![3])), first_last6_solution(vec![3]), first_last6(vec![3]));
    layout.print_test(format!("{:?}", (vec![5, 6])), first_last6_solution(vec![5, 6]), first_last6(vec![5, 6]));
    layout.print_test(format!("{:?}", (vec![5, 5])), first_last6_solution(vec![5, 5]), first_last6(vec![5, 5]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3, 4, 6])), first_last6_solution(vec![1, 2, 3, 4, 6]), first_last6(vec![1, 2, 3, 4, 6]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3, 4])), first_last6_solution(vec![1, 2, 3, 4]), first_last6(vec![1, 2, 3, 4]));
    layout.print_footer();
}

pub fn same_first_last_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("same_first_last");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 2, 3])), same_first_last_solution(vec![1, 2, 3]), same_first_last(vec![1, 2, 3]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3, 1])), same_first_last_solution(vec![1, 2, 3, 1]), same_first_last(vec![1, 2, 3, 1]));
    layout.print_test(format!("{:?}", (vec![1, 2, 1])), same_first_last_solution(vec![1, 2, 1]), same_first_last(vec![1, 2, 1]));
    layout.print_test(format!("{:?}", (vec![7])), same_first_last_solution(vec![7]), same_first_last(vec![7]));
    layout.print_test(format!("{:?}", (vec![0; 0])), same_first_last_solution(vec![0; 0]), same_first_last(vec![0; 0]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3, 4, 5, 1])), same_first_last_solution(vec![1, 2, 3, 4, 5, 1]), same_first_last(vec![1, 2, 3, 4, 5, 1]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3, 4, 5, 13])), same_first_last_solution(vec![1, 2, 3, 4, 5, 13]), same_first_last(vec![1, 2, 3, 4, 5, 13]));
    layout.print_test(format!("{:?}", (vec![13, 2, 3, 4, 5, 13])), same_first_last_solution(vec![13, 2, 3, 4, 5, 13]), same_first_last(vec![13, 2, 3, 4, 5, 13]));
    layout.print_test(format!("{:?}", (vec![7, 7])), same_first_last_solution(vec![7, 7]), same_first_last(vec![7, 7]));
    layout.print_footer();
}

pub fn make_pi_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("make_pi");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(format!("{:?}", ()), make_pi_solution().into(), make_pi().into());
    layout.print_footer();
}

pub fn common_end_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("common_end");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 2, 3], vec![7, 3])), common_end_solution(vec![1, 2, 3], vec![7, 3]), common_end(vec![1, 2, 3], vec![7, 3]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3], vec![7, 3, 2])), common_end_solution(vec![1, 2, 3], vec![7, 3, 2]), common_end(vec![1, 2, 3], vec![7, 3, 2]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3], vec![1, 3])), common_end_solution(vec![1, 2, 3], vec![1, 3]), common_end(vec![1, 2, 3], vec![1, 3]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3], vec![1])), common_end_solution(vec![1, 2, 3], vec![1]), common_end(vec![1, 2, 3], vec![1]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3], vec![2])), common_end_solution(vec![1, 2, 3], vec![2]), common_end(vec![1, 2, 3], vec![2]));
    layout.print_footer();
}

pub fn sum3_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("sum3");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 2, 3])), sum3_solution(vec![1, 2, 3]), sum3(vec![1, 2, 3]));
    layout.print_test(format!("{:?}", (vec![5, 11, 2])), sum3_solution(vec![5, 11, 2]), sum3(vec![5, 11, 2]));
    layout.print_test(format!("{:?}", (vec![7, 0, 0])), sum3_solution(vec![7, 0, 0]), sum3(vec![7, 0, 0]));
    layout.print_test(format!("{:?}", (vec![1, 2, 1])), sum3_solution(vec![1, 2, 1]), sum3(vec![1, 2, 1]));
    layout.print_test(format!("{:?}", (vec![1, 1, 1])), sum3_solution(vec![1, 1, 1]), sum3(vec![1, 1, 1]));
    layout.print_test(format!("{:?}", (vec![2, 7, 2])), sum3_solution(vec![2, 7, 2]), sum3(vec![2, 7, 2]));
    layout.print_footer();
}

pub fn rotate_left3_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("rotate_left3");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2, 3])), rotate_left3_solution(vec![1, 2, 3]).into(), rotate_left3(vec![1, 2, 3]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![5, 11, 9])), rotate_left3_solution(vec![5, 11, 9]).into(), rotate_left3(vec![5, 11, 9]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![7, 0, 0])), rotate_left3_solution(vec![7, 0, 0]).into(), rotate_left3(vec![7, 0, 0]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2, 1])), rotate_left3_solution(vec![1, 2, 1]).into(), rotate_left3(vec![1, 2, 1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![0, 0, 1])), rotate_left3_solution(vec![0, 0, 1]).into(), rotate_left3(vec![0, 0, 1]).into());
    layout.print_footer();
}

pub fn reverse3_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("reverse3");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2, 3])), reverse3_solution(vec![1, 2, 3]).into(), reverse3(vec![1, 2, 3]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![5, 11, 9])), reverse3_solution(vec![5, 11, 9]).into(), reverse3(vec![5, 11, 9]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![7, 0, 0])), reverse3_solution(vec![7, 0, 0]).into(), reverse3(vec![7, 0, 0]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![2, 1, 2])), reverse3_solution(vec![2, 1, 2]).into(), reverse3(vec![2, 1, 2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2, 1])), reverse3_solution(vec![1, 2, 1]).into(), reverse3(vec![1, 2, 1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![2, 11, 3])), reverse3_solution(vec![2, 11, 3]).into(), reverse3(vec![2, 11, 3]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![0, 6, 5])), reverse3_solution(vec![0, 6, 5]).into(), reverse3(vec![0, 6, 5]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![7, 2, 3])), reverse3_solution(vec![7, 2, 3]).into(), reverse3(vec![7, 2, 3]).into());
    layout.print_footer();
}

pub fn max_end3_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("max_end3");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2, 3])), max_end3_solution(vec![1, 2, 3]).into(), max_end3(vec![1, 2, 3]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![11, 5, 9])), max_end3_solution(vec![11, 5, 9]).into(), max_end3(vec![11, 5, 9]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![2, 11, 3])), max_end3_solution(vec![2, 11, 3]).into(), max_end3(vec![2, 11, 3]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![11, 3, 3])), max_end3_solution(vec![11, 3, 3]).into(), max_end3(vec![11, 3, 3]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![3, 11, 11])), max_end3_solution(vec![3, 11, 11]).into(), max_end3(vec![3, 11, 11]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![2, 2, 2])), max_end3_solution(vec![2, 2, 2]).into(), max_end3(vec![2, 2, 2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![2, 11, 2])), max_end3_solution(vec![2, 11, 2]).into(), max_end3(vec![2, 11, 2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![0, 0, 1])), max_end3_solution(vec![0, 0, 1]).into(), max_end3(vec![0, 0, 1]).into());
    layout.print_footer();
}

pub fn sum2_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("sum2");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 2, 3])), sum2_solution(vec![1, 2, 3]), sum2(vec![1, 2, 3]));
    layout.print_test(format!("{:?}", (vec![1, 1])), sum2_solution(vec![1, 1]), sum2(vec![1, 1]));
    layout.print_test(format!("{:?}", (vec![1, 1, 1, 1])), sum2_solution(vec![1, 1, 1, 1]), sum2(vec![1, 1, 1, 1]));
    layout.print_test(format!("{:?}", (vec![1, 2])), sum2_solution(vec![1, 2]), sum2(vec![1, 2]));
    layout.print_test(format!("{:?}", (vec![1])), sum2_solution(vec![1]), sum2(vec![1]));
    layout.print_test(format!("{:?}", (vec![0; 0])), sum2_solution(vec![0; 0]), sum2(vec![0; 0]));
    layout.print_test(format!("{:?}", (vec![4, 5, 6])), sum2_solution(vec![4, 5, 6]), sum2(vec![4, 5, 6]));
    layout.print_test(format!("{:?}", (vec![4])), sum2_solution(vec![4]), sum2(vec![4]));
    layout.print_footer();
}

pub fn middle_way_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("middle_way");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2, 3], vec![4, 5, 6])), middle_way_solution(vec![1, 2, 3], vec![4, 5, 6]).into(), middle_way(vec![1, 2, 3], vec![4, 5, 6]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![7, 7, 7], vec![3, 8, 0])), middle_way_solution(vec![7, 7, 7], vec![3, 8, 0]).into(), middle_way(vec![7, 7, 7], vec![3, 8, 0]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![5, 2, 9], vec![1, 4, 5])), middle_way_solution(vec![5, 2, 9], vec![1, 4, 5]).into(), middle_way(vec![5, 2, 9], vec![1, 4, 5]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 9, 7], vec![4, 8, 8])), middle_way_solution(vec![1, 9, 7], vec![4, 8, 8]).into(), middle_way(vec![1, 9, 7], vec![4, 8, 8]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2, 3], vec![3, 1, 4])), middle_way_solution(vec![1, 2, 3], vec![3, 1, 4]).into(), middle_way(vec![1, 2, 3], vec![3, 1, 4]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2, 3], vec![4, 1, 1])), middle_way_solution(vec![1, 2, 3], vec![4, 1, 1]).into(), middle_way(vec![1, 2, 3], vec![4, 1, 1]).into());
    layout.print_footer();
}

pub fn make_ends_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("make_ends");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2, 3])), make_ends_solution(vec![1, 2, 3]).into(), make_ends(vec![1, 2, 3]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2, 3, 4])), make_ends_solution(vec![1, 2, 3, 4]).into(), make_ends(vec![1, 2, 3, 4]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![7, 4, 6, 2])), make_ends_solution(vec![7, 4, 6, 2]).into(), make_ends(vec![7, 4, 6, 2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2, 2, 2, 2, 2, 2, 3])), make_ends_solution(vec![1, 2, 2, 2, 2, 2, 2, 3]).into(), make_ends(vec![1, 2, 2, 2, 2, 2, 2, 3]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![7, 4])), make_ends_solution(vec![7, 4]).into(), make_ends(vec![7, 4]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![7])), make_ends_solution(vec![7]).into(), make_ends(vec![7]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![5, 2, 9])), make_ends_solution(vec![5, 2, 9]).into(), make_ends(vec![5, 2, 9]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![2, 3, 4, 1])), make_ends_solution(vec![2, 3, 4, 1]).into(), make_ends(vec![2, 3, 4, 1]).into());
    layout.print_footer();
}

pub fn has23_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("has23");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![2, 5])), has23_solution(vec![2, 5]), has23(vec![2, 5]));
    layout.print_test(format!("{:?}", (vec![4, 3])), has23_solution(vec![4, 3]), has23(vec![4, 3]));
    layout.print_test(format!("{:?}", (vec![4, 5])), has23_solution(vec![4, 5]), has23(vec![4, 5]));
    layout.print_test(format!("{:?}", (vec![2, 2])), has23_solution(vec![2, 2]), has23(vec![2, 2]));
    layout.print_test(format!("{:?}", (vec![3, 2])), has23_solution(vec![3, 2]), has23(vec![3, 2]));
    layout.print_test(format!("{:?}", (vec![3, 3])), has23_solution(vec![3, 3]), has23(vec![3, 3]));
    layout.print_test(format!("{:?}", (vec![7, 7])), has23_solution(vec![7, 7]), has23(vec![7, 7]));
    layout.print_test(format!("{:?}", (vec![3, 9])), has23_solution(vec![3, 9]), has23(vec![3, 9]));
    layout.print_test(format!("{:?}", (vec![9, 5])), has23_solution(vec![9, 5]), has23(vec![9, 5]));
    layout.print_footer();
}

pub fn no23_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("no23");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![4, 5])), no23_solution(vec![4, 5]), no23(vec![4, 5]));
    layout.print_test(format!("{:?}", (vec![4, 2])), no23_solution(vec![4, 2]), no23(vec![4, 2]));
    layout.print_test(format!("{:?}", (vec![3, 5])), no23_solution(vec![3, 5]), no23(vec![3, 5]));
    layout.print_test(format!("{:?}", (vec![1, 9])), no23_solution(vec![1, 9]), no23(vec![1, 9]));
    layout.print_test(format!("{:?}", (vec![2, 9])), no23_solution(vec![2, 9]), no23(vec![2, 9]));
    layout.print_test(format!("{:?}", (vec![1, 3])), no23_solution(vec![1, 3]), no23(vec![1, 3]));
    layout.print_test(format!("{:?}", (vec![1, 1])), no23_solution(vec![1, 1]), no23(vec![1, 1]));
    layout.print_test(format!("{:?}", (vec![2, 2])), no23_solution(vec![2, 2]), no23(vec![2, 2]));
    layout.print_test(format!("{:?}", (vec![3, 3])), no23_solution(vec![3, 3]), no23(vec![3, 3]));
    layout.print_test(format!("{:?}", (vec![7, 8])), no23_solution(vec![7, 8]), no23(vec![7, 8]));
    layout.print_test(format!("{:?}", (vec![8, 7])), no23_solution(vec![8, 7]), no23(vec![8, 7]));
    layout.print_footer();
}

pub fn make_last_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("make_last");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![4, 5, 6])), make_last_solution(vec![4, 5, 6]).into(), make_last(vec![4, 5, 6]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2])), make_last_solution(vec![1, 2]).into(), make_last(vec![1, 2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![3])), make_last_solution(vec![3]).into(), make_last(vec![3]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![0])), make_last_solution(vec![0]).into(), make_last(vec![0]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![7, 7, 7])), make_last_solution(vec![7, 7, 7]).into(), make_last(vec![7, 7, 7]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![3, 1, 4])), make_last_solution(vec![3, 1, 4]).into(), make_last(vec![3, 1, 4]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2, 3, 4])), make_last_solution(vec![1, 2, 3, 4]).into(), make_last(vec![1, 2, 3, 4]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2, 3, 0])), make_last_solution(vec![1, 2, 3, 0]).into(), make_last(vec![1, 2, 3, 0]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![2, 4])), make_last_solution(vec![2, 4]).into(), make_last(vec![2, 4]).into());
    layout.print_footer();
}

pub fn double23_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("double23");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![2, 2])), double23_solution(vec![2, 2]), double23(vec![2, 2]));
    layout.print_test(format!("{:?}", (vec![3, 3])), double23_solution(vec![3, 3]), double23(vec![3, 3]));
    layout.print_test(format!("{:?}", (vec![2, 3])), double23_solution(vec![2, 3]), double23(vec![2, 3]));
    layout.print_test(format!("{:?}", (vec![3, 2])), double23_solution(vec![3, 2]), double23(vec![3, 2]));
    layout.print_test(format!("{:?}", (vec![4, 5])), double23_solution(vec![4, 5]), double23(vec![4, 5]));
    layout.print_test(format!("{:?}", (vec![2])), double23_solution(vec![2]), double23(vec![2]));
    layout.print_test(format!("{:?}", (vec![3])), double23_solution(vec![3]), double23(vec![3]));
    layout.print_test(format!("{:?}", (vec![0; 0])), double23_solution(vec![0; 0]), double23(vec![0; 0]));
    layout.print_test(format!("{:?}", (vec![3, 4])), double23_solution(vec![3, 4]), double23(vec![3, 4]));
    layout.print_footer();
}

pub fn fix23_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("fix23");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2, 3])), fix23_solution(vec![1, 2, 3]).into(), fix23(vec![1, 2, 3]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![2, 3, 5])), fix23_solution(vec![2, 3, 5]).into(), fix23(vec![2, 3, 5]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2, 1])), fix23_solution(vec![1, 2, 1]).into(), fix23(vec![1, 2, 1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![3, 2, 1])), fix23_solution(vec![3, 2, 1]).into(), fix23(vec![3, 2, 1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![2, 1, 3])), fix23_solution(vec![2, 1, 3]).into(), fix23(vec![2, 1, 3]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![2, 2, 3])), fix23_solution(vec![2, 2, 3]).into(), fix23(vec![2, 2, 3]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![2, 3, 3])), fix23_solution(vec![2, 3, 3]).into(), fix23(vec![2, 3, 3]).into());
    layout.print_footer();
}

pub fn start1_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("start1");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 2, 3], vec![1, 3])), start1_solution(vec![1, 2, 3], vec![1, 3]), start1(vec![1, 2, 3], vec![1, 3]));
    layout.print_test(format!("{:?}", (vec![7, 2, 3], vec![1])), start1_solution(vec![7, 2, 3], vec![1]), start1(vec![7, 2, 3], vec![1]));
    layout.print_test(format!("{:?}", (vec![1, 2], vec![0; 0])), start1_solution(vec![1, 2], vec![0; 0]), start1(vec![1, 2], vec![0; 0]));
    layout.print_test(format!("{:?}", (vec![0; 0], vec![1, 2])), start1_solution(vec![0; 0], vec![1, 2]), start1(vec![0; 0], vec![1, 2]));
    layout.print_test(format!("{:?}", (vec![7], vec![0; 0])), start1_solution(vec![7], vec![0; 0]), start1(vec![7], vec![0; 0]));
    layout.print_test(format!("{:?}", (vec![7], vec![1])), start1_solution(vec![7], vec![1]), start1(vec![7], vec![1]));
    layout.print_test(format!("{:?}", (vec![1], vec![1])), start1_solution(vec![1], vec![1]), start1(vec![1], vec![1]));
    layout.print_test(format!("{:?}", (vec![7], vec![8])), start1_solution(vec![7], vec![8]), start1(vec![7], vec![8]));
    layout.print_test(format!("{:?}", (vec![0; 0], vec![0; 0])), start1_solution(vec![0; 0], vec![0; 0]), start1(vec![0; 0], vec![0; 0]));
    layout.print_test(format!("{:?}", (vec![1, 3], vec![1])), start1_solution(vec![1, 3], vec![1]), start1(vec![1, 3], vec![1]));
    layout.print_footer();
}

pub fn bigger_two_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("bigger_two");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2], vec![3, 4])), bigger_two_solution(vec![1, 2], vec![3, 4]).into(), bigger_two(vec![1, 2], vec![3, 4]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![3, 4], vec![1, 2])), bigger_two_solution(vec![3, 4], vec![1, 2]).into(), bigger_two(vec![3, 4], vec![1, 2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 1], vec![1, 2])), bigger_two_solution(vec![1, 1], vec![1, 2]).into(), bigger_two(vec![1, 1], vec![1, 2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![2, 1], vec![1, 1])), bigger_two_solution(vec![2, 1], vec![1, 1]).into(), bigger_two(vec![2, 1], vec![1, 1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![2, 2], vec![1, 3])), bigger_two_solution(vec![2, 2], vec![1, 3]).into(), bigger_two(vec![2, 2], vec![1, 3]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 3], vec![2, 2])), bigger_two_solution(vec![1, 3], vec![2, 2]).into(), bigger_two(vec![1, 3], vec![2, 2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![6, 7], vec![3, 1])), bigger_two_solution(vec![6, 7], vec![3, 1]).into(), bigger_two(vec![6, 7], vec![3, 1]).into());
    layout.print_footer();
}

pub fn make_middle_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("make_middle");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2, 3, 4])), make_middle_solution(vec![1, 2, 3, 4]).into(), make_middle(vec![1, 2, 3, 4]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![7, 1, 2, 3, 4, 9])), make_middle_solution(vec![7, 1, 2, 3, 4, 9]).into(), make_middle(vec![7, 1, 2, 3, 4, 9]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2])), make_middle_solution(vec![1, 2]).into(), make_middle(vec![1, 2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![5, 2, 4, 7])), make_middle_solution(vec![5, 2, 4, 7]).into(), make_middle(vec![5, 2, 4, 7]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![9, 0, 4, 3, 9, 1])), make_middle_solution(vec![9, 0, 4, 3, 9, 1]).into(), make_middle(vec![9, 0, 4, 3, 9, 1]).into());
    layout.print_footer();
}

pub fn plus_two_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("plus_two");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2], vec![3, 4])), plus_two_solution(vec![1, 2], vec![3, 4]).into(), plus_two(vec![1, 2], vec![3, 4]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![4, 4], vec![2, 2])), plus_two_solution(vec![4, 4], vec![2, 2]).into(), plus_two(vec![4, 4], vec![2, 2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![9, 2], vec![3, 4])), plus_two_solution(vec![9, 2], vec![3, 4]).into(), plus_two(vec![9, 2], vec![3, 4]).into());
    layout.print_footer();
}

pub fn swap_ends_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("swap_ends");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2, 3, 4])), swap_ends_solution(vec![1, 2, 3, 4]).into(), swap_ends(vec![1, 2, 3, 4]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2, 3])), swap_ends_solution(vec![1, 2, 3]).into(), swap_ends(vec![1, 2, 3]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![8, 6, 7, 9, 5])), swap_ends_solution(vec![8, 6, 7, 9, 5]).into(), swap_ends(vec![8, 6, 7, 9, 5]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![3, 1, 4, 1, 5, 9])), swap_ends_solution(vec![3, 1, 4, 1, 5, 9]).into(), swap_ends(vec![3, 1, 4, 1, 5, 9]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2])), swap_ends_solution(vec![1, 2]).into(), swap_ends(vec![1, 2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1])), swap_ends_solution(vec![1]).into(), swap_ends(vec![1]).into());
    layout.print_footer();
}

pub fn mid_three_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("mid_three");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2, 3, 4, 5])), mid_three_solution(vec![1, 2, 3, 4, 5]).into(), mid_three(vec![1, 2, 3, 4, 5]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![8, 6, 7, 5, 3, 0, 9])), mid_three_solution(vec![8, 6, 7, 5, 3, 0, 9]).into(), mid_three(vec![8, 6, 7, 5, 3, 0, 9]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2, 3])), mid_three_solution(vec![1, 2, 3]).into(), mid_three(vec![1, 2, 3]).into());
    layout.print_footer();
}

pub fn max_triple_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("max_triple");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 2, 3])), max_triple_solution(vec![1, 2, 3]), max_triple(vec![1, 2, 3]));
    layout.print_test(format!("{:?}", (vec![1, 5, 3])), max_triple_solution(vec![1, 5, 3]), max_triple(vec![1, 5, 3]));
    layout.print_test(format!("{:?}", (vec![5, 2, 3])), max_triple_solution(vec![5, 2, 3]), max_triple(vec![5, 2, 3]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3, 1, 1])), max_triple_solution(vec![1, 2, 3, 1, 1]), max_triple(vec![1, 2, 3, 1, 1]));
    layout.print_test(format!("{:?}", (vec![1, 7, 3, 1, 5])), max_triple_solution(vec![1, 7, 3, 1, 5]), max_triple(vec![1, 7, 3, 1, 5]));
    layout.print_test(format!("{:?}", (vec![5, 1, 3, 7, 1])), max_triple_solution(vec![5, 1, 3, 7, 1]), max_triple(vec![5, 1, 3, 7, 1]));
    layout.print_test(format!("{:?}", (vec![5, 1, 7, 3, 7, 8, 1])), max_triple_solution(vec![5, 1, 7, 3, 7, 8, 1]), max_triple(vec![5, 1, 7, 3, 7, 8, 1]));
    layout.print_test(format!("{:?}", (vec![5, 1, 7, 9, 7, 8, 1])), max_triple_solution(vec![5, 1, 7, 9, 7, 8, 1]), max_triple(vec![5, 1, 7, 9, 7, 8, 1]));
    layout.print_test(format!("{:?}", (vec![5, 1, 7, 3, 7, 8, 9])), max_triple_solution(vec![5, 1, 7, 3, 7, 8, 9]), max_triple(vec![5, 1, 7, 3, 7, 8, 9]));
    layout.print_test(format!("{:?}", (vec![2, 2, 5, 1, 1])), max_triple_solution(vec![2, 2, 5, 1, 1]), max_triple(vec![2, 2, 5, 1, 1]));
    layout.print_footer();
}

pub fn front_piece_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("front_piece");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2, 3])), front_piece_solution(vec![1, 2, 3]).into(), front_piece(vec![1, 2, 3]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2])), front_piece_solution(vec![1, 2]).into(), front_piece(vec![1, 2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1])), front_piece_solution(vec![1]).into(), front_piece(vec![1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![0; 0])), front_piece_solution(vec![0; 0]).into(), front_piece(vec![0; 0]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![6, 5, 0])), front_piece_solution(vec![6, 5, 0]).into(), front_piece(vec![6, 5, 0]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![6, 5])), front_piece_solution(vec![6, 5]).into(), front_piece(vec![6, 5]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![3, 1, 4, 1, 5])), front_piece_solution(vec![3, 1, 4, 1, 5]).into(), front_piece(vec![3, 1, 4, 1, 5]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![6])), front_piece_solution(vec![6]).into(), front_piece(vec![6]).into());
    layout.print_footer();
}

pub fn unlucky1_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("unlucky1");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 3, 4, 5])), unlucky1_solution(vec![1, 3, 4, 5]), unlucky1(vec![1, 3, 4, 5]));
    layout.print_test(format!("{:?}", (vec![2, 1, 3, 4, 5])), unlucky1_solution(vec![2, 1, 3, 4, 5]), unlucky1(vec![2, 1, 3, 4, 5]));
    layout.print_test(format!("{:?}", (vec![1, 1, 1])), unlucky1_solution(vec![1, 1, 1]), unlucky1(vec![1, 1, 1]));
    layout.print_test(format!("{:?}", (vec![1, 3, 1])), unlucky1_solution(vec![1, 3, 1]), unlucky1(vec![1, 3, 1]));
    layout.print_test(format!("{:?}", (vec![1, 1, 3])), unlucky1_solution(vec![1, 1, 3]), unlucky1(vec![1, 1, 3]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3])), unlucky1_solution(vec![1, 2, 3]), unlucky1(vec![1, 2, 3]));
    layout.print_test(format!("{:?}", (vec![3, 3, 3])), unlucky1_solution(vec![3, 3, 3]), unlucky1(vec![3, 3, 3]));
    layout.print_test(format!("{:?}", (vec![1, 3])), unlucky1_solution(vec![1, 3]), unlucky1(vec![1, 3]));
    layout.print_test(format!("{:?}", (vec![1, 4])), unlucky1_solution(vec![1, 4]), unlucky1(vec![1, 4]));
    layout.print_test(format!("{:?}", (vec![1])), unlucky1_solution(vec![1]), unlucky1(vec![1]));
    layout.print_test(format!("{:?}", (vec![0; 0])), unlucky1_solution(vec![0; 0]), unlucky1(vec![0; 0]));
    layout.print_test(format!("{:?}", (vec![1, 1, 1, 3, 1])), unlucky1_solution(vec![1, 1, 1, 3, 1]), unlucky1(vec![1, 1, 1, 3, 1]));
    layout.print_test(format!("{:?}", (vec![1, 1, 3, 1, 1])), unlucky1_solution(vec![1, 1, 3, 1, 1]), unlucky1(vec![1, 1, 3, 1, 1]));
    layout.print_test(format!("{:?}", (vec![1, 1, 1, 1, 3])), unlucky1_solution(vec![1, 1, 1, 1, 3]), unlucky1(vec![1, 1, 1, 1, 3]));
    layout.print_test(format!("{:?}", (vec![1, 4, 1, 5])), unlucky1_solution(vec![1, 4, 1, 5]), unlucky1(vec![1, 4, 1, 5]));
    layout.print_test(format!("{:?}", (vec![1, 1, 2, 3])), unlucky1_solution(vec![1, 1, 2, 3]), unlucky1(vec![1, 1, 2, 3]));
    layout.print_test(format!("{:?}", (vec![2, 3, 2, 1])), unlucky1_solution(vec![2, 3, 2, 1]), unlucky1(vec![2, 3, 2, 1]));
    layout.print_test(format!("{:?}", (vec![2, 3, 1, 3])), unlucky1_solution(vec![2, 3, 1, 3]), unlucky1(vec![2, 3, 1, 3]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3, 4, 1, 3])), unlucky1_solution(vec![1, 2, 3, 4, 1, 3]), unlucky1(vec![1, 2, 3, 4, 1, 3]));
    layout.print_footer();
}

pub fn make2_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("make2");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![4, 5], vec![1, 2, 3])), make2_solution(vec![4, 5], vec![1, 2, 3]).into(), make2(vec![4, 5], vec![1, 2, 3]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![4], vec![1, 2, 3])), make2_solution(vec![4], vec![1, 2, 3]).into(), make2(vec![4], vec![1, 2, 3]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![0; 0], vec![1, 2])), make2_solution(vec![0; 0], vec![1, 2]).into(), make2(vec![0; 0], vec![1, 2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2], vec![0; 0])), make2_solution(vec![1, 2], vec![0; 0]).into(), make2(vec![1, 2], vec![0; 0]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![3], vec![1, 2, 3])), make2_solution(vec![3], vec![1, 2, 3]).into(), make2(vec![3], vec![1, 2, 3]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![3], vec![1])), make2_solution(vec![3], vec![1]).into(), make2(vec![3], vec![1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![3, 1, 4], vec![0; 0])), make2_solution(vec![3, 1, 4], vec![0; 0]).into(), make2(vec![3, 1, 4], vec![0; 0]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1], vec![1])), make2_solution(vec![1], vec![1]).into(), make2(vec![1], vec![1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2, 3], vec![7, 8])), make2_solution(vec![1, 2, 3], vec![7, 8]).into(), make2(vec![1, 2, 3], vec![7, 8]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![7, 8], vec![1, 2, 3])), make2_solution(vec![7, 8], vec![1, 2, 3]).into(), make2(vec![7, 8], vec![1, 2, 3]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![7], vec![1, 2, 3])), make2_solution(vec![7], vec![1, 2, 3]).into(), make2(vec![7], vec![1, 2, 3]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![5, 4], vec![2, 3, 7])), make2_solution(vec![5, 4], vec![2, 3, 7]).into(), make2(vec![5, 4], vec![2, 3, 7]).into());
    layout.print_footer();
}

pub fn front11_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("front11");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2, 3], vec![7, 9, 8])), front11_solution(vec![1, 2, 3], vec![7, 9, 8]).into(), front11(vec![1, 2, 3], vec![7, 9, 8]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1], vec![2])), front11_solution(vec![1], vec![2]).into(), front11(vec![1], vec![2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 7], vec![0; 0])), front11_solution(vec![1, 7], vec![0; 0]).into(), front11(vec![1, 7], vec![0; 0]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![0; 0], vec![2, 8])), front11_solution(vec![0; 0], vec![2, 8]).into(), front11(vec![0; 0], vec![2, 8]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![0; 0], vec![0; 0])), front11_solution(vec![0; 0], vec![0; 0]).into(), front11(vec![0; 0], vec![0; 0]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![3], vec![1, 4, 1, 9])), front11_solution(vec![3], vec![1, 4, 1, 9]).into(), front11(vec![3], vec![1, 4, 1, 9]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 4, 1, 9], vec![0; 0])), front11_solution(vec![1, 4, 1, 9], vec![0; 0]).into(), front11(vec![1, 4, 1, 9], vec![0; 0]).into());
    layout.print_footer();
}

pub fn find_lowest_index_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("find_lowest_index");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![99, 98, 97, 96, 95])), find_lowest_index_solution(vec![99, 98, 97, 96, 95]), find_lowest_index(vec![99, 98, 97, 96, 95]));
    layout.print_test(format!("{:?}", (vec![2, 2, 0])), find_lowest_index_solution(vec![2, 2, 0]), find_lowest_index(vec![2, 2, 0]));
    layout.print_test(format!("{:?}", (vec![1, 3, 5])), find_lowest_index_solution(vec![1, 3, 5]), find_lowest_index(vec![1, 3, 5]));
    layout.print_test(format!("{:?}", (vec![5])), find_lowest_index_solution(vec![5]), find_lowest_index(vec![5]));
    layout.print_test(format!("{:?}", (vec![11, 9, 0, 1])), find_lowest_index_solution(vec![11, 9, 0, 1]), find_lowest_index(vec![11, 9, 0, 1]));
    layout.print_test(format!("{:?}", (vec![2, 11, 9, 0])), find_lowest_index_solution(vec![2, 11, 9, 0]), find_lowest_index(vec![2, 11, 9, 0]));
    layout.print_test(format!("{:?}", (vec![2])), find_lowest_index_solution(vec![2]), find_lowest_index(vec![2]));
    layout.print_test(format!("{:?}", (vec![2, 5, -12])), find_lowest_index_solution(vec![2, 5, -12]), find_lowest_index(vec![2, 5, -12]));
    layout.print_footer();
}

pub fn count_evens_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("count_evens");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![2, 1, 2, 3, 4])), count_evens_solution(vec![2, 1, 2, 3, 4]), count_evens(vec![2, 1, 2, 3, 4]));
    layout.print_test(format!("{:?}", (vec![2, 2, 0])), count_evens_solution(vec![2, 2, 0]), count_evens(vec![2, 2, 0]));
    layout.print_test(format!("{:?}", (vec![1, 3, 5])), count_evens_solution(vec![1, 3, 5]), count_evens(vec![1, 3, 5]));
    layout.print_test(format!("{:?}", (vec![0; 0])), count_evens_solution(vec![0; 0]), count_evens(vec![0; 0]));
    layout.print_test(format!("{:?}", (vec![11, 9, 0, 1])), count_evens_solution(vec![11, 9, 0, 1]), count_evens(vec![11, 9, 0, 1]));
    layout.print_test(format!("{:?}", (vec![2, 11, 9, 0])), count_evens_solution(vec![2, 11, 9, 0]), count_evens(vec![2, 11, 9, 0]));
    layout.print_test(format!("{:?}", (vec![2])), count_evens_solution(vec![2]), count_evens(vec![2]));
    layout.print_test(format!("{:?}", (vec![2, 5, 12])), count_evens_solution(vec![2, 5, 12]), count_evens(vec![2, 5, 12]));
    layout.print_footer();
}

pub fn big_diff_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("big_diff");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![10, 3, 5, 6])), big_diff_solution(vec![10, 3, 5, 6]), big_diff(vec![10, 3, 5, 6]));
    layout.print_test(format!("{:?}", (vec![7, 2, 10, 9])), big_diff_solution(vec![7, 2, 10, 9]), big_diff(vec![7, 2, 10, 9]));
    layout.print_test(format!("{:?}", (vec![2, 10, 7, 2])), big_diff_solution(vec![2, 10, 7, 2]), big_diff(vec![2, 10, 7, 2]));
    layout.print_test(format!("{:?}", (vec![2, 10])), big_diff_solution(vec![2, 10]), big_diff(vec![2, 10]));
    layout.print_test(format!("{:?}", (vec![10, 2])), big_diff_solution(vec![10, 2]), big_diff(vec![10, 2]));
    layout.print_test(format!("{:?}", (vec![10, 0])), big_diff_solution(vec![10, 0]), big_diff(vec![10, 0]));
    layout.print_test(format!("{:?}", (vec![2, 3])), big_diff_solution(vec![2, 3]), big_diff(vec![2, 3]));
    layout.print_test(format!("{:?}", (vec![2, 2])), big_diff_solution(vec![2, 2]), big_diff(vec![2, 2]));
    layout.print_test(format!("{:?}", (vec![2])), big_diff_solution(vec![2]), big_diff(vec![2]));
    layout.print_test(format!("{:?}", (vec![5, 1, 6, 1, 9, 9])), big_diff_solution(vec![5, 1, 6, 1, 9, 9]), big_diff(vec![5, 1, 6, 1, 9, 9]));
    layout.print_test(format!("{:?}", (vec![7, 6, 8, 5])), big_diff_solution(vec![7, 6, 8, 5]), big_diff(vec![7, 6, 8, 5]));
    layout.print_test(format!("{:?}", (vec![7, 7, 6, 8, 5, 5, 6])), big_diff_solution(vec![7, 7, 6, 8, 5, 5, 6]), big_diff(vec![7, 7, 6, 8, 5, 5, 6]));
    layout.print_footer();
}

pub fn centered_average_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("centered_average");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 2, 3, 4, 100])), centered_average_solution(vec![1, 2, 3, 4, 100]), centered_average(vec![1, 2, 3, 4, 100]));
    layout.print_test(format!("{:?}", (vec![1, 1, 5, 5, 10, 8, 7])), centered_average_solution(vec![1, 1, 5, 5, 10, 8, 7]), centered_average(vec![1, 1, 5, 5, 10, 8, 7]));
    layout.print_test(format!("{:?}", (vec![-10, -4, -2, -4, -2, 0])), centered_average_solution(vec![-10, -4, -2, -4, -2, 0]), centered_average(vec![-10, -4, -2, -4, -2, 0]));
    layout.print_test(format!("{:?}", (vec![5, 3, 4, 6, 2])), centered_average_solution(vec![5, 3, 4, 6, 2]), centered_average(vec![5, 3, 4, 6, 2]));
    layout.print_test(format!("{:?}", (vec![5, 3, 4, 0, 100])), centered_average_solution(vec![5, 3, 4, 0, 100]), centered_average(vec![5, 3, 4, 0, 100]));
    layout.print_test(format!("{:?}", (vec![100, 0, 5, 3, 4])), centered_average_solution(vec![100, 0, 5, 3, 4]), centered_average(vec![100, 0, 5, 3, 4]));
    layout.print_test(format!("{:?}", (vec![4, 0, 100])), centered_average_solution(vec![4, 0, 100]), centered_average(vec![4, 0, 100]));
    layout.print_test(format!("{:?}", (vec![0, 2, 3, 4, 100])), centered_average_solution(vec![0, 2, 3, 4, 100]), centered_average(vec![0, 2, 3, 4, 100]));
    layout.print_test(format!("{:?}", (vec![1, 1, 100])), centered_average_solution(vec![1, 1, 100]), centered_average(vec![1, 1, 100]));
    layout.print_test(format!("{:?}", (vec![7, 7, 7])), centered_average_solution(vec![7, 7, 7]), centered_average(vec![7, 7, 7]));
    layout.print_test(format!("{:?}", (vec![1, 7, 8])), centered_average_solution(vec![1, 7, 8]), centered_average(vec![1, 7, 8]));
    layout.print_test(format!("{:?}", (vec![1, 1, 99, 99])), centered_average_solution(vec![1, 1, 99, 99]), centered_average(vec![1, 1, 99, 99]));
    layout.print_test(format!("{:?}", (vec![1000, 0, 1, 99])), centered_average_solution(vec![1000, 0, 1, 99]), centered_average(vec![1000, 0, 1, 99]));
    layout.print_test(format!("{:?}", (vec![4, 4, 4, 4, 5])), centered_average_solution(vec![4, 4, 4, 4, 5]), centered_average(vec![4, 4, 4, 4, 5]));
    layout.print_test(format!("{:?}", (vec![4, 4, 4, 1, 5])), centered_average_solution(vec![4, 4, 4, 1, 5]), centered_average(vec![4, 4, 4, 1, 5]));
    layout.print_test(format!("{:?}", (vec![6, 4, 8, 12, 3])), centered_average_solution(vec![6, 4, 8, 12, 3]), centered_average(vec![6, 4, 8, 12, 3]));
    layout.print_footer();
}

pub fn sum13_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("sum13");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 2, 2, 1])), sum13_solution(vec![1, 2, 2, 1]), sum13(vec![1, 2, 2, 1]));
    layout.print_test(format!("{:?}", (vec![1, 1])), sum13_solution(vec![1, 1]), sum13(vec![1, 1]));
    layout.print_test(format!("{:?}", (vec![1, 2, 2, 1, 13])), sum13_solution(vec![1, 2, 2, 1, 13]), sum13(vec![1, 2, 2, 1, 13]));
    layout.print_test(format!("{:?}", (vec![1, 2, 13, 2, 1, 13])), sum13_solution(vec![1, 2, 13, 2, 1, 13]), sum13(vec![1, 2, 13, 2, 1, 13]));
    layout.print_test(format!("{:?}", (vec![13, 1, 2, 13, 2, 1, 13])), sum13_solution(vec![13, 1, 2, 13, 2, 1, 13]), sum13(vec![13, 1, 2, 13, 2, 1, 13]));
    layout.print_test(format!("{:?}", (vec![0; 0])), sum13_solution(vec![0; 0]), sum13(vec![0; 0]));
    layout.print_test(format!("{:?}", (vec![13])), sum13_solution(vec![13]), sum13(vec![13]));
    layout.print_test(format!("{:?}", (vec![13, 13])), sum13_solution(vec![13, 13]), sum13(vec![13, 13]));
    layout.print_test(format!("{:?}", (vec![13, 0, 13])), sum13_solution(vec![13, 0, 13]), sum13(vec![13, 0, 13]));
    layout.print_test(format!("{:?}", (vec![13, 1, 13])), sum13_solution(vec![13, 1, 13]), sum13(vec![13, 1, 13]));
    layout.print_test(format!("{:?}", (vec![5, 7, 2])), sum13_solution(vec![5, 7, 2]), sum13(vec![5, 7, 2]));
    layout.print_test(format!("{:?}", (vec![5, 13, 2])), sum13_solution(vec![5, 13, 2]), sum13(vec![5, 13, 2]));
    layout.print_test(format!("{:?}", (vec![0])), sum13_solution(vec![0]), sum13(vec![0]));
    layout.print_test(format!("{:?}", (vec![13, 0])), sum13_solution(vec![13, 0]), sum13(vec![13, 0]));
    layout.print_footer();
}

pub fn sum67_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("sum67");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 2, 2])), sum67_solution(vec![1, 2, 2]), sum67(vec![1, 2, 2]));
    layout.print_test(format!("{:?}", (vec![1, 2, 2, 6, 99, 99, 7])), sum67_solution(vec![1, 2, 2, 6, 99, 99, 7]), sum67(vec![1, 2, 2, 6, 99, 99, 7]));
    layout.print_test(format!("{:?}", (vec![1, 1, 6, 7, 2])), sum67_solution(vec![1, 1, 6, 7, 2]), sum67(vec![1, 1, 6, 7, 2]));
    layout.print_test(format!("{:?}", (vec![1, 6, 2, 2, 7, 1, 6, 99, 99, 7])), sum67_solution(vec![1, 6, 2, 2, 7, 1, 6, 99, 99, 7]), sum67(vec![1, 6, 2, 2, 7, 1, 6, 99, 99, 7]));
    layout.print_test(format!("{:?}", (vec![1, 6, 2, 6, 2, 7, 1, 6, 99, 99, 7])), sum67_solution(vec![1, 6, 2, 6, 2, 7, 1, 6, 99, 99, 7]), sum67(vec![1, 6, 2, 6, 2, 7, 1, 6, 99, 99, 7]));
    layout.print_test(format!("{:?}", (vec![2, 7, 6, 2, 6, 7, 2, 7])), sum67_solution(vec![2, 7, 6, 2, 6, 7, 2, 7]), sum67(vec![2, 7, 6, 2, 6, 7, 2, 7]));
    layout.print_test(format!("{:?}", (vec![2, 7, 6, 2, 6, 2, 7])), sum67_solution(vec![2, 7, 6, 2, 6, 2, 7]), sum67(vec![2, 7, 6, 2, 6, 2, 7]));
    layout.print_test(format!("{:?}", (vec![1, 6, 7, 7])), sum67_solution(vec![1, 6, 7, 7]), sum67(vec![1, 6, 7, 7]));
    layout.print_test(format!("{:?}", (vec![6, 7, 1, 6, 7, 7])), sum67_solution(vec![6, 7, 1, 6, 7, 7]), sum67(vec![6, 7, 1, 6, 7, 7]));
    layout.print_test(format!("{:?}", (vec![6, 8, 1, 6, 7])), sum67_solution(vec![6, 8, 1, 6, 7]), sum67(vec![6, 8, 1, 6, 7]));
    layout.print_test(format!("{:?}", (vec![0; 0])), sum67_solution(vec![0; 0]), sum67(vec![0; 0]));
    layout.print_test(format!("{:?}", (vec![6, 7, 11])), sum67_solution(vec![6, 7, 11]), sum67(vec![6, 7, 11]));
    layout.print_test(format!("{:?}", (vec![11, 6, 7, 11])), sum67_solution(vec![11, 6, 7, 11]), sum67(vec![11, 6, 7, 11]));
    layout.print_test(format!("{:?}", (vec![2, 2, 6, 7, 7])), sum67_solution(vec![2, 2, 6, 7, 7]), sum67(vec![2, 2, 6, 7, 7]));
    layout.print_footer();
}

pub fn has22_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("has22");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 2, 2])), has22_solution(vec![1, 2, 2]), has22(vec![1, 2, 2]));
    layout.print_test(format!("{:?}", (vec![1, 2, 1, 2])), has22_solution(vec![1, 2, 1, 2]), has22(vec![1, 2, 1, 2]));
    layout.print_test(format!("{:?}", (vec![2, 1, 2])), has22_solution(vec![2, 1, 2]), has22(vec![2, 1, 2]));
    layout.print_test(format!("{:?}", (vec![2, 2, 1, 2])), has22_solution(vec![2, 2, 1, 2]), has22(vec![2, 2, 1, 2]));
    layout.print_test(format!("{:?}", (vec![1, 3, 2])), has22_solution(vec![1, 3, 2]), has22(vec![1, 3, 2]));
    layout.print_test(format!("{:?}", (vec![1, 3, 2, 2])), has22_solution(vec![1, 3, 2, 2]), has22(vec![1, 3, 2, 2]));
    layout.print_test(format!("{:?}", (vec![2, 3, 2, 2])), has22_solution(vec![2, 3, 2, 2]), has22(vec![2, 3, 2, 2]));
    layout.print_test(format!("{:?}", (vec![4, 2, 4, 2, 2, 5])), has22_solution(vec![4, 2, 4, 2, 2, 5]), has22(vec![4, 2, 4, 2, 2, 5]));
    layout.print_test(format!("{:?}", (vec![1, 2])), has22_solution(vec![1, 2]), has22(vec![1, 2]));
    layout.print_test(format!("{:?}", (vec![2, 2])), has22_solution(vec![2, 2]), has22(vec![2, 2]));
    layout.print_test(format!("{:?}", (vec![2])), has22_solution(vec![2]), has22(vec![2]));
    layout.print_test(format!("{:?}", (vec![0; 0])), has22_solution(vec![0; 0]), has22(vec![0; 0]));
    layout.print_test(format!("{:?}", (vec![3, 3, 2, 2])), has22_solution(vec![3, 3, 2, 2]), has22(vec![3, 3, 2, 2]));
    layout.print_test(format!("{:?}", (vec![5, 2, 5, 2])), has22_solution(vec![5, 2, 5, 2]), has22(vec![5, 2, 5, 2]));
    layout.print_footer();
}

pub fn lucky13_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("lucky13");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![0, 2, 4])), lucky13_solution(vec![0, 2, 4]), lucky13(vec![0, 2, 4]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3])), lucky13_solution(vec![1, 2, 3]), lucky13(vec![1, 2, 3]));
    layout.print_test(format!("{:?}", (vec![1, 2, 4])), lucky13_solution(vec![1, 2, 4]), lucky13(vec![1, 2, 4]));
    layout.print_test(format!("{:?}", (vec![2, 7, 2, 8])), lucky13_solution(vec![2, 7, 2, 8]), lucky13(vec![2, 7, 2, 8]));
    layout.print_test(format!("{:?}", (vec![2, 7, 1, 8])), lucky13_solution(vec![2, 7, 1, 8]), lucky13(vec![2, 7, 1, 8]));
    layout.print_test(format!("{:?}", (vec![3, 7, 2, 8])), lucky13_solution(vec![3, 7, 2, 8]), lucky13(vec![3, 7, 2, 8]));
    layout.print_test(format!("{:?}", (vec![2, 7, 2, 1])), lucky13_solution(vec![2, 7, 2, 1]), lucky13(vec![2, 7, 2, 1]));
    layout.print_test(format!("{:?}", (vec![1, 2])), lucky13_solution(vec![1, 2]), lucky13(vec![1, 2]));
    layout.print_test(format!("{:?}", (vec![2, 2])), lucky13_solution(vec![2, 2]), lucky13(vec![2, 2]));
    layout.print_test(format!("{:?}", (vec![2])), lucky13_solution(vec![2]), lucky13(vec![2]));
    layout.print_test(format!("{:?}", (vec![3])), lucky13_solution(vec![3]), lucky13(vec![3]));
    layout.print_test(format!("{:?}", (vec![0; 0])), lucky13_solution(vec![0; 0]), lucky13(vec![0; 0]));
    layout.print_footer();
}

pub fn sum28_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("sum28");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![2, 3, 2, 2, 4, 2])), sum28_solution(vec![2, 3, 2, 2, 4, 2]), sum28(vec![2, 3, 2, 2, 4, 2]));
    layout.print_test(format!("{:?}", (vec![2, 3, 2, 2, 4, 2, 2])), sum28_solution(vec![2, 3, 2, 2, 4, 2, 2]), sum28(vec![2, 3, 2, 2, 4, 2, 2]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3, 4])), sum28_solution(vec![1, 2, 3, 4]), sum28(vec![1, 2, 3, 4]));
    layout.print_test(format!("{:?}", (vec![2, 2, 2, 2])), sum28_solution(vec![2, 2, 2, 2]), sum28(vec![2, 2, 2, 2]));
    layout.print_test(format!("{:?}", (vec![1, 2, 2, 2, 2, 4])), sum28_solution(vec![1, 2, 2, 2, 2, 4]), sum28(vec![1, 2, 2, 2, 2, 4]));
    layout.print_test(format!("{:?}", (vec![0; 0])), sum28_solution(vec![0; 0]), sum28(vec![0; 0]));
    layout.print_test(format!("{:?}", (vec![2])), sum28_solution(vec![2]), sum28(vec![2]));
    layout.print_test(format!("{:?}", (vec![8])), sum28_solution(vec![8]), sum28(vec![8]));
    layout.print_test(format!("{:?}", (vec![2, 2, 2])), sum28_solution(vec![2, 2, 2]), sum28(vec![2, 2, 2]));
    layout.print_test(format!("{:?}", (vec![2, 2, 2, 2, 2])), sum28_solution(vec![2, 2, 2, 2, 2]), sum28(vec![2, 2, 2, 2, 2]));
    layout.print_test(format!("{:?}", (vec![1, 2, 2, 1, 2, 2])), sum28_solution(vec![1, 2, 2, 1, 2, 2]), sum28(vec![1, 2, 2, 1, 2, 2]));
    layout.print_test(format!("{:?}", (vec![5, 2, 2, 2, 4, 2])), sum28_solution(vec![5, 2, 2, 2, 4, 2]), sum28(vec![5, 2, 2, 2, 4, 2]));
    layout.print_footer();
}

pub fn more14_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("more14");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 4, 1])), more14_solution(vec![1, 4, 1]), more14(vec![1, 4, 1]));
    layout.print_test(format!("{:?}", (vec![1, 4, 1, 4])), more14_solution(vec![1, 4, 1, 4]), more14(vec![1, 4, 1, 4]));
    layout.print_test(format!("{:?}", (vec![1, 1])), more14_solution(vec![1, 1]), more14(vec![1, 1]));
    layout.print_test(format!("{:?}", (vec![1, 6, 6])), more14_solution(vec![1, 6, 6]), more14(vec![1, 6, 6]));
    layout.print_test(format!("{:?}", (vec![1])), more14_solution(vec![1]), more14(vec![1]));
    layout.print_test(format!("{:?}", (vec![1, 4])), more14_solution(vec![1, 4]), more14(vec![1, 4]));
    layout.print_test(format!("{:?}", (vec![6, 1, 1])), more14_solution(vec![6, 1, 1]), more14(vec![6, 1, 1]));
    layout.print_test(format!("{:?}", (vec![1, 6, 4])), more14_solution(vec![1, 6, 4]), more14(vec![1, 6, 4]));
    layout.print_test(format!("{:?}", (vec![1, 1, 4, 4, 1])), more14_solution(vec![1, 1, 4, 4, 1]), more14(vec![1, 1, 4, 4, 1]));
    layout.print_test(format!("{:?}", (vec![1, 1, 6, 4, 4, 1])), more14_solution(vec![1, 1, 6, 4, 4, 1]), more14(vec![1, 1, 6, 4, 4, 1]));
    layout.print_test(format!("{:?}", (vec![0; 0])), more14_solution(vec![0; 0]), more14(vec![0; 0]));
    layout.print_test(format!("{:?}", (vec![4, 1, 4, 6])), more14_solution(vec![4, 1, 4, 6]), more14(vec![4, 1, 4, 6]));
    layout.print_test(format!("{:?}", (vec![4, 1, 4, 6, 1])), more14_solution(vec![4, 1, 4, 6, 1]), more14(vec![4, 1, 4, 6, 1]));
    layout.print_test(format!("{:?}", (vec![1, 4, 1, 4, 1, 6])), more14_solution(vec![1, 4, 1, 4, 1, 6]), more14(vec![1, 4, 1, 4, 1, 6]));
    layout.print_footer();
}

pub fn prepend_sum_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("prepend_sum");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2, 4, 4])), prepend_sum_solution(vec![1, 2, 4, 4]).into(), prepend_sum(vec![1, 2, 4, 4]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![3, 3, 0])), prepend_sum_solution(vec![3, 3, 0]).into(), prepend_sum(vec![3, 3, 0]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 1, 1, 1, 1])), prepend_sum_solution(vec![1, 1, 1, 1, 1]).into(), prepend_sum(vec![1, 1, 1, 1, 1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![5, 7])), prepend_sum_solution(vec![5, 7]).into(), prepend_sum(vec![5, 7]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![0, 0, 0, 0])), prepend_sum_solution(vec![0, 0, 0, 0]).into(), prepend_sum(vec![0, 0, 0, 0]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![12, 13, 19, 20])), prepend_sum_solution(vec![12, 13, 19, 20]).into(), prepend_sum(vec![12, 13, 19, 20]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![-2, 2, -2, 2])), prepend_sum_solution(vec![-2, 2, -2, 2]).into(), prepend_sum(vec![-2, 2, -2, 2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![5, 4, 3, 2, 1, 0])), prepend_sum_solution(vec![5, 4, 3, 2, 1, 0]).into(), prepend_sum(vec![5, 4, 3, 2, 1, 0]).into());
    layout.print_footer();
}

pub fn fizz_array_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("fizz_array");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(format!("{:?}", (4)), fizz_array_solution(4).into(), fizz_array(4).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (1)), fizz_array_solution(1).into(), fizz_array(1).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (10)), fizz_array_solution(10).into(), fizz_array(10).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (0)), fizz_array_solution(0).into(), fizz_array(0).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (2)), fizz_array_solution(2).into(), fizz_array(2).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (7)), fizz_array_solution(7).into(), fizz_array(7).into());
    layout.print_footer();
}

pub fn only14_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("only14");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 4, 1, 4])), only14_solution(vec![1, 4, 1, 4]), only14(vec![1, 4, 1, 4]));
    layout.print_test(format!("{:?}", (vec![1, 4, 2, 4])), only14_solution(vec![1, 4, 2, 4]), only14(vec![1, 4, 2, 4]));
    layout.print_test(format!("{:?}", (vec![1, 1])), only14_solution(vec![1, 1]), only14(vec![1, 1]));
    layout.print_test(format!("{:?}", (vec![4, 1])), only14_solution(vec![4, 1]), only14(vec![4, 1]));
    layout.print_test(format!("{:?}", (vec![2])), only14_solution(vec![2]), only14(vec![2]));
    layout.print_test(format!("{:?}", (vec![0; 0])), only14_solution(vec![0; 0]), only14(vec![0; 0]));
    layout.print_test(format!("{:?}", (vec![1, 4, 1, 3])), only14_solution(vec![1, 4, 1, 3]), only14(vec![1, 4, 1, 3]));
    layout.print_test(format!("{:?}", (vec![3, 1, 3])), only14_solution(vec![3, 1, 3]), only14(vec![3, 1, 3]));
    layout.print_test(format!("{:?}", (vec![1])), only14_solution(vec![1]), only14(vec![1]));
    layout.print_test(format!("{:?}", (vec![4])), only14_solution(vec![4]), only14(vec![4]));
    layout.print_test(format!("{:?}", (vec![3, 4])), only14_solution(vec![3, 4]), only14(vec![3, 4]));
    layout.print_test(format!("{:?}", (vec![1, 3, 4])), only14_solution(vec![1, 3, 4]), only14(vec![1, 3, 4]));
    layout.print_test(format!("{:?}", (vec![1, 1, 1])), only14_solution(vec![1, 1, 1]), only14(vec![1, 1, 1]));
    layout.print_test(format!("{:?}", (vec![1, 1, 1, 5])), only14_solution(vec![1, 1, 1, 5]), only14(vec![1, 1, 1, 5]));
    layout.print_test(format!("{:?}", (vec![4, 1, 4, 1])), only14_solution(vec![4, 1, 4, 1]), only14(vec![4, 1, 4, 1]));
    layout.print_footer();
}

pub fn fizz_array2_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("fizz_array2");
    layout.print_header();
    layout.print_test::<BatVec<String>>(format!("{:?}", (4)), fizz_array2_solution(4).into(), fizz_array2(4).into());
    layout.print_test::<BatVec<String>>(format!("{:?}", (10)), fizz_array2_solution(10).into(), fizz_array2(10).into());
    layout.print_test::<BatVec<String>>(format!("{:?}", (2)), fizz_array2_solution(2).into(), fizz_array2(2).into());
    layout.print_test::<BatVec<String>>(format!("{:?}", (1)), fizz_array2_solution(1).into(), fizz_array2(1).into());
    layout.print_test::<BatVec<String>>(format!("{:?}", (0)), fizz_array2_solution(0).into(), fizz_array2(0).into());
    layout.print_test::<BatVec<String>>(format!("{:?}", (7)), fizz_array2_solution(7).into(), fizz_array2(7).into());
    layout.print_test::<BatVec<String>>(format!("{:?}", (9)), fizz_array2_solution(9).into(), fizz_array2(9).into());
    layout.print_test::<BatVec<String>>(format!("{:?}", (11)), fizz_array2_solution(11).into(), fizz_array2(11).into());
    layout.print_footer();
}

pub fn no14_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("no14");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 2, 3])), no14_solution(vec![1, 2, 3]), no14(vec![1, 2, 3]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3, 4])), no14_solution(vec![1, 2, 3, 4]), no14(vec![1, 2, 3, 4]));
    layout.print_test(format!("{:?}", (vec![2, 3, 4])), no14_solution(vec![2, 3, 4]), no14(vec![2, 3, 4]));
    layout.print_test(format!("{:?}", (vec![1, 1, 4, 4])), no14_solution(vec![1, 1, 4, 4]), no14(vec![1, 1, 4, 4]));
    layout.print_test(format!("{:?}", (vec![2, 2, 4, 4])), no14_solution(vec![2, 2, 4, 4]), no14(vec![2, 2, 4, 4]));
    layout.print_test(format!("{:?}", (vec![2, 3, 4, 1])), no14_solution(vec![2, 3, 4, 1]), no14(vec![2, 3, 4, 1]));
    layout.print_test(format!("{:?}", (vec![2, 1, 1])), no14_solution(vec![2, 1, 1]), no14(vec![2, 1, 1]));
    layout.print_test(format!("{:?}", (vec![1, 4])), no14_solution(vec![1, 4]), no14(vec![1, 4]));
    layout.print_test(format!("{:?}", (vec![2])), no14_solution(vec![2]), no14(vec![2]));
    layout.print_test(format!("{:?}", (vec![2, 1])), no14_solution(vec![2, 1]), no14(vec![2, 1]));
    layout.print_test(format!("{:?}", (vec![1])), no14_solution(vec![1]), no14(vec![1]));
    layout.print_test(format!("{:?}", (vec![4])), no14_solution(vec![4]), no14(vec![4]));
    layout.print_test(format!("{:?}", (vec![0; 0])), no14_solution(vec![0; 0]), no14(vec![0; 0]));
    layout.print_test(format!("{:?}", (vec![1, 1, 1, 1])), no14_solution(vec![1, 1, 1, 1]), no14(vec![1, 1, 1, 1]));
    layout.print_test(format!("{:?}", (vec![9, 4, 4, 1])), no14_solution(vec![9, 4, 4, 1]), no14(vec![9, 4, 4, 1]));
    layout.print_test(format!("{:?}", (vec![4, 2, 3, 1])), no14_solution(vec![4, 2, 3, 1]), no14(vec![4, 2, 3, 1]));
    layout.print_test(format!("{:?}", (vec![4, 2, 3, 5])), no14_solution(vec![4, 2, 3, 5]), no14(vec![4, 2, 3, 5]));
    layout.print_test(format!("{:?}", (vec![4, 4, 2])), no14_solution(vec![4, 4, 2]), no14(vec![4, 4, 2]));
    layout.print_test(format!("{:?}", (vec![1, 4, 4])), no14_solution(vec![1, 4, 4]), no14(vec![1, 4, 4]));
    layout.print_footer();
}

pub fn is_everywhere_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("is_everywhere");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 2, 1, 3], 1)), is_everywhere_solution(vec![1, 2, 1, 3], 1), is_everywhere(vec![1, 2, 1, 3], 1));
    layout.print_test(format!("{:?}", (vec![1, 2, 1, 3], 2)), is_everywhere_solution(vec![1, 2, 1, 3], 2), is_everywhere(vec![1, 2, 1, 3], 2));
    layout.print_test(format!("{:?}", (vec![1, 2, 1, 3, 4], 1)), is_everywhere_solution(vec![1, 2, 1, 3, 4], 1), is_everywhere(vec![1, 2, 1, 3, 4], 1));
    layout.print_test(format!("{:?}", (vec![2, 1, 2, 1], 1)), is_everywhere_solution(vec![2, 1, 2, 1], 1), is_everywhere(vec![2, 1, 2, 1], 1));
    layout.print_test(format!("{:?}", (vec![2, 1, 2, 1], 2)), is_everywhere_solution(vec![2, 1, 2, 1], 2), is_everywhere(vec![2, 1, 2, 1], 2));
    layout.print_test(format!("{:?}", (vec![2, 1, 2, 3, 1], 2)), is_everywhere_solution(vec![2, 1, 2, 3, 1], 2), is_everywhere(vec![2, 1, 2, 3, 1], 2));
    layout.print_test(format!("{:?}", (vec![3, 1], 3)), is_everywhere_solution(vec![3, 1], 3), is_everywhere(vec![3, 1], 3));
    layout.print_test(format!("{:?}", (vec![3, 1], 2)), is_everywhere_solution(vec![3, 1], 2), is_everywhere(vec![3, 1], 2));
    layout.print_test(format!("{:?}", (vec![3], 1)), is_everywhere_solution(vec![3], 1), is_everywhere(vec![3], 1));
    layout.print_test(format!("{:?}", (vec![0; 0], 1)), is_everywhere_solution(vec![0; 0], 1), is_everywhere(vec![0; 0], 1));
    layout.print_test(format!("{:?}", (vec![1, 2, 1, 2, 3, 2, 5], 2)), is_everywhere_solution(vec![1, 2, 1, 2, 3, 2, 5], 2), is_everywhere(vec![1, 2, 1, 2, 3, 2, 5], 2));
    layout.print_test(format!("{:?}", (vec![1, 2, 1, 1, 1, 2], 2)), is_everywhere_solution(vec![1, 2, 1, 1, 1, 2], 2), is_everywhere(vec![1, 2, 1, 1, 1, 2], 2));
    layout.print_test(format!("{:?}", (vec![2, 1, 2, 1, 1, 2], 2)), is_everywhere_solution(vec![2, 1, 2, 1, 1, 2], 2), is_everywhere(vec![2, 1, 2, 1, 1, 2], 2));
    layout.print_test(format!("{:?}", (vec![2, 1, 2, 2, 2, 1, 1, 2], 2)), is_everywhere_solution(vec![2, 1, 2, 2, 2, 1, 1, 2], 2), is_everywhere(vec![2, 1, 2, 2, 2, 1, 1, 2], 2));
    layout.print_test(format!("{:?}", (vec![2, 1, 2, 2, 2, 1, 2, 1], 2)), is_everywhere_solution(vec![2, 1, 2, 2, 2, 1, 2, 1], 2), is_everywhere(vec![2, 1, 2, 2, 2, 1, 2, 1], 2));
    layout.print_test(format!("{:?}", (vec![2, 1, 2, 1, 2], 2)), is_everywhere_solution(vec![2, 1, 2, 1, 2], 2), is_everywhere(vec![2, 1, 2, 1, 2], 2));
    layout.print_footer();
}

pub fn either24_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("either24");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 2, 2])), either24_solution(vec![1, 2, 2]), either24(vec![1, 2, 2]));
    layout.print_test(format!("{:?}", (vec![4, 4, 1])), either24_solution(vec![4, 4, 1]), either24(vec![4, 4, 1]));
    layout.print_test(format!("{:?}", (vec![4, 4, 1, 2, 2])), either24_solution(vec![4, 4, 1, 2, 2]), either24(vec![4, 4, 1, 2, 2]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3, 4])), either24_solution(vec![1, 2, 3, 4]), either24(vec![1, 2, 3, 4]));
    layout.print_test(format!("{:?}", (vec![3, 5, 9])), either24_solution(vec![3, 5, 9]), either24(vec![3, 5, 9]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3, 4, 4])), either24_solution(vec![1, 2, 3, 4, 4]), either24(vec![1, 2, 3, 4, 4]));
    layout.print_test(format!("{:?}", (vec![2, 2, 3, 4])), either24_solution(vec![2, 2, 3, 4]), either24(vec![2, 2, 3, 4]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3, 2, 2, 4])), either24_solution(vec![1, 2, 3, 2, 2, 4]), either24(vec![1, 2, 3, 2, 2, 4]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3, 2, 2, 4, 4])), either24_solution(vec![1, 2, 3, 2, 2, 4, 4]), either24(vec![1, 2, 3, 2, 2, 4, 4]));
    layout.print_test(format!("{:?}", (vec![1, 2])), either24_solution(vec![1, 2]), either24(vec![1, 2]));
    layout.print_test(format!("{:?}", (vec![2, 2])), either24_solution(vec![2, 2]), either24(vec![2, 2]));
    layout.print_test(format!("{:?}", (vec![4, 4])), either24_solution(vec![4, 4]), either24(vec![4, 4]));
    layout.print_test(format!("{:?}", (vec![2])), either24_solution(vec![2]), either24(vec![2]));
    layout.print_test(format!("{:?}", (vec![0; 0])), either24_solution(vec![0; 0]), either24(vec![0; 0]));
    layout.print_footer();
}

pub fn match_up_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("match_up");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 2, 3], vec![2, 3, 10])), match_up_solution(vec![1, 2, 3], vec![2, 3, 10]), match_up(vec![1, 2, 3], vec![2, 3, 10]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3], vec![2, 3, 5])), match_up_solution(vec![1, 2, 3], vec![2, 3, 5]), match_up(vec![1, 2, 3], vec![2, 3, 5]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3], vec![2, 3, 3])), match_up_solution(vec![1, 2, 3], vec![2, 3, 3]), match_up(vec![1, 2, 3], vec![2, 3, 3]));
    layout.print_test(format!("{:?}", (vec![5, 3], vec![5, 5])), match_up_solution(vec![5, 3], vec![5, 5]), match_up(vec![5, 3], vec![5, 5]));
    layout.print_test(format!("{:?}", (vec![5, 3], vec![4, 4])), match_up_solution(vec![5, 3], vec![4, 4]), match_up(vec![5, 3], vec![4, 4]));
    layout.print_test(format!("{:?}", (vec![5, 3], vec![3, 3])), match_up_solution(vec![5, 3], vec![3, 3]), match_up(vec![5, 3], vec![3, 3]));
    layout.print_test(format!("{:?}", (vec![5, 3], vec![2, 2])), match_up_solution(vec![5, 3], vec![2, 2]), match_up(vec![5, 3], vec![2, 2]));
    layout.print_test(format!("{:?}", (vec![5, 3], vec![1, 1])), match_up_solution(vec![5, 3], vec![1, 1]), match_up(vec![5, 3], vec![1, 1]));
    layout.print_test(format!("{:?}", (vec![5, 3], vec![0, 0])), match_up_solution(vec![5, 3], vec![0, 0]), match_up(vec![5, 3], vec![0, 0]));
    layout.print_test(format!("{:?}", (vec![4], vec![4])), match_up_solution(vec![4], vec![4]), match_up(vec![4], vec![4]));
    layout.print_test(format!("{:?}", (vec![4], vec![5])), match_up_solution(vec![4], vec![5]), match_up(vec![4], vec![5]));
    layout.print_footer();
}

pub fn has77_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("has77");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 7, 7])), has77_solution(vec![1, 7, 7]), has77(vec![1, 7, 7]));
    layout.print_test(format!("{:?}", (vec![1, 7, 1, 7])), has77_solution(vec![1, 7, 1, 7]), has77(vec![1, 7, 1, 7]));
    layout.print_test(format!("{:?}", (vec![1, 7, 1, 1, 7])), has77_solution(vec![1, 7, 1, 1, 7]), has77(vec![1, 7, 1, 1, 7]));
    layout.print_test(format!("{:?}", (vec![7, 7, 1, 1, 7])), has77_solution(vec![7, 7, 1, 1, 7]), has77(vec![7, 7, 1, 1, 7]));
    layout.print_test(format!("{:?}", (vec![2, 7, 2, 2, 7, 2])), has77_solution(vec![2, 7, 2, 2, 7, 2]), has77(vec![2, 7, 2, 2, 7, 2]));
    layout.print_test(format!("{:?}", (vec![2, 7, 2, 2, 7, 7])), has77_solution(vec![2, 7, 2, 2, 7, 7]), has77(vec![2, 7, 2, 2, 7, 7]));
    layout.print_test(format!("{:?}", (vec![7, 2, 7, 2, 2, 7])), has77_solution(vec![7, 2, 7, 2, 2, 7]), has77(vec![7, 2, 7, 2, 2, 7]));
    layout.print_test(format!("{:?}", (vec![7, 2, 6, 2, 2, 7])), has77_solution(vec![7, 2, 6, 2, 2, 7]), has77(vec![7, 2, 6, 2, 2, 7]));
    layout.print_test(format!("{:?}", (vec![7, 7, 7])), has77_solution(vec![7, 7, 7]), has77(vec![7, 7, 7]));
    layout.print_test(format!("{:?}", (vec![7, 1, 7])), has77_solution(vec![7, 1, 7]), has77(vec![7, 1, 7]));
    layout.print_test(format!("{:?}", (vec![7, 1, 1])), has77_solution(vec![7, 1, 1]), has77(vec![7, 1, 1]));
    layout.print_test(format!("{:?}", (vec![1, 2])), has77_solution(vec![1, 2]), has77(vec![1, 2]));
    layout.print_test(format!("{:?}", (vec![1, 7])), has77_solution(vec![1, 7]), has77(vec![1, 7]));
    layout.print_test(format!("{:?}", (vec![7])), has77_solution(vec![7]), has77(vec![7]));
    layout.print_footer();
}

pub fn has12_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("has12");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 3, 2])), has12_solution(vec![1, 3, 2]), has12(vec![1, 3, 2]));
    layout.print_test(format!("{:?}", (vec![3, 1, 2])), has12_solution(vec![3, 1, 2]), has12(vec![3, 1, 2]));
    layout.print_test(format!("{:?}", (vec![3, 1, 4, 5, 2])), has12_solution(vec![3, 1, 4, 5, 2]), has12(vec![3, 1, 4, 5, 2]));
    layout.print_test(format!("{:?}", (vec![3, 1, 4, 5, 6])), has12_solution(vec![3, 1, 4, 5, 6]), has12(vec![3, 1, 4, 5, 6]));
    layout.print_test(format!("{:?}", (vec![3, 1, 4, 1, 6, 2])), has12_solution(vec![3, 1, 4, 1, 6, 2]), has12(vec![3, 1, 4, 1, 6, 2]));
    layout.print_test(format!("{:?}", (vec![2, 1, 4, 1, 6, 2])), has12_solution(vec![2, 1, 4, 1, 6, 2]), has12(vec![2, 1, 4, 1, 6, 2]));
    layout.print_test(format!("{:?}", (vec![2, 1, 4, 1, 6])), has12_solution(vec![2, 1, 4, 1, 6]), has12(vec![2, 1, 4, 1, 6]));
    layout.print_test(format!("{:?}", (vec![1])), has12_solution(vec![1]), has12(vec![1]));
    layout.print_test(format!("{:?}", (vec![2, 1, 3])), has12_solution(vec![2, 1, 3]), has12(vec![2, 1, 3]));
    layout.print_test(format!("{:?}", (vec![2, 1, 3, 2])), has12_solution(vec![2, 1, 3, 2]), has12(vec![2, 1, 3, 2]));
    layout.print_test(format!("{:?}", (vec![2])), has12_solution(vec![2]), has12(vec![2]));
    layout.print_test(format!("{:?}", (vec![3, 2])), has12_solution(vec![3, 2]), has12(vec![3, 2]));
    layout.print_test(format!("{:?}", (vec![3, 1, 3, 2])), has12_solution(vec![3, 1, 3, 2]), has12(vec![3, 1, 3, 2]));
    layout.print_test(format!("{:?}", (vec![3, 5, 9])), has12_solution(vec![3, 5, 9]), has12(vec![3, 5, 9]));
    layout.print_test(format!("{:?}", (vec![3, 5, 1])), has12_solution(vec![3, 5, 1]), has12(vec![3, 5, 1]));
    layout.print_test(format!("{:?}", (vec![3, 2, 1])), has12_solution(vec![3, 2, 1]), has12(vec![3, 2, 1]));
    layout.print_test(format!("{:?}", (vec![1, 2])), has12_solution(vec![1, 2]), has12(vec![1, 2]));
    layout.print_footer();
}

pub fn mod_three_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("mod_three");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![2, 1, 3, 5])), mod_three_solution(vec![2, 1, 3, 5]), mod_three(vec![2, 1, 3, 5]));
    layout.print_test(format!("{:?}", (vec![2, 1, 2, 5])), mod_three_solution(vec![2, 1, 2, 5]), mod_three(vec![2, 1, 2, 5]));
    layout.print_test(format!("{:?}", (vec![2, 4, 2, 5])), mod_three_solution(vec![2, 4, 2, 5]), mod_three(vec![2, 4, 2, 5]));
    layout.print_test(format!("{:?}", (vec![1, 2, 1, 2, 1])), mod_three_solution(vec![1, 2, 1, 2, 1]), mod_three(vec![1, 2, 1, 2, 1]));
    layout.print_test(format!("{:?}", (vec![9, 9, 9])), mod_three_solution(vec![9, 9, 9]), mod_three(vec![9, 9, 9]));
    layout.print_test(format!("{:?}", (vec![1, 2, 1])), mod_three_solution(vec![1, 2, 1]), mod_three(vec![1, 2, 1]));
    layout.print_test(format!("{:?}", (vec![1, 2])), mod_three_solution(vec![1, 2]), mod_three(vec![1, 2]));
    layout.print_test(format!("{:?}", (vec![1])), mod_three_solution(vec![1]), mod_three(vec![1]));
    layout.print_test(format!("{:?}", (vec![0; 0])), mod_three_solution(vec![0; 0]), mod_three(vec![0; 0]));
    layout.print_test(format!("{:?}", (vec![9, 7, 2, 9])), mod_three_solution(vec![9, 7, 2, 9]), mod_three(vec![9, 7, 2, 9]));
    layout.print_test(format!("{:?}", (vec![9, 7, 2, 9, 2, 2])), mod_three_solution(vec![9, 7, 2, 9, 2, 2]), mod_three(vec![9, 7, 2, 9, 2, 2]));
    layout.print_test(format!("{:?}", (vec![9, 7, 2, 9, 2, 2, 6])), mod_three_solution(vec![9, 7, 2, 9, 2, 2, 6]), mod_three(vec![9, 7, 2, 9, 2, 2, 6]));
    layout.print_footer();
}

pub fn find_the_median_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("find_the_median");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![4, 9, 9, 2, 1, 5])), find_the_median_solution(vec![4, 9, 9, 2, 1, 5]), find_the_median(vec![4, 9, 9, 2, 1, 5]));
    layout.print_test(format!("{:?}", (vec![1, 5, 3, 1, 5])), find_the_median_solution(vec![1, 5, 3, 1, 5]), find_the_median(vec![1, 5, 3, 1, 5]));
    layout.print_test(format!("{:?}", (vec![10, 12, 15])), find_the_median_solution(vec![10, 12, 15]), find_the_median(vec![10, 12, 15]));
    layout.print_test(format!("{:?}", (vec![5])), find_the_median_solution(vec![5]), find_the_median(vec![5]));
    layout.print_test(format!("{:?}", (vec![11, 9, 0, 1])), find_the_median_solution(vec![11, 9, 0, 1]), find_the_median(vec![11, 9, 0, 1]));
    layout.print_test(format!("{:?}", (vec![-1, 11, -2, 10, -3, 15])), find_the_median_solution(vec![-1, 11, -2, 10, -3, 15]), find_the_median(vec![-1, 11, -2, 10, -3, 15]));
    layout.print_test(format!("{:?}", (vec![2, 10, 15, 13])), find_the_median_solution(vec![2, 10, 15, 13]), find_the_median(vec![2, 10, 15, 13]));
    layout.print_test(format!("{:?}", (vec![2, 5, -12])), find_the_median_solution(vec![2, 5, -12]), find_the_median(vec![2, 5, -12]));
    layout.print_footer();
}

pub fn have_three_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("have_three");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![3, 1, 3, 1, 3])), have_three_solution(vec![3, 1, 3, 1, 3]), have_three(vec![3, 1, 3, 1, 3]));
    layout.print_test(format!("{:?}", (vec![3, 1, 3, 3])), have_three_solution(vec![3, 1, 3, 3]), have_three(vec![3, 1, 3, 3]));
    layout.print_test(format!("{:?}", (vec![3, 4, 3, 3, 4])), have_three_solution(vec![3, 4, 3, 3, 4]), have_three(vec![3, 4, 3, 3, 4]));
    layout.print_test(format!("{:?}", (vec![1, 3, 1, 3, 1, 2])), have_three_solution(vec![1, 3, 1, 3, 1, 2]), have_three(vec![1, 3, 1, 3, 1, 2]));
    layout.print_test(format!("{:?}", (vec![1, 3, 1, 3, 1, 3])), have_three_solution(vec![1, 3, 1, 3, 1, 3]), have_three(vec![1, 3, 1, 3, 1, 3]));
    layout.print_test(format!("{:?}", (vec![1, 3, 3, 1, 3])), have_three_solution(vec![1, 3, 3, 1, 3]), have_three(vec![1, 3, 3, 1, 3]));
    layout.print_test(format!("{:?}", (vec![1, 3, 1, 3, 1, 3, 4, 3])), have_three_solution(vec![1, 3, 1, 3, 1, 3, 4, 3]), have_three(vec![1, 3, 1, 3, 1, 3, 4, 3]));
    layout.print_test(format!("{:?}", (vec![3, 4, 3, 4, 3, 4, 4])), have_three_solution(vec![3, 4, 3, 4, 3, 4, 4]), have_three(vec![3, 4, 3, 4, 3, 4, 4]));
    layout.print_test(format!("{:?}", (vec![3, 3, 3])), have_three_solution(vec![3, 3, 3]), have_three(vec![3, 3, 3]));
    layout.print_test(format!("{:?}", (vec![1, 3])), have_three_solution(vec![1, 3]), have_three(vec![1, 3]));
    layout.print_test(format!("{:?}", (vec![3])), have_three_solution(vec![3]), have_three(vec![3]));
    layout.print_test(format!("{:?}", (vec![1])), have_three_solution(vec![1]), have_three(vec![1]));
    layout.print_footer();
}

pub fn two_two_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("two_two");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![4, 2, 2, 3])), two_two_solution(vec![4, 2, 2, 3]), two_two(vec![4, 2, 2, 3]));
    layout.print_test(format!("{:?}", (vec![2, 2, 4])), two_two_solution(vec![2, 2, 4]), two_two(vec![2, 2, 4]));
    layout.print_test(format!("{:?}", (vec![2, 2, 4, 2])), two_two_solution(vec![2, 2, 4, 2]), two_two(vec![2, 2, 4, 2]));
    layout.print_test(format!("{:?}", (vec![1, 3, 4])), two_two_solution(vec![1, 3, 4]), two_two(vec![1, 3, 4]));
    layout.print_test(format!("{:?}", (vec![1, 2, 2, 3, 4])), two_two_solution(vec![1, 2, 2, 3, 4]), two_two(vec![1, 2, 2, 3, 4]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3, 4])), two_two_solution(vec![1, 2, 3, 4]), two_two(vec![1, 2, 3, 4]));
    layout.print_test(format!("{:?}", (vec![2, 2])), two_two_solution(vec![2, 2]), two_two(vec![2, 2]));
    layout.print_test(format!("{:?}", (vec![2, 2, 7])), two_two_solution(vec![2, 2, 7]), two_two(vec![2, 2, 7]));
    layout.print_test(format!("{:?}", (vec![2, 2, 7, 2, 1])), two_two_solution(vec![2, 2, 7, 2, 1]), two_two(vec![2, 2, 7, 2, 1]));
    layout.print_test(format!("{:?}", (vec![4, 2, 2, 2])), two_two_solution(vec![4, 2, 2, 2]), two_two(vec![4, 2, 2, 2]));
    layout.print_test(format!("{:?}", (vec![2, 2, 2])), two_two_solution(vec![2, 2, 2]), two_two(vec![2, 2, 2]));
    layout.print_test(format!("{:?}", (vec![1, 2])), two_two_solution(vec![1, 2]), two_two(vec![1, 2]));
    layout.print_test(format!("{:?}", (vec![2])), two_two_solution(vec![2]), two_two(vec![2]));
    layout.print_test(format!("{:?}", (vec![1])), two_two_solution(vec![1]), two_two(vec![1]));
    layout.print_test(format!("{:?}", (vec![0; 0])), two_two_solution(vec![0; 0]), two_two(vec![0; 0]));
    layout.print_test(format!("{:?}", (vec![5, 2, 2, 3])), two_two_solution(vec![5, 2, 2, 3]), two_two(vec![5, 2, 2, 3]));
    layout.print_test(format!("{:?}", (vec![2, 2, 5, 2])), two_two_solution(vec![2, 2, 5, 2]), two_two(vec![2, 2, 5, 2]));
    layout.print_footer();
}

pub fn same_ends_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("same_ends");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![5, 6, 45, 99, 13, 5, 6], 1)), same_ends_solution(vec![5, 6, 45, 99, 13, 5, 6], 1), same_ends(vec![5, 6, 45, 99, 13, 5, 6], 1));
    layout.print_test(format!("{:?}", (vec![5, 6, 45, 99, 13, 5, 6], 2)), same_ends_solution(vec![5, 6, 45, 99, 13, 5, 6], 2), same_ends(vec![5, 6, 45, 99, 13, 5, 6], 2));
    layout.print_test(format!("{:?}", (vec![5, 6, 45, 99, 13, 5, 6], 3)), same_ends_solution(vec![5, 6, 45, 99, 13, 5, 6], 3), same_ends(vec![5, 6, 45, 99, 13, 5, 6], 3));
    layout.print_test(format!("{:?}", (vec![1, 2, 5, 2, 1], 1)), same_ends_solution(vec![1, 2, 5, 2, 1], 1), same_ends(vec![1, 2, 5, 2, 1], 1));
    layout.print_test(format!("{:?}", (vec![1, 2, 5, 2, 1], 2)), same_ends_solution(vec![1, 2, 5, 2, 1], 2), same_ends(vec![1, 2, 5, 2, 1], 2));
    layout.print_test(format!("{:?}", (vec![1, 2, 5, 2, 1], 0)), same_ends_solution(vec![1, 2, 5, 2, 1], 0), same_ends(vec![1, 2, 5, 2, 1], 0));
    layout.print_test(format!("{:?}", (vec![1, 2, 5, 2, 1], 5)), same_ends_solution(vec![1, 2, 5, 2, 1], 5), same_ends(vec![1, 2, 5, 2, 1], 5));
    layout.print_test(format!("{:?}", (vec![1, 1, 1], 0)), same_ends_solution(vec![1, 1, 1], 0), same_ends(vec![1, 1, 1], 0));
    layout.print_test(format!("{:?}", (vec![1, 1, 1], 1)), same_ends_solution(vec![1, 1, 1], 1), same_ends(vec![1, 1, 1], 1));
    layout.print_test(format!("{:?}", (vec![1, 1, 1], 2)), same_ends_solution(vec![1, 1, 1], 2), same_ends(vec![1, 1, 1], 2));
    layout.print_test(format!("{:?}", (vec![1, 1, 1], 3)), same_ends_solution(vec![1, 1, 1], 3), same_ends(vec![1, 1, 1], 3));
    layout.print_test(format!("{:?}", (vec![1], 1)), same_ends_solution(vec![1], 1), same_ends(vec![1], 1));
    layout.print_test(format!("{:?}", (vec![0; 0], 0)), same_ends_solution(vec![0; 0], 0), same_ends(vec![0; 0], 0));
    layout.print_test(format!("{:?}", (vec![4, 2, 4, 5], 1)), same_ends_solution(vec![4, 2, 4, 5], 1), same_ends(vec![4, 2, 4, 5], 1));
    layout.print_footer();
}

pub fn triple_up_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("triple_up");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 4, 5, 6, 2])), triple_up_solution(vec![1, 4, 5, 6, 2]), triple_up(vec![1, 4, 5, 6, 2]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3])), triple_up_solution(vec![1, 2, 3]), triple_up(vec![1, 2, 3]));
    layout.print_test(format!("{:?}", (vec![1, 2, 4])), triple_up_solution(vec![1, 2, 4]), triple_up(vec![1, 2, 4]));
    layout.print_test(format!("{:?}", (vec![1, 2, 4, 5, 7, 6, 5, 6, 7, 6])), triple_up_solution(vec![1, 2, 4, 5, 7, 6, 5, 6, 7, 6]), triple_up(vec![1, 2, 4, 5, 7, 6, 5, 6, 7, 6]));
    layout.print_test(format!("{:?}", (vec![1, 2, 4, 5, 7, 6, 5, 7, 7, 6])), triple_up_solution(vec![1, 2, 4, 5, 7, 6, 5, 7, 7, 6]), triple_up(vec![1, 2, 4, 5, 7, 6, 5, 7, 7, 6]));
    layout.print_test(format!("{:?}", (vec![1, 2])), triple_up_solution(vec![1, 2]), triple_up(vec![1, 2]));
    layout.print_test(format!("{:?}", (vec![1])), triple_up_solution(vec![1]), triple_up(vec![1]));
    layout.print_test(format!("{:?}", (vec![0; 0])), triple_up_solution(vec![0; 0]), triple_up(vec![0; 0]));
    layout.print_test(format!("{:?}", (vec![10, 9, 8, -100, -99, -98, 100])), triple_up_solution(vec![10, 9, 8, -100, -99, -98, 100]), triple_up(vec![10, 9, 8, -100, -99, -98, 100]));
    layout.print_test(format!("{:?}", (vec![10, 9, 8, -100, -99, 99, 100])), triple_up_solution(vec![10, 9, 8, -100, -99, 99, 100]), triple_up(vec![10, 9, 8, -100, -99, 99, 100]));
    layout.print_test(format!("{:?}", (vec![-100, -99, -99, 100, 101, 102])), triple_up_solution(vec![-100, -99, -99, 100, 101, 102]), triple_up(vec![-100, -99, -99, 100, 101, 102]));
    layout.print_test(format!("{:?}", (vec![2, 3, 5, 6, 8, 9, 2, 3])), triple_up_solution(vec![2, 3, 5, 6, 8, 9, 2, 3]), triple_up(vec![2, 3, 5, 6, 8, 9, 2, 3]));
    layout.print_footer();
}

pub fn fizz_array3_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("fizz_array3");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(format!("{:?}", (5, 10)), fizz_array3_solution(5, 10).into(), fizz_array3(5, 10).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (11, 18)), fizz_array3_solution(11, 18).into(), fizz_array3(11, 18).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (1, 3)), fizz_array3_solution(1, 3).into(), fizz_array3(1, 3).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (1, 2)), fizz_array3_solution(1, 2).into(), fizz_array3(1, 2).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (1, 1)), fizz_array3_solution(1, 1).into(), fizz_array3(1, 1).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (1000, 1005)), fizz_array3_solution(1000, 1005).into(), fizz_array3(1000, 1005).into());
    layout.print_footer();
}

pub fn shift_left_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("shift_left");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![6, 2, 5, 3])), shift_left_solution(vec![6, 2, 5, 3]).into(), shift_left(vec![6, 2, 5, 3]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2])), shift_left_solution(vec![1, 2]).into(), shift_left(vec![1, 2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1])), shift_left_solution(vec![1]).into(), shift_left(vec![1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![0; 0])), shift_left_solution(vec![0; 0]).into(), shift_left(vec![0; 0]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 1, 2, 2, 4])), shift_left_solution(vec![1, 1, 2, 2, 4]).into(), shift_left(vec![1, 1, 2, 2, 4]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 1, 1])), shift_left_solution(vec![1, 1, 1]).into(), shift_left(vec![1, 1, 1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2, 3])), shift_left_solution(vec![1, 2, 3]).into(), shift_left(vec![1, 2, 3]).into());
    layout.print_footer();
}

pub fn ten_run_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("ten_run");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![2, 10, 3, 4, 20, 5])), ten_run_solution(vec![2, 10, 3, 4, 20, 5]).into(), ten_run(vec![2, 10, 3, 4, 20, 5]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![10, 1, 20, 2])), ten_run_solution(vec![10, 1, 20, 2]).into(), ten_run(vec![10, 1, 20, 2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![10, 1, 9, 20])), ten_run_solution(vec![10, 1, 9, 20]).into(), ten_run(vec![10, 1, 9, 20]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2, 50, 1])), ten_run_solution(vec![1, 2, 50, 1]).into(), ten_run(vec![1, 2, 50, 1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 20, 50, 1])), ten_run_solution(vec![1, 20, 50, 1]).into(), ten_run(vec![1, 20, 50, 1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![10, 10])), ten_run_solution(vec![10, 10]).into(), ten_run(vec![10, 10]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![10, 2])), ten_run_solution(vec![10, 2]).into(), ten_run(vec![10, 2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![0, 2])), ten_run_solution(vec![0, 2]).into(), ten_run(vec![0, 2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2])), ten_run_solution(vec![1, 2]).into(), ten_run(vec![1, 2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1])), ten_run_solution(vec![1]).into(), ten_run(vec![1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![0; 0])), ten_run_solution(vec![0; 0]).into(), ten_run(vec![0; 0]).into());
    layout.print_footer();
}

pub fn pre4_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("pre4");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2, 4, 1])), pre4_solution(vec![1, 2, 4, 1]).into(), pre4(vec![1, 2, 4, 1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![3, 1, 4])), pre4_solution(vec![3, 1, 4]).into(), pre4(vec![3, 1, 4]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 4, 4])), pre4_solution(vec![1, 4, 4]).into(), pre4(vec![1, 4, 4]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 4, 4, 2])), pre4_solution(vec![1, 4, 4, 2]).into(), pre4(vec![1, 4, 4, 2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 3, 4, 2, 4])), pre4_solution(vec![1, 3, 4, 2, 4]).into(), pre4(vec![1, 3, 4, 2, 4]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![4, 4])), pre4_solution(vec![4, 4]).into(), pre4(vec![4, 4]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![3, 3, 4])), pre4_solution(vec![3, 3, 4]).into(), pre4(vec![3, 3, 4]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2, 1, 4])), pre4_solution(vec![1, 2, 1, 4]).into(), pre4(vec![1, 2, 1, 4]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![2, 1, 4, 2])), pre4_solution(vec![2, 1, 4, 2]).into(), pre4(vec![2, 1, 4, 2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![2, 1, 2, 1, 4, 2])), pre4_solution(vec![2, 1, 2, 1, 4, 2]).into(), pre4(vec![2, 1, 2, 1, 4, 2]).into());
    layout.print_footer();
}

pub fn post4_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("post4");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![2, 4, 1, 2])), post4_solution(vec![2, 4, 1, 2]).into(), post4(vec![2, 4, 1, 2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![4, 1, 4, 2])), post4_solution(vec![4, 1, 4, 2]).into(), post4(vec![4, 1, 4, 2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![4, 4, 1, 2, 3])), post4_solution(vec![4, 4, 1, 2, 3]).into(), post4(vec![4, 4, 1, 2, 3]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![4, 2])), post4_solution(vec![4, 2]).into(), post4(vec![4, 2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![4, 4, 3])), post4_solution(vec![4, 4, 3]).into(), post4(vec![4, 4, 3]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![4, 4])), post4_solution(vec![4, 4]).into(), post4(vec![4, 4]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![4])), post4_solution(vec![4]).into(), post4(vec![4]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![2, 4, 1, 4, 3, 2])), post4_solution(vec![2, 4, 1, 4, 3, 2]).into(), post4(vec![2, 4, 1, 4, 3, 2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![4, 1, 4, 2, 2, 2])), post4_solution(vec![4, 1, 4, 2, 2, 2]).into(), post4(vec![4, 1, 4, 2, 2, 2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![3, 4, 3, 2])), post4_solution(vec![3, 4, 3, 2]).into(), post4(vec![3, 4, 3, 2]).into());
    layout.print_footer();
}

pub fn not_alone_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("not_alone");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2, 3], 2)), not_alone_solution(vec![1, 2, 3], 2).into(), not_alone(vec![1, 2, 3], 2).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2, 3, 2, 5, 2], 2)), not_alone_solution(vec![1, 2, 3, 2, 5, 2], 2).into(), not_alone(vec![1, 2, 3, 2, 5, 2], 2).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![3, 4], 3)), not_alone_solution(vec![3, 4], 3).into(), not_alone(vec![3, 4], 3).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![3, 3], 3)), not_alone_solution(vec![3, 3], 3).into(), not_alone(vec![3, 3], 3).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 3, 1, 2], 1)), not_alone_solution(vec![1, 3, 1, 2], 1).into(), not_alone(vec![1, 3, 1, 2], 1).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![3], 3)), not_alone_solution(vec![3], 3).into(), not_alone(vec![3], 3).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![0; 0], 3)), not_alone_solution(vec![0; 0], 3).into(), not_alone(vec![0; 0], 3).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![7, 1, 6], 1)), not_alone_solution(vec![7, 1, 6], 1).into(), not_alone(vec![7, 1, 6], 1).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 1, 1], 1)), not_alone_solution(vec![1, 1, 1], 1).into(), not_alone(vec![1, 1, 1], 1).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 1, 1, 2], 1)), not_alone_solution(vec![1, 1, 1, 2], 1).into(), not_alone(vec![1, 1, 1, 2], 1).into());
    layout.print_footer();
}

pub fn zero_front_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("zero_front");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 0, 0, 1])), zero_front_solution(vec![1, 0, 0, 1]).into(), zero_front(vec![1, 0, 0, 1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![0, 1, 1, 0, 1])), zero_front_solution(vec![0, 1, 1, 0, 1]).into(), zero_front(vec![0, 1, 1, 0, 1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 0])), zero_front_solution(vec![1, 0]).into(), zero_front(vec![1, 0]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![0, 1])), zero_front_solution(vec![0, 1]).into(), zero_front(vec![0, 1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 1, 1, 0])), zero_front_solution(vec![1, 1, 1, 0]).into(), zero_front(vec![1, 1, 1, 0]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![2, 2, 2, 2])), zero_front_solution(vec![2, 2, 2, 2]).into(), zero_front(vec![2, 2, 2, 2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![0, 0, 1, 0])), zero_front_solution(vec![0, 0, 1, 0]).into(), zero_front(vec![0, 0, 1, 0]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![-1, 0, 0, -1, 0])), zero_front_solution(vec![-1, 0, 0, -1, 0]).into(), zero_front(vec![-1, 0, 0, -1, 0]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![0, -3, 0, -3])), zero_front_solution(vec![0, -3, 0, -3]).into(), zero_front(vec![0, -3, 0, -3]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![0; 0])), zero_front_solution(vec![0; 0]).into(), zero_front(vec![0; 0]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![9, 9, 0, 9, 0, 9])), zero_front_solution(vec![9, 9, 0, 9, 0, 9]).into(), zero_front(vec![9, 9, 0, 9, 0, 9]).into());
    layout.print_footer();
}

pub fn without_ten_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("without_ten");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 10, 10, 2])), without_ten_solution(vec![1, 10, 10, 2]).into(), without_ten(vec![1, 10, 10, 2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![10, 2, 10])), without_ten_solution(vec![10, 2, 10]).into(), without_ten(vec![10, 2, 10]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 99, 10])), without_ten_solution(vec![1, 99, 10]).into(), without_ten(vec![1, 99, 10]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![10, 13, 10, 14])), without_ten_solution(vec![10, 13, 10, 14]).into(), without_ten(vec![10, 13, 10, 14]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![10, 13, 10, 14, 10])), without_ten_solution(vec![10, 13, 10, 14, 10]).into(), without_ten(vec![10, 13, 10, 14, 10]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![10, 10, 3])), without_ten_solution(vec![10, 10, 3]).into(), without_ten(vec![10, 10, 3]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1])), without_ten_solution(vec![1]).into(), without_ten(vec![1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![13, 1])), without_ten_solution(vec![13, 1]).into(), without_ten(vec![13, 1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![10])), without_ten_solution(vec![10]).into(), without_ten(vec![10]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![0; 0])), without_ten_solution(vec![0; 0]).into(), without_ten(vec![0; 0]).into());
    layout.print_footer();
}

pub fn zero_max_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("zero_max");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![0, 5, 0, 3])), zero_max_solution(vec![0, 5, 0, 3]).into(), zero_max(vec![0, 5, 0, 3]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![0, 4, 0, 3])), zero_max_solution(vec![0, 4, 0, 3]).into(), zero_max(vec![0, 4, 0, 3]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![0, 1, 0])), zero_max_solution(vec![0, 1, 0]).into(), zero_max(vec![0, 1, 0]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![0, 1, 5])), zero_max_solution(vec![0, 1, 5]).into(), zero_max(vec![0, 1, 5]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![0, 2, 0])), zero_max_solution(vec![0, 2, 0]).into(), zero_max(vec![0, 2, 0]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1])), zero_max_solution(vec![1]).into(), zero_max(vec![1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![0])), zero_max_solution(vec![0]).into(), zero_max(vec![0]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![0; 0])), zero_max_solution(vec![0; 0]).into(), zero_max(vec![0; 0]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![7, 0, 4, 3, 0, 2])), zero_max_solution(vec![7, 0, 4, 3, 0, 2]).into(), zero_max(vec![7, 0, 4, 3, 0, 2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![7, 0, 4, 3, 0, 1])), zero_max_solution(vec![7, 0, 4, 3, 0, 1]).into(), zero_max(vec![7, 0, 4, 3, 0, 1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![7, 0, 4, 3, 0, 0])), zero_max_solution(vec![7, 0, 4, 3, 0, 0]).into(), zero_max(vec![7, 0, 4, 3, 0, 0]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![7, 0, 1, 0, 0, 7])), zero_max_solution(vec![7, 0, 1, 0, 0, 7]).into(), zero_max(vec![7, 0, 1, 0, 0, 7]).into());
    layout.print_footer();
}

pub fn even_odd_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("even_odd");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 0, 1, 0, 0, 1, 1])), even_odd_solution(vec![1, 0, 1, 0, 0, 1, 1]).into(), even_odd(vec![1, 0, 1, 0, 0, 1, 1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![3, 3, 2])), even_odd_solution(vec![3, 3, 2]).into(), even_odd(vec![3, 3, 2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![2, 2, 2])), even_odd_solution(vec![2, 2, 2]).into(), even_odd(vec![2, 2, 2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![3, 2, 2])), even_odd_solution(vec![3, 2, 2]).into(), even_odd(vec![3, 2, 2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 1, 0, 1, 0])), even_odd_solution(vec![1, 1, 0, 1, 0]).into(), even_odd(vec![1, 1, 0, 1, 0]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1])), even_odd_solution(vec![1]).into(), even_odd(vec![1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 2])), even_odd_solution(vec![1, 2]).into(), even_odd(vec![1, 2]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![2, 1])), even_odd_solution(vec![2, 1]).into(), even_odd(vec![2, 1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![0; 0])), even_odd_solution(vec![0; 0]).into(), even_odd(vec![0; 0]).into());
    layout.print_footer();
}

pub fn fizz_buzz_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("fizz_buzz");
    layout.print_header();
    layout.print_test::<BatVec<String>>(format!("{:?}", (1, 6)), fizz_buzz_solution(1, 6).into(), fizz_buzz(1, 6).into());
    layout.print_test::<BatVec<String>>(format!("{:?}", (1, 8)), fizz_buzz_solution(1, 8).into(), fizz_buzz(1, 8).into());
    layout.print_test::<BatVec<String>>(format!("{:?}", (1, 11)), fizz_buzz_solution(1, 11).into(), fizz_buzz(1, 11).into());
    layout.print_test::<BatVec<String>>(format!("{:?}", (1, 16)), fizz_buzz_solution(1, 16).into(), fizz_buzz(1, 16).into());
    layout.print_test::<BatVec<String>>(format!("{:?}", (1, 4)), fizz_buzz_solution(1, 4).into(), fizz_buzz(1, 4).into());
    layout.print_test::<BatVec<String>>(format!("{:?}", (1, 2)), fizz_buzz_solution(1, 2).into(), fizz_buzz(1, 2).into());
    layout.print_test::<BatVec<String>>(format!("{:?}", (50, 56)), fizz_buzz_solution(50, 56).into(), fizz_buzz(50, 56).into());
    layout.print_test::<BatVec<String>>(format!("{:?}", (15, 17)), fizz_buzz_solution(15, 17).into(), fizz_buzz(15, 17).into());
    layout.print_test::<BatVec<String>>(format!("{:?}", (30, 36)), fizz_buzz_solution(30, 36).into(), fizz_buzz(30, 36).into());
    layout.print_test::<BatVec<String>>(format!("{:?}", (1000, 1006)), fizz_buzz_solution(1000, 1006).into(), fizz_buzz(1000, 1006).into());
    layout.print_test::<BatVec<String>>(format!("{:?}", (99, 102)), fizz_buzz_solution(99, 102).into(), fizz_buzz(99, 102).into());
    layout.print_test::<BatVec<String>>(format!("{:?}", (14, 20)), fizz_buzz_solution(14, 20).into(), fizz_buzz(14, 20).into());
    layout.print_footer();
}

pub fn max_span_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("max_span");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 2, 1, 1, 3])), max_span_solution(vec![1, 2, 1, 1, 3]), max_span(vec![1, 2, 1, 1, 3]));
    layout.print_test(format!("{:?}", (vec![1, 4, 2, 1, 4, 1, 4])), max_span_solution(vec![1, 4, 2, 1, 4, 1, 4]), max_span(vec![1, 4, 2, 1, 4, 1, 4]));
    layout.print_test(format!("{:?}", (vec![1, 4, 2, 1, 4, 4, 4])), max_span_solution(vec![1, 4, 2, 1, 4, 4, 4]), max_span(vec![1, 4, 2, 1, 4, 4, 4]));
    layout.print_test(format!("{:?}", (vec![3, 3, 3])), max_span_solution(vec![3, 3, 3]), max_span(vec![3, 3, 3]));
    layout.print_test(format!("{:?}", (vec![3, 9, 3])), max_span_solution(vec![3, 9, 3]), max_span(vec![3, 9, 3]));
    layout.print_test(format!("{:?}", (vec![3, 9, 9])), max_span_solution(vec![3, 9, 9]), max_span(vec![3, 9, 9]));
    layout.print_test(format!("{:?}", (vec![3, 9])), max_span_solution(vec![3, 9]), max_span(vec![3, 9]));
    layout.print_test(format!("{:?}", (vec![3, 3])), max_span_solution(vec![3, 3]), max_span(vec![3, 3]));
    layout.print_test(format!("{:?}", (vec![0; 0])), max_span_solution(vec![0; 0]), max_span(vec![0; 0]));
    layout.print_test(format!("{:?}", (vec![1])), max_span_solution(vec![1]), max_span(vec![1]));
    layout.print_footer();
}

pub fn fix34_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("fix34");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 3, 1, 4])), fix34_solution(vec![1, 3, 1, 4]).into(), fix34(vec![1, 3, 1, 4]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 3, 1, 4, 4, 3, 1])), fix34_solution(vec![1, 3, 1, 4, 4, 3, 1]).into(), fix34(vec![1, 3, 1, 4, 4, 3, 1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![3, 2, 2, 4])), fix34_solution(vec![3, 2, 2, 4]).into(), fix34(vec![3, 2, 2, 4]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![3, 2, 3, 2, 4, 4])), fix34_solution(vec![3, 2, 3, 2, 4, 4]).into(), fix34(vec![3, 2, 3, 2, 4, 4]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![2, 3, 2, 3, 2, 4, 4])), fix34_solution(vec![2, 3, 2, 3, 2, 4, 4]).into(), fix34(vec![2, 3, 2, 3, 2, 4, 4]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![5, 3, 5, 4, 5, 4, 5, 4, 3, 5, 3, 5])), fix34_solution(vec![5, 3, 5, 4, 5, 4, 5, 4, 3, 5, 3, 5]).into(), fix34(vec![5, 3, 5, 4, 5, 4, 5, 4, 3, 5, 3, 5]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![3, 1, 4])), fix34_solution(vec![3, 1, 4]).into(), fix34(vec![3, 1, 4]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![3, 4, 1])), fix34_solution(vec![3, 4, 1]).into(), fix34(vec![3, 4, 1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 1, 1])), fix34_solution(vec![1, 1, 1]).into(), fix34(vec![1, 1, 1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1])), fix34_solution(vec![1]).into(), fix34(vec![1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![0; 0])), fix34_solution(vec![0; 0]).into(), fix34(vec![0; 0]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![7, 3, 7, 7, 4])), fix34_solution(vec![7, 3, 7, 7, 4]).into(), fix34(vec![7, 3, 7, 7, 4]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![3, 1, 4, 3, 1, 4])), fix34_solution(vec![3, 1, 4, 3, 1, 4]).into(), fix34(vec![3, 1, 4, 3, 1, 4]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![3, 1, 1, 3, 4, 4])), fix34_solution(vec![3, 1, 1, 3, 4, 4]).into(), fix34(vec![3, 1, 1, 3, 4, 4]).into());
    layout.print_footer();
}

pub fn fix45_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("fix45");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![5, 4, 9, 4, 9, 5])), fix45_solution(vec![5, 4, 9, 4, 9, 5]).into(), fix45(vec![5, 4, 9, 4, 9, 5]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 4, 1, 5])), fix45_solution(vec![1, 4, 1, 5]).into(), fix45(vec![1, 4, 1, 5]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 4, 1, 5, 5, 4, 1])), fix45_solution(vec![1, 4, 1, 5, 5, 4, 1]).into(), fix45(vec![1, 4, 1, 5, 5, 4, 1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![4, 9, 4, 9, 5, 5, 4, 9, 5])), fix45_solution(vec![4, 9, 4, 9, 5, 5, 4, 9, 5]).into(), fix45(vec![4, 9, 4, 9, 5, 5, 4, 9, 5]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![5, 5, 4, 1, 4, 1])), fix45_solution(vec![5, 5, 4, 1, 4, 1]).into(), fix45(vec![5, 5, 4, 1, 4, 1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![4, 2, 2, 5])), fix45_solution(vec![4, 2, 2, 5]).into(), fix45(vec![4, 2, 2, 5]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![4, 2, 4, 2, 5, 5])), fix45_solution(vec![4, 2, 4, 2, 5, 5]).into(), fix45(vec![4, 2, 4, 2, 5, 5]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![4, 2, 4, 5, 5])), fix45_solution(vec![4, 2, 4, 5, 5]).into(), fix45(vec![4, 2, 4, 5, 5]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![1, 1, 1])), fix45_solution(vec![1, 1, 1]).into(), fix45(vec![1, 1, 1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![4, 5])), fix45_solution(vec![4, 5]).into(), fix45(vec![4, 5]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![5, 4, 1])), fix45_solution(vec![5, 4, 1]).into(), fix45(vec![5, 4, 1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![0; 0])), fix45_solution(vec![0; 0]).into(), fix45(vec![0; 0]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![5, 4, 5, 4, 1])), fix45_solution(vec![5, 4, 5, 4, 1]).into(), fix45(vec![5, 4, 5, 4, 1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![4, 5, 4, 1, 5])), fix45_solution(vec![4, 5, 4, 1, 5]).into(), fix45(vec![4, 5, 4, 1, 5]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![3, 4, 5])), fix45_solution(vec![3, 4, 5]).into(), fix45(vec![3, 4, 5]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![4, 1, 5])), fix45_solution(vec![4, 1, 5]).into(), fix45(vec![4, 1, 5]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![5, 4, 1])), fix45_solution(vec![5, 4, 1]).into(), fix45(vec![5, 4, 1]).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (vec![2, 4, 2, 5])), fix45_solution(vec![2, 4, 2, 5]).into(), fix45(vec![2, 4, 2, 5]).into());
    layout.print_footer();
}

pub fn can_balance_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("can_balance");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 1, 1, 2, 1])), can_balance_solution(vec![1, 1, 1, 2, 1]), can_balance(vec![1, 1, 1, 2, 1]));
    layout.print_test(format!("{:?}", (vec![2, 1, 1, 2, 1])), can_balance_solution(vec![2, 1, 1, 2, 1]), can_balance(vec![2, 1, 1, 2, 1]));
    layout.print_test(format!("{:?}", (vec![10, 10])), can_balance_solution(vec![10, 10]), can_balance(vec![10, 10]));
    layout.print_test(format!("{:?}", (vec![10, 0, 1, -1, 10])), can_balance_solution(vec![10, 0, 1, -1, 10]), can_balance(vec![10, 0, 1, -1, 10]));
    layout.print_test(format!("{:?}", (vec![1, 1, 1, 1, 4])), can_balance_solution(vec![1, 1, 1, 1, 4]), can_balance(vec![1, 1, 1, 1, 4]));
    layout.print_test(format!("{:?}", (vec![2, 1, 1, 1, 4])), can_balance_solution(vec![2, 1, 1, 1, 4]), can_balance(vec![2, 1, 1, 1, 4]));
    layout.print_test(format!("{:?}", (vec![2, 3, 4, 1, 2])), can_balance_solution(vec![2, 3, 4, 1, 2]), can_balance(vec![2, 3, 4, 1, 2]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3, 1, 0, 2, 3])), can_balance_solution(vec![1, 2, 3, 1, 0, 2, 3]), can_balance(vec![1, 2, 3, 1, 0, 2, 3]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3, 1, 0, 1, 3])), can_balance_solution(vec![1, 2, 3, 1, 0, 1, 3]), can_balance(vec![1, 2, 3, 1, 0, 1, 3]));
    layout.print_test(format!("{:?}", (vec![1])), can_balance_solution(vec![1]), can_balance(vec![1]));
    layout.print_test(format!("{:?}", (vec![1, 1, 1, 2, 1])), can_balance_solution(vec![1, 1, 1, 2, 1]), can_balance(vec![1, 1, 1, 2, 1]));
    layout.print_footer();
}

pub fn linear_in_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("linear_in");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 2, 4, 6], vec![2, 4])), linear_in_solution(vec![1, 2, 4, 6], vec![2, 4]), linear_in(vec![1, 2, 4, 6], vec![2, 4]));
    layout.print_test(format!("{:?}", (vec![1, 2, 4, 6], vec![2, 3, 4])), linear_in_solution(vec![1, 2, 4, 6], vec![2, 3, 4]), linear_in(vec![1, 2, 4, 6], vec![2, 3, 4]));
    layout.print_test(format!("{:?}", (vec![1, 2, 4, 4, 6], vec![2, 4])), linear_in_solution(vec![1, 2, 4, 4, 6], vec![2, 4]), linear_in(vec![1, 2, 4, 4, 6], vec![2, 4]));
    layout.print_test(format!("{:?}", (vec![2, 2, 4, 4, 6, 6], vec![2, 4])), linear_in_solution(vec![2, 2, 4, 4, 6, 6], vec![2, 4]), linear_in(vec![2, 2, 4, 4, 6, 6], vec![2, 4]));
    layout.print_test(format!("{:?}", (vec![2, 2, 2, 2, 2], vec![2, 2])), linear_in_solution(vec![2, 2, 2, 2, 2], vec![2, 2]), linear_in(vec![2, 2, 2, 2, 2], vec![2, 2]));
    layout.print_test(format!("{:?}", (vec![2, 2, 2, 2, 2], vec![2, 4])), linear_in_solution(vec![2, 2, 2, 2, 2], vec![2, 4]), linear_in(vec![2, 2, 2, 2, 2], vec![2, 4]));
    layout.print_test(format!("{:?}", (vec![2, 2, 2, 2, 4], vec![2, 4])), linear_in_solution(vec![2, 2, 2, 2, 4], vec![2, 4]), linear_in(vec![2, 2, 2, 2, 4], vec![2, 4]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3], vec![2])), linear_in_solution(vec![1, 2, 3], vec![2]), linear_in(vec![1, 2, 3], vec![2]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3], vec![-1])), linear_in_solution(vec![1, 2, 3], vec![-1]), linear_in(vec![1, 2, 3], vec![-1]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3], vec![0; 0])), linear_in_solution(vec![1, 2, 3], vec![0; 0]), linear_in(vec![1, 2, 3], vec![0; 0]));
    layout.print_test(format!("{:?}", (vec![-1, 0, 3, 3, 3, 10, 12], vec![-1, 0, 3, 12])), linear_in_solution(vec![-1, 0, 3, 3, 3, 10, 12], vec![-1, 0, 3, 12]), linear_in(vec![-1, 0, 3, 3, 3, 10, 12], vec![-1, 0, 3, 12]));
    layout.print_test(format!("{:?}", (vec![-1, 0, 3, 3, 3, 10, 12], vec![0, 3, 12, 14])), linear_in_solution(vec![-1, 0, 3, 3, 3, 10, 12], vec![0, 3, 12, 14]), linear_in(vec![-1, 0, 3, 3, 3, 10, 12], vec![0, 3, 12, 14]));
    layout.print_test(format!("{:?}", (vec![-1, 0, 3, 3, 3, 10, 12], vec![-1, 10, 11])), linear_in_solution(vec![-1, 0, 3, 3, 3, 10, 12], vec![-1, 10, 11]), linear_in(vec![-1, 0, 3, 3, 3, 10, 12], vec![-1, 10, 11]));
    layout.print_footer();
}

pub fn square_up_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("square_up");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(format!("{:?}", (3)), square_up_solution(3).into(), square_up(3).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (2)), square_up_solution(2).into(), square_up(2).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (4)), square_up_solution(4).into(), square_up(4).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (1)), square_up_solution(1).into(), square_up(1).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (0)), square_up_solution(0).into(), square_up(0).into());
    layout.print_footer();
}

pub fn series_up_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("series_up");
    layout.print_header();
    layout.print_test::<BatVec<i32>>(format!("{:?}", (3)), series_up_solution(3).into(), series_up(3).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (4)), series_up_solution(4).into(), series_up(4).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (2)), series_up_solution(2).into(), series_up(2).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (1)), series_up_solution(1).into(), series_up(1).into());
    layout.print_test::<BatVec<i32>>(format!("{:?}", (0)), series_up_solution(0).into(), series_up(0).into());
    layout.print_footer();
}

pub fn max_mirror_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("max_mirror");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 2, 3, 8, 9, 3, 2, 1])), max_mirror_solution(vec![1, 2, 3, 8, 9, 3, 2, 1]), max_mirror(vec![1, 2, 3, 8, 9, 3, 2, 1]));
    layout.print_test(format!("{:?}", (vec![1, 2, 1, 4])), max_mirror_solution(vec![1, 2, 1, 4]), max_mirror(vec![1, 2, 1, 4]));
    layout.print_test(format!("{:?}", (vec![7, 1, 2, 9, 7, 2, 1])), max_mirror_solution(vec![7, 1, 2, 9, 7, 2, 1]), max_mirror(vec![7, 1, 2, 9, 7, 2, 1]));
    layout.print_test(format!("{:?}", (vec![21, 22, 9, 8, 7, 6, 23, 24, 6, 7, 8, 9, 25, 7, 8, 9])), max_mirror_solution(vec![21, 22, 9, 8, 7, 6, 23, 24, 6, 7, 8, 9, 25, 7, 8, 9]), max_mirror(vec![21, 22, 9, 8, 7, 6, 23, 24, 6, 7, 8, 9, 25, 7, 8, 9]));
    layout.print_test(format!("{:?}", (vec![1, 2, 1, 20, 21, 1, 2, 1, 2, 23, 24, 2, 1, 2, 1, 25])), max_mirror_solution(vec![1, 2, 1, 20, 21, 1, 2, 1, 2, 23, 24, 2, 1, 2, 1, 25]), max_mirror(vec![1, 2, 1, 20, 21, 1, 2, 1, 2, 23, 24, 2, 1, 2, 1, 25]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3, 2, 1])), max_mirror_solution(vec![1, 2, 3, 2, 1]), max_mirror(vec![1, 2, 3, 2, 1]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3, 3, 8])), max_mirror_solution(vec![1, 2, 3, 3, 8]), max_mirror(vec![1, 2, 3, 3, 8]));
    layout.print_test(format!("{:?}", (vec![1, 2, 7, 8, 1, 7, 2])), max_mirror_solution(vec![1, 2, 7, 8, 1, 7, 2]), max_mirror(vec![1, 2, 7, 8, 1, 7, 2]));
    layout.print_test(format!("{:?}", (vec![1, 1, 1])), max_mirror_solution(vec![1, 1, 1]), max_mirror(vec![1, 1, 1]));
    layout.print_test(format!("{:?}", (vec![1])), max_mirror_solution(vec![1]), max_mirror(vec![1]));
    layout.print_test(format!("{:?}", (vec![0; 0])), max_mirror_solution(vec![0; 0]), max_mirror(vec![0; 0]));
    layout.print_test(format!("{:?}", (vec![9, 1, 1, 4, 2, 1, 1, 1])), max_mirror_solution(vec![9, 1, 1, 4, 2, 1, 1, 1]), max_mirror(vec![9, 1, 1, 4, 2, 1, 1, 1]));
    layout.print_test(format!("{:?}", (vec![5, 9, 9, 4, 5, 4, 9, 9, 2])), max_mirror_solution(vec![5, 9, 9, 4, 5, 4, 9, 9, 2]), max_mirror(vec![5, 9, 9, 4, 5, 4, 9, 9, 2]));
    layout.print_test(format!("{:?}", (vec![5, 9, 9, 6, 5, 4, 9, 9, 2])), max_mirror_solution(vec![5, 9, 9, 6, 5, 4, 9, 9, 2]), max_mirror(vec![5, 9, 9, 6, 5, 4, 9, 9, 2]));
    layout.print_test(format!("{:?}", (vec![5, 9, 1, 6, 5, 4, 1, 9, 5])), max_mirror_solution(vec![5, 9, 1, 6, 5, 4, 1, 9, 5]), max_mirror(vec![5, 9, 1, 6, 5, 4, 1, 9, 5]));
    layout.print_footer();
}

pub fn count_clumps_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("count_clumps");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 2, 2, 3, 4, 4])), count_clumps_solution(vec![1, 2, 2, 3, 4, 4]), count_clumps(vec![1, 2, 2, 3, 4, 4]));
    layout.print_test(format!("{:?}", (vec![1, 1, 2, 1, 1])), count_clumps_solution(vec![1, 1, 2, 1, 1]), count_clumps(vec![1, 1, 2, 1, 1]));
    layout.print_test(format!("{:?}", (vec![1, 1, 1, 1, 1])), count_clumps_solution(vec![1, 1, 1, 1, 1]), count_clumps(vec![1, 1, 1, 1, 1]));
    layout.print_test(format!("{:?}", (vec![1, 2, 3])), count_clumps_solution(vec![1, 2, 3]), count_clumps(vec![1, 2, 3]));
    layout.print_test(format!("{:?}", (vec![2, 2, 1, 1, 1, 2, 1, 1, 2, 2])), count_clumps_solution(vec![2, 2, 1, 1, 1, 2, 1, 1, 2, 2]), count_clumps(vec![2, 2, 1, 1, 1, 2, 1, 1, 2, 2]));
    layout.print_test(format!("{:?}", (vec![0, 2, 2, 1, 1, 1, 2, 1, 1, 2, 2])), count_clumps_solution(vec![0, 2, 2, 1, 1, 1, 2, 1, 1, 2, 2]), count_clumps(vec![0, 2, 2, 1, 1, 1, 2, 1, 1, 2, 2]));
    layout.print_test(format!("{:?}", (vec![0, 0, 2, 2, 1, 1, 1, 2, 1, 1, 2, 2])), count_clumps_solution(vec![0, 0, 2, 2, 1, 1, 1, 2, 1, 1, 2, 2]), count_clumps(vec![0, 0, 2, 2, 1, 1, 1, 2, 1, 1, 2, 2]));
    layout.print_test(format!("{:?}", (vec![0, 0, 0, 2, 2, 1, 1, 1, 2, 1, 1, 2, 2])), count_clumps_solution(vec![0, 0, 0, 2, 2, 1, 1, 1, 2, 1, 1, 2, 2]), count_clumps(vec![0, 0, 0, 2, 2, 1, 1, 1, 2, 1, 1, 2, 2]));
    layout.print_test(format!("{:?}", (vec![0; 0])), count_clumps_solution(vec![0; 0]), count_clumps(vec![0; 0]));
    layout.print_footer();
}
