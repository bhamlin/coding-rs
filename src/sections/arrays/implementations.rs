/// ## Array-1 -- firstLast6
/// Given an array of ints, return true if 6 appears as either the first or last
/// element in the array. The array will be length 1 or more.
/// ### Examples
///    first_last6([1, 2, 6]) -> true
///    first_last6([6, 1, 2, 3]) -> true
///    first_last6([13, 6, 1, 2, 3]) -> false
#[allow(unused)]
pub fn first_last6(nums: Vec<i32>) -> bool {
    todo!()
}

/// ## Array-1 -- sameFirstLast
/// Given an array of ints, return true if the array is length 1 or more, and
/// the first element and the last element are equal.
/// ### Examples
///    same_first_last([1, 2, 3]) -> false
///    same_first_last([1, 2, 3, 1]) -> true
///    same_first_last([1, 2, 1]) -> true
#[allow(unused)]
pub fn same_first_last(nums: Vec<i32>) -> bool {
    todo!()
}

/// ## Array-1 -- makePi
/// Return an int array length 3 containing the first 3 digits of pi, {3, 1, 4}.
/// ### Examples
///    make_pi() -> 3,1,4
#[allow(unused)]
pub fn make_pi() -> Vec<i32> {
    todo!()
}

/// ## Array-1 -- commonEnd
/// Given 2 arrays of ints, a and b, return true if they have the same first
/// element or they have the same last element. Both arrays will be length 1 or
/// more.
/// ### Examples
///    common_end([1, 2, 3], [7, 3]) -> true
///    common_end([1, 2, 3], [7, 3, 2]) -> false
///    common_end([1, 2, 3], [1, 3]) -> true
#[allow(unused)]
pub fn common_end(a: Vec<i32>, b: Vec<i32>) -> bool {
    todo!()
}

/// ## Array-1 -- sum3
/// Given an array of ints length 3, return the sum of all the elements.
/// ### Examples
///    sum3([1, 2, 3]) -> 6
///    sum3([5, 11, 2]) -> 18
///    sum3([7, 0, 0]) -> 7
#[allow(unused)]
pub fn sum3(nums: Vec<i32>) -> i32 {
    todo!()
}

/// ## Array-1 -- rotateLeft3
/// Given an array of ints length 3, return an array with the elements "rotated
/// left" so {1, 2, 3} yields {2, 3, 1}.
/// ### Examples
///    rotate_left3([1, 2, 3]) -> 2,3,1
///    rotate_left3([5, 11, 9]) -> 11,9,5
///    rotate_left3([7, 0, 0]) -> 0,0,7
#[allow(unused)]
pub fn rotate_left3(nums: Vec<i32>) -> Vec<i32> {
    todo!()
}

/// ## Array-1 -- reverse3
/// Given an array of ints length 3, return a new array with the elements in
/// reverse order, so {1, 2, 3} becomes {3, 2, 1}.
/// ### Examples
///    reverse3([1, 2, 3]) -> 3,2,1
///    reverse3([5, 11, 9]) -> 9,11,5
///    reverse3([7, 0, 0]) -> 0,0,7
#[allow(unused)]
pub fn reverse3(nums: Vec<i32>) -> Vec<i32> {
    todo!()
}

/// ## Array-1 -- maxEnd3
/// Given an array of ints length 3, figure out which is larger, the first or
/// last element in the array, and set all the other elements to be that value.
/// Return the changed array.
/// ### Examples
///    max_end3([1, 2, 3]) -> 3,3,3
///    max_end3([11, 5, 9]) -> 11,11,11
///    max_end3([2, 11, 3]) -> 3,3,3
#[allow(unused)]
pub fn max_end3(nums: Vec<i32>) -> Vec<i32> {
    todo!()
}

/// ## Array-1 -- sum2
/// Given an array of ints, return the sum of the first 2 elements in the array.
/// If the array length is less than 2, just sum up the elements that exist,
/// returning 0 if the array is length 0.
/// ### Examples
///    sum2([1, 2, 3]) -> 3
///    sum2([1, 1]) -> 2
///    sum2([1, 1, 1, 1]) -> 2
#[allow(unused)]
pub fn sum2(nums: Vec<i32>) -> i32 {
    todo!()
}

/// ## Array-1 -- middleWay
/// Given 2 int arrays, a and b, each length 3, return a new array length 2
/// containing their middle elements.
/// ### Examples
///    middle_way([1, 2, 3], [4, 5, 6]) -> 2,5
///    middle_way([7, 7, 7], [3, 8, 0]) -> 7,8
///    middle_way([5, 2, 9], [1, 4, 5]) -> 2,4
#[allow(unused)]
pub fn middle_way(a: Vec<i32>, b: Vec<i32>) -> Vec<i32> {
    todo!()
}

/// ## Array-1 -- makeEnds
/// Given an array of ints, return a new array length 2 containing the first and
/// last elements from the original array. The original array will be length 1
/// or more.
/// ### Examples
///    make_ends([1, 2, 3]) -> 1,3
///    make_ends([1, 2, 3, 4]) -> 1,4
///    make_ends([7, 4, 6, 2]) -> 7,2
#[allow(unused)]
pub fn make_ends(nums: Vec<i32>) -> Vec<i32> {
    todo!()
}

/// ## Array-1 -- has23
/// Given an int array length 2, return true if it contains a 2 or a 3.
/// ### Examples
///    has23([2, 5]) -> true
///    has23([4, 3]) -> true
///    has23([4, 5]) -> false
#[allow(unused)]
pub fn has23(nums: Vec<i32>) -> bool {
    todo!()
}

/// ## Array-1 -- no23
/// Given an int array length 2, return true if it does not contain a 2 or 3.
/// ### Examples
///    no23([4, 5]) -> true
///    no23([4, 2]) -> false
///    no23([3, 5]) -> false
#[allow(unused)]
pub fn no23(nums: Vec<i32>) -> bool {
    todo!()
}

/// ## Array-1 -- makeLast
/// Given an int array, return a new array with double the length where its last
/// element is the same as the original array, and all the other elements are 0.
/// The original array will be length 1 or more.
/// ### Examples
///    make_last([4, 5, 6]) -> 0,0,0,0,0,6
///    make_last([1, 2]) -> 0,0,0,2
///    make_last([3]) -> 0,3
#[allow(unused)]
pub fn make_last(nums: Vec<i32>) -> Vec<i32> {
    todo!()
}

/// ## Array-1 -- double23
/// Given an int array, return true if the array contains 2 twice, or 3 twice.
/// The array will be length 0, 1, or 2.
/// ### Examples
///    double23([2, 2]) -> true
///    double23([3, 3]) -> true
///    double23([2, 3]) -> false
#[allow(unused)]
pub fn double23(nums: Vec<i32>) -> bool {
    todo!()
}

/// ## Array-1 -- fix23
/// Given an int array length 3, if there is a 2 in the array immediately
/// followed by a 3, set the 3 element to 0. Return the changed array.
/// ### Examples
///    fix23([1, 2, 3]) -> 1,2,0
///    fix23([2, 3, 5]) -> 2,0,5
///    fix23([1, 2, 1]) -> 1,2,1
#[allow(unused)]
pub fn fix23(nums: Vec<i32>) -> Vec<i32> {
    todo!()
}

/// ## Array-1 -- start1
/// Start with 2 int arrays, a and b, of any length. Return how many of the
/// arrays have 1 as their first element.
/// ### Examples
///    start1([1, 2, 3], [1, 3]) -> 2
///    start1([7, 2, 3], [1]) -> 1
///    start1([1, 2], []) -> 1
#[allow(unused)]
pub fn start1(a: Vec<i32>, b: Vec<i32>) -> i32 {
    todo!()
}

/// ## Array-1 -- biggerTwo
/// Start with 2 int arrays, a and b, each length 2. Consider the sum of the
/// values in each array. Return the array which has the largest sum. In event
/// of a tie, return a.
/// ### Examples
///    bigger_two([1, 2], [3, 4]) -> 3,4
///    bigger_two([3, 4], [1, 2]) -> 3,4
///    bigger_two([1, 1], [1, 2]) -> 1,2
#[allow(unused)]
pub fn bigger_two(a: Vec<i32>, b: Vec<i32>) -> Vec<i32> {
    todo!()
}

/// ## Array-1 -- makeMiddle
/// Given an array of ints of even length, return a new array length 2
/// containing the middle two elements from the original array. The original
/// array will be length 2 or more.
/// ### Examples
///    make_middle([1, 2, 3, 4]) -> 2,3
///    make_middle([7, 1, 2, 3, 4, 9]) -> 2,3
///    make_middle([1, 2]) -> 1,2
#[allow(unused)]
pub fn make_middle(nums: Vec<i32>) -> Vec<i32> {
    todo!()
}

/// ## Array-1 -- plusTwo
/// Given 2 int arrays, each length 2, return a new array length 4 containing
/// all their elements.
/// ### Examples
///    plus_two([1, 2], [3, 4]) -> 1,2,3,4
///    plus_two([4, 4], [2, 2]) -> 4,4,2,2
///    plus_two([9, 2], [3, 4]) -> 9,2,3,4
#[allow(unused)]
pub fn plus_two(a: Vec<i32>, b: Vec<i32>) -> Vec<i32> {
    todo!()
}

/// ## Array-1 -- swapEnds
/// Given an array of ints, swap the first and last elements in the array.
/// Return the modified array. The array length will be at least 1.
/// ### Examples
///    swap_ends([1, 2, 3, 4]) -> 4,2,3,1
///    swap_ends([1, 2, 3]) -> 3,2,1
///    swap_ends([8, 6, 7, 9, 5]) -> 5,6,7,9,8
#[allow(unused)]
pub fn swap_ends(nums: Vec<i32>) -> Vec<i32> {
    todo!()
}

/// ## Array-1 -- midThree
/// Given an array of ints of odd length, return a new array length 3 containing
/// the elements from the middle of the array. The array length will be at least
/// 3.
/// ### Examples
///    mid_three([1, 2, 3, 4, 5]) -> 2,3,4
///    mid_three([8, 6, 7, 5, 3, 0, 9]) -> 7,5,3
///    mid_three([1, 2, 3]) -> 1,2,3
#[allow(unused)]
pub fn mid_three(nums: Vec<i32>) -> Vec<i32> {
    todo!()
}

/// ## Array-1 -- maxTriple
/// Given an array of ints of odd length, look at the first, last, and middle
/// values in the array and return the largest. The array length will be a least
/// 1.
/// ### Examples
///    max_triple([1, 2, 3]) -> 3
///    max_triple([1, 5, 3]) -> 5
///    max_triple([5, 2, 3]) -> 5
#[allow(unused)]
pub fn max_triple(nums: Vec<i32>) -> i32 {
    todo!()
}

/// ## Array-1 -- frontPiece
/// Given an int array of any length, return a new array of its first 2
/// elements. If the array is smaller than length 2, use whatever elements are
/// present.
/// ### Examples
///    front_piece([1, 2, 3]) -> 1,2
///    front_piece([1, 2]) -> 1,2
///    front_piece([1]) -> 1
#[allow(unused)]
pub fn front_piece(nums: Vec<i32>) -> Vec<i32> {
    todo!()
}

/// ## Array-1 -- unlucky1
/// We'll say that a 1 immediately followed by a 3 in an array is an "unlucky"
/// 1. Return true if the given array contains an unlucky 1 in the first 2 or
/// last 2 positions in the array.
/// ### Examples
///    unlucky1([1, 3, 4, 5]) -> true
///    unlucky1([2, 1, 3, 4, 5]) -> true
///    unlucky1([1, 1, 1]) -> false
#[allow(unused)]
pub fn unlucky1(nums: Vec<i32>) -> bool {
    todo!()
}

/// ## Array-1 -- make2
/// Given 2 int arrays, a and b, return a new array length 2 containing, as much
/// as will fit, the elements from a followed by the elements from b. The arrays
/// may be any length, including 0, but there will be 2 or more elements
/// available between the 2 arrays.
/// ### Examples
///    make2([4, 5], [1, 2, 3]) -> 4,5
///    make2([4], [1, 2, 3]) -> 4,1
///    make2([], [1, 2]) -> 1,2
#[allow(unused)]
pub fn make2(a: Vec<i32>, b: Vec<i32>) -> Vec<i32> {
    todo!()
}

/// ## Array-1 -- front11
/// Given 2 int arrays, a and b, of any length, return a new array with the
/// first element of each array. If either array is length 0, ignore that array.
/// ### Examples
///    front11([1, 2, 3], [7, 9, 8]) -> 1,7
///    front11([1], [2]) -> 1,2
///    front11([1, 7], []) -> 1
#[allow(unused)]
pub fn front11(a: Vec<i32>, b: Vec<i32>) -> Vec<i32> {
    todo!()
}

/// ## Array-2 -- findLowestIndex
/// Return the index of the minimum value in an array. The input array will have
/// at least one element in it.
/// ### Examples
///    find_lowest_index([99, 98, 97, 96, 95]) -> 4
///    find_lowest_index([2, 2, 0]) -> 2
///    find_lowest_index([1, 3, 5]) -> 0
#[allow(unused)]
pub fn find_lowest_index(nums: Vec<i32>) -> i32 {
    todo!()
}

/// ## Array-2 -- countEvens
/// Return the number of even ints in the given array. Note: the % "mod"
/// operator computes the remainder, e.g. 5 % 2 is 1.
/// ### Examples
///    count_evens([2, 1, 2, 3, 4]) -> 3
///    count_evens([2, 2, 0]) -> 3
///    count_evens([1, 3, 5]) -> 0
#[allow(unused)]
pub fn count_evens(nums: Vec<i32>) -> i32 {
    todo!()
}

/// ## Array-2 -- bigDiff
/// Given an array length 1 or more of ints, return the difference between the
/// largest and smallest values in the array. Note: the built-in Math.min(v1,
/// v2) and Math.max(v1, v2) methods return the smaller or larger of two values.
/// ### Examples
///    big_diff([10, 3, 5, 6]) -> 7
///    big_diff([7, 2, 10, 9]) -> 8
///    big_diff([2, 10, 7, 2]) -> 8
#[allow(unused)]
pub fn big_diff(nums: Vec<i32>) -> i32 {
    todo!()
}

/// ## Array-2 -- centeredAverage
/// Return the "centered" average of an array of ints, which we'll say is the
/// mean average of the values, except ignoring the largest and smallest values
/// in the array. If there are multiple copies of the smallest value, ignore
/// just one copy, and likewise for the largest value. Use int division to
/// produce the final average. You may assume that the array is length 3 or
/// more.
/// ### Examples
///    centered_average([1, 2, 3, 4, 100]) -> 3
///    centered_average([1, 1, 5, 5, 10, 8, 7]) -> 5.2
///    centered_average([-10, -4, -2, -4, -2, 0]) -> -3
#[allow(unused)]
pub fn centered_average(nums: Vec<i32>) -> f64 {
    todo!()
}

/// ## Array-2 -- sum13
/// Return the sum of the numbers in the array, returning 0 for an empty array.
/// Except the number 13 is very unlucky, so it does not count and numbers that
/// come immediately after a 13 also do not count.
/// ### Examples
///    sum13([1, 2, 2, 1]) -> 6
///    sum13([1, 1]) -> 2
///    sum13([1, 2, 2, 1, 13]) -> 6
#[allow(unused)]
pub fn sum13(nums: Vec<i32>) -> i32 {
    todo!()
}

/// ## Array-2 -- sum67
/// Return the sum of the numbers in the array, except ignore sections of
/// numbers starting with a 6 and extending to the next 7 (every 6 will be
/// followed by at least one 7). Return 0 for no numbers.
/// ### Examples
///    sum67([1, 2, 2]) -> 5
///    sum67([1, 2, 2, 6, 99, 99, 7]) -> 5
///    sum67([1, 1, 6, 7, 2]) -> 4
#[allow(unused)]
pub fn sum67(nums: Vec<i32>) -> i32 {
    todo!()
}

/// ## Array-2 -- has22
/// Given an array of ints, return true if the array contains a 2 next to a 2
/// somewhere.
/// ### Examples
///    has22([1, 2, 2]) -> true
///    has22([1, 2, 1, 2]) -> false
///    has22([2, 1, 2]) -> false
#[allow(unused)]
pub fn has22(nums: Vec<i32>) -> bool {
    todo!()
}

/// ## Array-2 -- lucky13
/// Given an array of ints, return true if the array contains no 1's and no 3's.
/// ### Examples
///    lucky13([0, 2, 4]) -> true
///    lucky13([1, 2, 3]) -> false
///    lucky13([1, 2, 4]) -> false
#[allow(unused)]
pub fn lucky13(nums: Vec<i32>) -> bool {
    todo!()
}

/// ## Array-2 -- sum28
/// Given an array of ints, return true if the sum of all the 2's in the array
/// is exactly 8.
/// ### Examples
///    sum28([2, 3, 2, 2, 4, 2]) -> true
///    sum28([2, 3, 2, 2, 4, 2, 2]) -> false
///    sum28([1, 2, 3, 4]) -> false
#[allow(unused)]
pub fn sum28(nums: Vec<i32>) -> bool {
    todo!()
}

/// ## Array-2 -- more14
/// Given an array of ints, return true if the number of 1's is greater than the
/// number of 4's
/// ### Examples
///    more14([1, 4, 1]) -> true
///    more14([1, 4, 1, 4]) -> false
///    more14([1, 1]) -> true
#[allow(unused)]
pub fn more14(nums: Vec<i32>) -> bool {
    todo!()
}

/// ## Array-2 -- prependSum
/// Return a modified version of the input array (nums), where the first two
/// items have been removed and one item – the sum of those two items - is added
/// to the start of the array.
/// ### Examples
///    prepend_sum([1, 2, 4, 4]) -> 3,4,4
///    prepend_sum([3, 3, 0]) -> 6,0
///    prepend_sum([1, 1, 1, 1, 1]) -> 2,1,1,1
#[allow(unused)]
pub fn prepend_sum(nums: Vec<i32>) -> Vec<i32> {
    todo!()
}

/// ## Array-2 -- fizzArray
/// Given a number n, create and return a new array of length n, containing the
/// numbers 0, 1, 2, ... n-1. The given n may be 0, in which case just return a
/// length 0 array. You do not need a separate if-statement for the length-0
/// case; the for-loop should naturally execute 0 times in that case, so it just
/// works. The syntax to make a new array is let myArray = [];
/// ### Examples
///    fizz_array(4) -> 0,1,2,3
///    fizz_array(1) -> 0
///    fizz_array(10) -> 0,1,2,3,4,5,6,7,8,9
#[allow(unused)]
pub fn fizz_array(n: i32) -> Vec<i32> {
    todo!()
}

/// ## Array-2 -- only14
/// Given an array of ints, return true if every element is a 1 or a 4.
/// ### Examples
///    only14([1, 4, 1, 4]) -> true
///    only14([1, 4, 2, 4]) -> false
///    only14([1, 1]) -> true
#[allow(unused)]
pub fn only14(nums: Vec<i32>) -> bool {
    todo!()
}

/// ## Array-2 -- fizzArray2
/// Given a number n, create and return a new string array of length n,
/// containing the strings "0", "1" "2" .. through n-1. N may be 0, in which
/// case just return a length 0 array. Note: String(xxx) will make the String
/// form of most types.
/// ### Examples
///    fizz_array2(4) -> 0,1,2,3
///    fizz_array2(10) -> 0,1,2,3,4,5,6,7,8,9
///    fizz_array2(2) -> 0,1
#[allow(unused)]
pub fn fizz_array2(n: i32) -> Vec<String> {
    todo!()
}

/// ## Array-2 -- no14
/// Given an array of ints, return true if it contains no 1's or it contains no
/// 4's.
/// ### Examples
///    no14([1, 2, 3]) -> true
///    no14([1, 2, 3, 4]) -> false
///    no14([2, 3, 4]) -> true
#[allow(unused)]
pub fn no14(nums: Vec<i32>) -> bool {
    todo!()
}

/// ## Array-2 -- isEverywhere
/// We'll say that a value is "everywhere" in an array if for every pair of
/// adjacent elements in the array, at least one of the pair is that value.
/// Return true if the given value is everywhere in the array.
/// ### Examples
///    is_everywhere([1, 2, 1, 3], 1) -> true
///    is_everywhere([1, 2, 1, 3], 2) -> false
///    is_everywhere([1, 2, 1, 3, 4], 1) -> false
#[allow(unused)]
pub fn is_everywhere(nums: Vec<i32>, val: i32) -> bool {
    todo!()
}

/// ## Array-2 -- either24
/// Given an array of ints, return true if the array contains a 2 next to a 2 or
/// a 4 next to a 4, but not both.
/// ### Examples
///    either24([1, 2, 2]) -> true
///    either24([4, 4, 1]) -> true
///    either24([4, 4, 1, 2, 2]) -> false
#[allow(unused)]
pub fn either24(nums: Vec<i32>) -> bool {
    todo!()
}

/// ## Array-2 -- matchUp
/// Given arrays nums1 and nums2 of the same length, for every element in nums1,
/// consider the corresponding element in nums2 (at the same index). Return the
/// count of the number of times that the two elements differ by 2 or less, but
/// are not equal.
/// ### Examples
///    match_up([1, 2, 3], [2, 3, 10]) -> 0
///    match_up([1, 2, 3], [2, 3, 5]) -> 0
///    match_up([1, 2, 3], [2, 3, 3]) -> 0
#[allow(unused)]
pub fn match_up(a: Vec<i32>, b: Vec<i32>) -> i32 {
    todo!()
}

/// ## Array-2 -- has77
/// Given an array of ints, return true if the array contains two 7's next to
/// each other, or there are two 7's separated by one element, such as with {7,
/// 1, 7}.
/// ### Examples
///    has77([1, 7, 7]) -> true
///    has77([1, 7, 1, 7]) -> true
///    has77([1, 7, 1, 1, 7]) -> false
#[allow(unused)]
pub fn has77(nums: Vec<i32>) -> bool {
    todo!()
}

/// ## Array-2 -- has12
/// Given an array of ints, return true if there is a 1 in the array with a 2
/// somewhere later in the array.
/// ### Examples
///    has12([1, 3, 2]) -> true
///    has12([3, 1, 2]) -> true
///    has12([3, 1, 4, 5, 2]) -> true
#[allow(unused)]
pub fn has12(nums: Vec<i32>) -> bool {
    todo!()
}

/// ## Array-2 -- modThree
/// Given an array of ints, return true if the array contains either 3 even or 3
/// odd values all next to each other.
/// ### Examples
///    mod_three([2, 1, 3, 5]) -> true
///    mod_three([2, 1, 2, 5]) -> false
///    mod_three([2, 4, 2, 5]) -> true
#[allow(unused)]
pub fn mod_three(nums: Vec<i32>) -> bool {
    todo!()
}

/// ## Array-2 -- findTheMedian
/// Write a method that returns the median value of an array. The input array
/// will never be empty. If the array is odd in length, the median is the value
/// in the centre of the array. If the array is even, the median should be the
/// average of the two middle values. Hint: You will need to ensure the input
/// array is sorted - there is a sort() array method you can use for this step.
/// ### Examples
///    find_the_median([4, 9, 9, 2, 1, 5]) -> 5.5
///    find_the_median([1, 5, 3, 1 , 5]) -> 3.0
///    find_the_median([10, 12, 15]) -> 12.0
#[allow(unused)]
pub fn find_the_median(nums: Vec<i32>) -> f64 {
    todo!()
}

/// ## Array-2 -- haveThree
/// Given an array of ints, return true if the value 3 appears in the array
/// exactly 3 times, and no 3's are next to each other.
/// ### Examples
///    have_three([3, 1, 3, 1, 3]) -> true
///    have_three([3, 1, 3, 3]) -> false
///    have_three([3, 4, 3, 3, 4]) -> false
#[allow(unused)]
pub fn have_three(nums: Vec<i32>) -> bool {
    todo!()
}

/// ## Array-2 -- twoTwo
/// Given an array of ints, return true if every 2 that appears in the array is
/// next to another 2.
/// ### Examples
///    two_two([4, 2, 2, 3]) -> true
///    two_two([2, 2, 4]) -> true
///    two_two([2, 2, 4, 2]) -> false
#[allow(unused)]
pub fn two_two(nums: Vec<i32>) -> bool {
    todo!()
}

/// ## Array-2 -- sameEnds
/// Return true if the group of N numbers at the start and end of the array are
/// the same. For example, with {5, 6, 45, 99, 13, 5, 6}, the ends are the same
/// for n=0 and n=2, and false for n=1 and n=3. You may assume that n is in the
/// range 0..nums.length inclusive.
/// ### Examples
///    same_ends([5, 6, 45, 99, 13, 5, 6], 1) -> false
///    same_ends([5, 6, 45, 99, 13, 5, 6], 2) -> true
///    same_ends([5, 6, 45, 99, 13, 5, 6], 3) -> false
#[allow(unused)]
pub fn same_ends(nums: Vec<i32>, len: usize) -> bool {
    todo!()
}

/// ## Array-2 -- tripleUp
/// Return true if the array contains, somewhere, three increasing adjacent
/// numbers like .... 4, 5, 6, ... or 23, 24, 25.
/// ### Examples
///    triple_up([1, 4, 5, 6, 2]) -> true
///    triple_up([1, 2, 3]) -> true
///    triple_up([1, 2, 4]) -> false
#[allow(unused)]
pub fn triple_up(nums: Vec<i32>) -> bool {
    todo!()
}

/// ## Array-2 -- fizzArray3
/// Given start and end numbers, return a new array containing the sequence of
/// integers from start up to but not including end, so start=5 and end=10
/// yields {5, 6, 7, 8, 9}. The end number will be greater or equal to the start
/// number. Note that a length-0 array is valid.
/// ### Examples
///    fizz_array3(5, 10) -> 5, 6, 7, 8, 9
///    fizz_array3(11, 18) -> 11, 12, 13, 14, 15, 16, 17
///    fizz_array3(1, 3) -> 1,2
#[allow(unused)]
pub fn fizz_array3(start: i32, end: i32) -> Vec<i32> {
    todo!()
}

/// ## Array-2 -- shiftLeft
/// Return an array that is "left shifted" by one -- so {6, 2, 5, 3} returns {2,
/// 5, 3, 6}. You may modify and return the given array, or return a new array.
/// ### Examples
///    shift_left([6, 2, 5, 3]) -> 2,5,3,6
///    shift_left([1, 2]) -> 2,1
///    shift_left([1]) -> 1
#[allow(unused)]
pub fn shift_left(nums: Vec<i32>) -> Vec<i32> {
    todo!()
}

/// ## Array-2 -- tenRun
/// For each multiple of 10 in the given array, change all the values following
/// it to be that multiple of 10, until encountering another multiple of 10. So
/// {2, 10, 3, 4, 20, 5} yields {2, 10, 10, 10, 20, 20}.
/// ### Examples
///    ten_run([2, 10, 3, 4, 20, 5]) -> 2,10,10,10,20,20
///    ten_run([10, 1, 20, 2]) -> 10,10,20,20
///    ten_run([10, 1, 9, 20]) -> 10,10,10,20
#[allow(unused)]
pub fn ten_run(nums: Vec<i32>) -> Vec<i32> {
    todo!()
}

/// ## Array-2 -- pre4
/// Given a non-empty array of ints, return a new array containing the elements
/// from the original array that come before the first 4 in the original array.
/// The original array will contain at least one 4. Note that it is valid in
/// java to create an array of length 0.
/// ### Examples
///    pre4([1, 2, 4, 1]) -> 1,2
///    pre4([3, 1, 4]) -> 3,1
///    pre4([1, 4, 4]) -> 1
#[allow(unused)]
pub fn pre4(nums: Vec<i32>) -> Vec<i32> {
    todo!()
}

/// ## Array-2 -- post4
/// Given a non-empty array of ints, return a new array containing the elements
/// from the original array that come after the last 4 in the original array.
/// The original array will contain at least one 4. Note that it is valid in
/// java to create an array of length 0.
/// ### Examples
///    post4([2, 4, 1, 2]) -> 1,2
///    post4([4, 1, 4, 2]) -> 2
///    post4([4, 4, 1, 2, 3]) -> 1,2,3
#[allow(unused)]
pub fn post4(nums: Vec<i32>) -> Vec<i32> {
    todo!()
}

/// ## Array-2 -- notAlone
/// We'll say that an element in an array is "alone" if there are values before
/// and after it, and those values are different from it. Return a version of
/// the given array where every instance of the given value which is alone is
/// replaced by whichever value to its left or right is larger.
/// ### Examples
///    not_alone([1, 2, 3], 2) -> 1,3,3
///    not_alone([1, 2, 3, 2, 5, 2], 2) -> 1,3,3,5,5,2
///    not_alone([3, 4], 3) -> 3,4
#[allow(unused)]
pub fn not_alone(nums: Vec<i32>, val: i32) -> Vec<i32> {
    todo!()
}

/// ## Array-2 -- zeroFront
/// Return an array that contains the exact same numbers as the given array, but
/// rearranged so that all the zeros are grouped at the start of the array. The
/// order of the non-zero numbers does not matter. So {1, 0, 0, 1} becomes {0
/// ,0, 1, 1}. You may modify and return the given array or make a new array.
/// ### Examples
///    zero_front([1, 0, 0, 1]) -> 0,0,1,1
///    zero_front([0, 1, 1, 0, 1]) -> 0,0,1,1,1
///    zero_front([1, 0]) -> 0,1
#[allow(unused)]
pub fn zero_front(nums: Vec<i32>) -> Vec<i32> {
    todo!()
}

/// ## Array-2 -- withoutTen
/// Return a version of the given array where all the 10's have been removed.
/// The remaining elements should shift left towards the start of the array as
/// needed, and the empty spaces a the end of the array should be 0. So {1, 10,
/// 10, 2} yields {1, 2, 0, 0}. You may modify and return the given array or
/// make a new array.
/// ### Examples
///    without_ten([1, 10, 10, 2]) -> 1,2,0,0
///    without_ten([10, 2, 10]) -> 2,0,0
///    without_ten([1, 99, 10]) -> 1,99,0
#[allow(unused)]
pub fn without_ten(nums: Vec<i32>) -> Vec<i32> {
    todo!()
}

/// ## Array-2 -- zeroMax
/// Return a version of the given array where each zero value in the array is
/// replaced by the largest odd value to the right of the zero in the array. If
/// there is no odd value to the right of the zero, leave the zero as a zero.
/// ### Examples
///    zero_max([0, 5, 0, 3]) -> 5,5,3,3
///    zero_max([0, 4, 0, 3]) -> 3,4,3,3
///    zero_max([0, 1, 0]) -> 1,1,0
#[allow(unused)]
pub fn zero_max(nums: Vec<i32>) -> Vec<i32> {
    todo!()
}

/// ## Array-2 -- evenOdd
/// Return an array that contains the exact same numbers as the given array, but
/// rearranged so that all the even numbers come before all the odd numbers.
/// Other than that, the numbers can be in any order. You may modify and return
/// the given array, or make a new array.
/// ### Examples
///    even_odd([1, 0, 1, 0, 0, 1, 1]) -> 0,0,0,1,1,1,1
///    even_odd([3, 3, 2]) -> 2,3,3
///    even_odd([2, 2, 2]) -> 2,2,2
#[allow(unused)]
pub fn even_odd(nums: Vec<i32>) -> Vec<i32> {
    todo!()
}

/// ## Array-2 -- fizzBuzz
/// This is slightly more difficult version of the famous FizzBuzz problem which
/// is sometimes given as a first problem for job interviews. Consider the
/// series of numbers beginning at start and running up to but not including
/// end, so for example start=1 and end=5 gives the series 1, 2, 3, 4. Return a
/// new String[] array containing the string form of these numbers, except for
/// multiples of 3, use "Fizz" instead of the number, for multiples of 5 use
/// "Buzz", and for multiples of both 3 and 5 use "FizzBuzz". In Java,
/// String.valueOf(xxx) will make the String form of an int or other type. This
/// version is a little more complicated than the usual version since you have
/// to allocate and index into an array instead of just printing, and we vary
/// the start/end instead of just always doing 1..100.
/// ### Examples
///    fizz_buzz(1, 6) -> 1,2,Fizz,4,Buzz
///    fizz_buzz(1, 8) -> 1,2,Fizz,4,Buzz,Fizz,7
///    fizz_buzz(1, 11) -> 1,2,Fizz,4,Buzz,Fizz,7,8,Fizz,Buzz
#[allow(unused)]
pub fn fizz_buzz(start: i32, end: i32) -> Vec<String> {
    todo!()
}

/// ## Array-3 -- maxSpan
/// Consider the leftmost and righmost appearances of some value in an array.
/// We'll say that the "span" is the number of elements between the two
/// inclusive. A single value has a span of 1. Returns the largest span found in
/// the given array. (Efficiency is not a priority.)
/// ### Examples
///    max_span([1, 2, 1, 1, 3]) -> 4
///    max_span([1, 4, 2, 1, 4, 1, 4]) -> 6
///    max_span([1, 4, 2, 1, 4, 4, 4]) -> 6
#[allow(unused)]
pub fn max_span(nums: Vec<i32>) -> i32 {
    todo!()
}

/// ## Array-3 -- fix34
/// Return an array that contains exactly the same numbers as the given array,
/// but rearranged so that every 3 is immediately followed by a 4. Do not move
/// the 3's, but every other number may move. The array contains the same number
/// of 3's and 4's, every 3 has a number after it that is not a 3, and a 3
/// appears in the array before any 4.
/// ### Examples
///    fix34([1, 3, 1, 4]) -> 1,3,4,1
///    fix34([1, 3, 1, 4, 4, 3, 1]) -> 1,3,4,1,1,3,4
///    fix34([3, 2, 2, 4]) -> 3,4,2,2
#[allow(unused)]
pub fn fix34(nums: Vec<i32>) -> Vec<i32> {
    todo!()
}

/// ## Array-3 -- fix45
/// (This is a slightly harder version of the fix34 problem.) Return an array
/// that contains exactly the same numbers as the given array, but rearranged so
/// that every 4 is immediately followed by a 5. Do not move the 4's, but every
/// other number may move. The array contains the same number of 4's and 5's,
/// and every 4 has a number after it that is not a 4. In this version, 5's may
/// appear anywhere in the original array.
/// ### Examples
///    fix45([5, 4, 9, 4, 9, 5]) -> 9,4,5,4,5,9
///    fix45([1, 4, 1, 5]) -> 1,4,5,1
///    fix45([1, 4, 1, 5, 5, 4, 1]) -> 1,4,5,1,1,4,5
#[allow(unused)]
pub fn fix45(nums: Vec<i32>) -> Vec<i32> {
    todo!()
}

/// ## Array-3 -- canBalance
/// Given a non-empty array, return true if there is a place to split the array
/// so that the sum of the numbers on one side is equal to the sum of the
/// numbers on the other side.
/// ### Examples
///    can_balance([1, 1, 1, 2, 1]) -> true
///    can_balance([2, 1, 1, 2, 1]) -> false
///    can_balance([10, 10]) -> true
#[allow(unused)]
pub fn can_balance(nums: Vec<i32>) -> bool {
    todo!()
}

/// ## Array-3 -- linearIn
/// Given two arrays of ints sorted in increasing order, outer and inner, return
/// true if all of the numbers in inner appear in outer. The best solution makes
/// only a single "linear" pass of both arrays, taking advantage of the fact
/// that both arrays are already in sorted order.
/// ### Examples
///    linear_in([1, 2, 4, 6], [2, 4]) -> true
///    linear_in([1, 2, 4, 6], [2, 3, 4]) -> false
///    linear_in([1, 2, 4, 4, 6], [2, 4]) -> true
#[allow(unused)]
pub fn linear_in(outer: Vec<i32>, inner: Vec<i32>) -> bool {
    todo!()
}

/// ## Array-3 -- squareUp
/// Given n>=0, create an array length n*n with the following pattern, shown
/// here for n=3 : {0, 0, 1, 0, 2, 1, 3, 2, 1} (spaces added to show the 3
/// groups).
/// ### Examples
///    square_up(3) -> 0,0,1,0,2,1,3,2,1
///    square_up(2) -> 0,1,2,1
///    square_up(4) -> 0,0,0,1,0,0,0,2,1,0,3,2,1,4,3,2,1
#[allow(unused)]
pub fn square_up(n: i32) -> Vec<i32> {
    todo!()
}

/// ## Array-3 -- seriesUp
/// Given n>=0, create an array with the pattern {1, 1, 2, 1, 2, 3, ... 1, 2, 3
/// .. n} (spaces added to show the grouping). Note that the length of the array
/// will be 1 + 2 + 3 ... + n, which is known to sum to exactly n*(n + 1)/2.
/// ### Examples
///    series_up(3) -> 1,1,2,1,2,3
///    series_up(4) -> 1,1,2,1,2,3,1,2,3,4
///    series_up(2) -> 1,1,2
#[allow(unused)]
pub fn series_up(n: i32) -> Vec<i32> {
    todo!()
}

/// ## Array-3 -- maxMirror
/// We'll say that a "mirror" section in an array is a group of contiguous
/// elements such that somewhere in the array, the same group appears in reverse
/// order. For example, the largest mirror section in {1, 2, 3, 8, 9, 3, 2, 1}
/// is length 3 (the {1, 2, 3} part). Return the size of the largest mirror
/// section found in the given array.
/// ### Examples
///    max_mirror([1, 2, 3, 8, 9, 3, 2, 1]) -> 3
///    max_mirror([1, 2, 1, 4]) -> 3
///    max_mirror([7, 1, 2, 9, 7, 2, 1]) -> 2
#[allow(unused)]
pub fn max_mirror(nums: Vec<i32>) -> i32 {
    todo!()
}

/// ## Array-3 -- countClumps
/// Say that a "clump" in an array is a series of 2 or more adjacent elements of
/// the same value. Return the number of clumps in the given array.
/// ### Examples
///    count_clumps([1, 2, 2, 3, 4, 4]) -> 2
///    count_clumps([1, 1, 2, 1, 1]) -> 2
///    count_clumps([1, 1, 1, 1, 1]) -> 1
#[allow(unused)]
pub fn count_clumps(nums: Vec<i32>) -> i32 {
    todo!()
}
