use std::cmp::{max, min};

use itertools::Itertools;

fn get_indicies_of_value_in<T>(vec: &Vec<T>, value: T) -> Vec<usize>
where
    T: PartialEq,
{
    vec.iter() // Wrap
        .enumerate()
        .filter_map(|(i, e)| if *e == value { Some(i) } else { None })
        .collect::<Vec<_>>()
}

pub fn first_last6_solution(nums: Vec<i32>) -> bool {
    let first: i32 = *nums.first().unwrap_or(&0);
    let last: i32 = *nums.last().unwrap_or(&0);

    first == 6 || last == 6
}

pub fn same_first_last_solution(nums: Vec<i32>) -> bool {
    if nums.len() > 0 {
        let first: i32 = *nums.first().unwrap_or(&0);
        let last: i32 = *nums.last().unwrap_or(&0);

        first == last
    } else {
        false
    }
}

pub fn make_pi_solution() -> Vec<i32> {
    vec![3, 1, 4].into()
}

pub fn common_end_solution<T: PartialEq>(a: Vec<T>, b: Vec<T>) -> bool {
    if a.len() < 1 || b.len() < 1 {
        false
    } else {
        let a_len = a.len();
        let b_len = b.len();
        let firsts = vec![&a[0], &b[0]];
        let lasts = vec![&a[a_len - 1], &b[b_len - 1]];

        firsts.iter().all(|e| e == &firsts[0]) || // Wrap
         lasts.iter().all(|e| e == &lasts[0])
    }
}

pub fn sum3_solution(nums: Vec<i32>) -> i32 {
    nums.iter().sum()
}

pub fn rotate_left3_solution(nums: Vec<i32>) -> Vec<i32> {
    let mut result = nums.clone();
    result.rotate_left(1);
    result
}

pub fn reverse3_solution(nums: Vec<i32>) -> Vec<i32> {
    let mut result = nums.clone();
    result.reverse();
    result
}

pub fn max_end3_solution(nums: Vec<i32>) -> Vec<i32> {
    let first: i32 = *nums.first().unwrap_or(&0);
    let last: i32 = *nums.last().unwrap_or(&0);
    let value = max(first, last);

    vec![value; 3]
}

pub fn sum2_solution(nums: Vec<i32>) -> i32 {
    nums.iter().take(2).sum()
}

pub fn middle_way_solution(a: Vec<i32>, b: Vec<i32>) -> Vec<i32> {
    vec![a[1], b[1]]
}

pub fn make_ends_solution(nums: Vec<i32>) -> Vec<i32> {
    let first: i32 = *nums.first().unwrap_or(&0);
    let last: i32 = *nums.last().unwrap_or(&0);

    vec![first, last]
}

pub fn has23_solution(nums: Vec<i32>) -> bool {
    nums.iter().any(|&e| e == 2 || e == 3)
}

pub fn no23_solution(nums: Vec<i32>) -> bool {
    // !has23_solution(nums)
    nums.iter().all(|&e| e != 2 && e != 3)
}

pub fn make_last_solution(nums: Vec<i32>) -> Vec<i32> {
    let last: i32 = *nums.last().unwrap_or(&0);
    let mut result = vec![nums[0]; (nums.len() * 2) - 1];
    result.push(last);
    result
}

pub fn double23_solution(nums: Vec<i32>) -> bool {
    nums.len() > 1 && nums.iter().all(|&e| e == nums[0]) && ((nums[0] == 2) || nums[0] == 3)
}

pub fn fix23_solution(nums: Vec<i32>) -> Vec<i32> {
    let mut two_alert = false;
    let mut output = vec![nums[0]; 0];

    for i in 0..nums.len() {
        if two_alert && nums[i] == 3 {
            output.push(0);
            two_alert = false;
        } else {
            if nums[i] == 2 {
                two_alert = true;
            } else {
                two_alert = false;
            }
            output.push(nums[i]);
        }
    }

    output
}

pub fn start1_solution(a: Vec<i32>, b: Vec<i32>) -> i32 {
    let a_one = if (a.len() > 0) && (a[0] == 1) { 1 } else { 0 };
    let b_one = if (b.len() > 0) && (b[0] == 1) { 1 } else { 0 };

    a_one + b_one
}

pub fn bigger_two_solution(a: Vec<i32>, b: Vec<i32>) -> Vec<i32> {
    let a_sum: i32 = a.iter().sum();
    let b_sum: i32 = b.iter().sum();

    if b_sum > a_sum {
        b
    } else {
        a
    }
}

pub fn make_middle_solution(nums: Vec<i32>) -> Vec<i32> {
    let len = nums.len();

    nums.iter().skip((len / 2) - 1).take(2).map(|&e| e).collect::<Vec<i32>>()
}

pub fn plus_two_solution(a: Vec<i32>, b: Vec<i32>) -> Vec<i32> {
    let mut output = vec![a[0]; 0];
    output.extend(a);
    output.extend(b);

    output
}

pub fn swap_ends_solution(nums: Vec<i32>) -> Vec<i32> {
    let len = nums.len();

    if len > 0 {
        let mut nums = nums;
        let end = nums.len() - 1;

        if end > 0 {
            nums[0] = nums[0] ^ nums[end];
            nums[end] = nums[end] ^ nums[0];
            nums[0] = nums[0] ^ nums[end];
        }

        nums
    } else {
        nums
    }
}

pub fn mid_three_solution(nums: Vec<i32>) -> Vec<i32> {
    let len = nums.len();

    nums.iter().skip((len / 2) - 1).take(3).map(|&e| e).collect::<Vec<i32>>()
}

pub fn max_triple_solution(nums: Vec<i32>) -> i32 {
    let len = nums.len();
    let first = nums[0];
    let middle = nums[len / 2];
    let last = nums[len - 1];

    vec![first, middle, last]
        .iter() //Wrap
        .map(|&e| e)
        .fold(nums[0], |a, e| max(a, e))
}

pub fn front_piece_solution(nums: Vec<i32>) -> Vec<i32> {
    nums.iter().take(2).map(|e| *e).collect::<Vec<i32>>()
}

pub fn unlucky1_solution(nums: Vec<i32>) -> bool {
    let front1 = nums.iter().take(2).map(|e| *e).collect::<Vec<_>>();
    let front2 = nums.iter().skip(1).take(2).map(|e| *e).collect::<Vec<_>>();
    let back = nums.iter().rev().take(2).map(|e| *e).collect::<Vec<_>>();
    // let back = back.iter().rev().take(2).map(|e| *e).collect::<Vec<_>>();
    let bad = vec![1, 3];
    let bad_back = vec![3, 1];

    front1 == bad || front2 == bad || back == bad_back
}

pub fn make2_solution(a: Vec<i32>, b: Vec<i32>) -> Vec<i32> {
    let mut result = vec![0; 0];

    result.extend(a.iter().take(2));

    if result.len() < 2 {
        let remaining = 2 - result.len();
        result.extend(b.iter().take(remaining));
    }

    result
}

pub fn front11_solution(a: Vec<i32>, b: Vec<i32>) -> Vec<i32> {
    let mut result = vec![0; 0];
    result.extend(a.iter().take(1));
    result.extend(b.iter().take(1));

    result
}

pub fn find_lowest_index_solution(nums: Vec<i32>) -> i32 {
    let len = nums.len();
    let mut index = 0 as usize;
    let mut value = nums[0];

    for i in 1..len {
        if nums[i] < value {
            value = nums[i];
            index = i;
        }
    }

    index as i32
}

pub fn count_evens_solution(nums: Vec<i32>) -> i32 {
    nums.iter().filter(|&e| 0 == e % 2).count().try_into().unwrap_or(0)
}

pub fn big_diff_solution(nums: Vec<i32>) -> i32 {
    let biggest = *nums.iter().max().unwrap();
    let smallest = *nums.iter().min().unwrap();

    (biggest - smallest).abs().try_into().expect("How did this even happen?")
}

pub fn centered_average_solution(nums: Vec<i32>) -> f64 {
    let mut nums = nums;
    let len = nums.len();
    nums.sort();

    let values = nums.iter().skip(1).take(len - 2).map(|e| *e).collect::<Vec<_>>();

    (values.iter().sum::<i32>() as f64) / (values.len() as f64)
}

pub fn sum13_solution(nums: Vec<i32>) -> i32 {
    let len = nums.len();
    let mut value = 0;
    let mut skip = false;

    for i in 0..len {
        if nums[i] == 13 {
            skip = true;
            continue;
        } else {
            if !skip {
                value += nums[i];
            } else {
                skip = false;
            }
        }
    }

    value
}

pub fn sum67_solution(nums: Vec<i32>) -> i32 {
    let len = nums.len();
    let mut value = 0;
    let mut skip = false;

    for i in 0..len {
        if nums[i] == 6 {
            skip = true;
            continue;
        } else if !skip {
            value += nums[i];
        } else if nums[i] == 7 {
            skip = false;
        }
    }

    value
}

pub fn has22_solution(nums: Vec<i32>) -> bool {
    let len = nums.len();

    if len > 0 {
        for i in 0..len - 1 {
            if nums[i] == 2 && nums[i + 1] == 2 {
                return true;
            }
        }
    }

    false
}

pub fn lucky13_solution(nums: Vec<i32>) -> bool {
    nums.iter().all(|&e| e != 1 && e != 3)
}

pub fn sum28_solution(nums: Vec<i32>) -> bool {
    nums.iter().filter(|&e| *e == 2).sum::<i32>() == 8
}

pub fn more14_solution(nums: Vec<i32>) -> bool {
    nums.iter().filter(|&e| *e == 1).count() > nums.iter().filter(|&e| *e == 4).count()
}

pub fn prepend_sum_solution(nums: Vec<i32>) -> Vec<i32> {
    let deref = |e: &i32| *e;
    let front = nums.iter().take(2).map(deref).collect::<Vec<_>>();
    let mut result = vec![front.iter().sum::<i32>()];

    if nums.len() > front.len() {
        let front_len = front.len();
        let nums_len = nums.len();
        let back = nums.iter().skip(front_len).take(nums_len).map(deref).collect::<Vec<_>>();
        result.extend(back);
    }

    result
}

pub fn fizz_array_solution(n: i32) -> Vec<i32> {
    (0..n).collect::<Vec<_>>()
}

pub fn only14_solution(nums: Vec<i32>) -> bool {
    nums.iter().all(|&e| e == 1 || e == 4)
}

pub fn fizz_array2_solution(n: i32) -> Vec<String> {
    (0..n).map(|e| e.to_string()).collect::<Vec<_>>()
}

pub fn no14_solution(nums: Vec<i32>) -> bool {
    let has_ones = nums.iter().any(|&e| e == 1);
    let has_fours = nums.iter().any(|&e| e == 4);

    !(has_ones && has_fours) || (has_ones ^ has_fours)
}

pub fn is_everywhere_solution(nums: Vec<i32>, val: i32) -> bool {
    let val_present = |e: &[i32]| e.iter().any(|&e| e == val);
    let len = nums.len();

    if len > 0 {
        for i in 0..len - 1 {
            if !val_present(&nums[i..i + 2]) {
                return false;
            }
        }
    }

    true
}

pub fn either24_solution(nums: Vec<i32>) -> bool {
    let len = nums.len();
    let mut a_two = false;
    let mut a_four = false;

    if len > 0 {
        for i in 0..len - 1 {
            if !a_two && nums[i] == 2 && nums[i + 1] == 2 {
                a_two = true;
            }
            if !a_four && nums[i] == 4 && nums[i + 1] == 4 {
                a_four = true;
            }
        }
    }

    a_two ^ a_four
}

pub fn match_up_solution(a: Vec<i32>, b: Vec<i32>) -> i32 {
    let get_delta = |a: i32, b: i32| (a - b).abs();
    let test = |a: i32, b: i32| {
        let delta = get_delta(a, b);

        if delta > 0 && delta <= 2 {
            Some(true)
        } else {
            None
        }
    };
    let len = min(a.len(), b.len());

    (0..len).filter_map(|e| test(a[e], b[e])).count().try_into().expect("How did we get here?")
}

pub fn has77_solution(nums: Vec<i32>) -> bool {
    for view in nums.windows(2) {
        if view[0] == 7 && view[1] == 7 {
            return true;
        }
    }

    for view in nums.windows(3) {
        if view[0] == 7 && view[2] == 7 {
            return true;
        }
    }

    false
}

pub fn has12_solution(nums: Vec<i32>) -> bool {
    let one = get_indicies_of_value_in(&nums, 1);
    let two = get_indicies_of_value_in(&nums, 2);

    let one_spot = one.first();
    let two_spot = two.last();

    match (one_spot, two_spot) {
        (Some(x), Some(y)) => x < y,
        _ => false,
    }
}

pub fn mod_three_solution(nums: Vec<i32>) -> bool {
    let test = |v: i32, r: i32| r == (v % 2);
    for view in nums.windows(3) {
        let all_odd = view.iter().all(|e| test(*e, 1));
        let all_even = view.iter().all(|e| test(*e, 0));

        if all_odd || all_even {
            return true;
        }
    }

    false
}

pub fn find_the_median_solution(nums: Vec<i32>) -> f64 {
    let len = nums.len();

    if 0 == len % 2 {
        nums.iter().skip((len / 2) - 1).take(2).sum::<i32>() as f64 / 2.0
    } else {
        nums[len / 2] as f64
    }
}

pub fn have_three_solution(nums: Vec<i32>) -> bool {
    let threes = get_indicies_of_value_in(&nums, 3);
    let right_quantity = threes.len() == 3;
    let are_spread = threes.windows(2).all(|e| (e[1] - e[0]) > 1);

    right_quantity && are_spread
}

pub fn two_two_solution(nums: Vec<i32>) -> bool {
    let twos = get_indicies_of_value_in(&nums, 2);
    let quantity = twos.len();

    if quantity != 1 {
        for view in twos.windows(2) {
            let delta = view[1] - view[0];
            if delta != 1 {
                return false;
            }
        }
        true
    } else {
        false
    }
}

pub fn same_ends_solution(nums: Vec<i32>, len: usize) -> bool {
    let n_len = nums.len();
    let b_skip = n_len - len;
    let front = nums.iter().take(len).collect::<Vec<_>>();
    let back = nums.iter().skip(b_skip).take(len).collect::<Vec<_>>();

    front == back
}

pub fn triple_up_solution(nums: Vec<i32>) -> bool {
    nums.windows(3).any(|e| (e[2] - e[1]) == 1 && (e[1] - e[0]) == 1)
}

pub fn fizz_array3_solution(start: i32, end: i32) -> Vec<i32> {
    (start..end).collect()
}

pub fn shift_left_solution(nums: Vec<i32>) -> Vec<i32> {
    if nums.len() > 0 {
        let mut nums = nums;
        nums.rotate_left(1);
        nums
    } else {
        nums
    }
}

pub fn ten_run_solution(nums: Vec<i32>) -> Vec<i32> {
    let mut output = Vec::<i32>::new();
    let mut replace = None;
    for value in nums {
        if 0 == (value % 10) {
            replace = Some(value);
        }
        if replace.is_some() {
            output.push(replace.unwrap());
        } else {
            output.push(value);
        }
    }
    output
}

pub fn pre4_solution(nums: Vec<i32>) -> Vec<i32> {
    let index = nums.iter().position(|&e| e == 4).unwrap_or(0);
    nums.iter().take(index).map(|e| *e).collect()
}

pub fn post4_solution(nums: Vec<i32>) -> Vec<i32> {
    let index = nums.iter().positions(|&e| e == 4).last().unwrap_or(0) + 1;
    nums.iter().skip(index).map(|e| *e).collect()
}

pub fn not_alone_solution(nums: Vec<i32>, val: i32) -> Vec<i32> {
    let len = nums.len();
    let mut result = Vec::<i32>::new();
    if len > 0 {
        result.push(nums[0]);

        for i in 1..len - 1 {
            let v = nums[i];
            let left = nums[i - 1];
            let right = nums[i + 1];

            if nums[i] == val && v != left && v != right {
                result.push(max(left, right));
            } else {
                result.push(nums[i]);
            }
        }

        if result.len() < len {
            result.push(nums[len - 1]);
        }
    }
    result
}

pub fn zero_front_solution(nums: Vec<i32>) -> Vec<i32> {
    let mut result = Vec::<i32>::new();
    let zeroes = nums.iter().filter(|&e| *e == 0).count();

    result.extend((0..zeroes).map(|_| 0));
    result.extend(nums.iter().filter(|&e| *e != 0));

    result
}

pub fn without_ten_solution(nums: Vec<i32>) -> Vec<i32> {
    let len = nums.len();
    let mut result = Vec::<i32>::new();
    // let mut result = vec![0; len];
    // let mut next = 0;

    // for v in nums.iter().filter(|&e| *e != 10) {
    //     result[next] = *v;
    //     next += 1;
    // }
    result.extend(nums.iter().filter(|&e| *e != 10));
    result.extend((0..len - result.len()).map(|_| 0));

    result
}

pub fn zero_max_solution(nums: Vec<i32>) -> Vec<i32> {
    let is_odd = |v: &&i32| 1 == **v % 2;
    let get_max_after_with_filter = |a: &Vec<i32>, start: usize, test: fn(v: &&i32) -> bool| {
        a.iter() // Iter
            .skip(start)
            .filter(test)
            .fold(0, |a, &e| max(a, e))
    };

    let zeroes = nums.iter().positions(|&e| e == 0).collect::<Vec<_>>();
    if !zeroes.is_empty() && nums.len() > 1 {
        let maxes = zeroes
            .iter() //Iter
            .map(|&e| get_max_after_with_filter(&nums, e, is_odd))
            .collect::<Vec<_>>();

        let mut results = nums.clone();
        for i in 0..zeroes.len() {
            let z = zeroes[i];
            let v = maxes[i];

            results[z] = v;
        }
        results
    } else {
        nums
    }
}

pub fn even_odd_solution(nums: Vec<i32>) -> Vec<i32> {
    let mut evens = Vec::<i32>::new();
    let mut odds = Vec::<i32>::new();

    for value in nums {
        match value % 2 {
            0 => evens.push(value),
            _ => odds.push(value),
        }
    }

    evens.extend(odds);
    evens
}

pub fn fizz_buzz_solution(start: i32, end: i32) -> Vec<String> {
    let is_divis = |v, d| 0 == v % d;
    let three = |v| is_divis(v, 3);
    let five = |v| is_divis(v, 5);
    (start..end)
        .map(|e| match (three(e), five(e)) {
            (true, false) => "Fizz".into(),
            (false, true) => "Buzz".into(),
            (true, true) => "FizzBuzz".into(),
            _ => e.to_string(),
        })
        .collect::<Vec<_>>()
}

pub fn max_span_solution(nums: Vec<i32>) -> i32 {
    nums.iter()
        .map(|&e| {
            nums.iter().rposition(|&v| v == e).unwrap_or(0) // Wrap 
                - nums.iter().position(|&v| v == e).unwrap_or(0)
                + 1
        })
        .fold(0, |a, e| max(a, e)) as i32
}

pub fn fix34_solution(nums: Vec<i32>) -> Vec<i32> {
    let mut fours = nums
        .iter() //Iter
        .positions(|&v| v == 4)
        .collect_vec();
    let mut result = nums.clone();

    for (i, &v) in nums.iter().enumerate() {
        let next = i + 1;
        if v == 3 && result[next] != 4 {
            if let Some(from) = fours.pop() {
                result[next] ^= result[from];
                result[from] ^= result[next];
                result[next] ^= result[from];
            }
        }
    }

    result
}

pub fn fix45_solution(nums: Vec<i32>) -> Vec<i32> {
    let mut fives = nums
        .iter() //Iter
        .positions(|&v| v == 5)
        .filter(|&e| if e == 0 { true } else { nums[e - 1] != 4 })
        .collect_vec();
    let mut result = nums.clone();

    for (i, &v) in nums.iter().enumerate() {
        let next = i + 1;
        if v == 4 && result[next] != 5 {
            if let Some(from) = fives.pop() {
                result[next] ^= result[from];
                result[from] ^= result[next];
                result[next] ^= result[from];
            }
        }
    }

    result
}

pub fn can_balance_solution(nums: Vec<i32>) -> bool {
    let len = nums.len();
    if len > 0 {
        for i in 1..len {
            let left: i32 = nums[0..i].iter().sum();
            let right: i32 = nums[i..len].iter().sum();
            if left == right {
                return true;
            }
        }
    }
    false
}

pub fn linear_in_solution(outer: Vec<i32>, inner: Vec<i32>) -> bool {
    let i_len = inner.len();
    let mut u = 0;

    if i_len > 0 {
        for v in 0..outer.len() {
            if (u < i_len) && (inner[u] == outer[v]) {
                u += 1;
            }
        }
    }

    u == i_len
}

pub fn square_up_solution(n: i32) -> Vec<i32> {
    let mut result = vec![];

    for v in 1..=(n as usize) {
        let mut array = vec![0; n as usize];

        for u in 1..=v {
            array[u - 1] = if u <= v { u as i32 } else { 0 };
        }
        result.extend(array.iter().rev().collect_vec());
    }

    result
}

pub fn series_up_solution(n: i32) -> Vec<i32> {
    (1..=n)
        .map(|e| {
            (1..=e) // Wrap
                .map(|v| v)
                .collect_vec()
        })
        .flatten()
        .map(|e| e)
        .collect_vec()
}

pub fn max_mirror_solution(nums: Vec<i32>) -> i32 {
    let a_rmap = |a: &[i32]| a.iter().rev().map(|&e| e).collect_vec();
    let a_fmap = |a: &[i32]| a.iter().map(|&e| e).collect_vec();
    let len = nums.len();

    if len == 0 {
        0
    } else if len > 1 {
        let mut found = 1;

        for v in 2..=len {
            let windows = nums.windows(v);
            for section in windows.clone().map(a_rmap) {
                for blocks in windows.clone().map(a_fmap) {
                    if section == blocks {
                        found = max(found, v);
                    }
                }
            }
        }

        found as i32
    } else {
        1
    }
}

pub fn count_clumps_solution(nums: Vec<i32>) -> i32 {
    let mut in_clump = false;
    let mut clump_count = 0;

    if nums.len() > 0 {
        let mut last_value = nums[0];
        for value in nums.iter().skip(1) {
            if (*value == last_value) && !in_clump {
                in_clump = true;
                clump_count += 1;
            } else if *value != last_value {
                in_clump = false;
                last_value = *value;
            }
        }
    }

    clump_count
}
