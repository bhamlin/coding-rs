pub mod implementations;
pub mod runners;
pub mod solutions;

#[cfg(test)]
mod test_scores_increasing {
    use crate::batvec;
    use crate::display::BatVec;

    use super::implementations::scores_increasing;
    use super::solutions::scores_increasing_solution;

    #[test]
    fn test_scores_increasing_1() {
        assert_eq!(
            scores_increasing(Into::<BatVec<_>>::into(batvec![1, 3, 4]).0),
            scores_increasing_solution(Into::<BatVec<_>>::into(batvec![1, 3, 4]).0)
        )
    }
    #[test]
    fn test_scores_increasing_2() {
        assert_eq!(
            scores_increasing(Into::<BatVec<_>>::into(batvec![1, 3, 2]).0),
            scores_increasing_solution(Into::<BatVec<_>>::into(batvec![1, 3, 2]).0)
        )
    }
    #[test]
    fn test_scores_increasing_3() {
        assert_eq!(
            scores_increasing(Into::<BatVec<_>>::into(batvec![1, 1, 4]).0),
            scores_increasing_solution(Into::<BatVec<_>>::into(batvec![1, 1, 4]).0)
        )
    }
    #[test]
    fn test_scores_increasing_4() {
        assert_eq!(
            scores_increasing(Into::<BatVec<_>>::into(batvec![1, 1, 2, 4, 4, 7]).0),
            scores_increasing_solution(Into::<BatVec<_>>::into(batvec![1, 1, 2, 4, 4, 7]).0)
        )
    }
    #[test]
    fn test_scores_increasing_5() {
        assert_eq!(
            scores_increasing(Into::<BatVec<_>>::into(batvec![1, 1, 2, 4, 3, 7]).0),
            scores_increasing_solution(Into::<BatVec<_>>::into(batvec![1, 1, 2, 4, 3, 7]).0)
        )
    }
}

#[cfg(test)]
mod test_scores100 {
    use crate::batvec;
    use crate::display::BatVec;

    use super::implementations::scores100;
    use super::solutions::scores100_solution;

    #[test]
    fn test_scores100_1() {
        assert_eq!(
            scores100(Into::<BatVec<_>>::into(batvec![1, 100, 100]).0),
            scores100_solution(Into::<BatVec<_>>::into(batvec![1, 100, 100]).0)
        )
    }
    #[test]
    fn test_scores100_2() {
        assert_eq!(
            scores100(Into::<BatVec<_>>::into(batvec![1, 100, 99, 100]).0),
            scores100_solution(Into::<BatVec<_>>::into(batvec![1, 100, 99, 100]).0)
        )
    }
    #[test]
    fn test_scores100_3() {
        assert_eq!(
            scores100(Into::<BatVec<_>>::into(batvec![100, 1, 100, 100]).0),
            scores100_solution(Into::<BatVec<_>>::into(batvec![100, 1, 100, 100]).0)
        )
    }
    #[test]
    fn test_scores100_4() {
        assert_eq!(
            scores100(Into::<BatVec<_>>::into(batvec![100, 1, 100, 1]).0),
            scores100_solution(Into::<BatVec<_>>::into(batvec![100, 1, 100, 1]).0)
        )
    }
    #[test]
    fn test_scores100_5() {
        assert_eq!(
            scores100(Into::<BatVec<_>>::into(batvec![1, 2, 3, 4, 5]).0),
            scores100_solution(Into::<BatVec<_>>::into(batvec![1, 2, 3, 4, 5]).0)
        )
    }
    #[test]
    fn test_scores100_6() {
        assert_eq!(
            scores100(Into::<BatVec<_>>::into(batvec![1, 2, 100, 4, 5]).0),
            scores100_solution(Into::<BatVec<_>>::into(batvec![1, 2, 100, 4, 5]).0)
        )
    }
}

#[cfg(test)]
mod test_scores_clump {
    use crate::batvec;
    use crate::display::BatVec;

    use super::implementations::scores_clump;
    use super::solutions::scores_clump_solution;

    #[test]
    fn test_scores_clump_1() {
        assert_eq!(
            scores_clump(Into::<BatVec<_>>::into(batvec![3, 4, 5]).0),
            scores_clump_solution(Into::<BatVec<_>>::into(batvec![3, 4, 5]).0)
        )
    }
    #[test]
    fn test_scores_clump_2() {
        assert_eq!(
            scores_clump(Into::<BatVec<_>>::into(batvec![3, 4, 6]).0),
            scores_clump_solution(Into::<BatVec<_>>::into(batvec![3, 4, 6]).0)
        )
    }
    #[test]
    fn test_scores_clump_3() {
        assert_eq!(
            scores_clump(Into::<BatVec<_>>::into(batvec![1, 3, 5, 5]).0),
            scores_clump_solution(Into::<BatVec<_>>::into(batvec![1, 3, 5, 5]).0)
        )
    }
    #[test]
    fn test_scores_clump_4() {
        assert_eq!(
            scores_clump(Into::<BatVec<_>>::into(batvec![2, 4, 5, 6]).0),
            scores_clump_solution(Into::<BatVec<_>>::into(batvec![2, 4, 5, 6]).0)
        )
    }
    #[test]
    fn test_scores_clump_5() {
        assert_eq!(
            scores_clump(Into::<BatVec<_>>::into(batvec![2, 4, 5, 7]).0),
            scores_clump_solution(Into::<BatVec<_>>::into(batvec![2, 4, 5, 7]).0)
        )
    }
    #[test]
    fn test_scores_clump_6() {
        assert_eq!(
            scores_clump(Into::<BatVec<_>>::into(batvec![2, 4, 4, 7]).0),
            scores_clump_solution(Into::<BatVec<_>>::into(batvec![2, 4, 4, 7]).0)
        )
    }
    #[test]
    fn test_scores_clump_7() {
        assert_eq!(
            scores_clump(Into::<BatVec<_>>::into(batvec![3, 3, 6, 7, 9]).0),
            scores_clump_solution(Into::<BatVec<_>>::into(batvec![3, 3, 6, 7, 9]).0)
        )
    }
    #[test]
    fn test_scores_clump_8() {
        assert_eq!(
            scores_clump(Into::<BatVec<_>>::into(batvec![3, 3, 7, 7, 9]).0),
            scores_clump_solution(Into::<BatVec<_>>::into(batvec![3, 3, 7, 7, 9]).0)
        )
    }
    #[test]
    fn test_scores_clump_9() {
        assert_eq!(
            scores_clump(Into::<BatVec<_>>::into(batvec![4, 5, 8]).0),
            scores_clump_solution(Into::<BatVec<_>>::into(batvec![4, 5, 8]).0)
        )
    }
}

#[cfg(test)]
mod test_scores_average {
    use crate::batvec;
    use crate::display::BatVec;

    use super::implementations::scores_average;
    use super::solutions::scores_average_solution;

    #[test]
    fn test_scores_average_1() {
        assert_eq!(
            scores_average(Into::<BatVec<_>>::into(batvec![2, 2, 4, 4]).0),
            scores_average_solution(Into::<BatVec<_>>::into(batvec![2, 2, 4, 4]).0)
        )
    }
    #[test]
    fn test_scores_average_2() {
        assert_eq!(
            scores_average(Into::<BatVec<_>>::into(batvec![4, 4, 4, 2, 2, 2]).0),
            scores_average_solution(Into::<BatVec<_>>::into(batvec![4, 4, 4, 2, 2, 2]).0)
        )
    }
    #[test]
    fn test_scores_average_3() {
        assert_eq!(
            scores_average(Into::<BatVec<_>>::into(batvec![3, 4, 5, 1, 2, 3]).0),
            scores_average_solution(Into::<BatVec<_>>::into(batvec![3, 4, 5, 1, 2, 3]).0)
        )
    }
    #[test]
    fn test_scores_average_4() {
        assert_eq!(
            scores_average(Into::<BatVec<_>>::into(batvec![5, 6]).0),
            scores_average_solution(Into::<BatVec<_>>::into(batvec![5, 6]).0)
        )
    }
    #[test]
    fn test_scores_average_5() {
        assert_eq!(
            scores_average(Into::<BatVec<_>>::into(batvec![5, 4]).0),
            scores_average_solution(Into::<BatVec<_>>::into(batvec![5, 4]).0)
        )
    }
    #[test]
    fn test_scores_average_6() {
        assert_eq!(
            scores_average(Into::<BatVec<_>>::into(batvec![5, 4, 5, 6, 2, 1, 2, 3]).0),
            scores_average_solution(Into::<BatVec<_>>::into(batvec![5, 4, 5, 6, 2, 1, 2, 3]).0)
        )
    }
}

#[cfg(test)]
mod test_words_count {
    use crate::batvec;
    use crate::display::BatVec;

    use super::implementations::words_count;
    use super::solutions::words_count_solution;

    #[test]
    fn test_words_count_1() {
        assert_eq!(
            words_count(Into::<BatVec<_>>::into(batvec!["a", "bb", "b", "ccc"]).0, 1),
            words_count_solution(Into::<BatVec<_>>::into(batvec!["a", "bb", "b", "ccc"]).0, 1)
        )
    }
    #[test]
    fn test_words_count_2() {
        assert_eq!(
            words_count(Into::<BatVec<_>>::into(batvec!["a", "bb", "b", "ccc"]).0, 3),
            words_count_solution(Into::<BatVec<_>>::into(batvec!["a", "bb", "b", "ccc"]).0, 3)
        )
    }
    #[test]
    fn test_words_count_3() {
        assert_eq!(
            words_count(Into::<BatVec<_>>::into(batvec!["a", "bb", "b", "ccc"]).0, 4),
            words_count_solution(Into::<BatVec<_>>::into(batvec!["a", "bb", "b", "ccc"]).0, 4)
        )
    }
    #[test]
    fn test_words_count_4() {
        assert_eq!(
            words_count(Into::<BatVec<_>>::into(batvec!["xx", "yyy", "x", "yy", "z"]).0, 1),
            words_count_solution(Into::<BatVec<_>>::into(batvec!["xx", "yyy", "x", "yy", "z"]).0, 1)
        )
    }
    #[test]
    fn test_words_count_5() {
        assert_eq!(
            words_count(Into::<BatVec<_>>::into(batvec!["xx", "yyy", "x", "yy", "z"]).0, 2),
            words_count_solution(Into::<BatVec<_>>::into(batvec!["xx", "yyy", "x", "yy", "z"]).0, 2)
        )
    }
}

#[cfg(test)]
mod test_words_front {
    use crate::batvec;
    use crate::display::BatVec;

    use super::implementations::words_front;
    use super::solutions::words_front_solution;

    #[test]
    fn test_words_front_1() {
        assert_eq!(
            words_front(Into::<BatVec<_>>::into(batvec!["a", "b", "c", "d"]).0, 1),
            words_front_solution(Into::<BatVec<_>>::into(batvec!["a", "b", "c", "d"]).0, 1)
        )
    }
    #[test]
    fn test_words_front_2() {
        assert_eq!(
            words_front(Into::<BatVec<_>>::into(batvec!["a", "b", "c", "d"]).0, 2),
            words_front_solution(Into::<BatVec<_>>::into(batvec!["a", "b", "c", "d"]).0, 2)
        )
    }
    #[test]
    fn test_words_front_3() {
        assert_eq!(
            words_front(Into::<BatVec<_>>::into(batvec!["a", "b", "c", "d"]).0, 3),
            words_front_solution(Into::<BatVec<_>>::into(batvec!["a", "b", "c", "d"]).0, 3)
        )
    }
    #[test]
    fn test_words_front_4() {
        assert_eq!(
            words_front(Into::<BatVec<_>>::into(batvec!["a", "b", "c", "d"]).0, 4),
            words_front_solution(Into::<BatVec<_>>::into(batvec!["a", "b", "c", "d"]).0, 4)
        )
    }
    #[test]
    fn test_words_front_5() {
        assert_eq!(
            words_front(Into::<BatVec<_>>::into(batvec!["Hi", "There"]).0, 1),
            words_front_solution(Into::<BatVec<_>>::into(batvec!["Hi", "There"]).0, 1)
        )
    }
}

#[cfg(test)]
mod test_words_without_list {
    use crate::batvec;
    use crate::display::BatVec;

    use super::implementations::words_without_list;
    use super::solutions::words_without_list_solution;

    #[test]
    fn test_words_without_list_1() {
        assert_eq!(
            words_without_list(Into::<BatVec<_>>::into(batvec!["a", "bb", "b", "ccc"]).0, 1),
            words_without_list_solution(Into::<BatVec<_>>::into(batvec!["a", "bb", "b", "ccc"]).0, 1)
        )
    }
    #[test]
    fn test_words_without_list_2() {
        assert_eq!(
            words_without_list(Into::<BatVec<_>>::into(batvec!["a", "bb", "b", "ccc"]).0, 3),
            words_without_list_solution(Into::<BatVec<_>>::into(batvec!["a", "bb", "b", "ccc"]).0, 3)
        )
    }
    #[test]
    fn test_words_without_list_3() {
        assert_eq!(
            words_without_list(Into::<BatVec<_>>::into(batvec!["a", "bb", "b", "ccc"]).0, 4),
            words_without_list_solution(Into::<BatVec<_>>::into(batvec!["a", "bb", "b", "ccc"]).0, 4)
        )
    }
    #[test]
    fn test_words_without_list_4() {
        assert_eq!(
            words_without_list(Into::<BatVec<_>>::into(batvec!["xx", "yyy", "x", "yy", "z"]).0, 1),
            words_without_list_solution(Into::<BatVec<_>>::into(batvec!["xx", "yyy", "x", "yy", "z"]).0, 1)
        )
    }
}

#[cfg(test)]
mod test_has_one {
    use super::implementations::has_one;
    use super::solutions::has_one_solution;

    #[test]
    fn test_has_one_1() {
        assert_eq!(has_one(10), has_one_solution(10))
    }
    #[test]
    fn test_has_one_2() {
        assert_eq!(has_one(22), has_one_solution(22))
    }
    #[test]
    fn test_has_one_3() {
        assert_eq!(has_one(220), has_one_solution(220))
    }
    #[test]
    fn test_has_one_4() {
        assert_eq!(has_one(212), has_one_solution(212))
    }
    #[test]
    fn test_has_one_5() {
        assert_eq!(has_one(1), has_one_solution(1))
    }
    #[test]
    fn test_has_one_6() {
        assert_eq!(has_one(9), has_one_solution(9))
    }
    #[test]
    fn test_has_one_7() {
        assert_eq!(has_one(211112), has_one_solution(211112))
    }
    #[test]
    fn test_has_one_8() {
        assert_eq!(has_one(121121), has_one_solution(121121))
    }
    #[test]
    fn test_has_one_9() {
        assert_eq!(has_one(222222), has_one_solution(222222))
    }
    #[test]
    fn test_has_one_10() {
        assert_eq!(has_one(56156), has_one_solution(56156))
    }
    #[test]
    fn test_has_one_11() {
        assert_eq!(has_one(56556), has_one_solution(56556))
    }
}

#[cfg(test)]
mod test_divides_self {
    use super::implementations::divides_self;
    use super::solutions::divides_self_solution;

    #[test]
    fn test_divides_self_1() {
        assert_eq!(divides_self(128), divides_self_solution(128))
    }
    #[test]
    fn test_divides_self_2() {
        assert_eq!(divides_self(12), divides_self_solution(12))
    }
    #[test]
    fn test_divides_self_3() {
        assert_eq!(divides_self(120), divides_self_solution(120))
    }
    #[test]
    fn test_divides_self_4() {
        assert_eq!(divides_self(122), divides_self_solution(122))
    }
    #[test]
    fn test_divides_self_5() {
        assert_eq!(divides_self(13), divides_self_solution(13))
    }
    #[test]
    fn test_divides_self_6() {
        assert_eq!(divides_self(32), divides_self_solution(32))
    }
    #[test]
    fn test_divides_self_7() {
        assert_eq!(divides_self(22), divides_self_solution(22))
    }
    #[test]
    fn test_divides_self_8() {
        assert_eq!(divides_self(42), divides_self_solution(42))
    }
    #[test]
    fn test_divides_self_9() {
        assert_eq!(divides_self(212), divides_self_solution(212))
    }
    #[test]
    fn test_divides_self_10() {
        assert_eq!(divides_self(213), divides_self_solution(213))
    }
    #[test]
    fn test_divides_self_11() {
        assert_eq!(divides_self(162), divides_self_solution(162))
    }
}

#[cfg(test)]
mod test_copy_evens {
    use crate::batvec;
    use crate::display::BatVec;

    use super::implementations::copy_evens;
    use super::solutions::copy_evens_solution;

    #[test]
    fn test_copy_evens_1() {
        assert_eq!(
            copy_evens(Into::<BatVec<_>>::into(batvec![3, 2, 4, 5, 8]).0, 2),
            copy_evens_solution(Into::<BatVec<_>>::into(batvec![3, 2, 4, 5, 8]).0, 2)
        )
    }
    #[test]
    fn test_copy_evens_2() {
        assert_eq!(
            copy_evens(Into::<BatVec<_>>::into(batvec![3, 2, 4, 5, 8]).0, 3),
            copy_evens_solution(Into::<BatVec<_>>::into(batvec![3, 2, 4, 5, 8]).0, 3)
        )
    }
    #[test]
    fn test_copy_evens_3() {
        assert_eq!(
            copy_evens(Into::<BatVec<_>>::into(batvec![6, 1, 2, 4, 5, 8]).0, 3),
            copy_evens_solution(Into::<BatVec<_>>::into(batvec![6, 1, 2, 4, 5, 8]).0, 3)
        )
    }
    #[test]
    fn test_copy_evens_4() {
        assert_eq!(
            copy_evens(Into::<BatVec<_>>::into(batvec![6, 1, 2, 4, 5, 8]).0, 4),
            copy_evens_solution(Into::<BatVec<_>>::into(batvec![6, 1, 2, 4, 5, 8]).0, 4)
        )
    }
    #[test]
    fn test_copy_evens_5() {
        assert_eq!(
            copy_evens(Into::<BatVec<_>>::into(batvec![3, 1, 4, 1, 5]).0, 1),
            copy_evens_solution(Into::<BatVec<_>>::into(batvec![3, 1, 4, 1, 5]).0, 1)
        )
    }
    #[test]
    fn test_copy_evens_6() {
        assert_eq!(copy_evens(Into::<BatVec<_>>::into(batvec![2]).0, 1), copy_evens_solution(Into::<BatVec<_>>::into(batvec![2]).0, 1))
    }
    #[test]
    fn test_copy_evens_7() {
        assert_eq!(
            copy_evens(Into::<BatVec<_>>::into(batvec![6, 2, 4, 8]).0, 2),
            copy_evens_solution(Into::<BatVec<_>>::into(batvec![6, 2, 4, 8]).0, 2)
        )
    }
    #[test]
    fn test_copy_evens_8() {
        assert_eq!(
            copy_evens(Into::<BatVec<_>>::into(batvec![6, 2, 4, 8]).0, 3),
            copy_evens_solution(Into::<BatVec<_>>::into(batvec![6, 2, 4, 8]).0, 3)
        )
    }
    #[test]
    fn test_copy_evens_9() {
        assert_eq!(
            copy_evens(Into::<BatVec<_>>::into(batvec![6, 2, 4, 8]).0, 4),
            copy_evens_solution(Into::<BatVec<_>>::into(batvec![6, 2, 4, 8]).0, 4)
        )
    }
    #[test]
    fn test_copy_evens_10() {
        assert_eq!(
            copy_evens(Into::<BatVec<_>>::into(batvec![1, 8, 4]).0, 1),
            copy_evens_solution(Into::<BatVec<_>>::into(batvec![1, 8, 4]).0, 1)
        )
    }
    #[test]
    fn test_copy_evens_11() {
        assert_eq!(
            copy_evens(Into::<BatVec<_>>::into(batvec![1, 8, 4]).0, 2),
            copy_evens_solution(Into::<BatVec<_>>::into(batvec![1, 8, 4]).0, 2)
        )
    }
    #[test]
    fn test_copy_evens_12() {
        assert_eq!(
            copy_evens(Into::<BatVec<_>>::into(batvec![2, 8, 4]).0, 2),
            copy_evens_solution(Into::<BatVec<_>>::into(batvec![2, 8, 4]).0, 2)
        )
    }
}

#[cfg(test)]
mod test_copy_endy {
    use crate::batvec;
    use crate::display::BatVec;

    use super::implementations::copy_endy;
    use super::solutions::copy_endy_solution;

    #[test]
    fn test_copy_endy_1() {
        assert_eq!(
            copy_endy(Into::<BatVec<_>>::into(batvec![9, 11, 90, 22, 6]).0, 2),
            copy_endy_solution(Into::<BatVec<_>>::into(batvec![9, 11, 90, 22, 6]).0, 2)
        )
    }
    #[test]
    fn test_copy_endy_2() {
        assert_eq!(
            copy_endy(Into::<BatVec<_>>::into(batvec![9, 11, 90, 22, 6]).0, 3),
            copy_endy_solution(Into::<BatVec<_>>::into(batvec![9, 11, 90, 22, 6]).0, 3)
        )
    }
    #[test]
    fn test_copy_endy_3() {
        assert_eq!(
            copy_endy(Into::<BatVec<_>>::into(batvec![12, 1, 1, 13, 0, 20]).0, 2),
            copy_endy_solution(Into::<BatVec<_>>::into(batvec![12, 1, 1, 13, 0, 20]).0, 2)
        )
    }
    #[test]
    fn test_copy_endy_4() {
        assert_eq!(
            copy_endy(Into::<BatVec<_>>::into(batvec![12, 1, 1, 13, 0, 20]).0, 3),
            copy_endy_solution(Into::<BatVec<_>>::into(batvec![12, 1, 1, 13, 0, 20]).0, 3)
        )
    }
    #[test]
    fn test_copy_endy_5() {
        assert_eq!(copy_endy(Into::<BatVec<_>>::into(batvec![0]).0, 1), copy_endy_solution(Into::<BatVec<_>>::into(batvec![0]).0, 1))
    }
    #[test]
    fn test_copy_endy_6() {
        assert_eq!(
            copy_endy(Into::<BatVec<_>>::into(batvec![10, 11, 90]).0, 2),
            copy_endy_solution(Into::<BatVec<_>>::into(batvec![10, 11, 90]).0, 2)
        )
    }
    #[test]
    fn test_copy_endy_7() {
        assert_eq!(
            copy_endy(Into::<BatVec<_>>::into(batvec![90, 22, 100]).0, 2),
            copy_endy_solution(Into::<BatVec<_>>::into(batvec![90, 22, 100]).0, 2)
        )
    }
    #[test]
    fn test_copy_endy_8() {
        assert_eq!(
            copy_endy(Into::<BatVec<_>>::into(batvec![12, 11, 10, 89, 101, 4]).0, 1),
            copy_endy_solution(Into::<BatVec<_>>::into(batvec![12, 11, 10, 89, 101, 4]).0, 1)
        )
    }
    #[test]
    fn test_copy_endy_9() {
        assert_eq!(
            copy_endy(Into::<BatVec<_>>::into(batvec![13, 2, 2, 0]).0, 2),
            copy_endy_solution(Into::<BatVec<_>>::into(batvec![13, 2, 2, 0]).0, 2)
        )
    }
    #[test]
    fn test_copy_endy_10() {
        assert_eq!(
            copy_endy(Into::<BatVec<_>>::into(batvec![13, 2, 2, 0]).0, 3),
            copy_endy_solution(Into::<BatVec<_>>::into(batvec![13, 2, 2, 0]).0, 3)
        )
    }
    #[test]
    fn test_copy_endy_11() {
        assert_eq!(
            copy_endy(Into::<BatVec<_>>::into(batvec![13, 2, 13, 2, 0, 30]).0, 2),
            copy_endy_solution(Into::<BatVec<_>>::into(batvec![13, 2, 13, 2, 0, 30]).0, 2)
        )
    }
    #[test]
    fn test_copy_endy_12() {
        assert_eq!(
            copy_endy(Into::<BatVec<_>>::into(batvec![13, 2, 13, 2, 0, 30]).0, 3),
            copy_endy_solution(Into::<BatVec<_>>::into(batvec![13, 2, 13, 2, 0, 30]).0, 3)
        )
    }
}

#[cfg(test)]
mod test_match_up {
    use crate::batvec;
    use crate::display::BatVec;

    use super::implementations::match_up;
    use super::solutions::match_up_solution;

    #[test]
    fn test_match_up_1() {
        assert_eq!(
            match_up(Into::<BatVec<_>>::into(batvec!["aa", "bb", "cc"]).0, Into::<BatVec<_>>::into(batvec!["aaa", "xx", "bb"]).0),
            match_up_solution(Into::<BatVec<_>>::into(batvec!["aa", "bb", "cc"]).0, Into::<BatVec<_>>::into(batvec!["aaa", "xx", "bb"]).0)
        )
    }
    #[test]
    fn test_match_up_2() {
        assert_eq!(
            match_up(Into::<BatVec<_>>::into(batvec!["aa", "bb", "cc"]).0, Into::<BatVec<_>>::into(batvec!["aaa", "b", "bb"]).0),
            match_up_solution(Into::<BatVec<_>>::into(batvec!["aa", "bb", "cc"]).0, Into::<BatVec<_>>::into(batvec!["aaa", "b", "bb"]).0)
        )
    }
    #[test]
    fn test_match_up_3() {
        assert_eq!(
            match_up(Into::<BatVec<_>>::into(batvec!["aa", "bb", "cc"]).0, Into::<BatVec<_>>::into(batvec!["", "", "ccc"]).0),
            match_up_solution(Into::<BatVec<_>>::into(batvec!["aa", "bb", "cc"]).0, Into::<BatVec<_>>::into(batvec!["", "", "ccc"]).0)
        )
    }
    #[test]
    fn test_match_up_4() {
        assert_eq!(
            match_up(Into::<BatVec<_>>::into(batvec!["", "", "ccc"]).0, Into::<BatVec<_>>::into(batvec!["aa", "bb", "cc"]).0),
            match_up_solution(Into::<BatVec<_>>::into(batvec!["", "", "ccc"]).0, Into::<BatVec<_>>::into(batvec!["aa", "bb", "cc"]).0)
        )
    }
    #[test]
    fn test_match_up_5() {
        assert_eq!(
            match_up(Into::<BatVec<_>>::into(batvec!["", "", ""]).0, Into::<BatVec<_>>::into(batvec!["", "bb", "cc"]).0),
            match_up_solution(Into::<BatVec<_>>::into(batvec!["", "", ""]).0, Into::<BatVec<_>>::into(batvec!["", "bb", "cc"]).0)
        )
    }
    #[test]
    fn test_match_up_6() {
        assert_eq!(
            match_up(Into::<BatVec<_>>::into(batvec!["aa", "bb", "cc"]).0, Into::<BatVec<_>>::into(batvec!["", "", ""]).0),
            match_up_solution(Into::<BatVec<_>>::into(batvec!["aa", "bb", "cc"]).0, Into::<BatVec<_>>::into(batvec!["", "", ""]).0)
        )
    }
    #[test]
    fn test_match_up_7() {
        assert_eq!(
            match_up(Into::<BatVec<_>>::into(batvec!["aa", "", "ccc"]).0, Into::<BatVec<_>>::into(batvec!["", "bb", "cc"]).0),
            match_up_solution(Into::<BatVec<_>>::into(batvec!["aa", "", "ccc"]).0, Into::<BatVec<_>>::into(batvec!["", "bb", "cc"]).0)
        )
    }
    #[test]
    fn test_match_up_8() {
        assert_eq!(
            match_up(Into::<BatVec<_>>::into(batvec!["x", "y", "z"]).0, Into::<BatVec<_>>::into(batvec!["y", "z", "x"]).0),
            match_up_solution(Into::<BatVec<_>>::into(batvec!["x", "y", "z"]).0, Into::<BatVec<_>>::into(batvec!["y", "z", "x"]).0)
        )
    }
    #[test]
    fn test_match_up_9() {
        assert_eq!(
            match_up(Into::<BatVec<_>>::into(batvec!["", "y", "z"]).0, Into::<BatVec<_>>::into(batvec!["", "y", "x"]).0),
            match_up_solution(Into::<BatVec<_>>::into(batvec!["", "y", "z"]).0, Into::<BatVec<_>>::into(batvec!["", "y", "x"]).0)
        )
    }
    #[test]
    fn test_match_up_10() {
        assert_eq!(
            match_up(Into::<BatVec<_>>::into(batvec!["x", "y", "z"]).0, Into::<BatVec<_>>::into(batvec!["xx", "yyy", "zzz"]).0),
            match_up_solution(Into::<BatVec<_>>::into(batvec!["x", "y", "z"]).0, Into::<BatVec<_>>::into(batvec!["xx", "yyy", "zzz"]).0)
        )
    }
    #[test]
    fn test_match_up_11() {
        assert_eq!(
            match_up(Into::<BatVec<_>>::into(batvec!["x", "y", "z"]).0, Into::<BatVec<_>>::into(batvec!["xx", "yyy", ""]).0),
            match_up_solution(Into::<BatVec<_>>::into(batvec!["x", "y", "z"]).0, Into::<BatVec<_>>::into(batvec!["xx", "yyy", ""]).0)
        )
    }
    #[test]
    fn test_match_up_12() {
        assert_eq!(
            match_up(Into::<BatVec<_>>::into(batvec!["b", "x", "y", "z"]).0, Into::<BatVec<_>>::into(batvec!["a", "xx", "yyy", "zzz"]).0),
            match_up_solution(Into::<BatVec<_>>::into(batvec!["b", "x", "y", "z"]).0, Into::<BatVec<_>>::into(batvec!["a", "xx", "yyy", "zzz"]).0)
        )
    }
    #[test]
    fn test_match_up_13() {
        assert_eq!(
            match_up(Into::<BatVec<_>>::into(batvec!["aaa", "bb", "c"]).0, Into::<BatVec<_>>::into(batvec!["aaa", "xx", "bb"]).0),
            match_up_solution(Into::<BatVec<_>>::into(batvec!["aaa", "bb", "c"]).0, Into::<BatVec<_>>::into(batvec!["aaa", "xx", "bb"]).0)
        )
    }
}

#[cfg(test)]
mod test_score_up {
    use crate::batvec;
    use crate::display::BatVec;

    use super::implementations::score_up;
    use super::solutions::score_up_solution;

    #[test]
    fn test_score_up_1() {
        assert_eq!(
            score_up(Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b"]).0, Into::<BatVec<_>>::into(batvec!["a", "c", "b", "c"]).0),
            score_up_solution(Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b"]).0, Into::<BatVec<_>>::into(batvec!["a", "c", "b", "c"]).0)
        )
    }
    #[test]
    fn test_score_up_2() {
        assert_eq!(
            score_up(Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b"]).0, Into::<BatVec<_>>::into(batvec!["a", "a", "b", "c"]).0),
            score_up_solution(Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b"]).0, Into::<BatVec<_>>::into(batvec!["a", "a", "b", "c"]).0)
        )
    }
    #[test]
    fn test_score_up_3() {
        assert_eq!(
            score_up(Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b"]).0, Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b"]).0),
            score_up_solution(Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b"]).0, Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b"]).0)
        )
    }
    #[test]
    fn test_score_up_4() {
        assert_eq!(
            score_up(Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b"]).0, Into::<BatVec<_>>::into(batvec!["?", "c", "b", "?"]).0),
            score_up_solution(Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b"]).0, Into::<BatVec<_>>::into(batvec!["?", "c", "b", "?"]).0)
        )
    }
    #[test]
    fn test_score_up_5() {
        assert_eq!(
            score_up(Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b"]).0, Into::<BatVec<_>>::into(batvec!["?", "c", "?", "?"]).0),
            score_up_solution(Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b"]).0, Into::<BatVec<_>>::into(batvec!["?", "c", "?", "?"]).0)
        )
    }
    #[test]
    fn test_score_up_6() {
        assert_eq!(
            score_up(Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b"]).0, Into::<BatVec<_>>::into(batvec!["c", "?", "b", "b"]).0),
            score_up_solution(Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b"]).0, Into::<BatVec<_>>::into(batvec!["c", "?", "b", "b"]).0)
        )
    }
    #[test]
    fn test_score_up_7() {
        assert_eq!(
            score_up(Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b"]).0, Into::<BatVec<_>>::into(batvec!["c", "?", "b", "?"]).0),
            score_up_solution(Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b"]).0, Into::<BatVec<_>>::into(batvec!["c", "?", "b", "?"]).0)
        )
    }
    #[test]
    fn test_score_up_8() {
        assert_eq!(
            score_up(Into::<BatVec<_>>::into(batvec!["a", "b", "c"]).0, Into::<BatVec<_>>::into(batvec!["a", "c", "b"]).0),
            score_up_solution(Into::<BatVec<_>>::into(batvec!["a", "b", "c"]).0, Into::<BatVec<_>>::into(batvec!["a", "c", "b"]).0)
        )
    }
    #[test]
    fn test_score_up_9() {
        assert_eq!(
            score_up(
                Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b", "c", "c"]).0,
                Into::<BatVec<_>>::into(batvec!["a", "c", "a", "c", "a", "c"]).0
            ),
            score_up_solution(
                Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b", "c", "c"]).0,
                Into::<BatVec<_>>::into(batvec!["a", "c", "a", "c", "a", "c"]).0
            )
        )
    }
    #[test]
    fn test_score_up_10() {
        assert_eq!(
            score_up(
                Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b", "c", "c"]).0,
                Into::<BatVec<_>>::into(batvec!["a", "c", "?", "?", "a", "c"]).0
            ),
            score_up_solution(
                Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b", "c", "c"]).0,
                Into::<BatVec<_>>::into(batvec!["a", "c", "?", "?", "a", "c"]).0
            )
        )
    }
    #[test]
    fn test_score_up_11() {
        assert_eq!(
            score_up(
                Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b", "c", "c"]).0,
                Into::<BatVec<_>>::into(batvec!["a", "c", "?", "?", "c", "c"]).0
            ),
            score_up_solution(
                Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b", "c", "c"]).0,
                Into::<BatVec<_>>::into(batvec!["a", "c", "?", "?", "c", "c"]).0
            )
        )
    }
    #[test]
    fn test_score_up_12() {
        assert_eq!(
            score_up(Into::<BatVec<_>>::into(batvec!["a", "b", "c"]).0, Into::<BatVec<_>>::into(batvec!["a", "b", "c"]).0),
            score_up_solution(Into::<BatVec<_>>::into(batvec!["a", "b", "c"]).0, Into::<BatVec<_>>::into(batvec!["a", "b", "c"]).0)
        )
    }
}

#[cfg(test)]
mod test_words_without {
    use crate::batvec;
    use crate::display::BatVec;

    use super::implementations::words_without;
    use super::solutions::words_without_solution;

    #[test]
    fn test_words_without_1() {
        assert_eq!(
            words_without(Into::<BatVec<_>>::into(batvec!["a", "b", "c", "a"]).0, "a"),
            words_without_solution(Into::<BatVec<_>>::into(batvec!["a", "b", "c", "a"]).0, "a")
        )
    }
    #[test]
    fn test_words_without_2() {
        assert_eq!(
            words_without(Into::<BatVec<_>>::into(batvec!["a", "b", "c", "a"]).0, "b"),
            words_without_solution(Into::<BatVec<_>>::into(batvec!["a", "b", "c", "a"]).0, "b")
        )
    }
    #[test]
    fn test_words_without_3() {
        assert_eq!(
            words_without(Into::<BatVec<_>>::into(batvec!["a", "b", "c", "a"]).0, "c"),
            words_without_solution(Into::<BatVec<_>>::into(batvec!["a", "b", "c", "a"]).0, "c")
        )
    }
    #[test]
    fn test_words_without_4() {
        assert_eq!(
            words_without(Into::<BatVec<_>>::into(batvec!["b", "c", "a", "a"]).0, "b"),
            words_without_solution(Into::<BatVec<_>>::into(batvec!["b", "c", "a", "a"]).0, "b")
        )
    }
    #[test]
    fn test_words_without_5() {
        assert_eq!(
            words_without(Into::<BatVec<_>>::into(batvec!["xx", "yyy", "x", "yy", "x"]).0, "x"),
            words_without_solution(Into::<BatVec<_>>::into(batvec!["xx", "yyy", "x", "yy", "x"]).0, "x")
        )
    }
    #[test]
    fn test_words_without_6() {
        assert_eq!(
            words_without(Into::<BatVec<_>>::into(batvec!["xx", "yyy", "x", "yy", "x"]).0, "yy"),
            words_without_solution(Into::<BatVec<_>>::into(batvec!["xx", "yyy", "x", "yy", "x"]).0, "yy")
        )
    }
    #[test]
    fn test_words_without_7() {
        assert_eq!(
            words_without(Into::<BatVec<_>>::into(batvec!["aa", "ab", "ac", "aa"]).0, "aa"),
            words_without_solution(Into::<BatVec<_>>::into(batvec!["aa", "ab", "ac", "aa"]).0, "aa")
        )
    }
}

#[cfg(test)]
mod test_scores_special {
    use crate::batvec;
    use crate::display::BatVec;

    use super::implementations::scores_special;
    use super::solutions::scores_special_solution;

    #[test]
    fn test_scores_special_1() {
        assert_eq!(
            scores_special(Into::<BatVec<_>>::into(batvec![12, 10, 4]).0, Into::<BatVec<_>>::into(batvec![2, 20, 30]).0),
            scores_special_solution(Into::<BatVec<_>>::into(batvec![12, 10, 4]).0, Into::<BatVec<_>>::into(batvec![2, 20, 30]).0)
        )
    }
    #[test]
    fn test_scores_special_2() {
        assert_eq!(
            scores_special(Into::<BatVec<_>>::into(batvec![20, 10, 4]).0, Into::<BatVec<_>>::into(batvec![2, 20, 10]).0),
            scores_special_solution(Into::<BatVec<_>>::into(batvec![20, 10, 4]).0, Into::<BatVec<_>>::into(batvec![2, 20, 10]).0)
        )
    }
    #[test]
    fn test_scores_special_3() {
        assert_eq!(
            scores_special(Into::<BatVec<_>>::into(batvec![12, 11, 4]).0, Into::<BatVec<_>>::into(batvec![2, 20, 31]).0),
            scores_special_solution(Into::<BatVec<_>>::into(batvec![12, 11, 4]).0, Into::<BatVec<_>>::into(batvec![2, 20, 31]).0)
        )
    }
    #[test]
    fn test_scores_special_4() {
        assert_eq!(
            scores_special(Into::<BatVec<_>>::into(batvec![1, 20, 2, 50]).0, Into::<BatVec<_>>::into(batvec![3, 4, 5]).0),
            scores_special_solution(Into::<BatVec<_>>::into(batvec![1, 20, 2, 50]).0, Into::<BatVec<_>>::into(batvec![3, 4, 5]).0)
        )
    }
    #[test]
    fn test_scores_special_5() {
        assert_eq!(
            scores_special(Into::<BatVec<_>>::into(batvec![3, 4, 5]).0, Into::<BatVec<_>>::into(batvec![1, 50, 2, 20]).0),
            scores_special_solution(Into::<BatVec<_>>::into(batvec![3, 4, 5]).0, Into::<BatVec<_>>::into(batvec![1, 50, 2, 20]).0)
        )
    }
    #[test]
    fn test_scores_special_6() {
        assert_eq!(
            scores_special(Into::<BatVec<_>>::into(batvec![10, 4, 20, 30]).0, Into::<BatVec<_>>::into(batvec![20]).0),
            scores_special_solution(Into::<BatVec<_>>::into(batvec![10, 4, 20, 30]).0, Into::<BatVec<_>>::into(batvec![20]).0)
        )
    }
    #[test]
    fn test_scores_special_7() {
        assert_eq!(
            scores_special(Into::<BatVec<_>>::into(batvec![10, 4, 20, 30]).0, Into::<BatVec<_>>::into(batvec![20]).0),
            scores_special_solution(Into::<BatVec<_>>::into(batvec![10, 4, 20, 30]).0, Into::<BatVec<_>>::into(batvec![20]).0)
        )
    }
    #[test]
    fn test_scores_special_8() {
        assert_eq!(
            scores_special(Into::<BatVec<_>>::into(batvec![10, 4, 20, 30]).0, Into::<BatVec<_>>::into(batvec![3, 20, 99]).0),
            scores_special_solution(Into::<BatVec<_>>::into(batvec![10, 4, 20, 30]).0, Into::<BatVec<_>>::into(batvec![3, 20, 99]).0)
        )
    }
    #[test]
    fn test_scores_special_9() {
        assert_eq!(
            scores_special(Into::<BatVec<_>>::into(batvec![10, 4, 20, 30]).0, Into::<BatVec<_>>::into(batvec![30, 20, 99]).0),
            scores_special_solution(Into::<BatVec<_>>::into(batvec![10, 4, 20, 30]).0, Into::<BatVec<_>>::into(batvec![30, 20, 99]).0)
        )
    }
    #[test]
    fn test_scores_special_10() {
        assert_eq!(
            scores_special(Into::<BatVec<_>>::into(batvec![]).0, Into::<BatVec<_>>::into(batvec![2]).0),
            scores_special_solution(Into::<BatVec<_>>::into(batvec![]).0, Into::<BatVec<_>>::into(batvec![2]).0)
        )
    }
    #[test]
    fn test_scores_special_11() {
        assert_eq!(
            scores_special(Into::<BatVec<_>>::into(batvec![]).0, Into::<BatVec<_>>::into(batvec![20]).0),
            scores_special_solution(Into::<BatVec<_>>::into(batvec![]).0, Into::<BatVec<_>>::into(batvec![20]).0)
        )
    }
    #[test]
    fn test_scores_special_12() {
        assert_eq!(
            scores_special(Into::<BatVec<_>>::into(batvec![14, 10, 4]).0, Into::<BatVec<_>>::into(batvec![4, 20, 30]).0),
            scores_special_solution(Into::<BatVec<_>>::into(batvec![14, 10, 4]).0, Into::<BatVec<_>>::into(batvec![4, 20, 30]).0)
        )
    }
}

#[cfg(test)]
mod test_sum_heights {
    use crate::batvec;
    use crate::display::BatVec;

    use super::implementations::sum_heights;
    use super::solutions::sum_heights_solution;

    #[test]
    fn test_sum_heights_1() {
        assert_eq!(
            sum_heights(Into::<BatVec<_>>::into(batvec![5, 3, 6, 7, 2]).0, 2, 4),
            sum_heights_solution(Into::<BatVec<_>>::into(batvec![5, 3, 6, 7, 2]).0, 2, 4)
        )
    }
    #[test]
    fn test_sum_heights_2() {
        assert_eq!(
            sum_heights(Into::<BatVec<_>>::into(batvec![5, 3, 6, 7, 2]).0, 0, 1),
            sum_heights_solution(Into::<BatVec<_>>::into(batvec![5, 3, 6, 7, 2]).0, 0, 1)
        )
    }
    #[test]
    fn test_sum_heights_3() {
        assert_eq!(
            sum_heights(Into::<BatVec<_>>::into(batvec![5, 3, 6, 7, 2]).0, 0, 4),
            sum_heights_solution(Into::<BatVec<_>>::into(batvec![5, 3, 6, 7, 2]).0, 0, 4)
        )
    }
    #[test]
    fn test_sum_heights_4() {
        assert_eq!(
            sum_heights(Into::<BatVec<_>>::into(batvec![5, 3, 6, 7, 2]).0, 1, 1),
            sum_heights_solution(Into::<BatVec<_>>::into(batvec![5, 3, 6, 7, 2]).0, 1, 1)
        )
    }
    #[test]
    fn test_sum_heights_5() {
        assert_eq!(
            sum_heights(Into::<BatVec<_>>::into(batvec![1, 2, 3, 4, 5, 4, 3, 2, 10]).0, 0, 3),
            sum_heights_solution(Into::<BatVec<_>>::into(batvec![1, 2, 3, 4, 5, 4, 3, 2, 10]).0, 0, 3)
        )
    }
    #[test]
    fn test_sum_heights_6() {
        assert_eq!(
            sum_heights(Into::<BatVec<_>>::into(batvec![1, 2, 3, 4, 5, 4, 3, 2, 10]).0, 4, 8),
            sum_heights_solution(Into::<BatVec<_>>::into(batvec![1, 2, 3, 4, 5, 4, 3, 2, 10]).0, 4, 8)
        )
    }
    #[test]
    fn test_sum_heights_7() {
        assert_eq!(
            sum_heights(Into::<BatVec<_>>::into(batvec![1, 2, 3, 4, 5, 4, 3, 2, 10]).0, 7, 8),
            sum_heights_solution(Into::<BatVec<_>>::into(batvec![1, 2, 3, 4, 5, 4, 3, 2, 10]).0, 7, 8)
        )
    }
    #[test]
    fn test_sum_heights_8() {
        assert_eq!(
            sum_heights(Into::<BatVec<_>>::into(batvec![1, 2, 3, 4, 5, 4, 3, 2, 10]).0, 8, 8),
            sum_heights_solution(Into::<BatVec<_>>::into(batvec![1, 2, 3, 4, 5, 4, 3, 2, 10]).0, 8, 8)
        )
    }
    #[test]
    fn test_sum_heights_9() {
        assert_eq!(
            sum_heights(Into::<BatVec<_>>::into(batvec![1, 2, 3, 4, 5, 4, 3, 2, 10]).0, 2, 2),
            sum_heights_solution(Into::<BatVec<_>>::into(batvec![1, 2, 3, 4, 5, 4, 3, 2, 10]).0, 2, 2)
        )
    }
    #[test]
    fn test_sum_heights_10() {
        assert_eq!(
            sum_heights(Into::<BatVec<_>>::into(batvec![1, 2, 3, 4, 5, 4, 3, 2, 10]).0, 3, 6),
            sum_heights_solution(Into::<BatVec<_>>::into(batvec![1, 2, 3, 4, 5, 4, 3, 2, 10]).0, 3, 6)
        )
    }
    #[test]
    fn test_sum_heights_11() {
        assert_eq!(
            sum_heights(Into::<BatVec<_>>::into(batvec![10, 8, 7, 7, 7, 6, 7]).0, 1, 4),
            sum_heights_solution(Into::<BatVec<_>>::into(batvec![10, 8, 7, 7, 7, 6, 7]).0, 1, 4)
        )
    }
    #[test]
    fn test_sum_heights_12() {
        assert_eq!(
            sum_heights(Into::<BatVec<_>>::into(batvec![10, 8, 7, 7, 7, 6, 7]).0, 1, 5),
            sum_heights_solution(Into::<BatVec<_>>::into(batvec![10, 8, 7, 7, 7, 6, 7]).0, 1, 5)
        )
    }
}

#[cfg(test)]
mod test_sum_heights2 {
    use crate::batvec;
    use crate::display::BatVec;

    use super::implementations::sum_heights2;
    use super::solutions::sum_heights2_solution;

    #[test]
    fn test_sum_heights2_1() {
        assert_eq!(
            sum_heights2(Into::<BatVec<_>>::into(batvec![5, 3, 6, 7, 2]).0, 2, 4),
            sum_heights2_solution(Into::<BatVec<_>>::into(batvec![5, 3, 6, 7, 2]).0, 2, 4)
        )
    }
    #[test]
    fn test_sum_heights2_2() {
        assert_eq!(
            sum_heights2(Into::<BatVec<_>>::into(batvec![5, 3, 6, 7, 2]).0, 0, 1),
            sum_heights2_solution(Into::<BatVec<_>>::into(batvec![5, 3, 6, 7, 2]).0, 0, 1)
        )
    }
    #[test]
    fn test_sum_heights2_3() {
        assert_eq!(
            sum_heights2(Into::<BatVec<_>>::into(batvec![5, 3, 6, 7, 2]).0, 0, 4),
            sum_heights2_solution(Into::<BatVec<_>>::into(batvec![5, 3, 6, 7, 2]).0, 0, 4)
        )
    }
    #[test]
    fn test_sum_heights2_4() {
        assert_eq!(
            sum_heights2(Into::<BatVec<_>>::into(batvec![5, 3, 6, 7, 2]).0, 1, 1),
            sum_heights2_solution(Into::<BatVec<_>>::into(batvec![5, 3, 6, 7, 2]).0, 1, 1)
        )
    }
    #[test]
    fn test_sum_heights2_5() {
        assert_eq!(
            sum_heights2(Into::<BatVec<_>>::into(batvec![1, 2, 3, 4, 5, 4, 3, 2, 10]).0, 0, 3),
            sum_heights2_solution(Into::<BatVec<_>>::into(batvec![1, 2, 3, 4, 5, 4, 3, 2, 10]).0, 0, 3)
        )
    }
    #[test]
    fn test_sum_heights2_6() {
        assert_eq!(
            sum_heights2(Into::<BatVec<_>>::into(batvec![1, 2, 3, 4, 5, 4, 3, 2, 10]).0, 4, 8),
            sum_heights2_solution(Into::<BatVec<_>>::into(batvec![1, 2, 3, 4, 5, 4, 3, 2, 10]).0, 4, 8)
        )
    }
    #[test]
    fn test_sum_heights2_7() {
        assert_eq!(
            sum_heights2(Into::<BatVec<_>>::into(batvec![1, 2, 3, 4, 5, 4, 3, 2, 10]).0, 7, 8),
            sum_heights2_solution(Into::<BatVec<_>>::into(batvec![1, 2, 3, 4, 5, 4, 3, 2, 10]).0, 7, 8)
        )
    }
    #[test]
    fn test_sum_heights2_8() {
        assert_eq!(
            sum_heights2(Into::<BatVec<_>>::into(batvec![1, 2, 3, 4, 5, 4, 3, 2, 10]).0, 8, 8),
            sum_heights2_solution(Into::<BatVec<_>>::into(batvec![1, 2, 3, 4, 5, 4, 3, 2, 10]).0, 8, 8)
        )
    }
    #[test]
    fn test_sum_heights2_9() {
        assert_eq!(
            sum_heights2(Into::<BatVec<_>>::into(batvec![1, 2, 3, 4, 5, 4, 3, 2, 10]).0, 2, 2),
            sum_heights2_solution(Into::<BatVec<_>>::into(batvec![1, 2, 3, 4, 5, 4, 3, 2, 10]).0, 2, 2)
        )
    }
    #[test]
    fn test_sum_heights2_10() {
        assert_eq!(
            sum_heights2(Into::<BatVec<_>>::into(batvec![1, 2, 3, 4, 5, 4, 3, 2, 10]).0, 3, 6),
            sum_heights2_solution(Into::<BatVec<_>>::into(batvec![1, 2, 3, 4, 5, 4, 3, 2, 10]).0, 3, 6)
        )
    }
    #[test]
    fn test_sum_heights2_11() {
        assert_eq!(
            sum_heights2(Into::<BatVec<_>>::into(batvec![10, 8, 7, 7, 7, 6, 7]).0, 1, 4),
            sum_heights2_solution(Into::<BatVec<_>>::into(batvec![10, 8, 7, 7, 7, 6, 7]).0, 1, 4)
        )
    }
    #[test]
    fn test_sum_heights2_12() {
        assert_eq!(
            sum_heights2(Into::<BatVec<_>>::into(batvec![10, 8, 7, 7, 7, 6, 7]).0, 1, 5),
            sum_heights2_solution(Into::<BatVec<_>>::into(batvec![10, 8, 7, 7, 7, 6, 7]).0, 1, 5)
        )
    }
}

#[cfg(test)]
mod test_big_heights {
    use crate::batvec;
    use crate::display::BatVec;

    use super::implementations::big_heights;
    use super::solutions::big_heights_solution;

    #[test]
    fn test_big_heights_1() {
        assert_eq!(
            big_heights(Into::<BatVec<_>>::into(batvec![5, 3, 6, 7, 2]).0, 2, 4),
            big_heights_solution(Into::<BatVec<_>>::into(batvec![5, 3, 6, 7, 2]).0, 2, 4)
        )
    }
    #[test]
    fn test_big_heights_2() {
        assert_eq!(
            big_heights(Into::<BatVec<_>>::into(batvec![5, 3, 6, 7, 2]).0, 0, 1),
            big_heights_solution(Into::<BatVec<_>>::into(batvec![5, 3, 6, 7, 2]).0, 0, 1)
        )
    }
    #[test]
    fn test_big_heights_3() {
        assert_eq!(
            big_heights(Into::<BatVec<_>>::into(batvec![5, 3, 6, 7, 2]).0, 0, 4),
            big_heights_solution(Into::<BatVec<_>>::into(batvec![5, 3, 6, 7, 2]).0, 0, 4)
        )
    }
    #[test]
    fn test_big_heights_4() {
        assert_eq!(
            big_heights(Into::<BatVec<_>>::into(batvec![5, 3, 6, 7, 3]).0, 0, 4),
            big_heights_solution(Into::<BatVec<_>>::into(batvec![5, 3, 6, 7, 3]).0, 0, 4)
        )
    }
    #[test]
    fn test_big_heights_5() {
        assert_eq!(
            big_heights(Into::<BatVec<_>>::into(batvec![5, 3, 6, 7, 2]).0, 1, 1),
            big_heights_solution(Into::<BatVec<_>>::into(batvec![5, 3, 6, 7, 2]).0, 1, 1)
        )
    }
    #[test]
    fn test_big_heights_6() {
        assert_eq!(
            big_heights(Into::<BatVec<_>>::into(batvec![5, 13, 6, 7, 2]).0, 1, 2),
            big_heights_solution(Into::<BatVec<_>>::into(batvec![5, 13, 6, 7, 2]).0, 1, 2)
        )
    }
    #[test]
    fn test_big_heights_7() {
        assert_eq!(
            big_heights(Into::<BatVec<_>>::into(batvec![5, 13, 6, 7, 2]).0, 0, 2),
            big_heights_solution(Into::<BatVec<_>>::into(batvec![5, 13, 6, 7, 2]).0, 0, 2)
        )
    }
    #[test]
    fn test_big_heights_8() {
        assert_eq!(
            big_heights(Into::<BatVec<_>>::into(batvec![5, 13, 6, 7, 2]).0, 1, 4),
            big_heights_solution(Into::<BatVec<_>>::into(batvec![5, 13, 6, 7, 2]).0, 1, 4)
        )
    }
    #[test]
    fn test_big_heights_9() {
        assert_eq!(
            big_heights(Into::<BatVec<_>>::into(batvec![5, 13, 6, 7, 2]).0, 0, 4),
            big_heights_solution(Into::<BatVec<_>>::into(batvec![5, 13, 6, 7, 2]).0, 0, 4)
        )
    }
    #[test]
    fn test_big_heights_10() {
        assert_eq!(
            big_heights(Into::<BatVec<_>>::into(batvec![5, 13, 6, 7, 2]).0, 0, 3),
            big_heights_solution(Into::<BatVec<_>>::into(batvec![5, 13, 6, 7, 2]).0, 0, 3)
        )
    }
    #[test]
    fn test_big_heights_11() {
        assert_eq!(
            big_heights(Into::<BatVec<_>>::into(batvec![1, 2, 3, 4, 5, 4, 3, 2, 10]).0, 0, 3),
            big_heights_solution(Into::<BatVec<_>>::into(batvec![1, 2, 3, 4, 5, 4, 3, 2, 10]).0, 0, 3)
        )
    }
    #[test]
    fn test_big_heights_12() {
        assert_eq!(
            big_heights(Into::<BatVec<_>>::into(batvec![1, 2, 3, 4, 5, 4, 3, 2, 10]).0, 4, 8),
            big_heights_solution(Into::<BatVec<_>>::into(batvec![1, 2, 3, 4, 5, 4, 3, 2, 10]).0, 4, 8)
        )
    }
    #[test]
    fn test_big_heights_13() {
        assert_eq!(
            big_heights(Into::<BatVec<_>>::into(batvec![1, 2, 3, 14, 5, 4, 3, 2, 10]).0, 0, 3),
            big_heights_solution(Into::<BatVec<_>>::into(batvec![1, 2, 3, 14, 5, 4, 3, 2, 10]).0, 0, 3)
        )
    }
    #[test]
    fn test_big_heights_14() {
        assert_eq!(
            big_heights(Into::<BatVec<_>>::into(batvec![1, 2, 3, 14, 5, 4, 3, 2, 10]).0, 7, 8),
            big_heights_solution(Into::<BatVec<_>>::into(batvec![1, 2, 3, 14, 5, 4, 3, 2, 10]).0, 7, 8)
        )
    }
    #[test]
    fn test_big_heights_15() {
        assert_eq!(
            big_heights(Into::<BatVec<_>>::into(batvec![1, 2, 3, 14, 5, 4, 3, 2, 10]).0, 3, 8),
            big_heights_solution(Into::<BatVec<_>>::into(batvec![1, 2, 3, 14, 5, 4, 3, 2, 10]).0, 3, 8)
        )
    }
    #[test]
    fn test_big_heights_16() {
        assert_eq!(
            big_heights(Into::<BatVec<_>>::into(batvec![1, 2, 3, 14, 5, 4, 3, 2, 10]).0, 2, 8),
            big_heights_solution(Into::<BatVec<_>>::into(batvec![1, 2, 3, 14, 5, 4, 3, 2, 10]).0, 2, 8)
        )
    }
}

#[cfg(test)]
mod test_user_compare {
    use super::implementations::user_compare;
    use super::implementations::User;
    use super::solutions::user_compare_solution;

    #[test]
    fn test_user_compare_1() {
        assert_eq!(user_compare(User::new("bb", 1), User::new("zz", 2)), user_compare_solution(User::new("bb", 1), User::new("zz", 2)))
    }
    #[test]
    fn test_user_compare_2() {
        assert_eq!(user_compare(User::new("bb", 1), User::new("aa", 2)), user_compare_solution(User::new("bb", 1), User::new("aa", 2)))
    }
    #[test]
    fn test_user_compare_3() {
        assert_eq!(user_compare(User::new("bb", 1), User::new("bb", 1)), user_compare_solution(User::new("bb", 1), User::new("bb", 1)))
    }
    #[test]
    fn test_user_compare_4() {
        assert_eq!(user_compare(User::new("bb", 5), User::new("bb", 1)), user_compare_solution(User::new("bb", 5), User::new("bb", 1)))
    }
    #[test]
    fn test_user_compare_5() {
        assert_eq!(user_compare(User::new("bb", 5), User::new("bb", 10)), user_compare_solution(User::new("bb", 5), User::new("bb", 10)))
    }
    #[test]
    fn test_user_compare_6() {
        assert_eq!(
            user_compare(User::new("adam", 1), User::new("bob", 2)),
            user_compare_solution(User::new("adam", 1), User::new("bob", 2))
        )
    }
    #[test]
    fn test_user_compare_7() {
        assert_eq!(user_compare(User::new("bob", 1), User::new("bob", 2)), user_compare_solution(User::new("bob", 1), User::new("bob", 2)))
    }
    #[test]
    fn test_user_compare_8() {
        assert_eq!(user_compare(User::new("bzb", 1), User::new("bob", 2)), user_compare_solution(User::new("bzb", 1), User::new("bob", 2)))
    }
}

#[cfg(test)]
mod test_merge_two {
    use crate::batvec;
    use crate::display::BatVec;

    use super::implementations::merge_two;
    use super::solutions::merge_two_solution;

    #[test]
    fn test_merge_two_1() {
        assert_eq!(
            merge_two(Into::<BatVec<_>>::into(batvec!["a", "c", "z"]).0, Into::<BatVec<_>>::into(batvec!["b", "f", "z"]).0, 3),
            merge_two_solution(Into::<BatVec<_>>::into(batvec!["a", "c", "z"]).0, Into::<BatVec<_>>::into(batvec!["b", "f", "z"]).0, 3)
        )
    }
    #[test]
    fn test_merge_two_2() {
        assert_eq!(
            merge_two(Into::<BatVec<_>>::into(batvec!["a", "c", "z"]).0, Into::<BatVec<_>>::into(batvec!["c", "f", "z"]).0, 3),
            merge_two_solution(Into::<BatVec<_>>::into(batvec!["a", "c", "z"]).0, Into::<BatVec<_>>::into(batvec!["c", "f", "z"]).0, 3)
        )
    }
    #[test]
    fn test_merge_two_3() {
        assert_eq!(
            merge_two(Into::<BatVec<_>>::into(batvec!["f", "g", "z"]).0, Into::<BatVec<_>>::into(batvec!["c", "f", "g"]).0, 3),
            merge_two_solution(Into::<BatVec<_>>::into(batvec!["f", "g", "z"]).0, Into::<BatVec<_>>::into(batvec!["c", "f", "g"]).0, 3)
        )
    }
    #[test]
    fn test_merge_two_4() {
        assert_eq!(
            merge_two(Into::<BatVec<_>>::into(batvec!["a", "c", "z"]).0, Into::<BatVec<_>>::into(batvec!["a", "c", "z"]).0, 3),
            merge_two_solution(Into::<BatVec<_>>::into(batvec!["a", "c", "z"]).0, Into::<BatVec<_>>::into(batvec!["a", "c", "z"]).0, 3)
        )
    }
    #[test]
    fn test_merge_two_5() {
        assert_eq!(
            merge_two(Into::<BatVec<_>>::into(batvec!["a", "b", "c", "z"]).0, Into::<BatVec<_>>::into(batvec!["a", "c", "z"]).0, 3),
            merge_two_solution(Into::<BatVec<_>>::into(batvec!["a", "b", "c", "z"]).0, Into::<BatVec<_>>::into(batvec!["a", "c", "z"]).0, 3)
        )
    }
    #[test]
    fn test_merge_two_6() {
        assert_eq!(
            merge_two(Into::<BatVec<_>>::into(batvec!["a", "c", "z"]).0, Into::<BatVec<_>>::into(batvec!["a", "b", "c", "z"]).0, 3),
            merge_two_solution(Into::<BatVec<_>>::into(batvec!["a", "c", "z"]).0, Into::<BatVec<_>>::into(batvec!["a", "b", "c", "z"]).0, 3)
        )
    }
    #[test]
    fn test_merge_two_7() {
        assert_eq!(
            merge_two(Into::<BatVec<_>>::into(batvec!["a", "c", "z"]).0, Into::<BatVec<_>>::into(batvec!["a", "c", "z"]).0, 2),
            merge_two_solution(Into::<BatVec<_>>::into(batvec!["a", "c", "z"]).0, Into::<BatVec<_>>::into(batvec!["a", "c", "z"]).0, 2)
        )
    }
    #[test]
    fn test_merge_two_8() {
        assert_eq!(
            merge_two(Into::<BatVec<_>>::into(batvec!["a", "c", "z"]).0, Into::<BatVec<_>>::into(batvec!["a", "c", "y", "z"]).0, 3),
            merge_two_solution(Into::<BatVec<_>>::into(batvec!["a", "c", "z"]).0, Into::<BatVec<_>>::into(batvec!["a", "c", "y", "z"]).0, 3)
        )
    }
    #[test]
    fn test_merge_two_9() {
        assert_eq!(
            merge_two(Into::<BatVec<_>>::into(batvec!["x", "y", "z"]).0, Into::<BatVec<_>>::into(batvec!["a", "b", "z"]).0, 3),
            merge_two_solution(Into::<BatVec<_>>::into(batvec!["x", "y", "z"]).0, Into::<BatVec<_>>::into(batvec!["a", "b", "z"]).0, 3)
        )
    }
}

#[cfg(test)]
mod test_common_two {
    use crate::batvec;
    use crate::display::BatVec;

    use super::implementations::common_two;
    use super::solutions::common_two_solution;

    #[test]
    fn test_common_two_1() {
        assert_eq!(
            common_two(Into::<BatVec<_>>::into(batvec!["a", "c", "x"]).0, Into::<BatVec<_>>::into(batvec!["b", "c", "d", "x"]).0),
            common_two_solution(Into::<BatVec<_>>::into(batvec!["a", "c", "x"]).0, Into::<BatVec<_>>::into(batvec!["b", "c", "d", "x"]).0)
        )
    }
    #[test]
    fn test_common_two_2() {
        assert_eq!(
            common_two(Into::<BatVec<_>>::into(batvec!["a", "c", "x"]).0, Into::<BatVec<_>>::into(batvec!["a", "b", "c", "x", "z"]).0),
            common_two_solution(Into::<BatVec<_>>::into(batvec!["a", "c", "x"]).0, Into::<BatVec<_>>::into(batvec!["a", "b", "c", "x", "z"]).0)
        )
    }
    #[test]
    fn test_common_two_3() {
        assert_eq!(
            common_two(Into::<BatVec<_>>::into(batvec!["a", "b", "c"]).0, Into::<BatVec<_>>::into(batvec!["a", "b", "c"]).0),
            common_two_solution(Into::<BatVec<_>>::into(batvec!["a", "b", "c"]).0, Into::<BatVec<_>>::into(batvec!["a", "b", "c"]).0)
        )
    }
    #[test]
    fn test_common_two_4() {
        assert_eq!(
            common_two(Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b", "c"]).0, Into::<BatVec<_>>::into(batvec!["a", "b", "c"]).0),
            common_two_solution(Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b", "c"]).0, Into::<BatVec<_>>::into(batvec!["a", "b", "c"]).0)
        )
    }
    #[test]
    fn test_common_two_5() {
        assert_eq!(
            common_two(Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b", "c"]).0, Into::<BatVec<_>>::into(batvec!["a", "b", "b", "b", "c"]).0),
            common_two_solution(Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b", "c"]).0, Into::<BatVec<_>>::into(batvec!["a", "b", "b", "b", "c"]).0)
        )
    }
    #[test]
    fn test_common_two_6() {
        assert_eq!(
            common_two(Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b", "c"]).0, Into::<BatVec<_>>::into(batvec!["a", "b", "b", "c", "c"]).0),
            common_two_solution(Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b", "c"]).0, Into::<BatVec<_>>::into(batvec!["a", "b", "b", "c", "c"]).0)
        )
    }
    #[test]
    fn test_common_two_7() {
        assert_eq!(
            common_two(Into::<BatVec<_>>::into(batvec!["b", "b", "b", "b", "c"]).0, Into::<BatVec<_>>::into(batvec!["a", "b", "b", "b", "c"]).0),
            common_two_solution(Into::<BatVec<_>>::into(batvec!["b", "b", "b", "b", "c"]).0, Into::<BatVec<_>>::into(batvec!["a", "b", "b", "b", "c"]).0)
        )
    }
    #[test]
    fn test_common_two_8() {
        assert_eq!(
            common_two(
                Into::<BatVec<_>>::into(batvec!["a", "b", "c", "c", "d"]).0,
                Into::<BatVec<_>>::into(batvec!["a", "b", "b", "c", "d", "d"]).0
            ),
            common_two_solution(
                Into::<BatVec<_>>::into(batvec!["a", "b", "c", "c", "d"]).0,
                Into::<BatVec<_>>::into(batvec!["a", "b", "b", "c", "d", "d"]).0
            )
        )
    }
    #[test]
    fn test_common_two_9() {
        assert_eq!(
            common_two(Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b", "c"]).0, Into::<BatVec<_>>::into(batvec!["b", "b", "b"]).0),
            common_two_solution(Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b", "c"]).0, Into::<BatVec<_>>::into(batvec!["b", "b", "b"]).0)
        )
    }
    #[test]
    fn test_common_two_10() {
        assert_eq!(
            common_two(Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b", "c"]).0, Into::<BatVec<_>>::into(batvec!["c", "c"]).0),
            common_two_solution(Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b", "c"]).0, Into::<BatVec<_>>::into(batvec!["c", "c"]).0)
        )
    }
    #[test]
    fn test_common_two_11() {
        assert_eq!(
            common_two(Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b", "c"]).0, Into::<BatVec<_>>::into(batvec!["b", "b", "b", "x"]).0),
            common_two_solution(Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b", "c"]).0, Into::<BatVec<_>>::into(batvec!["b", "b", "b", "x"]).0)
        )
    }
    #[test]
    fn test_common_two_12() {
        assert_eq!(
            common_two(Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b", "c"]).0, Into::<BatVec<_>>::into(batvec!["b", "b"]).0),
            common_two_solution(Into::<BatVec<_>>::into(batvec!["a", "a", "b", "b", "c"]).0, Into::<BatVec<_>>::into(batvec!["b", "b"]).0)
        )
    }
    #[test]
    fn test_common_two_13() {
        assert_eq!(
            common_two(Into::<BatVec<_>>::into(batvec!["a"]).0, Into::<BatVec<_>>::into(batvec!["a", "b"]).0),
            common_two_solution(Into::<BatVec<_>>::into(batvec!["a"]).0, Into::<BatVec<_>>::into(batvec!["a", "b"]).0)
        )
    }
    #[test]
    fn test_common_two_14() {
        assert_eq!(
            common_two(Into::<BatVec<_>>::into(batvec!["a"]).0, Into::<BatVec<_>>::into(batvec!["b"]).0),
            common_two_solution(Into::<BatVec<_>>::into(batvec!["a"]).0, Into::<BatVec<_>>::into(batvec!["b"]).0)
        )
    }
    #[test]
    fn test_common_two_15() {
        assert_eq!(
            common_two(Into::<BatVec<_>>::into(batvec!["a", "a"]).0, Into::<BatVec<_>>::into(batvec!["b", "b"]).0),
            common_two_solution(Into::<BatVec<_>>::into(batvec!["a", "a"]).0, Into::<BatVec<_>>::into(batvec!["b", "b"]).0)
        )
    }
    #[test]
    fn test_common_two_16() {
        assert_eq!(
            common_two(Into::<BatVec<_>>::into(batvec!["a", "b"]).0, Into::<BatVec<_>>::into(batvec!["a", "b"]).0),
            common_two_solution(Into::<BatVec<_>>::into(batvec!["a", "b"]).0, Into::<BatVec<_>>::into(batvec!["a", "b"]).0)
        )
    }
}
