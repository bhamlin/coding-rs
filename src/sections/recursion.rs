pub mod implementations;
pub mod runners;
pub mod solutions;

#[cfg(test)]
mod test_factorial {
    use super::implementations::factorial;
    use super::solutions::factorial_solution;

    #[test]
    fn test_factorial_1() {
        assert_eq!(factorial(1), factorial_solution(1))
    }
    #[test]
    fn test_factorial_2() {
        assert_eq!(factorial(2), factorial_solution(2))
    }
    #[test]
    fn test_factorial_3() {
        assert_eq!(factorial(3), factorial_solution(3))
    }
    #[test]
    fn test_factorial_4() {
        assert_eq!(factorial(4), factorial_solution(4))
    }
    #[test]
    fn test_factorial_5() {
        assert_eq!(factorial(5), factorial_solution(5))
    }
    #[test]
    fn test_factorial_6() {
        assert_eq!(factorial(6), factorial_solution(6))
    }
    #[test]
    fn test_factorial_7() {
        assert_eq!(factorial(7), factorial_solution(7))
    }
    #[test]
    fn test_factorial_8() {
        assert_eq!(factorial(8), factorial_solution(8))
    }
    #[test]
    fn test_factorial_9() {
        assert_eq!(factorial(12), factorial_solution(12))
    }
}

#[cfg(test)]
mod test_bunny_ears {
    use super::implementations::bunny_ears;
    use super::solutions::bunny_ears_solution;

    #[test]
    fn test_bunny_ears_1() {
        assert_eq!(bunny_ears(0), bunny_ears_solution(0))
    }
    #[test]
    fn test_bunny_ears_2() {
        assert_eq!(bunny_ears(1), bunny_ears_solution(1))
    }
    #[test]
    fn test_bunny_ears_3() {
        assert_eq!(bunny_ears(2), bunny_ears_solution(2))
    }
    #[test]
    fn test_bunny_ears_4() {
        assert_eq!(bunny_ears(3), bunny_ears_solution(3))
    }
    #[test]
    fn test_bunny_ears_5() {
        assert_eq!(bunny_ears(4), bunny_ears_solution(4))
    }
    #[test]
    fn test_bunny_ears_6() {
        assert_eq!(bunny_ears(5), bunny_ears_solution(5))
    }
    #[test]
    fn test_bunny_ears_7() {
        assert_eq!(bunny_ears(12), bunny_ears_solution(12))
    }
    #[test]
    fn test_bunny_ears_8() {
        assert_eq!(bunny_ears(50), bunny_ears_solution(50))
    }
    #[test]
    fn test_bunny_ears_9() {
        assert_eq!(bunny_ears(234), bunny_ears_solution(234))
    }
}

#[cfg(test)]
mod test_fibonacci {
    use super::implementations::fibonacci;
    use super::solutions::fibonacci_solution;

    #[test]
    fn test_fibonacci_1() {
        assert_eq!(fibonacci(0), fibonacci_solution(0))
    }
    #[test]
    fn test_fibonacci_2() {
        assert_eq!(fibonacci(1), fibonacci_solution(1))
    }
    #[test]
    fn test_fibonacci_3() {
        assert_eq!(fibonacci(2), fibonacci_solution(2))
    }
    #[test]
    fn test_fibonacci_4() {
        assert_eq!(fibonacci(3), fibonacci_solution(3))
    }
    #[test]
    fn test_fibonacci_5() {
        assert_eq!(fibonacci(4), fibonacci_solution(4))
    }
    #[test]
    fn test_fibonacci_6() {
        assert_eq!(fibonacci(5), fibonacci_solution(5))
    }
    #[test]
    fn test_fibonacci_7() {
        assert_eq!(fibonacci(6), fibonacci_solution(6))
    }
    #[test]
    fn test_fibonacci_8() {
        assert_eq!(fibonacci(7), fibonacci_solution(7))
    }
}

#[cfg(test)]
mod test_bunny_ears2 {
    use super::implementations::bunny_ears2;
    use super::solutions::bunny_ears2_solution;

    #[test]
    fn test_bunny_ears2_1() {
        assert_eq!(bunny_ears2(0), bunny_ears2_solution(0))
    }
    #[test]
    fn test_bunny_ears2_2() {
        assert_eq!(bunny_ears2(1), bunny_ears2_solution(1))
    }
    #[test]
    fn test_bunny_ears2_3() {
        assert_eq!(bunny_ears2(2), bunny_ears2_solution(2))
    }
    #[test]
    fn test_bunny_ears2_4() {
        assert_eq!(bunny_ears2(3), bunny_ears2_solution(3))
    }
    #[test]
    fn test_bunny_ears2_5() {
        assert_eq!(bunny_ears2(4), bunny_ears2_solution(4))
    }
    #[test]
    fn test_bunny_ears2_6() {
        assert_eq!(bunny_ears2(5), bunny_ears2_solution(5))
    }
    #[test]
    fn test_bunny_ears2_7() {
        assert_eq!(bunny_ears2(6), bunny_ears2_solution(6))
    }
    #[test]
    fn test_bunny_ears2_8() {
        assert_eq!(bunny_ears2(10), bunny_ears2_solution(10))
    }
}

#[cfg(test)]
mod test_triangle {
    use super::implementations::triangle;
    use super::solutions::triangle_solution;

    #[test]
    fn test_triangle_1() {
        assert_eq!(triangle(0), triangle_solution(0))
    }
    #[test]
    fn test_triangle_2() {
        assert_eq!(triangle(1), triangle_solution(1))
    }
    #[test]
    fn test_triangle_3() {
        assert_eq!(triangle(2), triangle_solution(2))
    }
    #[test]
    fn test_triangle_4() {
        assert_eq!(triangle(3), triangle_solution(3))
    }
    #[test]
    fn test_triangle_5() {
        assert_eq!(triangle(4), triangle_solution(4))
    }
    #[test]
    fn test_triangle_6() {
        assert_eq!(triangle(5), triangle_solution(5))
    }
    #[test]
    fn test_triangle_7() {
        assert_eq!(triangle(6), triangle_solution(6))
    }
    #[test]
    fn test_triangle_8() {
        assert_eq!(triangle(7), triangle_solution(7))
    }
}

#[cfg(test)]
mod test_sum_digits1 {
    use super::implementations::sum_digits1;
    use super::solutions::sum_digits1_solution;

    #[test]
    fn test_sum_digits1_1() {
        assert_eq!(sum_digits1(126), sum_digits1_solution(126))
    }
    #[test]
    fn test_sum_digits1_2() {
        assert_eq!(sum_digits1(49), sum_digits1_solution(49))
    }
    #[test]
    fn test_sum_digits1_3() {
        assert_eq!(sum_digits1(12), sum_digits1_solution(12))
    }
    #[test]
    fn test_sum_digits1_4() {
        assert_eq!(sum_digits1(10), sum_digits1_solution(10))
    }
    #[test]
    fn test_sum_digits1_5() {
        assert_eq!(sum_digits1(1), sum_digits1_solution(1))
    }
    #[test]
    fn test_sum_digits1_6() {
        assert_eq!(sum_digits1(0), sum_digits1_solution(0))
    }
    #[test]
    fn test_sum_digits1_7() {
        assert_eq!(sum_digits1(730), sum_digits1_solution(730))
    }
    #[test]
    fn test_sum_digits1_8() {
        assert_eq!(sum_digits1(1111), sum_digits1_solution(1111))
    }
    #[test]
    fn test_sum_digits1_9() {
        assert_eq!(sum_digits1(11111), sum_digits1_solution(11111))
    }
    #[test]
    fn test_sum_digits1_10() {
        assert_eq!(sum_digits1(10110), sum_digits1_solution(10110))
    }
    #[test]
    fn test_sum_digits1_11() {
        assert_eq!(sum_digits1(235), sum_digits1_solution(235))
    }
}

#[cfg(test)]
mod test_count7 {
    use super::implementations::count7;
    use super::solutions::count7_solution;

    #[test]
    fn test_count7_1() {
        assert_eq!(count7(717), count7_solution(717))
    }
    #[test]
    fn test_count7_2() {
        assert_eq!(count7(7), count7_solution(7))
    }
    #[test]
    fn test_count7_3() {
        assert_eq!(count7(123), count7_solution(123))
    }
    #[test]
    fn test_count7_4() {
        assert_eq!(count7(77), count7_solution(77))
    }
    #[test]
    fn test_count7_5() {
        assert_eq!(count7(7123), count7_solution(7123))
    }
    #[test]
    fn test_count7_6() {
        assert_eq!(count7(771237), count7_solution(771237))
    }
    #[test]
    fn test_count7_7() {
        assert_eq!(count7(771737), count7_solution(771737))
    }
    #[test]
    fn test_count7_8() {
        assert_eq!(count7(47571), count7_solution(47571))
    }
    #[test]
    fn test_count7_9() {
        assert_eq!(count7(777777), count7_solution(777777))
    }
    #[test]
    fn test_count7_10() {
        assert_eq!(count7(70701277), count7_solution(70701277))
    }
    #[test]
    fn test_count7_11() {
        assert_eq!(count7(777576197), count7_solution(777576197))
    }
    #[test]
    fn test_count7_12() {
        assert_eq!(count7(99999), count7_solution(99999))
    }
    #[test]
    fn test_count7_13() {
        assert_eq!(count7(99799), count7_solution(99799))
    }
}

#[cfg(test)]
mod test_count8 {
    use super::implementations::count8;
    use super::solutions::count8_solution;

    #[test]
    fn test_count8_1() {
        assert_eq!(count8(8), count8_solution(8))
    }
    #[test]
    fn test_count8_2() {
        assert_eq!(count8(818), count8_solution(818))
    }
    #[test]
    fn test_count8_3() {
        assert_eq!(count8(8818), count8_solution(8818))
    }
    #[test]
    fn test_count8_4() {
        assert_eq!(count8(8088), count8_solution(8088))
    }
    #[test]
    fn test_count8_5() {
        assert_eq!(count8(123), count8_solution(123))
    }
    #[test]
    fn test_count8_6() {
        assert_eq!(count8(81238), count8_solution(81238))
    }
    #[test]
    fn test_count8_7() {
        assert_eq!(count8(88788), count8_solution(88788))
    }
    #[test]
    fn test_count8_8() {
        assert_eq!(count8(8234), count8_solution(8234))
    }
    #[test]
    fn test_count8_9() {
        assert_eq!(count8(2348), count8_solution(2348))
    }
    #[test]
    fn test_count8_10() {
        assert_eq!(count8(23884), count8_solution(23884))
    }
    #[test]
    fn test_count8_11() {
        assert_eq!(count8(0), count8_solution(0))
    }
    #[test]
    fn test_count8_12() {
        assert_eq!(count8(1818188), count8_solution(1818188))
    }
    #[test]
    fn test_count8_13() {
        assert_eq!(count8(8818181), count8_solution(8818181))
    }
    #[test]
    fn test_count8_14() {
        assert_eq!(count8(1080), count8_solution(1080))
    }
    #[test]
    fn test_count8_15() {
        assert_eq!(count8(188), count8_solution(188))
    }
    #[test]
    fn test_count8_16() {
        assert_eq!(count8(88888), count8_solution(88888))
    }
    #[test]
    fn test_count8_17() {
        assert_eq!(count8(9898), count8_solution(9898))
    }
    #[test]
    fn test_count8_18() {
        assert_eq!(count8(78), count8_solution(78))
    }
}

#[cfg(test)]
mod test_power_n {
    use super::implementations::power_n;
    use super::solutions::power_n_solution;

    #[test]
    fn test_power_n_1() {
        assert_eq!(power_n(3, 1), power_n_solution(3, 1))
    }
    #[test]
    fn test_power_n_2() {
        assert_eq!(power_n(3, 2), power_n_solution(3, 2))
    }
    #[test]
    fn test_power_n_3() {
        assert_eq!(power_n(3, 3), power_n_solution(3, 3))
    }
    #[test]
    fn test_power_n_4() {
        assert_eq!(power_n(2, 1), power_n_solution(2, 1))
    }
    #[test]
    fn test_power_n_5() {
        assert_eq!(power_n(2, 2), power_n_solution(2, 2))
    }
    #[test]
    fn test_power_n_6() {
        assert_eq!(power_n(2, 3), power_n_solution(2, 3))
    }
    #[test]
    fn test_power_n_7() {
        assert_eq!(power_n(2, 4), power_n_solution(2, 4))
    }
    #[test]
    fn test_power_n_8() {
        assert_eq!(power_n(2, 5), power_n_solution(2, 5))
    }
    #[test]
    fn test_power_n_9() {
        assert_eq!(power_n(10, 1), power_n_solution(10, 1))
    }
    #[test]
    fn test_power_n_10() {
        assert_eq!(power_n(10, 2), power_n_solution(10, 2))
    }
    #[test]
    fn test_power_n_11() {
        assert_eq!(power_n(10, 3), power_n_solution(10, 3))
    }
}

#[cfg(test)]
mod test_count_x {
    use super::implementations::count_x;
    use super::solutions::count_x_solution;

    #[test]
    fn test_count_x_1() {
        assert_eq!(count_x("xxhixx"), count_x_solution("xxhixx"))
    }
    #[test]
    fn test_count_x_2() {
        assert_eq!(count_x("xhixhix"), count_x_solution("xhixhix"))
    }
    #[test]
    fn test_count_x_3() {
        assert_eq!(count_x("hi"), count_x_solution("hi"))
    }
    #[test]
    fn test_count_x_4() {
        assert_eq!(count_x("h"), count_x_solution("h"))
    }
    #[test]
    fn test_count_x_5() {
        assert_eq!(count_x("x"), count_x_solution("x"))
    }
    #[test]
    fn test_count_x_6() {
        assert_eq!(count_x(""), count_x_solution(""))
    }
    #[test]
    fn test_count_x_7() {
        assert_eq!(count_x("hihi"), count_x_solution("hihi"))
    }
    #[test]
    fn test_count_x_8() {
        assert_eq!(count_x("hiAAhi12hi"), count_x_solution("hiAAhi12hi"))
    }
}

#[cfg(test)]
mod test_count_hi {
    use super::implementations::count_hi;
    use super::solutions::count_hi_solution;

    #[test]
    fn test_count_hi_1() {
        assert_eq!(count_hi("xxhixx"), count_hi_solution("xxhixx"))
    }
    #[test]
    fn test_count_hi_2() {
        assert_eq!(count_hi("xhixhix"), count_hi_solution("xhixhix"))
    }
    #[test]
    fn test_count_hi_3() {
        assert_eq!(count_hi("hi"), count_hi_solution("hi"))
    }
    #[test]
    fn test_count_hi_4() {
        assert_eq!(count_hi("hihih"), count_hi_solution("hihih"))
    }
    #[test]
    fn test_count_hi_5() {
        assert_eq!(count_hi("h"), count_hi_solution("h"))
    }
    #[test]
    fn test_count_hi_6() {
        assert_eq!(count_hi(""), count_hi_solution(""))
    }
    #[test]
    fn test_count_hi_7() {
        assert_eq!(count_hi("ihihihihih"), count_hi_solution("ihihihihih"))
    }
    #[test]
    fn test_count_hi_8() {
        assert_eq!(count_hi("ihihihihihi"), count_hi_solution("ihihihihihi"))
    }
    #[test]
    fn test_count_hi_9() {
        assert_eq!(count_hi("hiAAhi12hi"), count_hi_solution("hiAAhi12hi"))
    }
    #[test]
    fn test_count_hi_10() {
        assert_eq!(count_hi("xhixhxihihhhih"), count_hi_solution("xhixhxihihhhih"))
    }
    #[test]
    fn test_count_hi_11() {
        assert_eq!(count_hi("ship"), count_hi_solution("ship"))
    }
}

#[cfg(test)]
mod test_change_x_y {
    use super::implementations::change_x_y;
    use super::solutions::change_x_y_solution;

    #[test]
    fn test_change_x_y_1() {
        assert_eq!(change_x_y("codex"), change_x_y_solution("codex"))
    }
    #[test]
    fn test_change_x_y_2() {
        assert_eq!(change_x_y("xxhixx"), change_x_y_solution("xxhixx"))
    }
    #[test]
    fn test_change_x_y_3() {
        assert_eq!(change_x_y("xhixhix"), change_x_y_solution("xhixhix"))
    }
    #[test]
    fn test_change_x_y_4() {
        assert_eq!(change_x_y("hiy"), change_x_y_solution("hiy"))
    }
    #[test]
    fn test_change_x_y_5() {
        assert_eq!(change_x_y("h"), change_x_y_solution("h"))
    }
    #[test]
    fn test_change_x_y_6() {
        assert_eq!(change_x_y("x"), change_x_y_solution("x"))
    }
    #[test]
    fn test_change_x_y_7() {
        assert_eq!(change_x_y(""), change_x_y_solution(""))
    }
    #[test]
    fn test_change_x_y_8() {
        assert_eq!(change_x_y("xxx"), change_x_y_solution("xxx"))
    }
    #[test]
    fn test_change_x_y_9() {
        assert_eq!(change_x_y("yyhxyi"), change_x_y_solution("yyhxyi"))
    }
    #[test]
    fn test_change_x_y_10() {
        assert_eq!(change_x_y("hihi"), change_x_y_solution("hihi"))
    }
}

#[cfg(test)]
mod test_change_pi {
    use super::implementations::change_pi;
    use super::solutions::change_pi_solution;

    #[test]
    fn test_change_pi_1() {
        assert_eq!(change_pi("xpix"), change_pi_solution("xpix"))
    }
    #[test]
    fn test_change_pi_2() {
        assert_eq!(change_pi("pipi"), change_pi_solution("pipi"))
    }
    #[test]
    fn test_change_pi_3() {
        assert_eq!(change_pi("pip"), change_pi_solution("pip"))
    }
    #[test]
    fn test_change_pi_4() {
        assert_eq!(change_pi("pi"), change_pi_solution("pi"))
    }
    #[test]
    fn test_change_pi_5() {
        assert_eq!(change_pi("hip"), change_pi_solution("hip"))
    }
    #[test]
    fn test_change_pi_6() {
        assert_eq!(change_pi("p"), change_pi_solution("p"))
    }
    #[test]
    fn test_change_pi_7() {
        assert_eq!(change_pi("x"), change_pi_solution("x"))
    }
    #[test]
    fn test_change_pi_8() {
        assert_eq!(change_pi(""), change_pi_solution(""))
    }
    #[test]
    fn test_change_pi_9() {
        assert_eq!(change_pi("pixx"), change_pi_solution("pixx"))
    }
    #[test]
    fn test_change_pi_10() {
        assert_eq!(change_pi("xyzzy"), change_pi_solution("xyzzy"))
    }
}

#[cfg(test)]
mod test_no_x {
    use super::implementations::no_x;
    use super::solutions::no_x_solution;

    #[test]
    fn test_no_x_1() {
        assert_eq!(no_x("xaxb"), no_x_solution("xaxb"))
    }
    #[test]
    fn test_no_x_2() {
        assert_eq!(no_x("abc"), no_x_solution("abc"))
    }
    #[test]
    fn test_no_x_3() {
        assert_eq!(no_x("xx"), no_x_solution("xx"))
    }
    #[test]
    fn test_no_x_4() {
        assert_eq!(no_x(""), no_x_solution(""))
    }
    #[test]
    fn test_no_x_5() {
        assert_eq!(no_x("axxbxx"), no_x_solution("axxbxx"))
    }
    #[test]
    fn test_no_x_6() {
        assert_eq!(no_x("Hellox"), no_x_solution("Hellox"))
    }
}

#[cfg(test)]
mod test_array6 {
    use super::implementations::array6;
    use super::solutions::array6_solution;

    #[test]
    fn test_array6_1() {
        assert_eq!(array6(vec![1, 6, 4], 0), array6_solution(vec![1, 6, 4], 0))
    }
    #[test]
    fn test_array6_2() {
        assert_eq!(array6(vec![1, 4], 0), array6_solution(vec![1, 4], 0))
    }
    #[test]
    fn test_array6_3() {
        assert_eq!(array6(vec![6], 0), array6_solution(vec![6], 0))
    }
    #[test]
    fn test_array6_4() {
        assert_eq!(array6(vec![], 0), array6_solution(vec![], 0))
    }
    #[test]
    fn test_array6_5() {
        assert_eq!(array6(vec![6, 2, 2], 0), array6_solution(vec![6, 2, 2], 0))
    }
    #[test]
    fn test_array6_6() {
        assert_eq!(array6(vec![2, 5], 0), array6_solution(vec![2, 5], 0))
    }
    #[test]
    fn test_array6_7() {
        assert_eq!(array6(vec![1, 9, 4, 6, 6], 0), array6_solution(vec![1, 9, 4, 6, 6], 0))
    }
    #[test]
    fn test_array6_8() {
        assert_eq!(array6(vec![2, 5, 6], 0), array6_solution(vec![2, 5, 6], 0))
    }
}

#[cfg(test)]
mod test_array11 {
    use super::implementations::array11;
    use super::solutions::array11_solution;

    #[test]
    fn test_array11_1() {
        assert_eq!(array11(vec![1, 2, 11], 0), array11_solution(vec![1, 2, 11], 0))
    }
    #[test]
    fn test_array11_2() {
        assert_eq!(array11(vec![11, 11], 0), array11_solution(vec![11, 11], 0))
    }
    #[test]
    fn test_array11_3() {
        assert_eq!(array11(vec![1, 2, 3, 4], 0), array11_solution(vec![1, 2, 3, 4], 0))
    }
    #[test]
    fn test_array11_4() {
        assert_eq!(array11(vec![1, 11, 3, 11, 11], 0), array11_solution(vec![1, 11, 3, 11, 11], 0))
    }
    #[test]
    fn test_array11_5() {
        assert_eq!(array11(vec![11], 0), array11_solution(vec![11], 0))
    }
    #[test]
    fn test_array11_6() {
        assert_eq!(array11(vec![1], 0), array11_solution(vec![1], 0))
    }
    #[test]
    fn test_array11_7() {
        assert_eq!(array11(vec![], 0), array11_solution(vec![], 0))
    }
    #[test]
    fn test_array11_8() {
        assert_eq!(array11(vec![11, 2, 3, 4, 11, 5], 0), array11_solution(vec![11, 2, 3, 4, 11, 5], 0))
    }
    #[test]
    fn test_array11_9() {
        assert_eq!(array11(vec![11, 5, 11], 0), array11_solution(vec![11, 5, 11], 0))
    }
}

#[cfg(test)]
mod test_array220 {
    use super::implementations::array220;
    use super::solutions::array220_solution;

    #[test]
    fn test_array220_1() {
        assert_eq!(array220(vec![1, 2, 20], 0), array220_solution(vec![1, 2, 20], 0))
    }
    #[test]
    fn test_array220_2() {
        assert_eq!(array220(vec![3, 30], 0), array220_solution(vec![3, 30], 0))
    }
    #[test]
    fn test_array220_3() {
        assert_eq!(array220(vec![3], 0), array220_solution(vec![3], 0))
    }
    #[test]
    fn test_array220_4() {
        assert_eq!(array220(vec![], 0), array220_solution(vec![], 0))
    }
    #[test]
    fn test_array220_5() {
        assert_eq!(array220(vec![3, 3, 30, 4], 0), array220_solution(vec![3, 3, 30, 4], 0))
    }
    #[test]
    fn test_array220_6() {
        assert_eq!(array220(vec![2, 19, 4], 0), array220_solution(vec![2, 19, 4], 0))
    }
    #[test]
    fn test_array220_7() {
        assert_eq!(array220(vec![20, 2, 21], 0), array220_solution(vec![20, 2, 21], 0))
    }
    #[test]
    fn test_array220_8() {
        assert_eq!(array220(vec![20, 2, 21, 210], 0), array220_solution(vec![20, 2, 21, 210], 0))
    }
    #[test]
    fn test_array220_9() {
        assert_eq!(array220(vec![2, 200, 2000], 0), array220_solution(vec![2, 200, 2000], 0))
    }
    #[test]
    fn test_array220_10() {
        assert_eq!(array220(vec![0, 0], 0), array220_solution(vec![0, 0], 0))
    }
    #[test]
    fn test_array220_11() {
        assert_eq!(array220(vec![1, 2, 3, 4, 5, 6], 0), array220_solution(vec![1, 2, 3, 4, 5, 6], 0))
    }
    #[test]
    fn test_array220_12() {
        assert_eq!(array220(vec![1, 2, 3, 4, 5, 50, 6], 0), array220_solution(vec![1, 2, 3, 4, 5, 50, 6], 0))
    }
    #[test]
    fn test_array220_13() {
        assert_eq!(array220(vec![1, 2, 3, 4, 5, 51, 6], 0), array220_solution(vec![1, 2, 3, 4, 5, 51, 6], 0))
    }
    #[test]
    fn test_array220_14() {
        assert_eq!(
            array220(vec![1, 2, 3, 4, 4, 50, 500, 6], 0),
            array220_solution(vec![1, 2, 3, 4, 4, 50, 500, 6], 0)
        )
    }
}

#[cfg(test)]
mod test_all_star {
    use super::implementations::all_star;
    use super::solutions::all_star_solution;

    #[test]
    fn test_all_star_1() {
        assert_eq!(all_star("hello"), all_star_solution("hello"))
    }
    #[test]
    fn test_all_star_2() {
        assert_eq!(all_star("abc"), all_star_solution("abc"))
    }
    #[test]
    fn test_all_star_3() {
        assert_eq!(all_star("ab"), all_star_solution("ab"))
    }
    #[test]
    fn test_all_star_4() {
        assert_eq!(all_star("a"), all_star_solution("a"))
    }
    #[test]
    fn test_all_star_5() {
        assert_eq!(all_star(""), all_star_solution(""))
    }
    #[test]
    fn test_all_star_6() {
        assert_eq!(all_star("3.14"), all_star_solution("3.14"))
    }
    #[test]
    fn test_all_star_7() {
        assert_eq!(all_star("Chocolate"), all_star_solution("Chocolate"))
    }
    #[test]
    fn test_all_star_8() {
        assert_eq!(all_star("1234"), all_star_solution("1234"))
    }
}

#[cfg(test)]
mod test_pair_star {
    use super::implementations::pair_star;
    use super::solutions::pair_star_solution;

    #[test]
    fn test_pair_star_1() {
        assert_eq!(pair_star("hello"), pair_star_solution("hello"))
    }
    #[test]
    fn test_pair_star_2() {
        assert_eq!(pair_star("xxyy"), pair_star_solution("xxyy"))
    }
    #[test]
    fn test_pair_star_3() {
        assert_eq!(pair_star("aaaa"), pair_star_solution("aaaa"))
    }
    #[test]
    fn test_pair_star_4() {
        assert_eq!(pair_star("aaab"), pair_star_solution("aaab"))
    }
    #[test]
    fn test_pair_star_5() {
        assert_eq!(pair_star("aa"), pair_star_solution("aa"))
    }
    #[test]
    fn test_pair_star_6() {
        assert_eq!(pair_star("a"), pair_star_solution("a"))
    }
    #[test]
    fn test_pair_star_7() {
        assert_eq!(pair_star(""), pair_star_solution(""))
    }
    #[test]
    fn test_pair_star_8() {
        assert_eq!(pair_star("noadjacent"), pair_star_solution("noadjacent"))
    }
    #[test]
    fn test_pair_star_9() {
        assert_eq!(pair_star("abba"), pair_star_solution("abba"))
    }
    #[test]
    fn test_pair_star_10() {
        assert_eq!(pair_star("abbba"), pair_star_solution("abbba"))
    }
}

#[cfg(test)]
mod test_end_x {
    use super::implementations::end_x;
    use super::solutions::end_x_solution;

    #[test]
    fn test_end_x_1() {
        assert_eq!(end_x("xxre"), end_x_solution("xxre"))
    }
    #[test]
    fn test_end_x_2() {
        assert_eq!(end_x("xxhixx"), end_x_solution("xxhixx"))
    }
    #[test]
    fn test_end_x_3() {
        assert_eq!(end_x("xhixhix"), end_x_solution("xhixhix"))
    }
    #[test]
    fn test_end_x_4() {
        assert_eq!(end_x("hiy"), end_x_solution("hiy"))
    }
    #[test]
    fn test_end_x_5() {
        assert_eq!(end_x("h"), end_x_solution("h"))
    }
    #[test]
    fn test_end_x_6() {
        assert_eq!(end_x("x"), end_x_solution("x"))
    }
    #[test]
    fn test_end_x_7() {
        assert_eq!(end_x("xx"), end_x_solution("xx"))
    }
    #[test]
    fn test_end_x_8() {
        assert_eq!(end_x(""), end_x_solution(""))
    }
    #[test]
    fn test_end_x_9() {
        assert_eq!(end_x("bxx"), end_x_solution("bxx"))
    }
    #[test]
    fn test_end_x_10() {
        assert_eq!(end_x("bxax"), end_x_solution("bxax"))
    }
    #[test]
    fn test_end_x_11() {
        assert_eq!(end_x("axaxax"), end_x_solution("axaxax"))
    }
    #[test]
    fn test_end_x_12() {
        assert_eq!(end_x("xxhxi"), end_x_solution("xxhxi"))
    }
}

#[cfg(test)]
mod test_count_pairs {
    use super::implementations::count_pairs;
    use super::solutions::count_pairs_solution;

    #[test]
    fn test_count_pairs_1() {
        assert_eq!(count_pairs("axa"), count_pairs_solution("axa"))
    }
    #[test]
    fn test_count_pairs_2() {
        assert_eq!(count_pairs("axax"), count_pairs_solution("axax"))
    }
    #[test]
    fn test_count_pairs_3() {
        assert_eq!(count_pairs("axbx"), count_pairs_solution("axbx"))
    }
    #[test]
    fn test_count_pairs_4() {
        assert_eq!(count_pairs("hi"), count_pairs_solution("hi"))
    }
    #[test]
    fn test_count_pairs_5() {
        assert_eq!(count_pairs("hihih"), count_pairs_solution("hihih"))
    }
    #[test]
    fn test_count_pairs_6() {
        assert_eq!(count_pairs("ihihhh"), count_pairs_solution("ihihhh"))
    }
    #[test]
    fn test_count_pairs_7() {
        assert_eq!(count_pairs("ihjxhh"), count_pairs_solution("ihjxhh"))
    }
    #[test]
    fn test_count_pairs_8() {
        assert_eq!(count_pairs(""), count_pairs_solution(""))
    }
    #[test]
    fn test_count_pairs_9() {
        assert_eq!(count_pairs("a"), count_pairs_solution("a"))
    }
    #[test]
    fn test_count_pairs_10() {
        assert_eq!(count_pairs("aa"), count_pairs_solution("aa"))
    }
    #[test]
    fn test_count_pairs_11() {
        assert_eq!(count_pairs("aaa"), count_pairs_solution("aaa"))
    }
}

#[cfg(test)]
mod test_count_abc {
    use super::implementations::count_abc;
    use super::solutions::count_abc_solution;

    #[test]
    fn test_count_abc_1() {
        assert_eq!(count_abc("abc"), count_abc_solution("abc"))
    }
    #[test]
    fn test_count_abc_2() {
        assert_eq!(count_abc("abcxxabc"), count_abc_solution("abcxxabc"))
    }
    #[test]
    fn test_count_abc_3() {
        assert_eq!(count_abc("abaxxaba"), count_abc_solution("abaxxaba"))
    }
    #[test]
    fn test_count_abc_4() {
        assert_eq!(count_abc("ababc"), count_abc_solution("ababc"))
    }
    #[test]
    fn test_count_abc_5() {
        assert_eq!(count_abc("abxbc"), count_abc_solution("abxbc"))
    }
    #[test]
    fn test_count_abc_6() {
        assert_eq!(count_abc("aaabc"), count_abc_solution("aaabc"))
    }
    #[test]
    fn test_count_abc_7() {
        assert_eq!(count_abc("hello"), count_abc_solution("hello"))
    }
    #[test]
    fn test_count_abc_8() {
        assert_eq!(count_abc(""), count_abc_solution(""))
    }
    #[test]
    fn test_count_abc_9() {
        assert_eq!(count_abc("ab"), count_abc_solution("ab"))
    }
    #[test]
    fn test_count_abc_10() {
        assert_eq!(count_abc("aba"), count_abc_solution("aba"))
    }
    #[test]
    fn test_count_abc_11() {
        assert_eq!(count_abc("aca"), count_abc_solution("aca"))
    }
    #[test]
    fn test_count_abc_12() {
        assert_eq!(count_abc("aaa"), count_abc_solution("aaa"))
    }
}

#[cfg(test)]
mod test_count11 {
    use super::implementations::count11;
    use super::solutions::count11_solution;

    #[test]
    fn test_count11_1() {
        assert_eq!(count11("11abc11"), count11_solution("11abc11"))
    }
    #[test]
    fn test_count11_2() {
        assert_eq!(count11("abc11x11x11"), count11_solution("abc11x11x11"))
    }
    #[test]
    fn test_count11_3() {
        assert_eq!(count11("111"), count11_solution("111"))
    }
    #[test]
    fn test_count11_4() {
        assert_eq!(count11("1111"), count11_solution("1111"))
    }
    #[test]
    fn test_count11_5() {
        assert_eq!(count11("1"), count11_solution("1"))
    }
    #[test]
    fn test_count11_6() {
        assert_eq!(count11(""), count11_solution(""))
    }
    #[test]
    fn test_count11_7() {
        assert_eq!(count11("hi"), count11_solution("hi"))
    }
    #[test]
    fn test_count11_8() {
        assert_eq!(count11("11x111x1111"), count11_solution("11x111x1111"))
    }
    #[test]
    fn test_count11_9() {
        assert_eq!(count11("1x111"), count11_solution("1x111"))
    }
    #[test]
    fn test_count11_10() {
        assert_eq!(count11("1Hello1"), count11_solution("1Hello1"))
    }
    #[test]
    fn test_count11_11() {
        assert_eq!(count11("Hello"), count11_solution("Hello"))
    }
}

#[cfg(test)]
mod test_string_clean {
    use super::implementations::string_clean;
    use super::solutions::string_clean_solution;

    #[test]
    fn test_string_clean_1() {
        assert_eq!(string_clean("yyzzza"), string_clean_solution("yyzzza"))
    }
    #[test]
    fn test_string_clean_2() {
        assert_eq!(string_clean("abbbcdd"), string_clean_solution("abbbcdd"))
    }
    #[test]
    fn test_string_clean_3() {
        assert_eq!(string_clean("Hello"), string_clean_solution("Hello"))
    }
    #[test]
    fn test_string_clean_4() {
        assert_eq!(string_clean("XXabcYY"), string_clean_solution("XXabcYY"))
    }
    #[test]
    fn test_string_clean_5() {
        assert_eq!(string_clean("112ab445"), string_clean_solution("112ab445"))
    }
    #[test]
    fn test_string_clean_6() {
        assert_eq!(string_clean("Hello Bookkeeper"), string_clean_solution("Hello Bookkeeper"))
    }
}

#[cfg(test)]
mod test_count_hi2 {
    use super::implementations::count_hi2;
    use super::solutions::count_hi2_solution;

    #[test]
    fn test_count_hi2_1() {
        assert_eq!(count_hi2("ahixhi"), count_hi2_solution("ahixhi"))
    }
    #[test]
    fn test_count_hi2_2() {
        assert_eq!(count_hi2("ahibhi"), count_hi2_solution("ahibhi"))
    }
    #[test]
    fn test_count_hi2_3() {
        assert_eq!(count_hi2("xhixhi"), count_hi2_solution("xhixhi"))
    }
    #[test]
    fn test_count_hi2_4() {
        assert_eq!(count_hi2("hixhi"), count_hi2_solution("hixhi"))
    }
    #[test]
    fn test_count_hi2_5() {
        assert_eq!(count_hi2("hixhhi"), count_hi2_solution("hixhhi"))
    }
    #[test]
    fn test_count_hi2_6() {
        assert_eq!(count_hi2("hihihi"), count_hi2_solution("hihihi"))
    }
    #[test]
    fn test_count_hi2_7() {
        assert_eq!(count_hi2("hihihix"), count_hi2_solution("hihihix"))
    }
    #[test]
    fn test_count_hi2_8() {
        assert_eq!(count_hi2("xhihihix"), count_hi2_solution("xhihihix"))
    }
    #[test]
    fn test_count_hi2_9() {
        assert_eq!(count_hi2("xxhi"), count_hi2_solution("xxhi"))
    }
    #[test]
    fn test_count_hi2_10() {
        assert_eq!(count_hi2("hixxhi"), count_hi2_solution("hixxhi"))
    }
    #[test]
    fn test_count_hi2_11() {
        assert_eq!(count_hi2("hi"), count_hi2_solution("hi"))
    }
    #[test]
    fn test_count_hi2_12() {
        assert_eq!(count_hi2("xxxx"), count_hi2_solution("xxxx"))
    }
    #[test]
    fn test_count_hi2_13() {
        assert_eq!(count_hi2("h"), count_hi2_solution("h"))
    }
    #[test]
    fn test_count_hi2_14() {
        assert_eq!(count_hi2("x"), count_hi2_solution("x"))
    }
    #[test]
    fn test_count_hi2_15() {
        assert_eq!(count_hi2(""), count_hi2_solution(""))
    }
    #[test]
    fn test_count_hi2_16() {
        assert_eq!(count_hi2("Hellohi"), count_hi2_solution("Hellohi"))
    }
}

#[cfg(test)]
mod test_star_bit {
    use super::implementations::star_bit;
    use super::solutions::star_bit_solution;

    #[test]
    fn test_star_bit_1() {
        assert_eq!(star_bit("xyz,-abc*123"), star_bit_solution("xyz,-abc*123"))
    }
    #[test]
    fn test_star_bit_2() {
        assert_eq!(star_bit("x,-hello*"), star_bit_solution("x,-hello*"))
    }
    #[test]
    fn test_star_bit_3() {
        assert_eq!(star_bit(",-xy*1"), star_bit_solution(",-xy*1"))
    }
    #[test]
    fn test_star_bit_4() {
        assert_eq!(star_bit("not really ,-possible*"), star_bit_solution("not really ,-possible*"))
    }
    #[test]
    fn test_star_bit_5() {
        assert_eq!(star_bit(",-abc*"), star_bit_solution(",-abc*"))
    }
    #[test]
    fn test_star_bit_6() {
        assert_eq!(star_bit(",-abc*xyz"), star_bit_solution(",-abc*xyz"))
    }
    #[test]
    fn test_star_bit_7() {
        assert_eq!(star_bit(",-abc*x"), star_bit_solution(",-abc*x"))
    }
    #[test]
    fn test_star_bit_8() {
        assert_eq!(star_bit(",-x*"), star_bit_solution(",-x*"))
    }
    #[test]
    fn test_star_bit_9() {
        assert_eq!(star_bit(",-)*"), star_bit_solution(",-)*"))
    }
    #[test]
    fn test_star_bit_10() {
        assert_eq!(star_bit("res ,-ipsa* loquitor"), star_bit_solution("res ,-ipsa* loquitor"))
    }
    #[test]
    fn test_star_bit_11() {
        assert_eq!(star_bit("hello,-not really*there"), star_bit_solution("hello,-not really*there"))
    }
    #[test]
    fn test_star_bit_12() {
        assert_eq!(star_bit("ab,-ab*ab"), star_bit_solution("ab,-ab*ab"))
    }
}

#[cfg(test)]
mod test_nest_paren {
    use super::implementations::nest_paren;
    use super::solutions::nest_paren_solution;

    #[test]
    fn test_nest_paren_1() {
        assert_eq!(nest_paren("(())"), nest_paren_solution("(())"))
    }
    #[test]
    fn test_nest_paren_2() {
        assert_eq!(nest_paren("((()))"), nest_paren_solution("((()))"))
    }
    #[test]
    fn test_nest_paren_3() {
        assert_eq!(nest_paren("(((x))"), nest_paren_solution("(((x))"))
    }
    #[test]
    fn test_nest_paren_4() {
        assert_eq!(nest_paren("((())"), nest_paren_solution("((())"))
    }
    #[test]
    fn test_nest_paren_5() {
        assert_eq!(nest_paren("((()()"), nest_paren_solution("((()()"))
    }
    #[test]
    fn test_nest_paren_6() {
        assert_eq!(nest_paren("()"), nest_paren_solution("()"))
    }
    #[test]
    fn test_nest_paren_7() {
        assert_eq!(nest_paren(""), nest_paren_solution(""))
    }
    #[test]
    fn test_nest_paren_8() {
        assert_eq!(nest_paren("(yy)"), nest_paren_solution("(yy)"))
    }
    #[test]
    fn test_nest_paren_9() {
        assert_eq!(nest_paren("(())"), nest_paren_solution("(())"))
    }
    #[test]
    fn test_nest_paren_10() {
        assert_eq!(nest_paren("(((y))"), nest_paren_solution("(((y))"))
    }
    #[test]
    fn test_nest_paren_11() {
        assert_eq!(nest_paren("((y)))"), nest_paren_solution("((y)))"))
    }
    #[test]
    fn test_nest_paren_12() {
        assert_eq!(nest_paren("((()))"), nest_paren_solution("((()))"))
    }
    #[test]
    fn test_nest_paren_13() {
        assert_eq!(nest_paren("(())))"), nest_paren_solution("(())))"))
    }
    #[test]
    fn test_nest_paren_14() {
        assert_eq!(nest_paren("((yy())))"), nest_paren_solution("((yy())))"))
    }
    #[test]
    fn test_nest_paren_15() {
        assert_eq!(nest_paren("(((())))"), nest_paren_solution("(((())))"))
    }
}

#[cfg(test)]
mod test_str_count {
    use super::implementations::str_count;
    use super::solutions::str_count_solution;

    #[test]
    fn test_str_count_1() {
        assert_eq!(str_count("catcowcat", "cat"), str_count_solution("catcowcat", "cat"))
    }
    #[test]
    fn test_str_count_2() {
        assert_eq!(str_count("catcowcat", "cow"), str_count_solution("catcowcat", "cow"))
    }
    #[test]
    fn test_str_count_3() {
        assert_eq!(str_count("catcowcat", "dog"), str_count_solution("catcowcat", "dog"))
    }
    #[test]
    fn test_str_count_4() {
        assert_eq!(str_count("cacatcowcat", "cat"), str_count_solution("cacatcowcat", "cat"))
    }
    #[test]
    fn test_str_count_5() {
        assert_eq!(str_count("xyx", "x"), str_count_solution("xyx", "x"))
    }
    #[test]
    fn test_str_count_6() {
        assert_eq!(str_count("iiiijj", "i"), str_count_solution("iiiijj", "i"))
    }
    #[test]
    fn test_str_count_7() {
        assert_eq!(str_count("iiiijj", "ii"), str_count_solution("iiiijj", "ii"))
    }
    #[test]
    fn test_str_count_8() {
        assert_eq!(str_count("iiiijj", "iii"), str_count_solution("iiiijj", "iii"))
    }
    #[test]
    fn test_str_count_9() {
        assert_eq!(str_count("iiiijj", "j"), str_count_solution("iiiijj", "j"))
    }
    #[test]
    fn test_str_count_10() {
        assert_eq!(str_count("iiiijj", "jj"), str_count_solution("iiiijj", "jj"))
    }
    #[test]
    fn test_str_count_11() {
        assert_eq!(str_count("aaabababab", "ab"), str_count_solution("aaabababab", "ab"))
    }
    #[test]
    fn test_str_count_12() {
        assert_eq!(str_count("aaabababab", "aa"), str_count_solution("aaabababab", "aa"))
    }
    #[test]
    fn test_str_count_13() {
        assert_eq!(str_count("aaabababab", "a"), str_count_solution("aaabababab", "a"))
    }
    #[test]
    fn test_str_count_14() {
        assert_eq!(str_count("aaabababab", "b"), str_count_solution("aaabababab", "b"))
    }
}

#[cfg(test)]
mod test_str_copies {
    use super::implementations::str_copies;
    use super::solutions::str_copies_solution;

    #[test]
    fn test_str_copies_1() {
        assert_eq!(str_copies("catcowcat", "cat", 2), str_copies_solution("catcowcat", "cat", 2))
    }
    #[test]
    fn test_str_copies_2() {
        assert_eq!(str_copies("catcowcat", "cow", 2), str_copies_solution("catcowcat", "cow", 2))
    }
    #[test]
    fn test_str_copies_3() {
        assert_eq!(str_copies("catcowcat", "cow", 1), str_copies_solution("catcowcat", "cow", 1))
    }
    #[test]
    fn test_str_copies_4() {
        assert_eq!(str_copies("iiijjj", "i", 3), str_copies_solution("iiijjj", "i", 3))
    }
    #[test]
    fn test_str_copies_5() {
        assert_eq!(str_copies("iiijjj", "i", 4), str_copies_solution("iiijjj", "i", 4))
    }
    #[test]
    fn test_str_copies_6() {
        assert_eq!(str_copies("iiijjj", "ii", 2), str_copies_solution("iiijjj", "ii", 2))
    }
    #[test]
    fn test_str_copies_7() {
        assert_eq!(str_copies("iiijjj", "ii", 3), str_copies_solution("iiijjj", "ii", 3))
    }
    #[test]
    fn test_str_copies_8() {
        assert_eq!(str_copies("iiijjj", "x", 3), str_copies_solution("iiijjj", "x", 3))
    }
    #[test]
    fn test_str_copies_9() {
        assert_eq!(str_copies("iiijjj", "x", 0), str_copies_solution("iiijjj", "x", 0))
    }
    #[test]
    fn test_str_copies_10() {
        assert_eq!(str_copies("iiiiij", "iii", 3), str_copies_solution("iiiiij", "iii", 3))
    }
    #[test]
    fn test_str_copies_11() {
        assert_eq!(str_copies("iiiiij", "iii", 4), str_copies_solution("iiiiij", "iii", 4))
    }
    #[test]
    fn test_str_copies_12() {
        assert_eq!(str_copies("ijiiiiij", "iiii", 2), str_copies_solution("ijiiiiij", "iiii", 2))
    }
    #[test]
    fn test_str_copies_13() {
        assert_eq!(str_copies("ijiiiiij", "iiii", 3), str_copies_solution("ijiiiiij", "iiii", 3))
    }
    #[test]
    fn test_str_copies_14() {
        assert_eq!(str_copies("dogcatdogcat", "dog", 2), str_copies_solution("dogcatdogcat", "dog", 2))
    }
}

#[cfg(test)]
mod test_str_dist {
    use super::implementations::str_dist;
    use super::solutions::str_dist_solution;

    #[test]
    fn test_str_dist_1() {
        assert_eq!(str_dist("catcowcat", "cat"), str_dist_solution("catcowcat", "cat"))
    }
    #[test]
    fn test_str_dist_2() {
        assert_eq!(str_dist("catcowcat", "cow"), str_dist_solution("catcowcat", "cow"))
    }
    #[test]
    fn test_str_dist_3() {
        assert_eq!(str_dist("cccatcowcatxx", "cat"), str_dist_solution("cccatcowcatxx", "cat"))
    }
    #[test]
    fn test_str_dist_4() {
        assert_eq!(str_dist("abccatcowcatcatxyz", "cat"), str_dist_solution("abccatcowcatcatxyz", "cat"))
    }
    #[test]
    fn test_str_dist_5() {
        assert_eq!(str_dist("xyx", "x"), str_dist_solution("xyx", "x"))
    }
    #[test]
    fn test_str_dist_6() {
        assert_eq!(str_dist("xyx", "y"), str_dist_solution("xyx", "y"))
    }
    #[test]
    fn test_str_dist_7() {
        assert_eq!(str_dist("xyx", "z"), str_dist_solution("xyx", "z"))
    }
    #[test]
    fn test_str_dist_8() {
        assert_eq!(str_dist("z", "z"), str_dist_solution("z", "z"))
    }
    #[test]
    fn test_str_dist_9() {
        assert_eq!(str_dist("x", "z"), str_dist_solution("x", "z"))
    }
    #[test]
    fn test_str_dist_10() {
        assert_eq!(str_dist("", "z"), str_dist_solution("", "z"))
    }
    #[test]
    fn test_str_dist_11() {
        assert_eq!(str_dist("hiHellohihihi", "hi"), str_dist_solution("hiHellohihihi", "hi"))
    }
    #[test]
    fn test_str_dist_12() {
        assert_eq!(str_dist("hiHellohihihi", "hih"), str_dist_solution("hiHellohihihi", "hih"))
    }
    #[test]
    fn test_str_dist_13() {
        assert_eq!(str_dist("hiHellohihihi", "o"), str_dist_solution("hiHellohihihi", "o"))
    }
    #[test]
    fn test_str_dist_14() {
        assert_eq!(str_dist("hiHellohihihi", "ll"), str_dist_solution("hiHellohihihi", "ll"))
    }
}
