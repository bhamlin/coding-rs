pub mod implementations;
pub mod runners;
pub mod solutions;

#[cfg(test)]
mod test_hello_name {
    use super::implementations::hello_name;
    use super::solutions::hello_name_solution;

    #[test]
    fn test_hello_name_1() {
        assert_eq!(hello_name("Bob"), hello_name_solution("Bob"))
    }
    #[test]
    fn test_hello_name_2() {
        assert_eq!(hello_name("Alice"), hello_name_solution("Alice"))
    }
    #[test]
    fn test_hello_name_3() {
        assert_eq!(hello_name("X"), hello_name_solution("X"))
    }
    #[test]
    fn test_hello_name_4() {
        assert_eq!(hello_name("Dolly"), hello_name_solution("Dolly"))
    }
    #[test]
    fn test_hello_name_5() {
        assert_eq!(hello_name("Alpha"), hello_name_solution("Alpha"))
    }
    #[test]
    fn test_hello_name_6() {
        assert_eq!(hello_name("Omega"), hello_name_solution("Omega"))
    }
    #[test]
    fn test_hello_name_7() {
        assert_eq!(hello_name("Goodbye"), hello_name_solution("Goodbye"))
    }
    #[test]
    fn test_hello_name_8() {
        assert_eq!(hello_name("ho ho ho"), hello_name_solution("ho ho ho"))
    }
    #[test]
    fn test_hello_name_9() {
        assert_eq!(hello_name("xyz!"), hello_name_solution("xyz!"))
    }
    #[test]
    fn test_hello_name_10() {
        assert_eq!(hello_name("Hello!"), hello_name_solution("Hello!"))
    }
}

#[cfg(test)]
mod test_make_abba {
    use super::implementations::make_abba;
    use super::solutions::make_abba_solution;

    #[test]
    fn test_make_abba_1() {
        assert_eq!(make_abba("Hi", "Bye"), make_abba_solution("Hi", "Bye"))
    }
    #[test]
    fn test_make_abba_2() {
        assert_eq!(make_abba("Yo", "Alice"), make_abba_solution("Yo", "Alice"))
    }
    #[test]
    fn test_make_abba_3() {
        assert_eq!(make_abba("What", "Up"), make_abba_solution("What", "Up"))
    }
    #[test]
    fn test_make_abba_4() {
        assert_eq!(make_abba("aaa", "bbb"), make_abba_solution("aaa", "bbb"))
    }
    #[test]
    fn test_make_abba_5() {
        assert_eq!(make_abba("x", "y"), make_abba_solution("x", "y"))
    }
    #[test]
    fn test_make_abba_6() {
        assert_eq!(make_abba("x", ""), make_abba_solution("x", ""))
    }
    #[test]
    fn test_make_abba_7() {
        assert_eq!(make_abba("ba", "Ya"), make_abba_solution("ba", "Ya"))
    }
    #[test]
    fn test_make_abba_8() {
        assert_eq!(make_abba("Ya", "Ya"), make_abba_solution("Ya", "Ya"))
    }
}

#[cfg(test)]
mod test_make_tags {
    use super::implementations::make_tags;
    use super::solutions::make_tags_solution;

    #[test]
    fn test_make_tags_1() {
        assert_eq!(make_tags("i", "Yay"), make_tags_solution("i", "Yay"))
    }
    #[test]
    fn test_make_tags_2() {
        assert_eq!(make_tags("i", "Hello"), make_tags_solution("i", "Hello"))
    }
    #[test]
    fn test_make_tags_3() {
        assert_eq!(make_tags("cite", "Yay"), make_tags_solution("cite", "Yay"))
    }
    #[test]
    fn test_make_tags_4() {
        assert_eq!(make_tags("address", "here"), make_tags_solution("address", "here"))
    }
    #[test]
    fn test_make_tags_5() {
        assert_eq!(make_tags("body", "Heart"), make_tags_solution("body", "Heart"))
    }
    #[test]
    fn test_make_tags_6() {
        assert_eq!(make_tags("i", "i"), make_tags_solution("i", "i"))
    }
    #[test]
    fn test_make_tags_7() {
        assert_eq!(make_tags("i", "i"), make_tags_solution("i", "i"))
    }
    #[test]
    fn test_make_tags_8() {
        assert_eq!(make_tags("i", ""), make_tags_solution("i", ""))
    }
}

#[cfg(test)]
mod test_make_out_word {
    use super::implementations::make_out_word;
    use super::solutions::make_out_word_solution;

    #[test]
    fn test_make_out_word_1() {
        assert_eq!(make_out_word("<<>>", "Yay"), make_out_word_solution("<<>>", "Yay"))
    }
    #[test]
    fn test_make_out_word_2() {
        assert_eq!(make_out_word("<<>>", "WooHoo"), make_out_word_solution("<<>>", "WooHoo"))
    }
    #[test]
    fn test_make_out_word_3() {
        assert_eq!(make_out_word("[[]]", "word"), make_out_word_solution("[[]]", "word"))
    }
    #[test]
    fn test_make_out_word_4() {
        assert_eq!(make_out_word("HHoo", "Hello"), make_out_word_solution("HHoo", "Hello"))
    }
    #[test]
    fn test_make_out_word_5() {
        assert_eq!(make_out_word("abyz", "YAY"), make_out_word_solution("abyz", "YAY"))
    }
}

#[cfg(test)]
mod test_extra_end {
    use super::implementations::extra_end;
    use super::solutions::extra_end_solution;

    #[test]
    fn test_extra_end_1() {
        assert_eq!(extra_end("Hello"), extra_end_solution("Hello"))
    }
    #[test]
    fn test_extra_end_2() {
        assert_eq!(extra_end("ab"), extra_end_solution("ab"))
    }
    #[test]
    fn test_extra_end_3() {
        assert_eq!(extra_end("Hi"), extra_end_solution("Hi"))
    }
    #[test]
    fn test_extra_end_4() {
        assert_eq!(extra_end("Candy"), extra_end_solution("Candy"))
    }
    #[test]
    fn test_extra_end_5() {
        assert_eq!(extra_end("Code"), extra_end_solution("Code"))
    }
}

#[cfg(test)]
mod test_first_two {
    use super::implementations::first_two;
    use super::solutions::first_two_solution;

    #[test]
    fn test_first_two_1() {
        assert_eq!(first_two("Hello"), first_two_solution("Hello"))
    }
    #[test]
    fn test_first_two_2() {
        assert_eq!(first_two("abcdefg"), first_two_solution("abcdefg"))
    }
    #[test]
    fn test_first_two_3() {
        assert_eq!(first_two("ab"), first_two_solution("ab"))
    }
    #[test]
    fn test_first_two_4() {
        assert_eq!(first_two("a"), first_two_solution("a"))
    }
    #[test]
    fn test_first_two_5() {
        assert_eq!(first_two(""), first_two_solution(""))
    }
    #[test]
    fn test_first_two_6() {
        assert_eq!(first_two("kitten"), first_two_solution("kitten"))
    }
    #[test]
    fn test_first_two_7() {
        assert_eq!(first_two("hi"), first_two_solution("hi"))
    }
    #[test]
    fn test_first_two_8() {
        assert_eq!(first_two("hiya"), first_two_solution("hiya"))
    }
}

#[cfg(test)]
mod test_first_half {
    use super::implementations::first_half;
    use super::solutions::first_half_solution;

    #[test]
    fn test_first_half_1() {
        assert_eq!(first_half("WooHoo"), first_half_solution("WooHoo"))
    }
    #[test]
    fn test_first_half_2() {
        assert_eq!(first_half("HelloThere"), first_half_solution("HelloThere"))
    }
    #[test]
    fn test_first_half_3() {
        assert_eq!(first_half("abcdefg"), first_half_solution("abcdefg"))
    }
    #[test]
    fn test_first_half_4() {
        assert_eq!(first_half("ab"), first_half_solution("ab"))
    }
    #[test]
    fn test_first_half_5() {
        assert_eq!(first_half(""), first_half_solution(""))
    }
    #[test]
    fn test_first_half_6() {
        assert_eq!(first_half("0123456789"), first_half_solution("0123456789"))
    }
    #[test]
    fn test_first_half_7() {
        assert_eq!(first_half("kitten"), first_half_solution("kitten"))
    }
}

#[cfg(test)]
mod test_without_end {
    use super::implementations::without_end;
    use super::solutions::without_end_solution;

    #[test]
    fn test_without_end_1() {
        assert_eq!(without_end("Hello"), without_end_solution("Hello"))
    }
    #[test]
    fn test_without_end_2() {
        assert_eq!(without_end("java"), without_end_solution("java"))
    }
    #[test]
    fn test_without_end_3() {
        assert_eq!(without_end("coding"), without_end_solution("coding"))
    }
    #[test]
    fn test_without_end_4() {
        assert_eq!(without_end("code"), without_end_solution("code"))
    }
    #[test]
    fn test_without_end_5() {
        assert_eq!(without_end("ab"), without_end_solution("ab"))
    }
    #[test]
    fn test_without_end_6() {
        assert_eq!(without_end("Chocolate!"), without_end_solution("Chocolate!"))
    }
    #[test]
    fn test_without_end_7() {
        assert_eq!(without_end("kitten"), without_end_solution("kitten"))
    }
    #[test]
    fn test_without_end_8() {
        assert_eq!(without_end("woohoo"), without_end_solution("woohoo"))
    }
}

#[cfg(test)]
mod test_combo_string {
    use super::implementations::combo_string;
    use super::solutions::combo_string_solution;

    #[test]
    fn test_combo_string_1() {
        assert_eq!(combo_string("Hello", "hi"), combo_string_solution("Hello", "hi"))
    }
    #[test]
    fn test_combo_string_2() {
        assert_eq!(combo_string("Hi", "Hello"), combo_string_solution("Hi", "Hello"))
    }
    #[test]
    fn test_combo_string_3() {
        assert_eq!(combo_string("aaa", "b"), combo_string_solution("aaa", "b"))
    }
    #[test]
    fn test_combo_string_4() {
        assert_eq!(combo_string("b", "aaa"), combo_string_solution("b", "aaa"))
    }
    #[test]
    fn test_combo_string_5() {
        assert_eq!(combo_string("aaa", ""), combo_string_solution("aaa", ""))
    }
    #[test]
    fn test_combo_string_6() {
        assert_eq!(combo_string("", "bb"), combo_string_solution("", "bb"))
    }
    #[test]
    fn test_combo_string_7() {
        assert_eq!(combo_string("aaa", "1234"), combo_string_solution("aaa", "1234"))
    }
    #[test]
    fn test_combo_string_8() {
        assert_eq!(combo_string("aaa", "bb"), combo_string_solution("aaa", "bb"))
    }
    #[test]
    fn test_combo_string_9() {
        assert_eq!(combo_string("a", "bb"), combo_string_solution("a", "bb"))
    }
    #[test]
    fn test_combo_string_10() {
        assert_eq!(combo_string("bb", "a"), combo_string_solution("bb", "a"))
    }
    #[test]
    fn test_combo_string_11() {
        assert_eq!(combo_string("a", "bb"), combo_string_solution("a", "bb"))
    }
    #[test]
    fn test_combo_string_12() {
        assert_eq!(combo_string("xyz", "ab"), combo_string_solution("xyz", "ab"))
    }
}

#[cfg(test)]
mod test_non_start {
    use super::implementations::non_start;
    use super::solutions::non_start_solution;

    #[test]
    fn test_non_start_1() {
        assert_eq!(non_start("Hello", "There"), non_start_solution("Hello", "There"))
    }
    #[test]
    fn test_non_start_2() {
        assert_eq!(non_start("java", "code"), non_start_solution("java", "code"))
    }
    #[test]
    fn test_non_start_3() {
        assert_eq!(non_start("shotl", "java"), non_start_solution("shotl", "java"))
    }
    #[test]
    fn test_non_start_4() {
        assert_eq!(non_start("ab", "xy"), non_start_solution("ab", "xy"))
    }
    #[test]
    fn test_non_start_5() {
        assert_eq!(non_start("ab", "x"), non_start_solution("ab", "x"))
    }
    #[test]
    fn test_non_start_6() {
        assert_eq!(non_start("x", "ac"), non_start_solution("x", "ac"))
    }
    #[test]
    fn test_non_start_7() {
        assert_eq!(non_start("a", "x"), non_start_solution("a", "x"))
    }
    #[test]
    fn test_non_start_8() {
        assert_eq!(non_start("kit", "kat"), non_start_solution("kit", "kat"))
    }
    #[test]
    fn test_non_start_9() {
        assert_eq!(non_start("mart", "dart"), non_start_solution("mart", "dart"))
    }
}

#[cfg(test)]
mod test_left2 {
    use super::implementations::left2;
    use super::solutions::left2_solution;

    #[test]
    fn test_left2_1() {
        assert_eq!(left2("Hello"), left2_solution("Hello"))
    }
    #[test]
    fn test_left2_2() {
        assert_eq!(left2("java"), left2_solution("java"))
    }
    #[test]
    fn test_left2_3() {
        assert_eq!(left2("Hi"), left2_solution("Hi"))
    }
    #[test]
    fn test_left2_4() {
        assert_eq!(left2("code"), left2_solution("code"))
    }
    #[test]
    fn test_left2_5() {
        assert_eq!(left2("cat"), left2_solution("cat"))
    }
    #[test]
    fn test_left2_6() {
        assert_eq!(left2("12345"), left2_solution("12345"))
    }
    #[test]
    fn test_left2_7() {
        assert_eq!(left2("Chocolate"), left2_solution("Chocolate"))
    }
    #[test]
    fn test_left2_8() {
        assert_eq!(left2("bricks"), left2_solution("bricks"))
    }
}

#[cfg(test)]
mod test_right2 {
    use super::implementations::right2;
    use super::solutions::right2_solution;

    #[test]
    fn test_right2_1() {
        assert_eq!(right2("Hello"), right2_solution("Hello"))
    }
    #[test]
    fn test_right2_2() {
        assert_eq!(right2("java"), right2_solution("java"))
    }
    #[test]
    fn test_right2_3() {
        assert_eq!(right2("Hi"), right2_solution("Hi"))
    }
    #[test]
    fn test_right2_4() {
        assert_eq!(right2("code"), right2_solution("code"))
    }
    #[test]
    fn test_right2_5() {
        assert_eq!(right2("cat"), right2_solution("cat"))
    }
    #[test]
    fn test_right2_6() {
        assert_eq!(right2("12345"), right2_solution("12345"))
    }
}

#[cfg(test)]
mod test_the_end {
    use super::implementations::the_end;
    use super::solutions::the_end_solution;

    #[test]
    fn test_the_end_1() {
        assert_eq!(the_end("Hello", true), the_end_solution("Hello", true))
    }
    #[test]
    fn test_the_end_2() {
        assert_eq!(the_end("Hello", false), the_end_solution("Hello", false))
    }
    #[test]
    fn test_the_end_3() {
        assert_eq!(the_end("oh", true), the_end_solution("oh", true))
    }
    #[test]
    fn test_the_end_4() {
        assert_eq!(the_end("oh", false), the_end_solution("oh", false))
    }
    #[test]
    fn test_the_end_5() {
        assert_eq!(the_end("x", true), the_end_solution("x", true))
    }
    #[test]
    fn test_the_end_6() {
        assert_eq!(the_end("x", false), the_end_solution("x", false))
    }
    #[test]
    fn test_the_end_7() {
        assert_eq!(the_end("java", true), the_end_solution("java", true))
    }
    #[test]
    fn test_the_end_8() {
        assert_eq!(the_end("chocolate", false), the_end_solution("chocolate", false))
    }
    #[test]
    fn test_the_end_9() {
        assert_eq!(the_end("1234", true), the_end_solution("1234", true))
    }
    #[test]
    fn test_the_end_10() {
        assert_eq!(the_end("code", false), the_end_solution("code", false))
    }
}

#[cfg(test)]
mod test_without_end2 {
    use super::implementations::without_end2;
    use super::solutions::without_end2_solution;

    #[test]
    fn test_without_end2_1() {
        assert_eq!(without_end2("Hello"), without_end2_solution("Hello"))
    }
    #[test]
    fn test_without_end2_2() {
        assert_eq!(without_end2("abc"), without_end2_solution("abc"))
    }
    #[test]
    fn test_without_end2_3() {
        assert_eq!(without_end2("ab"), without_end2_solution("ab"))
    }
    #[test]
    fn test_without_end2_4() {
        assert_eq!(without_end2("a"), without_end2_solution("a"))
    }
    #[test]
    fn test_without_end2_5() {
        assert_eq!(without_end2(""), without_end2_solution(""))
    }
    #[test]
    fn test_without_end2_6() {
        assert_eq!(without_end2("coldy"), without_end2_solution("coldy"))
    }
    #[test]
    fn test_without_end2_7() {
        assert_eq!(without_end2("java code"), without_end2_solution("java code"))
    }
}

#[cfg(test)]
mod test_middle_two {
    use super::implementations::middle_two;
    use super::solutions::middle_two_solution;

    #[test]
    fn test_middle_two_1() {
        assert_eq!(middle_two("string"), middle_two_solution("string"))
    }
    #[test]
    fn test_middle_two_2() {
        assert_eq!(middle_two("code"), middle_two_solution("code"))
    }
    #[test]
    fn test_middle_two_3() {
        assert_eq!(middle_two("Practice"), middle_two_solution("Practice"))
    }
    #[test]
    fn test_middle_two_4() {
        assert_eq!(middle_two("ab"), middle_two_solution("ab"))
    }
    #[test]
    fn test_middle_two_5() {
        assert_eq!(middle_two("123456789"), middle_two_solution("123456789"))
    }
}

#[cfg(test)]
mod test_ends_ly {
    use super::implementations::ends_ly;
    use super::solutions::ends_ly_solution;

    #[test]
    fn test_ends_ly_1() {
        assert_eq!(ends_ly("oddly"), ends_ly_solution("oddly"))
    }
    #[test]
    fn test_ends_ly_2() {
        assert_eq!(ends_ly("y"), ends_ly_solution("y"))
    }
    #[test]
    fn test_ends_ly_3() {
        assert_eq!(ends_ly("oddl"), ends_ly_solution("oddl"))
    }
    #[test]
    fn test_ends_ly_4() {
        assert_eq!(ends_ly("olydd"), ends_ly_solution("olydd"))
    }
    #[test]
    fn test_ends_ly_5() {
        assert_eq!(ends_ly("ly"), ends_ly_solution("ly"))
    }
    #[test]
    fn test_ends_ly_6() {
        assert_eq!(ends_ly(""), ends_ly_solution(""))
    }
    #[test]
    fn test_ends_ly_7() {
        assert_eq!(ends_ly("falsely"), ends_ly_solution("falsely"))
    }
    #[test]
    fn test_ends_ly_8() {
        assert_eq!(ends_ly("evenly"), ends_ly_solution("evenly"))
    }
}

#[cfg(test)]
mod test_n_twice {
    use super::implementations::n_twice;
    use super::solutions::n_twice_solution;

    #[test]
    fn test_n_twice_1() {
        assert_eq!(n_twice("hello", 2), n_twice_solution("hello", 2))
    }
    #[test]
    fn test_n_twice_2() {
        assert_eq!(n_twice("Chocolate", 3), n_twice_solution("Chocolate", 3))
    }
    #[test]
    fn test_n_twice_3() {
        assert_eq!(n_twice("Chocolate", 1), n_twice_solution("Chocolate", 1))
    }
    #[test]
    fn test_n_twice_4() {
        assert_eq!(n_twice("Chocolate", 0), n_twice_solution("Chocolate", 0))
    }
    #[test]
    fn test_n_twice_5() {
        assert_eq!(n_twice("Hello", 4), n_twice_solution("Hello", 4))
    }
    #[test]
    fn test_n_twice_6() {
        assert_eq!(n_twice("Code", 4), n_twice_solution("Code", 4))
    }
    #[test]
    fn test_n_twice_7() {
        assert_eq!(n_twice("Code", 2), n_twice_solution("Code", 2))
    }
}

#[cfg(test)]
mod test_two_char {
    use super::implementations::two_char;
    use super::solutions::two_char_solution;

    #[test]
    fn test_two_char_1() {
        assert_eq!(two_char("java", 0), two_char_solution("java", 0))
    }
    #[test]
    fn test_two_char_2() {
        assert_eq!(two_char("java", 2), two_char_solution("java", 2))
    }
    #[test]
    fn test_two_char_3() {
        assert_eq!(two_char("java", 3), two_char_solution("java", 3))
    }
    #[test]
    fn test_two_char_4() {
        assert_eq!(two_char("java", 4), two_char_solution("java", 4))
    }
    #[test]
    fn test_two_char_5() {
        assert_eq!(two_char("java", -1), two_char_solution("java", -1))
    }
    #[test]
    fn test_two_char_6() {
        assert_eq!(two_char("Hello", 0), two_char_solution("Hello", 0))
    }
    #[test]
    fn test_two_char_7() {
        assert_eq!(two_char("Hello", 1), two_char_solution("Hello", 1))
    }
    #[test]
    fn test_two_char_8() {
        assert_eq!(two_char("Hello", 99), two_char_solution("Hello", 99))
    }
    #[test]
    fn test_two_char_9() {
        assert_eq!(two_char("Hello", 3), two_char_solution("Hello", 3))
    }
    #[test]
    fn test_two_char_10() {
        assert_eq!(two_char("Hello", 4), two_char_solution("Hello", 4))
    }
    #[test]
    fn test_two_char_11() {
        assert_eq!(two_char("Hello", 5), two_char_solution("Hello", 5))
    }
    #[test]
    fn test_two_char_12() {
        assert_eq!(two_char("Hello", -7), two_char_solution("Hello", -7))
    }
    #[test]
    fn test_two_char_13() {
        assert_eq!(two_char("Hello", 6), two_char_solution("Hello", 6))
    }
    #[test]
    fn test_two_char_14() {
        assert_eq!(two_char("Hello", -1), two_char_solution("Hello", -1))
    }
    #[test]
    fn test_two_char_15() {
        assert_eq!(two_char("yay", 0), two_char_solution("yay", 0))
    }
}

#[cfg(test)]
mod test_middle_three {
    use super::implementations::middle_three;
    use super::solutions::middle_three_solution;

    #[test]
    fn test_middle_three_1() {
        assert_eq!(middle_three("Candy"), middle_three_solution("Candy"))
    }
    #[test]
    fn test_middle_three_2() {
        assert_eq!(middle_three("and"), middle_three_solution("and"))
    }
    #[test]
    fn test_middle_three_3() {
        assert_eq!(middle_three("solving"), middle_three_solution("solving"))
    }
    #[test]
    fn test_middle_three_4() {
        assert_eq!(middle_three("Hi yet Hi"), middle_three_solution("Hi yet Hi"))
    }
    #[test]
    fn test_middle_three_5() {
        assert_eq!(middle_three("java yet java"), middle_three_solution("java yet java"))
    }
    #[test]
    fn test_middle_three_6() {
        assert_eq!(middle_three("Chocolate"), middle_three_solution("Chocolate"))
    }
    #[test]
    fn test_middle_three_7() {
        assert_eq!(middle_three("XabcxyzabcX"), middle_three_solution("XabcxyzabcX"))
    }
}

#[cfg(test)]
mod test_has_bad {
    use super::implementations::has_bad;
    use super::solutions::has_bad_solution;

    #[test]
    fn test_has_bad_1() {
        assert_eq!(has_bad("badxx"), has_bad_solution("badxx"))
    }
    #[test]
    fn test_has_bad_2() {
        assert_eq!(has_bad("xbadxx"), has_bad_solution("xbadxx"))
    }
    #[test]
    fn test_has_bad_3() {
        assert_eq!(has_bad("xxbadxx"), has_bad_solution("xxbadxx"))
    }
    #[test]
    fn test_has_bad_4() {
        assert_eq!(has_bad("code"), has_bad_solution("code"))
    }
    #[test]
    fn test_has_bad_5() {
        assert_eq!(has_bad("bad"), has_bad_solution("bad"))
    }
    #[test]
    fn test_has_bad_6() {
        assert_eq!(has_bad("ba"), has_bad_solution("ba"))
    }
    #[test]
    fn test_has_bad_7() {
        assert_eq!(has_bad("xba"), has_bad_solution("xba"))
    }
    #[test]
    fn test_has_bad_8() {
        assert_eq!(has_bad("xbad"), has_bad_solution("xbad"))
    }
    #[test]
    fn test_has_bad_9() {
        assert_eq!(has_bad(""), has_bad_solution(""))
    }
    #[test]
    fn test_has_bad_10() {
        assert_eq!(has_bad("badyy"), has_bad_solution("badyy"))
    }
}

#[cfg(test)]
mod test_at_first {
    use super::implementations::at_first;
    use super::solutions::at_first_solution;

    #[test]
    fn test_at_first_1() {
        assert_eq!(at_first("hello"), at_first_solution("hello"))
    }
    #[test]
    fn test_at_first_2() {
        assert_eq!(at_first("hi"), at_first_solution("hi"))
    }
    #[test]
    fn test_at_first_3() {
        assert_eq!(at_first("h"), at_first_solution("h"))
    }
    #[test]
    fn test_at_first_4() {
        assert_eq!(at_first(""), at_first_solution(""))
    }
    #[test]
    fn test_at_first_5() {
        assert_eq!(at_first("kitten"), at_first_solution("kitten"))
    }
    #[test]
    fn test_at_first_6() {
        assert_eq!(at_first("java"), at_first_solution("java"))
    }
    #[test]
    fn test_at_first_7() {
        assert_eq!(at_first("j"), at_first_solution("j"))
    }
}

#[cfg(test)]
mod test_last_chars {
    use super::implementations::last_chars;
    use super::solutions::last_chars_solution;

    #[test]
    fn test_last_chars_1() {
        assert_eq!(last_chars("last", "chars"), last_chars_solution("last", "chars"))
    }
    #[test]
    fn test_last_chars_2() {
        assert_eq!(last_chars("yo", "java"), last_chars_solution("yo", "java"))
    }
    #[test]
    fn test_last_chars_3() {
        assert_eq!(last_chars("hi", ""), last_chars_solution("hi", ""))
    }
    #[test]
    fn test_last_chars_4() {
        assert_eq!(last_chars("", "hello"), last_chars_solution("", "hello"))
    }
    #[test]
    fn test_last_chars_5() {
        assert_eq!(last_chars("", ""), last_chars_solution("", ""))
    }
    #[test]
    fn test_last_chars_6() {
        assert_eq!(last_chars("kitten", "hi"), last_chars_solution("kitten", "hi"))
    }
    #[test]
    fn test_last_chars_7() {
        assert_eq!(last_chars("k", "zip"), last_chars_solution("k", "zip"))
    }
    #[test]
    fn test_last_chars_8() {
        assert_eq!(last_chars("kitten", ""), last_chars_solution("kitten", ""))
    }
    #[test]
    fn test_last_chars_9() {
        assert_eq!(last_chars("kitten", "zip"), last_chars_solution("kitten", "zip"))
    }
}

#[cfg(test)]
mod test_con_cat {
    use super::implementations::con_cat;
    use super::solutions::con_cat_solution;

    #[test]
    fn test_con_cat_1() {
        assert_eq!(con_cat("abc", "cat"), con_cat_solution("abc", "cat"))
    }
    #[test]
    fn test_con_cat_2() {
        assert_eq!(con_cat("dog", "cat"), con_cat_solution("dog", "cat"))
    }
    #[test]
    fn test_con_cat_3() {
        assert_eq!(con_cat("abc", ""), con_cat_solution("abc", ""))
    }
    #[test]
    fn test_con_cat_4() {
        assert_eq!(con_cat("", "cat"), con_cat_solution("", "cat"))
    }
    #[test]
    fn test_con_cat_5() {
        assert_eq!(con_cat("pig", "g"), con_cat_solution("pig", "g"))
    }
    #[test]
    fn test_con_cat_6() {
        assert_eq!(con_cat("pig", "doggy"), con_cat_solution("pig", "doggy"))
    }
}

#[cfg(test)]
mod test_last_two {
    use super::implementations::last_two;
    use super::solutions::last_two_solution;

    #[test]
    fn test_last_two_1() {
        assert_eq!(last_two("coding"), last_two_solution("coding"))
    }
    #[test]
    fn test_last_two_2() {
        assert_eq!(last_two("cat"), last_two_solution("cat"))
    }
    #[test]
    fn test_last_two_3() {
        assert_eq!(last_two("ab"), last_two_solution("ab"))
    }
    #[test]
    fn test_last_two_4() {
        assert_eq!(last_two("a"), last_two_solution("a"))
    }
    #[test]
    fn test_last_two_5() {
        assert_eq!(last_two(""), last_two_solution(""))
    }
}

#[cfg(test)]
mod test_see_color {
    use super::implementations::see_color;
    use super::solutions::see_color_solution;

    #[test]
    fn test_see_color_1() {
        assert_eq!(see_color("redxx"), see_color_solution("redxx"))
    }
    #[test]
    fn test_see_color_2() {
        assert_eq!(see_color("xxred"), see_color_solution("xxred"))
    }
    #[test]
    fn test_see_color_3() {
        assert_eq!(see_color("blueTimes"), see_color_solution("blueTimes"))
    }
    #[test]
    fn test_see_color_4() {
        assert_eq!(see_color("NoColor"), see_color_solution("NoColor"))
    }
    #[test]
    fn test_see_color_5() {
        assert_eq!(see_color("red"), see_color_solution("red"))
    }
    #[test]
    fn test_see_color_6() {
        assert_eq!(see_color("re"), see_color_solution("re"))
    }
    #[test]
    fn test_see_color_7() {
        assert_eq!(see_color("blu"), see_color_solution("blu"))
    }
    #[test]
    fn test_see_color_8() {
        assert_eq!(see_color("blue"), see_color_solution("blue"))
    }
    #[test]
    fn test_see_color_9() {
        assert_eq!(see_color("a"), see_color_solution("a"))
    }
    #[test]
    fn test_see_color_10() {
        assert_eq!(see_color(""), see_color_solution(""))
    }
    #[test]
    fn test_see_color_11() {
        assert_eq!(see_color("xyzred"), see_color_solution("xyzred"))
    }
}

#[cfg(test)]
mod test_front_again {
    use super::implementations::front_again;
    use super::solutions::front_again_solution;

    #[test]
    fn test_front_again_1() {
        assert_eq!(front_again("edited"), front_again_solution("edited"))
    }
    #[test]
    fn test_front_again_2() {
        assert_eq!(front_again("edit"), front_again_solution("edit"))
    }
    #[test]
    fn test_front_again_3() {
        assert_eq!(front_again("ed"), front_again_solution("ed"))
    }
    #[test]
    fn test_front_again_4() {
        assert_eq!(front_again("jj"), front_again_solution("jj"))
    }
    #[test]
    fn test_front_again_5() {
        assert_eq!(front_again("jjj"), front_again_solution("jjj"))
    }
    #[test]
    fn test_front_again_6() {
        assert_eq!(front_again("jjjj"), front_again_solution("jjjj"))
    }
    #[test]
    fn test_front_again_7() {
        assert_eq!(front_again("jjjk"), front_again_solution("jjjk"))
    }
    #[test]
    fn test_front_again_8() {
        assert_eq!(front_again("x"), front_again_solution("x"))
    }
    #[test]
    fn test_front_again_9() {
        assert_eq!(front_again(""), front_again_solution(""))
    }
    #[test]
    fn test_front_again_10() {
        assert_eq!(front_again("java"), front_again_solution("java"))
    }
    #[test]
    fn test_front_again_11() {
        assert_eq!(front_again("javaja"), front_again_solution("javaja"))
    }
}

#[cfg(test)]
mod test_min_cat {
    use super::implementations::min_cat;
    use super::solutions::min_cat_solution;

    #[test]
    fn test_min_cat_1() {
        assert_eq!(min_cat("Hello", "Hi"), min_cat_solution("Hello", "Hi"))
    }
    #[test]
    fn test_min_cat_2() {
        assert_eq!(min_cat("Hello", "java"), min_cat_solution("Hello", "java"))
    }
    #[test]
    fn test_min_cat_3() {
        assert_eq!(min_cat("java", "Hello"), min_cat_solution("java", "Hello"))
    }
    #[test]
    fn test_min_cat_4() {
        assert_eq!(min_cat("abc", "x"), min_cat_solution("abc", "x"))
    }
    #[test]
    fn test_min_cat_5() {
        assert_eq!(min_cat("x", "abc"), min_cat_solution("x", "abc"))
    }
    #[test]
    fn test_min_cat_6() {
        assert_eq!(min_cat("abc", ""), min_cat_solution("abc", ""))
    }
}

#[cfg(test)]
mod test_extra_front {
    use super::implementations::extra_front;
    use super::solutions::extra_front_solution;

    #[test]
    fn test_extra_front_1() {
        assert_eq!(extra_front("Hello"), extra_front_solution("Hello"))
    }
    #[test]
    fn test_extra_front_2() {
        assert_eq!(extra_front("ab"), extra_front_solution("ab"))
    }
    #[test]
    fn test_extra_front_3() {
        assert_eq!(extra_front("H"), extra_front_solution("H"))
    }
    #[test]
    fn test_extra_front_4() {
        assert_eq!(extra_front(""), extra_front_solution(""))
    }
    #[test]
    fn test_extra_front_5() {
        assert_eq!(extra_front("Candy"), extra_front_solution("Candy"))
    }
    #[test]
    fn test_extra_front_6() {
        assert_eq!(extra_front("Code"), extra_front_solution("Code"))
    }
}

#[cfg(test)]
mod test_without2 {
    use super::implementations::without2;
    use super::solutions::without2_solution;

    #[test]
    fn test_without2_1() {
        assert_eq!(without2("HelloHe"), without2_solution("HelloHe"))
    }
    #[test]
    fn test_without2_2() {
        assert_eq!(without2("HelloHi"), without2_solution("HelloHi"))
    }
    #[test]
    fn test_without2_3() {
        assert_eq!(without2("Hi"), without2_solution("Hi"))
    }
    #[test]
    fn test_without2_4() {
        assert_eq!(without2("Chocolate"), without2_solution("Chocolate"))
    }
    #[test]
    fn test_without2_5() {
        assert_eq!(without2("xxx"), without2_solution("xxx"))
    }
    #[test]
    fn test_without2_6() {
        assert_eq!(without2("xx"), without2_solution("xx"))
    }
    #[test]
    fn test_without2_7() {
        assert_eq!(without2("x"), without2_solution("x"))
    }
    #[test]
    fn test_without2_8() {
        assert_eq!(without2(""), without2_solution(""))
    }
    #[test]
    fn test_without2_9() {
        assert_eq!(without2("Fruits"), without2_solution("Fruits"))
    }
}

#[cfg(test)]
mod test_de_front {
    use super::implementations::de_front;
    use super::solutions::de_front_solution;

    #[test]
    fn test_de_front_1() {
        assert_eq!(de_front("Hello"), de_front_solution("Hello"))
    }
    #[test]
    fn test_de_front_2() {
        assert_eq!(de_front("java"), de_front_solution("java"))
    }
    #[test]
    fn test_de_front_3() {
        assert_eq!(de_front("away"), de_front_solution("away"))
    }
    #[test]
    fn test_de_front_4() {
        assert_eq!(de_front("axy"), de_front_solution("axy"))
    }
    #[test]
    fn test_de_front_5() {
        assert_eq!(de_front("abc"), de_front_solution("abc"))
    }
    #[test]
    fn test_de_front_6() {
        assert_eq!(de_front("xby"), de_front_solution("xby"))
    }
    #[test]
    fn test_de_front_7() {
        assert_eq!(de_front("ab"), de_front_solution("ab"))
    }
    #[test]
    fn test_de_front_8() {
        assert_eq!(de_front("ax"), de_front_solution("ax"))
    }
    #[test]
    fn test_de_front_9() {
        assert_eq!(de_front("axb"), de_front_solution("axb"))
    }
    #[test]
    fn test_de_front_10() {
        assert_eq!(de_front("aaa"), de_front_solution("aaa"))
    }
    #[test]
    fn test_de_front_11() {
        assert_eq!(de_front("xbc"), de_front_solution("xbc"))
    }
    #[test]
    fn test_de_front_12() {
        assert_eq!(de_front("bbb"), de_front_solution("bbb"))
    }
    #[test]
    fn test_de_front_13() {
        assert_eq!(de_front("bazz"), de_front_solution("bazz"))
    }
    #[test]
    fn test_de_front_14() {
        assert_eq!(de_front("ba"), de_front_solution("ba"))
    }
    #[test]
    fn test_de_front_15() {
        assert_eq!(de_front("abxyz"), de_front_solution("abxyz"))
    }
    #[test]
    fn test_de_front_16() {
        assert_eq!(de_front("hi"), de_front_solution("hi"))
    }
    #[test]
    fn test_de_front_17() {
        assert_eq!(de_front("his"), de_front_solution("his"))
    }
    #[test]
    fn test_de_front_18() {
        assert_eq!(de_front("xz"), de_front_solution("xz"))
    }
    #[test]
    fn test_de_front_19() {
        assert_eq!(de_front("zzz"), de_front_solution("zzz"))
    }
}

#[cfg(test)]
mod test_start_word {
    use super::implementations::start_word;
    use super::solutions::start_word_solution;

    #[test]
    fn test_start_word_1() {
        assert_eq!(start_word("hippo", "hi"), start_word_solution("hippo", "hi"))
    }
    #[test]
    fn test_start_word_2() {
        assert_eq!(start_word("hippo", "xip"), start_word_solution("hippo", "xip"))
    }
    #[test]
    fn test_start_word_3() {
        assert_eq!(start_word("hippo", "i"), start_word_solution("hippo", "i"))
    }
    #[test]
    fn test_start_word_4() {
        assert_eq!(start_word("hippo", "ix"), start_word_solution("hippo", "ix"))
    }
    #[test]
    fn test_start_word_5() {
        assert_eq!(start_word("h", "ix"), start_word_solution("h", "ix"))
    }
    #[test]
    fn test_start_word_6() {
        assert_eq!(start_word("", "i"), start_word_solution("", "i"))
    }
    #[test]
    fn test_start_word_7() {
        assert_eq!(start_word("hip", "zi"), start_word_solution("hip", "zi"))
    }
    #[test]
    fn test_start_word_8() {
        assert_eq!(start_word("hip", "zip"), start_word_solution("hip", "zip"))
    }
    #[test]
    fn test_start_word_9() {
        assert_eq!(start_word("hip", "zig"), start_word_solution("hip", "zig"))
    }
    #[test]
    fn test_start_word_10() {
        assert_eq!(start_word("h", "z"), start_word_solution("h", "z"))
    }
    #[test]
    fn test_start_word_11() {
        assert_eq!(start_word("hippo", "xippo"), start_word_solution("hippo", "xippo"))
    }
    #[test]
    fn test_start_word_12() {
        assert_eq!(start_word("hippo", "xyz"), start_word_solution("hippo", "xyz"))
    }
    #[test]
    fn test_start_word_13() {
        assert_eq!(start_word("hippo", "hip"), start_word_solution("hippo", "hip"))
    }
    #[test]
    fn test_start_word_14() {
        assert_eq!(start_word("kitten", "cit"), start_word_solution("kitten", "cit"))
    }
    #[test]
    fn test_start_word_15() {
        assert_eq!(start_word("kit", "cit"), start_word_solution("kit", "cit"))
    }
}

#[cfg(test)]
mod test_without_x {
    use super::implementations::without_x;
    use super::solutions::without_x_solution;

    #[test]
    fn test_without_x_1() {
        assert_eq!(without_x("xHix"), without_x_solution("xHix"))
    }
    #[test]
    fn test_without_x_2() {
        assert_eq!(without_x("xHi"), without_x_solution("xHi"))
    }
    #[test]
    fn test_without_x_3() {
        assert_eq!(without_x("Hxix"), without_x_solution("Hxix"))
    }
    #[test]
    fn test_without_x_4() {
        assert_eq!(without_x("Hi"), without_x_solution("Hi"))
    }
    #[test]
    fn test_without_x_5() {
        assert_eq!(without_x("xxHi"), without_x_solution("xxHi"))
    }
    #[test]
    fn test_without_x_6() {
        assert_eq!(without_x("Hix"), without_x_solution("Hix"))
    }
    #[test]
    fn test_without_x_7() {
        assert_eq!(without_x("xaxbx"), without_x_solution("xaxbx"))
    }
    #[test]
    fn test_without_x_8() {
        assert_eq!(without_x("xx"), without_x_solution("xx"))
    }
    #[test]
    fn test_without_x_9() {
        assert_eq!(without_x("x"), without_x_solution("x"))
    }
    #[test]
    fn test_without_x_10() {
        assert_eq!(without_x(""), without_x_solution(""))
    }
    #[test]
    fn test_without_x_11() {
        assert_eq!(without_x("Hello"), without_x_solution("Hello"))
    }
    #[test]
    fn test_without_x_12() {
        assert_eq!(without_x("Hexllo"), without_x_solution("Hexllo"))
    }
}

#[cfg(test)]
mod test_without_x2 {
    use super::implementations::without_x2;
    use super::solutions::without_x2_solution;

    #[test]
    fn test_without_x2_1() {
        assert_eq!(without_x2("xHi"), without_x2_solution("xHi"))
    }
    #[test]
    fn test_without_x2_2() {
        assert_eq!(without_x2("Hxi"), without_x2_solution("Hxi"))
    }
    #[test]
    fn test_without_x2_3() {
        assert_eq!(without_x2("Hi"), without_x2_solution("Hi"))
    }
    #[test]
    fn test_without_x2_4() {
        assert_eq!(without_x2("xxHi"), without_x2_solution("xxHi"))
    }
    #[test]
    fn test_without_x2_5() {
        assert_eq!(without_x2("Hix"), without_x2_solution("Hix"))
    }
    #[test]
    fn test_without_x2_6() {
        assert_eq!(without_x2("xaxb"), without_x2_solution("xaxb"))
    }
    #[test]
    fn test_without_x2_7() {
        assert_eq!(without_x2("xx"), without_x2_solution("xx"))
    }
    #[test]
    fn test_without_x2_8() {
        assert_eq!(without_x2("x"), without_x2_solution("x"))
    }
    #[test]
    fn test_without_x2_9() {
        assert_eq!(without_x2(""), without_x2_solution(""))
    }
    #[test]
    fn test_without_x2_10() {
        assert_eq!(without_x2("Hello"), without_x2_solution("Hello"))
    }
    #[test]
    fn test_without_x2_11() {
        assert_eq!(without_x2("Hexllo"), without_x2_solution("Hexllo"))
    }
    #[test]
    fn test_without_x2_12() {
        assert_eq!(without_x2("xHxllo"), without_x2_solution("xHxllo"))
    }
}

#[cfg(test)]
mod test_double_char {
    use super::implementations::double_char;
    use super::solutions::double_char_solution;

    #[test]
    fn test_double_char_1() {
        assert_eq!(double_char("The"), double_char_solution("The"))
    }
    #[test]
    fn test_double_char_2() {
        assert_eq!(double_char("AAbb"), double_char_solution("AAbb"))
    }
    #[test]
    fn test_double_char_3() {
        assert_eq!(double_char("Hi-There"), double_char_solution("Hi-There"))
    }
    #[test]
    fn test_double_char_4() {
        assert_eq!(double_char("Word!"), double_char_solution("Word!"))
    }
    #[test]
    fn test_double_char_5() {
        assert_eq!(double_char("!!"), double_char_solution("!!"))
    }
    #[test]
    fn test_double_char_6() {
        assert_eq!(double_char(""), double_char_solution(""))
    }
    #[test]
    fn test_double_char_7() {
        assert_eq!(double_char("a"), double_char_solution("a"))
    }
    #[test]
    fn test_double_char_8() {
        assert_eq!(double_char("."), double_char_solution("."))
    }
    #[test]
    fn test_double_char_9() {
        assert_eq!(double_char("aa"), double_char_solution("aa"))
    }
}

#[cfg(test)]
mod test_count_hi {
    use super::implementations::count_hi;
    use super::solutions::count_hi_solution;

    #[test]
    fn test_count_hi_1() {
        assert_eq!(count_hi("abc hi ho"), count_hi_solution("abc hi ho"))
    }
    #[test]
    fn test_count_hi_2() {
        assert_eq!(count_hi("ABChi hi"), count_hi_solution("ABChi hi"))
    }
    #[test]
    fn test_count_hi_3() {
        assert_eq!(count_hi("hihi"), count_hi_solution("hihi"))
    }
    #[test]
    fn test_count_hi_4() {
        assert_eq!(count_hi("hiHIhi"), count_hi_solution("hiHIhi"))
    }
    #[test]
    fn test_count_hi_5() {
        assert_eq!(count_hi(""), count_hi_solution(""))
    }
    #[test]
    fn test_count_hi_6() {
        assert_eq!(count_hi("h"), count_hi_solution("h"))
    }
    #[test]
    fn test_count_hi_7() {
        assert_eq!(count_hi("hi"), count_hi_solution("hi"))
    }
    #[test]
    fn test_count_hi_8() {
        assert_eq!(count_hi("Hi is no HI on ahI"), count_hi_solution("Hi is no HI on ahI"))
    }
    #[test]
    fn test_count_hi_9() {
        assert_eq!(count_hi("hiho not HOHIhi"), count_hi_solution("hiho not HOHIhi"))
    }
}

#[cfg(test)]
mod test_cat_dog {
    use super::implementations::cat_dog;
    use super::solutions::cat_dog_solution;

    #[test]
    fn test_cat_dog_1() {
        assert_eq!(cat_dog("catdog"), cat_dog_solution("catdog"))
    }
    #[test]
    fn test_cat_dog_2() {
        assert_eq!(cat_dog("catcat"), cat_dog_solution("catcat"))
    }
    #[test]
    fn test_cat_dog_3() {
        assert_eq!(cat_dog("1cat1cadodog"), cat_dog_solution("1cat1cadodog"))
    }
    #[test]
    fn test_cat_dog_4() {
        assert_eq!(cat_dog("catxxdogxxxdog"), cat_dog_solution("catxxdogxxxdog"))
    }
    #[test]
    fn test_cat_dog_5() {
        assert_eq!(cat_dog("catxdogxdogxcat"), cat_dog_solution("catxdogxdogxcat"))
    }
    #[test]
    fn test_cat_dog_6() {
        assert_eq!(cat_dog("catxdogxdogxca"), cat_dog_solution("catxdogxdogxca"))
    }
    #[test]
    fn test_cat_dog_7() {
        assert_eq!(cat_dog("dogdogcat"), cat_dog_solution("dogdogcat"))
    }
    #[test]
    fn test_cat_dog_8() {
        assert_eq!(cat_dog("dogogcat"), cat_dog_solution("dogogcat"))
    }
    #[test]
    fn test_cat_dog_9() {
        assert_eq!(cat_dog("dog"), cat_dog_solution("dog"))
    }
    #[test]
    fn test_cat_dog_10() {
        assert_eq!(cat_dog("cat"), cat_dog_solution("cat"))
    }
    #[test]
    fn test_cat_dog_11() {
        assert_eq!(cat_dog("ca"), cat_dog_solution("ca"))
    }
    #[test]
    fn test_cat_dog_12() {
        assert_eq!(cat_dog("c"), cat_dog_solution("c"))
    }
    #[test]
    fn test_cat_dog_13() {
        assert_eq!(cat_dog(""), cat_dog_solution(""))
    }
}

#[cfg(test)]
mod test_count_code {
    use super::implementations::count_code;
    use super::solutions::count_code_solution;

    #[test]
    fn test_count_code_1() {
        assert_eq!(count_code("aaacodebbb"), count_code_solution("aaacodebbb"))
    }
    #[test]
    fn test_count_code_2() {
        assert_eq!(count_code("codexxcode"), count_code_solution("codexxcode"))
    }
    #[test]
    fn test_count_code_3() {
        assert_eq!(count_code("cozexxcope"), count_code_solution("cozexxcope"))
    }
    #[test]
    fn test_count_code_4() {
        assert_eq!(count_code("cozfxxcope"), count_code_solution("cozfxxcope"))
    }
    #[test]
    fn test_count_code_5() {
        assert_eq!(count_code("xxcozeyycop"), count_code_solution("xxcozeyycop"))
    }
    #[test]
    fn test_count_code_6() {
        assert_eq!(count_code("cozcop"), count_code_solution("cozcop"))
    }
    #[test]
    fn test_count_code_7() {
        assert_eq!(count_code("abcxyz"), count_code_solution("abcxyz"))
    }
    #[test]
    fn test_count_code_8() {
        assert_eq!(count_code("code"), count_code_solution("code"))
    }
    #[test]
    fn test_count_code_9() {
        assert_eq!(count_code("ode"), count_code_solution("ode"))
    }
    #[test]
    fn test_count_code_10() {
        assert_eq!(count_code("c"), count_code_solution("c"))
    }
    #[test]
    fn test_count_code_11() {
        assert_eq!(count_code(""), count_code_solution(""))
    }
    #[test]
    fn test_count_code_12() {
        assert_eq!(count_code("AAcodeBBcoleCCccoreDD"), count_code_solution("AAcodeBBcoleCCccoreDD"))
    }
    #[test]
    fn test_count_code_13() {
        assert_eq!(count_code("AAcodeBBcoleCCccorfDD"), count_code_solution("AAcodeBBcoleCCccorfDD"))
    }
    #[test]
    fn test_count_code_14() {
        assert_eq!(count_code("coAcodeBcoleccoreDD"), count_code_solution("coAcodeBcoleccoreDD"))
    }
}

#[cfg(test)]
mod test_end_other {
    use super::implementations::end_other;
    use super::solutions::end_other_solution;

    #[test]
    fn test_end_other_1() {
        assert_eq!(end_other("Hiabc", "abc"), end_other_solution("Hiabc", "abc"))
    }
    #[test]
    fn test_end_other_2() {
        assert_eq!(end_other("AbC", "HiaBc"), end_other_solution("AbC", "HiaBc"))
    }
    #[test]
    fn test_end_other_3() {
        assert_eq!(end_other("abc", "abXabc"), end_other_solution("abc", "abXabc"))
    }
    #[test]
    fn test_end_other_4() {
        assert_eq!(end_other("Hiabc", "abcd"), end_other_solution("Hiabc", "abcd"))
    }
    #[test]
    fn test_end_other_5() {
        assert_eq!(end_other("Hiabc", "bc"), end_other_solution("Hiabc", "bc"))
    }
    #[test]
    fn test_end_other_6() {
        assert_eq!(end_other("Hiabcx", "bc"), end_other_solution("Hiabcx", "bc"))
    }
    #[test]
    fn test_end_other_7() {
        assert_eq!(end_other("abc", "abc"), end_other_solution("abc", "abc"))
    }
    #[test]
    fn test_end_other_8() {
        assert_eq!(end_other("xyz", "12xyz"), end_other_solution("xyz", "12xyz"))
    }
    #[test]
    fn test_end_other_9() {
        assert_eq!(end_other("yz", "12xz"), end_other_solution("yz", "12xz"))
    }
    #[test]
    fn test_end_other_10() {
        assert_eq!(end_other("Z", "12xz"), end_other_solution("Z", "12xz"))
    }
    #[test]
    fn test_end_other_11() {
        assert_eq!(end_other("12", "12"), end_other_solution("12", "12"))
    }
    #[test]
    fn test_end_other_12() {
        assert_eq!(end_other("abcXYZ", "abcDEF"), end_other_solution("abcXYZ", "abcDEF"))
    }
    #[test]
    fn test_end_other_13() {
        assert_eq!(end_other("ab", "ab12"), end_other_solution("ab", "ab12"))
    }
    #[test]
    fn test_end_other_14() {
        assert_eq!(end_other("ab", "12ab"), end_other_solution("ab", "12ab"))
    }
}

#[cfg(test)]
mod test_xyz_there {
    use super::implementations::xyz_there;
    use super::solutions::xyz_there_solution;

    #[test]
    fn test_xyz_there_1() {
        assert_eq!(xyz_there("abcxyz"), xyz_there_solution("abcxyz"))
    }
    #[test]
    fn test_xyz_there_2() {
        assert_eq!(xyz_there("abc.xyz"), xyz_there_solution("abc.xyz"))
    }
    #[test]
    fn test_xyz_there_3() {
        assert_eq!(xyz_there("xyz.abc"), xyz_there_solution("xyz.abc"))
    }
    #[test]
    fn test_xyz_there_4() {
        assert_eq!(xyz_there("abcxy"), xyz_there_solution("abcxy"))
    }
    #[test]
    fn test_xyz_there_5() {
        assert_eq!(xyz_there("xyz"), xyz_there_solution("xyz"))
    }
    #[test]
    fn test_xyz_there_6() {
        assert_eq!(xyz_there("xy"), xyz_there_solution("xy"))
    }
    #[test]
    fn test_xyz_there_7() {
        assert_eq!(xyz_there("x"), xyz_there_solution("x"))
    }
    #[test]
    fn test_xyz_there_8() {
        assert_eq!(xyz_there(""), xyz_there_solution(""))
    }
    #[test]
    fn test_xyz_there_9() {
        assert_eq!(xyz_there("abc.xyzxyz"), xyz_there_solution("abc.xyzxyz"))
    }
    #[test]
    fn test_xyz_there_10() {
        assert_eq!(xyz_there("abc.xxyz"), xyz_there_solution("abc.xxyz"))
    }
    #[test]
    fn test_xyz_there_11() {
        assert_eq!(xyz_there(".xyz"), xyz_there_solution(".xyz"))
    }
    #[test]
    fn test_xyz_there_12() {
        assert_eq!(xyz_there("12.xyz"), xyz_there_solution("12.xyz"))
    }
    #[test]
    fn test_xyz_there_13() {
        assert_eq!(xyz_there("12xyz"), xyz_there_solution("12xyz"))
    }
    #[test]
    fn test_xyz_there_14() {
        assert_eq!(xyz_there("1.xyz.xyz2.xyz"), xyz_there_solution("1.xyz.xyz2.xyz"))
    }
}

#[cfg(test)]
mod test_bob_there {
    use super::implementations::bob_there;
    use super::solutions::bob_there_solution;

    #[test]
    fn test_bob_there_1() {
        assert_eq!(bob_there("abcbob"), bob_there_solution("abcbob"))
    }
    #[test]
    fn test_bob_there_2() {
        assert_eq!(bob_there("b9b"), bob_there_solution("b9b"))
    }
    #[test]
    fn test_bob_there_3() {
        assert_eq!(bob_there("bac"), bob_there_solution("bac"))
    }
    #[test]
    fn test_bob_there_4() {
        assert_eq!(bob_there("bbb"), bob_there_solution("bbb"))
    }
    #[test]
    fn test_bob_there_5() {
        assert_eq!(bob_there("abcdefb"), bob_there_solution("abcdefb"))
    }
    #[test]
    fn test_bob_there_6() {
        assert_eq!(bob_there("123abcbcdbabxyz"), bob_there_solution("123abcbcdbabxyz"))
    }
    #[test]
    fn test_bob_there_7() {
        assert_eq!(bob_there("b12"), bob_there_solution("b12"))
    }
    #[test]
    fn test_bob_there_8() {
        assert_eq!(bob_there("b1b"), bob_there_solution("b1b"))
    }
    #[test]
    fn test_bob_there_9() {
        assert_eq!(bob_there("b12b1b"), bob_there_solution("b12b1b"))
    }
    #[test]
    fn test_bob_there_10() {
        assert_eq!(bob_there("bbc"), bob_there_solution("bbc"))
    }
    #[test]
    fn test_bob_there_11() {
        assert_eq!(bob_there("bbb"), bob_there_solution("bbb"))
    }
    #[test]
    fn test_bob_there_12() {
        assert_eq!(bob_there("bb"), bob_there_solution("bb"))
    }
    #[test]
    fn test_bob_there_13() {
        assert_eq!(bob_there("b"), bob_there_solution("b"))
    }
}

#[cfg(test)]
mod test_xy_balance {
    use super::implementations::xy_balance;
    use super::solutions::xy_balance_solution;

    #[test]
    fn test_xy_balance_1() {
        assert_eq!(xy_balance("aaxbby"), xy_balance_solution("aaxbby"))
    }
    #[test]
    fn test_xy_balance_2() {
        assert_eq!(xy_balance("aaxbb"), xy_balance_solution("aaxbb"))
    }
    #[test]
    fn test_xy_balance_3() {
        assert_eq!(xy_balance("yaaxbb"), xy_balance_solution("yaaxbb"))
    }
    #[test]
    fn test_xy_balance_4() {
        assert_eq!(xy_balance("yaaxbby"), xy_balance_solution("yaaxbby"))
    }
    #[test]
    fn test_xy_balance_5() {
        assert_eq!(xy_balance("xaxxbby"), xy_balance_solution("xaxxbby"))
    }
    #[test]
    fn test_xy_balance_6() {
        assert_eq!(xy_balance("xaxxbbyx"), xy_balance_solution("xaxxbbyx"))
    }
    #[test]
    fn test_xy_balance_7() {
        assert_eq!(xy_balance("xxbxy"), xy_balance_solution("xxbxy"))
    }
    #[test]
    fn test_xy_balance_8() {
        assert_eq!(xy_balance("xxbx"), xy_balance_solution("xxbx"))
    }
    #[test]
    fn test_xy_balance_9() {
        assert_eq!(xy_balance("bbb"), xy_balance_solution("bbb"))
    }
    #[test]
    fn test_xy_balance_10() {
        assert_eq!(xy_balance("bxbb"), xy_balance_solution("bxbb"))
    }
    #[test]
    fn test_xy_balance_11() {
        assert_eq!(xy_balance("bxyb"), xy_balance_solution("bxyb"))
    }
    #[test]
    fn test_xy_balance_12() {
        assert_eq!(xy_balance("xy"), xy_balance_solution("xy"))
    }
    #[test]
    fn test_xy_balance_13() {
        assert_eq!(xy_balance("y"), xy_balance_solution("y"))
    }
    #[test]
    fn test_xy_balance_14() {
        assert_eq!(xy_balance("x"), xy_balance_solution("x"))
    }
    #[test]
    fn test_xy_balance_15() {
        assert_eq!(xy_balance(""), xy_balance_solution(""))
    }
    #[test]
    fn test_xy_balance_16() {
        assert_eq!(xy_balance("yxyxyxyx"), xy_balance_solution("yxyxyxyx"))
    }
    #[test]
    fn test_xy_balance_17() {
        assert_eq!(xy_balance("yxyxyxyxy"), xy_balance_solution("yxyxyxyxy"))
    }
    #[test]
    fn test_xy_balance_18() {
        assert_eq!(xy_balance("12xabxxydxyxyzz"), xy_balance_solution("12xabxxydxyxyzz"))
    }
}

#[cfg(test)]
mod test_mix_string {
    use super::implementations::mix_string;
    use super::solutions::mix_string_solution;

    #[test]
    fn test_mix_string_1() {
        assert_eq!(mix_string("abc", "xyz"), mix_string_solution("abc", "xyz"))
    }
    #[test]
    fn test_mix_string_2() {
        assert_eq!(mix_string("Hi", "There"), mix_string_solution("Hi", "There"))
    }
    #[test]
    fn test_mix_string_3() {
        assert_eq!(mix_string("xxxx", "There"), mix_string_solution("xxxx", "There"))
    }
    #[test]
    fn test_mix_string_4() {
        assert_eq!(mix_string("xxx", "X"), mix_string_solution("xxx", "X"))
    }
    #[test]
    fn test_mix_string_5() {
        assert_eq!(mix_string("2/", "27 around"), mix_string_solution("2/", "27 around"))
    }
    #[test]
    fn test_mix_string_6() {
        assert_eq!(mix_string("", "Hello"), mix_string_solution("", "Hello"))
    }
    #[test]
    fn test_mix_string_7() {
        assert_eq!(mix_string("Abc", ""), mix_string_solution("Abc", ""))
    }
    #[test]
    fn test_mix_string_8() {
        assert_eq!(mix_string("", ""), mix_string_solution("", ""))
    }
    #[test]
    fn test_mix_string_9() {
        assert_eq!(mix_string("a", "b"), mix_string_solution("a", "b"))
    }
    #[test]
    fn test_mix_string_10() {
        assert_eq!(mix_string("ax", "b"), mix_string_solution("ax", "b"))
    }
    #[test]
    fn test_mix_string_11() {
        assert_eq!(mix_string("a", "bx"), mix_string_solution("a", "bx"))
    }
    #[test]
    fn test_mix_string_12() {
        assert_eq!(mix_string("So", "Long"), mix_string_solution("So", "Long"))
    }
    #[test]
    fn test_mix_string_13() {
        assert_eq!(mix_string("Long", "So"), mix_string_solution("Long", "So"))
    }
}

#[cfg(test)]
mod test_repeat_end {
    use super::implementations::repeat_end;
    use super::solutions::repeat_end_solution;

    #[test]
    fn test_repeat_end_1() {
        assert_eq!(repeat_end("Hello", 3), repeat_end_solution("Hello", 3))
    }
    #[test]
    fn test_repeat_end_2() {
        assert_eq!(repeat_end("Hello", 2), repeat_end_solution("Hello", 2))
    }
    #[test]
    fn test_repeat_end_3() {
        assert_eq!(repeat_end("Hello", 1), repeat_end_solution("Hello", 1))
    }
    #[test]
    fn test_repeat_end_4() {
        assert_eq!(repeat_end("Hello", 0), repeat_end_solution("Hello", 0))
    }
    #[test]
    fn test_repeat_end_5() {
        assert_eq!(repeat_end("abc", 3), repeat_end_solution("abc", 3))
    }
    #[test]
    fn test_repeat_end_6() {
        assert_eq!(repeat_end("1234", 2), repeat_end_solution("1234", 2))
    }
    #[test]
    fn test_repeat_end_7() {
        assert_eq!(repeat_end("1234", 3), repeat_end_solution("1234", 3))
    }
    #[test]
    fn test_repeat_end_8() {
        assert_eq!(repeat_end("", 0), repeat_end_solution("", 0))
    }
}

#[cfg(test)]
mod test_repeat_front {
    use super::implementations::repeat_front;
    use super::solutions::repeat_front_solution;

    #[test]
    fn test_repeat_front_1() {
        assert_eq!(repeat_front("Chocolate", 4), repeat_front_solution("Chocolate", 4))
    }
    #[test]
    fn test_repeat_front_2() {
        assert_eq!(repeat_front("Chocolate", 3), repeat_front_solution("Chocolate", 3))
    }
    #[test]
    fn test_repeat_front_3() {
        assert_eq!(repeat_front("Ice Cream", 2), repeat_front_solution("Ice Cream", 2))
    }
    #[test]
    fn test_repeat_front_4() {
        assert_eq!(repeat_front("Ice Cream", 1), repeat_front_solution("Ice Cream", 1))
    }
    #[test]
    fn test_repeat_front_5() {
        assert_eq!(repeat_front("Ice Cream", 0), repeat_front_solution("Ice Cream", 0))
    }
    #[test]
    fn test_repeat_front_6() {
        assert_eq!(repeat_front("xyz", 3), repeat_front_solution("xyz", 3))
    }
    #[test]
    fn test_repeat_front_7() {
        assert_eq!(repeat_front("", 0), repeat_front_solution("", 0))
    }
    #[test]
    fn test_repeat_front_8() {
        assert_eq!(repeat_front("Java", 4), repeat_front_solution("Java", 4))
    }
    #[test]
    fn test_repeat_front_9() {
        assert_eq!(repeat_front("Java", 1), repeat_front_solution("Java", 1))
    }
}

#[cfg(test)]
mod test_repeat_separator {
    use super::implementations::repeat_separator;
    use super::solutions::repeat_separator_solution;

    #[test]
    fn test_repeat_separator_1() {
        assert_eq!(repeat_separator("Word", "X", 3), repeat_separator_solution("Word", "X", 3))
    }
    #[test]
    fn test_repeat_separator_2() {
        assert_eq!(repeat_separator("This", "And", 2), repeat_separator_solution("This", "And", 2))
    }
    #[test]
    fn test_repeat_separator_3() {
        assert_eq!(repeat_separator("This", "And", 1), repeat_separator_solution("This", "And", 1))
    }
    #[test]
    fn test_repeat_separator_4() {
        assert_eq!(repeat_separator("Hi", "-n-", 2), repeat_separator_solution("Hi", "-n-", 2))
    }
    #[test]
    fn test_repeat_separator_5() {
        assert_eq!(repeat_separator("AAA", "", 1), repeat_separator_solution("AAA", "", 1))
    }
    #[test]
    fn test_repeat_separator_6() {
        assert_eq!(repeat_separator("AAA", "", 0), repeat_separator_solution("AAA", "", 0))
    }
    #[test]
    fn test_repeat_separator_7() {
        assert_eq!(repeat_separator("A", "B", 5), repeat_separator_solution("A", "B", 5))
    }
    #[test]
    fn test_repeat_separator_8() {
        assert_eq!(repeat_separator("abc", "XX", 3), repeat_separator_solution("abc", "XX", 3))
    }
    #[test]
    fn test_repeat_separator_9() {
        assert_eq!(repeat_separator("abc", "XX", 2), repeat_separator_solution("abc", "XX", 2))
    }
    #[test]
    fn test_repeat_separator_10() {
        assert_eq!(repeat_separator("abc", "XX", 1), repeat_separator_solution("abc", "XX", 1))
    }
    #[test]
    fn test_repeat_separator_11() {
        assert_eq!(repeat_separator("XYZ", "a", 2), repeat_separator_solution("XYZ", "a", 2))
    }
}

#[cfg(test)]
mod test_prefix_again {
    use super::implementations::prefix_again;
    use super::solutions::prefix_again_solution;

    #[test]
    fn test_prefix_again_1() {
        assert_eq!(prefix_again("abXYabc", 1), prefix_again_solution("abXYabc", 1))
    }
    #[test]
    fn test_prefix_again_2() {
        assert_eq!(prefix_again("abXYabc", 2), prefix_again_solution("abXYabc", 2))
    }
    #[test]
    fn test_prefix_again_3() {
        assert_eq!(prefix_again("abXYabc", 3), prefix_again_solution("abXYabc", 3))
    }
    #[test]
    fn test_prefix_again_4() {
        assert_eq!(prefix_again("xyzxyxyxy", 2), prefix_again_solution("xyzxyxyxy", 2))
    }
    #[test]
    fn test_prefix_again_5() {
        assert_eq!(prefix_again("xyzxyxyxy", 3), prefix_again_solution("xyzxyxyxy", 3))
    }
    #[test]
    fn test_prefix_again_6() {
        assert_eq!(prefix_again("Hi12345Hi6789Hi10", 1), prefix_again_solution("Hi12345Hi6789Hi10", 1))
    }
    #[test]
    fn test_prefix_again_7() {
        assert_eq!(prefix_again("Hi12345Hi6789Hi10", 2), prefix_again_solution("Hi12345Hi6789Hi10", 2))
    }
    #[test]
    fn test_prefix_again_8() {
        assert_eq!(prefix_again("Hi12345Hi6789Hi10", 3), prefix_again_solution("Hi12345Hi6789Hi10", 3))
    }
    #[test]
    fn test_prefix_again_9() {
        assert_eq!(prefix_again("Hi12345Hi6789Hi10", 4), prefix_again_solution("Hi12345Hi6789Hi10", 4))
    }
    #[test]
    fn test_prefix_again_10() {
        assert_eq!(prefix_again("a", 1), prefix_again_solution("a", 1))
    }
    #[test]
    fn test_prefix_again_11() {
        assert_eq!(prefix_again("aa", 1), prefix_again_solution("aa", 1))
    }
    #[test]
    fn test_prefix_again_12() {
        assert_eq!(prefix_again("ab", 1), prefix_again_solution("ab", 1))
    }
}

#[cfg(test)]
mod test_xyz_middle {
    use super::implementations::xyz_middle;
    use super::solutions::xyz_middle_solution;

    #[test]
    fn test_xyz_middle_1() {
        assert_eq!(xyz_middle("AAxyzBB"), xyz_middle_solution("AAxyzBB"))
    }
    #[test]
    fn test_xyz_middle_2() {
        assert_eq!(xyz_middle("AxyzBB"), xyz_middle_solution("AxyzBB"))
    }
    #[test]
    fn test_xyz_middle_3() {
        assert_eq!(xyz_middle("AxyzBBB"), xyz_middle_solution("AxyzBBB"))
    }
    #[test]
    fn test_xyz_middle_4() {
        assert_eq!(xyz_middle("AxyzBBBB"), xyz_middle_solution("AxyzBBBB"))
    }
    #[test]
    fn test_xyz_middle_5() {
        assert_eq!(xyz_middle("AAAxyzB"), xyz_middle_solution("AAAxyzB"))
    }
    #[test]
    fn test_xyz_middle_6() {
        assert_eq!(xyz_middle("AAAxyzBB"), xyz_middle_solution("AAAxyzBB"))
    }
    #[test]
    fn test_xyz_middle_7() {
        assert_eq!(xyz_middle("AAAAxyzBB"), xyz_middle_solution("AAAAxyzBB"))
    }
    #[test]
    fn test_xyz_middle_8() {
        assert_eq!(xyz_middle("AAAAAxyzBBB"), xyz_middle_solution("AAAAAxyzBBB"))
    }
    #[test]
    fn test_xyz_middle_9() {
        assert_eq!(xyz_middle("1x345xyz12x4"), xyz_middle_solution("1x345xyz12x4"))
    }
    #[test]
    fn test_xyz_middle_10() {
        assert_eq!(xyz_middle("xyzAxyzBBB"), xyz_middle_solution("xyzAxyzBBB"))
    }
    #[test]
    fn test_xyz_middle_11() {
        assert_eq!(xyz_middle("xyzAxyzBxyz"), xyz_middle_solution("xyzAxyzBxyz"))
    }
    #[test]
    fn test_xyz_middle_12() {
        assert_eq!(xyz_middle("xyzxyzAxyzBxyzxyz"), xyz_middle_solution("xyzxyzAxyzBxyzxyz"))
    }
    #[test]
    fn test_xyz_middle_13() {
        assert_eq!(xyz_middle("xyzxyzxyzBxyzxyz"), xyz_middle_solution("xyzxyzxyzBxyzxyz"))
    }
    #[test]
    fn test_xyz_middle_14() {
        assert_eq!(xyz_middle("xyzxyzAxyzxyzxyz"), xyz_middle_solution("xyzxyzAxyzxyzxyz"))
    }
    #[test]
    fn test_xyz_middle_15() {
        assert_eq!(xyz_middle("xyzxyzAxyzxyzxy"), xyz_middle_solution("xyzxyzAxyzxyzxy"))
    }
    #[test]
    fn test_xyz_middle_16() {
        assert_eq!(xyz_middle("AxyzxyzBB"), xyz_middle_solution("AxyzxyzBB"))
    }
    #[test]
    fn test_xyz_middle_17() {
        assert_eq!(xyz_middle(""), xyz_middle_solution(""))
    }
    #[test]
    fn test_xyz_middle_18() {
        assert_eq!(xyz_middle("x"), xyz_middle_solution("x"))
    }
    #[test]
    fn test_xyz_middle_19() {
        assert_eq!(xyz_middle("xy"), xyz_middle_solution("xy"))
    }
    #[test]
    fn test_xyz_middle_20() {
        assert_eq!(xyz_middle("xyz"), xyz_middle_solution("xyz"))
    }
    #[test]
    fn test_xyz_middle_21() {
        assert_eq!(xyz_middle("xyzz"), xyz_middle_solution("xyzz"))
    }
}

#[cfg(test)]
mod test_get_sandwich {
    use super::implementations::get_sandwich;
    use super::solutions::get_sandwich_solution;

    #[test]
    fn test_get_sandwich_1() {
        assert_eq!(get_sandwich("breadjambread"), get_sandwich_solution("breadjambread"))
    }
    #[test]
    fn test_get_sandwich_2() {
        assert_eq!(get_sandwich("xxbreadjambreadyy"), get_sandwich_solution("xxbreadjambreadyy"))
    }
    #[test]
    fn test_get_sandwich_3() {
        assert_eq!(get_sandwich("xxbreadyy"), get_sandwich_solution("xxbreadyy"))
    }
    #[test]
    fn test_get_sandwich_4() {
        assert_eq!(get_sandwich("xxbreadbreadjambreadyy"), get_sandwich_solution("xxbreadbreadjambreadyy"))
    }
    #[test]
    fn test_get_sandwich_5() {
        assert_eq!(get_sandwich("breadAbread"), get_sandwich_solution("breadAbread"))
    }
    #[test]
    fn test_get_sandwich_6() {
        assert_eq!(get_sandwich("breadbread"), get_sandwich_solution("breadbread"))
    }
    #[test]
    fn test_get_sandwich_7() {
        assert_eq!(get_sandwich("abcbreaz"), get_sandwich_solution("abcbreaz"))
    }
    #[test]
    fn test_get_sandwich_8() {
        assert_eq!(get_sandwich("xyz"), get_sandwich_solution("xyz"))
    }
    #[test]
    fn test_get_sandwich_9() {
        assert_eq!(get_sandwich(""), get_sandwich_solution(""))
    }
    #[test]
    fn test_get_sandwich_10() {
        assert_eq!(get_sandwich("breadbreaxbread"), get_sandwich_solution("breadbreaxbread"))
    }
    #[test]
    fn test_get_sandwich_11() {
        assert_eq!(get_sandwich("breaxbreadybread"), get_sandwich_solution("breaxbreadybread"))
    }
    #[test]
    fn test_get_sandwich_12() {
        assert_eq!(get_sandwich("breadbreadbreadbread"), get_sandwich_solution("breadbreadbreadbread"))
    }
}

#[cfg(test)]
mod test_same_star_char {
    use super::implementations::same_star_char;
    use super::solutions::same_star_char_solution;

    #[test]
    fn test_same_star_char_1() {
        assert_eq!(same_star_char("xy*yzz"), same_star_char_solution("xy*yzz"))
    }
    #[test]
    fn test_same_star_char_2() {
        assert_eq!(same_star_char("xy*zzz"), same_star_char_solution("xy*zzz"))
    }
    #[test]
    fn test_same_star_char_3() {
        assert_eq!(same_star_char("*xa*az"), same_star_char_solution("*xa*az"))
    }
    #[test]
    fn test_same_star_char_4() {
        assert_eq!(same_star_char("*xa*bz"), same_star_char_solution("*xa*bz"))
    }
    #[test]
    fn test_same_star_char_5() {
        assert_eq!(same_star_char("*xa*a*"), same_star_char_solution("*xa*a*"))
    }
    #[test]
    fn test_same_star_char_6() {
        assert_eq!(same_star_char(""), same_star_char_solution(""))
    }
    #[test]
    fn test_same_star_char_7() {
        assert_eq!(same_star_char("*xa*a*a"), same_star_char_solution("*xa*a*a"))
    }
    #[test]
    fn test_same_star_char_8() {
        assert_eq!(same_star_char("*xa*a*b"), same_star_char_solution("*xa*a*b"))
    }
    #[test]
    fn test_same_star_char_9() {
        assert_eq!(same_star_char("*12*2*2"), same_star_char_solution("*12*2*2"))
    }
    #[test]
    fn test_same_star_char_10() {
        assert_eq!(same_star_char("12*2*3*"), same_star_char_solution("12*2*3*"))
    }
    #[test]
    fn test_same_star_char_11() {
        assert_eq!(same_star_char("abcDEF"), same_star_char_solution("abcDEF"))
    }
    #[test]
    fn test_same_star_char_12() {
        assert_eq!(same_star_char("XY*YYYY*Z*"), same_star_char_solution("XY*YYYY*Z*"))
    }
    #[test]
    fn test_same_star_char_13() {
        assert_eq!(same_star_char("XY*YYYY*Y*"), same_star_char_solution("XY*YYYY*Y*"))
    }
    #[test]
    fn test_same_star_char_14() {
        assert_eq!(same_star_char("12*2*3*"), same_star_char_solution("12*2*3*"))
    }
    #[test]
    fn test_same_star_char_15() {
        assert_eq!(same_star_char("*"), same_star_char_solution("*"))
    }
    #[test]
    fn test_same_star_char_16() {
        assert_eq!(same_star_char("**"), same_star_char_solution("**"))
    }
}

#[cfg(test)]
mod test_one_two {
    use super::implementations::one_two;
    use super::solutions::one_two_solution;

    #[test]
    fn test_one_two_1() {
        assert_eq!(one_two("abc"), one_two_solution("abc"))
    }
    #[test]
    fn test_one_two_2() {
        assert_eq!(one_two("tca"), one_two_solution("tca"))
    }
    #[test]
    fn test_one_two_3() {
        assert_eq!(one_two("tcagdo"), one_two_solution("tcagdo"))
    }
    #[test]
    fn test_one_two_4() {
        assert_eq!(one_two("chocolate"), one_two_solution("chocolate"))
    }
    #[test]
    fn test_one_two_5() {
        assert_eq!(one_two("1234567890"), one_two_solution("1234567890"))
    }
    #[test]
    fn test_one_two_6() {
        assert_eq!(one_two("xabxabxabxabxabxabxab"), one_two_solution("xabxabxabxabxabxabxab"))
    }
    #[test]
    fn test_one_two_7() {
        assert_eq!(one_two("abcdefx"), one_two_solution("abcdefx"))
    }
    #[test]
    fn test_one_two_8() {
        assert_eq!(one_two("abcdefxy"), one_two_solution("abcdefxy"))
    }
    #[test]
    fn test_one_two_9() {
        assert_eq!(one_two("abcdefxyz"), one_two_solution("abcdefxyz"))
    }
    #[test]
    fn test_one_two_10() {
        assert_eq!(one_two(""), one_two_solution(""))
    }
    #[test]
    fn test_one_two_11() {
        assert_eq!(one_two("x"), one_two_solution("x"))
    }
    #[test]
    fn test_one_two_12() {
        assert_eq!(one_two("xy"), one_two_solution("xy"))
    }
    #[test]
    fn test_one_two_13() {
        assert_eq!(one_two("xyz"), one_two_solution("xyz"))
    }
    #[test]
    fn test_one_two_14() {
        assert_eq!(
            one_two("abcdefghijklkmnopqrstuvwxyz1234567890"),
            one_two_solution("abcdefghijklkmnopqrstuvwxyz1234567890")
        )
    }
    #[test]
    fn test_one_two_15() {
        assert_eq!(
            one_two("abcdefghijklkmnopqrstuvwxyz123456789"),
            one_two_solution("abcdefghijklkmnopqrstuvwxyz123456789")
        )
    }
    #[test]
    fn test_one_two_16() {
        assert_eq!(
            one_two("abcdefghijklkmnopqrstuvwxyz12345678"),
            one_two_solution("abcdefghijklkmnopqrstuvwxyz12345678")
        )
    }
}

#[cfg(test)]
mod test_zip_zap {
    use super::implementations::zip_zap;
    use super::solutions::zip_zap_solution;

    #[test]
    fn test_zip_zap_1() {
        assert_eq!(zip_zap("zipXzap"), zip_zap_solution("zipXzap"))
    }
    #[test]
    fn test_zip_zap_2() {
        assert_eq!(zip_zap("zopzop"), zip_zap_solution("zopzop"))
    }
    #[test]
    fn test_zip_zap_3() {
        assert_eq!(zip_zap("zzzopzop"), zip_zap_solution("zzzopzop"))
    }
    #[test]
    fn test_zip_zap_4() {
        assert_eq!(zip_zap("zibzap"), zip_zap_solution("zibzap"))
    }
    #[test]
    fn test_zip_zap_5() {
        assert_eq!(zip_zap("zip"), zip_zap_solution("zip"))
    }
    #[test]
    fn test_zip_zap_6() {
        assert_eq!(zip_zap("zi"), zip_zap_solution("zi"))
    }
    #[test]
    fn test_zip_zap_7() {
        assert_eq!(zip_zap("z"), zip_zap_solution("z"))
    }
    #[test]
    fn test_zip_zap_8() {
        assert_eq!(zip_zap(""), zip_zap_solution(""))
    }
    #[test]
    fn test_zip_zap_9() {
        assert_eq!(zip_zap("zzp"), zip_zap_solution("zzp"))
    }
    #[test]
    fn test_zip_zap_10() {
        assert_eq!(zip_zap("abcppp"), zip_zap_solution("abcppp"))
    }
    #[test]
    fn test_zip_zap_11() {
        assert_eq!(zip_zap("azbcppp"), zip_zap_solution("azbcppp"))
    }
    #[test]
    fn test_zip_zap_12() {
        assert_eq!(zip_zap("azbcpzpp"), zip_zap_solution("azbcpzpp"))
    }
}

#[cfg(test)]
mod test_star_out {
    use super::implementations::star_out;
    use super::solutions::star_out_solution;

    #[test]
    fn test_star_out_1() {
        assert_eq!(star_out("ab*cd"), star_out_solution("ab*cd"))
    }
    #[test]
    fn test_star_out_2() {
        assert_eq!(star_out("ab**cd"), star_out_solution("ab**cd"))
    }
    #[test]
    fn test_star_out_3() {
        assert_eq!(star_out("sm*eilly"), star_out_solution("sm*eilly"))
    }
    #[test]
    fn test_star_out_4() {
        assert_eq!(star_out("sm*eil*ly"), star_out_solution("sm*eil*ly"))
    }
    #[test]
    fn test_star_out_5() {
        assert_eq!(star_out("sm**eil*ly"), star_out_solution("sm**eil*ly"))
    }
    #[test]
    fn test_star_out_6() {
        assert_eq!(star_out("sm***eil*ly"), star_out_solution("sm***eil*ly"))
    }
    #[test]
    fn test_star_out_7() {
        assert_eq!(star_out("stringy*"), star_out_solution("stringy*"))
    }
    #[test]
    fn test_star_out_8() {
        assert_eq!(star_out("*stringy"), star_out_solution("*stringy"))
    }
    #[test]
    fn test_star_out_9() {
        assert_eq!(star_out("*str*in*gy"), star_out_solution("*str*in*gy"))
    }
    #[test]
    fn test_star_out_10() {
        assert_eq!(star_out("abc"), star_out_solution("abc"))
    }
    #[test]
    fn test_star_out_11() {
        assert_eq!(star_out("a*bc"), star_out_solution("a*bc"))
    }
    #[test]
    fn test_star_out_12() {
        assert_eq!(star_out("ab"), star_out_solution("ab"))
    }
    #[test]
    fn test_star_out_13() {
        assert_eq!(star_out("a*b"), star_out_solution("a*b"))
    }
    #[test]
    fn test_star_out_14() {
        assert_eq!(star_out("a"), star_out_solution("a"))
    }
    #[test]
    fn test_star_out_15() {
        assert_eq!(star_out("a*"), star_out_solution("a*"))
    }
    #[test]
    fn test_star_out_16() {
        assert_eq!(star_out("*a"), star_out_solution("*a"))
    }
    #[test]
    fn test_star_out_17() {
        assert_eq!(star_out("*"), star_out_solution("*"))
    }
    #[test]
    fn test_star_out_18() {
        assert_eq!(star_out(""), star_out_solution(""))
    }
}

#[cfg(test)]
mod test_plus_out {
    use super::implementations::plus_out;
    use super::solutions::plus_out_solution;

    #[test]
    fn test_plus_out_1() {
        assert_eq!(plus_out("12xy34", "xy"), plus_out_solution("12xy34", "xy"))
    }
    #[test]
    fn test_plus_out_2() {
        assert_eq!(plus_out("12xy34", "1"), plus_out_solution("12xy34", "1"))
    }
    #[test]
    fn test_plus_out_3() {
        assert_eq!(plus_out("12xy34xyabcxy", "xy"), plus_out_solution("12xy34xyabcxy", "xy"))
    }
    #[test]
    fn test_plus_out_4() {
        assert_eq!(plus_out("abXYabcXYZ", "ab"), plus_out_solution("abXYabcXYZ", "ab"))
    }
    #[test]
    fn test_plus_out_5() {
        assert_eq!(plus_out("abXYabcXYZ", "abc"), plus_out_solution("abXYabcXYZ", "abc"))
    }
    #[test]
    fn test_plus_out_6() {
        assert_eq!(plus_out("abXYabcXYZ", "XY"), plus_out_solution("abXYabcXYZ", "XY"))
    }
    #[test]
    fn test_plus_out_7() {
        assert_eq!(plus_out("abXYxyzXYZ", "XYZ"), plus_out_solution("abXYxyzXYZ", "XYZ"))
    }
    #[test]
    fn test_plus_out_8() {
        assert_eq!(plus_out("--++ab", "++"), plus_out_solution("--++ab", "++"))
    }
    #[test]
    fn test_plus_out_9() {
        assert_eq!(plus_out("aaxxxxbb", "xx"), plus_out_solution("aaxxxxbb", "xx"))
    }
    #[test]
    fn test_plus_out_10() {
        assert_eq!(plus_out("123123", "3"), plus_out_solution("123123", "3"))
    }
}

#[cfg(test)]
mod test_word_ends {
    use super::implementations::word_ends;
    use super::solutions::word_ends_solution;

    #[test]
    fn test_word_ends_1() {
        assert_eq!(word_ends("abcXY123XYijk", "XY"), word_ends_solution("abcXY123XYijk", "XY"))
    }
    #[test]
    fn test_word_ends_2() {
        assert_eq!(word_ends("XY123XY", "XY"), word_ends_solution("XY123XY", "XY"))
    }
    #[test]
    fn test_word_ends_3() {
        assert_eq!(word_ends("XY1XY", "XY"), word_ends_solution("XY1XY", "XY"))
    }
    #[test]
    fn test_word_ends_4() {
        assert_eq!(word_ends("XYXY", "XY"), word_ends_solution("XYXY", "XY"))
    }
    #[test]
    fn test_word_ends_5() {
        assert_eq!(word_ends("XY", "XY"), word_ends_solution("XY", "XY"))
    }
    #[test]
    fn test_word_ends_6() {
        assert_eq!(word_ends("Hi", "XY"), word_ends_solution("Hi", "XY"))
    }
    #[test]
    fn test_word_ends_7() {
        assert_eq!(word_ends("", "XY"), word_ends_solution("", "XY"))
    }
    #[test]
    fn test_word_ends_8() {
        assert_eq!(word_ends("abc1xyz1i1j", "1"), word_ends_solution("abc1xyz1i1j", "1"))
    }
    #[test]
    fn test_word_ends_9() {
        assert_eq!(word_ends("abc1xyz1", "1"), word_ends_solution("abc1xyz1", "1"))
    }
    #[test]
    fn test_word_ends_10() {
        assert_eq!(word_ends("abc1xyz11", "1"), word_ends_solution("abc1xyz11", "1"))
    }
    #[test]
    fn test_word_ends_11() {
        assert_eq!(word_ends("abc1xyz1abc", "abc"), word_ends_solution("abc1xyz1abc", "abc"))
    }
    #[test]
    fn test_word_ends_12() {
        assert_eq!(word_ends("abc1xyz1abc", "b"), word_ends_solution("abc1xyz1abc", "b"))
    }
    #[test]
    fn test_word_ends_13() {
        assert_eq!(word_ends("abc1abc1abc", "abc"), word_ends_solution("abc1abc1abc", "abc"))
    }
}

#[cfg(test)]
mod test_count_y_z {
    use super::implementations::count_y_z;
    use super::solutions::count_y_z_solution;

    #[test]
    fn test_count_y_z_1() {
        assert_eq!(count_y_z("fez day"), count_y_z_solution("fez day"))
    }
    #[test]
    fn test_count_y_z_2() {
        assert_eq!(count_y_z("day fez"), count_y_z_solution("day fez"))
    }
    #[test]
    fn test_count_y_z_3() {
        assert_eq!(count_y_z("day fyyyz"), count_y_z_solution("day fyyyz"))
    }
    #[test]
    fn test_count_y_z_4() {
        assert_eq!(count_y_z("day yak"), count_y_z_solution("day yak"))
    }
    #[test]
    fn test_count_y_z_5() {
        assert_eq!(count_y_z("day:yak"), count_y_z_solution("day:yak"))
    }
    #[test]
    fn test_count_y_z_6() {
        assert_eq!(count_y_z("!!day--yaz!!"), count_y_z_solution("!!day--yaz!!"))
    }
    #[test]
    fn test_count_y_z_7() {
        assert_eq!(count_y_z("yak zak"), count_y_z_solution("yak zak"))
    }
    #[test]
    fn test_count_y_z_8() {
        assert_eq!(count_y_z("DAY abc XYZ"), count_y_z_solution("DAY abc XYZ"))
    }
    #[test]
    fn test_count_y_z_9() {
        assert_eq!(count_y_z("aaz yyz my"), count_y_z_solution("aaz yyz my"))
    }
    #[test]
    fn test_count_y_z_10() {
        assert_eq!(count_y_z("y2bz"), count_y_z_solution("y2bz"))
    }
    #[test]
    fn test_count_y_z_11() {
        assert_eq!(count_y_z("zxyx"), count_y_z_solution("zxyx"))
    }
}

#[cfg(test)]
mod test_without_string {
    use super::implementations::without_string;
    use super::solutions::without_string_solution;

    #[test]
    fn test_without_string_1() {
        assert_eq!(without_string("Hello there", "llo"), without_string_solution("Hello there", "llo"))
    }
    #[test]
    fn test_without_string_2() {
        assert_eq!(without_string("Hello there", "e"), without_string_solution("Hello there", "e"))
    }
    #[test]
    fn test_without_string_3() {
        assert_eq!(without_string("Hello there", "x"), without_string_solution("Hello there", "x"))
    }
    #[test]
    fn test_without_string_4() {
        assert_eq!(without_string("This is a FISH", "IS"), without_string_solution("This is a FISH", "IS"))
    }
    #[test]
    fn test_without_string_5() {
        assert_eq!(without_string("THIS is a FISH", "is"), without_string_solution("THIS is a FISH", "is"))
    }
    #[test]
    fn test_without_string_6() {
        assert_eq!(without_string("THIS is a FISH", "iS"), without_string_solution("THIS is a FISH", "iS"))
    }
    #[test]
    fn test_without_string_7() {
        assert_eq!(without_string("abxxxxab", "xx"), without_string_solution("abxxxxab", "xx"))
    }
    #[test]
    fn test_without_string_8() {
        assert_eq!(without_string("abxxxab", "xx"), without_string_solution("abxxxab", "xx"))
    }
    #[test]
    fn test_without_string_9() {
        assert_eq!(without_string("abxxxab", "x"), without_string_solution("abxxxab", "x"))
    }
    #[test]
    fn test_without_string_10() {
        assert_eq!(without_string("xxx", "x"), without_string_solution("xxx", "x"))
    }
    #[test]
    fn test_without_string_11() {
        assert_eq!(without_string("xxx", "xx"), without_string_solution("xxx", "xx"))
    }
    #[test]
    fn test_without_string_12() {
        assert_eq!(without_string("xyzzy", "Y"), without_string_solution("xyzzy", "Y"))
    }
    #[test]
    fn test_without_string_13() {
        assert_eq!(without_string("", "x"), without_string_solution("", "x"))
    }
    #[test]
    fn test_without_string_14() {
        assert_eq!(without_string("abcabc", "b"), without_string_solution("abcabc", "b"))
    }
    #[test]
    fn test_without_string_15() {
        assert_eq!(without_string("AA22bb", "2"), without_string_solution("AA22bb", "2"))
    }
    #[test]
    fn test_without_string_16() {
        assert_eq!(without_string("1111", "1"), without_string_solution("1111", "1"))
    }
    #[test]
    fn test_without_string_17() {
        assert_eq!(without_string("1111", "11"), without_string_solution("1111", "11"))
    }
    #[test]
    fn test_without_string_18() {
        assert_eq!(without_string("MkjtMkx", "Mk"), without_string_solution("MkjtMkx", "Mk"))
    }
    #[test]
    fn test_without_string_19() {
        assert_eq!(without_string("Hi HoHo", "Ho"), without_string_solution("Hi HoHo", "Ho"))
    }
}

#[cfg(test)]
mod test_equal_is_not {
    use super::implementations::equal_is_not;
    use super::solutions::equal_is_not_solution;

    #[test]
    fn test_equal_is_not_1() {
        assert_eq!(equal_is_not("This is not"), equal_is_not_solution("This is not"))
    }
    #[test]
    fn test_equal_is_not_2() {
        assert_eq!(equal_is_not("This is notnot"), equal_is_not_solution("This is notnot"))
    }
    #[test]
    fn test_equal_is_not_3() {
        assert_eq!(equal_is_not("noisxxnotyynotxisi"), equal_is_not_solution("noisxxnotyynotxisi"))
    }
    #[test]
    fn test_equal_is_not_4() {
        assert_eq!(equal_is_not("noisxxnotyynotxsi"), equal_is_not_solution("noisxxnotyynotxsi"))
    }
    #[test]
    fn test_equal_is_not_5() {
        assert_eq!(equal_is_not("xxxyyyzzzintint"), equal_is_not_solution("xxxyyyzzzintint"))
    }
    #[test]
    fn test_equal_is_not_6() {
        assert_eq!(equal_is_not(""), equal_is_not_solution(""))
    }
    #[test]
    fn test_equal_is_not_7() {
        assert_eq!(equal_is_not("isisnotnot"), equal_is_not_solution("isisnotnot"))
    }
    #[test]
    fn test_equal_is_not_8() {
        assert_eq!(equal_is_not("isisnotno7Not"), equal_is_not_solution("isisnotno7Not"))
    }
    #[test]
    fn test_equal_is_not_9() {
        assert_eq!(equal_is_not("isnotis"), equal_is_not_solution("isnotis"))
    }
    #[test]
    fn test_equal_is_not_10() {
        assert_eq!(equal_is_not("mis3notpotbotis"), equal_is_not_solution("mis3notpotbotis"))
    }
}

#[cfg(test)]
mod test_g_happy {
    use super::implementations::g_happy;
    use super::solutions::g_happy_solution;

    #[test]
    fn test_g_happy_1() {
        assert_eq!(g_happy("xxggxx"), g_happy_solution("xxggxx"))
    }
    #[test]
    fn test_g_happy_2() {
        assert_eq!(g_happy("xxgxx"), g_happy_solution("xxgxx"))
    }
    #[test]
    fn test_g_happy_3() {
        assert_eq!(g_happy("xxggyygxx"), g_happy_solution("xxggyygxx"))
    }
    #[test]
    fn test_g_happy_4() {
        assert_eq!(g_happy("g"), g_happy_solution("g"))
    }
    #[test]
    fn test_g_happy_5() {
        assert_eq!(g_happy("gg"), g_happy_solution("gg"))
    }
    #[test]
    fn test_g_happy_6() {
        assert_eq!(g_happy(""), g_happy_solution(""))
    }
    #[test]
    fn test_g_happy_7() {
        assert_eq!(g_happy("xxgggxyz"), g_happy_solution("xxgggxyz"))
    }
    #[test]
    fn test_g_happy_8() {
        assert_eq!(g_happy("xxgggxyg"), g_happy_solution("xxgggxyg"))
    }
    #[test]
    fn test_g_happy_9() {
        assert_eq!(g_happy("xxgggxygg"), g_happy_solution("xxgggxygg"))
    }
    #[test]
    fn test_g_happy_10() {
        assert_eq!(g_happy("mgm"), g_happy_solution("mgm"))
    }
    #[test]
    fn test_g_happy_11() {
        assert_eq!(g_happy("mggm"), g_happy_solution("mggm"))
    }
    #[test]
    fn test_g_happy_12() {
        assert_eq!(g_happy("yyygggxyy"), g_happy_solution("yyygggxyy"))
    }
}

#[cfg(test)]
mod test_count_triple {
    use super::implementations::count_triple;
    use super::solutions::count_triple_solution;

    #[test]
    fn test_count_triple_1() {
        assert_eq!(count_triple("abcXXXabc"), count_triple_solution("abcXXXabc"))
    }
    #[test]
    fn test_count_triple_2() {
        assert_eq!(count_triple("xxxabyyyycd"), count_triple_solution("xxxabyyyycd"))
    }
    #[test]
    fn test_count_triple_3() {
        assert_eq!(count_triple("a"), count_triple_solution("a"))
    }
    #[test]
    fn test_count_triple_4() {
        assert_eq!(count_triple(""), count_triple_solution(""))
    }
    #[test]
    fn test_count_triple_5() {
        assert_eq!(count_triple("XXXabc"), count_triple_solution("XXXabc"))
    }
    #[test]
    fn test_count_triple_6() {
        assert_eq!(count_triple("XXXXabc"), count_triple_solution("XXXXabc"))
    }
    #[test]
    fn test_count_triple_7() {
        assert_eq!(count_triple("XXXXXabc"), count_triple_solution("XXXXXabc"))
    }
    #[test]
    fn test_count_triple_8() {
        assert_eq!(count_triple("222abyyycdXXX"), count_triple_solution("222abyyycdXXX"))
    }
    #[test]
    fn test_count_triple_9() {
        assert_eq!(count_triple("abYYYabXXXXXab"), count_triple_solution("abYYYabXXXXXab"))
    }
    #[test]
    fn test_count_triple_10() {
        assert_eq!(count_triple("abYYXabXXYXXab"), count_triple_solution("abYYXabXXYXXab"))
    }
    #[test]
    fn test_count_triple_11() {
        assert_eq!(count_triple("abYYXabXXYXXab"), count_triple_solution("abYYXabXXYXXab"))
    }
    #[test]
    fn test_count_triple_12() {
        assert_eq!(count_triple("122abhhh2"), count_triple_solution("122abhhh2"))
    }
}

#[cfg(test)]
mod test_sum_digits {
    use super::implementations::sum_digits;
    use super::solutions::sum_digits_solution;

    #[test]
    fn test_sum_digits_1() {
        assert_eq!(sum_digits("aa1bc2d3"), sum_digits_solution("aa1bc2d3"))
    }
    #[test]
    fn test_sum_digits_2() {
        assert_eq!(sum_digits("aa11b33"), sum_digits_solution("aa11b33"))
    }
    #[test]
    fn test_sum_digits_3() {
        assert_eq!(sum_digits("Chocolate"), sum_digits_solution("Chocolate"))
    }
    #[test]
    fn test_sum_digits_4() {
        assert_eq!(sum_digits("5hoco1a1e"), sum_digits_solution("5hoco1a1e"))
    }
    #[test]
    fn test_sum_digits_5() {
        assert_eq!(sum_digits("123abc123"), sum_digits_solution("123abc123"))
    }
    #[test]
    fn test_sum_digits_6() {
        assert_eq!(sum_digits(""), sum_digits_solution(""))
    }
    #[test]
    fn test_sum_digits_7() {
        assert_eq!(sum_digits("Hello"), sum_digits_solution("Hello"))
    }
    #[test]
    fn test_sum_digits_8() {
        assert_eq!(sum_digits("X1z9b2"), sum_digits_solution("X1z9b2"))
    }
    #[test]
    fn test_sum_digits_9() {
        assert_eq!(sum_digits("5432a"), sum_digits_solution("5432a"))
    }
}

#[cfg(test)]
mod test_same_ends {
    use super::implementations::same_ends;
    use super::solutions::same_ends_solution;

    #[test]
    fn test_same_ends_1() {
        assert_eq!(same_ends("abXYab"), same_ends_solution("abXYab"))
    }
    #[test]
    fn test_same_ends_2() {
        assert_eq!(same_ends("xx"), same_ends_solution("xx"))
    }
    #[test]
    fn test_same_ends_3() {
        assert_eq!(same_ends("xxx"), same_ends_solution("xxx"))
    }
    #[test]
    fn test_same_ends_4() {
        assert_eq!(same_ends("xxxx"), same_ends_solution("xxxx"))
    }
    #[test]
    fn test_same_ends_5() {
        assert_eq!(same_ends("javaXYZjava"), same_ends_solution("javaXYZjava"))
    }
    #[test]
    fn test_same_ends_6() {
        assert_eq!(same_ends("javajava"), same_ends_solution("javajava"))
    }
    #[test]
    fn test_same_ends_7() {
        assert_eq!(same_ends("xavaXYZjava"), same_ends_solution("xavaXYZjava"))
    }
    #[test]
    fn test_same_ends_8() {
        assert_eq!(same_ends("Hello! and Hello!"), same_ends_solution("Hello! and Hello!"))
    }
    #[test]
    fn test_same_ends_9() {
        assert_eq!(same_ends("x"), same_ends_solution("x"))
    }
    #[test]
    fn test_same_ends_10() {
        assert_eq!(same_ends(""), same_ends_solution(""))
    }
    #[test]
    fn test_same_ends_11() {
        assert_eq!(same_ends("abcb"), same_ends_solution("abcb"))
    }
    #[test]
    fn test_same_ends_12() {
        assert_eq!(same_ends("mymmy"), same_ends_solution("mymmy"))
    }
}

#[cfg(test)]
mod test_mirror_ends {
    use super::implementations::mirror_ends;
    use super::solutions::mirror_ends_solution;

    #[test]
    fn test_mirror_ends_1() {
        assert_eq!(mirror_ends("abXYZba"), mirror_ends_solution("abXYZba"))
    }
    #[test]
    fn test_mirror_ends_2() {
        assert_eq!(mirror_ends("abca"), mirror_ends_solution("abca"))
    }
    #[test]
    fn test_mirror_ends_3() {
        assert_eq!(mirror_ends("aba"), mirror_ends_solution("aba"))
    }
    #[test]
    fn test_mirror_ends_4() {
        assert_eq!(mirror_ends("abab"), mirror_ends_solution("abab"))
    }
    #[test]
    fn test_mirror_ends_5() {
        assert_eq!(mirror_ends("xxx"), mirror_ends_solution("xxx"))
    }
    #[test]
    fn test_mirror_ends_6() {
        assert_eq!(mirror_ends("xxYxx"), mirror_ends_solution("xxYxx"))
    }
    #[test]
    fn test_mirror_ends_7() {
        assert_eq!(mirror_ends("Hi and iH"), mirror_ends_solution("Hi and iH"))
    }
    #[test]
    fn test_mirror_ends_8() {
        assert_eq!(mirror_ends("x"), mirror_ends_solution("x"))
    }
    #[test]
    fn test_mirror_ends_9() {
        assert_eq!(mirror_ends(""), mirror_ends_solution(""))
    }
    #[test]
    fn test_mirror_ends_10() {
        assert_eq!(mirror_ends("123and then 321"), mirror_ends_solution("123and then 321"))
    }
    #[test]
    fn test_mirror_ends_11() {
        assert_eq!(mirror_ends("band andab"), mirror_ends_solution("band andab"))
    }
}

#[cfg(test)]
mod test_max_block {
    use super::implementations::max_block;
    use super::solutions::max_block_solution;

    #[test]
    fn test_max_block_1() {
        assert_eq!(max_block("hoopla"), max_block_solution("hoopla"))
    }
    #[test]
    fn test_max_block_2() {
        assert_eq!(max_block("abbCCCddBBBxx"), max_block_solution("abbCCCddBBBxx"))
    }
    #[test]
    fn test_max_block_3() {
        assert_eq!(max_block(""), max_block_solution(""))
    }
    #[test]
    fn test_max_block_4() {
        assert_eq!(max_block("xyz"), max_block_solution("xyz"))
    }
    #[test]
    fn test_max_block_5() {
        assert_eq!(max_block("xxyz"), max_block_solution("xxyz"))
    }
    #[test]
    fn test_max_block_6() {
        assert_eq!(max_block("xyzz"), max_block_solution("xyzz"))
    }
    #[test]
    fn test_max_block_7() {
        assert_eq!(max_block("abbbcbbbxbbbx"), max_block_solution("abbbcbbbxbbbx"))
    }
    #[test]
    fn test_max_block_8() {
        assert_eq!(max_block("XXBBBbbxx"), max_block_solution("XXBBBbbxx"))
    }
    #[test]
    fn test_max_block_9() {
        assert_eq!(max_block("XXBBBBbbxx"), max_block_solution("XXBBBBbbxx"))
    }
    #[test]
    fn test_max_block_10() {
        assert_eq!(max_block("XXBBBbbxxXXXX"), max_block_solution("XXBBBbbxxXXXX"))
    }
    #[test]
    fn test_max_block_11() {
        assert_eq!(max_block("XX2222BBBbbXX2222"), max_block_solution("XX2222BBBbbXX2222"))
    }
}

#[cfg(test)]
mod test_sum_numbers {
    use super::implementations::sum_numbers;
    use super::solutions::sum_numbers_solution;

    #[test]
    fn test_sum_numbers_1() {
        assert_eq!(sum_numbers("abc123xyz"), sum_numbers_solution("abc123xyz"))
    }
    #[test]
    fn test_sum_numbers_2() {
        assert_eq!(sum_numbers("aa11b33"), sum_numbers_solution("aa11b33"))
    }
    #[test]
    fn test_sum_numbers_3() {
        assert_eq!(sum_numbers("7 11"), sum_numbers_solution("7 11"))
    }
    #[test]
    fn test_sum_numbers_4() {
        assert_eq!(sum_numbers("Chocolate"), sum_numbers_solution("Chocolate"))
    }
    #[test]
    fn test_sum_numbers_5() {
        assert_eq!(sum_numbers("5hoco1a1e"), sum_numbers_solution("5hoco1a1e"))
    }
    #[test]
    fn test_sum_numbers_6() {
        assert_eq!(sum_numbers("5$$1;;1!!"), sum_numbers_solution("5$$1;;1!!"))
    }
    #[test]
    fn test_sum_numbers_7() {
        assert_eq!(sum_numbers("a1234bb11"), sum_numbers_solution("a1234bb11"))
    }
    #[test]
    fn test_sum_numbers_8() {
        assert_eq!(sum_numbers(""), sum_numbers_solution(""))
    }
    #[test]
    fn test_sum_numbers_9() {
        assert_eq!(sum_numbers("a22bbb3"), sum_numbers_solution("a22bbb3"))
    }
}

#[cfg(test)]
mod test_not_replace {
    use super::implementations::not_replace;
    use super::solutions::not_replace_solution;

    #[test]
    fn test_not_replace_1() {
        assert_eq!(not_replace("is test"), not_replace_solution("is test"))
    }
    #[test]
    fn test_not_replace_2() {
        assert_eq!(not_replace("is-is"), not_replace_solution("is-is"))
    }
    #[test]
    fn test_not_replace_3() {
        assert_eq!(not_replace("This is right"), not_replace_solution("This is right"))
    }
    #[test]
    fn test_not_replace_4() {
        assert_eq!(not_replace("This is isabell"), not_replace_solution("This is isabell"))
    }
    #[test]
    fn test_not_replace_5() {
        assert_eq!(not_replace(""), not_replace_solution(""))
    }
    #[test]
    fn test_not_replace_6() {
        assert_eq!(not_replace("is"), not_replace_solution("is"))
    }
    #[test]
    fn test_not_replace_7() {
        assert_eq!(not_replace("isis"), not_replace_solution("isis"))
    }
    #[test]
    fn test_not_replace_8() {
        assert_eq!(not_replace("Dis is bliss is"), not_replace_solution("Dis is bliss is"))
    }
    #[test]
    fn test_not_replace_9() {
        assert_eq!(not_replace("is his"), not_replace_solution("is his"))
    }
    #[test]
    fn test_not_replace_10() {
        assert_eq!(not_replace("xis yis"), not_replace_solution("xis yis"))
    }
    #[test]
    fn test_not_replace_11() {
        assert_eq!(not_replace("AAAis is"), not_replace_solution("AAAis is"))
    }
}
