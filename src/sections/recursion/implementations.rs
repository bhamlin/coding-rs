/// ## Recursion-1 -- factorial
/// Given n of 1 or more, return the factorial of n, which is n * (n-1) * (n-2)
/// ... 1. Compute the result recursively (without loops).
/// ### Examples
///    factorial(1) -> 1
///    factorial(2) -> 2
///    factorial(3) -> 6
#[allow(unused)]
pub fn factorial(n: u32) -> u32 {
    todo!()
}

/// ## Recursion-1 -- bunnyEars
/// We have a number of bunnies and each bunny has two big floppy ears. We want
/// to compute the total number of ears across all the bunnies recursively
/// (without loops or multiplication).
/// ### Examples
///    bunny_ears(0) -> 0
///    bunny_ears(1) -> 2
///    bunny_ears(2) -> 4
#[allow(unused)]
pub fn bunny_ears(bunnies: u32) -> u32 {
    todo!()
}

/// ## Recursion-1 -- fibonacci
/// The fibonacci sequence is a famous bit of mathematics, and it happens to
/// have a recursive definition. The first two values in the sequence are 0 and
/// 1 (essentially 2 base cases). Each subsequent value is the sum of the
/// previous two values, so the whole sequence is: 0, 1, 1, 2, 3, 5, 8, 13, 21
/// and so on. Define a recursive fibonacci(n) method that returns the nth
/// fibonacci number, with n=0 representing the start of the sequence.
/// ### Examples
///    fibonacci(0) -> 0
///    fibonacci(1) -> 1
///    fibonacci(2) -> 1
#[allow(unused)]
pub fn fibonacci(n: u32) -> u32 {
    todo!()
}

/// ## Recursion-1 -- bunnyEars2
/// We have bunnies standing in a line, numbered 1, 2, ... The odd bunnies (1,
/// 3, ..) have the normal 2 ears. The even bunnies (2, 4, ..) we'll say have 3
/// ears, because they each have a raised foot. Recursively return the number of
/// "ears" in the bunny line 1, 2, ... n (without loops or multiplication).
/// ### Examples
///    bunny_ears2(0) -> 0
///    bunny_ears2(1) -> 2
///    bunny_ears2(2) -> 5
#[allow(unused)]
pub fn bunny_ears2(bunnies: u32) -> u32 {
    todo!()
}

/// ## Recursion-1 -- triangle
/// We have triangle made of blocks. The topmost row has 1 block, the next row
/// down has 2 blocks, the next row has 3 blocks, and so on. Compute recursively
/// (no loops or multiplication) the total number of blocks in such a triangle
/// with the given number of rows.
/// ### Examples
///    triangle(0) -> 0
///    triangle(1) -> 1
///    triangle(2) -> 3
#[allow(unused)]
pub fn triangle(rows: u32) -> u32 {
    todo!()
}

/// ## Recursion-1 -- sumDigits1
/// Given a non-negative int n, return the sum of its digits recursively (no
/// loops). Note that mod (%) by 10 yields the rightmost digit (126 % 10 is 6),
/// while flooring division (Math.floor(n/10)) by 10 removes the rightmost digit
/// [Math.floor(126 / 10) is 12].
/// ### Examples
///    sum_digits1(126) -> 9
///    sum_digits1(49) -> 13
///    sum_digits1(12) -> 3
#[allow(unused)]
pub fn sum_digits1(n: u32) -> u32 {
    todo!()
}

/// ## Recursion-1 -- count7
/// Given a non-negative int n, return the count of the occurrences of 7 as a
/// digit, so for example 717 yields 2. (no loops). Note that mod (%) by 10
/// yields the rightmost digit (126 % 10 is 6), while flooring division
/// (Math.floor(n/10)) by 10 removes the rightmost digit [Math.floor(126 / 10)
/// is 12].
/// ### Examples
///    count7(717) -> 2
///    count7(7) -> 1
///    count7(123) -> 0
#[allow(unused)]
pub fn count7(n: u32) -> u32 {
    todo!()
}

/// ## Recursion-1 -- count8
/// Given a non-negative int n, compute recursively (no loops) the count of the
/// occurrences of 8 as a digit, except that an 8 with another 8 immediately to
/// its left counts double, so 8818 yields 4. Note that mod (%) by 10 yields the
/// rightmost digit (126 % 10 is 6), while flooring division (Math.floor(n/10))
/// by 10 removes the rightmost digit [Math.floor(126 / 10) is 12].
/// ### Examples
///    count8(8) -> 1
///    count8(818) -> 2
///    count8(8818) -> 4
#[allow(unused)]
pub fn count8(n: u32) -> u32 {
    todo!()
}

/// ## Recursion-1 -- powerN
/// Given base and n that are both 1 or more, compute recursively (no loops) the
/// value of base to the n power, so powerN(3, 2) is 9 (3 squared).
/// ### Examples
///    power_n(3, 1) -> 3
///    power_n(3, 2) -> 9
///    power_n(3, 3) -> 27
#[allow(unused)]
pub fn power_n(base: u32, n: u32) -> u32 {
    todo!()
}

/// ## Recursion-1 -- countX
/// Given a string, compute recursively (no loops) the number of lowercase 'x'
/// chars in the string.
/// ### Examples
///    count_x("xxhixx") -> 4
///    count_x("xhixhix") -> 3
///    count_x("hi") -> 0
#[allow(unused)]
pub fn count_x(str: impl Into<String>) -> u32 {
    let str: String = str.into();
    todo!()
}

/// ## Recursion-1 -- countHi
/// Given a string, compute recursively (no loops) the number of times lowercase
/// "hi" appears in the string.
/// ### Examples
///    count_hi("xxhixx") -> 1
///    count_hi("xhixhix") -> 2
///    count_hi("hi") -> 1
#[allow(unused)]
pub fn count_hi(str: impl Into<String>) -> u32 {
    let str: String = str.into();
    todo!()
}

/// ## Recursion-1 -- changeXY
/// Given a string, compute recursively (no loops) a new string where all the
/// lowercase 'x' chars have been changed to 'y' chars.
/// ### Examples
///    change_x_y("codex") -> "codey"
///    change_x_y("xxhixx") -> "yyhiyy"
///    change_x_y("xhixhix") -> "yhiyhiy"
#[allow(unused)]
pub fn change_x_y(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## Recursion-1 -- changePi
/// Given a string, compute recursively (no loops) a new string where all
/// appearances of "pi" have been replaced by "3.14".
/// ### Examples
///    change_pi("xpix") -> "x3.14x"
///    change_pi("pipi") -> "3.143.14"
///    change_pi("pip") -> "3.14p"
#[allow(unused)]
pub fn change_pi(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## Recursion-1 -- noX
/// Given a string, compute recursively a new string where all the 'x' chars
/// have been removed.
/// ### Examples
///    no_x("xaxb") -> "ab"
///    no_x("abc") -> "abc"
///    no_x("xx") -> ""
#[allow(unused)]
pub fn no_x(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## Recursion-1 -- array6
/// Given an array of ints, compute recursively if the array contains a 6. We'll
/// use the convention of considering only the part of the array that begins at
/// the given index. In this way, a recursive call can pass index+1 to move down
/// the array. The initial call will pass in index as 0.
/// ### Examples
///    array6([1, 6, 4], 0) -> true
///    array6([1, 4], 0) -> false
///    array6([6], 0) -> true
#[allow(unused)]
pub fn array6(nums: Vec<u32>, i: usize) -> bool {
    todo!()
}

/// ## Recursion-1 -- array11
/// Given an array of ints, compute recursively the number of times that the
/// value 11 appears in the array. We'll use the convention of considering only
/// the part of the array that begins at the given index. In this way, a
/// recursive call can pass index+1 to move down the array. The initial call
/// will pass in index as 0.
/// ### Examples
///    array11([1, 2, 11], 0) -> 1
///    array11([11, 11], 0) -> 2
///    array11([1, 2, 3, 4], 0) -> 0
#[allow(unused)]
pub fn array11(nums: Vec<u32>, i: usize) -> u32 {
    todo!()
}

/// ## Recursion-1 -- array220
/// Given an array of ints, compute recursively if the array contains somewhere
/// a value followed in the array by that value times 10. We'll use the
/// convention of considering only the part of the array that begins at the
/// given index. In this way, a recursive call can pass index+1 to move down the
/// array. The initial call will pass in index as 0.
/// ### Examples
///    array220([1, 2, 20], 0) -> true
///    array220([3, 30], 0) -> true
///    array220([3], 0) -> false
#[allow(unused)]
pub fn array220(nums: Vec<u32>, i: usize) -> bool {
    todo!()
}

/// ## Recursion-1 -- allStar
/// Given a string, compute recursively a new string where all the adjacent
/// chars are now separated by a "*".
/// ### Examples
///    all_star("hello") -> "h*e*l*l*o"
///    all_star("abc") -> "a*b*c"
///    all_star("ab") -> "a*b"
#[allow(unused)]
pub fn all_star(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## Recursion-1 -- pairStar
/// Given a string, compute recursively a new string where identical chars that
/// are adjacent in the original string are separated from each other by a "*".
/// ### Examples
///    pair_star("hello") -> "hel*lo"
///    pair_star("xxyy") -> "x*xy*y"
///    pair_star("aaaa") -> "a*a*a*a"
#[allow(unused)]
pub fn pair_star(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## Recursion-1 -- endX
/// Given a string, compute recursively a new string where all the lowercase 'x'
/// chars have been moved to the end of the string.
/// ### Examples
///    end_x("xxre") -> "rexx"
///    end_x("xxhixx") -> "hixxxx"
///    end_x("xhixhix") -> "hihixxx"
#[allow(unused)]
pub fn end_x(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## Recursion-1 -- countPairs
/// We'll say that a "pair" in a string is two instances of a char separated by
/// a char. So "AxA" the A's make a pair. Pair's can overlap, so "AxAxA"
/// contains 3 pairs -- 2 for A and 1 for x. Recursively compute the number of
/// pairs in the given string.
/// ### Examples
///    count_pairs("axa") -> 1
///    count_pairs("axax") -> 2
///    count_pairs("axbx") -> 1
#[allow(unused)]
pub fn count_pairs(str: impl Into<String>) -> u32 {
    let str: String = str.into();
    todo!()
}

/// ## Recursion-1 -- countAbc
/// Count recursively the total number of "abc" and "aba" substrings that appear
/// in the given string.
/// ### Examples
///    count_abc("abc") -> 1
///    count_abc("abcxxabc") -> 2
///    count_abc("abaxxaba") -> 2
#[allow(unused)]
pub fn count_abc(str: impl Into<String>) -> u32 {
    let str: String = str.into();
    todo!()
}

/// ## Recursion-1 -- count11
/// Given a string, compute recursively (no loops) the number of "11" substrings
/// in the string. The "11" substrings should not overlap.
/// ### Examples
///    count11("11abc11") -> 2
///    count11("abc11x11x11") -> 3
///    count11("111") -> 1
#[allow(unused)]
pub fn count11(str: impl Into<String>) -> u32 {
    let str: String = str.into();
    todo!()
}

/// ## Recursion-1 -- stringClean
/// Given a string, return recursively a "cleaned" string where adjacent chars
/// that are the same have been reduced to a single char. So "yyzzza" yields
/// "yza".
/// ### Examples
///    string_clean("yyzzza") -> "yza"
///    string_clean("abbbcdd") -> "abcd"
///    string_clean("Hello") -> "Helo"
#[allow(unused)]
pub fn string_clean(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## Recursion-1 -- countHi2
/// Given a string, compute recursively the number of times lowercase "hi"
/// appears in the string, however do not count "hi" that have an 'x' immedately
/// before them.
/// ### Examples
///    count_hi2("ahixhi") -> 1
///    count_hi2("ahibhi") -> 2
///    count_hi2("xhixhi") -> 0
#[allow(unused)]
pub fn count_hi2(str: impl Into<String>) -> u32 {
    let str: String = str.into();
    todo!()
}

/// ## Recursion-1 -- starBit
/// This question is modified from parenBit on CodingBat to starBit. Given a
/// string that contains a dash and a star, compute recursively a new string
/// made of only of the dash and star and their contents, so "xyz-abc*123"
/// yields "-abc*".
/// ### Examples
///    star_bit("xyz,-abc*123") -> "-abc*"
///    star_bit("x,-hello*") -> "-hello*"
///    star_bit(",-xy*1") -> "-xy*"
#[allow(unused)]
pub fn star_bit(str: impl Into<String>) -> String {
    let str: String = str.into();
    todo!()
}

/// ## Recursion-1 -- nestParen
/// Given a string, return true if it is a nesting of zero or more pairs of
/// parenthesis, like "(())" or "((()))". Suggestion: check the first and last
/// chars, and then recur on what's inside them.
/// ### Examples
///    nest_paren("(())") -> true
///    nest_paren("((()))") -> true
///    nest_paren("(((x))") -> false
#[allow(unused)]
pub fn nest_paren(str: impl Into<String>) -> bool {
    let str: String = str.into();
    todo!()
}

/// ## Recursion-1 -- strCount
/// Given a string and a non-empty substring sub, compute recursively the number
/// of times that sub appears in the string, without the sub strings
/// overlapping.
/// ### Examples
///    str_count("catcowcat", "cat") -> 2
///    str_count("catcowcat", "cow") -> 1
///    str_count("catcowcat", "dog") -> 0
#[allow(unused)]
pub fn str_count(str: impl Into<String>, sub: impl Into<String>) -> u32 {
    let str: String = str.into();
    let sub: String = sub.into();
    todo!()
}

/// ## Recursion-1 -- strCopies
/// Given a string and a non-empty substring sub, compute recursively if at
/// least n copies of sub appear in the string somewhere, possibly with
/// overlapping. N will be non-negative.
/// ### Examples
///    str_copies("catcowcat", "cat", 2) -> true
///    str_copies("catcowcat", "cow", 2) -> false
///    str_copies("catcowcat", "cow", 1) -> true
#[allow(unused)]
pub fn str_copies(str: impl Into<String>, sub: impl Into<String>, n: u32) -> bool {
    let str: String = str.into();
    let sub: String = sub.into();
    todo!()
}

/// ## Recursion-1 -- strDist
/// Given a string and a non-empty substring sub, compute recursively the
/// largest substring which starts and ends with sub and return its length.
/// ### Examples
///    str_dist("catcowcat", "cat") -> 9
///    str_dist("catcowcat", "cow") -> 3
///    str_dist("cccatcowcatxx", "cat") -> 9
#[allow(unused)]
pub fn str_dist(str: impl Into<String>, sub: impl Into<String>) -> u32 {
    let str: String = str.into();
    let sub: String = sub.into();
    todo!()
}
