pub fn factorial_solution(n: u32) -> u32 {
    match n {
        0 => 1,
        1 => 1,
        _ => n * factorial_solution(n - 1),
    }
}

pub fn bunny_ears_solution(bunnies: u32) -> u32 {
    match bunnies {
        0 => 0,
        1 => 2,
        _ => 2 + bunny_ears_solution(bunnies - 1),
    }
}

pub fn fibonacci_solution(n: u32) -> u32 {
    match n {
        0 => 0,
        1 => 1,
        2 => 1,
        _ => fibonacci_solution(n - 1) + fibonacci_solution(n - 2),
    }
}

pub fn bunny_ears2_solution(bunnies: u32) -> u32 {
    match bunnies {
        0 => 0,
        _ => match bunnies % 2 {
            0 => 3 + bunny_ears2_solution(bunnies - 1),
            _ => 2 + bunny_ears2_solution(bunnies - 1),
        },
    }
}

pub fn triangle_solution(rows: u32) -> u32 {
    match rows {
        0 => 0,
        1 => 1,
        _ => rows + triangle_solution(rows - 1),
    }
}

pub fn sum_digits1_solution(n: u32) -> u32 {
    match n {
        0 => 0,
        _ => n % 10 + sum_digits1_solution(n / 10),
    }
}

pub fn count7_solution(n: u32) -> u32 {
    if n > 0 {
        match n % 10 {
            7 => 1 + count7_solution(n / 10),
            _ => count7_solution(n / 10),
        }
    } else {
        0
    }
}

pub fn count8_solution(n: u32) -> u32 {
    let this = n % 10;
    let future = n / 10;
    let next = future % 10;

    if n > 0 {
        let up = match this {
            8 => match next {
                8 => 2,
                _ => 1,
            },
            _ => 0,
        };
        up + count8_solution(future)
    } else {
        0
    }
}

pub fn power_n_solution(base: u32, n: u32) -> u32 {
    match n {
        0 => 1,
        1 => base,
        _ => base * power_n_solution(base, n - 1),
    }
}

pub fn count_x_solution(str: impl Into<String>) -> u32 {
    let mut input: String = str.into().clone();

    if input.len() > 0 {
        (match input.pop() {
            Some(c) => match c {
                'x' => 1,
                _ => 0,
            },
            None => 0,
        }) + count_x_solution(input)
    } else {
        0
    }
}

pub fn count_hi_solution(str: impl Into<String>) -> u32 {
    let input: Vec<char> = str.into().chars().collect();
    let chunk = input.iter().take(2).collect::<String>();
    let chunk = chunk.as_str();

    if input.len() > 0 {
        (match chunk {
            "hi" => 1,
            _ => 0,
        }) + count_hi_solution(input.iter().skip(1).collect::<String>())
    } else {
        0
    }
}

pub fn change_x_y_solution(str: impl Into<String>) -> String {
    let input: Vec<char> = str.into().chars().collect();

    if input.len() > 0 {
        format!(
            "{}{}",
            match input[0] {
                'x' => 'y',
                e => e,
            },
            change_x_y_solution(input.iter().skip(1).collect::<String>())
        )
    } else {
        String::new()
    }
}

pub fn change_pi_solution(str: impl Into<String>) -> String {
    let input: Vec<char> = str.into().chars().collect();
    let len = input.len();
    if len > 0 {
        let check: String = input.iter().take(2).collect();
        let this: String;
        let next: String;

        if check == "pi" {
            this = String::from("3.14");
            next = input.iter().skip(2).collect::<String>();
        } else {
            this = String::from(input[0]);
            next = input.iter().skip(1).collect::<String>();
        }
        format!("{this}{}", change_pi_solution(next))
    } else {
        String::new()
    }
}

pub fn no_x_solution(str: impl Into<String>) -> String {
    let input: Vec<char> = str.into().chars().collect();
    if input.len() > 0 {
        let next: String = input.iter().skip(1).collect();
        let c = input[0];
        if c == 'x' {
            no_x_solution(next)
        } else {
            format!("{c}{}", no_x_solution(next))
        }
    } else {
        String::new()
    }
}

pub fn array6_solution(nums: Vec<u32>, i: usize) -> bool {
    let len = nums.len();
    if i < len {
        match nums[i] {
            6 => true,
            _ => array6_solution(nums, i + 1),
        }
    } else {
        false
    }
}

pub fn array11_solution(nums: Vec<u32>, i: usize) -> u32 {
    let len = nums.len();
    if i < len {
        let update = if nums[i] == 11 { 1u32 } else { 0u32 };
        update + array11_solution(nums, i + 1)
    } else {
        0
    }
}

pub fn array220_solution(nums: Vec<u32>, i: usize) -> bool {
    let len = nums.len();
    if i < len {
        let search = nums[i] * 10;
        let found = nums.iter().skip(i).filter(|&&e| e == search).count() > 0;
        if found {
            found
        } else {
            array220_solution(nums, i + 1)
        }
    } else {
        false
    }
}

pub fn all_star_solution(str: impl Into<String>) -> String {
    let input: Vec<char> = str.into().chars().collect();
    let len = input.len();
    if len > 1 {
        let char = input[0];
        format!("{char}*{}", all_star_solution(input.iter().skip(1).collect::<String>()))
    } else {
        input.iter().collect::<String>()
    }
}

pub fn pair_star_solution(str: impl Into<String>) -> String {
    let input: Vec<char> = str.into().chars().collect();
    let len = input.len();

    if len > 1 {
        let here = input[0];
        let there = input[1];
        let sep: &str;

        if here == there {
            sep = "*";
        } else {
            sep = "";
        }
        format!("{here}{sep}{}", pair_star_solution(input.iter().skip(1).collect::<String>()))
    } else {
        input.iter().collect::<String>()
    }
}

pub fn end_x_solution(str: impl Into<String>) -> String {
    let input: Vec<char> = str.into().chars().collect();
    let len = input.len();

    if len > 0 {
        if input[0] == 'x' {
            format!("{}x", end_x_solution(input.iter().skip(1).collect::<String>()))
        } else {
            format!("{}{}", input[0], end_x_solution(input.iter().skip(1).collect::<String>()))
        }
    } else {
        String::new()
    }
}

pub fn count_pairs_solution(str: impl Into<String>) -> u32 {
    let input: Vec<char> = str.into().chars().collect();
    let len = input.len();

    if len > 2 {
        let this = input[0];
        let that = input[2];
        let state = if this == that { 1 } else { 0 };

        state + count_pairs_solution(input.iter().skip(1).collect::<String>())
    } else {
        0
    }
}

pub fn count_abc_solution(str: impl Into<String>) -> u32 {
    let input: Vec<char> = str.into().chars().collect();
    let len = input.len();

    if len > 2 {
        let chunk = input.iter().take(3).collect::<String>();
        let state: u32;

        if chunk == "abc" || chunk == "aba" {
            state = 1;
        } else {
            state = 0;
        }

        state + count_abc_solution(input.iter().skip(1).collect::<String>())
    } else {
        0
    }
}

pub fn count11_solution(str: impl Into<String>) -> u32 {
    let input: Vec<char> = str.into().chars().collect();
    let len = input.len();

    if len > 1 {
        if input[0] == '1' && input[1] == input[0] {
            1 + count11_solution(input.iter().skip(2).collect::<String>())
        } else {
            count11_solution(input.iter().skip(1).collect::<String>())
        }
    } else {
        0
    }
}

pub fn string_clean_solution(str: impl Into<String>) -> String {
    let input: Vec<char> = str.into().chars().collect();
    let len = input.len();

    if len > 1 {
        let next = input.iter().skip(1).collect::<String>();
        let char = input[0];

        if input[1] == char {
            string_clean_solution(next)
        } else {
            format!("{char}{}", string_clean_solution(next))
        }
    } else {
        input.iter().collect::<String>()
    }
}

pub fn count_hi2_solution(str: impl Into<String>) -> u32 {
    let input: Vec<char> = str.into().chars().collect();
    let len = input.len();

    if len > 0 {
        if input[0] == 'x' {
            if len > 1 && input[1] != 'x' {
                return count_hi2_solution(input.iter().skip(2).collect::<String>());
            }
        }
    }
    if len > 1 {
        let chunk = input.iter().take(2).collect::<String>();
        if chunk == "hi" {
            1 + count_hi2_solution(input.iter().skip(1).collect::<String>())
        } else {
            count_hi2_solution(input.iter().skip(1).collect::<String>())
        }
    } else {
        0
    }
}

pub fn star_bit_solution(str: impl Into<String>) -> String {
    let input: Vec<char> = str.into().chars().collect();
    let len = input.len();

    if len > 0 {
        let dash = input[0] == '-';
        let star = input[len - 1] == '*';

        match (dash, star) {
            (false, _) => star_bit_solution(input.iter().skip(1).collect::<String>()),
            (true, false) => star_bit_solution(input.iter().take(len - 1).collect::<String>()),
            _ => input.iter().collect::<String>(),
        }
    } else {
        String::from("")
    }
}

pub fn nest_paren_solution(str: impl Into<String>) -> bool {
    let input: Vec<char> = str.into().chars().collect();
    let len = input.len();

    if len > 1 {
        let front = input[0] == '(';
        let back = input[len - 1] == ')';

        if front && back {
            nest_paren_solution(input.iter().skip(1).take(len - 2).collect::<String>())
        } else {
            false
        }
    } else if len > 0 {
        !(input[0] == '(' || input[0] == ')')
    } else {
        true
    }
}

pub fn str_count_solution(str: impl Into<String>, sub: impl Into<String>) -> u32 {
    let input: Vec<char> = str.into().chars().collect();
    let i_len = input.len();
    let sub: String = sub.into();
    let s_len = sub.len();

    if i_len >= s_len {
        let chunk = input.iter().take(s_len).collect::<String>();

        if chunk == sub {
            1 + str_count_solution(input.iter().skip(s_len).collect::<String>(), sub)
        } else {
            str_count_solution(input.iter().skip(1).collect::<String>(), sub)
        }
    } else {
        0
    }
}

pub fn str_copies_solution(str: impl Into<String>, sub: impl Into<String>, n: u32) -> bool {
    let input: Vec<char> = str.into().chars().collect();
    let i_len = input.len();
    let sub: String = sub.into();
    let s_len = sub.len();

    if n > 0 {
        if i_len >= s_len {
            let chunk = input.iter().take(s_len).collect::<String>();
            let next = input.iter().skip(1).collect::<String>();
            let c: u32;

            if chunk == sub {
                c = n - 1;
            } else {
                c = n;
            }
            str_copies_solution(next, sub, c)
        } else {
            false
        }
    } else {
        true
    }
}

pub fn str_dist_solution(str: impl Into<String>, sub: impl Into<String>) -> u32 {
    let input: Vec<char> = str.into().chars().collect();
    let i_len = input.len();
    let sub: String = sub.into();
    let s_len = sub.len();

    if i_len >= s_len {
        let front = sub == input.iter().take(s_len).collect::<String>();
        let back = sub == input.iter().skip(i_len - s_len).collect::<String>();

        match (front, back) {
            (false, _) => str_dist_solution(input.iter().skip(1).collect::<String>(), sub),
            (true, false) => str_dist_solution(input.iter().take(i_len - 1).collect::<String>(), sub),
            (true, true) => i_len.try_into().expect("Why won't this fit?"),
        }
    } else {
        0
    }
}
