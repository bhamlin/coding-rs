use super::implementations::*;
use super::solutions::*;
use crate::display::TestDisplay;

pub fn run(test: Option<Vec<String>>) {
    match test {
        // None => println!("Please specify a test name"),
        None => {
            factorial_run();
            bunny_ears_run();
            fibonacci_run();
            bunny_ears2_run();
            triangle_run();
            sum_digits1_run();
            count7_run();
            count8_run();
            power_n_run();
            count_x_run();
            count_hi_run();
            change_x_y_run();
            change_pi_run();
            no_x_run();
            array6_run();
            array11_run();
            array220_run();
            all_star_run();
            pair_star_run();
            end_x_run();
            count_pairs_run();
            count_abc_run();
            count11_run();
            string_clean_run();
            count_hi2_run();
            star_bit_run();
            nest_paren_run();
            str_count_run();
            str_copies_run();
            str_dist_run();
        }
        Some(tests) => {
            for test_name in tests {
                match test_name.to_ascii_lowercase().as_str() {
                    "factorial" => factorial_run(),
                    "bunnyEars" | "bunny_ears" => bunny_ears_run(),
                    "fibonacci" => fibonacci_run(),
                    "bunnyEars2" | "bunny_ears2" => bunny_ears2_run(),
                    "triangle" => triangle_run(),
                    "sumDigits1" | "sum_digits1" => sum_digits1_run(),
                    "count7" => count7_run(),
                    "count8" => count8_run(),
                    "powerN" | "power_n" => power_n_run(),
                    "countX" | "count_x" => count_x_run(),
                    "countHi" | "count_hi" => count_hi_run(),
                    "changeXY" | "change_x_y" => change_x_y_run(),
                    "changePi" | "change_pi" => change_pi_run(),
                    "noX" | "no_x" => no_x_run(),
                    "array6" => array6_run(),
                    "array11" => array11_run(),
                    "array220" => array220_run(),
                    "allStar" | "all_star" => all_star_run(),
                    "pairStar" | "pair_star" => pair_star_run(),
                    "endX" | "end_x" => end_x_run(),
                    "countPairs" | "count_pairs" => count_pairs_run(),
                    "countAbc" | "count_abc" => count_abc_run(),
                    "count11" => count11_run(),
                    "stringClean" | "string_clean" => string_clean_run(),
                    "countHi2" | "count_hi2" => count_hi2_run(),
                    "starBit" | "star_bit" => star_bit_run(),
                    "nestParen" | "nest_paren" => nest_paren_run(),
                    "strCount" | "str_count" => str_count_run(),
                    "strCopies" | "str_copies" => str_copies_run(),
                    "strDist" | "str_dist" => str_dist_run(),
                    _ => println!("No test named {test_name}"),
                };
            }
        }
    };
}

pub fn factorial_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("factorial");
    layout.print_header();
    layout.print_test(format!("{:?}", (1)), factorial_solution(1), factorial(1));
    layout.print_test(format!("{:?}", (2)), factorial_solution(2), factorial(2));
    layout.print_test(format!("{:?}", (3)), factorial_solution(3), factorial(3));
    layout.print_test(format!("{:?}", (4)), factorial_solution(4), factorial(4));
    layout.print_test(format!("{:?}", (5)), factorial_solution(5), factorial(5));
    layout.print_test(format!("{:?}", (6)), factorial_solution(6), factorial(6));
    layout.print_test(format!("{:?}", (7)), factorial_solution(7), factorial(7));
    layout.print_test(format!("{:?}", (8)), factorial_solution(8), factorial(8));
    layout.print_test(format!("{:?}", (12)), factorial_solution(12), factorial(12));
    layout.print_footer();
}

pub fn bunny_ears_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("bunny_ears");
    layout.print_header();
    layout.print_test(format!("{:?}", (0)), bunny_ears_solution(0), bunny_ears(0));
    layout.print_test(format!("{:?}", (1)), bunny_ears_solution(1), bunny_ears(1));
    layout.print_test(format!("{:?}", (2)), bunny_ears_solution(2), bunny_ears(2));
    layout.print_test(format!("{:?}", (3)), bunny_ears_solution(3), bunny_ears(3));
    layout.print_test(format!("{:?}", (4)), bunny_ears_solution(4), bunny_ears(4));
    layout.print_test(format!("{:?}", (5)), bunny_ears_solution(5), bunny_ears(5));
    layout.print_test(format!("{:?}", (12)), bunny_ears_solution(12), bunny_ears(12));
    layout.print_test(format!("{:?}", (50)), bunny_ears_solution(50), bunny_ears(50));
    layout.print_test(format!("{:?}", (234)), bunny_ears_solution(234), bunny_ears(234));
    layout.print_footer();
}

pub fn fibonacci_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("fibonacci");
    layout.print_header();
    layout.print_test(format!("{:?}", (0)), fibonacci_solution(0), fibonacci(0));
    layout.print_test(format!("{:?}", (1)), fibonacci_solution(1), fibonacci(1));
    layout.print_test(format!("{:?}", (2)), fibonacci_solution(2), fibonacci(2));
    layout.print_test(format!("{:?}", (3)), fibonacci_solution(3), fibonacci(3));
    layout.print_test(format!("{:?}", (4)), fibonacci_solution(4), fibonacci(4));
    layout.print_test(format!("{:?}", (5)), fibonacci_solution(5), fibonacci(5));
    layout.print_test(format!("{:?}", (6)), fibonacci_solution(6), fibonacci(6));
    layout.print_test(format!("{:?}", (7)), fibonacci_solution(7), fibonacci(7));
    layout.print_footer();
}

pub fn bunny_ears2_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("bunny_ears2");
    layout.print_header();
    layout.print_test(format!("{:?}", (0)), bunny_ears2_solution(0), bunny_ears2(0));
    layout.print_test(format!("{:?}", (1)), bunny_ears2_solution(1), bunny_ears2(1));
    layout.print_test(format!("{:?}", (2)), bunny_ears2_solution(2), bunny_ears2(2));
    layout.print_test(format!("{:?}", (3)), bunny_ears2_solution(3), bunny_ears2(3));
    layout.print_test(format!("{:?}", (4)), bunny_ears2_solution(4), bunny_ears2(4));
    layout.print_test(format!("{:?}", (5)), bunny_ears2_solution(5), bunny_ears2(5));
    layout.print_test(format!("{:?}", (6)), bunny_ears2_solution(6), bunny_ears2(6));
    layout.print_test(format!("{:?}", (10)), bunny_ears2_solution(10), bunny_ears2(10));
    layout.print_footer();
}

pub fn triangle_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("triangle");
    layout.print_header();
    layout.print_test(format!("{:?}", (0)), triangle_solution(0), triangle(0));
    layout.print_test(format!("{:?}", (1)), triangle_solution(1), triangle(1));
    layout.print_test(format!("{:?}", (2)), triangle_solution(2), triangle(2));
    layout.print_test(format!("{:?}", (3)), triangle_solution(3), triangle(3));
    layout.print_test(format!("{:?}", (4)), triangle_solution(4), triangle(4));
    layout.print_test(format!("{:?}", (5)), triangle_solution(5), triangle(5));
    layout.print_test(format!("{:?}", (6)), triangle_solution(6), triangle(6));
    layout.print_test(format!("{:?}", (7)), triangle_solution(7), triangle(7));
    layout.print_footer();
}

pub fn sum_digits1_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("sum_digits1");
    layout.print_header();
    layout.print_test(format!("{:?}", (126)), sum_digits1_solution(126), sum_digits1(126));
    layout.print_test(format!("{:?}", (49)), sum_digits1_solution(49), sum_digits1(49));
    layout.print_test(format!("{:?}", (12)), sum_digits1_solution(12), sum_digits1(12));
    layout.print_test(format!("{:?}", (10)), sum_digits1_solution(10), sum_digits1(10));
    layout.print_test(format!("{:?}", (1)), sum_digits1_solution(1), sum_digits1(1));
    layout.print_test(format!("{:?}", (0)), sum_digits1_solution(0), sum_digits1(0));
    layout.print_test(format!("{:?}", (730)), sum_digits1_solution(730), sum_digits1(730));
    layout.print_test(format!("{:?}", (1111)), sum_digits1_solution(1111), sum_digits1(1111));
    layout.print_test(format!("{:?}", (11111)), sum_digits1_solution(11111), sum_digits1(11111));
    layout.print_test(format!("{:?}", (10110)), sum_digits1_solution(10110), sum_digits1(10110));
    layout.print_test(format!("{:?}", (235)), sum_digits1_solution(235), sum_digits1(235));
    layout.print_footer();
}

pub fn count7_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("count7");
    layout.print_header();
    layout.print_test(format!("{:?}", (717)), count7_solution(717), count7(717));
    layout.print_test(format!("{:?}", (7)), count7_solution(7), count7(7));
    layout.print_test(format!("{:?}", (123)), count7_solution(123), count7(123));
    layout.print_test(format!("{:?}", (77)), count7_solution(77), count7(77));
    layout.print_test(format!("{:?}", (7123)), count7_solution(7123), count7(7123));
    layout.print_test(format!("{:?}", (771237)), count7_solution(771237), count7(771237));
    layout.print_test(format!("{:?}", (771737)), count7_solution(771737), count7(771737));
    layout.print_test(format!("{:?}", (47571)), count7_solution(47571), count7(47571));
    layout.print_test(format!("{:?}", (777777)), count7_solution(777777), count7(777777));
    layout.print_test(format!("{:?}", (70701277)), count7_solution(70701277), count7(70701277));
    layout.print_test(format!("{:?}", (777576197)), count7_solution(777576197), count7(777576197));
    layout.print_test(format!("{:?}", (99999)), count7_solution(99999), count7(99999));
    layout.print_test(format!("{:?}", (99799)), count7_solution(99799), count7(99799));
    layout.print_footer();
}

pub fn count8_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("count8");
    layout.print_header();
    layout.print_test(format!("{:?}", (8)), count8_solution(8), count8(8));
    layout.print_test(format!("{:?}", (818)), count8_solution(818), count8(818));
    layout.print_test(format!("{:?}", (8818)), count8_solution(8818), count8(8818));
    layout.print_test(format!("{:?}", (8088)), count8_solution(8088), count8(8088));
    layout.print_test(format!("{:?}", (123)), count8_solution(123), count8(123));
    layout.print_test(format!("{:?}", (81238)), count8_solution(81238), count8(81238));
    layout.print_test(format!("{:?}", (88788)), count8_solution(88788), count8(88788));
    layout.print_test(format!("{:?}", (8234)), count8_solution(8234), count8(8234));
    layout.print_test(format!("{:?}", (2348)), count8_solution(2348), count8(2348));
    layout.print_test(format!("{:?}", (23884)), count8_solution(23884), count8(23884));
    layout.print_test(format!("{:?}", (0)), count8_solution(0), count8(0));
    layout.print_test(format!("{:?}", (1818188)), count8_solution(1818188), count8(1818188));
    layout.print_test(format!("{:?}", (8818181)), count8_solution(8818181), count8(8818181));
    layout.print_test(format!("{:?}", (1080)), count8_solution(1080), count8(1080));
    layout.print_test(format!("{:?}", (188)), count8_solution(188), count8(188));
    layout.print_test(format!("{:?}", (88888)), count8_solution(88888), count8(88888));
    layout.print_test(format!("{:?}", (9898)), count8_solution(9898), count8(9898));
    layout.print_test(format!("{:?}", (78)), count8_solution(78), count8(78));
    layout.print_footer();
}

pub fn power_n_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("power_n");
    layout.print_header();
    layout.print_test(format!("{:?}", (3, 1)), power_n_solution(3, 1), power_n(3, 1));
    layout.print_test(format!("{:?}", (3, 2)), power_n_solution(3, 2), power_n(3, 2));
    layout.print_test(format!("{:?}", (3, 3)), power_n_solution(3, 3), power_n(3, 3));
    layout.print_test(format!("{:?}", (2, 1)), power_n_solution(2, 1), power_n(2, 1));
    layout.print_test(format!("{:?}", (2, 2)), power_n_solution(2, 2), power_n(2, 2));
    layout.print_test(format!("{:?}", (2, 3)), power_n_solution(2, 3), power_n(2, 3));
    layout.print_test(format!("{:?}", (2, 4)), power_n_solution(2, 4), power_n(2, 4));
    layout.print_test(format!("{:?}", (2, 5)), power_n_solution(2, 5), power_n(2, 5));
    layout.print_test(format!("{:?}", (10, 1)), power_n_solution(10, 1), power_n(10, 1));
    layout.print_test(format!("{:?}", (10, 2)), power_n_solution(10, 2), power_n(10, 2));
    layout.print_test(format!("{:?}", (10, 3)), power_n_solution(10, 3), power_n(10, 3));
    layout.print_footer();
}

pub fn count_x_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("count_x");
    layout.print_header();
    layout.print_test(format!("{:?}", ("xxhixx")), count_x_solution("xxhixx"), count_x("xxhixx"));
    layout.print_test(format!("{:?}", ("xhixhix")), count_x_solution("xhixhix"), count_x("xhixhix"));
    layout.print_test(format!("{:?}", ("hi")), count_x_solution("hi"), count_x("hi"));
    layout.print_test(format!("{:?}", ("h")), count_x_solution("h"), count_x("h"));
    layout.print_test(format!("{:?}", ("x")), count_x_solution("x"), count_x("x"));
    layout.print_test(format!("{:?}", ("")), count_x_solution(""), count_x(""));
    layout.print_test(format!("{:?}", ("hihi")), count_x_solution("hihi"), count_x("hihi"));
    layout.print_test(format!("{:?}", ("hiAAhi12hi")), count_x_solution("hiAAhi12hi"), count_x("hiAAhi12hi"));
    layout.print_footer();
}

pub fn count_hi_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("count_hi");
    layout.print_header();
    layout.print_test(format!("{:?}", ("xxhixx")), count_hi_solution("xxhixx"), count_hi("xxhixx"));
    layout.print_test(format!("{:?}", ("xhixhix")), count_hi_solution("xhixhix"), count_hi("xhixhix"));
    layout.print_test(format!("{:?}", ("hi")), count_hi_solution("hi"), count_hi("hi"));
    layout.print_test(format!("{:?}", ("hihih")), count_hi_solution("hihih"), count_hi("hihih"));
    layout.print_test(format!("{:?}", ("h")), count_hi_solution("h"), count_hi("h"));
    layout.print_test(format!("{:?}", ("")), count_hi_solution(""), count_hi(""));
    layout.print_test(format!("{:?}", ("ihihihihih")), count_hi_solution("ihihihihih"), count_hi("ihihihihih"));
    layout.print_test(format!("{:?}", ("ihihihihihi")), count_hi_solution("ihihihihihi"), count_hi("ihihihihihi"));
    layout.print_test(format!("{:?}", ("hiAAhi12hi")), count_hi_solution("hiAAhi12hi"), count_hi("hiAAhi12hi"));
    layout.print_test(format!("{:?}", ("xhixhxihihhhih")), count_hi_solution("xhixhxihihhhih"), count_hi("xhixhxihihhhih"));
    layout.print_test(format!("{:?}", ("ship")), count_hi_solution("ship"), count_hi("ship"));
    layout.print_footer();
}

pub fn change_x_y_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("change_x_y");
    layout.print_header();
    layout.print_test(format!("{:?}", ("codex")), change_x_y_solution("codex"), change_x_y("codex"));
    layout.print_test(format!("{:?}", ("xxhixx")), change_x_y_solution("xxhixx"), change_x_y("xxhixx"));
    layout.print_test(format!("{:?}", ("xhixhix")), change_x_y_solution("xhixhix"), change_x_y("xhixhix"));
    layout.print_test(format!("{:?}", ("hiy")), change_x_y_solution("hiy"), change_x_y("hiy"));
    layout.print_test(format!("{:?}", ("h")), change_x_y_solution("h"), change_x_y("h"));
    layout.print_test(format!("{:?}", ("x")), change_x_y_solution("x"), change_x_y("x"));
    layout.print_test(format!("{:?}", ("")), change_x_y_solution(""), change_x_y(""));
    layout.print_test(format!("{:?}", ("xxx")), change_x_y_solution("xxx"), change_x_y("xxx"));
    layout.print_test(format!("{:?}", ("yyhxyi")), change_x_y_solution("yyhxyi"), change_x_y("yyhxyi"));
    layout.print_test(format!("{:?}", ("hihi")), change_x_y_solution("hihi"), change_x_y("hihi"));
    layout.print_footer();
}

pub fn change_pi_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("change_pi");
    layout.print_header();
    layout.print_test(format!("{:?}", ("xpix")), change_pi_solution("xpix"), change_pi("xpix"));
    layout.print_test(format!("{:?}", ("pipi")), change_pi_solution("pipi"), change_pi("pipi"));
    layout.print_test(format!("{:?}", ("pip")), change_pi_solution("pip"), change_pi("pip"));
    layout.print_test(format!("{:?}", ("pi")), change_pi_solution("pi"), change_pi("pi"));
    layout.print_test(format!("{:?}", ("hip")), change_pi_solution("hip"), change_pi("hip"));
    layout.print_test(format!("{:?}", ("p")), change_pi_solution("p"), change_pi("p"));
    layout.print_test(format!("{:?}", ("x")), change_pi_solution("x"), change_pi("x"));
    layout.print_test(format!("{:?}", ("")), change_pi_solution(""), change_pi(""));
    layout.print_test(format!("{:?}", ("pixx")), change_pi_solution("pixx"), change_pi("pixx"));
    layout.print_test(format!("{:?}", ("xyzzy")), change_pi_solution("xyzzy"), change_pi("xyzzy"));
    layout.print_footer();
}

pub fn no_x_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("no_x");
    layout.print_header();
    layout.print_test(format!("{:?}", ("xaxb")), no_x_solution("xaxb"), no_x("xaxb"));
    layout.print_test(format!("{:?}", ("abc")), no_x_solution("abc"), no_x("abc"));
    layout.print_test(format!("{:?}", ("xx")), no_x_solution("xx"), no_x("xx"));
    layout.print_test(format!("{:?}", ("")), no_x_solution(""), no_x(""));
    layout.print_test(format!("{:?}", ("axxbxx")), no_x_solution("axxbxx"), no_x("axxbxx"));
    layout.print_test(format!("{:?}", ("Hellox")), no_x_solution("Hellox"), no_x("Hellox"));
    layout.print_footer();
}

pub fn array6_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("array6");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 6, 4], 0)), array6_solution(vec![1, 6, 4], 0), array6(vec![1, 6, 4], 0));
    layout.print_test(format!("{:?}", (vec![1, 4], 0)), array6_solution(vec![1, 4], 0), array6(vec![1, 4], 0));
    layout.print_test(format!("{:?}", (vec![6], 0)), array6_solution(vec![6], 0), array6(vec![6], 0));
    layout.print_test(format!("{:?}", (vec![0; 0], 0)), array6_solution(vec![0; 0], 0), array6(vec![0; 0], 0));
    layout.print_test(format!("{:?}", (vec![6, 2, 2], 0)), array6_solution(vec![6, 2, 2], 0), array6(vec![6, 2, 2], 0));
    layout.print_test(format!("{:?}", (vec![2, 5], 0)), array6_solution(vec![2, 5], 0), array6(vec![2, 5], 0));
    layout.print_test(format!("{:?}", (vec![1, 9, 4, 6, 6], 0)), array6_solution(vec![1, 9, 4, 6, 6], 0), array6(vec![1, 9, 4, 6, 6], 0));
    layout.print_test(format!("{:?}", (vec![2, 5, 6], 0)), array6_solution(vec![2, 5, 6], 0), array6(vec![2, 5, 6], 0));
    layout.print_footer();
}

pub fn array11_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("array11");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 2, 11], 0)), array11_solution(vec![1, 2, 11], 0), array11(vec![1, 2, 11], 0));
    layout.print_test(format!("{:?}", (vec![11, 11], 0)), array11_solution(vec![11, 11], 0), array11(vec![11, 11], 0));
    layout.print_test(format!("{:?}", (vec![1, 2, 3, 4], 0)), array11_solution(vec![1, 2, 3, 4], 0), array11(vec![1, 2, 3, 4], 0));
    layout.print_test(format!("{:?}", (vec![1, 11, 3, 11, 11], 0)), array11_solution(vec![1, 11, 3, 11, 11], 0), array11(vec![1, 11, 3, 11, 11], 0));
    layout.print_test(format!("{:?}", (vec![11], 0)), array11_solution(vec![11], 0), array11(vec![11], 0));
    layout.print_test(format!("{:?}", (vec![1], 0)), array11_solution(vec![1], 0), array11(vec![1], 0));
    layout.print_test(format!("{:?}", (vec![0; 0], 0)), array11_solution(vec![0; 0], 0), array11(vec![0; 0], 0));
    layout.print_test(format!("{:?}", (vec![11, 2, 3, 4, 11, 5], 0)), array11_solution(vec![11, 2, 3, 4, 11, 5], 0), array11(vec![11, 2, 3, 4, 11, 5], 0));
    layout.print_test(format!("{:?}", (vec![11, 5, 11], 0)), array11_solution(vec![11, 5, 11], 0), array11(vec![11, 5, 11], 0));
    layout.print_footer();
}

pub fn array220_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("array220");
    layout.print_header();
    layout.print_test(format!("{:?}", (vec![1, 2, 20], 0)), array220_solution(vec![1, 2, 20], 0), array220(vec![1, 2, 20], 0));
    layout.print_test(format!("{:?}", (vec![3, 30], 0)), array220_solution(vec![3, 30], 0), array220(vec![3, 30], 0));
    layout.print_test(format!("{:?}", (vec![3], 0)), array220_solution(vec![3], 0), array220(vec![3], 0));
    layout.print_test(format!("{:?}", (vec![0; 0], 0)), array220_solution(vec![0; 0], 0), array220(vec![0; 0], 0));
    layout.print_test(format!("{:?}", (vec![3, 3, 30, 4], 0)), array220_solution(vec![3, 3, 30, 4], 0), array220(vec![3, 3, 30, 4], 0));
    layout.print_test(format!("{:?}", (vec![2, 19, 4], 0)), array220_solution(vec![2, 19, 4], 0), array220(vec![2, 19, 4], 0));
    layout.print_test(format!("{:?}", (vec![20, 2, 21], 0)), array220_solution(vec![20, 2, 21], 0), array220(vec![20, 2, 21], 0));
    layout.print_test(format!("{:?}", (vec![20, 2, 21, 210], 0)), array220_solution(vec![20, 2, 21, 210], 0), array220(vec![20, 2, 21, 210], 0));
    layout.print_test(format!("{:?}", (vec![2, 200, 2000], 0)), array220_solution(vec![2, 200, 2000], 0), array220(vec![2, 200, 2000], 0));
    layout.print_test(format!("{:?}", (vec![0, 0], 0)), array220_solution(vec![0, 0], 0), array220(vec![0, 0], 0));
    layout.print_test(format!("{:?}", (vec![1, 2, 3, 4, 5, 6], 0)), array220_solution(vec![1, 2, 3, 4, 5, 6], 0), array220(vec![1, 2, 3, 4, 5, 6], 0));
    layout.print_test(format!("{:?}", (vec![1, 2, 3, 4, 5, 50, 6], 0)), array220_solution(vec![1, 2, 3, 4, 5, 50, 6], 0), array220(vec![1, 2, 3, 4, 5, 50, 6], 0));
    layout.print_test(format!("{:?}", (vec![1, 2, 3, 4, 5, 51, 6], 0)), array220_solution(vec![1, 2, 3, 4, 5, 51, 6], 0), array220(vec![1, 2, 3, 4, 5, 51, 6], 0));
    layout.print_test(format!("{:?}", (vec![1, 2, 3, 4, 4, 50, 500, 6], 0)), array220_solution(vec![1, 2, 3, 4, 4, 50, 500, 6], 0), array220(vec![1, 2, 3, 4, 4, 50, 500, 6], 0));
    layout.print_footer();
}

pub fn all_star_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("all_star");
    layout.print_header();
    layout.print_test(format!("{:?}", ("hello")), all_star_solution("hello"), all_star("hello"));
    layout.print_test(format!("{:?}", ("abc")), all_star_solution("abc"), all_star("abc"));
    layout.print_test(format!("{:?}", ("ab")), all_star_solution("ab"), all_star("ab"));
    layout.print_test(format!("{:?}", ("a")), all_star_solution("a"), all_star("a"));
    layout.print_test(format!("{:?}", ("")), all_star_solution(""), all_star(""));
    layout.print_test(format!("{:?}", ("3.14")), all_star_solution("3.14"), all_star("3.14"));
    layout.print_test(format!("{:?}", ("Chocolate")), all_star_solution("Chocolate"), all_star("Chocolate"));
    layout.print_test(format!("{:?}", ("1234")), all_star_solution("1234"), all_star("1234"));
    layout.print_footer();
}

pub fn pair_star_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("pair_star");
    layout.print_header();
    layout.print_test(format!("{:?}", ("hello")), pair_star_solution("hello"), pair_star("hello"));
    layout.print_test(format!("{:?}", ("xxyy")), pair_star_solution("xxyy"), pair_star("xxyy"));
    layout.print_test(format!("{:?}", ("aaaa")), pair_star_solution("aaaa"), pair_star("aaaa"));
    layout.print_test(format!("{:?}", ("aaab")), pair_star_solution("aaab"), pair_star("aaab"));
    layout.print_test(format!("{:?}", ("aa")), pair_star_solution("aa"), pair_star("aa"));
    layout.print_test(format!("{:?}", ("a")), pair_star_solution("a"), pair_star("a"));
    layout.print_test(format!("{:?}", ("")), pair_star_solution(""), pair_star(""));
    layout.print_test(format!("{:?}", ("noadjacent")), pair_star_solution("noadjacent"), pair_star("noadjacent"));
    layout.print_test(format!("{:?}", ("abba")), pair_star_solution("abba"), pair_star("abba"));
    layout.print_test(format!("{:?}", ("abbba")), pair_star_solution("abbba"), pair_star("abbba"));
    layout.print_footer();
}

pub fn end_x_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("end_x");
    layout.print_header();
    layout.print_test(format!("{:?}", ("xxre")), end_x_solution("xxre"), end_x("xxre"));
    layout.print_test(format!("{:?}", ("xxhixx")), end_x_solution("xxhixx"), end_x("xxhixx"));
    layout.print_test(format!("{:?}", ("xhixhix")), end_x_solution("xhixhix"), end_x("xhixhix"));
    layout.print_test(format!("{:?}", ("hiy")), end_x_solution("hiy"), end_x("hiy"));
    layout.print_test(format!("{:?}", ("h")), end_x_solution("h"), end_x("h"));
    layout.print_test(format!("{:?}", ("x")), end_x_solution("x"), end_x("x"));
    layout.print_test(format!("{:?}", ("xx")), end_x_solution("xx"), end_x("xx"));
    layout.print_test(format!("{:?}", ("")), end_x_solution(""), end_x(""));
    layout.print_test(format!("{:?}", ("bxx")), end_x_solution("bxx"), end_x("bxx"));
    layout.print_test(format!("{:?}", ("bxax")), end_x_solution("bxax"), end_x("bxax"));
    layout.print_test(format!("{:?}", ("axaxax")), end_x_solution("axaxax"), end_x("axaxax"));
    layout.print_test(format!("{:?}", ("xxhxi")), end_x_solution("xxhxi"), end_x("xxhxi"));
    layout.print_footer();
}

pub fn count_pairs_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("count_pairs");
    layout.print_header();
    layout.print_test(format!("{:?}", ("axa")), count_pairs_solution("axa"), count_pairs("axa"));
    layout.print_test(format!("{:?}", ("axax")), count_pairs_solution("axax"), count_pairs("axax"));
    layout.print_test(format!("{:?}", ("axbx")), count_pairs_solution("axbx"), count_pairs("axbx"));
    layout.print_test(format!("{:?}", ("hi")), count_pairs_solution("hi"), count_pairs("hi"));
    layout.print_test(format!("{:?}", ("hihih")), count_pairs_solution("hihih"), count_pairs("hihih"));
    layout.print_test(format!("{:?}", ("ihihhh")), count_pairs_solution("ihihhh"), count_pairs("ihihhh"));
    layout.print_test(format!("{:?}", ("ihjxhh")), count_pairs_solution("ihjxhh"), count_pairs("ihjxhh"));
    layout.print_test(format!("{:?}", ("")), count_pairs_solution(""), count_pairs(""));
    layout.print_test(format!("{:?}", ("a")), count_pairs_solution("a"), count_pairs("a"));
    layout.print_test(format!("{:?}", ("aa")), count_pairs_solution("aa"), count_pairs("aa"));
    layout.print_test(format!("{:?}", ("aaa")), count_pairs_solution("aaa"), count_pairs("aaa"));
    layout.print_footer();
}

pub fn count_abc_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("count_abc");
    layout.print_header();
    layout.print_test(format!("{:?}", ("abc")), count_abc_solution("abc"), count_abc("abc"));
    layout.print_test(format!("{:?}", ("abcxxabc")), count_abc_solution("abcxxabc"), count_abc("abcxxabc"));
    layout.print_test(format!("{:?}", ("abaxxaba")), count_abc_solution("abaxxaba"), count_abc("abaxxaba"));
    layout.print_test(format!("{:?}", ("ababc")), count_abc_solution("ababc"), count_abc("ababc"));
    layout.print_test(format!("{:?}", ("abxbc")), count_abc_solution("abxbc"), count_abc("abxbc"));
    layout.print_test(format!("{:?}", ("aaabc")), count_abc_solution("aaabc"), count_abc("aaabc"));
    layout.print_test(format!("{:?}", ("hello")), count_abc_solution("hello"), count_abc("hello"));
    layout.print_test(format!("{:?}", ("")), count_abc_solution(""), count_abc(""));
    layout.print_test(format!("{:?}", ("ab")), count_abc_solution("ab"), count_abc("ab"));
    layout.print_test(format!("{:?}", ("aba")), count_abc_solution("aba"), count_abc("aba"));
    layout.print_test(format!("{:?}", ("aca")), count_abc_solution("aca"), count_abc("aca"));
    layout.print_test(format!("{:?}", ("aaa")), count_abc_solution("aaa"), count_abc("aaa"));
    layout.print_footer();
}

pub fn count11_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("count11");
    layout.print_header();
    layout.print_test(format!("{:?}", ("11abc11")), count11_solution("11abc11"), count11("11abc11"));
    layout.print_test(format!("{:?}", ("abc11x11x11")), count11_solution("abc11x11x11"), count11("abc11x11x11"));
    layout.print_test(format!("{:?}", ("111")), count11_solution("111"), count11("111"));
    layout.print_test(format!("{:?}", ("1111")), count11_solution("1111"), count11("1111"));
    layout.print_test(format!("{:?}", ("1")), count11_solution("1"), count11("1"));
    layout.print_test(format!("{:?}", ("")), count11_solution(""), count11(""));
    layout.print_test(format!("{:?}", ("hi")), count11_solution("hi"), count11("hi"));
    layout.print_test(format!("{:?}", ("11x111x1111")), count11_solution("11x111x1111"), count11("11x111x1111"));
    layout.print_test(format!("{:?}", ("1x111")), count11_solution("1x111"), count11("1x111"));
    layout.print_test(format!("{:?}", ("1Hello1")), count11_solution("1Hello1"), count11("1Hello1"));
    layout.print_test(format!("{:?}", ("Hello")), count11_solution("Hello"), count11("Hello"));
    layout.print_footer();
}

pub fn string_clean_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("string_clean");
    layout.print_header();
    layout.print_test(format!("{:?}", ("yyzzza")), string_clean_solution("yyzzza"), string_clean("yyzzza"));
    layout.print_test(format!("{:?}", ("abbbcdd")), string_clean_solution("abbbcdd"), string_clean("abbbcdd"));
    layout.print_test(format!("{:?}", ("Hello")), string_clean_solution("Hello"), string_clean("Hello"));
    layout.print_test(format!("{:?}", ("XXabcYY")), string_clean_solution("XXabcYY"), string_clean("XXabcYY"));
    layout.print_test(format!("{:?}", ("112ab445")), string_clean_solution("112ab445"), string_clean("112ab445"));
    layout.print_test(format!("{:?}", ("Hello Bookkeeper")), string_clean_solution("Hello Bookkeeper"), string_clean("Hello Bookkeeper"));
    layout.print_footer();
}

pub fn count_hi2_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("count_hi2");
    layout.print_header();
    layout.print_test(format!("{:?}", ("ahixhi")), count_hi2_solution("ahixhi"), count_hi2("ahixhi"));
    layout.print_test(format!("{:?}", ("ahibhi")), count_hi2_solution("ahibhi"), count_hi2("ahibhi"));
    layout.print_test(format!("{:?}", ("xhixhi")), count_hi2_solution("xhixhi"), count_hi2("xhixhi"));
    layout.print_test(format!("{:?}", ("hixhi")), count_hi2_solution("hixhi"), count_hi2("hixhi"));
    layout.print_test(format!("{:?}", ("hixhhi")), count_hi2_solution("hixhhi"), count_hi2("hixhhi"));
    layout.print_test(format!("{:?}", ("hihihi")), count_hi2_solution("hihihi"), count_hi2("hihihi"));
    layout.print_test(format!("{:?}", ("hihihix")), count_hi2_solution("hihihix"), count_hi2("hihihix"));
    layout.print_test(format!("{:?}", ("xhihihix")), count_hi2_solution("xhihihix"), count_hi2("xhihihix"));
    layout.print_test(format!("{:?}", ("xxhi")), count_hi2_solution("xxhi"), count_hi2("xxhi"));
    layout.print_test(format!("{:?}", ("hixxhi")), count_hi2_solution("hixxhi"), count_hi2("hixxhi"));
    layout.print_test(format!("{:?}", ("hi")), count_hi2_solution("hi"), count_hi2("hi"));
    layout.print_test(format!("{:?}", ("xxxx")), count_hi2_solution("xxxx"), count_hi2("xxxx"));
    layout.print_test(format!("{:?}", ("h")), count_hi2_solution("h"), count_hi2("h"));
    layout.print_test(format!("{:?}", ("x")), count_hi2_solution("x"), count_hi2("x"));
    layout.print_test(format!("{:?}", ("")), count_hi2_solution(""), count_hi2(""));
    layout.print_test(format!("{:?}", ("Hellohi")), count_hi2_solution("Hellohi"), count_hi2("Hellohi"));
    layout.print_footer();
}

pub fn star_bit_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("star_bit");
    layout.print_header();
    layout.print_test(format!("{:?}", ("xyz,-abc*123")), star_bit_solution("xyz,-abc*123"), star_bit("xyz,-abc*123"));
    layout.print_test(format!("{:?}", ("x,-hello*")), star_bit_solution("x,-hello*"), star_bit("x,-hello*"));
    layout.print_test(format!("{:?}", (",-xy*1")), star_bit_solution(",-xy*1"), star_bit(",-xy*1"));
    layout.print_test(format!("{:?}", ("not really ,-possible*")), star_bit_solution("not really ,-possible*"), star_bit("not really ,-possible*"));
    layout.print_test(format!("{:?}", (",-abc*")), star_bit_solution(",-abc*"), star_bit(",-abc*"));
    layout.print_test(format!("{:?}", (",-abc*xyz")), star_bit_solution(",-abc*xyz"), star_bit(",-abc*xyz"));
    layout.print_test(format!("{:?}", (",-abc*x")), star_bit_solution(",-abc*x"), star_bit(",-abc*x"));
    layout.print_test(format!("{:?}", (",-x*")), star_bit_solution(",-x*"), star_bit(",-x*"));
    layout.print_test(format!("{:?}", (",-)*")), star_bit_solution(",-)*"), star_bit(",-)*"));
    layout.print_test(format!("{:?}", ("res ,-ipsa* loquitor")), star_bit_solution("res ,-ipsa* loquitor"), star_bit("res ,-ipsa* loquitor"));
    layout.print_test(format!("{:?}", ("hello,-not really*there")), star_bit_solution("hello,-not really*there"), star_bit("hello,-not really*there"));
    layout.print_test(format!("{:?}", ("ab,-ab*ab")), star_bit_solution("ab,-ab*ab"), star_bit("ab,-ab*ab"));
    layout.print_footer();
}

pub fn nest_paren_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("nest_paren");
    layout.print_header();
    layout.print_test(format!("{:?}", ("(())")), nest_paren_solution("(())"), nest_paren("(())"));
    layout.print_test(format!("{:?}", ("((()))")), nest_paren_solution("((()))"), nest_paren("((()))"));
    layout.print_test(format!("{:?}", ("(((x))")), nest_paren_solution("(((x))"), nest_paren("(((x))"));
    layout.print_test(format!("{:?}", ("((())")), nest_paren_solution("((())"), nest_paren("((())"));
    layout.print_test(format!("{:?}", ("((()()")), nest_paren_solution("((()()"), nest_paren("((()()"));
    layout.print_test(format!("{:?}", ("()")), nest_paren_solution("()"), nest_paren("()"));
    layout.print_test(format!("{:?}", ("")), nest_paren_solution(""), nest_paren(""));
    layout.print_test(format!("{:?}", ("(yy)")), nest_paren_solution("(yy)"), nest_paren("(yy)"));
    layout.print_test(format!("{:?}", ("(())")), nest_paren_solution("(())"), nest_paren("(())"));
    layout.print_test(format!("{:?}", ("(((y))")), nest_paren_solution("(((y))"), nest_paren("(((y))"));
    layout.print_test(format!("{:?}", ("((y)))")), nest_paren_solution("((y)))"), nest_paren("((y)))"));
    layout.print_test(format!("{:?}", ("((()))")), nest_paren_solution("((()))"), nest_paren("((()))"));
    layout.print_test(format!("{:?}", ("(())))")), nest_paren_solution("(())))"), nest_paren("(())))"));
    layout.print_test(format!("{:?}", ("((yy())))")), nest_paren_solution("((yy())))"), nest_paren("((yy())))"));
    layout.print_test(format!("{:?}", ("(((())))")), nest_paren_solution("(((())))"), nest_paren("(((())))"));
    layout.print_footer();
}

pub fn str_count_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("str_count");
    layout.print_header();
    layout.print_test(format!("{:?}", ("catcowcat", "cat")), str_count_solution("catcowcat", "cat"), str_count("catcowcat", "cat"));
    layout.print_test(format!("{:?}", ("catcowcat", "cow")), str_count_solution("catcowcat", "cow"), str_count("catcowcat", "cow"));
    layout.print_test(format!("{:?}", ("catcowcat", "dog")), str_count_solution("catcowcat", "dog"), str_count("catcowcat", "dog"));
    layout.print_test(format!("{:?}", ("cacatcowcat", "cat")), str_count_solution("cacatcowcat", "cat"), str_count("cacatcowcat", "cat"));
    layout.print_test(format!("{:?}", ("xyx", "x")), str_count_solution("xyx", "x"), str_count("xyx", "x"));
    layout.print_test(format!("{:?}", ("iiiijj", "i")), str_count_solution("iiiijj", "i"), str_count("iiiijj", "i"));
    layout.print_test(format!("{:?}", ("iiiijj", "ii")), str_count_solution("iiiijj", "ii"), str_count("iiiijj", "ii"));
    layout.print_test(format!("{:?}", ("iiiijj", "iii")), str_count_solution("iiiijj", "iii"), str_count("iiiijj", "iii"));
    layout.print_test(format!("{:?}", ("iiiijj", "j")), str_count_solution("iiiijj", "j"), str_count("iiiijj", "j"));
    layout.print_test(format!("{:?}", ("iiiijj", "jj")), str_count_solution("iiiijj", "jj"), str_count("iiiijj", "jj"));
    layout.print_test(format!("{:?}", ("aaabababab", "ab")), str_count_solution("aaabababab", "ab"), str_count("aaabababab", "ab"));
    layout.print_test(format!("{:?}", ("aaabababab", "aa")), str_count_solution("aaabababab", "aa"), str_count("aaabababab", "aa"));
    layout.print_test(format!("{:?}", ("aaabababab", "a")), str_count_solution("aaabababab", "a"), str_count("aaabababab", "a"));
    layout.print_test(format!("{:?}", ("aaabababab", "b")), str_count_solution("aaabababab", "b"), str_count("aaabababab", "b"));
    layout.print_footer();
}

pub fn str_copies_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("str_copies");
    layout.print_header();
    layout.print_test(format!("{:?}", ("catcowcat", "cat", 2)), str_copies_solution("catcowcat", "cat", 2), str_copies("catcowcat", "cat", 2));
    layout.print_test(format!("{:?}", ("catcowcat", "cow", 2)), str_copies_solution("catcowcat", "cow", 2), str_copies("catcowcat", "cow", 2));
    layout.print_test(format!("{:?}", ("catcowcat", "cow", 1)), str_copies_solution("catcowcat", "cow", 1), str_copies("catcowcat", "cow", 1));
    layout.print_test(format!("{:?}", ("iiijjj", "i", 3)), str_copies_solution("iiijjj", "i", 3), str_copies("iiijjj", "i", 3));
    layout.print_test(format!("{:?}", ("iiijjj", "i", 4)), str_copies_solution("iiijjj", "i", 4), str_copies("iiijjj", "i", 4));
    layout.print_test(format!("{:?}", ("iiijjj", "ii", 2)), str_copies_solution("iiijjj", "ii", 2), str_copies("iiijjj", "ii", 2));
    layout.print_test(format!("{:?}", ("iiijjj", "ii", 3)), str_copies_solution("iiijjj", "ii", 3), str_copies("iiijjj", "ii", 3));
    layout.print_test(format!("{:?}", ("iiijjj", "x", 3)), str_copies_solution("iiijjj", "x", 3), str_copies("iiijjj", "x", 3));
    layout.print_test(format!("{:?}", ("iiijjj", "x", 0)), str_copies_solution("iiijjj", "x", 0), str_copies("iiijjj", "x", 0));
    layout.print_test(format!("{:?}", ("iiiiij", "iii", 3)), str_copies_solution("iiiiij", "iii", 3), str_copies("iiiiij", "iii", 3));
    layout.print_test(format!("{:?}", ("iiiiij", "iii", 4)), str_copies_solution("iiiiij", "iii", 4), str_copies("iiiiij", "iii", 4));
    layout.print_test(format!("{:?}", ("ijiiiiij", "iiii", 2)), str_copies_solution("ijiiiiij", "iiii", 2), str_copies("ijiiiiij", "iiii", 2));
    layout.print_test(format!("{:?}", ("ijiiiiij", "iiii", 3)), str_copies_solution("ijiiiiij", "iiii", 3), str_copies("ijiiiiij", "iiii", 3));
    layout.print_test(format!("{:?}", ("dogcatdogcat", "dog", 2)), str_copies_solution("dogcatdogcat", "dog", 2), str_copies("dogcatdogcat", "dog", 2));
    layout.print_footer();
}

pub fn str_dist_run() {
    let layout = TestDisplay::default();
    layout.print_test_name_banner("str_dist");
    layout.print_header();
    layout.print_test(format!("{:?}", ("catcowcat", "cat")), str_dist_solution("catcowcat", "cat"), str_dist("catcowcat", "cat"));
    layout.print_test(format!("{:?}", ("catcowcat", "cow")), str_dist_solution("catcowcat", "cow"), str_dist("catcowcat", "cow"));
    layout.print_test(format!("{:?}", ("cccatcowcatxx", "cat")), str_dist_solution("cccatcowcatxx", "cat"), str_dist("cccatcowcatxx", "cat"));
    layout.print_test(format!("{:?}", ("abccatcowcatcatxyz", "cat")), str_dist_solution("abccatcowcatcatxyz", "cat"), str_dist("abccatcowcatcatxyz", "cat"));
    layout.print_test(format!("{:?}", ("xyx", "x")), str_dist_solution("xyx", "x"), str_dist("xyx", "x"));
    layout.print_test(format!("{:?}", ("xyx", "y")), str_dist_solution("xyx", "y"), str_dist("xyx", "y"));
    layout.print_test(format!("{:?}", ("xyx", "z")), str_dist_solution("xyx", "z"), str_dist("xyx", "z"));
    layout.print_test(format!("{:?}", ("z", "z")), str_dist_solution("z", "z"), str_dist("z", "z"));
    layout.print_test(format!("{:?}", ("x", "z")), str_dist_solution("x", "z"), str_dist("x", "z"));
    layout.print_test(format!("{:?}", ("", "z")), str_dist_solution("", "z"), str_dist("", "z"));
    layout.print_test(format!("{:?}", ("hiHellohihihi", "hi")), str_dist_solution("hiHellohihihi", "hi"), str_dist("hiHellohihihi", "hi"));
    layout.print_test(format!("{:?}", ("hiHellohihihi", "hih")), str_dist_solution("hiHellohihihi", "hih"), str_dist("hiHellohihihi", "hih"));
    layout.print_test(format!("{:?}", ("hiHellohihihi", "o")), str_dist_solution("hiHellohihihi", "o"), str_dist("hiHellohihihi", "o"));
    layout.print_test(format!("{:?}", ("hiHellohihihi", "ll")), str_dist_solution("hiHellohihihi", "ll"), str_dist("hiHellohihihi", "ll"));
    layout.print_footer();
}
