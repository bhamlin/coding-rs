#[macro_export]
macro_rules! srep {
    ($elem:expr; $n:expr) => {
        std::iter::repeat($elem).take($n).collect::<String>()
    };
}

#[macro_export]
macro_rules! batvec {
    () => {
        crate::display::BatVec(vec![])
    };
    ($elem:expr; $n:expr) => {
        crate::display::BatVec(vec![$elem, $n])
    };
    ($($x:expr),+ $(,)?) => {
        crate::display::BatVec(vec![$($x),+])
    };
}

#[macro_export]
macro_rules! strvec {
    ($($x:expr),+ $(,)?) => {
        vec![$($x.to_string()),+]
    };
}
