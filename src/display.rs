use colored::Colorize;
use itertools::Itertools;
use std::fmt::Display;

use crate::srep;

#[derive(Debug, PartialEq)]
pub struct BatVec<T>(pub Vec<T>);

pub struct TestDisplay {
    pub argument_width: usize,
    pub expected_width: usize,
    pub actual_width: usize,
}

impl Default for TestDisplay {
    fn default() -> Self {
        Self {
            argument_width: 35,
            expected_width: 20,
            actual_width: 20,
        }
    }
}

impl<T> Display for BatVec<T>
where
    T: ToString,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "[{}]", self.0.iter().map(|e| e.to_string()).join(","))
    }
}

impl<T> From<Vec<T>> for BatVec<T> {
    fn from(value: Vec<T>) -> Self {
        BatVec(value)
    }
}

impl From<Vec<&str>> for BatVec<String> {
    fn from(value: Vec<&str>) -> Self {
        BatVec(value.iter().map(|e| e.to_string()).collect::<Vec<String>>())
    }
}

impl From<BatVec<&str>> for BatVec<String> {
    fn from(value: BatVec<&str>) -> Self {
        BatVec(value.0.iter().map(|e| e.to_string()).collect::<Vec<String>>())
    }
}

impl TestDisplay {
    pub fn print_test_name_banner(&self, name: impl Display) {
        println!("-----------------");
        println!("Test: {name}");
    }

    pub fn print_header(&self) {
        println!(
            "     ┌{}┐   ┌{}┐   ┌{}┐",
            // iter::repeat('─').take(self.argument_width).collect::<String>()
            srep!('─'; self.argument_width),
            srep!('─'; self.expected_width),
            srep!('─'; self.actual_width),
        );
        println!(
            "     │{:^r_width$}│   │{:>e_width$}│   │{:<a_width$}│",
            "Arguments".yellow(),
            "Expected".yellow(),
            "Actual".yellow(),
            r_width = self.argument_width,
            e_width = self.expected_width,
            a_width = self.actual_width
        );
        // println!("┌────┼───────────────────────────────────┼───┼────────────────────┼───┼────────────────────┤");
        println!(
            "┌────┼{}┼───┼{}┼───┼{}┤",
            srep!('─'; self.argument_width),
            srep!('─'; self.expected_width),
            srep!('─'; self.actual_width),
        );
    }

    pub fn print_test<T>(&self, args: String, expected: T, actual: T)
    where
        T: Display + PartialEq,
    {
        let result = if actual == expected { "PASS".green() } else { "FAIL".red() };
        let arrow = if actual == expected { "→".black().bold() } else { "→".red() };
        let symbol = if actual == expected { "≡".black().bold() } else { "≠".white() };
        let bar = "│".black().bold();

        let expected = expected.to_string();
        let actual = actual.to_string();

        println!(
            "│{result}│{args:^r_width$}│ {arrow} │{expected:>e_width$}{bar} {symbol} {bar}{actual:<a_width$}│",
            r_width = self.argument_width,
            e_width = self.expected_width,
            a_width = self.actual_width
        );
    }

    pub fn print_footer(self) {
        println!(
            "└────┴{}┴───┴{}┴───┴{}┘",
            srep!('─'; self.argument_width),
            srep!('─'; self.expected_width),
            srep!('─'; self.actual_width),
        );
    }
}

// #[deprecated(note = "This functionality has moved to reside within display::TestDisplay")]
// pub fn begin_test_layout() {
//     println!("     ┌───────────────────────────────────┐   ┌────────────────────┐   ┌────────────────────┐");
//     println!("     │{:^35}│   │{:>20}│   │{:<20}│", "Arguments".yellow(), "Expected".yellow(), "Actual".yellow());
//     println!("┌────┼───────────────────────────────────┼───┼────────────────────┼───┼────────────────────┤");
// }

// #[deprecated(note = "This functionality has moved to reside within display::TestDisplay")]
// pub fn run_test_sample<T>(args: String, expected: T, actual: T)
// where
//     T: Display + PartialEq,
// {
//     let result = if actual == expected { "PASS".green() } else { "FAIL".red() };
//     let arrow = if actual == expected { "→".black().bold() } else { "→".red() };
//     let symbol = if actual == expected { "≡".black().bold() } else { "≠".white() };
//     let bar = "│".black().bold();

//     let expected = expected.to_string();
//     let actual = actual.to_string();

//     println!("│{result}│{args:^35}│ {arrow} │{expected:>20}{bar} {symbol} {bar}{actual:<20}│");
// }

// #[deprecated(note = "This functionality has moved to reside within display::TestDisplay")]
// pub fn end_test_layout() {
//     println!("└────┴───────────────────────────────────┴───┴────────────────────┴───┴────────────────────┘");
// }
